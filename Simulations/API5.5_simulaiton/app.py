from flask import Flask, render_template, request, jsonify
import datetime
from TAVEngine.BVCEngineAPI import BVCEngineAPI as API
app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True


@app.route('/index')
def hello():
    return "Hello World!"


@app.route('/file', methods=['POST'])
def file():

    full_stream = request.stream.read()

    full_stream = full_stream[44:]

    api = API()
    mid_res = api.process_stream(full_stream)
    p = api.end_session()
    print('p = {}'.format(p))
    return jsonify(p)

@app.route('/')
def index():
    user = {'username': 'Miguel'}
    year = 2018
    return render_template('analyze.html', title='Home', user=user,year=year)

@app.route('/chart')
def chart():
    year = 2018
    return render_template('chart.html', title='Chart', year=year)


@app.route('/hosp')
def hosp():
    year = 2018
    return render_template('hospitality.html', title='Hospitalization', year=year)


@app.route('/list')
def list():
    now = datetime.datetime.now()
    year = now.year
    return render_template('listofpatients.html', title='List of patients with their scores', year=year)


if __name__ == '__main__':
    app.run()
