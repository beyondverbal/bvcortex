import pandas as pd
import numpy as np
from sklearn.model_selection import StratifiedKFold

K_FOLDS = 10
MD0_FILE = 'E:/Algo/dat/MetaData/PatientsMD181003.csv'
MD1_FILE = 'E:/Algo/dat/MetaData/RecordingsData181003.csv'
MD_OSA_FILE = 'E:/Algo/dat/MetaData/AHI_BMI.csv'

OUT_FILE = 'E:/Algo/dat/MetaData/PatientsMDnFolds181023.csv'

def stratify_by_patients(k_folds=K_FOLDS):
    md0 = pd.DataFrame.from_csv(MD0_FILE, index_col=None, encoding='utf-8')
    md1 = pd.DataFrame.from_csv(MD1_FILE, index_col=None)

    p_list = np.unique(md1['patient_id'].values)
    p_filter = pd.DataFrame(p_list, columns=['patient_id'])

    md_df = pd.merge(left=md0, right=p_filter, on='patient_id')

    mdOSA = pd.DataFrame.from_csv(MD_OSA_FILE, index_col=None)

    md_df = pd.merge(left=md0, right=md1, on='patient_id')

    xny_df = xny_df[(((xny_df['COPD_CD'] == 0) & ((xny_df['Asthma'] == 1) | (xny_df['Bronchitis'] == 1))) == 0)]

    # OSA = md_df['OSA'].values.astype(str)
    Gender = md_df['patient_gender'].values.astype(str)
    Age = 2018 - md_df['CUSTOMER_BIRTH_Year'].values
    BMI = md_df.BMI.values

    age_threshold = np.median(Age)
    BMI_threshold = np.nanmedian(BMI)
    stratification_key = []
    for idx in range(md_df.shape[0]):
        if Age[idx] < age_threshold:
            age_group = 'age1'
        else:
            age_group = 'age2'
        if np.isnan(BMI[idx]) or BMI[idx] < BMI_threshold:
            BMI_group = 'BMI1'
        else:
            BMI_group = 'BMI2'
        stratification_key.append(Gender[idx] + '_' + age_group + '_' + BMI_group) # + '_OSA' + OSA[idx])

    skf = StratifiedKFold(n_splits=K_FOLDS, shuffle=True)
    folds = skf.split(stratification_key, stratification_key)

    fold_id = 0
    folds_list = np.zeros(md_df.shape[0])
    for train_index, test_index in folds:
        folds_list[test_index] = fold_id
        fold_id += 1

    md_df = md_df.assign(stratification_key=stratification_key)
    md_df = md_df.assign(fold_id=folds_list)

    md_df = md_df.assign(cluster_weights=0)
    clusters, counts = np.unique(stratification_key, return_counts=True)
    for i in range(len(clusters)):
        idx = md_df[md_df.stratification_key == clusters[i]].index
        md_df.loc[idx,'cluster_weights'] = 1/counts[i]

    md_df.to_csv(OUT_FILE, index=False)


if __name__ == '__main__':

    stratify_by_patients(k_folds=K_FOLDS)
