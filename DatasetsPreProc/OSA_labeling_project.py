# Define a list of pre-labeled OSA patient, in order to label more files from them

import pandas as pd

thor_labeled = pd.read_csv('E:/Algo/dat/MetaData/DB4Segments190123.csv')[['patient_id']]
OSA_files = pd.read_csv('E:/Algo/dat/MetaData/OSA_AHI_BMI_wavs_190123.csv')[['patient_id']+['date']]

thor_labeled.drop_duplicates(subset=['patient_id'], inplace=True)
OSA_files.drop_duplicates(subset=['patient_id', 'date'], inplace=True)

merged = pd.merge(left=thor_labeled, right=OSA_files, on='patient_id')

print(merged.shape)

merged.to_csv('E:/Algo/dat/MetaData/OSA_label_project2.csv', index=False)