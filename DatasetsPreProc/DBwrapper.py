import pandas as pd
import pyodbc
import pymysql


class DBwrapper:
    def __init__(self, db_name):

        if db_name=='MKM':
            server = r'MKM-RESEARCH-DB\MKMSRV'
            database = 'BeyondVerbal'
            password = 'PASSWORD'
            username = 'USERNAME'
            self.conn = pyodbc.connect('DRIVER={SQL Server Native Client 11.0};SERVER=' + server + ';DATABASE=' + database + ';UID=' + username + ';PWD=' + password + '; Trusted_Connection=Yes')
        elif db_name=='THOR_MACCABI3':
            self.conn = pymysql.connect(host='127.0.0.1', port=3306, charset='utf8', user='root', password='bvc2017', db='bvc3')
        elif db_name=='THOR_MACCABI4':
            self.conn = pymysql.connect(host='127.0.0.1', port=3306, charset='utf8', user='root', password='bvc2017', db='bvc4')

    def Query2DF(self, sql):
        return pd.read_sql(sql, self.conn)
