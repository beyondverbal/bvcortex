import re,glob,ntpath,sys
import pandas as pd
import numpy as np

from scipy.interpolate import BPoly
import matplotlib.pyplot as plt

date_suffix = '190123.csv'

ONLY_FIRST_LAB = True # False #
print('ONLY_FIRST_LAB = {}'.format(ONLY_FIRST_LAB))

cutflow_list = []
cutflow_dict = {}

def print_cutflow(cutflow_list, cutflow_dict):
    print('Cutflow:')
    for cut in cutflow_list:
        print('{}: {}'.format(cut, cutflow_dict[cut]))


sections = {'אבחנה': ['אבחנות בביקור','הנחבא', 'אבחנה',"תונחבא",'עקר תונחבא',"רוקיבב תונחבא"],
            'סיבת הפניה': ['סיבת פניה','סיבת הפניה','תביס הינפה','הינפ תביס','הינפה תביס','ינפה תביס ה'],
            'אבחנות רקע': ['אבחנות רקע','תירקיע הנולת','עקר תונחבא','אבחנות רקע','מחלות', 'תולחמ','עקר יטרפו תולחמ','ופרטי רקע'],
            'תרופות קבועות': ['תרופות קבועות','תועובק תופורת','תופורת', 'ת תופור'],
            'תוצאות': ['תואצות','צות תוא','ואצות ת','תוצאות','אצות תו','בליל', 'לילב'],
            "סיכום": ['תוצלמהו םוכס','סכום', 'םוכס','םוכיס',"סיכום", 'תוצלמהו םוכיס']}

# section_list = ['אבחנה', 'סיבת הפניה', 'אבחנות רקע', 'תרופות קבועות', 'תוצאות', 'סיכום']
section_list = ['אבחנה', 'סיבת הפניה', 'אבחנות רקע', 'תרופות קבועות', 'תוצאות']

ahipat = [
        "תופיפצ הנישב המישנ תערפה AHI\s*\=?\s?([0-9]*\.?[0-9]*)\s*העשל םיעוריא\s?" ,
            "AHI[\s]*[\=]?[\.]?[\-]?[\s]*([0-9]*\.?[0-9]*)",
        "העשל םיעוריא\s*([0-9]*\.?[0-9]*)\s*המישנ תוקספה"   ,
        # "תופיפצ\s*\.?\s*תויקלח המישנ תוקספה\s*([0-9]*\.?[0-9]*)\s*ונחבוא",
        "המישנ תוקספה\s??–\\s?\d*\s?\w+\s?\,? תופיפצב\s*לש ([0-9]*\.?[0-9]*) העשל",
          # "([0-9]*\.?[0-9]*)[\s?]+של[\s]+AHI","([0-9]*\.?[0-9]*)\s*AHI", #"(\d+)\s*AHI",
          "AHI\=?\-?([0-9]*\.?[0-9]*)",
          # "([0-9]*\.?[0-9]*)\s?\-?\s?AHI", ###
          # "([0-9]*\.?[0-9]*)\s?\.?\=?\s?\.?AHI", ###
          "AHI\=?\s?\n\.?\s?([0-9]*\.?[0-9]*)",
          # "לשעה\s?([0-9]*\.?[0-9]*)\s?בצפיפות של",
          # "צפיפות הפסקות נשימה\s+([0-9]*\.?[0-9]*)",
          #   "\s*\.?([0-9]*\.?[0-9]*)המישנ תוקספה תופיפצ",
          #   #"לשעה\s?([0-9]*\.?[0-9]*)\s?של\s?בצפיפות"
          # "לש תופיפצב\s?([0-9]*\.?[0-9]*)\s?העשל"
         "המישנ תוקספה ?–\\s?\d*\s*\w*\s*\,*\s*לש\s*\n*תופיפצב\s*([0-9]*\.?[0-9]*)\s?העשל",
        '[\n]*\s*\w*\s*\,?\s*תופיפצב[\n]* לש ([0-9]*\.?[0-9]*) העשל',
        "המישנ תוקספה\s*[\–]?\s*\d*\s*\w*\s*?[\,]\s*תופיפצב\n*לש\s*([0-9]*\.?[0-9]*)\s*העשל",
        # "העשל םיעוריא\s*([0-9]*\.?[0-9]*)\s*המישנ תוקספה תופיפצ",
         "המישנ תוקספה *–\\s*\d*\s*\w*\s*\,?\s*\n*לש תופיפצב ([0-9]*\.?[0-9]*) העשל",
         "המישנ תוקספה\n?\w?\s*\w*\s?\–?\s?\d*\s?\w*\s??\,?\\s?לש תופיפצב\s?([0-9]*\.?[0-9]*)\s?העשל",
        "םיעוריא\s*([0-9]*\.?[0-9]*)\s*המישנ תוקספה תופיפצ",
        "(\d+)\s*AHI",
        'לש תופיפצב\s*([0-9]*\.?[0-9]*)\s?',
        '\s*([0-9]*\.?[0-9]*)\s?העשל םיעוריא',
        'העשל םיעוריא\s*([0-9]*\.?[0-9]*)\s?',
        "המישנ תוקספה\s*([0-9]*\.?[0-9]*)\s?העשל",
]

zero_ahi = ['תלבוס\s*הניא',
            'לבוס\s*וניא',
            'אינה\s*סובלת',
            'אינו\s*סובל',
            'תלבוס\s*הנניא',
            'לבוס\s*ונניא',
            'איננה\s*סובלת',
            'איננו\s*סובל',
            'ת/לבוס\s*ה/וניא',
            'ת/לבוס\s*ה/ונניא',
            'הקול\s*הניא',
            'הקול\s*וניא',
            'אינו\s*לוקה',
            'אינה\s*לוקה',
            'הניא\s*הקול',
            'וניא\s*הקול',
            'לוקה\s*אינו',
            'לוקה\s*אינה',
]


bmiregex = ['BMI\s*\=?\s*([0-9]*\.?[0-9]*)']
mishkalregex = ['לקשמ\s*([0-9]*\.?[0-9]*)\s?ק\s?"\s?ג']
highregex  = ['הבוג\s*([0-9]*\.?[0-9]*)\s*ס\s?"\s?מ']
genderrx =['ןימ\s*(הבקנ|רכז|ז|נ)','ןימ\s*\w*\s*(הבקנ|רכז|ז|נ)\s*']
agerx =['ליג\s*\w*\s*([0-9]*\.?[0-9]*)','ליג\s*([0-9]*\.?[0-9]*)',]


confphrases  =["הקידב הניש","הניש הקידב","תיפרגונמוסילופ"]
notconf = ["(תמאתהל\s*הקידב)", 'בדיקה\s*להתאמת']
def checkPhrases(p,text):
    for ph in confphrases:
        a = re.findall("({})".format(ph),text)
        if len(a)>0:
            p["doctype"] = 1
    for phh in notconf:
        aa = re.findall(phh,text)
        if len(aa)>0:
            p["doctype"]= -1



def GetTextSegments(text):

    # find summary
    s_marker = []
    for w in sections['סיכום']:
        pp = re.compile(w)
        for m in pp.finditer(text):
            if (text[m.start(0) - 1:m.start(0)] == '\n' or text[m.start(0)+len(w):m.start(0)+len(w)+1] == '\n'):
                s_marker.append(m.start(0))
    if len(s_marker) > 0:
        end_marker = min(s_marker)
    else:
        end_marker = -1

    seg_key = []
    seg_start = []
    marker = 0
    for s in section_list:
        s_marker = []
        for w in sections[s]:
            pp = re.compile(w)
            for m in pp.finditer(text[marker:end_marker]):
                potential_marker = marker + m.start(0)
                if marker == 0 or (marker > 0 and (text[potential_marker-1:potential_marker] == '\n' or text[potential_marker+len(w):potential_marker+len(w)+1] == '\n') ):
                    s_marker.append(potential_marker)
        if len(s_marker)>0:
            marker = min(s_marker)
            seg_start.append(marker)
            seg_key.append(s)
    if end_marker != -1:
        seg_start.append(end_marker)
        seg_key.append('סיכום')
    return seg_key, seg_start


def combine_lines(text):
    split_text = text.split('\n')
    full_text_forward = ''
    full_text_backwards = ''
    segment_num = len(split_text)

    for i in range(segment_num):
        full_text_forward += split_text[i]
        full_text_backwards += split_text[segment_num-1-i]

    return full_text_forward, full_text_backwards



def find_set(p,exp,indx,warr):
    for i,it in enumerate(exp):
        val = it.group(1)
        if is_number(val):
            s = it.start(0)
            if not val in warr and not(indx>2 and i>0):
                p["AHI"] += "{};".format(val)
                p['exp']+= "{};".format(str(indx))
                p['val_count']+=1
                warr.append(val)

def find_aset(p,name,qarr,text):
    warr = []
    for query in qarr:
        exp = re.finditer(query, text)
        for i,it in enumerate(exp):
            val = it.group(1)
            if is_number(val):
                if not val in warr :
                    p[name] = val
                    warr.append(val)

def find_nset(p,name,qarr,text):
    warr = []
    for query in qarr:
        exp = re.finditer(query, text)
        for i,it in enumerate(exp):
            val = it.group(1)
            if not val in warr :
                if val=='ז' or val=='רכז':
                    val = 'M'
                if val=='נ' or val=='הבקנ':
                    val = 'F'
                p[name] = val
                warr.append(val)

def setProperty(p,line):
    if len(line)<=2:
        p['doctype']="-10"
        return

    seg_key, seg_start = GetTextSegments(line)
    seg_start.append(-1)

    warr = []
    for idx, key in enumerate(seg_key):
        p[key] = line[seg_start[idx]:seg_start[idx+1]]
        if key=='תוצאות':
            text = line[seg_start[idx]:seg_start[idx+1]]
            checkPhrases(p,text)
            for inx,aquery in enumerate(ahipat):
                a = re.finditer(aquery, text)
                find_set(p,a,inx,warr)

            a = re.findall('((0[1-9]|[12]\d|3[01])/(0[1-9]|1[0-2])/[12]\d{3})', text)
            if len(a)>0:
                p['date']= a[0][0] if len(a[0])>0 else a[0]
            if p["AHI"]=='':
                ntext = text.replace('\n','').replace('\r','') # TODO: is ntext obsolete?
                ftext, btext = combine_lines(text)
                checkPhrases(p, text)
                for inx, aquery in enumerate(ahipat):
                    a = re.finditer(aquery, ntext)
                    find_set(p, a,inx,warr)
                    a = re.finditer(aquery, ftext)
                    find_set(p, a, inx, warr)
                    a = re.finditer(aquery, btext)
                    find_set(p, a, inx, warr)

        elif key=='סיכום' and p["AHI"]=='':
            text = line[seg_start[idx]:seg_start[idx + 1]]
            for inx,aquery in enumerate(ahipat):
                a = re.finditer(aquery, text)
                find_set(p,a,inx,warr)
            if p["AHI"]=='':
                for w in zero_ahi:
                    pp = re.compile(w)
                    for m in pp.finditer(text):
                        p["AHI"] += "0;"
                        p['val_count'] += 1

            # if after summary, the value is still empy, search the entire text for it and the date
            # this is for people with doctor appointments after the lab, with no lab report
            if p['AHI']=='':
                for inx, aquery in enumerate(ahipat):
                    a = re.finditer(aquery, line)
                    find_set(p, a, inx, warr)
                if p["AHI"] == '':
                    for w in zero_ahi:
                        pp = re.compile(w)
                        for m in pp.finditer(line):
                            p["AHI"] += "0;"
                            p['val_count'] += 1
                if p['date'] == '':
                    a = re.findall('((0[1-9]|[12]\d|3[01])/(0[1-9]|1[0-2])/[12]\d{3})', line)
                    if len(a) > 0:
                        p['date'] = a[0][0] if len(a[0]) > 0 else a[0]
                    if p['date'] == '':
                        a = re.findall('((0[1-9]|1[0-2])/[12]\d{3})', line)
                        if len(a) > 0:
                            p['date'] = a[0][0] if len(a[0]) > 0 else a[0]
                if p['AHI'] != '' and p["doctype"] == '':
                    p["doctype"] = 0.5


        elif key=='אבחנות רקע':
            text = line[seg_start[idx]:seg_start[idx+1]]
            ntext = text.replace('\n', '').replace('\r', '')
            find_aset(p,'BMI',bmiregex,ntext )
            find_aset(p,'age', agerx, ntext)
            find_aset(p,'height', highregex, ntext)
            find_aset(p,'weight',mishkalregex, ntext)
            find_nset(p,'gender',genderrx, ntext)


def sortcsv():
    a = ['file','fpath','AHI','exp','val_count','doctype','date','BMI','age','height','weight','gender']
    for k in sections:
        a.append(k)

    return a

def progress(count,total,status=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))
    percent = round(100.0 * count / float(total),1)

    bar = '#' * filled_len + '-' * (bar_len - filled_len)
    sys.stdout.write('\r[%s] %s%s %s' % (bar,percent,'%',status))
    sys.stdout.flush()

def is_number(s):
    try:
        f = float(s)
        if f> 200:
            return False
        return True
    except ValueError:
        return  False


def getfiles():
    d = []
    c = 0
    file_iter = glob.iglob('E:\OSA_pdf2txt/**/*.txt', recursive=True)
    file_num = sum(1 for x in file_iter)
    cutflow_list.append('report_num')
    cutflow_dict.update({'report_num': file_num})
    for fpath in glob.iglob('E:\OSA_pdf2txt/**/*.txt', recursive=True):
        fn = ntpath.basename(fpath)
        with open(fpath, 'r', encoding='utf-8') as openfile:
            # if fpath=='E:\\OSA_pdf2txt\\2014\\10\\15\\AS80000000062088001.txt':
            #     print()
            line = openfile.read()
            line = re.sub("\r\r\n\s*", "\n", line)
            line = re.sub("\s*\n\n\s*","\n",line)
            line = re.sub("\t", "", line)
            p={'file':fn,'fpath':fpath,'AHI':'','exp':'','val_count':0,'doctype':'','date':'','BMI':'','age':'','height':'','weight':'','gender':''}
            setProperty(p,line)
            d.append(p)
            c += 1
            progress(c,file_num)


    df = pd.DataFrame(d)
    df = df[sortcsv()]
    cutflow_list.append('is_parsed')
    cutflow_dict.update({'is_parsed': df.shape[0]})
    print()

    xls = pd.ExcelFile("E:/OSA_pdf2txt/PDF Files MetaData for BV.xlsx")
    data = xls.parse('Sheet1')

    file_list = []
    date_list = []
    path_list = data['File Name'].values
    for path in path_list:
        new_name = path.rsplit('\\',1)[1][:-3] + 'txt'
        file_date = path.rsplit('\\',1)[0]
        file_date = file_date.replace('\\', '-')[1:]
        file_list.append(new_name)
        date_list.append(file_date)
    data = data.assign(file=file_list)
    data = data.assign(file_date=date_list)
    data.file_date = pd.to_datetime(data.file_date)

    merged = pd.merge(left=df, right=data, how='left', left_on='file', right_on='file')
    merged = fill_missing_dates(merged)

    cutflow_list.append('hasPDF_MD')
    cutflow_dict.update({'hasPDF_MD': merged.shape[0]})


    merged.to_csv('E:/Algo/dat/MetaData/OSA_AHI_' + date_suffix, index=False, encoding='utf-8-sig')


def fill_missing_dates(df):
    df_dates = df.dropna(subset=['ID','date','AHI','file_date']).copy()
    df_dates.date = pd.to_datetime(df_dates.date)

    df_dates = df_dates[(df_dates.date != '') & (df_dates.file_date != '')]

    df_dates = df_dates[df_dates.file_date >= df_dates.date]

    distance = (df_dates.file_date - df_dates.date).dt.days
    median_dist = np.median(distance)
    # print('median distance between lab and report is {}.'.format(median_dist))

    missing_index = df[(df.val_count > 0) & (df.date == '') & (df.doctype != -1)].index
    df.at[missing_index, 'date'] = pd.DatetimeIndex(df.loc[missing_index,'file_date']) - pd.DateOffset(median_dist)

    return df



def produce_file_list():
    ahi_df = pd.read_csv('E:/Algo/dat/MetaData/OSA_AHI_' + date_suffix, index_col=None, encoding='utf-8-sig')

    ahi_df.dropna(subset=['AHI'], inplace=True)
    cutflow_list.append('hasAHI')
    cutflow_dict.update({'hasAHI': ahi_df.shape[0]})

    ahi_df.dropna(subset=['date'], inplace=True)  # TODO: this does not remove date '' values, check if no bug
    cutflow_list.append('hasDate')
    cutflow_dict.update({'hasDate': ahi_df.shape[0]})

    ahi_df.dropna(subset=['ID'], inplace=True)
    cutflow_list.append('hasPatientID')
    cutflow_dict.update({'hasPatientID': ahi_df.shape[0]})

    ahi_df.rename(columns={'ID': 'patient_id'}, inplace=True)
    ahi_df.date = pd.to_datetime(ahi_df.date)

    ahi_df = ahi_df[ahi_df.doctype != -1]
    cutflow_list.append('not_CPAPtest')
    cutflow_dict.update({'not_CPAPtest': ahi_df.shape[0]})

    miss_bmi_index = ahi_df[((ahi_df.BMI != ahi_df.BMI) | (ahi_df.BMI < 10)) &
                            ((ahi_df.height == ahi_df.height) & (ahi_df.weight == ahi_df.weight) & (ahi_df.height != 0))].index
    ahi_df.at[miss_bmi_index, 'BMI'] = 10000 * ahi_df.weight / (ahi_df.height*ahi_df.height)

    bmi_df = pd.read_csv('E:/Algo/dat/MetaData/BMI_181219.csv', index_col=None, encoding='utf-8-sig')

    bmi_df.Date_Medida_Refuit = pd.to_datetime(bmi_df.Date_Medida_Refuit)
    bmi_df.rename(columns={'customer_seq': 'patient_id', 'Date_Medida_Refuit': 'date'}, inplace=True)

    bmi_df = combine_BMI_info(ahi_df, bmi_df)

    # wav_df = pd.read_csv('E:/Algo/dat/MetaData/R2P_MRGD181218.csv', index_col=None, encoding='utf-8-sig')
    wav_df = pd.read_csv('E:/Algo/dat/R2P/R2P_MRGD190122.csv', index_col=None, encoding='utf-8-sig')


    patient_list = np.unique(ahi_df.patient_id.values)

    wav_df, ahi_df = estimate_BMI(bmi_df, wav_df, ahi_df, patient_list=patient_list)

    ahi_df = ahi_df[['patient_id']+['BMI']+['AHI']+['val_count']+['date']+['has_BMI_MOMA']]
    cutflow_list.append('unique_patient_num')
    cutflow_dict.update({'unique_patient_num': len(np.unique(ahi_df.patient_id.values))})

    ahi_df.rename(columns={'BMI': 'lab_BMI'}, inplace=True)
    wav_df.rename(columns={'BMI': 'rec_BMI'}, inplace=True)

    if ONLY_FIRST_LAB:
        ahi_df.sort_values(['patient_id', 'date'], inplace=True)
        ahi_df.drop_duplicates(subset='patient_id', keep='first', inplace=True)

    files_df = pd.merge(right=wav_df, left=ahi_df, on='patient_id')

    cutflow_list.append('has_wav')
    cutflow_dict.update({'has_wav': len(np.unique(files_df.patient_id.values))})

    files_df = files_df[files_df.has_BMI_MOMA == 1]
    cutflow_list.append('has_BMI_MD')
    cutflow_dict.update({'has_BMI_MD': len(np.unique(files_df.patient_id.values))})

    files_df.dropna(subset=['lab_BMI'], inplace=True)
    cutflow_list.append('has_labBMI')
    cutflow_dict.update({'has_labBMI': len(np.unique(files_df.patient_id.values))})

    files_df.dropna(subset=['rec_BMI'], inplace=True)
    cutflow_list.append('has_wavBMI')
    cutflow_dict.update({'has_wavBMI': len(np.unique(files_df.patient_id.values))})

    files_df.at[:,'diff_BMI'] = (files_df.lab_BMI - files_df.rec_BMI).abs()

    distance = (files_df.date - files_df.rec_date).dt.days
    files_df = files_df.assign(distance2lab=distance)
    files_df = files_df[files_df.distance2lab >= 0]
    cutflow_list.append('has_wav_before_lab')
    cutflow_dict.update({'has_wav_before_lab': len(np.unique(files_df.patient_id.values))})


    files_df = data_cleanup(files_df, BMI_diff_threshold=2.)

    files_df.to_csv('E:/Algo/dat/MetaData/OSA_AHI_BMI_wavs_' + date_suffix, index=False)

def data_cleanup(df, BMI_diff_threshold=None):
    df = df[df['val_count'] == 1]
    df.to_csv('E:/Algo/Nimrod/Debug/OSA_AHI_BMI_wavs_preThreshold.csv', index=False)
    patient_num = len(np.unique(df.patient_id.values))
    cutflow_list.append('has_1AHIval')
    cutflow_dict.update({'has_1AHIval': patient_num})
    if BMI_diff_threshold is not None:
        df = df[df.diff_BMI < BMI_diff_threshold]
    patient_num = len(np.unique(df.patient_id.values))
    cutflow_list.append('pass_BMI_threshold')
    cutflow_dict.update({'pass_BMI_threshold': patient_num})
    return df

def combine_BMI_info(ahi_df, bmi_df):
    lab_bmi = ahi_df[['patient_id']+['date']+['BMI']].copy()
    lab_bmi.dropna(subset=['patient_id', 'date', 'BMI'], inplace=True)

    bmi_df = pd.concat([bmi_df, lab_bmi], ignore_index=True)

    return bmi_df

def build_BMI_model(x, y):
    der = []
    for i in range(len(x)):
        if i == 0:
            x_diff = x[i + 1] - x[i]
            y_diff = y[i + 1] - y[i]
        elif i == len(x) - 1:
            x_diff = x[i] - x[i - 1]
            y_diff = y[i] - y[i - 1]
        else:
            x_diff = x[i + 1] - x[i - 1]
            y_diff = y[i + 1] - y[i - 1]

        if x_diff != 0:
            der.append(y_diff / x_diff)
        else:
            der.append(np.inf)

    y_der = []
    for i in range(len(y)):
        if np.isinf(der[i]):
            y_der.append([y[i]])
        else:
            y_der.append([y[i], der[i]])

    s = BPoly.from_derivatives(x, y_der)
    return s

def estimate_BMI(bmi_df, wav_df, ahi_df, patient_list=None):

    days_x = (bmi_df.date - pd.datetime(1970, 1, 1, 0, 0)).dt.days
    bmi_df = bmi_df.assign(days_x=days_x)

    wav_df['rec_date'] = wav_df['rec_date'].str.slice(start=0, stop=10)
    wav_df.rec_date = pd.to_datetime(wav_df.rec_date)
    days_x = (wav_df.rec_date - pd.datetime(1970, 1, 1, 0, 0)).dt.days
    wav_df = wav_df.assign(days_x=days_x)

    ahi_df.date = pd.to_datetime(ahi_df.date)
    days_x = (ahi_df.date - pd.datetime(1970, 1, 1, 0, 0)).dt.days
    ahi_df = ahi_df.assign(days_x=days_x)

    ahi_df = ahi_df.assign(has_BMI_MOMA=np.zeros(ahi_df.shape[0]))

    if patient_list is None:
        patient_list = np.unique(bmi_df.patient_id.values)

    fail_count = 0
    progress_c = 0
    for patient in patient_list:

        progress_c += 1
        progress(progress_c, len(patient_list))

        # Skip if there are no wavs to estimate
        if wav_df[wav_df.patient_id == patient].shape[0] == 0:
            continue

        patient_bmi = bmi_df[bmi_df.patient_id == patient].copy()
        patient_bmi.sort_values(['days_x'], inplace=True)

        try:
            x = patient_bmi.days_x.values
            y = patient_bmi.BMI.values

            # can't have same-value x's
            repeated_val_idx = [i for i in range(1,len(x)) if x[i]==x[i-1]]
            x = np.delete(x, repeated_val_idx)
            y = np.delete(y, repeated_val_idx)

            if len(x) <= 1:
                continue

            patient_idx = ahi_df[ahi_df.patient_id == patient].index
            ahi_df.at[patient_idx, 'has_BMI_MOMA'] = [1] * len(patient_idx)

            s = build_BMI_model(x, y)

            mean_y = np.mean(y)
            std_y = np.std(y)

            # Estimate on wavs
            patient_idx = wav_df[wav_df.patient_id==patient].index
            wav_df, outliers = estimate_on_index(wav_df, patient_idx, s, mean_y, std_y)

            # if len(outliers)>0:
            #     plot_BMI(s, x, y, wav_df.loc[patient_idx, 'days_x'].values, outliers)

            # Estimate on pdf
            patient_idx = ahi_df[(ahi_df.patient_id==patient) & (ahi_df.BMI!=ahi_df.BMI)].index
            ahi_df, outliers = estimate_on_index(ahi_df, patient_idx, s, mean_y, std_y)

        except:
            fail_count += 1
            continue

    print()
    print('Failed for {} out of {} patients.'.format(fail_count, len(patient_list)))
    return wav_df, ahi_df

def estimate_on_index(df, idx, s, mean, std):
    xs = df.loc[idx, 'days_x'].values
    ys = s(xs)

    if True:
        outer_x = np.where((xs < s.x[0]) | (xs > s.x[-1]))[0]
        # remove_idx = np.where(np.abs(ys - mean) > 3 * std)[0]
        outlier_idx = np.where(np.abs(ys - mean) > 3 * std)[0]
        remove_idx = [i for i in outlier_idx if i in outer_x]
        ys[remove_idx] = np.nan
        df.at[idx, 'BMI'] = ys

        outliers = []
        if len(remove_idx)>0:
            outliers = remove_idx

    else:
        if len(s.x) > 2:
            upper_range = s.x[-1] - s.x[-3]
            upper_border = s.x[-1] + upper_range
            upper_mean = np.mean(s(s.x[-3:]))

            lower_range = s.x[2] - s.x[0]
            lower_border = s.x[0] - lower_range
            lower_mean = np.mean(s(s.x[:3]))

        if len(s.x) == 2:
            range = s.x[1] - s.x[0]
            upper_border = s.x[1] + range
            lower_border = s.x[0] - range
            upper_mean = np.mean(s(s.x))
            lower_mean = np.mean(s(s.x))

        over_idx = np.where(xs > upper_border)[0]
        ys[over_idx] = upper_mean

        under_idx = np.where(xs < lower_border)[0]
        ys[over_idx] = lower_mean

        df.at[idx, 'BMI'] = ys

        outliers = []
        if len(over_idx)+len(under_idx) > 0:
            outliers = list(over_idx) + list(under_idx)

    return df, outliers


def plot_BMI(s, x, y, x_wav, outliers):
    xs = np.linspace(x[0], x[-1], 1000)
    ys = s(xs)

    ys_wav = s(x_wav)

    plt.plot(x, y, '.-')
    plt.plot(xs, ys, '-')
    plt.plot(x_wav, ys_wav, '.')

    plt.plot(x_wav[outliers], ys_wav[outliers], '.')

    plt.show()



if __name__ == '__main__':
    print('Parsing txt reports:')
    getfiles()
    print()
    print('Estimating BMI values:')
    produce_file_list()
    print()
    print_cutflow(cutflow_list, cutflow_dict)


