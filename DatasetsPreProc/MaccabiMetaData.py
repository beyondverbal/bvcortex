import pandas as pd
from DatasetsPreProc.DBwrapper import DBwrapper

db = DBwrapper('MKM')

date_suffix = '181219.csv'
root_path = 'E:/Algo/dat/MetaData/'

encoding = 'utf-8-sig'

query_on = 'all' # 'labeled' # 'paper_chf' #

if query_on == 'all':
    patient_source = []
    print('*** querries on all patients ***')
elif query_on == 'paper_chf':
    patient_source = pd.read_csv('E:/Algo/DataExperiments/vtlt23/all/M8_181004pPaper2.3/all_chf_info.csv')
    print('*** querries on chf patients ***')
elif query_on == 'labeled':
    patient_source = pd.read_csv('E:/Algo/dat/MetaData/RecordingsData181128.csv')
    print('*** querries on labled patients ***')


if len(patient_source)==0:
    WHERE_PATIENTS = ''
    AND_PATIENTS = ''
else:
    patient_list = patient_source.patient_id.values
    patient_list_str = ['\'' + str(pat) + '\'' for pat in patient_list]
    qry_patients = ', '.join(patient_list_str)
    WHERE_PATIENTS = ' WHERE customer_seq in (%s)' % qry_patients
    AND_PATIENTS = ' AND customer_seq in (%s)' % qry_patients



def Demographic():
    QRY = (
    'SELECT customer_seq, CUSTOMER_BIRTH_Year, CUSTOMER_SEX_CODE, STATUSCUSTOMER, CUSTOMER_STATUS_DATE, DATE_DEATH, ' +
    'CUSTOMER_DISTRICT_CODE, CUSTOMER_DISTRICT_DESC, CARDIO_YN_CD, DATEFROMCARDIO, CARDIO_CHF_CD, DATE_FROM_CHF, ' +
    'CARDIO_PIRPUR_CD, DATEFROMCARDIPIRPUR, CARDIO_IHD, DATEFROMCARDIHD, CARDIO_MI, DATEFROMCARDMI, CVD_CD, ' +
    'DATEFROMCVD, CVA_CD, DATEFROMCVA, TIA_CD, DATEFROMTIA, DATETOTIA, NONCVA_CD, DATEFROMNONCVA, PVD_CD, ' +
    'DATEFROMPVD, DIAB_YN_CD, DATEFROMDAIBETIC, DIAB_TYPE, BLOODPRESURE_CD, DATEFROMBLOODPRESURE, CANCER_REG_CD, ' +
    'DATEFROMCANCER, BREAST_CANCER_CD, BREAST_CANCER_FROM_DATE, COLON_CANCER_CD, DATE_FROM_COLON_CANCER, COUMADIN_CD, ' +
    'DATEFROMCOUMADIN, DIALIZACD, DATEFROMDIALIZA, CKD_CD, DATE_FROM_CKD, CKD_STAGE, COPD_CD, DATE_FROM_COPD' +
    ' FROM BeyondVerbal.dbo.[MOMA_Demographic_Data&Rashamim_2018_09]' +
    WHERE_PATIENTS)
    df = db.Query2DF(QRY)
    df.to_csv(root_path + 'Demographic_' + date_suffix, index=False, encoding=encoding)

def hosp_april():
    QRY = (
    'SELECT customer_seq, ISHPUZ_ENTRANCE_DATE, ISHPUZ_RELEASE_DATE, ISHPUZ_DAYS_CALCULATED, DPRT_ENTRANCE_DATE,' +
    ' DPRT_RELEASE_DATE, DPRT_DAYS_CALCULATED, CARE_GIVER_TYPE_CD, CARE_GIVER_TYPE_DESC, CARE_GIVER_CD, CARE_GIVER_DESC,' +
    ' CARE_GIVER_DPRT_TYPE_CD, CARE_GIVER_DPRT_TYPE_DESC, CARE_GIVER_DPRT_CD, CARE_GIVER_DPRT_DESC, ISHPUZ_TYPE_CD' +
    ' FROM BeyondVerbal.dbo.MOMA_HOSPITAL_150000' +
    WHERE_PATIENTS)
    df = db.Query2DF(QRY)
    df.to_csv(root_path + 'HOSPITAL_150000_' + date_suffix, index = False, encoding=encoding)


def hosp_september():
    QRY = (
    'SELECT customer_seq AS patient_id, ISHPUZ_ENTRANCE_DATE, ISHPUZ_RELEASE_DATE, ISHPUZ_DAYS_CALCULATED, DPRT_ENTRANCE_DATE,' +
    ' DPRT_RELEASE_DATE, DPRT_DAYS_CALCULATED, CARE_GIVER_TYPE_CD, CARE_GIVER_TYPE_DESC, CARE_GIVER_CD, CARE_GIVER_DESC,' +
    ' CARE_GIVER_DPRT_TYPE_CD, CARE_GIVER_DPRT_TYPE_DESC, CARE_GIVER_DPRT_CD, CARE_GIVER_DPRT_DESC, ISHPUZ_TYPE_CD' +
    ' FROM BeyondVerbal.dbo.MOMA_HOSPITAL_2018_09' +
    WHERE_PATIENTS)
    df = db.Query2DF(QRY)
    df.to_csv(root_path + 'HOSPITAL_2018_09_' + date_suffix, index=False, encoding=encoding)

def asthma():
    CODES = ['Y20410', 'Y24080', 'Y14710', 'Y14711', 'Y14712', 'Y14713', 'Y14714', 'Y14716', 'Y21927', 'Y21972', 'Y21973',
         'Y23235', 'Y14712', '493', '493.0', '493.01', '493.02', '493.1', '493.10', '493.11', '493.12', '493.2', '493.20',
         '493.21', '493.22', '493.8', '493.81', '493.82', '493.90', '493.91', '493.92']
    CODES_str = ['\'' + code + '\'' for code in CODES]
    code_list =  ', '.join(CODES_str)
    QRY = (
    'SELECT customer_seq, DIAGNOSIS_CODE, DATE_DIAGNOSIS, PROF_CD' +
    ' FROM BeyondVerbal.dbo.MOMA_ALL_Diagnoses_2018_09' +
    ' WHERE DIAGNOSIS_CODE IN (%s)' % code_list +
    AND_PATIENTS)
    df = db.Query2DF(QRY)
    df.to_csv(root_path + 'Asthma_' + date_suffix, index=False, encoding=encoding)

def explicit_tables(info_dict):
    for key in info_dict.keys():
        QRY = (
            'SELECT ' + ', '.join(info_dict[key]['columns']) +
            ' FROM BeyondVerbal.dbo.' + info_dict[key]['table'] +
            WHERE_PATIENTS)
        df = db.Query2DF(QRY)
        df.to_csv(root_path + key + '_' + date_suffix, index=False, encoding=encoding)

def lab_results(lab_codes):
    for key in lab_codes.keys():
        QRY = (
            'SELECT customer_seq, LAB_TEST_CD, SAMPLE_DATE_ISO, LAB_RESULT' +
            ' FROM BeyondVerbal.dbo.MOMA_LABS_2018_09' +
            ' WHERE LAB_TEST_CD in (%s)' % lab_codes[key] +
            AND_PATIENTS)
        df = db.Query2DF(QRY)
        df.to_csv(root_path + key + '_' + date_suffix, index=False, encoding=encoding)



if __name__ == '__main__':

    # Demographic()
    # hosp_april()
    # hosp_september()
    # asthma()
    #
    # info_dict = {'BMI':         {'table': 'MOMA_BMI',          'columns': ['customer_seq', 'Date_Medida_Refuit', 'BMI']},
    #          'hemog':       {'table': 'MOMA_HEMOGLOBIN',       'columns': ['customer_seq', 'SAMPLE_DATE_ISO', 'LAB_RESULT']},
    #          'LDL':         {'table': 'MOMA_LDL',          'columns': ['customer_seq', 'SAMPLE_DATE_ISO', 'LAB_RESULT']},
    #          'Lung_cancer':     {'table': 'MOMA_Lang_CANCER',      'columns': ['customer_seq', 'DISEASE_GROUP_CODE', 'DISEASE_CODE', 'column_4']},
    #          'BP':          {'table': 'MOMA_BP_2018_09',       'columns': ['customer_seq', 'Date_Medida_Refuit', 'Value1', 'Value2']},
    #          'cardio_drugs':    {'table': 'MOMA_Cardio_Drugs_Use', 'columns': ['customer_seq', 'BetaBlockers', 'ACI', 'Diuretic', 'Spironolactone', 'Statins']}
    #          }
    info_dict = {'BMI': {'table': 'MOMA_BMI', 'columns': ['customer_seq', 'Date_Medida_Refuit', 'BMI']}}

    explicit_tables(info_dict)
    #
    # lab_codes = {'cholesterol': '2465', 'creatinine': '2565', 'eGFR': '25651', 'glucose': '2947', 'trig': '4478'}
    # lab_results(lab_codes)
    #
