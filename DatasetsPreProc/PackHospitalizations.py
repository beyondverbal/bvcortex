import pandas as pd
import numpy as np
from datetime import date
from datetime import timedelta
from MaccabiExperiments import ConfigGrid as GridCfg
import datetime
EXP_DATE = GridCfg.EXPERIMENT_DATE

MD0_FILE = 'E:/Algo/dat/MetaData/PatientsMD181003.csv'
MD1_FILE = 'E:/Algo/dat/MetaData/RecordingsData181003.csv'
HOSP_FILE = 'E:/Algo/dat/MetaData/HOSPITAL_2018_09_181121.csv'

OUT_FILE = 'E:/Algo/dat/MetaData/PackedHosp190115.csv'


def LoadMetaData():
    md0 = pd.read_csv(MD0_FILE, index_col=None, encoding='utf-8')
    md1 = pd.read_csv(MD1_FILE, index_col=None)
    md_df = pd.merge(left=md0, right=md1, on='patient_id')

    return md_df


def ConvetDateFormat(dates_from_csv):
    yf = (dates_from_csv / 1e4)
    y = yf.astype(int)
    mf = (yf-y)*1e2
    m = mf.astype(int)
    df = (mf-m)*1e2+0.1
    d = df.astype(int)
    date_list = [date(y[i], m[i], d[i]) for i in range(len(y))]
    return date_list

def LoadHospData():
    hosp_df = pd.read_csv(HOSP_FILE, index_col=None, encoding='utf-8')
    hosp_dates_in = ConvetDateFormat(hosp_df['ISHPUZ_ENTRANCE_DATE'].values)
    hosp_dates_out = ConvetDateFormat(hosp_df['ISHPUZ_RELEASE_DATE'].values)
    hosp_df = hosp_df.assign(hosp_dates_in=hosp_dates_in)
    hosp_df = hosp_df.assign(hosp_dates_out=hosp_dates_out)

    return hosp_df


def compare_dates(cur_date, exp_date=GridCfg.EXPERIMENT_DATE):
    cur_date2 = cur_date[:cur_date.find(' ')]
    cur_dt = datetime.datetime(int(cur_date2[:4]), int(cur_date2[5:7]), int(cur_date2[8:10]))
    exp_dt = datetime.datetime(int(exp_date[:4]), int(exp_date[6:7]), int(exp_date[9:10]))
    diff_date_format = exp_dt-cur_dt
    return diff_date_format.days


def AssignHosp2Samples(md_df, hosp_df):
    len = md_df.shape[0]
    hosp_in_30d = np.zeros(len)
    hosp_in_90d = np.zeros(len)
    hosp_in_180d = np.zeros(len)
    hosp_in_360d = np.zeros(len)
    duration2death = np.zeros(len)
    duration2hosp = np.zeros(len)

    for index, row in md_df.iterrows():
        if index % 1000 == 0:
            print(index)
        f = row['filename']
        p = row['patient_id']
        rec_date = pd.to_datetime(row.rec_date).date()
        hosp_records = hosp_df[(hosp_df['hosp_dates_out'].values >= rec_date) & (hosp_df['hosp_dates_in'].values <= rec_date + timedelta(days=360)) & (hosp_df['patient_id'] == p)]
        hosp_days_from_rec = np.zeros(400)
        for h_i, h_r in hosp_records.iterrows():
            day_in = max((h_r['hosp_dates_in'] - rec_date).days, 0)
            day_out = min((h_r['hosp_dates_out'] - rec_date).days, 360)
            hosp_days_from_rec[day_in:day_out+1] = 1

        if row.DATE_DEATH == '1900-12-31':
            duration2death[index] = -7
            days2death = 10000
        else:
            duration2death[index] = (pd.to_datetime(row.DATE_DEATH).date() - rec_date).days
            days2death = duration2death[index]

            if duration2death[index] < hosp_days_from_rec.shape[0]:
                # hosp_days_from_rec[int(duration2death[index]):-1] = 1
                if duration2death[index] < 0:
                    hosp_in_30d[index] = np.nan
                    hosp_in_90d[index] = np.nan
                    hosp_in_180d[index] = np.nan
                    hosp_in_360d[index] = np.nan
                    continue
                else:
                    hosp_days_from_rec[int(duration2death[index])] = 1
                    # days2death = duration2death[index]

        days_from_rec_to_exp = compare_dates(row.rec_date, exp_date=EXP_DATE)

        # patch
        days2death = 10000

        if days_from_rec_to_exp >= 30 and days2death >= 30:
            hosp_in_30d[index] = np.sum(hosp_days_from_rec[:30])
        else:
            # print(days_from_rec_to_exp)
            hosp_in_30d[index] = np.nan

        if days_from_rec_to_exp >= 90 and days2death >= 90:
            hosp_in_90d[index] = np.sum(hosp_days_from_rec[:90])
        else:
            # print(days_from_rec_to_exp)
            hosp_in_90d[index] = np.nan

        if days_from_rec_to_exp >= 180 and days2death >= 180:
            hosp_in_180d[index] = np.sum(hosp_days_from_rec[:180])
        else:
            # print(days_from_rec_to_exp)
            hosp_in_180d[index] = np.nan

        if days_from_rec_to_exp >= 360 and days2death >= 360:
            hosp_in_360d[index] = np.sum(hosp_days_from_rec[:360])
        else:
            # print(days_from_rec_to_exp)
            hosp_in_360d[index] = np.nan

        d2h = np.nonzero(hosp_days_from_rec)[0]
        if d2h.shape[0] > 0:
            duration2hosp[index] = d2h[0]
        else:
            duration2hosp[index] = -7

        bb=0

    return pd.DataFrame(np.hstack((np.expand_dims(md_df.filename, axis=1),
                            np.expand_dims(md_df.patient_id, axis=1),
                            np.expand_dims(md_df.rec_date, axis=1),
                            np.expand_dims(hosp_in_30d, axis=1),
                            np.expand_dims(hosp_in_90d, axis=1),
                            np.expand_dims(hosp_in_180d, axis=1),
                            np.expand_dims(hosp_in_360d, axis=1),
                            np.expand_dims(duration2hosp, axis=1),
                            np.expand_dims(duration2death, axis=1))),
                 columns = ['filename', 'patient_id', 'rec_date', 'hosp_in_30d', 'hosp_in_90d', 'hosp_in_180d', 'hosp_in_360d', 'duration2hosp', 'duration2death'])


if __name__ == '__main__':
    md_df = LoadMetaData()
    hosp_df = LoadHospData()

    rec_hosps = AssignHosp2Samples(md_df, hosp_df)

    rec_hosps.to_csv(OUT_FILE)
