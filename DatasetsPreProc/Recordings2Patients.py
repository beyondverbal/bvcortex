import pandas as pd
import numpy as np
import time
import os
import sys
sys.path.append("../.")
from scripts.FeatureExtractionScripts import utils

from DatasetsPreProc.DBwrapper import DBwrapper

BYPASS_SQL = True # False #     # TODO: fix weird bug - when running in False, there is a bug when checking mismatched filenames. Running again in True (load csv), works fine
BYPASS_FILE_LIST = True # False #

mkm_db = DBwrapper('MKM')
thor_db = DBwrapper('THOR_MACCABI4')

date_suffix = '190122.csv'
root_path = 'E:/Algo/dat/R2P/'

encoding = 'utf-8-sig'


def mkm_r2p():
    start_time = time.time()
    csv_file = root_path + 'MKM_R2P' + date_suffix
    if BYPASS_SQL:
        df = pd.read_csv(csv_file)
    else:
        QRY = ('SELECT fshortname AS filename, Nurce_Code AS nurse_id, Starttime AS rec_date, incoming, L1.MomaId AS moma_id, customer_seq AS patient_id' +
               ' FROM MOMA_META_DATA_11_2018' +
               ' LEFT OUTER JOIN (' +
               ' SELECT DISTINCT MomaId, customer_seq' +
               ' FROM MOMA_Seq_Domein_12_2018) AS L1' +
               ' ON L1.MomaId = MOMA_META_DATA_11_2018.MomaId')

        df = mkm_db.Query2DF(QRY)

        df.rec_date = df.rec_date.str.slice(start=0, stop=19)
        df.rec_date = pd.to_datetime(df.rec_date)

        dup_df = df[df.duplicated(subset='filename', keep=False)]
        dup_df.to_csv(root_path + 'MKM_DUPR' + date_suffix, index=False, encoding=encoding)
        df.drop_duplicates(subset='filename', keep=False, inplace=True)

        df.to_csv(csv_file, index=False, encoding=encoding)

    print('mkm_df: shape {} - run {} minutes.'.format(df.shape,np.round((time.time( ) - start_time) / float(60), decimals=1)))
    return df

def thor_r2p():
    start_time = time.time()
    csv_file = root_path + 'THOR_R2P' + date_suffix
    if BYPASS_SQL:
        df = pd.read_csv(csv_file)
    else:
        QRY = ("SELECT CONCAT(CAST(filename AS CHAR),'.wav') AS filename, path, MID(SUBSTRING_INDEX(path, '\\\\', 2),2,20) AS p, Labeled, incomming" +
               " FROM recordings" +
               " LEFT OUTER JOIN (SELECT DISTINCT rec_id, 1 AS Labeled FROM timesegments WHERE confidence = 1) AS T1 ON recordings.id = T1.rec_id")
        df = thor_db.Query2DF(QRY)

        df.sort_values(['filename', 'Labeled'], ascending=[True, True], inplace=True)
        dup_df = df[df.duplicated(subset='filename', keep='first')]
        dup_df.to_csv(root_path + 'THOR_DUPR' + date_suffix, index=False, encoding=encoding)
        df.drop_duplicates(subset='filename', keep='first', inplace=True)

        df.to_csv(csv_file, index=False, encoding=encoding)

    print('thor_df: shape {} - run {} minutes.'.format(df.shape,np.round((time.time( ) - start_time) / float(60), decimals=1)))
    return df

def recording_paths():
    csv_file = root_path + 'RECORDING_FILES' + date_suffix
    if BYPASS_FILE_LIST:
        paths = pd.read_csv(csv_file)
    else:
        print('In recording_paths():')
        progress_c = 0
        dir_num = len(os.listdir('E:\\Recordings\\by_p'))
        filelist = []
        for dirpath, dirnames, filenames in os.walk('E:\\Recordings\\by_p'):
            progress_c += 1
            utils.progress_bar(progress_c, dir_num)
            for filename in [f for f in filenames if f.endswith('.wav')]:
                filelist.append(os.path.join(dirpath, filename))
        paths = pd.DataFrame(filelist, columns=['path'])
        paths['filename'] = [paths.path[i].split('\\')[-1:][0] for i in range(paths.shape[0])]
        paths['path_hd'] = ['\\' + paths.path.values[i].split('\\')[-2] + '\\' + paths.path.values[i].split('\\')[-1] for i in range(paths.shape[0])]

        paths.sort_values(['filename', 'path_hd'], ascending=[True, True], inplace=True)
        dup_df = paths[paths.duplicated(subset='filename', keep='first')].copy()
        dup_df.to_csv(root_path + 'DISK_DUPE' + date_suffix, index=False, encoding=encoding)
        paths.drop_duplicates(subset='filename', keep='first', inplace=True)

        # paths.to_csv('E:\\Recordings\\files_new.csv')
        paths.to_csv(csv_file, index = False, encoding = encoding)

    return paths

if __name__ == '__main__':

    mkm_df = mkm_r2p()
    bb=0

    print(len(np.unique(mkm_df['patient_id'].values)))

    thor_df = thor_r2p()

    paths = recording_paths()

    r2p_df = pd.merge(left=mkm_df, right=thor_df, on='filename', how='left')
    r2p_df.to_csv(root_path + 'R2P_MRGD' + date_suffix, index=False, encoding=encoding)

    r2p_df2 = pd.merge(left=r2p_df, right=paths, on='filename', how='left')
    r2p_df2.to_csv(root_path + 'R2P_MRGDex' + date_suffix, index=False, encoding=encoding)

    a = r2p_df2[r2p_df2.path_x != r2p_df2.path_hd]
    a.to_csv(root_path + 'R2P_mismatch' + date_suffix, index=False, encoding=encoding)

    a_thor = r2p_df2[(r2p_df2.path_x != r2p_df2.path_hd) & (r2p_df2.path_y == r2p_df2.path_y)]
    a_thor.to_csv(root_path + 'R2P_thor_mismatch' + date_suffix, index=False, encoding=encoding)

    thor_df['path_file'] = [thor_df.path.values[i].split('\\')[-1:][0] for i in range(thor_df.shape[0])]
    thor_df['path_patient'] = [int(thor_df.path.values[i].split('\\')[-2:][0]) for i in range(thor_df.shape[0])]
    b = thor_df[(thor_df.filename != thor_df.path_file) | (thor_df.p != thor_df.path_patient)].copy()
    filename_val = b.filename.values
    trunc_names = [filename_val[i][:-4] for i in range(len(filename_val))]
    b.filename = trunc_names
    b.to_csv(root_path + 'thor_mismatch' + date_suffix, index=False, encoding=encoding)

    c = r2p_df2[(r2p_df2.path_x != r2p_df2.path_x) & (r2p_df2.path_hd == r2p_df2.path_hd)].copy()
    filename_val = c.filename.values
    trunc_names = [filename_val[i][:-4] for i in range(len(filename_val))]
    c.filename = trunc_names
    c.to_csv(root_path + 'thor_missing_files' + date_suffix, index=False, encoding=encoding)

    d = r2p_df2[(r2p_df2.incoming != r2p_df2.incomming) & (r2p_df2.path_x == r2p_df2.path_hd)]
    d.to_csv(root_path + 'incoming_mismatch' + date_suffix, index=False, encoding=encoding)

    bb=0

