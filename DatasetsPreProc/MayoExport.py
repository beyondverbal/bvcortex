import pandas as pd

from ThorDBwrapper import DBwrapper


def get_mayo_data():
    db = DBwrapper()
    QRY = ('SELECT DISTINCT recordings.filename, path, patients.name AS patient, rec_date, seg_start, seg_end,' +
           ' languages.name AS language, legend AS label, rejects.name AS reject_state, is_completed, comment' +
           ' FROM recordings' +
           ' INNER JOIN patients2recordings ON recordings.id = patients2recordings.rec_id' +
           ' INNER JOIN patients ON patients.id = patients2recordings.patient_id' +
           ' LEFT OUTER JOIN timesegments ON recordings.id = timesegments.rec_id' +
           ' LEFT OUTER JOIN labels ON labels.id = timesegments.label_id' +
           ' LEFT OUTER JOIN languages ON languages.id = recordings.language_id' +
           ' LEFT OUTER JOIN rejects ON rejects.id = recordings.reject_id' +
           ' WHERE patients.name LIKE "mayo_%"')
    df = db.Query2DF(QRY)
    return df


if __name__ == '__main__':
    DO_QA = False
    root = 'C:/BVC/HealthAnalysis/Mayo/MetaData/'
    date_suffix = '181205.csv'

    df1 = get_mayo_data()
    df1.to_csv(root + 'MayoMD' + date_suffix, index = False)

    if DO_QA:
        df2 = pd.read_csv(root + 'mayo_full_171227.csv')
        df2.rename(index=str, columns={'name': 'filename'}, inplace=True)
        df2 = pd.merge(left=df1, right=df2, on='filename')
        df2.to_csv(root + 'MayoMDex' + date_suffix, index = False)

