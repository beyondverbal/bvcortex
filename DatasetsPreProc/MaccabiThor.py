from DatasetsPreProc.DBwrapper import DBwrapper

db = DBwrapper('THOR_MACCABI4')


def get_labeled_segments_all():
    COLUMNS = ['filename', 'nurse_id', 'patient_id', 'seg_start', 'seg_end', 'legend', 'confidence']
    QRY = ('SELECT DISTINCT ' + ', '.join(COLUMNS) +
           ' FROM recordings' +
           ' INNER JOIN patients2recordings ON recordings.id = patients2recordings.rec_id' +
           ' INNER JOIN timesegments ON recordings.id = timesegments.rec_id' +
           ' INNER JOIN labels ON timesegments.label_id = labels.id'
           ' WHERE confidence = 1')
    df = db.Query2DF(QRY)
    df.drop_duplicates(subset = ['filename', 'seg_start', 'seg_end'], keep = False, inplace = True)
    return df

def get_all_segments():
    COLUMNS = ['filename', 'nurse_id', 'patient_id', 'seg_start', 'seg_end', 'confidence', 'legend']
    QRY = ('SELECT DISTINCT ' + ', '.join(COLUMNS) +
           ' FROM recordings' +
           ' INNER JOIN patients2recordings ON recordings.id = patients2recordings.rec_id' +
           ' INNER JOIN timesegments ON recordings.id = timesegments.rec_id' +
           ' INNER JOIN labels ON timesegments.label_id = labels.id')
    df = db.Query2DF(QRY)
    df.drop_duplicates(subset = ['filename', 'seg_start', 'seg_end'], keep = False, inplace = True)
    return df

def get_file_patient_nurse():
    COLUMNS = ['filename', 'patient_id', 'nurse_id', 'path', 'rec_date']
    QRY = ('SELECT DISTINCT ' + ', '.join(COLUMNS) +
           ' FROM recordings INNER JOIN patients2recordings ON recordings.id = patients2recordings.rec_id')
    df = db.Query2DF(QRY)
    df.drop_duplicates(subset='path', keep=False, inplace=True)
    return df

def get_all_segments_from_filelist(file_list):
    file_list = ', '.join(file_list)
    COLUMNS = ['filename', 'nurse_id', 'patient_id', 'seg_start', 'seg_end', 'confidence', 'legend']
    QRY = ('SELECT DISTINCT ' + ', '.join(COLUMNS) +
           ' FROM recordings' +
           ' INNER JOIN patients2recordings ON recordings.id = patients2recordings.rec_id' +
           ' INNER JOIN timesegments ON recordings.id = timesegments.rec_id' +
           ' INNER JOIN labels ON timesegments.label_id = labels.id' +
           ' WHERE filename IN (%s)' % file_list +
           ' AND confidence >= 0 AND legend = "p"')
    df = db.Query2DF(QRY)
    df.drop_duplicates(subset=['filename', 'seg_start', 'seg_end'], keep=False, inplace=True)
    return df


def get_labeled_recordings():
    QRY = ('SELECT DISTINCT filename, path AS full_path, rec_date, nurse_id, incomming, patient_id, name AS language' +
           ' FROM recordings' +
           ' INNER JOIN patients2recordings ON recordings.id = patients2recordings.rec_id' +
           ' INNER JOIN timesegments ON recordings.id = timesegments.rec_id' +
           ' LEFT OUTER JOIN languages ON languages.id = recordings.language_id' +
           ' WHERE confidence = 1')
    df = db.Query2DF(QRY)
    # Note, we should not delete anything
    df.drop_duplicates(subset = 'filename', keep = False, inplace = True)
    return df

def get_labeled_segments():
    QRY = ('SELECT DISTINCT filename, patient_id, seg_start, seg_end' +
           ' FROM recordings' +
           ' INNER JOIN patients2recordings ON recordings.id = patients2recordings.rec_id' +
           ' INNER JOIN timesegments ON recordings.id = timesegments.rec_id' +
           ' INNER JOIN labels ON timesegments.label_id = labels.id'
           ' WHERE confidence = 1 AND legend = "p"')
    df = db.Query2DF(QRY)
    return df


if __name__ == '__main__':
    date_suffix = '190127.csv'

    # df1 = get_labeled_recordings()
    # df1.to_csv('E:/Algo/dat/MetaData/DB4Recordings' + date_suffix, index = False)

    df2 = get_labeled_segments()
    df2.to_csv('E:/Algo/dat/MetaData/DB4Segments' + date_suffix, index = False)

    # df3 = get_all_segments()
    # df3.to_csv('E:/Algo/dat/MetaData/AllSegments' + date_suffix, index = False)

    # df4 = get_file_patient_nurse()
    # df4.to_csv('E:/Algo/dat/MetaData/FileNursePatient' + date_suffix, index = False)

    # df5 = get_labeled_segments_all()
    # df5.to_csv('E:/Algo/dat/MetaData/SegmentsDataAll' + date_suffix, index=False)

    # OSA_info = pd.read_csv('E:/Algo/dat/MetaData/OSA_labeled_files.csv')
    # file_list = OSA_info.filename.values.astype(str)
    # df6 = get_all_segments_from_filelist(file_list)
    # df6.to_csv('E:/Algo/dat/MetaData/SegmentsDataOSA' + date_suffix, index=False)
