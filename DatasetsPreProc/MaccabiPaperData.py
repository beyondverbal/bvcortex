import pyodbc
import pandas as pd
import numpy as np
import datetime

server = r'MKM-RESEARCH-DB\MKMSRV'
database = 'BeyondVerbal'
username = 'USERNAME'
password = 'PASSWORD'
cnxn = pyodbc.connect('DRIVER={SQL Server Native Client 11.0};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password +  '; Trusted_Connection=Yes')
cursor = cnxn.cursor()

# basic registry data
COLUMNS = ['patient_id', 'CUSTOMER_BIRTH_Year', 'CUSTOMER_SEX_CODE', 'DATE_DEATH', 'CARDIO_YN_CD', 'CARDIO_CHF_CD',
           'CARDIO_PIRPUR_CD', 'CARDIO_IHD', 'CARDIO_MI', 'CVD_CD', 'CVA_CD', 'TIA_CD', 'NONCVA_CD', 'PVD_CD',
           'DIAB_YN_CD', 'BLOODPRESURE_CD', 'CANCER_REG_CD', 'CKD_CD', 'CKD_STAGE', 'COPD_CD']
query = 'SELECT customer_seq AS ' + ', '.join(COLUMNS) + ' ' + \
        'FROM [MOMA_Demographic_Data&Rashamim_2018_09] ' + \
        'WHERE customer_seq IS NOT NULL'
cursor.execute(query)
rows = cursor.fetchall()
df0 = pd.DataFrame(data=np.matrix(rows), columns=COLUMNS)
df0['DATE_DEATH'].replace(to_replace=datetime.datetime(9999, 12, 31, 0, 0), value=datetime.datetime(1900, 12, 31, 0, 0), inplace=True)  # year 9999 causes out-of-bounds in nanosecond units
df0.rename(columns={'CUSTOMER_SEX_CODE': 'patient_gender'}, inplace=True)
df0['patient_gender'].replace(to_replace='ז', value='M', inplace=True)
df0['patient_gender'].replace(to_replace='נ', value='F', inplace=True)

# blood pressure data
query = "SELECT customer_seq, AVG(CAST(Value1 AS float)), STDEV(CAST(Value1 AS float)), AVG(CAST(Value2 AS float)), STDEV(CAST(Value2 AS float)) " + \
        "FROM MOMA_BP_2018_09 " + \
        "WHERE Date_Medida_Refuit > '2012' " + \
        "GROUP BY customer_seq"
cursor.execute(query)
rows = cursor.fetchall()
df1 = pd.DataFrame(data=np.matrix(rows), columns=['patient_id', 'BP_HI', 'BP_HI_std', 'BP_LOW', 'BP_LOW_std'])

# BMI data
query = "SELECT customer_seq, AVG(CAST(BMI AS float)) " + \
        "FROM MOMA_BMI " + \
        "WHERE Date_Medida_Refuit > '2012' " + \
        "GROUP BY customer_seq"
cursor.execute(query)
rows = cursor.fetchall()
df2 = pd.DataFrame(data=np.matrix(rows), columns=['patient_id', 'BMI'])

# merge
df = pd.merge(left=df0, right=df1, how='left', on='patient_id')
df = pd.merge(left=df, right=df2, how='left', on='patient_id')

# # OSA data original Q from ALL_MOMA_Diagnoses
# query = "SELECT DISTINCT customer_seq " + \
#         "FROM MOMA_All_Diagnoses_2018_09 " + \
#         "WHERE DIAGNOSIS_CODE IN ('780.51', 'Y18404', 'Y18405', 'Y18409', '780.53', '780.57', 'Y18400', '327.2', '327.20', '327.23', '327.29')"
# cursor.execute(query)
# rows = cursor.fetchall()
# OSAdiagnosed = pd.DataFrame(data=np.matrix(rows), columns=['patient_id'])
# Went2SleepLab = pd.DataFrame.from_csv('E:/Algo/dat/MetaData/PatientsWent2SleepLab.csv', index_col=None, encoding='utf-8')
# df = df.assign(OSA=-7)
# idx0 = df.patient_id.isin(Went2SleepLab.patient_id.values)
# df.loc[idx0,'OSA'] = 0
# idx1 = (df.patient_id.isin(OSAdiagnosed.patient_id.values)) & (df.OSA == 0)
# df.loc[idx1,'OSA'] = 1

# Asthma data
query = "SELECT DISTINCT customer_seq " + \
        "FROM MOMA_All_Diagnoses_2018_09 " + \
        "WHERE DIAGNOSIS_CODE LIKE '493%'"
cursor.execute(query)
rows = cursor.fetchall()
AsthmaDiagnosed = pd.DataFrame(data=np.matrix(rows), columns=['patient_id'])
df = df.assign(Asthma=0)
idx0 = df.patient_id.isin(AsthmaDiagnosed.patient_id.values)
df.loc[idx0,'Asthma'] = 1

# Bronchitis data
query = "SELECT DISTINCT customer_seq " + \
        "FROM MOMA_All_Diagnoses_2018_09 " + \
        "WHERE DIAGNOSIS_CODE LIKE '491%'"
cursor.execute(query)
rows = cursor.fetchall()
BronchitisDiagnosed = pd.DataFrame(data=np.matrix(rows), columns=['patient_id'])
df = df.assign(Bronchitis=0)
idx0 = df.patient_id.isin(BronchitisDiagnosed.patient_id.values)
df.loc[idx0,'Bronchitis'] = 1

# export
date_suffix = '190120.csv'
df.to_csv('E:/Algo/dat/MetaData/PatientsMD' + date_suffix, index = False, encoding='utf-8')
cursor.close()
