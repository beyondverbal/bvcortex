function varargout = stats_utils_lib(varargin)

f_handles = struct(... 
        'ab_pdf_classifier_score', @ab_pdf_classifier_score,...
        'ab_pdf_flows_score', @ab_pdf_flows_score,...
        'entropy_calc_on_vec', @entropy_calc_on_vec,...        
        'entropy_ratio_flow_score', @entropy_ratio_flow_score,...  
        'var_ratio_flow_score', @var_ratio_flow_score,...  
        'gs_var_ratio_flow_score', @gs_var_ratio_flow_score,...          
        'postprocess_outliers_results_mat', @postprocess_outliers_results_mat,...        
        'ab_simple_flows_score', @ab_simple_flows_score,...
        'calc_edges_vec', @calc_edges_vec,...
        'collect_outliers_to_edges', @collect_outliers_to_edges,...
        'calc_outlier_range', @calc_outlier_range,...
        'remove_outliers', @remove_outliers,...
        'replace_outliers', @replace_outliers,...
        'vector_smoothing', @vector_smoothing,...
        'quick_iqr', @quick_iqr,...
        'gap_vector_smoothing', @gap_vector_smoothing,...
        'partial_sum_percentile_location_on_vec', @partial_sum_percentile_location_on_vec,...        
        'partial_sum_percentile_location_ratio_on_vec', @partial_sum_percentile_location_ratio_on_vec,...        
        'partial_sum_percentile_location_on_mat', @partial_sum_percentile_location_on_mat,...        
        'partial_sum_percentile_location_ratio_on_mat', @partial_sum_percentile_location_ratio_on_mat,...        
        'windowed_histogram_mat', @windowed_histogram_mat...        
         );
      
if nargin == 0
    varargout{1} = f_handles;
else
    if nargout>0
        varargout = {feval(eval(['@' varargin{1}]),varargin{2:end})};
    else
        feval(eval(['@' varargin{1}]),varargin{2:end});
    end
end

end

    % calculates single pdf classifier
    function [score_s, a_success_vec, b_success_vec, edges_success_vec] = ...
                                    ab_pdf_classifier_score(a_vec, b_vec,...
                                        apriori_a, apriori_b, num_edges, outlier_method, skip_nans)
        if ~exist('outlier_method','var')
            outlier_method = '';
        end
        
        if ~exist('skip_nans', 'var')
            % Assume there are no empty items (which are represented by NaN)
            skip_nans = false;
        end
        
        if isempty(a_vec) || isempty(b_vec)
            a_success_vec = [];
            if ~isempty(a_vec)
                a_success_vec = true(size(a_vec));
            end
            b_success_vec = [];
            if ~isempty(b_vec)
                b_success_vec = true(size(b_vec));
            end
            
            edges_success_vec = [];
        end
        
        if skip_nans
            % remove empty results from the calculation
            a_failed = isnan(a_vec);
            b_failed = isnan(b_vec);
            a_vec(a_failed) = [];
            b_vec(b_failed) = [];
            a_success_vec = ~a_failed;
            b_success_vec = ~b_failed;
            
            edges_success_vec = [];
        end

        if isempty(a_vec) || isempty(b_vec)
            score_s.success_rate = 0;
            score_s.a_err_rate = double(isempty(a_vec));
            score_s.b_err_rate = double(isempty(b_vec));
            score_s.score = 0;
            score_s.classifier = 0;
            return
        end    
                        
        score_s = [];
        a_vec = a_vec(:).';
        b_vec = b_vec(:).';
        
        %edges for histogram count
        edges_vec = calc_edges_vec([a_vec b_vec], num_edges, outlier_method);        
        a_vec = collect_outliers_to_edges(a_vec, edges_vec);
        b_vec = collect_outliers_to_edges(b_vec, edges_vec);

        % pdfs
        pdf_a = apriori_a.*histc(a_vec, edges_vec)./length(a_vec);
        pdf_b = apriori_b.*histc(b_vec, edges_vec)./length(b_vec);

        % range for calc
        mean_a_ind = round(sum((1:num_edges).*pdf_a)./(sum(pdf_a)+eps));
        mean_b_ind = round(sum((1:num_edges).*pdf_b)./(sum(pdf_b)+eps));
        
        % cumsums
        cs_a = cumsum(pdf_a);
        cs_b = cumsum(pdf_b);

        if mean_a_ind <= mean_b_ind
            edges_success_vec = abs(cs_a + cs_b(end) - cs_b);
        else
            edges_success_vec = abs(cs_b + cs_a(end) - cs_a);
        end
        [score_s.score, max_i]= max(edges_success_vec);

        score_s.classifier = edges_vec(max_i);
        score_s.success_rate = edges_success_vec(max_i);
        
        if cs_a(max_i) >= cs_b(max_i)            
            score_s.a_err_rate = (cs_a(end) - cs_a(max_i))./(cs_a(end)+eps);
            score_s.b_err_rate = cs_b(max_i)./(cs_b(end)+eps);
            a_success_vec = a_vec < score_s.classifier;
            b_success_vec = b_vec >= score_s.classifier;
        else
            score_s.a_err_rate = cs_a(max_i)./(cs_a(end)+eps);
            score_s.b_err_rate = (cs_b(end) - cs_b(max_i))./(cs_b(end)+eps);
            a_success_vec = a_vec > score_s.classifier;
            b_success_vec = b_vec <= score_s.classifier;
        end  
        
        % check that it's not in the epsilons
        if (edges_vec(end) - edges_vec(1)) < 0.0000001
            score_s.score = 0.5;
            score_s.success_rate = 0.5;
            score_s.a_err_rate = 0.5;
            score_s.b_err_rate = 0.5;
        end
        
        if skip_nans
            % Don't take failures into consideration
            v = zeros(1, numel(a_failed));
            v(~a_failed) = a_success_vec;
            a_success_vec = v;
            v = zeros(1, numel(b_failed));
            v(~b_failed) = b_success_vec;
            b_success_vec = v;
        end
        
    end
    
    % calculates simple score for the flows
    function flow_score = ab_simple_flows_score(a_res_mat, b_res_mat, skip_nans)
    
        if ~exist('skip_nans', 'var')
            % Assume there are no empty items (which are represented by NaN)
            skip_nans = false;
        end
    
        num_flows = size(a_res_mat,1);
        flow_score = zeros(1, num_flows);
        % calc simple score
        for flow_i = 1:num_flows
            a_flow_res = a_res_mat(flow_i,:);
            b_flow_res = b_res_mat(flow_i,:);
            
            if skip_nans
                a_flow_res(isnan(a_flow_res)) = [];
                b_flow_res(isnan(b_flow_res)) = [];
            end
            
            means_diff = abs(mean(a_flow_res) - mean(b_flow_res));
            sum_std = quick_iqr(a_flow_res) + quick_iqr(b_flow_res); 
            flow_score(flow_i) = means_diff / (sum_std + eps); 
        end
    end

    % calculates simple score for the flows
    function [flow_score, a_succ_mat, b_succ_mat] = ab_pdf_flows_score(a_res_mat, b_res_mat,...
                                    apriori_a, apriori_b, num_edges, outlier_method, skip_nans)
        if ~exist('outlier_method', 'var')
            outlier_method = '';
        end
        if ~exist('skip_nans', 'var')
            % Assume there are no empty items (which are represented by NaN)
            skip_nans = false;
        end
        
        num_flows = size(a_res_mat,1);
        [score, success_rate, classifier,a_err_rate,b_err_rate] = deal(zeros(1, num_flows));
        
        % calc pdf score
        parfor_progress = ParforProgMon('AB Score', num_flows);
        cleanupObj = onCleanup(@()parfor_progress.delete());
        a_succ_mat = false(size(a_res_mat));
        b_succ_mat = false(size(b_res_mat));
        
        parfor flow_i = 1:num_flows    
            
            a_vec = real(a_res_mat(flow_i,:));
            b_vec = real(b_res_mat(flow_i,:));
            
            [score_s, a_succ_vec, b_succ_vec] = ab_pdf_classifier_score(a_vec, b_vec,...
                                apriori_a, apriori_b, num_edges, outlier_method, skip_nans);

            % a, b classification success vectors
            if ~isempty(a_succ_vec)
                a_succ_mat(flow_i,:) = a_succ_vec;
            end
            if ~isempty(b_succ_vec)
                b_succ_mat(flow_i,:) = b_succ_vec;
            end
            % score details
            score(flow_i) = score_s.score;    
            success_rate(flow_i) = score_s.success_rate;
            a_err_rate(flow_i) = score_s.a_err_rate;
            b_err_rate(flow_i) = score_s.b_err_rate;
            classifier(flow_i) = score_s.classifier;
            
            parfor_progress.increment();
        end
        flow_score.pdf_score = score;
        flow_score.pdf_classifier = classifier;
        flow_score.success_rate = success_rate;
        flow_score.a_err_rate = a_err_rate;
        flow_score.b_err_rate = b_err_rate;
    end
    
    % calculates simple score for the flows
    function [entropy_tot, entropy_ab] = ...
        entropy_ratio_flow_score(res_mat_cell, a_ind, b_ind, num_edges, skip_nans)
    
        num_flows = size(res_mat_cell{1},1);
        num_groups = numel(res_mat_cell);
        num_files_tot = sum(cellfun(@(x)size(x,2), res_mat_cell));
        num_files_a = sum(cellfun(@(x)size(x,2), res_mat_cell(a_ind)));
        num_files_b = sum(cellfun(@(x)size(x,2), res_mat_cell(b_ind)));
        ab_ind = union(a_ind, b_ind);
        num_files_ab = sum(cellfun(@(x)size(x,2), res_mat_cell(ab_ind)));        
                
        all_res_mat = cat(2, res_mat_cell{:});
        
        entropy_tot = zeros(1, num_flows);
        entropy_ab = zeros(1, num_flows);
        
        % calc pdf score
        parfor_progress = ParforProgMon('Entropy Score', num_flows);
        cleanupObj = onCleanup(@()parfor_progress.delete());
        for flow_i = 1:num_flows
            parfor_progress.increment();

            %edges for histogram count
            edges_vec = calc_edges_vec(all_res_mat(flow_i,:), num_edges, 'iqr2'); 
            
            res_all_vec = all_res_mat(flow_i, :);
            res_all_vec = collect_outliers_to_edges(res_all_vec, edges_vec);
            
            % pre-alloc
            h_vec = zeros(1, num_groups);
            pdf_all = zeros(size(edges_vec));
            pdf_a = zeros(size(edges_vec));
            pdf_b = zeros(size(edges_vec));
            pdf_ab = zeros(size(edges_vec));
            
            cur_file = 1;
            for g_ind = 1:num_groups
                cur_files_ind = cur_file:(cur_file+size(res_mat_cell{g_ind},2)-1);
                
                res_vec = res_all_vec(cur_files_ind);
                
                pdf_vec = histc(res_vec, edges_vec)./length(res_vec);
                
                h_vec(g_ind) = length(res_vec).*entropy_calc_on_vec(pdf_vec);
                    
                pdf_all = pdf_all + length(res_vec).*pdf_vec;
                
                if any(g_ind == ab_ind)
                    pdf_ab = pdf_ab + length(res_vec).*pdf_vec;
                    if any(g_ind == a_ind)
                        pdf_a = pdf_a + length(res_vec).*pdf_vec;
                    else
                        pdf_b = pdf_b + length(res_vec).*pdf_vec;
                    end
                end
                
                cur_file = cur_file + size(res_mat_cell{g_ind},2);
            end
            
            % collective entropy
            h_all = length(res_all_vec).*entropy_calc_on_vec(pdf_all);
            
            % ab entropy
            h_a = num_files_a.*entropy_calc_on_vec(pdf_a);
            h_b = num_files_b.*entropy_calc_on_vec(pdf_b);
            h_ab = num_files_ab.*entropy_calc_on_vec(pdf_ab);
                
            % pdfs          
            entropy_tot(flow_i) = (h_all - sum(h_vec))/num_files_tot;
            entropy_ab(flow_i) = (h_ab - (h_a + h_b))/num_files_ab;
        end
    end
    
    % calculates simple score for the flows
    function [var_ratio_tot, var_ratio_ab, median_var_ratio] = ...
        var_ratio_flow_score(res_mat_cell, a_ind, b_ind, skip_nans)
    
        if ~exist('skip_nans', 'var')
            skip_nans = false;
        end    
    
        ab_ind = union(a_ind, b_ind);      
        
        if skip_nans
            var_fun = @nanvar;
            median_fun = @nanmedian;
        else
            var_fun = @var;
            median_fun = @median;
        end
        
%         tot_var_l1 = @(x)sumfun(abs(x-repmat(meanfun(x, 2), 1, size(x,2))), 2);
        tot_var_l2 = @(x)size(x,2).*var_fun(x, 0, 2);
        
        tot_var_vec = tot_var_l2(cat(2, res_mat_cell{:})); 
        ab_var_vec = tot_var_l2(cat(2, res_mat_cell{ab_ind}));  
        a_var_vec = tot_var_l2(cat(2, res_mat_cell{a_ind})); 
        b_var_vec = tot_var_l2(cat(2, res_mat_cell{b_ind})); 
        sum_group_var_vec = sum((cat(2,...
                     cell2mat(cellfun(tot_var_l2, res_mat_cell, 'uniformoutput', 0))...
                                            )).').';
        median_var_vec = tot_var_l2(cat(2,...
                     cell2mat(cellfun(@(x)median_fun(x, 2),...
                                      res_mat_cell, 'uniformoutput', 0))));
        
        var_ratio_tot = tot_var_vec ./ (sum_group_var_vec + eps);
        var_ratio_ab = ab_var_vec ./ (a_var_vec + b_var_vec + eps);

%             sum_group_var_vec_l2 = sum(cat(2,...
%                          cell2mat(cellfun(tot_var_l2, res_mat_cell, 'uniformoutput', 0))...
%                                                 ), 2);
%             var_ratio_ab = tot_var_l2(cat(2, res_mat_cell{:}))./(sum_group_var_vec_l2 + eps);
        
        median_var_ratio = median_var_vec ./ (sum_group_var_vec + eps);
            
        var_ratio_tot(isnan(var_ratio_tot)|isinf(var_ratio_tot)) = 0;
        var_ratio_ab(isnan(var_ratio_ab)|isinf(var_ratio_ab)) = 0;
        median_var_ratio(isnan(median_var_ratio)|isinf(median_var_ratio)) = 0;
    end
    
    % calculates variance ratio for gold standard files (robustness metric)
    function gs_var_ratio = gs_var_ratio_flow_score(res_groups, skip_nans)
    
        if ~exist('skip_nans', 'var')
            skip_nans = false;
        end    
    
        general_tag = 'src=goldstd';
        gs_file_names = {'angry bt customer01','angry dad','bvc intro01',...
                         'churchill01','ghandi01','gilad01',...
                         'jihad01','martin luther01','nassrallah01',...
                         'obama romni01','obamashootings01','pulp fiction 8k',...
                         'pulp fiction low 8k','will smith01',...
                         'yoram01','yoram02','yoram04',...
                         'yoram12','yoram21'};
                     
        if skip_nans
            var_fun = @nanvar;
        else
            var_fun = @var;
        end
                     
        file_list = cat(1,res_groups(:).files_list).';
        res_mat = cat(2, res_groups(:).res_mat);
        
        % remove all non gs files
        rem_ind = cellfun(@isempty, strfind(file_list, general_tag));
        file_list(rem_ind) = [];
        res_mat(:,rem_ind) = [];
        
        inds_cell = cell(size(gs_file_names));
        % find the index of each file in the file list
        for i_file = 1:numel(gs_file_names)
            inds_cell{i_file} = find(~cellfun(@isempty,...
                                        strfind( file_list, gs_file_names{i_file})));
            
        end
        
        % variance function
        tot_var_l2 = @(x)size(x,2).*var_fun(x, 0, 2);                
        
        % total variance over only the relevant files 
        ind_all_files = unique([inds_cell{:}], 'sorted');
        if ~isempty(ind_all_files)
            tot_var_vec = tot_var_l2(res_mat(:,ind_all_files));

            % sum internal variance for each file
            sum_group_var_vec = 0;
            for i_file = 1:numel(gs_file_names)
                if ~isempty(inds_cell{i_file})
                    sum_group_var_vec = sum_group_var_vec + ...
                                        tot_var_l2(res_mat(:, inds_cell{i_file}));
                end

            end

            % devide to get final ratio
            gs_var_ratio = tot_var_vec ./ (sum_group_var_vec + eps);    
        else
            gs_var_ratio = zeros(size(res_mat, 1), 1);
        end
        
        gs_var_ratio(isnan(gs_var_ratio)|isinf(gs_var_ratio)) = 0;
    end
    
    % calculates simple score for the flows
    function [res_mat_cell, edges_mat] = postprocess_outliers_results_mat(res_mat_cell, num_edges, skip_nans)
    
        if ~exist('skip_nans', 'var')
            skip_nans = false;
        end
        
        num_flows = size(res_mat_cell{1}, 1);
        num_groups = numel(res_mat_cell);
                
        all_res_mat = cat(2, res_mat_cell{:});
        edges_mat = zeros(num_flows, num_edges);        
            
%         try delete(gcp('nocreate')); catch, end;
        try
            c = parcluster('local');
            parpool(c.NumWorkers);
        catch
        end
        
        parfor_progress = ParforProgMon('Post Process', num_flows);
        cleanupObj = onCleanup(@()parfor_progress.delete());
        
        parfor flow_i = 1:num_flows
            %edges for histogram count
            edges_vec = calc_edges_vec(all_res_mat(flow_i,:), num_edges, 'iqr5', skip_nans); 
            
            % collect outliers
            all_res_mat(flow_i, :) = collect_outliers_to_edges(all_res_mat(flow_i,:), edges_vec);
            
            % save edges mat
            edges_mat(flow_i, :) = edges_vec;            
            
            parfor_progress.increment();
        end
        
        % redistribute files
        cur_file = 1;
        for g_ind = 1:num_groups
            cur_files_ind = cur_file:(cur_file+size(res_mat_cell{g_ind},2)-1);

            res_mat_cell{g_ind} = all_res_mat(:, cur_files_ind);

            cur_file = cur_file + size(res_mat_cell{g_ind},2);
        end
    end

    % calculates simple entropy
    function en_p = entropy_calc_on_vec(p)    
        sum_p = sum(p);
        en_p = -sum((p./sum_p).*log2(eps + p./sum_p));
    end
    
    % calculates edges vec after disregarding outliers
    function edges_vec = calc_edges_vec(data_vec, num_edges, method, skip_nans)
        if ~exist('method','var') || isempty(method)
            method = 'iqr5';
        end
        if ~exist('skip_nans', 'var')
            skip_nans = false;
        end
        
        if skip_nans
            data_vec(isnan(data_vec)) = [];
        end        
        
        if isempty(data_vec)
            edges_vec = linspace(0, 1, num_edges);
        else
            range_vec = calc_outlier_range(data_vec, method);
            edges_vec = linspace(range_vec(1), range_vec(2), num_edges);
        end
    end

    % calc outliers range
    function range_vec = calc_outlier_range(data_vec, method, skip_nans)
        if ~exist('method','var') || isempty(method)
            method = 'iqr5';
        end
        if ~exist('skip_nans', 'var')
            skip_nans = false;
        end
        
        if skip_nans
            data_vec(isnan(data_vec)) = [];
        end
        
        range_vec = [0 0];
        switch method
            case 'iqr5'
                prctile_vec = quick_prctile(data_vec, [25 50 75]);
                iqr_res = prctile_vec(3) - prctile_vec(1);
                median_res = prctile_vec(2);
%                 iqr_res = quick_prctile(data_vec, 75) - quick_prctile(data_vec, 25);
%                 median_res = quick_prctile(data_vec, 50);
                range_vec(1) = max(min(data_vec), median_res - 5*iqr_res);
                range_vec(2) = min(max(data_vec), median_res + 5*iqr_res);
            case 'iqr2'
                prctile_vec = quick_prctile(data_vec, [25 50 75]);
                iqr_res = prctile_vec(3) - prctile_vec(1);
                median_res = prctile_vec(2);
%                 iqr_res = quick_prctile(data_vec, 75) - quick_prctile(data_vec, 25);
%                 median_res = quick_prctile(data_vec, 50);
                range_vec(1) = max(min(data_vec), median_res - 2*iqr_res);
                range_vec(2) = min(max(data_vec), median_res + 2*iqr_res);
            case 'none'
                range_vec(1) = min(data_vec);
                range_vec(2) = max(data_vec);
        end
    end
    
    % iqr calculation based on quick percentile calculation
    function iqr_res = quick_iqr(data_vec, skip_nans) 
        if ~exist('skip_nans', 'var')
            skip_nans = false;
        end
        
        if skip_nans
            data_vec(isnan(data_vec)) = [];
        end
        prctile_vec = quick_prctile(data_vec, [25 75]);
        iqr_res = prctile_vec(2) - prctile_vec(1);
    end
    
    % remove outliers
    function [data_vec, rem_ind] = remove_outliers(data_vec, out_method)
        if ~exist('out_method','var')
            out_method = '';
        end
        [data_vec, rem_ind] = replace_outliers(data_vec, out_method, 'remove');
    end
    
    % replace outliers
    function [data_vec rep_ind] = replace_outliers(data_vec, out_method, rep_method)
        if ~exist('out_method','var')
            out_method = '';
        end
        if ~exist('rep_method','var')
            rep_method = 'remove';
        end
        if isempty(data_vec)
            rep_ind = [];
            return;
        end
        % calc range
        out_range = calc_outlier_range(data_vec, out_method);
        % replace
        try
            switch rep_method
                case 'remove'
                    data_vec(data_vec>out_range(2) | data_vec<out_range(1)) = [];
                case 'trim'
                    data_vec(data_vec>out_range(2)) = out_range(2);
                    data_vec(data_vec<out_range(1)) = out_range(1);
                otherwise
                    if length(rep_method) == 1
                        data_vec(data_vec>out_range(2) | data_vec<out_range(1)) = rep_method;
                    end
            end
        catch ex
            disp(['stats_utils_lib:replace_outliers: ' ex.message]);
        end
        % replacement ind
        rep_ind = find(data_vec>out_range(2) | data_vec<out_range(1));
    end
        
    % collects outliers into the edges vec specified
    function data_vec = collect_outliers_to_edges(data_vec, edges_vec)
        if isempty(data_vec)
            return
        end
        data_vec(data_vec>edges_vec(end)) = edges_vec(end);
        data_vec(data_vec<edges_vec(1)) = edges_vec(1);
    end

    % vector smoothing
    function out_vec = vector_smoothing(in_vec, delta, method_str)
        len = length(in_vec);
        out_vec = in_vec;
        switch method_str
            case 'poly1'
                for i=1:len
                    ind_vec = max(1,i-delta):min(len, i+delta);
                    out_vec(i) = polyval(polyfit(ind_vec, in_vec(ind_vec),1), i);
                end
            case 'poly2'
                for i=1:len
                    ind_vec = max(1,i-delta):min(len, i+delta);
                    out_vec(i) = polyval(polyfit(ind_vec, in_vec(ind_vec),2), i);
                end
            case 'lowpass'
%                 filter_vec = ones(1,2*delta+1)/(2*delta+1);
%                 filter_vec = [1:delta delta+1 delta:-1:1]./((1+delta).^2);
                if size(in_vec, 1)>1
                    in_vec = in_vec.';
                end                
                filter_vec = fast_norm_hann_win(2*delta+1);     
                conv_vec = conv(in_vec, filter_vec, 'full');
                out_vec = [conv_vec(delta:-1:1)+conv_vec((delta+1):(2*delta)),...
                           conv_vec((2*delta+1):(end-2*delta)),...
                           conv_vec((end-2*delta+1):(end-delta))+conv_vec(end:-1:(end-delta+1))];
                       
            case 'mean'
                if numel(in_vec) > 1
                    cs_vec = cumsum(in_vec);
                    i_vec=1:length(in_vec);
                    ind_1 = i_vec-delta-1;
                    ind_1(ind_1<1) = 1;
                    ind_2 = i_vec+delta;
                    ind_2(ind_2>length(in_vec)) = length(in_vec);
                    out_vec = (cs_vec(ind_2) - cs_vec(ind_1))./(ind_2-ind_1);
                end
            otherwise 
                disp('stats_utils_lib:vector_smoothing: method not recognized');
                
        end
                    
                    % hann
                    function w = fast_norm_hann_win(n)
                        if n==1
                            w = 1;
                        else
                            half_w = 0.5 - 0.5*cos(2*pi*(0:((n+1)/2)-1)/(n-1));
                            w = [half_w, half_w(end-1:-1:1)];
                            w = w./sum(w);
                        end
                    end
    end
    
    % filters gaps using according to threshold and length constants 
    function out_vec = gap_vector_smoothing(raw_vec, half_min_gap_len, threshold_val)
        mean_val = vector_smoothing(raw_vec, half_min_gap_len, 'mean');
        out_vec = double(mean_val > threshold_val);
    end
    
    % relative frequency calculations
    function out_val = partial_sum_percentile_location_on_vec(in_vec, pctl)
        in_vec = in_vec - min(0,min(in_vec(:))); 
        cs_vec = cumsum(in_vec); 
        norm_coord_vec = linspace(0,1,length(in_vec));
        cs_vec = cs_vec./(cs_vec(end)+eps)+0.0001*norm_coord_vec;
        norm_coord_vec(isnan(cs_vec) | isinf(cs_vec)) = [];
        cs_vec(isnan(cs_vec) | isinf(cs_vec)) = [];
        if numel(cs_vec)>1
%             out_val = (find(cs_vec>=((pctl/100).*cs_vec(end)),1,'first')-1)/length(in_vec);
            out_val = interp1(cs_vec,norm_coord_vec,pctl/100,'linear',0);
        else
            out_val = pctl/100;
        end
    end
    
    % relative frequency calculations
    function out_val = partial_sum_percentile_location_ratio_on_vec(in_vec, pctl1, pctl2)
        in_vec = in_vec - min(0,min(in_vec(:))); 
        cs_vec = cumsum(in_vec); 
        norm_coord_vec = linspace(0,1,length(in_vec));
        cs_vec = cs_vec./(cs_vec(end)+eps)+0.0001*norm_coord_vec;
        norm_coord_vec(isnan(cs_vec) | isinf(cs_vec)) = [];
        cs_vec(isnan(cs_vec) | isinf(cs_vec)) = [];
        if numel(cs_vec)>1
%             out_ind1 = (find(cs_vec>=((pctl1/100).*cs_vec(end)),1,'first')-1)/length(in_vec);
%             out_ind2 = (find(cs_vec>=((pctl2/100).*cs_vec(end)),1,'first')-1)/length(in_vec);
%             out_val = out_ind1./(out_ind2+eps);
            out_val1 = interp1(cs_vec,norm_coord_vec,pctl1/100,'linear',0);
            out_val2 = interp1(cs_vec,norm_coord_vec,pctl2/100,'linear',0);
            out_val = out_val1./(out_val2+eps);
        else
            out_val = pctl1/pctl2;
        end
    end
    
    % relative frequency calculations
    function out_vec = partial_sum_percentile_location_on_mat(in_mat, pctl, dim)
        %%% min(0,min)?
        if dim==1
            in_mat = in_mat - repmat(min(in_mat,[],1), size(in_mat,1), 1); %no negative values
            cs_mat = cumsum(in_mat, 1);

            % total number of rows - number of elements in each
            % row that is larger than x-th percentile of the sum
            ge_pctl_mat = cs_mat >= repmat((pctl/100)*cs_mat(end,:) ,size(cs_mat,1), 1);
            out_vec = (size(in_mat,1) - sum(ge_pctl_mat, 1) + 1); 
        else
            in_mat = in_mat - repmat(min(in_mat,[],2), 1, size(in_mat,2)); %no negative values
            cs_mat = cumsum(in_mat, 2);

            % total number of rows - number of elements in each
            % row that is larger than x-th percentile of the sum
            ge_pctl_mat = cs_mat >= repmat((pctl/100)*cs_mat(:,end) , 1, size(cs_mat,2));
            out_vec = (size(in_mat,2) - sum(ge_pctl_mat, 2) + 1); 
        end    
    end
    
    % relative frequency calculations
    function out_vec = partial_sum_percentile_location_ratio_on_mat(in_mat, pctl1, pctl2, dim)
        %%% min(0,min)?
        if dim==1
            in_mat = in_mat - repmat(min(in_mat,[],1), size(in_mat,1), 1); %no negative values
            cs_mat = cumsum(in_mat, 1);

            % total number of rows - number of elements in each
            % row that is larger than x-th percentile of the sum
            ge_pctl1_mat = cs_mat >= repmat((pctl1/100)*cs_mat(end,:) ,size(cs_mat,1), 1);
            ge_pctl2_mat = cs_mat >= repmat((pctl2/100)*cs_mat(end,:) ,size(cs_mat,1), 1);
            out_vec = (size(in_mat,1) - sum(ge_pctl1_mat, 1) + 1)./...
                                ((size(in_mat,1) - sum(ge_pctl2_mat, 1) + 1) + eps); 
        else
            in_mat = in_mat - repmat(min(in_mat,[],2), 1, size(in_mat,2)); %no negative values
            cs_mat = cumsum(in_mat, 2);

            % total number of rows - number of elements in each
            % row that is larger than x-th percentile of the sum  
            ge_pctl1_mat = cs_mat >= repmat((pctl1/100)*cs_mat(:,end) , 1, size(cs_mat,2));
            ge_pctl2_mat = cs_mat >= repmat((pctl2/100)*cs_mat(:,end) , 1, size(cs_mat,2));
            out_vec = (size(in_mat,2) - sum(ge_pctl1_mat, 2) + 1)./...
                                ((size(in_mat,2) - sum(ge_pctl2_mat, 2) + 2) + eps); 
        end    
    end
    
    % calculates a histogram matrix on a signal by windowing
    function [hist_mat, edges_vec] = windowed_histogram_mat(x, nwind, n_bins)
    
        % params
        noverlap = ceil(nwind.*0.5);
        x = x(:);
        
        % windowing
        ncol = fix((length(x)-noverlap)/(nwind-noverlap));
        colindex = (0:(ncol-1))*(nwind-noverlap)+1;

        % Calculate full frames
        [firsts, deltas] = meshgrid(colindex(1:ncol), (0:(nwind-1)));
        idx_mat = firsts + deltas;
        xin = x(idx_mat); 
        
        % edges
        edges_vec = linspace(min(x), max(x), n_bins);
        
        % hist count
        hist_mat = histc(xin, edges_vec);
        
%         figure;
%         imagesc(1:size(hist_mat,2),edges_vec,log(hist_mat)); 
%         axis xy;
    
    end