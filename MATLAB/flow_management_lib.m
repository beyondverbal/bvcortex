function varargout = flow_management_lib(varargin)
% library file for different flow management functions
 
basic_feat_fh = [];
cls_fh = [];
% feature_fh = [];
wsola_lib_fh = [];
psycho_feat_fh = [];
psysound_feat_fh = [];

f_handles = struct(... % recommended way to pass nested func handles
        'calc_simple_segments', @calc_simple_segments,...
        'calc_full_slicing_flow',@calc_full_slicing_flow,...
        'construct_params_s', @construct_params_s,...
        'append_signal_to_slices_online', @append_signal_to_slices_online,...
        'preload_models_by_config', @preload_models_by_config,...        
        'calc_basic_features',@calc_basic_features,...        
        'force_out_of_flow_slices', @force_out_of_flow_slices,...
        'garbage_collection_on_slices_online', @garbage_collection_on_slices_online,...
        'calc_slices_by_vad', @calc_slices_by_vad,...
        'slice_and_shrink_by_vad', @slice_and_shrink_by_vad,...
        'shrink_slice_by_vad', @shrink_slice_by_vad,...
        'shrink_audio_by_vad', @shrink_audio_by_vad,...
        'analyze_slices',@analyze_slices,...
        'calc_summary_analysis',@calc_summary_analysis,...
        'calc_call_trend_on_results_struct',@calc_call_trend_on_results_struct,...
        'get_slices_results_struct', @get_slices_results_struct,...
        'calc_avg_spectrum_tones', @calc_avg_spectrum_tones,...
        'calc_vad_sum_on_slices',@calc_vad_sum_on_slices,...
        'classify_temper_2d_on_slices',@classify_temper_2d_on_slices,...
        'classify_composure_on_slices',@classify_composure_on_slices,...
        'calc_cooperation',@calc_cooperation,...
        'calc_servicetone',@calc_servicetone,...
        'calc_salestone',@calc_salestone,...
        'calc_analyzer_dotnet_link',@calc_analyzer_dotnet_link,...
        'custom_preprocess', @custom_preprocess...
         );
     
varargout{1} = f_handles;

%%%% NESTED FUNCTIONS %%%%%

    % run lib funcs
    function run_libs()
        if isempty(basic_feat_fh)
            basic_feat_fh = basic_features_lib();     
        end
        if isempty(cls_fh)
            cls_fh = classifier_lib();
        end
%         if isempty(feature_fh)
%             feature_fh = feature_lib();
%         end
        if isempty(wsola_lib_fh)
            wsola_lib_fh = wsola_lib;
        end
        
        if isempty(psycho_feat_fh)
            psycho_feat_fh = psycho_features_lib();
        end
        
        if isempty(psysound_feat_fh)
            psysound_feat_fh = psysound_features_lib();
        end
        
    end

    %%% OFF-LINE functions

    % calculates buffered segments
    function seg_s = calc_simple_segments(signal, fs, buff_length, overlap_ratio)

        frame_len = floor(buff_length*fs); % translate time to samples length
        frame_step = floor(frame_len.*(1-overlap_ratio));
        n_frames = max(1,ceil((length(signal)-frame_len)./(frame_step))); % num frames
                
        for i=1:n_frames
            start_sample = (1+frame_step*(i-1));
            if i<n_frames
                seg_s(i).signal_ind_vec =...
                                start_sample:(start_sample+frame_len-1);
            else
                seg_s(i).signal_ind_vec =...
                                start_sample:(length(signal)); % leftovers
            end
        end          
 
    end

    % run full flow
    function [slices_s, feature_s] = calc_full_slicing_flow(signal, fs)
        
        params_s = default_params();

        slices_s = [];
        feature_s = [];
        
        % calculate temporary segments
        seg_s = calc_simple_segments(signal, fs, 5, 0);
        
        % calc features for each segment
        for seg_i = 1:length(seg_s)
            % signal segment
            sig_seg = signal(seg_s(seg_i).signal_ind_vec);
            
            [slices_s, feature_s] = append_signal_to_slices_online(...
                                            slices_s, feature_s, sig_seg, fs, params_s);
        end     
    end    

    %%% ON-LINE functions

    % set flow params
    function params_s = construct_params_s(params_s, default_s)
        if ~exist('default_s','var')
            default_s = struct('fs',8000,...
                      'max_relevance_time',45,'relevance_tolerance',15,...
                      'min_speech',9,'max_speech',10,...
                      'crit_snr',20,'shrink_audio',1,'slice_history',2,...
                      'max_forced_speech',20,'min_forced_speech',5,...
                      'min_rasta_time',4.97,'min_rasta_overlap',2.5,...
                      'temper_low_cutoff',40,'temper_high_cutoff',65,...
                      'vad_fs',100,...
                      'meta_data','',...                      
                      'basic_features',{{'~all','fft_1024','fft_256','rasta','vad',...
                                          'voc_src_filt','pitch_mat',...
                                          'tonal_dissonance','12th_octave_fft',...
                                          '~psysound'}},...
                      'analyses_types',{{'vad_sum','valence_arousal_temper_gender',...
                                         'audio_quality',...
                                        'tonetest','phrases_info','~basic_stats',...
                                        '~temper_2d','~composure',...
                                        '~cooperation', '~servicetone', '~salestone'}},...
                      'summary_analyses_types',{{'call_trend','total_voice_content',...
                                                 'basic_stats'
                                                 }},...
                      'summary_audio_quality_policy',...
                                        {{'good','marginal_better','marginal_worse','bad'}},... 
                      'slicing_progress_flag', 0 ...
                      );
        end
              
        fields = fieldnames(default_s);
        for i_field = 1:numel(fields)
            if ~isfield(params_s,fields{i_field}) || isempty(params_s.(fields{i_field}))
                params_s.(fields{i_field}) = default_s.(fields{i_field});
            end
        end
    end

    % default flow params
    function params_s = default_params
        params_s = construct_params_s(struct);
    end

    % run online
    function [slices_s, feature_s] = append_signal_to_slices_online(...
                                            slices_s, feature_s, signal, fs, params_s)
            
        if ~exist('params_s','var')
            params_s = default_params();
        end
        
        % append signal buffer
        feature_s = append_input_buffer(feature_s, signal, fs);
        
        % calculate the features 
        feature_s = process_buffer_into_basic_features(slices_s, feature_s, params_s);
%         save('C:\Users\yonatan.sasson\Documents\MATLAB\compare\ref_after_basic_features','feature_s')
        % add meta_data (didn't find a better place to do it)
        feature_s = add_meta_data_to_feature_s(feature_s, params_s);
        
        % if buffer was processessed
        if isempty(feature_s.input_buffer)
            
            % creates new slices if possible
            [slices_s, feature_s] = process_basic_features_into_slices(slices_s, feature_s, params_s);
%             save('C:\Users\yonatan.sasson\Documents\MATLAB\compare\ref_after_first_slice','slices_s')
            % update slicing progress
            feature_s.slicing_progress = update_slicing_progress(feature_s, params_s);
        end      
        
        
            % appends just the new signal buffer
            function feature_s = append_input_buffer(feature_s, new_signal_buffer, fs)
                feature_s.fs = fs;
                if isempty(feature_s) || ~isfield(feature_s,'input_buffer')
                    feature_s.input_buffer = new_signal_buffer;
                else
                    feature_s.input_buffer = [feature_s.input_buffer new_signal_buffer];
                end
            end
            
            % adds some meta data
            function feature_s = add_meta_data_to_feature_s(feature_s, params_s)
                feature_s.meta_data = params_s.meta_data;
            end    
            
    end

    % calculates progress of filling the next slice
    function slicing_progress = update_slicing_progress(feature_s, params_s)      
        if params_s.slicing_progress_flag
            if isfield(feature_s,'vad_vec')
                trimmed_vad_vec = feature_s.vad_vec(max(1,end-params_s.max_relevance_time.*params_s.vad_fs):end);
                slicing_progress = params_s.min_speech - (sum(trimmed_vad_vec)/params_s.vad_fs);
            else
                slicing_progress = params_s.min_speech;
            end
        else
            slicing_progress = [];
        end
    end

    % calculates the basic features
    function feature_s = process_buffer_into_basic_features(slices_s, feature_s, params_s)
%         features_cell = params_s.basic_features;
        
        % calculate buffer for processing and update feature_s if needed
        [feature_s, accepted_buffer] = ...
                basic_features_buffer_logic(feature_s, params_s);
                
        % if buffer was accepted (flushed from input buffer)
        if ~isempty(accepted_buffer)
% 			fprintf ('YL: 2 %d\n', length(accepted_buffer));
        
            % get time offset for all the clocks
            if isfield(feature_s,'t_vec_sig') && ~isempty(feature_s.t_vec_sig)
                time_offset = feature_s.t_vec_sig(end);
            elseif ~isempty(slices_s) && ...
                            isfield(slices_s(end),'t_vec_sig') &&...
                                       ~isempty(slices_s(end).t_vec_sig)
                time_offset = slices_s(end).t_vec_sig(end);
            else
                time_offset = 0;
            end

            % calc features
            if ~isfield(feature_s,'frame_overlap') || isempty(feature_s.frame_overlap)
                frame_overlap = [];
            else
                frame_overlap = feature_s.frame_overlap;
            end
            new_feature_s = calc_vad_features(...
                                    struct('signal',accepted_buffer,...
                                           'fs',feature_s.fs,...
                                           'frame_overlap', frame_overlap),...
                                    time_offset, params_s.basic_features);
                                
            % appends new feature data
            feature_s = merge_basic_feature_structs(feature_s, new_feature_s);
            
        end
        
            % calculate buffer for processing
            function [feature_s, accepted_buffer] = basic_features_buffer_logic(feature_s, params_s)

                in_buffer_len = length(feature_s.input_buffer)./feature_s.fs;
% 				fprintf ('YL: 3 %d\n', length(feature_s.input_buffer));
                if isfield(feature_s,'vad_vec')
                    old_vad_len =  sum(feature_s.vad_vec)/params_s.vad_fs;
                else
                    old_vad_len = 0;
                end
                
                % sufficient length for rasta
                if in_buffer_len >= params_s.min_rasta_time
                    accepted_buffer = feature_s.input_buffer;
                    feature_s.input_buffer = [];
                else 
                    % check if overlapping is permitted for this length
                    if ( (old_vad_len + in_buffer_len) >= params_s.min_speech ) &&...
                            ( in_buffer_len >= params_s.min_rasta_overlap )

                        % calc overlap length
                        overlap_len = params_s.min_rasta_time - in_buffer_len;
                        
                        [feature_s, overlap_signal] = crop_feature_s(feature_s, overlap_len, 'before_start');

                        accepted_buffer = [overlap_signal feature_s.input_buffer];                      
                        feature_s.input_buffer = [];
                    else
                        % not enough for overlap
                        accepted_buffer = [];
                    end
                end
%  				fprintf ('YL: 4 %d\n', length (accepted_buffer));
           end
        
    end
    
    % appends and merges the feature data
    function feature_s = merge_basic_feature_structs(feature_s, new_feature_s)
        if isempty(feature_s)
            feature_s = new_feature_s;
        else
            % replace  
            feature_s.f_vec_1024 = new_feature_s.f_vec_1024;
            feature_s.f_vec_256 = new_feature_s.f_vec_256;
            feature_s.frame_overlap = new_feature_s.frame_overlap;
%             feature_s.ct_vec_256 = new_feature_s.ct_vec_256;
            
            % merge with old features
            merge_fields = {'signal','t_vec_sig','t_vec_512','t_vec_80',...
                            'powspec_1024','powspec_256','spectrum_256',...
                            'rasta_mat','pure_rasta_mat',...
                            'vad_vec','raw_vad_vec','bp_ste_vec','input_buffer'};
            for i_field = 1:numel(merge_fields)
                field_name = merge_fields{i_field};
                if isfield(feature_s,field_name) && isfield(new_feature_s,field_name) ...
                        && size(feature_s.(field_name),1) == size(new_feature_s.(field_name),1)
                    feature_s.(field_name) = [feature_s.(field_name) new_feature_s.(field_name)];
                elseif isfield(new_feature_s,field_name) && ~isfield(feature_s,field_name)
                    feature_s.(field_name) = new_feature_s.(field_name);
                end
            end
            
           
        end
    end

    % creates new slices out of updated basic features
    function [slices_s, feature_s] = process_basic_features_into_slices(slices_s, feature_s, params_s)
        
        % recalc vad vec        
        feature_s.vad_vec = recalc_vad_vec();
        
%         tmp = feature_s.vad_vec;
%         save('C:\Users\yonatan.sasson\Documents\MATLAB\compare\ref_newvad','tmp')
        
        % calc slices
        [slices_start, slices_end] = calc_slices_by_vad(...
                                            feature_s.vad_vec,...
                                            feature_s.raw_vad_vec,...
                                            params_s);

        % slice data if possible
        if ~isempty(slices_start)
            [new_slices_s, feature_s] = slice_and_shrink_by_vad(...
                                feature_s, slices_start, slices_end, params_s);
            
%             save('C:\Users\yonatan.sasson\Documents\MATLAB\compare\ref_slice1','new_slices_s')
%             save('C:\Users\yonatan.sasson\Documents\MATLAB\compare\ref_feature1','feature_s')
%             save('C:\Users\yonatan.sasson\Documents\MATLAB\compare\ref_params','params_s')
                            
                            
            new_slices_s = post_process_slices_after_vad(new_slices_s, params_s);
            
%             save('C:\Users\yonatan.sasson\Documents\MATLAB\compare\ref_slice2','new_slices_s')
%             save('C:\Users\yonatan.sasson\Documents\MATLAB\compare\ref_feature2','feature_s')
            
            if (isstruct(slices_s) && numel(fieldnames(slices_s)))
                slices_s = add_new_slices(slices_s, new_slices_s);
            else
                slices_s = new_slices_s;
            end
        end       
        
%         save('C:\Users\yonatan.sasson\Documents\MATLAB\compare\ref_slice','slices_s')
        
        % remove old parts of feature_s (long silences)
        feature_s = garbage_collection_on_feature_s(feature_s, params_s);
        
            % add new slices
            function s = add_new_slices(s, new_s)
                n = length(s);
                fields = fieldnames(new_s);
                for i_new = 1:length(new_s)
                    for i_field = 1:length(fields)
                        s(n + i_new).(fields{i_field}) = new_s(i_new).(fields{i_field});
                    end
                end
            end
        
            % recalculates the VAD
            function vad_vec = recalc_vad_vec()
                run_libs();
                vad_vec = cls_fh.vad_vec_on_raw_vad_vec(...
                                feature_s.raw_vad_vec, feature_s.t_vec_80);
                            
                % history of bp_ste_vec from previous slices
                if ~isempty(slices_s) && isfield(slices_s, 'bp_ste_vec')
                    hist_bp_ste = [slices_s.bp_ste_vec];
                else
                    hist_bp_ste = [];
                end
                vad_vec = cls_fh.post_vad_volume_separation_on_ste(...
                                                vad_vec, feature_s.t_vec_80,...
                                                feature_s.bp_ste_vec,...
                                                hist_bp_ste, params_s.crit_snr);  
            end        
                        
            % remove old data
            function feature_s = garbage_collection_on_feature_s(feature_s, params_s)
                vad_vec = feature_s.vad_vec;
                t_vec_80 = feature_s.t_vec_80;
                vad_fs = params_s.vad_fs;
                if length(vad_vec)/vad_fs > ...
                            (params_s.max_relevance_time + params_s.relevance_tolerance)
                        
                    cand_ind = (t_vec_80>=(t_vec_80(end)-params_s.max_relevance_time));
                       
                    slice_start = find(cand_ind,1,'first');
                    slice_end = length(t_vec_80);
                        
                    ind_s = calc_clock_inds(feature_s, slice_start, slice_end);
                    
                    feature_s = crop_feature_s(feature_s, ind_s, 'middle');                    
                end
            end
            
    end

    % postprocesses slices after vad
    function new_slices_s = post_process_slices_after_vad(slices_s, params_s)
        run_libs();
        
        % rasta
        for i_slice = 1:length(slices_s)
            slices_s(i_slice).rasta_mat = basic_feat_fh.strech_snr_on_rasta(...
                    slices_s(i_slice).pure_rasta_mat);

            new_slices_s(i_slice) = add_post_vad_basic_features(slices_s(i_slice), [],...
                                               params_s.basic_features);            
        end
    end

    % forced slicing
    function f_slices_s = force_out_of_flow_slices(slices_s, feature_s, params_s)
        
        % process all available buffer
        if isfield(feature_s,'input_buffer') && ~isempty(feature_s.input_buffer)
            % override overlap settings
            old_overlap = params_s.min_rasta_overlap;
            old_min_speech = params_s.min_speech;
            params_s.min_rasta_overlap = 0;            
            params_s.min_speech = 0;            
            feature_s = process_buffer_into_basic_features(slices_s, feature_s, params_s);
            params_s.min_rasta_overlap = old_overlap; % not that it matters, but for piece of mind
            params_s.min_speech = old_min_speech;            
        end

        % join feature_s with previous slice
        if ~isempty(slices_s)
            forced_feature_s = merge_basic_feature_structs(slices_s(end), feature_s);
        else
            forced_feature_s = feature_s;
        end
        
        % find if new slice start can be found
        [slice_start_ind, slice_end_ind] = calc_forced_slice_start(forced_feature_s, params_s);
        
        % shrink the slice
        if ~isempty(slice_start_ind)
            % shrink slice
            [f_slices_s, ~] = slice_and_shrink_by_vad(...
                                forced_feature_s, slice_start_ind, slice_end_ind, params_s);
                            
            % recalc rasta
            f_slices_s = post_process_slices_after_vad(f_slices_s, params_s);
                                                   
            f_slices_s.flow_type = 'forced';
        else
            f_slices_s = [];
        end
        
            % looks back for a possible start of slice
            function [slice_start_ind, slice_end_ind] = calc_forced_slice_start(ft_s, params_s)
                min_vad = ceil(params_s.min_forced_speech*params_s.vad_fs);
                max_vad = ceil(params_s.max_forced_speech*params_s.vad_fs);
                
                vad_vec = ft_s.vad_vec;
                cs_vad = cumsum(vad_vec);
                t_vec = ft_s.t_vec_80;
                
                % conditions : in relevance time framce, at least min_vad,
                % best if closer to max_vad
                cand_ind = (t_vec>=(t_vec(end)-params_s.max_relevance_time)) &...
                           (cs_vad(end)-cs_vad)>=min_vad &...
                           (cs_vad(end)-cs_vad)<=max_vad;
                       
                slice_start_ind = find(cand_ind,1,'first');
                slice_end_ind = length(vad_vec);
            end
    end

    % calculates the basic features
    function preload_models_by_config(params_s)
        run_libs();
        cls_fh.preload_models_by_config([params_s.analyses_types...
                                        params_s.summary_analyses_types]);
    end

    % calculates the basic features
    function feature_s = calc_basic_features(feature_s, time_offset, features_cell)
        run_libs();
        
        if ~exist('time_offset','var') || isempty(time_offset)
            time_offset = 0;
        end
                
        if ~exist('features_cell','var') || isempty(features_cell)
            features_cell = {};
        end
        
        if isempty(features_cell) || any(strcmpi(features_cell,'all'))
            features_cell = [features_cell, {'fft_1024','fft_256','rasta','vad',...
                             'voc_src_filt','pitch_mat','tonal_dissonance',...
                             '12th_octave_fft'...
                             '~psysound'}];
        end
        
        % functions are nested for readability, 
        % feature_s is not passed back and forth for performance (because it is updated)
        
        if ~any(strcmpi(features_cell,'post_vad_process')) % in order to avoid recalc vad features
            calc_fft_256_on_feature_struct();

            calc_rasta_on_feature_struct();

            calc_vad_vec_on_feature_struct();
            
            calc_fft_1024_on_feature_struct();
        end
        
        if ~any(strcmpi(features_cell,'pre_vad_process'))
            calc_voc_src_filt_on_feature_struct();

            calc_pitch_mat_on_feature_struct();

            calc_psysound_on_feature_struct();
            
            calc_dissonance_on_feature_struct();
            
            calc_12th_octave_fft_on_feature_struct();
        end 
        
            % fft 256
            function calc_fft_256_on_feature_struct()
                if ~isfield(feature_s,'frame_overlap') || isempty(feature_s.frame_overlap)
                    frame_overlap = [];
                else
                    frame_overlap = feature_s.frame_overlap;
                end
                if any(strcmpi(features_cell,'fft_256')) || any(strcmpi(features_cell,'vad'))
                    [spectrum_256, f_vec_256, t_vec_80] = basic_feat_fh.param_specgram(...
                                                          [frame_overlap feature_s.signal],...
                                                          feature_s.fs, 256, 80);
                    powspec_256 = abs(spectrum_256).^2;
                else
                    [spectrum_256, powspec_256, f_vec_256, t_vec_80] = deal(0);
                end
                feature_s.frame_overlap = feature_s.signal((end-256+80+1):end);
                feature_s.t_vec_sig = time_offset + (1:length(feature_s.signal))./feature_s.fs;
                feature_s.f_vec_256 = f_vec_256;
                feature_s.t_vec_80 = time_offset + t_vec_80;
                feature_s.spectrum_256 = spectrum_256;
                feature_s.powspec_256 = powspec_256;
            end
            
            % rasta 
            function calc_rasta_on_feature_struct()
                if any(strcmpi(features_cell,'rasta')) || any(strcmpi(features_cell,'vad'))
                    [rasta_mat, pure_rasta_mat] = basic_feat_fh.mat_rasta(...
                                                  feature_s.powspec_256, ...
                                                  feature_s.f_vec_256, feature_s.t_vec_80);
                else
                    [rasta_mat, pure_rasta_mat] = deal(feature_s.powspec_256);
                end
                feature_s.rasta_mat = rasta_mat;
                feature_s.pure_rasta_mat = pure_rasta_mat;
            end
            
            % vad
            function calc_vad_vec_on_feature_struct()
                if any(strcmpi(features_cell,'vad'))
                    % get processed vad
                    [vad_vec, raw_vad_vec] = cls_fh.vad_rasta_mat2vec(...
                                             feature_s.rasta_mat, feature_s.f_vec_256,...
                                             feature_s.t_vec_80);
                    bp_ste_vec = cls_fh.bp_ste_on_powspeccell({...
                                             feature_s.powspec_256, ...
                                             feature_s.f_vec_256, feature_s.t_vec_80});
                else
                    [vad_vec, raw_vad_vec, bp_ste_vec] = deal(zeros(size(feature_s.t_vec_80)));
                end
                feature_s.vad_vec = vad_vec;
                feature_s.raw_vad_vec = raw_vad_vec;
                feature_s.bp_ste_vec = bp_ste_vec;
            end
            
            % fft 1024
            function calc_fft_1024_on_feature_struct()
                if any(strcmpi(features_cell, 'fft_1024'))
                    % get specgram for avg
                    [spectrum_1024, f_vec_1024, t_vec_512] = basic_feat_fh.param_specgram(...
                                                             feature_s.signal, feature_s.fs,...
                                                             1024, 512);
                    powspec_1024 = abs(spectrum_1024).^2;
                else
                    [spectrum_1024, powspec_1024, f_vec_1024, t_vec_512] = deal(0);
                end
                feature_s.t_vec_512 = time_offset + t_vec_512;
                feature_s.powspec_1024 = powspec_1024;
                feature_s.f_vec_1024 = f_vec_1024;
            end
            
            % source-filter voiced speech separation
            function calc_voc_src_filt_on_feature_struct()
                if any(strcmpi(features_cell,'voc_src_filt'))
                    [voc_src, voc_filt, ~, ~] = basic_feat_fh.vocal_source_filter_separation(...
                                                feature_s.signal, feature_s.fs, ...
                                                feature_s.spectrum_256, feature_s.powspec_256,...
                                                feature_s.f_vec_256, ...
                                                feature_s.t_vec_80);
                else
                    [voc_src, voc_filt] = deal(zeros(size(feature_s.spectrum_256)));
                end 
                feature_s.voc_src_256 = voc_src;
                feature_s.voc_filt_256 = voc_filt;
            end
            
            % pitch_mat calculation
            function calc_pitch_mat_on_feature_struct()
                if any(strcmpi(features_cell,'pitch_mat'))

                    [pitch_mat, ct_vec, pitch_t_vec] = basic_feat_fh.pitch_by_cepstrum_combined(...
                                            feature_s, fieldnames(feature_s), []);
                    [pitch_f, pitch_a, pitch_t_vec] = basic_feat_fh.pitch_contour_on_pitch_mat(...
                                            pitch_mat, ct_vec, pitch_t_vec);
                else
                    pitch_mat = zeros(size(feature_s.spectrum_256));
                    pitch_f = zeros(size(feature_s.t_vec_80));
                    pitch_a = zeros(size(feature_s.t_vec_80));
                    ct_vec = 0;
                    pitch_t_vec = 0;
                end
                feature_s.pitch_mat_256 = pitch_mat;
                feature_s.ct_vec_256 = ct_vec;
                feature_s.pitch_f = pitch_f;
                feature_s.pitch_a = pitch_a;
                feature_s.pitch_t_vec = pitch_t_vec;
            end
            
            % psysound
            function calc_psysound_on_feature_struct()
                
%                 ps_featurelist = {...
%                          'FFT', 'Hilbert', 'Cepstrum'...
%                         ,'CepstrumComplex', 'AutoCorrelation'...
%                         ,'SLM', 'RLB', 'ThirdOctaveBand', 'CPBFFT'...
%                         ,'LoudnessCF', 'LoudnessMGB', 'LoudnessMG'...
%                         ,'VirtualPitch', 'MIRPITCH', 'SWIPEP'...
%                         ,'ReverbTime', 'TwelfthOctConstQ', 'TwelfthOctIECFFT'...
%                         };
%                                 
%                 if any(strcmpi(features_cell, 'psysound'))
%                      psysound = psysound_feat_fh.psysound_features(...
%                                         feature_s.signal, feature_s.fs, ps_featurelist);
%                 else
                   psysound = struct;
%                 end
                feature_s.psysound = psysound;  
            end
            
            function calc_dissonance_on_feature_struct()
                if any(strcmpi(features_cell,'tonal_dissonance'))
                    [dissonance_hk, dissonance_s, diss_tonal_map, f_vec_diss_tonal_map] =...
                            psycho_feat_fh.TonalDissonance(...
                                    feature_s.powspec_256, feature_s.f_vec_256, feature_s.fs);
                else
                    dissonance_hk = 0;
                    dissonance_s = 0;
                    diss_tonal_map = 0;
                    f_vec_diss_tonal_map = 0;
                end
                feature_s.dissonance_hk = dissonance_hk;
                feature_s.dissonance_s = dissonance_s;
                feature_s.diss_tonal_map = diss_tonal_map;
                feature_s.f_vec_diss_tonal_map = f_vec_diss_tonal_map;
            end
            
            function calc_12th_octave_fft_on_feature_struct()
                if any(strcmpi(features_cell,'12th_octave_fft'))
                    [powspec_12thoct, f_vec_12thoct] =...
                            psycho_feat_fh.twelfth_oct_fft(feature_s.powspec_1024,...
                                                           feature_s.f_vec_1024);
                else
                    powspec_12thoct = 0;
                    f_vec_12thoct = 0;
                end
                feature_s.powspec_12thoct = powspec_12thoct;
                feature_s.f_vec_12thoct = f_vec_12thoct;
            end
    end

    function feature_s = calc_vad_features(varargin)
%         feature_s = calc_basic_features(varargin{:}, {'vad'});
        feature_s = calc_basic_features(varargin{1}, varargin{2},...
                                        [varargin{3}(:)' 'pre_vad_process']);
    end

    function feature_s = add_post_vad_basic_features(varargin)
        feature_s = calc_basic_features(varargin{1}, varargin{2},...
                                        [varargin{3}(:)' 'post_vad_process']);
    end

    % erases basic features for old slices
    function slices_s = garbage_collection_on_slices_online(slices_s, params_s)
        ind_to_erase = 1:(length(slices_s)-params_s.slice_history);
        gc_fields = {'signal',...
                    't_vec_sig',...
                    'raw_vad_vec',...
                    'bp_ste_vec',...
                    'pitch_f',...
                    'pitch_a',...
                    'pitch_t_vec',...                    
                    'spectrum_256',...
                    'powspec_1024',...
                    'powspec_256',...
                    'dissonance_hk',...
                    'dissonance_s',...
                    'diss_tonal_map',...
                    'powspec_12thoct',...
                    'rasta_mat',...
                    'pure_rasta_mat',...
                    'voc_src_256',...
                    'voc_filt_256',...
                    'pitch_mat_256',...
                    'psysound',...
                    't_vec_512',...
                    'f_vec_256',...
                    'f_vec_1024',...
                    'f_vec_diss_tonal_map',...
                    'f_vec_12thoct',...
                    'ct_vec_256',...
                    'out_takes',...
                    'signal_only_voiced'...                    
                    };                
%                     'vad_vec',...
        for i_field = 1:numel(gc_fields)
            if isfield(slices_s, gc_fields{i_field})
                [slices_s(ind_to_erase).(gc_fields{i_field})] = deal([]); 
            end
        end
    end

    % calculate new slices according ro VAD values
    function [slices_start, slices_end] = calc_slices_by_vad(vad_vec, raw_vad_vec, params_s)
        
        % params
        if ~exist('params_s','var') || isempty(params_s)
            params_s = default_params();
        end
        
        % calculate new slices
        max_time = params_s.max_relevance_time; %seconds
        min_speech = params_s.min_speech; %seconds
        max_speech = params_s.max_speech; %seconds
        
%         vad_fs = 100;
        max_time_len = ceil(max_time*params_s.vad_fs); %3000
        min_vad = ceil(min_speech*params_s.vad_fs);
        max_vad = ceil(max_speech*params_s.vad_fs);
        
        slices_start = [];
        slices_end = [];
        i_cur = min_vad;
        
        cs_vad = cumsum(vad_vec);
        if cs_vad(end)>min_vad % if enough speech
            smoothing_n = ceil(params_s.vad_fs/10);
            smooth_raw_vad = conv(raw_vad_vec,ones(1,smoothing_n)/smoothing_n,'same');
            while i_cur <= length(cs_vad)
                if isempty(slices_end)            
                    search_start = 1;
                else
                    search_start = slices_end(end)+1;
                end
                search_start = max(search_start, i_cur-max_time_len);
                if (cs_vad(i_cur) - cs_vad(search_start) > min_vad) &&...
                    ((cs_vad(i_cur) - cs_vad(search_start) > max_vad) ||...
                                            smooth_raw_vad(i_cur) < 1)
                        slices_start(end+1) = search_start;
                        slices_end(end+1) = i_cur;
                        i_cur = slices_end(end) + min_vad;
                else
                    i_cur = i_cur + 1;
                end
            end
        end
        
    end

    % reslice data
    function [slices_s, ft_s] = slice_and_shrink_by_vad(ft_s, slices_start, slices_end, params)
                
        slices_s = struct;
        ind_s = [];
        for i_slice = 1:length(slices_start)
            
            if i_slice>1
                % append a new slice
                [slices_s(i_slice), ind_s] = cut_a_slice_by_vad(ft_s,...
                                slices_start(i_slice), slices_end(i_slice), ind_s);
            else 
                [slices_s, ind_s] = cut_a_slice_by_vad(ft_s,...
                                slices_start(i_slice), slices_end(i_slice), ind_s);
            end
                            
            % shrink data in the new slice
            slices_s(i_slice) = shrink_slice_by_vad(slices_s(i_slice),params);
            
        end
        
        % trim start of feature_s
        ft_s = crop_feature_s(ft_s, ind_s, 'after_end');
        
    end

    % shrink vad to slices
    function slice_s = shrink_slice_by_vad(slice_s, params)
        
            % reslicing of all rasta time resolution vectors and matrices
            vad_vec = slice_s.vad_vec;
            vad_vec_512 = interp1(slice_s.t_vec_80, vad_vec,...
                          slice_s.t_vec_512, 'nearest', 'extrap');
            
            if params.shrink_audio
                % reslice signal audio for natural speech
                [new_sig, out_takes, new_t_vec_sig] = shrink_audio_by_vad(...
                                            slice_s.signal, slice_s.t_vec_sig,...
                                           vad_vec, slice_s.t_vec_80, 0.2);

                % reslice signal for only voiced
                [only_voiced, ~] = shrink_audio_by_vad(slice_s.signal, slice_s.t_vec_sig,...
                                           double(slice_s.raw_vad_vec>1), slice_s.t_vec_80, 0.01);
            
                slice_s.signal = new_sig;
                slice_s.signal_only_voiced = only_voiced;
                slice_s.out_takes = [slice_s.out_takes out_takes];
                slice_s.t_vec_sig = new_t_vec_sig;
                            
            end
            
            % delete columns
            del_i = find(vad_vec==0);
            del_i_512 = find(vad_vec_512==0);
            slice_s.vad_vec(del_i) = [];
            slice_s.raw_vad_vec(del_i) = [];
            slice_s.bp_ste_vec(del_i) = [];
            slice_s.t_vec_80(del_i) = [];
            slice_s.spectrum_256(:,del_i) = [];
            slice_s.powspec_256(:,del_i) = [];
            slice_s.rasta_mat(:,del_i) = [];
            slice_s.pure_rasta_mat(:,del_i) = [];
            slice_s.t_vec_512(del_i_512) = [];
            slice_s.powspec_1024(:,del_i_512) = [];
            
    end

    % cut a slice out
    function [slice_s, ind_s] = cut_a_slice_by_vad(ft_s, slice_start, slice_end, prev_ind_s)
            
        % frequency vectors
        slice_s.fs = ft_s.fs;
        slice_s.f_vec_1024 = ft_s.f_vec_1024;
        slice_s.f_vec_256 = ft_s.f_vec_256;

        ind_s = calc_clock_inds(ft_s, slice_start, slice_end);
        t_ind_80 = ind_s.t_ind_80;
        t_ind_512 = ind_s.t_ind_512;
        t_ind_sig = ind_s.t_ind_sig;
        
        % reslicing of all rasta time resolution vectors and matrices
        slice_s.vad_vec = ft_s.vad_vec(t_ind_80);
        slice_s.raw_vad_vec = ft_s.raw_vad_vec(t_ind_80);
        slice_s.bp_ste_vec = ft_s.bp_ste_vec(t_ind_80);
        slice_s.t_vec_80 = ft_s.t_vec_80(t_ind_80);
        slice_s.powspec_256 = ft_s.powspec_256(:,t_ind_80);
        slice_s.spectrum_256 = ft_s.spectrum_256(:,t_ind_80);
        slice_s.rasta_mat = ft_s.rasta_mat(:,t_ind_80);
        slice_s.pure_rasta_mat = ft_s.pure_rasta_mat(:,t_ind_80);

        % reslicing of all 512 time resolution vectors and matrices
        slice_s.t_vec_512 = ft_s.t_vec_512(t_ind_512);
        slice_s.powspec_1024 = ft_s.powspec_1024(:,t_ind_512);
                
        % reslicing signal
        slice_s.signal = ft_s.signal(t_ind_sig);
        slice_s.t_vec_sig = ft_s.t_vec_sig(t_ind_sig);

        % out-takes
        if exist('prev_ind_s','var') &&...
                    ~isempty(prev_ind_s) && isfield(prev_ind_s, 't_ind_sig')
            out_take_first = prev_ind_s.t_ind_sig(end);
        else
            out_take_first = 1;
        end
        slice_s.out_takes = ft_s.signal(out_take_first:t_ind_sig(1));

        % only voiced
        slice_s.signal_only_voiced = [];
        
        % flow type flag
        slice_s.flow_type = 'regular';
        % meta_data
        slice_s.meta_data = ft_s.meta_data;
    end

    function inds_clocks = calc_clock_inds(ft_s, slice_start, slice_end)        
        % calculating the indeces of the clocks
        inds_clocks.t_ind_80 = (slice_start:slice_end);        
        inds_clocks.t_ind_512 = convert_clock_ind(ft_s.t_vec_80,ft_s.t_vec_512,...
                                                    inds_clocks.t_ind_80);
        inds_clocks.t_ind_sig = convert_clock_ind(ft_s.t_vec_80,ft_s.t_vec_sig,...
                                                    inds_clocks.t_ind_80);
        inds_clocks.t_ind_sig = ...
                max(1,inds_clocks.t_ind_sig(1)-40):min(length(ft_s.signal),...
                                                inds_clocks.t_ind_sig(end)+40);
    end

    % converts between different clocks
    function ind_vec_out = convert_clock_ind(t_vec_in, t_vec_out, ind_vec_in)
        [~, start_ind] = min(abs(t_vec_out-t_vec_in(ind_vec_in(1))));
        [~, end_ind] = min(abs(t_vec_out-t_vec_in(ind_vec_in(end))));
        ind_vec_out = start_ind:end_ind;
    end

    % crop feature_s (remove beginnning or ending)
    function [ft_s, discarded_signal] = crop_feature_s(ft_s, ind_s, mode)
        % check for empty t_vecs
        if ~isfield(ft_s,'t_vec_80') || isempty(ft_s.t_vec_80) ||...
                isempty(ft_s.t_vec_512) || isempty(ft_s.t_vec_sig)
            discarded_signal = [];
            return;
        end
        % process input params
        ind_s = parse_ind_s(ind_s);
        
        switch mode
            case 'after_end'
                ind_80 = ft_s.t_vec_80>ft_s.t_vec_80(ind_s.t_ind_80(end));
                ind_512 = ft_s.t_vec_512>ft_s.t_vec_512(ind_s.t_ind_512(end));
                ind_sig = ft_s.t_vec_sig>ft_s.t_vec_sig(ind_s.t_ind_sig(end));
                dis_ind_sig = ft_s.t_vec_sig<=ft_s.t_vec_sig(ind_s.t_ind_sig(end));
                
            case 'before_start'                
                ind_80 = ft_s.t_vec_80<ft_s.t_vec_80(ind_s.t_ind_80(1));
                ind_512 = ft_s.t_vec_512<ft_s.t_vec_512(ind_s.t_ind_512(1));
                ind_sig = ft_s.t_vec_sig<ft_s.t_vec_sig(ind_s.t_ind_sig(1));
                dis_ind_sig = ft_s.t_vec_sig>=ft_s.t_vec_sig(ind_s.t_ind_sig(1));
                
            case 'middle'                
                ind_80 = ft_s.t_vec_80>=ft_s.t_vec_80(ind_s.t_ind_80(1)) &...
                                ft_s.t_vec_80<=ft_s.t_vec_80(ind_s.t_ind_80(end));
                ind_512 = ft_s.t_vec_512>=ft_s.t_vec_512(ind_s.t_ind_512(1)) &...
                                ft_s.t_vec_512<=ft_s.t_vec_512(ind_s.t_ind_512(end));
                ind_sig = ft_s.t_vec_sig>=ft_s.t_vec_sig(ind_s.t_ind_sig(1)) &...
                                ft_s.t_vec_sig<=ft_s.t_vec_sig(ind_s.t_ind_sig(end));
                dis_ind_sig = ft_s.t_vec_sig<ft_s.t_vec_sig(ind_s.t_ind_sig(1)) &...
                                ft_s.t_vec_sig>ft_s.t_vec_sig(ind_s.t_ind_sig(end));
        end
        
        % discarded signal
        discarded_signal = ft_s.signal(dis_ind_sig);        
        
        % trimmed data
        ft_s.signal = ft_s.signal(ind_sig);
        ft_s.t_vec_sig = ft_s.t_vec_sig(ind_sig);
        ft_s.t_vec_512 = ft_s.t_vec_512(ind_512);
        ft_s.t_vec_80 = ft_s.t_vec_80(ind_80);
        ft_s.powspec_1024 = ft_s.powspec_1024(:,ind_512);
        ft_s.powspec_256 = ft_s.powspec_256(:,ind_80);
        ft_s.spectrum_256 = ft_s.spectrum_256(:,ind_80);
        ft_s.rasta_mat = ft_s.rasta_mat(:,ind_80);
        ft_s.pure_rasta_mat = ft_s.pure_rasta_mat(:,ind_80);
        ft_s.vad_vec = ft_s.vad_vec(ind_80);
        ft_s.raw_vad_vec = ft_s.raw_vad_vec(ind_80);
        ft_s.bp_ste_vec = ft_s.bp_ste_vec(ind_80);
        
            % different input types
            function ind_s = parse_ind_s(ind_s)
                if ~isstruct(ind_s) 
                    trim_len = ind_s;
                    % calc clocks for trimmming
                    ind_s = calc_trim_ind_s(trim_len);
                    
                end
                
                    % calculate the different clocks for trimming
                    function ind_s = calc_trim_ind_s(trim_len)
                        ind_s.t_ind_sig = max(1,ceil(length(ft_s.t_vec_sig)-trim_len.*ft_s.fs)):...
                                      length(ft_s.t_vec_sig);    
                        ind_s.t_ind_80 = max(1,ceil(length(ft_s.t_vec_80)-trim_len.*ft_s.fs/80)):...
                                      length(ft_s.t_vec_80);
                        ind_s.t_ind_512 = convert_clock_ind(ft_s.t_vec_80,...
                                            ft_s.t_vec_512,ind_s.t_ind_80);  
                    end
            end
    end

    % shrink audio by vad vector
    function [new_sig, out_takes, new_t_vec_sig] = shrink_audio_by_vad(signal,t_vec_sig,...
                                       vad_vec, t_vec_vad, fade_time)
        start_t_80 = t_vec_vad(diff([0 vad_vec])==1);
        end_t_80 = t_vec_vad(diff([vad_vec 0])==-1);

        start_p = interp1(t_vec_sig, 1:length(t_vec_sig), start_t_80, 'nearest', 'extrap');
        end_p = interp1(t_vec_sig, 1:length(t_vec_sig), end_t_80, 'nearest', 'extrap');
        
        old_sig = signal;
        new_sig = old_sig(start_p(1):end_p(1));
        new_t_vec_sig = t_vec_sig(start_p(1):end_p(1));
        out_takes = old_sig(1:start_p(1));
        len_fade = round(fade_time/(median(diff(t_vec_sig))));
        fade_out = linspace(1, 0, len_fade);
        fade_in = linspace(0, 1, len_fade);
        if length(start_p)>1
            for i_seg = 2:length(start_p)
                add_seg = old_sig(start_p(i_seg):end_p(i_seg));
                add_t_vec = t_vec_sig(start_p(i_seg):end_p(i_seg));
                
                if length(new_sig)>len_fade && length(add_seg)>len_fade
                    len_new_sig = length(new_sig);      
                    new_sig((len_new_sig-len_fade+1):len_new_sig) =...
                                        fade_out.*new_sig((len_new_sig-len_fade+1):len_new_sig)+...
                                        fade_in.*add_seg(1:len_fade);  
                                    
                    new_sig((len_new_sig+1):(len_new_sig+length(add_seg)-len_fade)) =...
                                        add_seg((len_fade+1):end);
                    
                    % t_vec
                    new_t_vec_sig((len_new_sig+1):(len_new_sig+length(add_seg)-len_fade)) =...
                                        add_t_vec((len_fade+1):end);
                else 
                    s_len_fade = min(length(new_sig), length(add_seg));
                    s_fade_out = linspace(1, 0, s_len_fade);
                    s_fade_in = linspace(0, 1, s_len_fade);
                    len_new_sig = length(new_sig);      
                    new_sig((len_new_sig-s_len_fade+1):len_new_sig) =...
                                        s_fade_out.*new_sig((len_new_sig-s_len_fade+1):len_new_sig)+...
                                        s_fade_in.*add_seg(1:s_len_fade);
                                    
                    new_sig((len_new_sig+1):(len_new_sig+length(add_seg)-s_len_fade)) =...
                                        add_seg((s_len_fade+1):end);
                                    
                    % t_vec
                    new_t_vec_sig((len_new_sig+1):(len_new_sig+length(add_seg)-s_len_fade)) =...
                                        add_t_vec((s_len_fade+1):end);
                end
                
                % update outtakes
                out_take_seg = old_sig(end_p(i_seg-1):start_p(i_seg));
                out_takes((end+1):(end+length(out_take_seg))) = out_take_seg;
            end
        end
        out_takes = [out_takes, old_sig(end_p(end):end)];
        % make the clocks line up to each other
        new_t_vec_sig(end) = t_vec_sig(end);
    end

    % get only the results from the slices_s 
    function results_s = get_slices_results_struct(slices_s, params_s)
        
        try
            t_start = cellfun(@(x)(x(1)), {slices_s.t_vec_80},'UniformOutput',0);
            t_dur = cellfun(@(x)(x(end)-x(1)), {slices_s.t_vec_80},'UniformOutput',0);
        catch ex
            t_start = [];
            t_dur = [];
        end
        
        results_s = struct('start',t_start,'duration',t_dur);
        
        % fields to copy
        fields_to_copy = {'status'...
            ,'temper_2d','temper_2d_group','temper_2d_enum'...
            ,'energy_scale','energy_group','energy_enum','composure','ciCallService'...
            ,'coopLevel1','t1','t2','t3','t4','energyVal','energyEnum'...
            ,'phrase_1_id','phrase_2_id','phrase_1','phrase_2'...
            ,'moodgroup11_1','moodgroup11_2'...
            ,'highMed','lowMed','flow_1','flow_2'...
            ,'vad_sum_tot','vad_sum_voiced','flow_type'...
            ,'cooperation', 'servicetone', 'salestone'...
            ,'gender_val','gender_group','gender_conf'...
            ,'valence_val','valence_group','valence_conf'...
            ,'arousal_val','arousal_group','arousal_conf'...
            ,'temper_val','temper_group','temper_conf','temper_group_recalc'...
            ,'audio_quality_val','audio_quality_group'...
            ,'audio_quality_conf','audio_quality_class'...
            ,'trend_overall_val','trend_overall_group','trend_overall_conf'...
            ,'trend_improvement_val','trend_improvement_group','trend_improvement_conf'...
            ,'total_voice_content'...            
            ,'num_segments','temper_mean','temper_mode','temper_mode_percent'...
            ,'valence_mean','valence_mode','valence_mode_percent'...
            ,'arousal_mean','arousal_mode','arousal_mode_percent'...
            ,'gender_mean','gender_mode','gender_mode_percent'...
            ,'quality_mean','quality_mode','quality_mode_percent'...
            ,'quality_class_mode','quality_class_mode_percent'...
            ,'moodgroup11_lone_pct','moodgroup11_crea_pct','moodgroup11_host_pct'...
            ,'moodgroup11_defe_pct','moodgroup11_sadn_pct','moodgroup11_self_pct'...
            ,'moodgroup11_love_pct','moodgroup11_frie_pct','moodgroup11_supr_pct'...
            ,'moodgroup11_crit_pct','moodgroup11_lead_pct'...
            }; 
%             'flow_1','flow_2','flow_3','flow_4','flow_5','flow_6','flow_7','flow_8',...
%             'p_1','p_2','p_3','p_4','p_5','p_6','p_7','p_8','p_9'...
%             };        
        results_s = copy_fields(slices_s, results_s, fields_to_copy);
        
        % fields to add 
        % meta_data
        if isfield(params_s, 'meta_data')
            [results_s.meta_data] = deal(params_s.meta_data);
        end
    end

    % performs analyses on slices
    function slices_s = analyze_slices(slices_s, params_s)
        if any(strcmp('vad_sum', params_s.analyses_types))
            slices_s = calc_vad_sum_on_slices(slices_s ,params_s);
        end
        
        if any(strcmp('tonetest', params_s.analyses_types))
            slices_s = calc_avg_spectrum_tones(slices_s, params_s);        
        
            if any(strcmp('cooperation', params_s.analyses_types))
                slices_s = calc_cooperation(slices_s, params_s);    
            end
            if any(strcmp('servicetone', params_s.analyses_types))
                slices_s = calc_servicetone(slices_s, params_s);        
            end
            if any(strcmp('salestone', params_s.analyses_types))
                slices_s = calc_salestone(slices_s, params_s);        
            end            
        end
        
        if any(strcmp('temper_2d', params_s.analyses_types))
            slices_s = classify_temper_2d_on_slices(slices_s, params_s);
        end
        
        if any(strcmp('composure', params_s.analyses_types))
            slices_s = classify_composure_on_slices(slices_s, params_s);
        end 
        
        if any(strcmp('audio_quality', params_s.analyses_types))
            slices_s = classify_audio_quality_on_slices(slices_s, params_s);
        end
        
        if any(strcmp('valence_arousal_temper_gender', params_s.analyses_types))
            slices_s = classify_vatg_on_slices(slices_s, params_s);
        end 
        
        if any(strcmp('phrases_info',params_s.analyses_types))
            slices_s = calc_phrases_info(slices_s, params_s);    
        end        
        
        if any(strcmp('analyzer_dll',params_s.analyses_types))
            slices_s = calc_analyzer_dotnet_link(slices_s, params_s);
        end        
        
        if any(strcmp('basic_stats', params_s.summary_analyses_types))
            slices_s = calc_summary_basic_stats(slices_s, params_s);
        end
    end

    % performs analyses on slices
    function slices_s = calc_summary_analysis(slices_s, feature_s, params_s)
        
        if isempty(slices_s)
            return;
        end        
        slices_s(end+1) = slices_s(end);
        fields = fieldnames(slices_s);
        for i_field = 1:numel(fields)
            slices_s(end).(fields{i_field}) = [];
        end
        slices_s(end).flow_type = 'summary';
        
        if any(strcmp('total_voice_content', params_s.summary_analyses_types))
            slices_s = calc_total_voice_content(slices_s, feature_s, params_s);
        end        
        
        if any(strcmp('call_trend', params_s.summary_analyses_types))
            slices_s = calc_call_trend_on_results_struct(slices_s, params_s);
        end        
        
        if any(strcmp('basic_stats', params_s.summary_analyses_types))
            slices_s = calc_summary_basic_stats(slices_s, params_s);
        end
    end

    % calculates call trend analysis for a results array
    function slices_s = calc_call_trend_on_results_struct(slices_s, params_s)
        run_libs();
        
        % res struct
        results_s = get_slices_results_struct(slices_s, params_s);
        results_s = apply_summary_quality_policy(results_s, params_s);
        
        % calc
        if ~isempty(slices_s) && ~isempty(results_s) && ...
                                isfield(results_s, 'valence_val')
            % classification
            class_res = cls_fh.classify_call_trend(results_s);            

            [slices_s.trend_overall_val] = deal(class_res.overall.val);
            [slices_s.trend_overall_group] = deal(class_res.overall.group);
            [slices_s.trend_overall_conf] = deal(class_res.overall.conf);
            [slices_s.trend_improvement_val] = deal(class_res.improvement.val);
            [slices_s.trend_improvement_group] = deal(class_res.improvement.group);
            [slices_s.trend_improvement_conf] = deal(class_res.improvement.conf);
            
        end
        
    end

    % calculates total voice content
    function slices_s = calc_total_voice_content(slices_s, feature_s, params_s)
        params_s.slicing_progress_flag = 1;
        params_s.analyses_types = {'vad_sum'};
        
        slicing_progress = update_slicing_progress(feature_s, params_s);      
        
        if ~isfield(slices_s, 'vad_sum_tot')
            slices_s = calc_vad_sum_on_slices(slices_s, params_s);
        end
        
        if ~isempty(slices_s)
            [slices_s.total_voice_content] = deal(sum([slices_s.vad_sum_tot]) + ...
                                            params_s.min_speech - slicing_progress);
        else
            slices_s = struct('total_voice_content', ...
                            params_s.min_speech - slicing_progress);
        end
    end

    % calculates total voice content
    function slices_s = calc_summary_basic_stats(slices_s, params_s)
        run_libs();
        
        % res struct
        results_s = get_slices_results_struct(slices_s, params_s);
        results_s = apply_summary_quality_policy(results_s, params_s);
        
        % calc
        if ~isempty(slices_s) && ~isempty(results_s) && ...
                                isfield(results_s, 'valence_val')            
            % classification
            class_res = cls_fh.calc_summary_basic_stats(results_s);            
            class_res_fields = fieldnames(class_res);
            for i=1:numel(class_res_fields)
                [slices_s.(class_res_fields{i})] = deal(class_res.(class_res_fields{i}));
            end            
        end
    end

    % filters out results not matching quality criteria
    function results_s = apply_summary_quality_policy(results_s, params_s)
        if ~isempty(results_s) && isfield(results_s(1), 'audio_quality_group') && ...
                ~isempty(params_s.summary_audio_quality_policy)
            inds_in = false(size(results_s)); 
            for i=1:numel(params_s.summary_audio_quality_policy)
                inds_in(strcmp({results_s.audio_quality_group},...
                               params_s.summary_audio_quality_policy{i})) = true;
            end
            results_s(~inds_in) = [];
        end
    end        

    % calculates vad_sum for a slice stream
    function slices_s = calc_vad_sum_on_slices(slices_s, params_s)
        % calc
        for i_slice = 1:length(slices_s)
            if ~isfield(slices_s,'vad_sum') || isempty(slices_s(i_slice).vad_sum)
                slices_s(i_slice).vad_sum_tot = sum(slices_s(i_slice).vad_vec)./params_s.vad_fs;
                slices_s(i_slice).vad_sum_voiced = sum(double(slices_s(i_slice).raw_vad_vec>1))./params_s.vad_fs;
            end
        end
    end

    % classifies temper for a slice stream
    function slices_s = classify_temper_2d_on_slices(slices_s,params_s)
        run_libs();
        
        % calc
        for i_slice = 1:length(slices_s)
            if ~isfield(slices_s,'temper_2d') || isempty(slices_s(i_slice).temper_2d)
                    
                % data
                slice_rasta = slices_s(i_slice).rasta_mat;  
                f_vec = slices_s(i_slice).f_vec_256;
                t_vec = slices_s(i_slice).t_vec_80;

                % calculations
                [temper_val, val_vec, p_vec] = cls_fh.classify_temper_2d({slice_rasta, f_vec, t_vec});
                
                slices_s(i_slice).temper_2d = temper_val;
                
                [slices_s(i_slice).temper_2d_group,...
                   slices_s(i_slice).temper_2d_enum] = cls_fh.calc_temper_group_on_temper(...
                                                temper_val,params_s.temper_low_cutoff,...
                                                params_s.temper_high_cutoff);
                
                % old temper (energy) calculations
                slices_s(i_slice).energy_scale = cls_fh.energy_scale(val_vec(1), val_vec(2), 0);
                [slices_s(i_slice).energy_group,...
                   slices_s(i_slice).energy_enum] = cls_fh.calc_temper_group_on_temper(...
                                                slices_s(i_slice).energy_scale , 25, 75);             
            end
        end
    end

    % classifies temper for a slice stream
    function slices_s = classify_vatg_on_slices(slices_s, params_s)
        run_libs();
        
        % calc
        for i_slice = 1:length(slices_s)
            if ~isfield(slices_s,'gender_val') || isempty(slices_s(i_slice).gender_val)
                    
                % data
                
                % calculations
                res_struct = cls_fh.classify_vatg(slices_s(i_slice));  
                
                % gender
                slices_s(i_slice).gender_val = res_struct.gender.val;
                slices_s(i_slice).gender_group = res_struct.gender.group;
                slices_s(i_slice).gender_conf = res_struct.gender.conf;
                
                % arousal
                slices_s(i_slice).arousal_val = res_struct.arousal.val;
                slices_s(i_slice).arousal_group = res_struct.arousal.group;
                slices_s(i_slice).arousal_conf = res_struct.arousal.conf;
                
                % valence
                slices_s(i_slice).valence_val = res_struct.valence.val;
                slices_s(i_slice).valence_group = res_struct.valence.group;
                slices_s(i_slice).valence_conf = res_struct.valence.conf;
                
                % va_temper
                slices_s(i_slice).temper_val = res_struct.va_temper.val;
                slices_s(i_slice).temper_group = res_struct.va_temper.group;
                slices_s(i_slice).temper_conf = res_struct.va_temper.conf;
               
                % recalc group accoring to defined params
                [slices_s(i_slice).temper_group_recalc, ~] = cls_fh.calc_temper_group_on_temper(...
                                                slices_s(i_slice).temper_val,...
                                                params_s.temper_low_cutoff,...
                                                params_s.temper_high_cutoff);                
            end
        end
        
    end

    % classifies temper for a slice stream
    function slices_s = classify_audio_quality_on_slices(slices_s, params_s)
        run_libs();
        
        % calc
        for i_slice = 1:length(slices_s)
            if ~isfield(slices_s,'audio_quality_val') || isempty(slices_s(i_slice).audio_quality_val)
                    
                res_struct = cls_fh.classify_audio_quality(slices_s(i_slice));
                
                % audio_quality
                slices_s(i_slice).audio_quality_val = res_struct.audio_quality.val;
                slices_s(i_slice).audio_quality_group = res_struct.audio_quality.group;
                slices_s(i_slice).audio_quality_conf = res_struct.audio_quality.conf;
                slices_s(i_slice).audio_quality_class = res_struct.audio_quality.max_prob_class;
            end
        end
        
    end

    % classifies composure for a slice stream
    function slices_s = classify_composure_on_slices(slices_s,params_s)
        run_libs();
        
        % calc
        prev_rasta = [];
        prev_t_vec = [];
        for i_slice = 1:length(slices_s)
            % data
            slice_rasta = slices_s(i_slice).rasta_mat;
            comb_rasta = [prev_rasta, slice_rasta];    
            f_vec = slices_s(i_slice).f_vec_256;
            t_vec = slices_s(i_slice).t_vec_80;
            if ~isfield(slices_s,'composure') || isempty(slices_s(i_slice).composure)
                % calculations
                [comp_val, ~, ~] = cls_fh.classify_composure({comb_rasta, f_vec, [prev_t_vec t_vec]});
                slices_s(i_slice).composure = comp_val; 
            end
            prev_rasta = slice_rasta;
            prev_t_vec = t_vec;
        end
    end

    function slices_s = calc_cooperation(slices_s, params_s)
        
        for i_slice = 1:length(slices_s)
            if isfield(slices_s,'t1') && ~isempty(slices_s(i_slice).t1)
                
                slices_s(i_slice).cooperation = cls_fh.classify_cooperation(...
                    [   slices_s(i_slice).t1,...
                        slices_s(i_slice).t2,...
                        slices_s(i_slice).t3,...
                        slices_s(i_slice).t4...
                        ]);
            end
        end
        
    end

    function slices_s = calc_salestone(slices_s, params_s)

        for i_slice = 1:length(slices_s)
            if isfield(slices_s,'t1') && ~isempty(slices_s(i_slice).t1)
                
                slices_s(i_slice).salestone = cls_fh.classify_salestone(...
                    [   slices_s(i_slice).t1,...
                        slices_s(i_slice).t2,...
                        slices_s(i_slice).t3,...
                        slices_s(i_slice).t4...
                        ]);
            end
        end        
        
    end

    function slices_s = calc_servicetone(slices_s, params_s)
        for i_slice = 1:length(slices_s)
            if isfield(slices_s,'t1') && ~isempty(slices_s(i_slice).t1)
                
                slices_s(i_slice).servicetone = cls_fh.classify_servicetone(...
                    [   slices_s(i_slice).t1,...
                        slices_s(i_slice).t2,...
                        slices_s(i_slice).t3,...
                        slices_s(i_slice).t4...
                        ]);
            end
        end        
    end

    % link to analyzer calculations
    function slices_s = calc_analyzer_dotnet_link(slices_s, params_s)
        slices_s = calc_analyzer_dotnet(slices_s);
    end

    % add tones
    function slices_s = calc_avg_spectrum_tones(slices_s, params_s)
        run_libs();
        
        % calc
        for i_slice = 1:length(slices_s)
            if ~isfield(slices_s,'t1') || isempty(slices_s(i_slice).t1)
                [t1, t2, t3, t4] = cls_fh.calc_tonetest_on_powspec(...
                                        slices_s(i_slice).powspec_1024,...
                                        slices_s(i_slice).f_vec_1024);
                slices_s(i_slice).t1 = t1;
                slices_s(i_slice).t2 = t2;
                slices_s(i_slice).t3 = t3;
                slices_s(i_slice).t4 = t4;
            end
        end
    end

    % add p
    function slices_s = calc_phrases_info(slices_s, params_s)
        run_libs();
        
        % calc
        for i_slice = 1:length(slices_s)
            if (~isfield(slices_s,'phrase_1_id') || isempty(slices_s(i_slice).phrase_1_id))
                if isfield(slices_s,'t1') && ~isempty(slices_s(i_slice).t1) && ...
                     isfield(slices_s,'temper_group') && ~isempty(slices_s(i_slice).temper_group)
                            
                        class_res = cls_fh.calc_phrases_info(slices_s(i_slice));

                        slices_s(i_slice).phrase_1_id = class_res.phrase_1_id;
                        slices_s(i_slice).phrase_2_id = class_res.phrase_2_id;
                        slices_s(i_slice).phrase_1 = class_res.phrase_1;
                        slices_s(i_slice).phrase_2= class_res.phrase_2;
                        slices_s(i_slice).moodgroup11_1 = class_res.moodgroup11_1;
                        slices_s(i_slice).moodgroup11_2 = class_res.moodgroup11_2;
                end
            end
        end
    end

    % preprocess 
    function signal = custom_preprocess(signal, fs, setting_str)
        run_libs()
        switch setting_str
           case 'anti_atnt'
                f_x = [0,128,198,277,412,687,846,1144,1452,1881,2356,2990,3205,...
                        3503,3713,4000];
                f_y = [0,0,3.08,5.1,3.47,-1.67,-3.5,-4.3,-4.08,-7.16,-8.62,...
                    -12,-15.11,-20.4,-23.1,-23.3];

                signal = wsola_lib_fh.fir_filter(signal, fs, f_x, f_y, 128);
       end
    end

    

end % lib









