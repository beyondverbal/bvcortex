classdef NanIndexable < double   
    methods
        function obj = NanIndexable(data)
            obj= obj@double(data);
        end
        
        function objnew = subsref(obj,S)
            AllInds = S.subs{1};
            NanMap = isnan(AllInds);
            AllInds(NanMap) = 1;
            S.subs{1} = AllInds;
            
            pobj = double(obj);
            pobj = pobj(AllInds);
            pobj(NanMap) = nan;
            
            objnew = NanIndexable(pobj);
        end
        
        function display(obj)
            
            l = inputname(1);
            if isempty(l)
                l='ans'; 
            end
            
            disp ' '
            disp([l ' = ']);
            disp ' '
            disp(double(obj));
            
        end
        
    end
    
end 