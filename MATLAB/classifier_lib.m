function varargout = classifier_lib(varargin)
% library file for different feature manipulation functions
  
feature_fh = [];  
flow_mgr_fh = [];  
stats_utils_fh = [];
tt_lib = [];

persistent flows; 
flows = add_rasta_partial_flows(flows);
flows.saved_files_path = regexprep(mfilename('fullpath'),...
        'util functions\\classifier_lib','classifier data');

f_handles = struct(... % recommended way to pass nested func handles
		'calculate_flow_values_on_feature_struct', @calculate_flow_values_on_feature_struct, ... %YL
        'preload_models_by_config', @preload_models_by_config,...
        'high_med_energy', @high_med_energy,...
        'low_med_energy', @low_med_energy,...
        'vad_ssnr_energy', @vad_ssnr_energy,...
        'raw_vad_vec_on_rastacell', @raw_vad_vec_on_rastacell,...
        'vad_vec_on_raw_vad_vec', @vad_vec_on_raw_vad_vec,...
        'vad_rasta_mat2vec', @vad_rasta_mat2vec,...
        'post_vad_volume_separation_on_ste',@post_vad_volume_separation_on_ste,...
        'bp_ste_on_powspeccell',@bp_ste_on_powspeccell,...
        'energy_scale', @energy_scale,... 
        'low_med_norm_scale', @low_med_norm_scale,...
        'high_med_norm_scale', @high_med_norm_scale,...
        'calc_tonetest_on_powspec', @calc_tonetest_on_powspec,...
        'rasta_energy_classifiers', @rasta_energy_classifiers,...
        'calc_segments', @calc_segments,...
        'classify_rasta_stream', @classify_rasta_stream,...
        'classify_composure', @classify_composure,...
        'classify_composure_on_feature_mat', @classify_composure_on_feature_mat,...
        'classify_temper_2d', @classify_temper_2d,...
        'classify_temper_2d_on_feature_mat', @classify_temper_2d_on_feature_mat,...
        'classify_vatg', @classify_vatg,...
        'classify_audio_quality', @classify_audio_quality,...
        'classify_call_trend', @classify_call_trend,...
        'calc_summary_basic_stats', @calc_summary_basic_stats,...
        'calc_phrases_info', @calc_phrases_info,...
        'calc_temper_group_on_temper', @calc_temper_group_on_temper,...
        'classify_cooperation', @classify_cooperation,...
        'classify_servicetone', @classify_servicetone,...
        'classify_salestone', @classify_salestone,...
        'calculate_flow_values_on_feature_mat', @calculate_flow_values_on_feature_mat,...
        'calculate_naive_bayes_prob', @calculate_naive_bayes_prob,...
        'classify_treebagger_quick_predict', @classify_treebagger_quick_predict,...        
        'apriori_helper_calc_from_ginput', @apriori_helper_calc_from_ginput,...        
        'smooth_vad_silences', @smooth_vad_silences ...
        );
     
varargout{1} = f_handles;


%%% FUNCTIONS

    % run libs
    function run_libs
        if isempty(feature_fh)
            feature_fh = feature_lib();     
        end
        if isempty(flow_mgr_fh)
            flow_mgr_fh = flow_management_lib();     
        end
        if isempty(stats_utils_fh)
            stats_utils_fh = stats_utils_lib();     
        end
        if isempty(tt_lib)
            tt_lib = tone_test_lib();     
        end
    end

    % run libs
    function preload_models_by_config(analyses_cell)
        if any(strcmpi(analyses_cell,'temper_2d'))
            flows = add_temper_2d_bayes_mix(flows);
        end
        if any(strcmpi(analyses_cell,'composure'))
            flows = add_composure_bayes_mix(flows);
        end
        if any(strcmpi(analyses_cell,'valence_arousal_temper_gender'))
            flows = add_vatg_models(flows);
        end
        if any(strcmpi(analyses_cell,'audio_quality'))
            flows = add_audio_quality_model(flows);
        end
        if any(strcmpi(analyses_cell,'call_trend'))
            flows = add_call_trend_model(flows);
        end
        if any(strcmpi(analyses_cell,'phrases_info'))
            flows = add_phrases_table(flows);
        end
    end

    %%% PUBLIC

    % high med energy classifier
    function [val, cat_string] = high_med_energy(signal, fs)
        c_s = rasta_energy_classifiers(signal, fs, 'high-med');
        val = c_s.high_med_val;
        cat_string = c_s.high_med_cat;
    end

    % low med energy classifier
    function [val, cat_string] = low_med_energy(signal, fs)
        c_s = rasta_energy_classifiers(signal, fs, 'low-med');
        val = c_s.low_med_val;
        cat_string = c_s.low_med_cat;
    end

    % vad ssnr classifier
    function [val, cat_string] = vad_ssnr_energy(signal, fs)
        c_s = rasta_energy_classifiers(signal, fs, 'vad');
        val = c_s.vad_ssnr_val;
        cat_string = c_s.vad_ssnr_cat;
    end

    % calculate raw VAD vector
    function raw_vad_vec = raw_vad_vec_on_rastacell(rasta_cell)            
%         run_libs();
        feature_fh = feature_lib();
%         warning('run libs removed here');
        % OLD
%         old_vad_on_rasta.mat2mat = {'band_pass'}; 
%         old_vad_on_rasta.mat2vec = {'80/20pctl',1};
%         old_vad_on_rasta.vec2vec = {'log_abs'};
%         old_vad_on_rasta.threshold = 3.5;
%         old_vad_on_rasta.categories = {'nospeech','speech'};
%         output = feature_fh.process_feature_flow(old_vad_on_rasta, 'mat', rasta_cell, 'vec');
%         raw_vad_vec = output{1}./old_vad_on_rasta.threshold;
%         
        output = feature_fh.process_feature_flow(flows.vad_on_rasta, 'mat', rasta_cell, 'vec');
        raw_vad_vec = output{1}./flows.vad_on_rasta.threshold;
    end

    % calculate VAD vector on raw_vad_vec data
    function vad_vec = vad_vec_on_raw_vad_vec(raw_vad_vec, t_vec)
        raw_vad_bool = double(raw_vad_vec>1);
% 
%         min_silence_time = 1; %1
%         avg_silence_threshold = 0.1; %0.1
%         vad_vec = smooth_vad_silences(raw_vad_bool, t_vec, min_silence_time, avg_silence_threshold);
         
        min_silence_time = 1; %0.75
        avg_silence_threshold = 0.1; %0.2

        % vowel smoothing
        vad_vec = smooth_vad_silences(raw_vad_bool, t_vec, 0.1, 0.6);
        % speech flow smoothing
        vad_vec = smooth_vad_silences(vad_vec, t_vec, min_silence_time, avg_silence_threshold);        
    end

    % vad after rasta
    function [vad_vec, raw_vad_vec, t_vec] = vad_rasta_mat2vec(rasta_mat, f_vec, t_vec)
        raw_vad_vec = raw_vad_vec_on_rastacell({rasta_mat, f_vec, t_vec});
        vad_vec = vad_vec_on_raw_vad_vec(raw_vad_vec, t_vec);
    end

    % post vad processing based on short time energy - speech noise reduction
    function vad_vec = post_vad_volume_separation_on_ste(vad_vec, t_vec, ste_vec,...
                                                        hist_ste_vec, crit_snr)
        % calc threshold using historic values as well
        threshold = (quick_prctile([hist_ste_vec ste_vec],95)./10^(crit_snr/10));
        
        % erasing vad samples lacking volume   
        vad_vec(ste_vec < threshold) = 0;
        % smoothing for speech flow again
%         vad_vec = smooth_vad_silences(vad_vec, t_vec, 0.75, 0.2);
        vad_vec = smooth_vad_silences(vad_vec, t_vec, 1, 0.1);
    end

    % calculates band passed short time energy vector on
    % powerspecgrum cell
    function bp_ste_vec = bp_ste_on_powspeccell(powspeccell, low_f, high_f)
        if ~exist('low_f','var')
            low_f = 100;
        end
        if ~exist('high_f','var')
            high_f = 2000;
        end

        bp_ste_vec = sum(powspeccell{1}(...
                    powspeccell{2}>low_f & powspeccell{2}<high_f, :),1);
    end

    % vad ssnr classifier
    function energy_scale = energy_scale(high_med_val, low_med_val, norm_mode)
        energy_scale = energy_scale_calc(high_med_val, low_med_val, norm_mode);
    end

    % rasta energy classifiers
    function c_struct = rasta_energy_classifiers(input_cell, to_do_cell)
        run_libs();
        
        switch numel(input_cell)
            case 2 % signal
                % get rasta mat
                signal = input_cell{1};
                fs = input_cell{2};
                [out_mat, f_vec, t_vec] = feature_fh.sig2mat_transform(signal, fs, 'specgram_256');
                [out_mat, f_vec, t_vec] = feature_fh.mat2mat_serial(...
                                                out_mat, f_vec, t_vec, {'rasta_vad'});
            case 3 % rasta
                out_mat = input_cell{1};
                f_vec = input_cell{2};
                t_vec = input_cell{3};
        end
        
        % init output struct
        c_struct.high_med_val = [];
        c_struct.high_med_cat = [];
        c_struct.low_med_val = [];
        c_struct.low_med_cat = [];
        c_struct.vad_ssnr_val = [];
        c_struct.vad_ssnr_cat = [];
        c_struct.energy_scale = [];
        c_struct.lm_norm = [];
        c_struct.hm_norm = [];
        
        % high-med
        if any(strcmpi(to_do_cell,'high-med')) || any(strcmpi(to_do_cell,'energy-scale'))
            output = feature_fh.process_feature_flow(flows.high_med_flow, 'mat', {out_mat, f_vec, t_vec}, 'val');
            [c_struct.high_med_val, c_struct.high_med_cat] = deal(output{:});
                   
        end
        
        % low-med
        if any(strcmpi(to_do_cell,'low-med')) || any(strcmpi(to_do_cell,'energy-scale'))
            output = feature_fh.process_feature_flow(flows.low_med_flow, 'mat', {out_mat, f_vec, t_vec},'val');
            [c_struct.low_med_val, c_struct.low_med_cat] = deal(output{:});
                   
        end
        
        % energy-scale
        if any(strcmpi(to_do_cell,'energy-scale'))
            if ~isempty(c_struct.high_med_val) && ~isempty(c_struct.low_med_val)
                [c_struct.energy_scale, c_struct.hm_norm, c_struct.lm_norm] = ...
                        energy_scale_calc(c_struct.high_med_val, c_struct.low_med_val);
            end
        end
        
        % vad
        if ~isempty(strfind(to_do_cell,'vad'))
            output = feature_fh.process_feature_flow(flows.vad_ssnr_flow, 'mat', {out_mat, f_vec, t_vec},'val');
            [c_struct.vad_ssnr_val, c_struct.vad_ssnr_cat] = deal(output{:});                   
        end
                
    end %rasta_energy_classifiers

    % classifys a ready rasta matrix
    function c_struct = classify_rasta_stream(input_cell, buff_length,...
                                                    overlap_ratio, to_do_cell)
        rasta_mat = input_cell{1};
        f_vec = input_cell{2};
        t_vec = input_cell{3};
        
        stream_s = calc_segments(t_vec, 1./mode(diff(t_vec)), buff_length, overlap_ratio);
        
        % calc classifier values
        for i=1:length(stream_s)
            ind_vec = stream_s(i).signal_ind_vec;
            c_struct(i) = rasta_energy_classifiers(...
                        {rasta_mat(:,ind_vec), f_vec, t_vec(ind_vec)}, to_do_cell);
        end
        
        % add indices and times fields
        for i=1:length(stream_s)
            ind_vec = stream_s(i).signal_ind_vec;
            c_struct(i).signal_ind_vec = ind_vec;
            c_struct(i).t_start = t_vec(ind_vec(1));
            c_struct(i).t_end = t_vec(ind_vec(end));
        end
        
    end

    % turns various input cells into feature mat type input cell
    function feature_cell = input_cell_to_feature_cell(input_cell, flows_struct)
        run_libs();
        switch feature_fh.input_cell_type(input_cell)
            case 'sig'
                feature_cell = calculate_feature_mat_on_signal(input_cell, flows_struct);
            case 'spec_mat'
                feature_cell = calculate_feature_mat_on_rasta(input_cell, flows_struct);
            case 'feature_mat'
                feature_cell = input_cell;
        end
    end

    % calc feature mat
    function output_cell = calculate_feature_mat_on_signal(input_cell, flows_struct)
        partial_flow = flows_struct(1);
        output_cell = feature_fh.process_feature_flow(partial_flow, 'sig',...
                                input_cell, 'mat');
    end

    % calc feature mat
    function output_cell = calculate_feature_mat_on_rasta(input_cell, flows_struct)
        partial_flow = flows_struct(1);
        partial_flow.mat2mat = partial_flow.mat2mat(end);
        output_cell = feature_fh.process_feature_flow(partial_flow, 'mat',...
                                input_cell, 'mat');
    end

    % calculate flow values
    function val_vec = calculate_flow_values_on_feature_mat(input_cell, flows_struct)
        % calc flow values for classification
        val_vec = zeros(1,numel(flows_struct));
        for flow_i = 1:numel(flows_struct)
            partial_flow = flows_struct(flow_i);
            partial_flow.mat2mat = {'none'};
            out_cell = ...
                feature_fh.process_feature_flow(partial_flow, 'mat',...
                                                                input_cell,'val');
            val_vec(flow_i) = out_cell{1};
        end
    end

    % calculate flow values
    function [val_vec, val_ind_vec, feat_struct] = calculate_flow_values_on_feature_struct(...
                                                    feat_struct, flows_struct)
        run_libs();
        
        if (nargout>=3)  || ...
                (isfield(feat_struct, 'feat_cache_values') &&...
                        ~isempty(feat_struct.feat_cache_values))
            % feat struct is returned
            cache_flag = true;
        else
            cache_flag = false;
        end
        % init feature memory if does not exist or is corrupt
        if cache_flag && (~isfield(feat_struct, 'feat_cache_values')...
                        || ~isfield(feat_struct, 'feat_cache_inds') ...
                        || ~isfield(feat_struct, 'feat_cache_names') ...
                    || numel(feat_struct.feat_cache_values)~=numel(feat_struct.feat_cache_names))
            feat_struct.feat_cache_values = [];
            feat_struct.feat_cache_inds = [];
            feat_struct.feat_cache_names = {};
        end
            
        % calc flow values for classification
        val_vec = zeros(1,numel(flows_struct));
        val_ind_vec = zeros(1,numel(flows_struct));
        input_cell = {feat_struct, fieldnames(feat_struct), 0};
        for flow_i = 1:numel(flows_struct)
            cur_flow = flows_struct(flow_i);
            if cache_flag && ...
                        any(strcmp(cur_flow.flow_str, feat_struct.feat_cache_names))
                ind_match = find(...
                        strcmp(cur_flow.flow_str, feat_struct.feat_cache_names),1);
                val_vec(flow_i) = feat_struct.feat_cache_values(ind_match);
%                 val_ind_vec(flow_i) = feat_struct.feat_cache_inds(ind_match);                                                                   
            else
                % calc feature
                out_cell = feature_fh.process_feature_flow(cur_flow, 'mat', input_cell,'val');
            
                val_vec(flow_i) = out_cell{1};
%                 if isfield(flows_struct,'edges_vec')
%                     [~, val_ind_vec(flow_i)] =  min(abs(...
%                                                     flows_struct(flow_i).edges_vec...
%                                                         - val_vec(flow_i))); 
%                 end
                                       
                if cache_flag                                    
                    % add to feature memory
                    feat_struct.feat_cache_names{end + 1} = cur_flow.flow_str;
                    feat_struct.feat_cache_values(end + 1) = val_vec(flow_i);                
                    feat_struct.feat_cache_inds(end + 1) = val_ind_vec(flow_i);                
                end
            end                
        end
    end
    
    % calculate naive bayes probabilities
    function [p_vec, sum_p] = calculate_naive_bayes_prob(groups_s, flows_s,...
                                                            val_vec, val_ind_vec, f_inds)                                                        

        if nargin<=3            % faster than: if ~exist('val_ind_vec','var') || isempty(val_ind_vec) 
            val_ind_vec = zeros(size(val_vec));
        end
        if nargin<=4            % faster than: if ~exist('f_inds','var') || isempty(f_inds) 
            f_inds = 1:numel(flows_s);
        end
        n_f_inds = numel(f_inds);
        n_groups = numel(groups_s);        
        
        p_mat = zeros(n_f_inds, n_groups);
%         near_inds = zeros(size(f_inds));
        % look up probablity for each flow and each group         
        for flow_i = 1:n_f_inds    
            
            % if we got no cached index we calculate it
            if ~val_ind_vec(f_inds(flow_i))
                [~, val_ind_vec(f_inds(flow_i))] =  min(abs(flows_s(f_inds(flow_i)).edges_vec...
                                                        - val_vec(f_inds(flow_i))));
            end
                                                    
            % assign probability into the probability matrix
            for g_ind = 1:n_groups
                p_mat(flow_i, g_ind) = groups_s(g_ind).pdf_mat(...
                                        f_inds(flow_i),val_ind_vec(f_inds(flow_i)));
            end
        end
        p_mat(p_mat<0)=0;
        
        % only features
        p_vec = prod(p_mat+eps,1);
        
        % final posterior
        p_vec = p_vec.*[groups_s.apriori_vec];
        
        % sum of unnormalized probabilities
        sum_p = sum(p_vec);
        
        %normalize
        p_vec = p_vec./sum_p;  
        
%         debug_plots();

            % debug plots
            function debug_plots()
                % comparative:
                figure; imagesc(((p_mat./repmat(max(p_mat,[], 2), 1, size(p_mat, 2))))); colorbar;
                % cumulative:
                figure; imagesc((cumprod((p_mat), 1))./...
                        repmat(max((cumprod((p_mat), 1)),[],2),[1 size(p_mat,2)])); colorbar;
            end
    end

    function [score, max_label] = classify_random_forest(model_data, obs)
        
        [max_label, class_names, score] = eval_RF(obs, model_data.tb_inst, varargin);
                          
       % reorder results according to the order in model_info
        [~, ~, i_b] = intersect(...
                            model_data.model_info.class_names, class_names, 'stable'); 
        score = score(i_b);

        % apply prior
        try
            score = score.*model_data.model_info.apriori_vec; 
        catch ex
            breakpoint();
        end
        
        % normalize
        score = score./(sum(score)+eps);        
    end

    % calculate random forest probabilities
    function [score, max_label] = classify_treebagger_quick_predict(...
                                                model_data, obs, tree_inds)
        
        labels = model_data.tb_class_names;
        num_trees = numel(model_data.trees_cell);
        
        if ~exist('tree_inds','var') || isempty(tree_inds)
            tree_inds = 1:num_trees;
        elseif islogical(tree_inds) || mean(tree_inds)<=1 % in case they are logical
            tree_inds = find(tree_inds);
        end
        
        % sum score
        score_mat = zeros(numel(tree_inds),numel(labels)); 
        iscat = false(size(obs,2),1);
        for i_t = 1:numel(tree_inds)
            tree_inst = model_data.trees_cell{tree_inds(i_t)};
            node = classreg.learning.treeutils.findNode(obs,...
                0,[],...
                tree_inst.Children', iscat,...
                tree_inst.CutVar,tree_inst.CutPoint,tree_inst.CutCategories,...
                {},{},...
                {},{},...
                0);             
            score_mat(i_t,:) = tree_inst.ClassProb(node,:);
        end
        score = sum(score_mat, 1);
        
        % reorder results according to the order in model_info
        [~, ~, i_b] = intersect(...
                            model_data.model_info.class_names, labels, 'stable'); 
        score = score(i_b);
%         figure;
%         plot(1:numel(score),score,'b*:');
%         text(1:numel(score),score, labels); 
        
        % apply prior
        try
            score = score.*model_data.model_info.apriori_vec; 
        catch ex
            breakpoint();
        end
        
        % normalize
        score = score./(sum(score)+eps);

        % label
        [~, i_max] = max(score);
        max_label = model_data.model_info.class_names{i_max};
    end

    % calculates temper score
    function [temper_val, val_vec, p_vec] = classify_temper_2d(input_cell)
        flows = add_temper_2d_bayes_mix(flows);
        feature_cell = input_cell_to_feature_cell(input_cell, flows.temper_2d_mix.flows);
        [temper_val, val_vec, p_vec] = classify_temper_2d_on_feature_mat(feature_cell);
    end
    
    % calculates composure score
    function [comp_val, val_vec, p_vec] = classify_composure(input_cell)
        flows = add_composure_bayes_mix(flows);
        feature_cell = input_cell_to_feature_cell(input_cell, flows.composure_mix.flows);
        [comp_val, val_vec, p_vec] = classify_composure_on_feature_mat(feature_cell);
    end
    
    % calculates composure score
    function [temper_val, val_vec, temper_p_vec] = classify_temper_2d_on_feature_mat(input_cell)
        flows = add_temper_2d_bayes_mix(flows);
        t_flows = flows.temper_2d_mix.flows;
        t_groups = flows.temper_2d_mix.groups;
        
        % values vec for classification (hm and lm features)
        val_vec = calculate_flow_values_on_feature_mat(input_cell, t_flows);
        
        % temper calc
        [temper_p_vec, sum_p] = calculate_naive_bayes_prob(t_groups,...
                                        t_flows, val_vec);
        
        temper_val = calc_temper_score_mean(temper_p_vec,...
                                    flows.temper_2d_mix.model_info.score_vec);
        
        temper_val = min(100,max(0,temper_val));
        
        % confidence  
        temper_p_vec = [temper_p_vec(:); sum_p]; 
        
            function score = calc_temper_score_mean(p_vec, score_vec)
                score = dot(p_vec, score_vec);
            end            
    end

    % calculates class using a given model
    function [p_res, feat_struct] = classify_model_on_feat_struct(model_data, feat_struct)
        if isfield(model_data, 'flows')
            flows_s = model_data.flows;
        else
            flows_s = model_data.pairwise_bayesian.flows;
        end
        
        opt_s = classification_model_options_struct(model_data);
        
        % calc all feature values for classification
        if nargout <=1
            [val_vec, val_ind_vec] = ...
                                    calculate_flow_values_on_feature_struct(...
                                                         feat_struct, flows_s);
        else
            [val_vec, val_ind_vec, feat_struct] = ...
                                    calculate_flow_values_on_feature_struct(...
                                                         feat_struct, flows_s);
        end
                
        % classify
        switch opt_s.classifier_type
            case 'full_bayes'
                % full Nbayes    
                [p_vec, ~] = calculate_naive_bayes_prob(model_data.groups, flows_s, val_vec, val_ind_vec);
                p_res = p_vec;
                
            case 'pairwise_bayes'
                pairwise_best = model_data.pairwise_best;
                
                % pairwise classification
                group_names = {model_data.groups.name}';
                num_groups = numel(group_names);
                p_mat = zeros(num_groups, num_groups); 
                for i_pair = 1:numel(pairwise_best)
                    f_ind = pairwise_best(i_pair).flows_inds;
                    
                    pair_groups = pairwise_best(i_pair).groups;
                    g_ind = [find(strcmp(group_names, pair_groups{1})); ...
                             find(strcmp(group_names, pair_groups{2}))];
                         
                    % calc bayes
                    [pair_p_vec, ~] = calculate_naive_bayes_prob(model_data.groups(g_ind),...
                                                    flows_s, val_vec, val_ind_vec, f_ind);
                                                
                    % wins
                    p_mat(g_ind(1), g_ind(2)) = pair_p_vec(2);
                    p_mat(g_ind(2), g_ind(1)) = pair_p_vec(1);                    
                end
                p_res = p_mat;
                
%                 figure;
%                 plot(pairwise_coupling_methods(...
%                             p_mat,...
%                             model_data.model_info.submodels.gender.model_info.num_winners,...
%                             model_data.model_info.submodels.gender.model_info.voting_mode),...
%                             '.-');
%                         
%                 figure;
%                 plot(pairwise_coupling_methods(...
%                             p_mat,...
%                             4,...
%                             'stv_voting'),...
%                             '.-');
                
            case 'treebagger'
                
                score_vec = [];
                
                % try to predict out-of-bag if the information is available
                if isfield(model_data.model_info, 'oob_predict') && ...
                                        model_data.model_info.oob_predict && ...
                                                    ~isempty(feat_struct(1).meta_data)
                                                
                    [~, filename, ~] = fileparts(feat_struct(1).meta_data);
                    tf_filename = find(strcmp(model_data.file_names, filename));
                    if ~isempty(tf_filename)
                        tf_filename = tf_filename(1);
                        tree_inds = model_data.tb_useifort(tf_filename,:);
                        [score_vec, max_label] =...
                                        classify_treebagger_quick_predict(...
                                                        model_data,...
                                                        val_vec, tree_inds);                
                    end
                end
                                      
                % full ensemble prediction if was no match for OOB 
                if isempty(score_vec) 
                    [score_vec, max_label] =...
                                    classify_treebagger_quick_predict(...
                                                    model_data,...
                                                    val_vec);                
                end
                
                p_res = score_vec;
%                   
%                 figure;
%                 plot(1:numel(p_res),p_res,'b*:');
%                 text(1:numel(p_res),p_res, model_data.model_info.class_names); 

            case 'randomforest'
                [score_vec, max_label] =...
                                    classify_random_forest(...
                                                    model_data,...
                                                    val_vec);           
                p_res = score_vec;
                
        end    
    end
        
    % calculates class using naive bayes and a feature struct
    function out_struct = results_scoring_on_probability_results(...
                                                model_data, p_res)
        % params and options 
        model_info = model_data.model_info;
        opt_s = classification_model_options_struct(model_data);
    
        % pariwise coupling if needed
        switch opt_s.classifier_type
            case 'full_bayes'
                p_vec = p_res;
                
            case 'pairwise_bayes'    
                p_vec = pairwise_coupling_methods(...
                            p_res, opt_s.num_winners, opt_s.voting_mode); 
                        
            case 'treebagger'
                p_vec = p_res;
                
            case 'randomforest'
                p_vec = p_res;
        end
        
        % score
        out_val = aggregation_scoring_methods(p_vec,...
                            model_data.model_info.score_vec,...
                            'top_n', opt_s.num_winners);
        out_val = min(100,max(0,out_val));   
        % results struct
        out_struct.classification_type = opt_s.classifier_type;
        out_struct.val = out_val;
        % YL: origina code: out_struct.group= get_group_name(model_info, out_val);
        if (opt_s.num_winners==1)
            out_struct.group = get_group_name(model_info, out_val);
        else
            out_struct.group = GetGroupName (p_vec, model_info);
        end
%         out_struct.conf =  CalcConfidence (p_vec); % YL: - origina code: 100*max(p_vec);
        out_struct.conf =  100*max(p_vec);
        out_struct.p_vec = p_vec;
        out_struct.max_prob_class = model_info.class_names{find(p_vec==max(p_vec),1,'first')}; 

        function GroupName = GetGroupName (p_vec, model_info)
            n = length (model_info.group_names);
            [~, i] = max (p_vec);
            if (i <= n)
                i = n + 1 - i; % YL: the order of p_vec is reversed to the intuition and group_names - no idea why
                GroupName = model_info.group_names {i};
            else
                GroupName = model_info.group_names {1}; % YL: some anomality in the 
            end
        end
		function group_name = get_group_name (model_info, out_val) 
			[t_sort, i_sort] = sort(out_val - model_info.thresholds,'ascend');
			i_g = find(t_sort>=0,1,'first');                
			group_name = model_info.group_names{i_sort(i_g)};
        end
        
        % YL: adhock serialization for further computing
		function conf = CalcConfidence (p_vec)
            p2 = round (100*p_vec);
            l2 = length (p2) - 1;
            conf = sum (100.^(0:l2) .* p2);
% 			Psort = sort (p_vec, 'descend');
% 			Pa = Psort (1);
% 			Pb = Psort (2);
% 			D = 5;
% 			S = .2;
% 			
% 			Da = exp (-D * ((Pa - 1) ^ 2 + S * Pb ^ 2));
% 			Db = exp (-D * ((Pb - 1) ^ 2 + S * Pa ^ 2));
% 			conf = 100 * (Da - Db) ^ .2;
		end
    end

    % classification options
    function opt_s = classification_model_options_struct(model_data)
        
        if isfield(model_data.model_info, 'classifier_type')
            opt_s.classifier_type = model_data.model_info.classifier_type;
        else
            opt_s.classifier_type = 'full_bayes';
        end
        
        if isfield(model_data.model_info,'num_winners') % in case no pairwise data is available
            opt_s.num_winners = model_data.model_info.num_winners;
        else
            opt_s.num_winners = 2;
        end
        
        if isfield(model_data.model_info,'voting_mode') % in case no pairwise data is available
            opt_s.voting_mode = model_data.model_info.voting_mode;
        else
            opt_s.voting_mode = 'elimination_voting';
        end
        
    end

    % various scoring methods
    function score = aggregation_scoring_methods(p_vec, score_vec, method, varargin)                
        switch method
            case 'mean'
                score = dot(p_vec, score_vec);

            case 'max'
                [~, ind_max] = max(p_vec);
                score = score_vec(ind_max);

            case 'mean_adjusted'
                p_vec = p_vec - 1/numel(p_vec);
                p_vec(p_vec<0) = 0;
                p_vec = p_vec./sum(p_vec);
                score = dot(p_vec, score_vec);

            case 'top_n'
                
                if nargin>=4
                    n = varargin{1}; 
                else
                    n = 2; % for conversion to general case
                end
                [~, sort_i] = sort(p_vec,'descend');
                
                if n >= numel(sort_i)
                    p_floor = 0;
				else
                    p_floor = p_vec(sort_i(n+1));
                end
                p_vec = p_vec - p_floor;
                p_vec(p_vec<0) = 0;
%                 p_vec(p_vec<=p_floor) = 0;
                p_vec = p_vec./sum(p_vec);
                score = dot(p_vec, score_vec);
                
            otherwise
                score = 0;
        end
    end

    % various pairwise coupling methods
    function p_vec = pairwise_coupling_methods(p_mat, n_winners, method_str)
        switch method_str
            case 'elimination_voting'
                n_winners = n_winners + 1; %because loser might be 0 in the end

                % sum winning probabilities to select winners
                p_mat_wins = p_mat;
                p_mat_wins(p_mat_wins<0.5) = 0;
                sum_p_wins = sum(p_mat_wins, 1)./sum(sum(p_mat_wins, 1));

                % erase losers
                [~, ind_sort] = sort(sum_p_wins,'descend');
                ind_losers = ind_sort((n_winners+1):end);
                p_mat_wins(ind_losers, :) = 0;
                p_mat_wins(:, ind_losers) = 0;

                % sum again to break ties
                p_vec = sum(p_mat_wins, 1)./(sum(sum(p_mat_wins, 1))+eps);                

            case 'pkpd'
                % http://www.csie.ntu.edu.tw/~cjlin/papers/svmprob/svmprob.pdf
                p_mat_temp = p_mat;
                p_mat_temp(p_mat_temp==0) = nan;
                p_mat_temp(p_mat_temp<=0.01) = 0.01;
                p_vec = 1./(nansum(1./(p_mat_temp),1) - (size(p_mat_temp,1)-2));
                p_vec = p_vec./sum(p_vec);                        

            case {'schulze_voting', 'stv_voting'}
                switch method_str
                    case 'schulze_voting'
                        election_mode_str = 'schulze';
                    case 'stv_voting'
                        election_mode_str = 'pref';
                end
                        
                p_mat_temp = p_mat;
                winners = [];

                for i=1:n_winners                            
                    [~, i_sort_mat] = sort(1-p_mat_temp,1,'descend');
                    i_sort_mat = i_sort_mat(2:end,:)';
                    winners = [winners election_methods(i_sort_mat, election_mode_str)];

                    % erase winners
                    p_mat_temp(winners(end), :) = 1;                        
                    p_mat_temp(:, winners(end)) = 0;
                end

                % erase losers
                losers = setdiff(1:size(p_mat_temp,1),winners);
                p_mat_temp = p_mat;                        
                p_mat_temp(losers, :) = [];
                p_mat_temp(:, losers) = 0;
                % mean probablity outof winners
                p_vec = sum(p_mat_temp,1)./(sum(sum(p_mat_temp,1))+eps); 

            case 'prod'
                p_mat_temp = p_mat;
                p_mat_temp(p_mat_temp<=0.01) = 1;
                p_vec = prod(p_mat_temp, 1);
                p_vec = p_vec./sum(p_vec);   

        end
    end

    % calculates vatg analyses
    function res_struct = classify_vatg(feat_struct)
        flows = add_vatg_models(flows);

        res_struct = struct();        
        
%         direct valence
        [p_res, feat_struct] = classify_model_on_feat_struct(...
                                                flows.valence_model, feat_struct);
        res_struct.valence = results_scoring_on_probability_results(...
                                flows.valence_model, p_res);
                            
        % direct arousal
        [p_res, feat_struct] = classify_model_on_feat_struct(...
                                                flows.arousal_model, feat_struct);
        res_struct.arousal = results_scoring_on_probability_results(...
                                flows.arousal_model, p_res);
                            
        % direct gender
        [p_res, feat_struct] = classify_model_on_feat_struct(...
                                                flows.gender_model, feat_struct);
        res_struct.gender = results_scoring_on_probability_results(...
                                flows.gender_model, p_res);                            
                    
        % direct temper
        [p_res, ~] = classify_model_on_feat_struct(...
                                                flows.temper_model, feat_struct);
        res_struct.va_temper = results_scoring_on_probability_results(...
                                flows.temper_model, p_res);
       
    end 

    % calculates vatg analyses
    function res_struct = classify_audio_quality(feat_struct)
        flows = add_audio_quality_model(flows);

        res_struct = struct();        
        
        p_res = classify_model_on_feat_struct(...
                                                flows.audio_quality_model, feat_struct);
          
        res_struct.audio_quality = results_scoring_on_probability_results(...
                                flows.audio_quality_model, p_res);                    
    end 

    % calculates vatg analyses
    function res_struct = classify_call_trend(feat_struct)
        flows = add_call_trend_model(flows);

        res_struct = struct();        
        
        p_res = classify_model_on_feat_struct(...
                                            flows.call_trend_model, feat_struct);

        res_struct.overall = results_scoring_on_probability_results(...
                        flows.call_trend_model.model_info.submodels.overall, p_res);
                    
        res_struct.improvement = results_scoring_on_probability_results(...
                    flows.call_trend_model.model_info.submodels.improvement, p_res);
    end 

    % adds phrases info
    function res_struct = calc_phrases_info(feature_s)
        flows = add_phrases_table(flows);
        res_struct = struct(); 
            
        en_ind = find(strcmpi(feature_s.temper_group,...
                                            {'low','medium','high'}));
        if isempty(en_ind)
            return;
        end

        res_struct.phrase_1_id = feature_s.t1*12*3 +...
                                        feature_s.t2*3 + ...
                                        en_ind;
        res_struct.phrase_2_id = feature_s.t3*12*3 +...
                                        feature_s.t4*3 + ...
                                        en_ind; 

        res_struct.phrase_1 = flows.phrases_info.ref_table{res_struct.phrase_1_id,...
                                                        'MoodDescription'};
        res_struct.phrase_2 = flows.phrases_info.ref_table{res_struct.phrase_2_id,...
                                                        'MoodDescription'};

        res_struct.moodgroup11_1 = flows.phrases_info.ref_table{res_struct.phrase_1_id,...
                                                        'MoodGroup11'};
        res_struct.moodgroup11_2 = flows.phrases_info.ref_table{res_struct.phrase_2_id,...
                                                        'MoodGroup11'};
    end
    
    % calculates summary statistics on existing analyses
    function out_struct = calc_summary_basic_stats(res_struct)
        flows = add_phrases_table(flows);
        
        out_struct = struct;  
        if isempty(res_struct) || ~isfield(res_struct, 'flow_type')
            return;
        end
                  
        res_struct(~strcmp({res_struct.flow_type},'regular')) = [];
        out_struct.num_segments = numel(res_struct);
        
        if ~isempty(res_struct)
            % VATG analyses
            if isfield(res_struct, 'valence_val')
                out_struct.temper_mean = mean([res_struct.temper_val]);
                [out_struct.temper_mode, out_struct.temper_mode_percent] = ...
                                            calc_mode_str_value({'temper_group'});
                out_struct.valence_mean = mean([res_struct.valence_val]);
                [out_struct.valence_mode, out_struct.valence_mode_percent] = ...
                                            calc_mode_str_value({'valence_group'});
                out_struct.arousal_mean = mean([res_struct.arousal_val]);
                [out_struct.arousal_mode, out_struct.arousal_mode_percent] = ...
                                            calc_mode_str_value({'arousal_group'});
                out_struct.gender_mean = mean([res_struct.gender_val]);
                [out_struct.gender_mode, out_struct.gender_mode_percent] = ...
                                           calc_mode_str_value({'gender_group'});
            end
                                   
            % quality
            if isfield(res_struct, 'audio_quality_val')
                out_struct.quality_mean = mean([res_struct.audio_quality_val]);
                [out_struct.quality_mode, out_struct.quality_mode_percent] = ...
                                           calc_mode_str_value({'audio_quality_group'}); 
                [out_struct.quality_class_mode, out_struct.quality_class_mode_percent] = ...
                                           calc_mode_str_value({'audio_quality_class'});
            end

            % moodgroups
            if isfield(res_struct, 'moodgroup11_1')
                [mg_uniques, mg_hist_count] = hist_count_str({'moodgroup11_1','moodgroup11_2'});
                for i_mg = 1:numel(flows.phrases_info.moodgroup_11_names)
                    hist_mg_ind = find(strcmp(mg_uniques,...
                                flows.phrases_info.moodgroup_11_full_names{i_mg}));
                    if isempty(hist_mg_ind)
                        out_struct.(flows.phrases_info.moodgroup_11_names{i_mg}) = 0;
                    else
                        out_struct.(flows.phrases_info.moodgroup_11_names{i_mg}) = ...
                                            100*mg_hist_count(hist_mg_ind)./sum(mg_hist_count);
                    end  
                end     
            end
        end
        
            function [mode_str, mode_percent] = calc_mode_str_value(field_name)
                [uniques, hist_count] = hist_count_str(field_name);
                [num_max, max_i] = max(hist_count);
                mode_str = uniques{max_i};
                mode_percent = 100*num_max/sum(hist_count);
            end

            function [uniques, hist_count] = hist_count_str(field_names)
                str_cell = {};
                for i_field = 1:numel(field_names)
                    str_cell = [str_cell res_struct.(field_names{i_field})];
                end
                uniques = unique(str_cell);
                hist_count = cellfun(@(x)nnz(strcmp(str_cell, x)), uniques);
            end    
    end 

    % calculates temper_group and temper_enum values
    function [temper_group, temper_enum] = calc_temper_group_on_temper(temper_val, low_cutoff, high_cutoff)
        if ~exist('low_cutoff','var')
            low_cutoff = 35;
        end
        if ~exist('high_cutoff','var')
            high_cutoff = 75;
        end
        temper_group_names = {'low','med','high'};
        tresholds_vec = [0 low_cutoff high_cutoff];
        temper_enum_vec = [2 1 0];
        group_ind = find(temper_val >= tresholds_vec, 1, 'last');
        temper_group = temper_group_names{group_ind};
        temper_enum = temper_enum_vec(group_ind);
    end

    % calculates composure score
    function [comp_val, val_vec, comp_p_vec] = classify_composure_on_feature_mat(input_cell)
        flows = add_composure_bayes_mix(flows);

        comp_flows = flows.composure_mix.flows;
        comp_groups = flows.composure_mix.groups;
        
        % values vec for classification
        val_vec = calculate_flow_values_on_feature_mat(input_cell, comp_flows);
        
        % composure calc
        [comp_p_vec, sum_p] = calculate_naive_bayes_prob(comp_groups,...
                                                    comp_flows, val_vec);
                
        % composure score
        comp_val = dot(comp_p_vec, flows.composure_mix.model_info.score_vec);
        comp_val = min(100,max(0,comp_val));
        
        % confidence            
        comp_p_vec(end+1) = sum_p;  
    end
    
    % calculates buffered segments
    function seg_s = calc_segments(signal, fs, buff_length, overlap_ratio)
        run_libs();
        seg_s = flow_mgr_fh.calc_simple_segments(signal, fs, buff_length, overlap_ratio);
    end
    
    % Calculates distance from cooperation ideal tone
    function coop = classify_cooperation(tones)
        
        cooperation_ideal_tones = [0 2 11 4];   % TBD
        
        coop = impression_distance(cooperation_ideal_tones, tones);
    end

    % Calculates distance from service ideal tone
    function servicetone = classify_servicetone(tones)
        
        servicetone_ideal_tones = [7 0 8 6];    % SOL DO SOL# FA#
        
        servicetone = impression_distance(servicetone_ideal_tones, tones);
    end

    % Calculates distance from sales ideal tone
    function salestone = classify_salestone(tones)
        
        salestone_ideal_tones = [9 1 7 6];  % LA DO# SOL FA#
        
        salestone = impression_distance(salestone_ideal_tones, tones);
    end

    %%% PRIVATE

    % filters silences using specific time constants 
    function vad_vec = smooth_vad_silences(raw_vad_bool, t_vec, min_silence_time, avg_silence_threshold)
        run_libs();
%         warning('run_lib removed')
        min_silence_len = ceil(min_silence_time./median(diff(t_vec))); %1.5 sec
        vad_vec = stats_utils_fh.gap_vector_smoothing(raw_vad_bool, min_silence_len, avg_silence_threshold);
    end
    
    % add the rasta flows
    function flows = add_rasta_partial_flows(flows)        
        if ~isfield(flows,'high_med_flow') || isempty(flows.high_med_flow)
            % hid-med flow
            high_med_flow.mat2mat = {'digitize80'};
            high_med_flow.mat2vec = {'std',2};
            high_med_flow.vec2vec = {'square'};
            high_med_flow.vec2val = {'80-20 pctl freq ratio'};
            high_med_flow.threshold = 4.3;
            high_med_flow.categories = {'high','med'};

            % low-med flow
            low_med_flow.mat2mat = {'add_noise','band_pass'};
            low_med_flow.mat2vec = {'kurtosis',1};
            low_med_flow.vec2vec = {'none'};
            low_med_flow.vec2val = {'std'};
            low_med_flow.threshold = 5.2;
            low_med_flow.categories = {'low','med'};

            % vad spectral snr flow
            vad_ssnr_flow.mat2mat = {'band_pass'};
            vad_ssnr_flow.mat2vec = {'80/20pctl',1};
            vad_ssnr_flow.vec2vec = {'log_abs'};
            vad_ssnr_flow.vec2val = {'numel_over_threshold'};
            vad_ssnr_flow.threshold = 550;
            vad_ssnr_flow.categories = {'nospeech','speech'};

            % vad on rasta flow
            vad_on_rasta.mat2mat = {'anti_g711','band_pass'}; %the anti_g711 does not reduce regular performance
            vad_on_rasta.mat2vec = {'80/20pctl',1};
            vad_on_rasta.vec2vec = {'log_abs'};
    %         vad_on_rasta.threshold = 3.5;
            vad_on_rasta.threshold = 3;
            vad_on_rasta.categories = {'nospeech','speech'};

            flows.high_med_flow = high_med_flow;
            flows.low_med_flow = low_med_flow;
            flows.vad_ssnr_flow = vad_ssnr_flow;
            flows.vad_on_rasta = vad_on_rasta;
        end
    end

    % defines parameters for loading and pre-processing a model
    function model_info = load_model_info(model_name)
        model_info.model_name = model_name;
        switch model_name
            case 'valence_03'
                model_info.model_name = 'valence_3_groups';
                model_info = default_properties(model_info); 
            
            case 'temper_03'
                model_info.model_name = 'temper_3_groups';
                model_info = default_properties(model_info);
            
            case 'arousal_03'
                model_info.model_name = 'arousal_3_groups';
                model_info = default_properties(model_info);    
                
            case 'gender_03'
                model_info.model_name = 'gender_03';
                model_info = default_properties(model_info); 
               
            case 'audio_quality_02' 
                model_info.model_name = 'audio_quality_02';                
                model_info = default_properties(model_info); 
            
            case 'audio_quality_03' 
                model_info.model_name = 'audio_quality_03';                
                model_info = default_properties(model_info); 
                
            case 'call_trend_01' 
                model_info.model_name = 'call_trend_01';                
                model_info = default_properties(model_info); 
                
            case 'temper_ver_1.5'
                model_info.mix_file = 'temper_mix_2feat_3db_ver1.5.mat';

                model_info.class_names = {'gs_low','cc_low','wm_low','gs_med','cc_med','wm_med',...
                                            'cc_ang','gs_high','cc_high','wm_high'};   

                model_info.apriori_vec = [[0.2 0.15 0.65].*0.4 [0.35 0.2 0.45].*0.4 [0.4 0.2 0.4 0.0].*0.2];        
                model_info.score_vec = [[0 0 0] [50 60 55] [80 100 100 90]]; 
                
            case 'composure_ver_1.2'
                model_info.mix_file = 'composure_temper_mix_matlab3.mat';

                % apriori and scores setting
                model_info.class_names = {'calm', 'anger', 'outbreak', 'calm2anger', 'anger2outbreak',...
                                                    'low', 'med', 'high'};   
                % composure
                model_info.apriori_vec = [0.3 0.25 0.03 0.15 0.06 0.25 0.25 0.03];   
                model_info.score_vec = [0 50 100 40 70 25 0 100];
                
            otherwise
                model_info = [];
        end
        
                % load default model properties
                function model_info = default_properties(model_info, score_mode)
                    % YL: setting release params
%                     RevConfig.ID = '2.0.5.1'; RevConfig.NumWinners = 1;
                    RevConfig.ID = '2.0.7.0'; RevConfig.NumWinners = 1;
%                     RevConfig.ID = '2.0.7.0-s'; RevConfig.NumWinners = 2;
%                     RevConfig.ID = '2.0.11.0'; RevConfig.NumWinners = 1;
                    % YL: setting release params
                    
                    switch model_info.model_name
                        
                        case 'audio_quality_02'  
                                                        
%                             model_info.mix_file = 'treebagger_data_QUALITY_18class_36feat.mat';
                            model_info.mix_file = 'treebagger_data_QUALITY_18class_36feat_150x4.mat';                            
                            model_info.classifier_type = 'treebagger'; 
                            model_info.oob_predict = 1;
                            
%                             model_info.mix_file = 'bayes_data_QUALITY_18class_36feat.mat';
%                             model_info.classifier_type = 'pairwise_bayes'; 
                            
                            %  num,  name,              app, quality
                            meta_data_cell = {...
                                1, 'good'               ,1 ,100; ...
                                2, 'marginal'           ,1 ,60; ... %2
                                3, 'too short'          ,1 ,0.1; ...
                                4, 'bad uncategorized'  ,1 ,0.2; ...
                                5, 'broadband stat'     ,1 ,0.3; ...
                                6, 'clipping'           ,1 ,0.4; ...
                                7, 'compression'        ,1 ,0.5; ...
                                8, 'corrupt'           ,1 ,0.6; ...
                                9, 'distortion'        ,1 ,0.7; ...                              
                                10, 'missing-weak'      ,1 ,0.8; ...
                                11, 'music'             ,1 ,0.9; ...
                                12, 'not speech'        ,1 ,1; ...
                                13, 'tonal noises'      ,1 ,1.1; ...
                                14, 'tonal stat'        ,1 ,1.2; ...                               
                                15, 'callcenter_good'  ,1 ,90;...
                                16, 'gs_marginal'       ,0.2 ,50;... %1
                                17, 'web_marginal'      ,1 ,65;... %2
                                18, 'callcenter_bad'   ,0.5 ,1.3;...
                                }; 
                            
                            model_info.class_names = meta_data_cell(:,2);                            
                            model_info.voting_mode = 'pkpd';
                            model_info.apriori_vec = [meta_data_cell{:,3}];      
                            model_info.apriori_vec = model_info.apriori_vec./...
                                                            sum(model_info.apriori_vec);
                                
                            model_info.num_winners = 3;
                            model_info.score_vec = [meta_data_cell{:,4}];                                    
                            
                            model_info.group_names = {'bad', 'marginal_worse', 'marginal_better', 'good'};   
                            model_info.thresholds = [0, 30, 45, 60];
                            
                        case 'audio_quality_03'  
                            model_info.mix_file = 'Quality5Lrev2.0.3.0-s.mat';
%                             model_info.mix_file = 'treebagger_data_QUALITY_5class_31feat_100x10.mat';                            
                            model_info.classifier_type = 'treebagger'; 
%                             model_info.classifier_type = 'pairwise_bayes'; 
                            model_info.oob_predict = 1;
                            
                            %  num,  name,              app, quality
                            meta_data_cell = {...
                                1, 'good'               ,1 ,100; ...
                                2, 'marginal'           ,1 ,50; ... %2
                                3, 'bad'                ,1 ,0; ...
                                4, 'good_cc'            ,1 ,100; ...
                                5, 'bad_cc'             ,1 ,0; ...
                                }; 
                            
                            model_info.class_names = meta_data_cell(:,2);                            
                            model_info.voting_mode = 'pkpd';
                            model_info.apriori_vec = [meta_data_cell{:,3}];      
                            model_info.apriori_vec = model_info.apriori_vec./...
                                                            sum(model_info.apriori_vec);
                                
                            model_info.num_winners = 2;
                            model_info.score_vec = [meta_data_cell{:,4}];                                    
                            
                            model_info.group_names = {'bad', 'marginal_worse', 'marginal_better', 'good'};   
                            model_info.thresholds = [0, 20, 45, 65];
            
                        case 'valence_3_groups'
                            model_info.num_winners = RevConfig.NumWinners;
%                             model_info.mix_file = ['Valence3Lrev' RevConfig.ID '.mat'];
                            model_info.mix_file = 'treebagger_data_VALENCE_109feat_3cl_500t.mat';						

                            model_info.classifier_type = 'treebagger'; 
                            model_info.oob_predict = 1;                             
                            
                            %  num,  name,          app, val, 
                            meta_data_cell = {...
                                1,  'pos',       1, 100;... 
                                2,  'neut',      1, 50;...
                                3,  'neg',       1, 0;...
                                };
                            
                            model_info.class_names = meta_data_cell(:,2);      
                            model_info.apriori_vec = [meta_data_cell{:,3}];      
                            model_info.apriori_vec = model_info.apriori_vec./...
                                                            sum(model_info.apriori_vec);
                                
                            model_info.score_vec = [meta_data_cell{:,4}];                                   
                            
                            model_info.group_names = {'negative', 'neutral', 'positive'};   
                            model_info.thresholds = [0, 24, 61];
                            
                       case 'arousal_3_groups'
                            model_info.num_winners = RevConfig.NumWinners;
%                             model_info.mix_file = ['Arousal3Lrev' RevConfig.ID '.mat'];
                            model_info.mix_file = 'treebagger_data_AROUSAL_33_feat_3cl_300t.mat';
                            model_info.classifier_type = 'treebagger'; 
                            model_info.oob_predict = 1;
                            
                            %  num,  name,          app, arousa;
                            meta_data_cell = {...
                                1,  'high',       1, 100;... 
                                2,  'neut',      1, 50;...
                                3,  'low',       1, 0;...
                                };
                            
                            model_info.class_names = meta_data_cell(:,2);      
                            model_info.apriori_vec = [meta_data_cell{:,3}];      
                            model_info.apriori_vec = model_info.apriori_vec./...
                                                            sum(model_info.apriori_vec);
                                
                            model_info.score_vec = [meta_data_cell{:,4}];                                   
                            
                            model_info.group_names = {'low', 'neutral', 'high'};   
                            model_info.thresholds = [0, 38, 76];   
                            
                        case 'temper_3_groups'
                            model_info.num_winners = RevConfig.NumWinners;
%                             model_info.mix_file = 'Temper3Lrev2.0.11.0.mat';
%                             model_info.mix_file = ['Temper3Lrev' RevConfig.ID '.mat'];
                            model_info.mix_file = 'treebagger_data_TEMPER_34_feat_3cl_300t.mat';
                            model_info.classifier_type = 'treebagger'; 
                            model_info.oob_predict = 1;
                            
                            %  num,  name,          app, temper
                            meta_data_cell = {...
                                1,  'high',       0.5, 100;... 
                                2,  'med',      1, 50;...
                                3,  'low',       1, 0;...
                                };
                            
                            model_info.class_names = meta_data_cell(:,2);      
                            model_info.apriori_vec = [meta_data_cell{:,3}];      
                            model_info.apriori_vec = model_info.apriori_vec./...
                                                            sum(model_info.apriori_vec);
                                
                            model_info.score_vec = [meta_data_cell{:,4}];
                            
                            model_info.group_names = {'low', 'medium', 'high'};   
                            model_info.thresholds = [0, 23, 63];
                            
                        case 'gender_03'
                            model_info.mix_file = 'Gender2Lrev2.0.3.0-s.mat';
%                             model_info.mix_file = 'treebagger_data_GENDER_9_feat_2cl_200t.mat';
                            model_info.classifier_type = 'treebagger'; 
                            model_info.oob_predict = 1;
                            
                            %  num,  name,          app, gen,
                            meta_data_cell = {...
                                1,  'mal',       1, 100;... 
                                2,  'fem',      1, 0;...
                                };
                            
                            model_info.class_names = meta_data_cell(:,2);      
                            model_info.apriori_vec = [meta_data_cell{:,3}];      
                            model_info.apriori_vec = model_info.apriori_vec./...
                                                            sum(model_info.apriori_vec);
                                
                            model_info.num_winners = 2;
                            model_info.score_vec = [meta_data_cell{:,4}];                                   
                            
                            model_info.group_names = {'female', 'male'};   
                            model_info.thresholds = [0, 50];
                                                    
                        case 'call_trend_01'
                            model_info.mix_file = 'Trend3Lrev2.0.3.0-s.mat';
%                             model_info.mix_file = 'treebagger_data_TREND_atnt_5feat.mat';
%                             model_info.mix_file = 'treebagger_data_TREND_atnt_7feat.mat';
                            model_info.classifier_type = 'treebagger'; 
                            model_info.oob_predict = 1;
                             
                            %  num,  name,       app, overall, improvement
                            meta_data_cell = {...
                                1,  'g2g',            1, 100, 20;... 
                                2,  'b2b',            1, 0, 0;...
                                3,  'b2g',            1, 50, 100;...
                                };
                            
                            model_info.class_names = meta_data_cell(:,2);                            
                            model_info.voting_mode = 'pkpd';
                            model_info.apriori_vec = [meta_data_cell{:,3}];      
                            model_info.apriori_vec = model_info.apriori_vec./...
                                                            sum(model_info.apriori_vec);
                                
                            submodel_names = {'improvement','overall'}; 
                            for i_sub = 1:numel(submodel_names)
                                model_info.submodels.(submodel_names{i_sub}).model_info.apriori_vec =...
                                                            model_info.apriori_vec;
                                model_info.submodels.(submodel_names{i_sub}).model_info.voting_mode = ...
                                                            model_info.voting_mode;
                                model_info.submodels.(submodel_names{i_sub}).model_info.classifier_type = ...
                                                            model_info.classifier_type; 
                                model_info.submodels.(submodel_names{i_sub}).model_info.class_names = ...
                                                            model_info.class_names; 
                            end
                            
                            % scoring: winners
                            % overall submodel                                                        
                            model_info.submodels.overall.model_info.num_winners = 2;
                            model_info.submodels.overall.model_info.score_vec = [meta_data_cell{:,4}];
                            model_info.submodels.overall.model_info.group_names = {'bad','good'};   
                            model_info.submodels.overall.model_info.thresholds = [0, 50];
                            
                            % improvement submodel
                            model_info.submodels.improvement.model_info.num_winners = 2;
                            model_info.submodels.improvement.model_info.score_vec = [meta_data_cell{:,5}];
                            model_info.submodels.improvement.model_info.group_names = {'no_improvement','improved'};   
                            model_info.submodels.improvement.model_info.thresholds = [0, 50];
                    end
                end
    end

    % load model data file
    function model_data = load_model_data(varargin)
        model_info = varargin{1};
        if isfield(model_info, 'classifier_type')
            classifier_type = model_info.classifier_type;
        else
            classifier_type = 'full_bayes';
        end
        
        switch classifier_type
            case {'full_bayes','pairwise_bayes'}
                model_data = load_bayes_model_data_file(varargin{:});
            case 'treebagger'
                model_data = load_treebagger_data_file(varargin{:});
            case 'randomforest'
                model_data = load_randomforest_data_file(varargin{:});
        end
        
    end

    % loads a bayes classification data file
    function model_data = load_bayes_model_data_file(model_info, nmean_edges)
        
        if ~isdeployed
            data_filename = fullfile(flows.saved_files_path, model_info.mix_file);
            batch_functions_lib('copy_file_into_higher_dir', data_filename,...
                                        'in_use', '', 0);
        else
            data_filename = which(model_info.mix_file);
        end
        data_file = load(data_filename);
        if isfield(data_file, 'exp_res')
            exp_res = data_file.exp_res;
        else
            exp_res = data_file.exp_struct.pairwise_bayesian;
        end
        
        % process file
        model_data = pre_process_bayes_mix_struct(exp_res,...
                                model_info.class_names, model_info.apriori_vec);
        model_data.model_info = model_info;  
        
        
            % bayes distribution pre-process function 
            function model_data = pre_process_bayes_mix_struct(bayes_mix_struct_infile, names,...
                                                                    apriori_vec, nmean_edges)
                if ~exist('apriori_vec','var')
                    apriori_vec = ones(1, numel(names));
                end
%                 if ~exist('score_vec','var')
%                     score_vec = -1*ones(1, numel(names));
%                 end
                run_libs();
                % flows
                model_data.flows = bayes_mix_struct_infile.flows;
                
                % reorder groups
                infile_names = {bayes_mix_struct_infile.groups.name};
                for g_ind=1:numel(names)
                    infile_ind = find(strcmpi(names{g_ind}, infile_names));
                    if ~isempty(infile_ind)
%                         bayes_mix_struct_infile.groups(infile_ind).apriori_vec = apriori_vec(g_ind);
%                         bayes_mix_struct.groups(infile_ind).score_vec = score_vec(g_ind);
                        model_data.groups(g_ind) = bayes_mix_struct_infile.groups(infile_ind);
                    end
                end
                
                % assign apriori
                for g_ind=1:numel(names)
                    model_data.groups(g_ind).apriori_vec = apriori_vec(g_ind);
                end                

                % prepare edges for extrapolation and add pseudocount 
                if ~exist('nmean_edges','var')
                    nmean_edges = 7;
                end
                pseudocount_val = 0.001;
%                 gauss_mat = fspecial('gaussian',nmean_edges+1,nmean_edges*2);
%                 filt_vec = gauss_mat(nmean_edges/2+1,:)./sum(gauss_mat(nmean_edges/2+1,:));
                for flow_i = 1:numel(model_data.flows)
                    for g_ind = 1:numel(model_data.groups)
                        % pseudocount
                         p_dens = model_data.groups(g_ind).pdf_mat(flow_i,:) + pseudocount_val; 

%                         % additional gaussian smoothing
%                         bayes_mix_struct.groups(g_ind).pdf_mat(flow_i,:)=...
%                             conv(bayes_mix_struct.groups(g_ind).pdf_mat(flow_i,:), filt_vec, 'same');

                        % lowpass smoothing
                        p_dens = stats_utils_fh.vector_smoothing(p_dens, nmean_edges, 'lowpass');
                        p_dens = p_dens./sum(p_dens);                        
                        
                        % edges
                        p_dens(1) = mean(p_dens(1:nmean_edges));
                        p_dens(end) = mean(p_dens((end-nmean_edges+1):end));
                        
                        model_data.groups(g_ind).pdf_mat(flow_i,:) = p_dens;
                    end
                end
                
                % recalc pairwise g_ind indeces
                if isfield(bayes_mix_struct_infile, 'pairwise_best')
                    model_data.pairwise_best = bayes_mix_struct_infile.pairwise_best;
                    for i_pair = 1:numel(model_data.pairwise_best)                        
                        [~, model_data.pairwise_best(i_pair).groups_inds, ~] =...
                                    intersect(names, model_data.pairwise_best(i_pair).groups,'stable');
                    end
                end
            end
    end

    function model_data = load_randomforest_data_file(model_info)
        if ~isdeployed
            data_filename = fullfile(flows.saved_files_path, model_info.mix_file);
            batch_functions_lib('copy_file_into_higher_dir', data_filename,...
                                        'in_use', '', 0);
        else
            data_filename = which(model_info.mix_file);
        end
        load(data_filename,'exp_struct');
        
        % process file
        model_data = exp_struct;
        model_data.model_info = model_info; 
    end

    % loads a bayes classification data file
    function model_data = load_treebagger_data_file(model_info)
        if ~isdeployed
            data_filename = fullfile(flows.saved_files_path, model_info.mix_file);
            batch_functions_lib('copy_file_into_higher_dir', data_filename,...
                                        'in_use', '', 0);
        else
            data_filename = which(model_info.mix_file);
        end
        load(data_filename,'exp_struct');
        
        try
        lg = log4m.getLogger('MatlabAnalyzer');
        lg.error('Filename: %s', data_filename);
        lg.error(savejson('tb_inst', exp_struct.tb_inst));
        catch ex
        end
        
        % process file
        model_data = exp_struct;
        model_data.model_info = model_info; 
        model_data = pre_process_treebagger_mix_struct(model_data);
        
            function model_data = pre_process_treebagger_mix_struct(model_data)
                % process flow names into flow struct for feature extraction
                mfu_lib_fh = multiflow_utils_lib();
                feat_lib_fh = feature_lib();
                for i = 1:numel(model_data.flow_names)                 
                     flows_s(i) = mfu_lib_fh.flow_str_to_flow_struct(...
                                        model_data.flow_names{i} ,feat_lib_fh);                            
                end
                model_data.flows = flows_s;
                % remove old field
                model_data = rmfield(model_data, 'flow_names');
                
%                 % load all trees into a cell to save subsref time
%                 if isfield(model_data, 'trees_to_use')
%                     tree_inds = find(model_data.trees_to_use);
%                 else


                try
                    tree_inds = 1:numel(model_data.tb_inst.Trees); 
                catch ex
                    lg = log4m.getLogger('MatlabAnalyzer');
                    lg.error('Filename: %s', data_filename);
%                     ld.error(savejson('exception', ex));
                    lg.error(savejson('tb_inst', model_data.tb_inst));
                end
                

                    
%                 end
                subs_s = struct('subs',{'Trees',{0},'Impl'},...
                            'type',{'.', '{}', '.'});
                model_data.trees_cell = cell(size(tree_inds));
                for i_t = 1:numel(tree_inds)
        %             tree_inst = t_bagger.Trees{i_t}.Impl;
                    subs_s(2).subs = {tree_inds(i_t)}; 
                    model_data.trees_cell{i_t} = builtin('subsref', model_data.tb_inst, subs_s);
                    model_data.trees_cell{i_t}.CutCategories = {}; % reduces memory footprint and is empty
                end 
                
                % oob prediction params
                model_data.tb_useifort = model_data.tb_inst.OOBIndices(:,tree_inds);
                [~, model_data.file_names, ~] = cellfun(@fileparts,...
                                    model_data.file_names,'uni',0);
                % tree params
                model_data.tb_prior = model_data.tb_inst.Prior;
                model_data.tb_class_names = model_data.tb_inst.ClassNames;
                % remove the treebagger
                model_data = rmfield(model_data, 'tb_inst');
            end
    end


    % add temper bayes mix
    function flows = add_temper_2d_bayes_mix(flows)
        model_info = load_model_info('temper_ver_1.5');        

        if ~isfield(flows,'temper_2d_mix') || isempty(flows.temper_2d_mix)
            flows.temper_2d_mix = load_model_data(model_info, 10);
        end
    end

    % add composure bayes mix
    function flows = add_composure_bayes_mix(flows)        
        model_info = load_model_info('composure_ver_1.2');        
        
        if ~isfield(flows,'composure_mix') || isempty(flows.composure_mix)
            flows.composure_mix = load_model_data(model_info);
        end
    end

    % add vatg model
    function flows = add_vatg_models(flows)        
%        % flows is persistent, so no need to reload every time

%         if ~isfield(flows,'vatg_model') || isempty(flows.vatg_model)
%             flows.vatg_model = load_model_data(load_model_info('vatg_02'));
%         end    
        
        if ~isfield(flows,'temper_model') || isempty(flows.temper_model)
            flows.temper_model = load_model_data(load_model_info('temper_03'));
        end 
        if ~isfield(flows,'valence_model') || isempty(flows.valence_model)
            flows.valence_model = load_model_data(load_model_info('valence_03'));
        end 
        if ~isfield(flows,'arousal_model') || isempty(flows.arousal_model)
            flows.arousal_model = load_model_data(load_model_info('arousal_03'));
        end 
        if ~isfield(flows,'gender_model') || isempty(flows.gender_model)
            flows.gender_model = load_model_data(load_model_info('gender_03'));
        end 
    end

    % add audio quality model
    function flows = add_audio_quality_model(flows)        
        % flows is persistent, so no need to reload every time
        if ~isfield(flows,'audio_quality_model') || isempty(flows.audio_quality_model)
            flows.audio_quality_model = load_model_data(load_model_info('audio_quality_03'));
        end
    end

    % add call trend model
    function flows = add_call_trend_model(flows)        
        % flows is persistent, so no need to reload every time
        if ~isfield(flows,'call_trend_model') || isempty(flows.call_trend_model)
            flows.call_trend_model = load_model_data(load_model_info('call_trend_01'));
        end
    end

    % add phrases / moodgroups / etc data
    function flows = add_phrases_table(flows)        
        % flows is persistent, so no need to reload every time
        if ~isfield(flows,'phrases_info') || isempty(flows.phrases_info)
            ref_table = load_phrases_data();
            
            flows.phrases_info.ref_table = cell2table(ref_table(2:end,:),...
                                            'VariableNames',ref_table(1,:)); 
            flows.phrases_info.moodgroup_11_full_names =...
                            unique(flows.phrases_info.ref_table{:,'MoodGroup11'},...
                                                                   'stable');
            flows.phrases_info.moodgroup_11_names = cellfun(...
                            @(x)['moodgroup11_' lower(x(1:4)) '_pct'],...
                            flows.phrases_info.moodgroup_11_full_names,'uni',0);
        end
        
            function ref_table = load_phrases_data()
                data_filename = 'phrases_reference_table.csv';
                if ~isdeployed
                    data_filename = fullfile(flows.saved_files_path, data_filename);
                    batch_functions_lib('copy_file_into_higher_dir', data_filename,...
                                                'in_use', '', 0);
                else
                    data_filename = which(data_filename);
                end
                ref_table = csvimport(data_filename);
            end
    end

    % helps calculate apriori vec based on user plot
    function apriori_vec = apriori_helper_calc_from_ginput(scores_vec)
        
%         scores_vec = [0 50 100 40 70 25 0 100];
%                 0.13 0.12 0.03 0.12 0.13 0.3 0.13 0.03

        figure; 
        axes; hold on;
        button = 1; 
        [x, y, h_line] = deal([]);
        xx = linspace(0,1,101);
        while button
            [x_press, y_press, button] = ginput(1);
            if ~isempty(x_press)
                x(end+1) = x_press;
                y(end+1) = y_press;
            end
            plot((x), (y),'.');  
            axis([0 1 0 1]); 
            [new_x, sort_i] = sort(x); 
            new_y = y(sort_i);
            if numel(x)>1
                yy=interp1(new_x,new_y,xx,'linear','extrap');
                delete(h_line);
                h_line = plot(xx,yy);
            end
        end
        
        cs_vec=cumsum(yy)./sum(yy);
        [sorted_scores, sort_i] = sort(scores_vec);
        half_points = ([sorted_scores(1) sorted_scores] + [sorted_scores sorted_scores(end)])./2;
        apriori_vec(sort_i) = diff(cs_vec(1+ceil(half_points)));
    end

    % calc energy scale formula
    function [e_scale, hm_norm, lm_norm] = energy_scale_calc(high_med, low_med, norm_mode)
        if ~exist('norm_mode','var')
            norm_mode = 1;
        end
        
        % consts
        min_e_scale = 0;
        max_e_scale = 100;
        low_bound = 25;
        high_bound = 75;
        
        % scaled values
        if norm_mode
            hm_norm = high_med_norm_scale(high_med);
            lm_norm = low_med_norm_scale(low_med);
        else
            hm_norm = high_med;
            lm_norm = low_med;
        end
        
        if (lm_norm>=0) && (hm_norm>=0)
            theta_r = atan((lm_norm)/(hm_norm));
            e_scale = low_bound + (high_bound - low_bound).*(theta_r./(pi/2));
        elseif (lm_norm<0)
            e_scale = max(low_bound - low_bound.*(abs(lm_norm)).^2, min_e_scale);
        else 
            e_scale = min(high_bound + (max_e_scale-high_bound).*(abs(hm_norm)).^2, max_e_scale);
        end
        
    end
    
    % low-med scaling
    function lm_norm = low_med_norm_scale(low_med)
        % ranges taken from the GS distribution
        lm_bottom_range = 1.2; 
        lm_top_range = 3.8; 
%         lm_bottom_range = 1.5; 
%         lm_top_range = 1.8; 
%         lm_bottom_range = flows.low_med_flow.threshold - 4; 
%         lm_top_range = 9 - flows.low_med_flow.threshold; 
        %norm
        lm_norm = (low_med - flows.low_med_flow.threshold);
        lm_norm(lm_norm>0) = lm_norm(lm_norm>0)./lm_top_range;
        lm_norm(lm_norm<0) = lm_norm(lm_norm<0)./lm_bottom_range;
    end

    % high-med scaling
    function hm_norm = high_med_norm_scale(high_med)
        hm_left_range = 1.1; 
        hm_right_range = 1.5; 
%         hm_left_range = 1.5; 
%         hm_right_range = 3.5; 
%         hm_left_range = flows.high_med_flow.threshold - 3.4; 
%         hm_right_range = 6 - flows.high_med_flow.threshold; 
        %norm
        hm_norm = (high_med - flows.high_med_flow.threshold);
        hm_norm(hm_norm>0) = hm_norm(hm_norm>0)./hm_right_range;
        hm_norm(hm_norm<0) = hm_norm(hm_norm<0)./hm_left_range;
    end

    % calculates tone test on a spectrum
    function [t1, t2, t3, t4] = calc_tonetest_on_powspec(powspec_mat, f_vec)
        run_libs();
        
        f_ignore_low = 100; % Hz
        f_ignore_high = 4000; % Hz
        
        % band pass
        atten_ind = (f_vec<f_ignore_low) | (f_vec>f_ignore_high);
        powspec_mat(atten_ind,:) = powspec_mat(atten_ind,:)./10^8;

        % calculate average spectrum
        avg_spectrum = mean(powspec_mat,2);
        avg_spectrum = avg_spectrum./(max(avg_spectrum)+eps);

        % Call tonetest lib to calculate tonetest
        [t1, t2, t3, t4] = tt_lib.calc_tonetest(avg_spectrum.', f_vec(2) - f_vec(1), f_ignore_low, f_ignore_high);
    end

end %main
