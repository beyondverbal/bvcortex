function varargout = harmonics_features_lib(opt)

    if exist('opt', 'var')
        if strcmp(opt, 'populate_features')
            varargout{1} = [ ...
                gen_feature_list('Timbre') ...
                gen_feature_list('Distortion') ...
                ];            
            return
        end
    end
    
    basic_feat_fh = [];
    
    f_handles = struct( ... 
        'process_harmonics_features', @process_harmonics_features, ...
        'GetHarmonicsMat', @GetHarmonicsMat, ...
        'GetDistortion', @GetDistortion, ...
        'run_libs', @run_libs ...
        );
    
    % run libs
    function run_libs()
        if isempty(basic_feat_fh)
            basic_feat_fh = basic_features_lib();
        end
    end    
     
    function feature_list = gen_feature_list(feature_type)
        
        percents = [0, 4, 8, 10, 12, 14, 16, 18, 20];
        harmonic_bases = [1, 3, 5, 7];
        harmonic_lengths = [3, 5, 10];
        
        freq_ranges = [0, 4000 ; 0, 1000 ; 1000, 2000 ; 1000, 4000].';
        
        feature_list = {};
        
        for frange = freq_ranges
            for perc = percents
                
                feature_list{end+1} = sprintf('%s_ALL_%d%%_%d_%d', ...
                    feature_type, ...
                    perc, ...
                    frange(1), frange(2));                        
                
                for hlen = harmonic_lengths
                    for hbase = harmonic_bases
                        
                        first_harm = hbase;
                        last_harm = hbase + hlen;
                        
                        if first_harm == 1 && last_harm == 11
                            last_harm = 10;
                        end
                        
                        feature_list{end+1} = sprintf('%s_%d-%d_%d%%_%d_%d', ...
                            feature_type, ...
                            first_harm, last_harm, ...
                            perc, ...
                            frange(1), frange(2));                        
                    end
                end
            end
        end    
    end
    
    varargout{1} = f_handles;

    
    function [out_mat, f_vec, t_vec] = process_harmonics_features(in_mat, f_vec, t_vec, feature_name)

        params = regexp(feature_name, '(Timbre|Distortion)_(ALL|\d{1,2}-\d{1,2})_(\d{1,2})%_(\d{1,4})_(\d{1,4})', 'tokens');
        params = params{1};
        
        feature_type = params{1};
        harmonics_range_str = params{2};
        width_percent = str2double(params{3});
        frange_start = str2double(params{4});
        frange_end = str2double(params{5});
        
        switch(feature_type)
            case 'Timbre'
                feat_func = @GetHarmonicsMat;
            case 'Distortion'
                feat_func = @GetDistortion;
            otherwise
                error('Unknown feature');
        end
        
        if strcmp(harmonics_range_str, 'ALL')
            harmonics = 0;
        else
            harms = regexp(harmonics_range_str, '(\d{1,2})-(\d{1,2})', 'tokens');
            harms = harms{1};
            harmonics = str2double(harms{1}) : str2double(harms{2});
        end
        
        [out_mat, f_vec, t_vec] = feat_func(in_mat, harmonics, width_percent / 100, frange_start, frange_end);
    end
    
    function [HarmMat, Freq, Time] = GetHarmonicsMat(in_mat, Ordinals, WidthRatio, MinFreq, MaxFreq)
        run_libs();        
        if isstruct(in_mat)
            [HarmMat, Freq, Time] = basic_feat_fh.harmonics_mat(      ...
                in_mat.pitch_f,             ...
                in_mat.pitch_t_vec,         ...
                in_mat.powspec_256, ...
                in_mat.f_vec_256,   ...
                in_mat.t_vec_80,    ...
                Ordinals, ...
                WidthRatio, ...
                MinFreq, ...
                MaxFreq ...
                );
        else
            [HarmMat, Freq, Time] = deal(0);
        end
        
    end

    function [Dist, Freq, Time] = GetDistortion(in_mat, Ordinals, WidthRatio, MinFreq, MaxFreq)
        run_libs();
        if isstruct(in_mat)
            [Dist, Freq, Time] = basic_feat_fh.distortion(              ...
                in_mat.pitch_f,             ...
                in_mat.pitch_t_vec,         ...
                in_mat.powspec_256,   ...
                in_mat.f_vec_256,           ...
                in_mat.t_vec_80,            ...
                Ordinals,                   ...
                WidthRatio,                 ...
                MinFreq,                    ...
                MaxFreq                     ...
                );
        else
            [Dist, Freq, Time] = deal(0);
        end
            
    end
end