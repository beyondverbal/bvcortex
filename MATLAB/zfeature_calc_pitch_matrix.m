function pitch_mat = zfeature_calc_pitch_matrix()

[signal, fs] = audioread('C:\BVC\DownloadedVoices\f9a0632a-7eec-4ec7-bb4d-4e462605bdd4.wav');
assert(fs == 8000)
%spectrum / powerSpec

NFFT = 256;
OVERLAP = 80;
%POWER SPEC
[spectrum_256, f_vec_256, t_vec_80] = quick_spectrogram(signal,hann(NFFT,'periodic')',NFFT - OVERLAP,NFFT, fs);
powspec_256 = abs(spectrum_256).^2;
t_vec_sig = (1:length(signal))./fs;

%RASTA
[rasta_mat, pure_rasta_mat] = mat_rasta(powspec_256, f_vec_256, t_vec_80);

NFFT = 1024;
OVERLAP = 512;
 % calc_fft_1024_on_feature_struct();
[spectrum_1024, f_vec_1024, t_vec_512] = quick_spectrogram(signal,hann(NFFT,'periodic')',NFFT - OVERLAP,NFFT, fs);
powspec_1024 = abs(spectrum_1024).^2;

pow_256 = powspec_256;
pow_1024 = powspec_1024;
[pitch_mat_256, ct_vec_256, ~] = pitch_by_cepstrum(pow_256, f_vec_256, t_vec_80);
[pitch_mat_rasta, ~, ~] = pitch_by_cepstrum(rasta_mat, f_vec_256, t_vec_80);

[pitch_mat_hps, new_f_vec_1024, t_vec_512] = pitch_by_hps(pow_1024, f_vec_1024, t_vec_512);
pitch_mat_hps_interp = interp1(t_vec_512', pitch_mat_hps', t_vec_80','nearest',0)';
pitch_mat_hps_interp = interp1q(1./new_f_vec_1024(end:-1:1), pitch_mat_hps_interp(end:-1:1,:), ct_vec_256');
pitch_mat_hps_interp(isnan(pitch_mat_hps_interp)) = 0;                               
pitch_mat_comb = (pitch_mat_hps_interp+0.01).*(pitch_mat_256+0.001).*(pitch_mat_rasta+0.001).*(0+0.001)./(0.01*0.001*0.001*0.001);
ct_vec = ct_vec_256;
t_vec = t_vec_80;
fprintf('mazal tof')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% PITCH %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 % pitch matrix calculation based on cepstrum
function [pitch_mat, ct_vec, t_vec] = pitch_by_cepstrum(s_mat, f_vec, t_vec)
        
    % consts
    f0_min = 65; %Hz
    f0_max = 650; %Hz
    bpf_min = 65; %Hz
    bpf_max = 2000; %Hz

    % x_low
    % band pass 
    [s_low, f_vec] = band_pass_spectrum(s_mat, f_vec, bpf_min, bpf_max);
    % get cepstrum
    [cep_mat_low, ct_vec, ~] = fft_cepstrum(s_low, f_vec, t_vec, 'eac');
    cep_mat_low = real(cep_mat_low);        
    % eac enhancement
    ecep_mat_low = enhance_acf(cep_mat_low, ct_vec); 

    % band pass "lifter"        
    [ecep_mat_low, ct_vec] = crop_spectrum(ecep_mat_low, ct_vec, 1/f0_max, 1/f0_min);       
    pitch_mat = ecep_mat_low;

        % band pass
        function [s_low, f_vec] = band_pass_spectrum(s_mat, f_vec, bpf_min, bpf_max)
            i_f_min = find(f_vec>=bpf_min,1,'first');
            i_f_max = find(f_vec<=bpf_max,1,'last');
            s_low = zeros(size(s_mat));
            s_low(i_f_min:i_f_max,:) = s_mat(i_f_min:i_f_max,:);
        end

        % highpass envelope
        function s_high = calc_x_high_envelope(s_mat, f_vec, bpf_max)
            i_f_max = find(f_vec<=bpf_max,1,'last');
            s_high = s_mat;
            s_high(1:i_f_max,:) = s_mat(1:i_f_max,:)./10^3;

            x_high = ispecgram(s_high, 256, 8000, 256, (256-80));
            x_high = x_high.*(x_high>0);
            [s_high, f_vec, ~] = simple_specgram(x_high, 8000);
            [s_high, ~] = band_pass_spectrum(s_high, f_vec, bpf_min, bpf_max);
        end

        % band pass
        function [s_out, f_vec] = crop_spectrum(s_mat, f_vec, bpf_min, bpf_max)
            % s_low
            i_f_min = find(f_vec>=bpf_min,1,'first');
            i_f_max = find(f_vec<=bpf_max,1,'last');
            s_out = s_mat(i_f_min:i_f_max,:);
            f_vec = f_vec(i_f_min:i_f_max);
        end

        % enhancement
        function cep_mat = enhance_acf(cep_mat, ct_vec)
            % eac
            cep_mat(cep_mat<0) = 0;
            cep2_mat = interp1(2.*ct_vec, cep_mat, ct_vec,'nearest',0);
            cep_mat = cep_mat - cep2_mat;
            cep_mat(cep_mat<0) = 0;
%                 cep3_mat = interp1(3.*ct_vec, cep_mat, ct_vec,'nearest',0);
%                 cep_mat = cep_mat - cep3_mat;
%                 cep_mat(cep_mat<0) = 0;
        end

        % scale matrix
        function out_mat = scale_mat(in_mat)
            in_mat = in_mat-min(in_mat(:));
            out_mat = in_mat./max(in_mat(:));
        end

end
    





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% BASIC %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% matrix input rasta (spectrum rasta)
function [rasta_spectrum, pure_rasta_spectrum] = mat_rasta(spectrum, ~, t_vec)        
  
    low_f = 0.5;
    high_f = 15;
    
    % add noise to handle transient zeros 
    noise_factor = mean(spectrum(:)); 
    rng(1,'twister');
    spectrum = spectrum + 0.0001*noise_factor.*(rand(size(spectrum))) + eps;
    rng(mod(floor(now*8640000).*labindex ,2^31-1));
       
    % put in log domain
    nl_aspectrum = log(spectrum);

    [rasta_spectrum_log] = param_rastafilt_fir(nl_aspectrum, low_f, high_f, t_vec);
    % do inverse log
    pure_rasta_spectrum = exp(rasta_spectrum_log);
    
    % improve SNR by streching 
    % TODO: this needs to be carefully removed
    rasta_spectrum = strech_snr_on_rasta(pure_rasta_spectrum);
end
% rasta filtering with parameters using IIR filter
function y = param_rastafilt_fir(x, f1, f2, t_vec)
% the modulation bandpass filter itself

    filt_nfft = 128;

    % return if input is too short
    if (size(x, 2) < filt_nfft)
        y = x;
        return;
    end

    % calc timeseries sample rate and frequency vector
    d_t = diff(t_vec);
    filt_fs = 1./min(d_t(d_t>0));
    filt_f_vec = filt_fs/2*linspace(0,1,filt_nfft/2+1);

    % return if input time resolution is not fit for filtering
    if (f2/filt_f_vec(end) >= 1) || ...
                isempty(filt_fs)
        y = x;
        return;
    end

    %  filter design
    if f1==0 % lowpass
        fir1_b = fir1(filt_nfft, f2./filt_f_vec(end));
    elseif f2==0 % highpass
        fir1_b = fir1(filt_nfft, f1./filt_f_vec(end),'high');
    else
        fir1_b = fir1(filt_nfft, [f1 f2]./filt_f_vec(end));
    end

    % filter
    y = simple_filt_filt(fir1_b, 1, x, filt_nfft);

end     
function y = simple_filt_filt(numer, denom, x, mirror_length)

    row_len = size(x,2);

    % mirror the end and beginning
    if mirror_length > (row_len - 2)
        mirror_length = row_len-2;
    end
    x = x.';

    x_temp = [x((mirror_length+1):-1:2, :); x; x((end-1):-1:(end-mirror_length), :)];

    % forwards
    [x_temp, ZF] = filter(numer, denom, x_temp);

    % backwards
    x_temp = x_temp(end:-1:1,:);
    [y, ~] = filter(numer, denom, x_temp, ZF);

    % flip & trim
    y = y(end:-1:1, :); %flip
    y = y((mirror_length+1):(mirror_length + row_len), :); %trim 

    y = y.';

end
% a quicker spectrogram with fewer argument checks and internal calls
function [y, f, t] = quick_spectrogram(x,win,noverlap,nfft,Fs)        
    
    % Window length
    nwind = length(win);

%             f = psdfreqvec('npts',nfft,'Fs',Fs,'Range','half');
    f = get_f_vec(nfft,Fs);
    num_f = length(f);

    % Make x and win into columns
    x = x(:); 
    win = win(:); 

    ncol = fix((length(x)-noverlap)/(nwind-noverlap));
    colindex = (0:(ncol-1))*(nwind-noverlap)+1;

    % Calculate full frames
    [firsts, deltas] = meshgrid(colindex(1:ncol), (0:(nwind-1)));
    idx_mat = firsts + deltas;
    xin = x(idx_mat) .* repmat(win, [1, size(idx_mat, 2)]); 

    % Compute the raw STFT with the appropriate algorithm
%             [y,f] = computeDFT(xin,nfft,Fs);
    y_full = fft(xin,nfft);   
    y = y_full(1:num_f,:); % one sided            

    % colindex already takes into account the noverlap factor; Return a T
    % vector whose elements are centered in the segment.
    t = ((colindex)+((nwind)/2)')/Fs; 

    
          

        function w = get_f_vec(nfft, fs)
            freq_res = fs/nfft;
            w = freq_res*(0:nfft-1);

            halfNPTS = (nfft/2)+1;
            w(halfNPTS) = fs/2;

            w = w(1:halfNPTS);
            w = w(:);                
        end
end        
     
function rasta_spectrum = strech_snr_on_rasta(rasta_spectrum)        
    % improve SNR (contrast)
    max_spec = max(rasta_spectrum(:));
    if ~isempty(max_spec)
        atten_ind = (rasta_spectrum<=(max_spec./exp(5))) & (rasta_spectrum>=(max_spec./exp(15)));
        rasta_spectrum(atten_ind) =...
                rasta_spectrum(atten_ind).^2./abs(max_spec./exp(5));   
    end
end
    


