function varargout = basic_features_lib(varargin)
% library file for basic DSP features and function such as spectrograms,
% cepstrum, rasta, lpc, filters, pitch calculations...

stats_utils_fh = [];

f_handles = struct(... % recommended way to pass nested func handles
        'simple_rasta', @simple_rasta,...
        'rasta_online', @rasta_online,...
        'mat_rasta', @mat_rasta,...
        'pure_mat_rasta', @pure_mat_rasta,...
        'strech_snr_on_rasta', @strech_snr_on_rasta,...
        'param_rastafilt_fir', @param_rastafilt_fir,...
        'rastafilt_orig', @rastafilt_orig,...
        'simple_specgram', @simple_specgram,...
        'param_specgram', @param_specgram,...
        'quick_spectrogram', @quick_spectrogram, ...
        'synth_sound_from_image', @synth_sound_from_image,...
        'ispecgram', @ispecgram,...
        'ispecgram_simil', @ispecgram_simil,...
        'spec2cep', @spec2cep,...
        'cep2spec', @cep2spec,...
        'lifter', @lifter,...
        'rasta_to_signal', @rasta_to_signal,...
        'channel_spectrum', @channel_spectrum,...
        'fft_cepstrum', @fft_cepstrum,...
        'pitch_by_cepstrum', @pitch_by_cepstrum,...
        'pitch_by_cepstrum_combined', @pitch_by_cepstrum_combined,...
        'pitch_by_hps', @pitch_by_hps,...        
        'pitch_contour_on_pitch_mat', @pitch_contour_on_pitch_mat,...
        'vocal_source_filter_separation', @vocal_source_filter_separation, ...
        'simple_filt_filt', @simple_filt_filt, ...
        'harmonics_mat', @harmonics_mat, ...
        'distortion', @distortion ...
         );
     
if nargin == 0
    varargout{1} = f_handles;
else
    if nargout>0
        [varargout{1:nargout}] = feval(eval(['@' varargin{1}]),varargin{2:end});
    else
        feval(eval(['@' varargin{1}]),varargin{2:end});
    end
end

    % run libs
    function run_libs()
        if isempty(stats_utils_fh)
            stats_utils_fh = stats_utils_lib();
        end
    end

    % simple rasta - signal input
    function [spectrum, rasta_spectrum, f_vec, t_vec] = simple_rasta(signal, fs, low_f, high_f)

        [spectrum, f_vec, t_vec] = simple_specgram(signal, fs);
        powspec = abs(spectrum).^2;
        if nargin == 2
            rasta_spectrum = mat_rasta(powspec,  f_vec, t_vec);
        elseif nargin == 4
            rasta_spectrum = mat_rasta(powspec,  f_vec, t_vec, low_f, high_f);
        end
        
    end

    % matrix input rasta (spectrum rasta)
    function [rasta_spectrum, pure_rasta_spectrum] = mat_rasta(varargin)        
        pure_rasta_spectrum = pure_mat_rasta(varargin{:});
        
        % improve SNR by streching 
        % TODO: this needs to be carefully removed
        rasta_spectrum = strech_snr_on_rasta(pure_rasta_spectrum);
    end
    
    % only the pure rasta filtering
    function rasta_spectrum = pure_mat_rasta(spectrum, f_vec, t_vec, low_f, high_f)
        % make sure it's a power spectrum
        if ~isreal(spectrum(:))
            powspec = abs(spectrum).^2;
        else
            powspec = spectrum;
        end
        % filter params
        if nargin <= 3
            low_f = 0.5;
            high_f = 15;
        end
        % add noise to handle transient zeros 
        noise_factor = mean(powspec(:)); 
        rng(1,'twister');
        powspec = powspec + 0.0001*noise_factor.*(rand(size(powspec))) + eps;
%         rng('shuffle','twister');
        rng(mod(floor(now*8640000).*labindex ,2^31-1));
        
        % put in log domain
        nl_aspectrum = log(powspec);

        [rasta_spectrum_log] = param_rastafilt_fir(nl_aspectrum, low_f, high_f, t_vec);

        % do inverse log
        rasta_spectrum = exp(rasta_spectrum_log);
    end

    function rasta_spectrum = strech_snr_on_rasta(rasta_spectrum)        
        % improve SNR (contrast)
        max_spec = max(rasta_spectrum(:));
        if ~isempty(max_spec)
            atten_ind = (rasta_spectrum<=(max_spec./exp(5))) & (rasta_spectrum>=(max_spec./exp(15)));
            rasta_spectrum(atten_ind) =...
                    rasta_spectrum(atten_ind).^2./abs(max_spec./exp(5));   
        end
    end

    % rasta back to signal
    function new_signal = rasta_to_signal(signal, fs, low_f, high_f)
    
        [~, rasta_spectrum, r_f_vec, r_t_vec] = simple_rasta(signal, fs, low_f, high_f);
   
        nfft = 1024;
        window = 256;
        overlap = 256-80;
        
        [spectrum, f_vec, t_vec] = spectrogram(signal,window,overlap,nfft,fs);        
        interp_rasta = interp2(r_t_vec, r_f_vec, log(rasta_spectrum+eps), t_vec,...
                            f_vec,'linear',0);
        rasta_spectrum = exp(interp_rasta);
        
        % mask current spectrum with rasta
        new_spectrum = (rasta_spectrum.^0.5).*exp(1i.*angle(spectrum));
        
        new_signal =  ispecgram(new_spectrum, nfft, fs, window, overlap);
        
    end
    
    % channel spectrum function (average spectrum of some rasta filtering)
    function [channel_spectrum, f_vec] = channel_spectrum(signal, fs, low_f, high_f, amp_ratio)
    
        % rasta params
        if nargin<=2
            low_f = 0.5;
            high_f = 15;
            amp_ratio = -1;
        end
        atten_ratio = min([max([(1-amp_ratio) 0]), 1]);
        
        [spectrum, rasta_spectrum, f_vec, ~] = simple_rasta(signal, fs, low_f, high_f);

        new_spectrum = (atten_ratio.*abs(spectrum)+...
                    amp_ratio.*((abs(spectrum).^atten_ratio).*rasta_spectrum.^(amp_ratio/2))).*exp(1i.*angle(spectrum));

        channel_spectrum = 10*log10(mean(abs(new_spectrum).^2,2));
        channel_spectrum = channel_spectrum - max(channel_spectrum);
    end

    % cepstrum calculation
    function [fft_cep, new_f_vec, t_vec] = fft_cepstrum(s_mat, f_vec, t_vec, mode)
        if ~exist('mode','var') || isempty(mode)
            mode = 'log';
        end
        
        % check input
        if ~isreal(s_mat) % complex spectrum
            mag_spectrum = abs(s_mat).^2;
        elseif any(s_mat<0) % log magnitude spectrum
            mag_spectrum = exp(s_mat);
        else % magnitude spectrum
            mag_spectrum = s_mat;
        end
        
        switch mode
            case 'eac'
%                 mag_spectrum = mag_spectrum.^0.33;
                mag_spectrum = exp(0.33.*log(mag_spectrum + eps)); % faster
                % add missing mirror part
                mag_spectrum = [mag_spectrum; mag_spectrum(end:-1:2,:)];
            case 'oneside'
                mag_spectrum = log(mag_spectrum+eps);
            otherwise 
                % add missing mirror part
                mag_spectrum = [mag_spectrum; mag_spectrum(end:-1:2,:)];
                mag_spectrum = log(mag_spectrum+eps);
        end            
        
        new_nfft = 2*(length(f_vec)-1);
%         new_nfft = length(f_vec);
        fs = (length(f_vec)-1)./(f_vec(end)-f_vec(1));
        new_f_vec = (fs/2*linspace(0,1,new_nfft/2+1));
        
        % perform ifft on each column
        fft_cep = ifft(mag_spectrum, new_nfft);
%         fft_cep = ifft(mag_spectrum);
        % discard the symmetric half
        fft_cep = fft_cep(1:length(new_f_vec),:);  
        % compute magnitude spectrum
        fft_cep = (fft_cep);
    end
    
    % pitch matrix calculation based on cepstrum
    function [pitch_mat, ct_vec, t_vec] = pitch_by_cepstrum(s_mat, f_vec, t_vec)
        
        % consts
        f0_min = 65; %Hz
        f0_max = 650; %Hz
        bpf_min = 65; %Hz
        bpf_max = 2000; %Hz
        
        % x_low
        % band pass 
        [s_low, f_vec] = band_pass_spectrum(s_mat, f_vec, bpf_min, bpf_max);
        % get cepstrum
        [cep_mat_low, ct_vec, ~] = fft_cepstrum(s_low, f_vec, t_vec, 'eac');
        cep_mat_low = real(cep_mat_low);        
        % eac enhancement
        ecep_mat_low = enhance_acf(cep_mat_low, ct_vec); 
        
        % band pass "lifter"        
        [ecep_mat_low, ct_vec] = crop_spectrum(ecep_mat_low, ct_vec, 1/f0_max, 1/f0_min);       
        pitch_mat = ecep_mat_low;
        
        
%         % x_high
%         s_high = calc_x_high_envelope(s_mat, f_vec, bpf_max);
%         % get cepstrum
%         [cep_mat_high ct_vec ~] = fft_cepstrum(s_high, f_vec, t_vec, 'eac');
%         cep_mat_high = real(cep_mat_high);
%         % eac enhancement
%         ecep_mat_high = enhance_acf(cep_mat_high, ct_vec);     
%         % band pass "lifter"        
%         [ecep_mat_high, ~] = crop_spectrum(ecep_mat_high, ct_vec, 1/f0_max, 1/f0_min);
%         
%         % combined
%         cep_comb = cep_mat_low + cep_mat_high;
%         % eac enhancement
%         ecep_comb = enhance_acf(cep_comb, ct_vec);     
%         % band pass "lifter"        
%         [ecep_comb, ct_vec] = crop_spectrum(ecep_comb, ct_vec, 1/f0_max, 1/f0_min);
%         pitch_mat = ecep_comb;
%         
        
%         % old calc
%         [cep_mat_old, ct_vec_old, ~] = fft_cepstrum(s_mat, f_vec, t_vec);
%         cep_mat_old = abs(cep_mat_old);
%         [cep_mat_old, ~] = crop_spectrum(cep_mat_old, ct_vec_old, 1/f0_max, 1/f0_min);
%         % augment contrasts using diff
%         abs_diff_mat = max(abs(diff(cep_mat_old([1 1:end],:),1,1)),...
%                            abs(diff(cep_mat_old([1:end end],:),1,1)));
%         aug_mat = (abs_diff_mat + 1).*(cep_mat_old + 1);      
%         pitch_mat = aug_mat;

        
            % band pass
            function [s_low, f_vec] = band_pass_spectrum(s_mat, f_vec, bpf_min, bpf_max)
                i_f_min = find(f_vec>=bpf_min,1,'first');
                i_f_max = find(f_vec<=bpf_max,1,'last');
                s_low = zeros(size(s_mat));
                s_low(i_f_min:i_f_max,:) = s_mat(i_f_min:i_f_max,:);
            end
            
            % highpass envelope
            function s_high = calc_x_high_envelope(s_mat, f_vec, bpf_max)
                i_f_max = find(f_vec<=bpf_max,1,'last');
                s_high = s_mat;
                s_high(1:i_f_max,:) = s_mat(1:i_f_max,:)./10^3;
            
                x_high = ispecgram(s_high, 256, 8000, 256, (256-80));
                x_high = x_high.*(x_high>0);
                [s_high, f_vec, ~] = simple_specgram(x_high, 8000);
                [s_high, ~] = band_pass_spectrum(s_high, f_vec, bpf_min, bpf_max);
            end
            
            % band pass
            function [s_out, f_vec] = crop_spectrum(s_mat, f_vec, bpf_min, bpf_max)
                % s_low
                i_f_min = find(f_vec>=bpf_min,1,'first');
                i_f_max = find(f_vec<=bpf_max,1,'last');
                s_out = s_mat(i_f_min:i_f_max,:);
                f_vec = f_vec(i_f_min:i_f_max);
            end
            
            % enhancement
            function cep_mat = enhance_acf(cep_mat, ct_vec)
                % eac
                cep_mat(cep_mat<0) = 0;
                cep2_mat = interp1(2.*ct_vec, cep_mat, ct_vec,'nearest',0);
                cep_mat = cep_mat - cep2_mat;
                cep_mat(cep_mat<0) = 0;
%                 cep3_mat = interp1(3.*ct_vec, cep_mat, ct_vec,'nearest',0);
%                 cep_mat = cep_mat - cep3_mat;
%                 cep_mat(cep_mat<0) = 0;
            end
            
            % scale matrix
            function out_mat = scale_mat(in_mat)
                in_mat = in_mat-min(in_mat(:));
                out_mat = in_mat./max(in_mat(:));
            end
            
    end
    
    % pitch matrix by harmonic product spectrum
    function [pitch_mat, new_f_vec, t_vec] = pitch_by_hps(s_mat, f_vec, t_vec)
        
        % consts
        f0_min = 65; %Hz
        f0_max = 650; %Hz
        max_harm = min(6, floor(f_vec(end)./f0_max));
        i_f_min = find(f_vec>=f0_min*max_harm,1,'first');
        i_f_max = find(f_vec<=f0_max*max_harm,1,'last');
        
        hps_mat = 1+zeros(size(s_mat));
        ind_final = (i_f_min):(i_f_max);
        pseudocount = 0.5/numel(ind_final);
        
        % product calculaton
        for i_harm = 1:max_harm
            ind_to_calc = floor(i_f_min*i_harm/max_harm):ceil(i_f_max*i_harm/max_harm);
            
            harm_mat = multipy_spec_mat(s_mat,f_vec, ind_to_calc,...
                                              max_harm./i_harm);
                                           
            % normalize
            harm_mat(ind_final,:) = harm_mat(ind_final,:)./...
                                    (repmat(sum(harm_mat(ind_final,:),1),[length(ind_final) 1]) + eps);
                                        
            hps_mat(ind_final,:) = hps_mat(ind_final,:).*(harm_mat(ind_final,:) + pseudocount);

        end  
            
        pitch_mat = hps_mat(ind_final,:);
        
        max_spec = max(pitch_mat(:));
        pitch_mat = pitch_mat./max_spec;
        pitch_mat(pitch_mat<(1/10000)) = 1/10000;
        pitch_mat = log10(pitch_mat)-log10(1/10000);
        
        new_f_vec = f_vec(ind_final)./max_harm;
 
        
        % multiplys and upsamples a spectrum mat
        function out_mat = multipy_spec_mat(in_mat, f_vec, ind_to_calc, n_harm)
            if n_harm~=1
                out_mat = interp1(n_harm.*f_vec(ind_to_calc),...
                                        in_mat(ind_to_calc,:),...
                                        f_vec,'nearest',0);
            else
                out_mat = in_mat;
            end
        end
    
    end
    
    % pitch matrix calculation based on cepstrum
    function [pitch_mat_comb, ct_vec, t_vec] = pitch_by_cepstrum_combined(s_mat, f_vec, t_vec)
        if isempty(s_mat) || ~iscell(f_vec) || ~isstruct(s_mat)...
                || ~any(strcmpi('powspec_256',f_vec)) || ~any(strcmpi('powspec_1024',f_vec))
            [pitch_mat_comb, ct_vec, t_vec] = deal(0);
            return;
        end
        
        pow_256 = s_mat.powspec_256;
        f_vec_256 = s_mat.f_vec_256;
        t_vec_80 =  s_mat.t_vec_80;
        
        rasta_mat = s_mat.rasta_mat;
        
        pow_1024 = s_mat.powspec_1024;
        f_vec_1024 = s_mat.f_vec_1024;
        t_vec_512 =  s_mat.t_vec_512;
        
        %%%%        
        [pitch_mat_256, ct_vec_256, ~] = pitch_by_cepstrum(pow_256, f_vec_256, t_vec_80);
%         pitch_mat_256_interp = interp1(1./ct_vec_256(end:-1:1), pitch_mat_256(end:-1:1,:),...
%                                     new_f_vec_1024);
        
        [pitch_mat_rasta, ~, ~] = pitch_by_cepstrum(rasta_mat, f_vec_256, t_vec_80);
%         pitch_mat_rasta_interp = interp2(t_vec_80, 1./ct_vec_256(end:-1:1), pitch_mat_rasta(end:-1:1,:),...
%                                     t_vec_80, new_f_vec_1024);

        [pitch_mat_hps, new_f_vec_1024, t_vec_512] = pitch_by_hps(pow_1024, f_vec_1024, t_vec_512);
        pitch_mat_hps_interp = interp1(t_vec_512', pitch_mat_hps', t_vec_80','nearest',0)';
        pitch_mat_hps_interp = interp1q(1./new_f_vec_1024(end:-1:1),...
                                pitch_mat_hps_interp(end:-1:1,:), ct_vec_256');
        pitch_mat_hps_interp(isnan(pitch_mat_hps_interp)) = 0;
                
%         [pitch_mat_1024 ct_vec_1024 t_vec_512] = pitch_by_cepstrum(pow_1024, f_vec_1024, t_vec_512);
%         pitch_mat_1024_interp = interp2(t_vec_512, 1./ct_vec_1024(end:-1:1), pitch_mat_1024(end:-1:1,:),...
%                                     t_vec_80, new_f_vec_1024);
       
                                
        pitch_mat_comb = (pitch_mat_hps_interp+0.01).*(pitch_mat_256+0.001).*...
                         (pitch_mat_rasta+0.001).*(0+0.001)./(0.01*0.001*0.001*0.001);
        ct_vec = ct_vec_256;
        t_vec = t_vec_80;
        
%         n_plots = 4;
%         figure; 
%         subplot(n_plots,1,1);
%         imagesc(t_vec,ct_vec_256,(pitch_mat_256+eps)); 
%         subplot(n_plots,1,2);
%         imagesc(t_vec,ct_vec_256,(pitch_mat_rasta+eps)); 
% %         subplot(n_plots,1,3);
% %         imagesc(t_vec_80,new_f_vec_1024,(pitch_mat_1024_interp+eps)); axis xy; colorbar;
%         subplot(n_plots,1,3);
%         imagesc(t_vec,ct_vec_256,(pitch_mat_hps_interp)); 
%         subplot(n_plots,1,4);
%         imagesc(t_vec,ct_vec_256,log(pitch_mat_comb)); 
%         linkaxes(findobj(gcf,'type','axes'));
        
    end

    function [pitch_f_out, pitch_a_out, t_vec_out] = pitch_contour_on_pitch_mat(pitch_mat, f_vec, t_vec)
        run_libs();
        f_vec = f_vec(:)';
        
        % first contour
        c_snr = 6;
        c_t_diff = 0.0005;
        f_range = [60 600];
        [pitch_f_out, ~, ~] = ...
            pitch_contour_on_pitch_mat_internal(pitch_mat, f_vec, t_vec, f_range, c_snr, c_t_diff);
        
        % second contour 
        % calc frequency range
        med_f = median(pitch_f_out);
        f_range = [0.5*med_f 2*med_f];
        % more demanding noise
        c_snr = 8;
        c_t_diff = 0.0005;
        [pitch_f_out, pitch_a_out, t_vec_out] = ...
            pitch_contour_on_pitch_mat_internal(pitch_mat, f_vec, t_vec, f_range, c_snr, c_t_diff);
        
        
            function [pitch_f_out, pitch_a_out, t_vec_out] = ...
                    pitch_contour_on_pitch_mat_internal(pitch_mat, f_vec, t_vec, f_range, crit_snr, crit_t_diff) 
                        
                % frequency
                if f_vec(end)>100 % frequencies
                    f_ind = find(((f_vec >= f_range(1)) & (f_vec <= f_range(2))));
                    [pitch_a, pitch_ind] = max(pitch_mat(f_ind,:),[],1);
                    pitch_a = log(pitch_a+eps);
                    
                    pitch_f = f_vec(f_ind(pitch_ind));
                else              % quefrencies
                    f_ind = find(((1./f_vec >= f_range(1)) & (1./f_vec <= f_range(2))));
                    
                    [pitch_a, pitch_ind] = max(pitch_mat(f_ind,:),[],1);
                    pitch_a = log(pitch_a+eps);
                    
                    pitch_f = 1./f_vec(f_ind(pitch_ind));
                end
                
                % smth wrong with the input
                if isempty(pitch_f)
                    [pitch_f_out, pitch_a_out, t_vec_out] = deal(0);
                    return;
                end
                
                % remove noise
                [pitch_f_out, pitch_a_out, t_vec_out] = ...
                            filter_by_diff_and_amp(pitch_f, pitch_a, t_vec, crit_snr, crit_t_diff);

                [pitch_f_out, pitch_a_out, t_vec_out] = ...
                            filter_by_diff_and_amp(pitch_f_out, pitch_a_out, t_vec_out, crit_snr, crit_t_diff);

                % remove outliers
                [pitch_f_out, pitch_a_out, t_vec_out] = ...
                            remove_outliers(pitch_f_out, pitch_a_out, t_vec_out);
                        
%                 % debug plots
%                 figure; hold on;
%                 imagesc(t_vec, f_vec, log(pitch_mat+eps), 'alphadata', 0.3); axis tight; axis ij; colorbar;
%                 plot(t_vec_out, 1./pitch_f_out, '.-');
      
            end            
        
            % remove noise by using frequency diff and amplitude
            function [pitch_f_out, pitch_a_out, t_vec_out] =...
                    filter_by_diff_and_amp(pitch_f_in, pitch_a_in, t_vec_in, crit_snr, crit_diff)
                
                pitch_ct_in = 1./pitch_f_in;
                pitch_diff = max(abs(diff([pitch_ct_in(1) pitch_ct_in])),abs(diff([pitch_ct_in pitch_ct_in(end)])));

                % stability + amplitude criteria   
                crit_raw = (1 + (pitch_a_in - crit_snr)./abs(crit_snr)).*...
                        (2*crit_diff./(pitch_diff + crit_diff));

                % smoothing the ciriteria
                crit_smooth = stats_utils_fh.gap_vector_smoothing(crit_raw>1, 2, 0.6);
                
                % build back the edges
                diff_crit = diff(crit_smooth);
                crit_smooth((abs([diff_crit 0])==1) | (abs([0 diff_crit])==-1)) = 1;

                % nulling the pitch contour
                pitch_f_out = pitch_f_in(crit_smooth>=1);
                pitch_a_out = pitch_a_in(crit_smooth>=1);
                t_vec_out = t_vec_in(crit_smooth>=1);

            end
            
            function [pitch_f, pitch_a, t_vec] =...
                    remove_outliers(pitch_f, pitch_a, t_vec)
                
                ct_vec = 1./(pitch_f+eps);
                pctl_vec = deal(quick_prctile(ct_vec, [25 50 75]));
%                 out_range = pctl_vec(3) - pctl_vec(1);
%                 
%                 outlier_ind = abs(ct_vec - pctl_vec(2)) > out_range; 
                outlier_ind = abs(ct_vec - pctl_vec(2)) > pctl_vec(2);

                % nulling the pitch contour
                pitch_f(outlier_ind) = [];
                pitch_a(outlier_ind) = [];
                t_vec(outlier_ind) = [];
            end
    end
        
    % rasta filtering with parameters using IIR filter
    function y = param_rastafilt_fir(x, f1, f2, t_vec)
    % the modulation bandpass filter itself
    
        filt_nfft = 128;
        
        % return if input is too short
        if (size(x, 2) < filt_nfft)
            y = x;
            return;
        end
        
        % calc timeseries sample rate and frequency vector
        d_t = diff(t_vec);
        filt_fs = 1./min(d_t(d_t>0));
        filt_f_vec = filt_fs/2*linspace(0,1,filt_nfft/2+1);
        
        % return if input time resolution is not fit for filtering
        if (f2/filt_f_vec(end) >= 1) || ...
                    isempty(filt_fs)
            y = x;
            return;
        end
                 
        %  filter design
        if f1==0 % lowpass
            fir1_b = fir1(filt_nfft, f2./filt_f_vec(end));
        elseif f2==0 % highpass
            fir1_b = fir1(filt_nfft, f1./filt_f_vec(end),'high');
        else
            fir1_b = fir1(filt_nfft, [f1 f2]./filt_f_vec(end));
        end
        
        % filter
        y = simple_filt_filt(fir1_b, 1, x, filt_nfft);
                    
    end 
    
    % rasta filtering with parameters using FIR filter
    function y = param_rastafilt_iir(x, f1, f2, t_vec)
    % the modulation bandpass filter itself
        
% %         % original rasta filter
% %         numer = [-2:2];
% %         numer = -numer ./ sum(numer.*numer);
% %         denom = [1 -0.94];
% %         [y,z] = filter(numer, 1, x(:,1:(5-1))',[],1);
% %         y = 0*y';
% %         y = [y,filter(numer, denom, x(:,5:end)',z,1)'];
        
        
        filt_order = 6; %the filter returns a 2*n order filter
        
        %  filter design
        filt_fs = round(length(t_vec)./(t_vec(end)-t_vec(1)));
        [numer, denom] = cheby1(filt_order./2, 1, [f1 f2]./(filt_fs./2));
       
        % filter
        y = simple_filt_filt(numer, denom, x, filt_order*30);
        
    end   
    
    % filters forward and backward to get zero phase and no transients
    function y = simple_filt_filt(numer, denom, x, mirror_length)

        row_len = size(x,2);
        
        % mirror the end and beginning
        if mirror_length > (row_len - 2)
            mirror_length = row_len-2;
        end
        x = x.';
        
        x_temp = [x((mirror_length+1):-1:2, :); x; x((end-1):-1:(end-mirror_length), :)];
        
        % forwards
        [x_temp, ZF] = filter(numer, denom, x_temp);
%         clear x_temp;
        
        % backwards
        x_temp = x_temp(end:-1:1,:);
        [y, ~] = filter(numer, denom, x_temp, ZF);
                
        % flip & trim
        y = y(end:-1:1, :); %flip
        y = y((mirror_length+1):(mirror_length + row_len), :); %trim 
        
        y = y.';
        
%         % forwards
%         y_temp = zeros(size(x_temp));
%         zf = zeros(size(x_temp,1),length(numer)-1);
%         for col_i=1:size(x,1)
%             [y_temp(col_i,:), zf(col_i,:)] = filter(numer, denom, x_temp(col_i,:));
%         end
% 
%         y = zeros(size(x_temp));
% %         clear x_temp;
%         % backwards
%         for col_i=1:size(x,1)
%             [y(col_i,:), ~] = filter(numer, denom, y_temp(col_i,end:-1:1), zf(col_i,:));
%         end
% 
%         y = y(:, end:-1:1); %flip
%         y = y(:, (mirror_length+1):(mirror_length + size(x,2))); %trim 
    end

    function [src, filt, f_vec, t_vec] = vocal_source_filter_separation(signal, fs,...
                                                    spectrum, powspec, f_vec, t_vec)
        lpc_order = 11;
        
%         wsize = 256;
%         step = 80;
% 
% 
%         % Window the signal
%         [first, last, prefix, postfix] = slidewin(numel(signal), wsize, step, wsize);
%         assert(all(prefix == 0));
%         last_full_frame = find(postfix == 0, 1, 'last');
% 
%         % Calculate full frames
%         [firsts, deltas] = meshgrid(first(1:last_full_frame), (0:(wsize-1)));
%         idx_mat = firsts + deltas;
%         wsignal = signal(idx_mat) .* repmat(hamming(wsize), [1, size(idx_mat, 2)]);
% %         [spectrum, f_vec, t_vec] = quick_spectrogram(signal,256,256-80,256,fs);
%         lpc_coeff = lpc(wsignal, lpc_order).';

        lpc_coeff = lpc_on_ready_powspec(powspec, lpc_order).';
        
%         % Add postfix partial frames
%         start_idx = last_full_frame + 1;
%         end_idx = start_idx-1 + find(last(start_idx:end)+1 - first(start_idx:end) > lpc_order, 1, 'last');
%         for idx = start_idx : end_idx
%             lpc_coeff(:, idx) = lpc(in_mat.signal(first(idx) : last(idx)).' .* hamming(last(idx)+1 - first(idx)), lpc_order).';
%         end

        % Create frequency response for each frame's filter
        % fast_allpole_freqz = @(B, A, f, fs) B ./ (exp(-1j*2*pi*f/fs * (0:(size(A, 1)-1))) * A);
        filt = fast_allpole_freqz(1, lpc_coeff, f_vec, fs);
        
        % output filter
        time_len = min(size(spectrum, 2), size(filt, 2));
        src = spectrum(:, 1:time_len) ./ (filt(:, 1:time_len)+eps);
        
        % feature extraction friendly real values
%         matmoodies(ispecgram(src, 256, 800, 256, 256-80), 8000);
        src = abs(src);
        filt = abs(filt);
        
        % remove nans
        if isnan(sum(sum(filt,1))) % faster 
            nan_mat = isnan(filt);
            src(nan_mat) = 0;
            filt(nan_mat) = 0;
        end
        
        % t_vec = (first(1:time_len)-1) / fs;
        	
	    function samples = fast_allpole_freqz(B, A, f, fs)
            
	            % allpole filter order (number of poles)
	            m = size(A, 1);
            
	            % Create z^-m matrix (f - colvec, 0:m-1 - rowvec)
	            z_mat = exp(-1j*2*pi*f/fs * (0:(m-1)));
            
	            % sample the filter at the requested frequencies
	            samples = B ./ (z_mat * A);
	    end
	    
            % custom version of LPC, utilizing the input spectrum calculation
            function [a,e] = lpc_on_ready_powspec(powspec, N)

                % Compute autocorrelation vector or matrix
                powspec = [powspec(1:(end-1),:); powspec(end:-1:2,:)];
%                 powspec = [powspec; powspec(end:-1:2,:)];
%                 X = fft(x,2^nextpow2(2*size(x,1)-1));
                m = 2^nextpow2((size(powspec,1)-1));
                R = ifft(powspec,m);
                R = R./m; % Biased autocorrelation estimate

                [a,e] = levinson(R,N);

                % Return only real coefficients for the predictor if the input is real
                for k = 1:size(powspec,2)
%                     if isreal(x(:,k))
                        a(k,:) = real(a(k,:));
%                     end
                end
            end
        
    end

    function [Intensity, Freq, Time] = harmonics_mat(F0, F0Time, PowSpec, SpecFreq, SpecTime, Ordinals, WidthRatio, MinFreq, MaxFreq)
        
        [HarmIdx, ~, ~, ~] = InlineAlgoOps('GetHarmonicNeighb', F0, F0Time, Ordinals, WidthRatio, SpecFreq, SpecTime, MinFreq, MaxFreq);

        Intensity = zeros(size(PowSpec));
        Idx = HarmIdx(~isnan(HarmIdx(:)));
        Intensity(Idx) = PowSpec(Idx);
        
        Freq = SpecFreq;
        Time = SpecTime;

    end

    function [Dist, Freq, Time] = distortion(F0, F0Time, PowSpec, SpecFreq, SpecTime, Ordinals, WidthRatio, MinFreq, MaxFreq)

        [HarmEnergy, ~, TimeIdx, Time] = InlineAlgoOps('TimbreEnergy', F0, F0Time, PowSpec, SpecFreq, SpecTime, Ordinals, WidthRatio, MinFreq, MaxFreq);

        if isempty(HarmEnergy)
            Dist = [];
            Freq = [];
            return;
        end
        
        rowvec = @(x)x(:)';
        HarmFrameEnergy = rowvec(sum(HarmEnergy, 1));

        minmax_idx = interp1(SpecFreq, 1:numel(SpecFreq), [MinFreq, MaxFreq], 'nearest');
        
        TotalFrameEnergy = sum(PowSpec(minmax_idx(1) : minmax_idx(2), TimeIdx), 1);

        Dist = HarmFrameEnergy ./ TotalFrameEnergy;
        Freq = 0;
    end
    
    % specgram using default parameters
    function [spectrum, f_vec, t_vec] = simple_specgram(signal, fs)

        % simple spectrogram and power spectrum calculation wrapper
        wintime = 0.032;
        steptime = 0.010;

        winpts = round(wintime*fs);
        steppts = round(steptime*fs);

        NFFT = 2^(ceil(log(winpts)/log(2)));
        WINDOW = hann(winpts,'periodic')';
        NOVERLAP = winpts - steppts;
        SAMPRATE = fs;

%         [spectrum, f_vec, t_vec] = spectrogram(signal,WINDOW,NOVERLAP,NFFT,SAMPRATE);
        [spectrum, f_vec, t_vec] = quick_spectrogram(signal,WINDOW,NOVERLAP,NFFT,SAMPRATE);

    end
    
    % specgram with custom parameters
    function [spectrum, f_vec, t_vec] = param_specgram(signal, fs, winpts, steppts)

        NFFT = winpts;
        WINDOW = [hann(winpts,'periodic')'];
        NOVERLAP = winpts - steppts;
        SAMPRATE = fs;
        
%         [spectrum, f_vec, t_vec] = spectrogram(signal,WINDOW,NOVERLAP,NFFT,SAMPRATE);
        [spectrum, f_vec, t_vec] = quick_spectrogram(signal,WINDOW,NOVERLAP,NFFT,SAMPRATE);

    end
   
    % a quicker spectrogram with fewer argument checks and internal calls
    function [y, f, t] = quick_spectrogram(x,win,noverlap,nfft,Fs)        
        try
            % Window length
            nwind = length(win);

%             f = psdfreqvec('npts',nfft,'Fs',Fs,'Range','half');
            f = get_f_vec(nfft,Fs);
            num_f = length(f);
            
            % Make x and win into columns
            x = x(:); 
            win = win(:); 
            
            ncol = fix((length(x)-noverlap)/(nwind-noverlap));
            colindex = (0:(ncol-1))*(nwind-noverlap)+1;

            % Calculate full frames
            [firsts, deltas] = meshgrid(colindex(1:ncol), (0:(nwind-1)));
            idx_mat = firsts + deltas;
            xin = x(idx_mat) .* repmat(win, [1, size(idx_mat, 2)]); 
            
            % Compute the raw STFT with the appropriate algorithm
%             [y,f] = computeDFT(xin,nfft,Fs);
            y_full = fft(xin,nfft);   
            y = y_full(1:num_f,:); % one sided            

            % colindex already takes into account the noverlap factor; Return a T
            % vector whose elements are centered in the segment.
            t = ((colindex)+((nwind)/2)')/Fs; 
            
        catch ex
            disp(['basic_features:quick_spectrogram: ' ex.message]);
            [y, f, t] = spectrogram(x,win,noverlap,nfft,Fs);
        end      
        
            function w = get_f_vec(nfft, fs)
                freq_res = fs/nfft;
                w = freq_res*(0:nfft-1);

                halfNPTS = (nfft/2)+1;
                w(halfNPTS) = fs/2;

                w = w(1:halfNPTS);
                w = w(:);                
            end
    end        
        
    % inverse specgram
    function output_signal = ispecgram(s_mat, nfft, fs, window, overlap)
    
        
        s_mat = [s_mat; conj(s_mat(end:-1:2,:))];
        [Nf,Nw] = size(s_mat);
        
        if ~exist('nfft','var') || isempty(nfft)
            nfft = 2*(Nf-1);
        end
        
        if ~exist('window','var') || isempty(window)
            window = nfft;     
        end
        
        if length(window)==1           
            window_length = window;
            window=hann(window_length,'periodic');
        else
            window_length = length(window);
            window = window(:);
        end
            
        if ~exist('overlap','var') || isempty(overlap)
            overlap = nfft/2;
        end
        step = window_length - overlap;
               
        win_ind=mod((1:window_length)'-1,Nf)+1;
        output_signal=zeros((Nw-1)*step+window_length,1);
        window_norm=zeros((Nw-1)*step+window_length,1);

        for ind_col=1:Nw %Overlapp add technique
            signal_index=((ind_col-1)*step)+(1:window_length);
            temp_ifft=real(ifft(s_mat(:,ind_col)));
            output_signal(signal_index)= output_signal(signal_index)+...
                                                temp_ifft(win_ind).*window.*nfft/window_length;
            window_norm(signal_index)= window_norm(signal_index)+window;
        end
        
        window_norm(window_norm<0.1) = 0.1;
        output_signal = output_signal./(window_norm+eps);
%         output_signal = output_signal*window_length/nfft;
    end
    
    % overlap add inverse specgram with time signal similarity measure
    function sig_out = ispecgram_simil(spec_mat, nfft, step)
        
        [nspec,ncol] = size(spec_mat);
        if nargin < 2
            nfft = (nspec-1).*2;
        end
        
        if nspec ~= (nfft/2)+1
            error('number of rows should be fnfft/2+1')
        end
        
        if nargin < 3
            step = 80;
        end
        
        % lengths
        win_len = nfft;
        max_err = ceil(step/2);
        sig_len = nfft + (ncol-1)*(step+max_err);

        % vectors
        win_vec = hann(win_len,'periodic');
        sig_out = zeros(sig_len,1);
        win_out = zeros(sig_len,1);
        
        % loop init
        prev_seg = zeros(nfft,1);
        cursor_out = 1;

        for i_col = 1:ncol
            ft = spec_mat(:,i_col);
            ft = [ft(1:(nfft/2+1)); conj(ft((nfft/2):-1:2))];
            new_seg = real(ifft(ft));
            
            shift = min_amdf_similarity(...
                    prev_seg((end-step+1):end), new_seg(1:step), max_err);
            
            % adjust cursor place
            cursor_out = max(1, cursor_out - shift);
            
            % overlap add
            sig_out(cursor_out:(cursor_out+win_len-1)) = ...
                    sig_out(cursor_out:(cursor_out+win_len-1)) + new_seg;

            % overlap add window normalization vec
            win_out(cursor_out:(cursor_out+win_len-1)) = ...
                    win_out(cursor_out:(cursor_out+win_len-1)) + win_vec;            

            % move cursors
            cursor_out = cursor_out + step;
        end

        % remove slack
        sig_out((cursor_out+win_len):end) = [];
        win_out((cursor_out+win_len):end) = [];
        win_out(win_out<0.1) = 0.1;
        sig_out = sig_out./(win_out+eps); %normalize to remove possible modulations
        
        % amdf based similarity calculation
        function shift = min_amdf_similarity(seg1, seg2, max_lag)
            n = length(seg1);
            amdf = ones(1,2*max_lag-1);
            for lag=-max_lag:max_lag
                amdf(lag+max_lag+1) = sum(abs(seg2(max(1,(-lag+1)):min(n,(n-lag)))-...
                               seg1(max(1,(lag+1)):min(n,(n+lag))) ))/n;
            end
            [~, min_i] = min(amdf);
            shift = min_i - max_lag;
        end
        
    end
    
    % create sound from image
    function [signal, fs] = synth_sound_from_image(imfilename, params)
        im = imread(imfilename); 
        X =  rgb2ind(im,jet);
%         X =  rgb2gray(im);
        if nargin < 2
            opt.min_f = 0;
            opt.max_f = 4000;
            opt.f_scale = 'linear'; %log
            opt.time = 5;
            opt.phase = 'noise';
        end
        X = flipdim(X,1);
        n_freq = size(X, 1);
        n_frames = size(X, 2);
        
        fs = ceil(opt.max_f.*2);
        nfft = (n_freq-1).*2;
        
        sig_len = max(opt.time.*fs, n_frames.*nfft);
        n_step = ceil(min(0.25*nfft,(sig_len-nfft)./(n_frames)));
        n_over = nfft - n_step;
        
        X = 10.^(double(X)./10); % to linear amp scale
        X = X.*exp(1i.*2*pi*rand(size(X)));
        
%         signal = ispecgram(X, nfft, fs, nfft, n_over);
        signal = ispecgram_simil(X, nfft, n_step);        
        
    end
    
    % original rasta code
    function y = rastafilt_orig(x)
        % y = rastafilt(x)
        %
        % rows of x = critical bands, cols of x = frame
        % same for y but after filtering
        % 
        % default filter is single pole at 0.94

        % rasta filter
        numer = -2:2;
        numer = -numer ./ sum(numer.*numer);
        denom = [1 -0.94];

        % Initialize the state.  This avoids a big spike at the beginning 
        % resulting from the dc offset level in each band.
        % (this is effectively what rasta/rasta_filt.c does).
        % Because Matlab uses a DF2Trans implementation, we have to 
        % specify the FIR part to get the state right (but not the IIR part)
        %[y,z] = filter(numer, 1, x(:,1:4)');
        % 2010-08-12 filter() chooses the wrong dimension for very short
        % signals (thanks to Benjamin K otric1@gmail.com)
        [y,z] = filter(numer, 1, x(:,1:4)',[],1);
        % .. but don't keep any of these values, just output zero at the beginning
        y = 0*y';

        % Apply the full filter to the rest of the signal, append it
        y = [y,filter(numer, denom, x(:,5:end)',z,1)'];
        %y = [y,filter(numer, denom, x(:,5:end)',z)'];
    end   
    
    % original cepstrum 
    function [cep,dctm] = spec2cep(spec, ncep, type)
        % [cep,dctm] = spec2cep(spec, ncep, type)
        %     Calculate cepstra from spectral samples (in columns of spec)
        %     Return ncep cepstral rows (defaults to 9)
        %     This one does type II dct, or type I if type is specified as 1
        %     dctm returns the DCT matrix that spec was multiplied by to give cep.
        % 2005-04-19 dpwe@ee.columbia.edu  for mfcc_dpwe

        if nargin < 2;   ncep = 13;   end
        if nargin < 3;   type = 2;   end   % type of DCT

        [nrow, ncol] = size(spec);

        % Make the DCT matrix
        dctm = zeros(ncep, nrow);
        if type == 2 || type == 3
          % this is the orthogonal one, the one you want
          for i = 1:ncep
            dctm(i,:) = cos((i-1)*[1:2:(2*nrow-1)]/(2*nrow)*pi) * sqrt(2/nrow);
          end
          if type == 2
            % make it unitary! (but not for HTK type 3)
            dctm(1,:) = dctm(1,:)/sqrt(2);
          end
        elseif type == 4 % type 1 with implicit repeating of first, last bins
          % Deep in the heart of the rasta/feacalc code, there is the logic 
          % that the first and last auditory bands extend beyond the edge of 
          % the actual spectra, and they are thus copied from their neighbors.
          % Normally, we just ignore those bands and take the 19 in the middle, 
          % but when feacalc calculates mfccs, it actually takes the cepstrum 
          % over the spectrum *including* the repeated bins at each end.
          % Here, we simulate 'repeating' the bins and an nrow+2-length 
          % spectrum by adding in extra DCT weight to the first and last
          % bins.
          for i = 1:ncep
            dctm(i,:) = cos((i-1)*[1:nrow]/(nrow+1)*pi) * 2;
            % Add in edge points at ends (includes fixup scale)
            dctm(i,1) = dctm(i,1) + 1;
            dctm(i,nrow) = dctm(i,nrow) + ((-1)^(i-1));
          end
          dctm = dctm / (2*(nrow+1));
        else % dpwe type 1 - same as old spec2cep that expanded & used fft
          for i = 1:ncep
            dctm(i,:) = cos((i-1)*[0:(nrow-1)]/(nrow-1)*pi) * 2 / (2*(nrow-1));
          end
          % fixup 'non-repeated' points
          dctm(:,[1 nrow]) = dctm(:, [1 nrow])/2;
        end  

        cep = dctm*log(spec);
    end
    
    % original liftering function 
    function y = lifter(x, lift, invs)
        % y = lifter(x, lift, invs)
        %   Apply lifter to matrix of cepstra (one per column)
        %   lift = exponent of x i^n liftering
        %   or, as a negative integer, the length of HTK-style sin-curve liftering.
        %   If inverse == 1 (default 0), undo the liftering.
        % 2005-05-19 dpwe@ee.columbia.edu

        if nargin < 2;   lift = 0.6; end   % liftering exponent
        if nargin < 3;   invs = 0; end      % flag to undo liftering

        [ncep, nfrm] = size(x);

        if lift == 0
          y = x;
        else

          if lift > 0
            if lift > 10
              disp(['Unlikely lift exponent of ', num2str(lift),' (did you mean -ve?)']);
            end
            liftwts = [1, ([1:(ncep-1)].^lift)];
          elseif lift < 0
            % Hack to support HTK liftering
            L = -lift;
            if (L ~= round(L)) 
              disp(['HTK liftering value ', num2str(L),' must be integer']);
            end
            liftwts = [1, (1+L/2*sin([1:(ncep-1)]*pi/L))];
          end

          if (invs)
            liftwts = 1./liftwts;
          end

          y = diag(liftwts)*x;

        end
    end
    
    % original cepstrum to spectrum 
    function [spec,idctm] = cep2spec(cep, nfreq, type)
        % spec = cep2spec(cep, nfreq, type)
        %   Reverse the cepstrum to recover a spectrum.
        %   i.e. converse of spec2cep
        %   nfreq is how many points to reconstruct in spec
        % 2005-05-15 dpwe@ee.columbia.edu

        if nargin < 2;   nfreq = 21;   end
        if nargin < 3;   type = 2;   end   % type of DCT

        [ncep,ncol] = size(cep);

        % Make the DCT matrix
        dctm = zeros(ncep, nfreq);
        idctm = zeros(nfreq, ncep);
        if type == 2 || type == 3
          % this is the orthogonal one, so inv matrix is same as fwd matrix
          for i = 1:ncep
            dctm(i,:) = cos((i-1)*[1:2:(2*nfreq-1)]/(2*nfreq)*pi) * sqrt(2/nfreq);
          end
          if type == 2 
            % make it unitary! (but not for HTK type 3)
            dctm(1,:) = dctm(1,:)/sqrt(2);
          else
            dctm(1,:) = dctm(1,:)/2;    
          end
          idctm = dctm';
        elseif type == 4 % type 1 with implicit repetition of first, last bins
          % so all we do is reconstruct the middle nfreq rows of an nfreq+2 row idctm
          for i = 1:ncep
            % 2x to compensate for fact that only getting +ve freq half
            idctm(:,i) = 2*cos((i-1)*[1:nfreq]'/(nfreq+1)*pi);
          end
          % fixup 'non-repeated' basis fns 
          idctm(:, [1 ncep]) = idctm(:, [1 ncep])/2;
        else % dpwe type 1 - idft of cosine terms
          for i = 1:ncep
            % 2x to compensate for fact that only getting +ve freq half
            idctm(:,i) = 2*cos((i-1)*[0:(nfreq-1)]'/(nfreq-1)*pi);
          end
          % fixup 'non-repeated' basis fns 
          idctm(:, [1 ncep]) = 0.5* idctm(:, [1 ncep]);
        end  

        spec = exp(idctm*cep);
    end
    
    
end