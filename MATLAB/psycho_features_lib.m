function varargout = psycho_features_lib(varargin)
% library file for psychacoustics-related features such as loudness,
% distortion, dissonance etc.

psycho_feat_fh = [];

f_handles = struct(... % recommended way to pass nested func handles
	'TonalDissonance', @TonalDissonance, ...
    'twelfth_oct_fft', @twelfth_oct_fft ...
         );
     
if nargin == 0
    varargout{1} = f_handles;
else
    if nargout>0
        [varargout{1:nargout}] = feval(eval(['@' varargin{1}]),varargin{2:end});
    else
        feval(eval(['@' varargin{1}]),varargin{2:end});
    end
end

    % run libs
    function run_libs()
        if isempty(psycho_feat_fh)
            psycho_feat_fh = psycho_feat_fh();
        end
    end

    % Tonal dissonance
    function [DissonanceHK, DissonanceS, tonal_map_full, f_vec_tonal_map] = TonalDissonance(spec256, f_vec, fs)
        
        winsize = 256;
        intensity = spec256 / ((0.805*(10^-5)*1.8119).*winsize^2);

        [freq_mat, inten_mat, tonal_map_full, f_vec_tonal_map] = ExtractTonalComponents(...
                                                        intensity, f_vec, fs, winsize);

        [DissonanceHK, DissonanceS] = CalculateToneDissonance(freq_mat, inten_mat);
        
            function [freq_mat, inten_mat, inten_mat_full, f_vec] = ExtractTonalComponents(Intensity, f_vec, fs, n_win)
            %extracts tonal components from the lowest 1/4 of the Fourier spectrum}

                % Adapted to work with 256-spectrogram

                % perform all the tests
                fi_s = 4;
                fi_e = 64;
                f_vec = f_vec(fi_s:(fi_e-3));
                Intensity((fi_e+1):end ,:) = []; % remove higher frequencies for performance
                dBIntensity = 10*log10(Intensity) + 104.11;

                % Test0 - Each partial must be > 10dB
                Test0 = dBIntensity(fi_s : (fi_e-3), :) > 10;

                % Test1 - Each partial must be a local peak
                Diff = dBIntensity((fi_s-1) : (fi_e-3), :) < dBIntensity(fi_s : (fi_e-2), :);
                Test1 = Diff(1:(end-1), :) & ~Diff(2:end, :);

                % Test2 - each partial is at least 7dB higher than samples not directly
                % neighboring it
                Diff = dBIntensity((fi_s-2) : (fi_e-3), :) - dBIntensity(fi_s : (fi_e-1), :);
                Test2 = (Diff(1:(end-2), :) <= -7) & (Diff(3:end, :) >= 7);
                NeighbDiff = -Diff(2 : (end-1), :);

                % Test3 - same as Test2 for 3 samples away from the partial
                Diff = dBIntensity((fi_s-3) : (fi_e-3), :) - dBIntensity(fi_s : (fi_e), :);
                Test3 = (Diff(1:(end-3), :) <= -7) & (Diff(4:end, :) >= 7);

                % Merge the tests
                Test = Test0 & Test1 & Test2 & Test3;

                % create sparse matrices of intensity and frequency
                dBIntensity([1:(fi_s-1) (fi_e-2):end] ,:) = [];
                inten_mat_full = zeros(size(NeighbDiff));
                inten_mat_full(Test) = dBIntensity(Test) + 1.6;  
                rows_mat = fi_s - 1 + repmat((1:size(dBIntensity,1))', [1 size(dBIntensity,2)]);
                freq_mat_full = zeros(size(NeighbDiff));
                freq_mat_full(Test) = rows_mat(Test) * fs / n_win + 0.46 * NeighbDiff(Test);

                % collapse sparse matrices to have only short matrices of only tones
                num_tones_vec = sum(Test,1);
                inten_mat = zeros(max(num_tones_vec), size(Test, 2));
                freq_mat = zeros(max(num_tones_vec), size(Test, 2));
                % done in a loop for different numbers of partial tones
                for n_tones = 1:max(num_tones_vec) 
                    col_inds = num_tones_vec == n_tones;  
                    inten_mat_partial = inten_mat_full(:, col_inds);
                    inten_mat(1:n_tones, col_inds) =...
                        reshape(inten_mat_partial(Test(:, col_inds)), n_tones, nnz(col_inds));
                    freq_mat_partial = freq_mat_full(:, col_inds);
                    freq_mat(1:n_tones, col_inds) =...
                        reshape(freq_mat_partial(Test(:, col_inds)), n_tones, nnz(col_inds));
                end

            end
        
            % calculates HK ans S dissonance on the collapsed frequencies and intensities
            function [DissHK, DissS] = CalculateToneDissonance(freq_mat, inten_mat)

                num_tones = size(freq_mat, 1);

                % indexes to be used for tones pair calculations
                % two index vectors one is [1 1 1 2 2 2 3 3 3] and the other
                % is [1 2 3 1 2 3 1 2 3] - so all combinations of pairs are one
                % against the other
                ind_mult1 = repmat(1:num_tones, [1 num_tones]);
                ind_mult2 = repmat(1:num_tones, [num_tones 1]);
                ind_mult2 = ind_mult2(:)';
                ind_diag = ind_mult1==ind_mult2; % for the diag / non-diag poerations
                ind_non_diag = ind_mult1~=ind_mult2; % precalced to save ~ind_diag operation

                % create pairs of the tone frequencies
                mult1_freq_mat = freq_mat(ind_mult1,:); 
                mult2_freq_mat = freq_mat(ind_mult2,:);             

                % calculate freq diff relative to the CBW at that frequency
                diffs = abs(mult1_freq_mat - mult2_freq_mat); % pairs difference
                means = 0.5.*(mult1_freq_mat + mult2_freq_mat); % pairs means
                log_crit_band = log(diffs) - 0.65.*log(means) - log(1.72); % in log form to save calc time

                % ignore differences which are too high
                not_in_range_ind = ~(log_crit_band < log(1.2));

                % calc each pair's dissonance g(y) as a polynomial 
                diss_poly = ...
                    5.439061859 * exp(5*log_crit_band) ...
                    - 25.24247636 * exp(4*log_crit_band) ...
                    + 44.07577904 * exp(3*log_crit_band) ...
                    - 34.4678618 * exp(2*log_crit_band)  ...
                    + 10.2345604 * exp(log_crit_band)  ...
                    + 5.012466409E-03; % exp(a*log(x)) is faster than x.^a

                diss_poly(diss_poly>1) = 1; % clip so not greater than 1
                % not in range places will not be summed later 
                % because will be zeros in the multiplication
                diss_poly(not_in_range_ind) = 0; 
                
                % Calculate (squared) amplitude of each partial above hearing thresh
                norm_freq = freq_mat / 1000;
                thresh_mat = 3.64*exp(-0.8.*log(norm_freq)) ...
                    - 6.5*exp(-0.6*((norm_freq-3.3).^2)) ...
                    + 1e-3*exp(4*log(norm_freq));
                asq_mat = 10.^((inten_mat - thresh_mat)/10) / 1e4;

                % Create tone pairs of amplitude and the numerator, denominator
                asq_sq_mat = sqrt(asq_mat);

                % Take the diagonal once and the out-of-diagonal twice
                % all-pairs multiplication using ind_mult1/2 index vectors
                DissHK = 0.5 .* sum(asq_sq_mat(ind_mult1,:).*...
                                    asq_sq_mat(ind_mult2,:).*...
                                        diss_poly, 1) ./ sum(asq_mat, 1);
                % 0.5 factor because off-diag pairs are summed twice
                
                % Sethares
                db_asq_mat = 10*log10(asq_mat + 1e-32) / 20;
                db_asq_mat(db_asq_mat < 0) = 0;

                % again all-pair multiplication
                mult_db_asq_mat = db_asq_mat(ind_mult1,:).*db_asq_mat(ind_mult2,:);      
                mult_db_asq_mat(not_in_range_ind) = 0;            

                % more pair calculations for future all-pair multiplication
                mult1_s = 0.24.* diffs ./ (0.0207 * mult1_freq_mat + 18.96);
                mult2_s = 0.24.* diffs ./ (0.0207 * mult2_freq_mat + 18.96);
                mult1_a = exp(-3.5 * mult1_s ) - exp(-5.75 * mult1_s);
                mult2_a = exp(-3.5 * mult2_s) - exp(-5.75 * mult2_s);

                % the summation           
                DissS = sum(mult_db_asq_mat(ind_diag,:).*...
                                    mult1_a(ind_diag,:), 1) ... 
                      + 0.5.* sum(mult_db_asq_mat(ind_non_diag,:).*...
                                        mult1_a(ind_non_diag,:), 1) ... 
                      + 0.5.* sum(mult_db_asq_mat(ind_non_diag,:).*...
                                        mult2_a(ind_non_diag,:), 1);  
                % 0.5 factor because off-diag pairs are summed twice

                % remove nans and values for single partial tones (should be zero)
                ind_rem = sum(logical(freq_mat))<2;
                DissHK(ind_rem) = 0;
                DissS(ind_rem) = 0;

            end
        
    end % function TonalDissonance()
 
    function [spec_12thoct, f_vec_12thoct] = twelfth_oct_fft(spec1024, f_vec_1024)
        
        % Note that 4*power2dB is equivalent to 40*log10, which yields 
        % integer steps for each 1/12 oct band.
        % frequencybands = round(4*power2dB((fs/N) * (0:N2)))-63;        
        
        % Start at 60 Hz band
        n_start = find(f_vec_1024>60,1,'first');
        frequencybands = round(4*10*log10(f_vec_1024));
        frequencybands = frequencybands - frequencybands(n_start) + 1;
        
        % remove empty bands (those that have no frequencies mapped to them)
        frequencybands((n_start+1):end) = frequencybands((n_start+1):end)-cumsum(diff(frequencybands(n_start:end))>1);
        numberofbands = frequencybands(end);

        % initialise output spectrum
        twelfthoctspectrum = zeros(numberofbands, size(spec1024,2));

        % Sum power spectrum components within each 1/12-octave band
        for n = n_start:numel(f_vec_1024)
            twelfthoctspectrum(frequencybands(n),:) =...
                twelfthoctspectrum(frequencybands(n),:) + spec1024(n,:);
        end

%         spec_12thoct = twelfthoctspectrum ./ (1024/65536)^2;

        spec_12thoct = abs(twelfthoctspectrum);
        spec_12thoct(spec_12thoct < eps) = eps;
%         spec_12thoct = 10*log10(spec_12thoct) + 0.66;
        spec_12thoct = 10*log10(spec_12thoct);

        f_vec_12thoct = 1:numberofbands;
    end

     
end % psycho_feature_lib