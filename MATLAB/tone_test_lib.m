function varargout = tone_test_lib(varargin)
% Library for performing ToneTest algorithm
% As implemented in the C analyzer

    f_handles = struct( ...
        'calc_tonetest',    @calc_tonetest,     ...
        'find_peaks',       @find_peaks,        ...
        'freq_to_tone',     @freq_to_tone      ...
        );
    
    if nargout >= 1        
        varargout = {deal(f_handles)};
    end

end

function varargout = calc_tonetest(fft_buf, freq_resolution, varargin)
%
% Calculate tonetest algorithm
%
% Syntax:
%
% ... = calc_tonetest(fft_buf, freq_resolution)
% ... = calc_tonetest(fft_buf, freq_resolution, [start_freq end_freq])
% ... = calc_tonetest(fft_buf, freq_resolution, start_freq, end_freq)
% ... = calc_tonetest(fft_buf, freq_resolution, ..., skip_top_peak)
%
% [T1, T2, T3, T4] = calc_tonetest(...)
% tone_array       = calc_tonetest(...)
%
% fft_buf           - Buffer of single-sided FFT magnitude (mean or max FFT of a spectrogram)
% freq_resolution   - The frequency distance between two adjacent FFT samples (in Hz)
% start_freq        - the minimum frequency for analysis (in Hz)
% end_freq          - the maximum frequency for analysis (in Hz)
% skip_top_peak     - (true/false) whether or not to skip the most intense peak.
%

    % Parse parameters
    MANDATORY_PARAMS_COUNT = 2;
    arg_index = 1;
    
    if nargin >= MANDATORY_PARAMS_COUNT + arg_index
        % Get the frequency range
        if numel(varargin{arg_index}) == 2
            % This is a frequency range
            [start_freq, end_freq] = varargin{arg_index};
            arg_index = arg_index + 1;
        else
            assert(numel(varargin{arg_index}) == 1);

            start_freq  = varargin{arg_index};
            end_freq    = varargin{arg_index + 1};

            arg_index = arg_index + 2;        
        end
    else
        start_freq = 0;
        end_freq = (numel(fft_buf) + 1 ) * freq_resolution;
    end
    
    if nargin >= MANDATORY_PARAMS_COUNT + arg_index
        
        % Get the skip_top_peak    
        skip_top_peak = varargin{arg_index};
        arg_index = arg_index + 1;
        
    else
        skip_top_peak = false;
    end
    
    % Find the peaks in the FFT
    [peak_indices, peak_values, peak_strengths] = find_peaks(fft_buf, freq_resolution, start_freq, end_freq);
    
    % Sort the peaks by intensity
    [~, sort_mapping] = sort(peak_values, 'descend');
    peak_indices_sorted = peak_indices(sort_mapping);
    
    % If we need to ignore the highest peak, do it
    if skip_top_peak
        peak_indices_sorted(1) = [];
    end
    
    % Map frequencies to tones
    peak_freq = freq_resolution * (peak_indices_sorted - 1);
    tones_by_intensity = freq_to_tone(peak_freq);
    
    % Count peaks density for each tone
    tones_peak_density = zeros(1, 12);
    for tone = 0 : 11
        tones_peak_density(tone + 1) = sum(peak_strengths(tones_by_intensity == tone));
    end
    [tones_density_sorted, sort_mapping] = sort(tones_peak_density, 'descend');
    tones_by_density = 0:11;
    tones_by_density = tones_by_density(sort_mapping);
    tones_by_density(tones_density_sorted == 0) = [];
    
    
    % Assign tones
    try
        T1 = tones_by_intensity(1);     % Tone related to the most intense peak
        T2 = tones_by_density(1);       % Tone with the largest number of peaks
        T3 = tones_by_intensity(2);     % Tone with second most intense peak
        T4 = tones_by_density(2);       % Tone with second largest number of peaks
    catch err
        [T1, T2, T3, T4] = deal(-1);    
    end
    
    % Assign out parameters
    if nargout == 1
        varargout{1} = [T1, T2, T3, T4];
    else
        assert(nargout == 4);
        
        varargout{1} = T1;
        varargout{2} = T2;
        varargout{3} = T3;
        varargout{4} = T4;
    end

end

function varargout = find_peaks(fft_buf, freq_resolution, varargin)

    %--- Define constants
    Hz = 1;
    MIN_PERCENT_GAP_FROM_MAX = 0.0075;  % 0.75%
    PEAK_WIDTH = 16*Hz;
    RELATIVE_TEST_PERCENT_THRESHOLD = 10; % percent
    MIN_FREQ_FOR_HARMONICS_CHECK = 1200 * Hz;
    MAYBE_PEAK_DOWNHILL_RANGE = 40*Hz;
    MAYBE_PEAK_MIN_FREQ = 2200*Hz;
    TABLE_CASE_MAX_SLOPE_WIDTH = 30*Hz;
    TABLE_CASE_PERCENT_THRESHOLD = 25; % percent
    TABLE_CASE_MAX_FLAT_RANGE = 40*Hz;
    TABLE_CASE_GAP_BETWEEN_NEAR_PEAKS = 10; % percent
    TABLE_CASE_GAP_BETWEEN_LOCAL_MIN = 20; % percent
    
    %--- Validate parameters
    MANDATORY_PARAMS_COUNT = 2;
    arg_index = 1;
        
    if nargin >= MANDATORY_PARAMS_COUNT + arg_index
        % Get the frequency range
        if numel(varargin{arg_index}) == 2
            % This is a frequency range
            [start_freq, end_freq] = varargin{arg_index};
            arg_index = arg_index + 1;
        else
            assert(numel(varargin{arg_index}) == 1);

            start_freq  = varargin{arg_index};
            end_freq    = varargin{arg_index + 1};

            arg_index = arg_index + 2;        
        end
    else
        start_freq = 0;
        end_freq = (numel(fft_buf) + 1 ) * freq_resolution;
    end
   
    if size(fft_buf, 1) > 1
        assert(size(fft_buf, 2) == 1);
        fft_buf = fft_buf.';
    end
    
    %--- Start working
    
    % Calculate absolute threshold as a fraction of the maximum intensity
    min_intensity_gap = max(fft_buf(2:end)) * MIN_PERCENT_GAP_FROM_MAX;
    
    % Find "potential peaks" - places in the FFT where we're switching from
    % ascend to descend.
    d_fft  = [sign(diff(fft_buf)) 0];
    dd_fft = [0 sign(diff(d_fft))];
    
    % Check when the first two derivatives are both negative
    potential_peak_indices = find(d_fft + dd_fft == -2);
    
    % Calculate associated frequencies
    potential_peak_freqs = (potential_peak_indices - 1) * freq_resolution;
    
    % Narrow down the peaks to only those in the analisys band
    select_idx = (start_freq < potential_peak_freqs) .* (potential_peak_freqs < end_freq);
    potential_peak_indices(~select_idx) = [];
    potential_peak_freqs(~select_idx) = [];
    potential_peak_intensities = fft_buf(potential_peak_indices);
    
    
    % Calcualte the mean of the FFT neighbourhood around each potential peak
    neighbours_mean = zeros(size(potential_peak_intensities));
    
    % Divide processing to various frequency bands (filter banks)
    freq_bands = [...
            0*Hz,   800*Hz, 1600*Hz,    3200*Hz ; ...   % min frequency
            1,      2,      3,          4         ...   % peak width
        ];
    
    % Traverse the bands list in a reverse order in order to process the
    % high frequencies first (saves some duplicated frequency range tests)
    unprocessed_idxs = true(size(potential_peak_indices));
    for band = freq_bands(:, end:-1:1)
        
        min_freq    = band(1);
        peak_width  = floor(band(2) * PEAK_WIDTH / freq_resolution);
        
        % Map the band range (extract indices of all peaks in the band range)
        subband_idx_mask = unprocessed_idxs & (potential_peak_freqs >= min_freq);
        band_peaks_idxs = potential_peak_indices(subband_idx_mask);
        unprocessed_idxs(subband_idx_mask) = false;

        % Calculate fft buffer indices of neighbourhoods (matrix)
        if ~isempty(band_peaks_idxs)
            fft_indices = repmat(band_peaks_idxs, 2 * peak_width, 1) + repmat([-peak_width:-1 1:peak_width].', 1, size(band_peaks_idxs, 2));
        else
            continue;
        end
        
        % Create a mask of valid indices (which are in range)
        fft_valid_indices_mask = (fft_indices >= 1) & (fft_indices <= length(fft_buf));
        
        if ~any(fft_valid_indices_mask)
            continue;
        end

        % Calcualte intensities for the valid indices (zero the others)
        nighbours_intensities = zeros(size(fft_valid_indices_mask));
        nighbours_intensities(fft_valid_indices_mask) = fft_buf(fft_indices(fft_valid_indices_mask));
        
        % Calculate the sum of neighbours as well as the number of valid
        % neighbours
        neighbours_sum = sum(nighbours_intensities);
        neighbours_count = sum(fft_valid_indices_mask);
        
        % Calculate the mean value of the neighbourhoods for this band
        neighbours_mean(subband_idx_mask) = neighbours_sum ./ neighbours_count;
        
    end
    
    % Get peaks that pass the relative test (potential_peak_intensities never contains zeros)
    assert(all(potential_peak_intensities > 0));
    rel_test = (1 - neighbours_mean ./ potential_peak_intensities) * 100 > RELATIVE_TEST_PERCENT_THRESHOLD;
        
    % Get peaks that pass the absolute test
    abs_test = potential_peak_intensities - neighbours_mean >= min_intensity_gap;
        
    % Keep track of identified peaks (start with no peaks)
    peaks_list = zeros(size(potential_peak_intensities));
    
    % --- TEST 1 - strong and bright peaks
    
    % Mark all candidate peaks that passed both the absolute and reltaive
    % tests as strong peaks
    peaks_list(rel_test & abs_test) = 1;    
   
    % Map frequencies to range indices (for iterative processing)
    %
    % 0Hz    -- 1200Hz  := 0
    % 1200Hz -- 2400Hz  := 1
    % 2400Hz -- 4800Hz  := 2
    % 4800Hz -- 9600Hz  := 3
    % etc.    
    freq_ranges = log2(potential_peak_freqs / MIN_FREQ_FOR_HARMONICS_CHECK);
    freq_ranges(freq_ranges < 0) = -1;
    freq_ranges = floor(freq_ranges) + 1;
    
    TABLE_SEARCH_START  = 1;
    TABLE_SEARCH_END    = 2;
    
    table_search_mode = TABLE_SEARCH_START;
    prev_peak_index = 0;
    
    for freq_range_index = unique(freq_ranges)
    
        freq_range_mask = freq_ranges == freq_range_index;
        
        % Select the peaks that passed only the relative test (but failed the
        % absolute test)
        rel_test_only = rel_test & ~abs_test & freq_range_mask;
        
        if freq_range_index > 0
            
            % Perform test 2 (harmonic peaks)            
           
            % --- TEST 2 - Harmonics-boosted peaks

            % For high frequencies which did not pass the absolute test, check
            % whether they are harmonics of lower frequencies which contain peaks.
            % If so, lower the absolute threshold and try the test again.

            % Check if there is a peak at half the frequency
            for peak_band_idx = find(rel_test_only)

                % If there's a peak at the half-frequency (subtract 1 from
                % potential_peak_indices(peak_band_idx) to comply with C legacy)
                if any(peaks_list > 0 & (potential_peak_indices - 1 == floor((potential_peak_indices(peak_band_idx) - 1) / 2)))
                    
                    % Check absolute test with a lower threshold
                    if potential_peak_intensities(peak_band_idx) - neighbours_mean(peak_band_idx) >= min_intensity_gap / 2
                        % The peak is good enough (boosted from a half-frequency peak). Add it
                        peaks_list(peak_band_idx) = 1;

                    end
                end
            end
                
        end % freq_range_index > 0
        

        % --- TEST 3 - Low prefix peaks

        % Low prefix peaks are peaks that follow a certain shape in the FFT
        % graph - the peaks follow a "v" shape graph of a local maximum
        % descending to a local minimum and then ascending to the potential
        % peak. The features of the local maximum and minimum help to determine
        % whether this is a peak or not.

        % Collect all the candidate peaks that passed the relative test, but
        % failed the harmonics boost or that didn't even get there (too low
        % frequencies)
        low_prefix_peaks = rel_test_only & ~peaks_list;

        for peak_band_idx = find(low_prefix_peaks)

            candidate_peak_index = potential_peak_indices(peak_band_idx);
            assert(all(candidate_peak_index > 1));

            [local_max_idx, local_min_idx] = get_maxmin_indices(dd_fft, candidate_peak_index);

            descent_width = (local_min_idx - local_max_idx) * freq_resolution;

            % Check that the downhill width is large enough
            if descent_width >= MAYBE_PEAK_DOWNHILL_RANGE

                % Assume it's a strong peak
                peak_power = 1;

                % If this peak is of a high frequency
                if (candidate_peak_index - 1) * freq_resolution > MAYBE_PEAK_MIN_FREQ
                    % consider it a weak peak
                    peak_power = 0.5;
                end

                % Add the peak
                peaks_list(peak_band_idx) = peak_power;
            end

        end

        % --- TEST 4 - Table cases

        % Table-related peaks are those that failed the relative test
        table_peaks_idx = find(~rel_test & freq_range_mask);

        for i = 1:numel(table_peaks_idx)

            band_idx = table_peaks_idx(i);
            current_peak_fft_index = potential_peak_indices(band_idx);
                        
            % In case that there is a peak between two table peaks, start a
            % new table search
            if (table_search_mode == TABLE_SEARCH_END) && (sum(peaks_list(prev_peak_index + 1 : band_idx - 1)) > 0)
                % Return to table candidate search.
                table_search_mode = TABLE_SEARCH_START;
            end            
            
            switch table_search_mode
                case TABLE_SEARCH_START

                    [~, local_min_fft_idx] = get_maxmin_indices(dd_fft, current_peak_fft_index);
                    ascent_width = (current_peak_fft_index - local_min_fft_idx) * freq_resolution;
                    ascent_height_percentage = (fft_buf(current_peak_fft_index) - fft_buf(local_min_fft_idx)) / fft_buf(current_peak_fft_index) * 100;

                    % If the ascent to the peak is steep enough
                    if ascent_width < TABLE_CASE_MAX_SLOPE_WIDTH && ascent_height_percentage > TABLE_CASE_PERCENT_THRESHOLD                    
                        
                        % This is a good table candidate. Register it

                        table_candidate_band_idx = band_idx;
                        assert(peaks_list(table_candidate_band_idx) == 0);
                        table_candidate_fft_index = current_peak_fft_index;
                        table_candidate_intensity = fft_buf(table_candidate_fft_index);
                        
                        table_search_mode = TABLE_SEARCH_END;
                        
                    end
                        
                case TABLE_SEARCH_END
                    
                    % Search for the table end
                    
                    % Calculate table width in Hz
                    table_width = (current_peak_fft_index - table_candidate_fft_index) * freq_resolution;

                    % Verify that the table is small enough
                    if table_width < TABLE_CASE_MAX_FLAT_RANGE

                        % Table width is small enough.
                        
                        current_peak_intensity = fft_buf(current_peak_fft_index);

                        % Check that the peaks difference is about 10% of the
                        % height
                        height_gap = abs(table_candidate_intensity - current_peak_intensity);
                        if height_gap / (table_candidate_intensity + height_gap) * 100 < TABLE_CASE_GAP_BETWEEN_NEAR_PEAKS
                            
                            min_intensity = min(fft_buf(table_candidate_fft_index : current_peak_fft_index));

                            if abs(table_candidate_intensity - min_intensity) / table_candidate_intensity * 100 < TABLE_CASE_GAP_BETWEEN_LOCAL_MIN
                                
                                % This is the table's end.

                                table_ends_band_indices = [table_candidate_band_idx, band_idx];
                                [~, max_index] = max([table_candidate_intensity, current_peak_intensity]);

                                peaks_list(table_ends_band_indices(max_index)) = 1;
                                
                                % Reset the table peak detection mechanism
                                table_search_mode = TABLE_SEARCH_START;
                                
                            end
                        end
                    end
            end
            
            prev_peak_index = band_idx;            
        end        
    end

    peak_indices        = potential_peak_indices(peaks_list > 0);
    peak_intensities    = fft_buf(peak_indices);
    peak_strengths      = peaks_list(peaks_list > 0);
    
    outputs = {peak_indices, peak_intensities, peak_strengths};
    
    assert(nargout <= 3);
    varargout = outputs(1:nargout);
end


function [local_max_idx, local_min_idx] = get_maxmin_indices(dd_fft, peak_index)
% 
% Find local maximum and minimum preceding a certain peak
% 
%  the maximum    ........ the peak
%         :       :
%         :      /\
%     /\  :     /  \/\
%    /  \ :    /      \   
%   /    \/\  /        \
%  /  FFT   \/      
% /  GRAPH   :..... the first minimum
%                 
%           >-----<
%          uphill (ascent) width    
%            
%         >--<
%       downhill (descent) width
% 

    % Find the indices of the former local minimum and the yet former
    % local maximum
    maxmin_indices = find(dd_fft(1 : peak_index - 1), 2, 'last');
    if length(maxmin_indices) < 2

        if length(maxmin_indices) < 1
            % Set the index of the local minimum as 1
            maxmin_indices = 1;
        end

        % Set the index of the local maximum as 1
        maxmin_indices = [1, maxmin_indices];
    end

    maxmin_cell = num2cell(maxmin_indices);
    [local_max_idx, local_min_idx] = deal(maxmin_cell{:});
    
end

function tones = freq_to_tone(freqs)

    TONES_PER_OCTAVE = 12;
    MIDDLE_LA = 440; % Hz

    tones = floor(mod(TONES_PER_OCTAVE * log2(freqs / MIDDLE_LA) -3 + 0.5, 12));

end


