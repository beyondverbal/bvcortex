function varargout = vsauce_features_lib(opt)

    if exist('opt', 'var')
        if strcmp(opt, 'populate_features')
            varargout{1} = {...
                'Fs', 'windowsize', 'frameshift', ...
                'CPP', 'Energy', 'preemphasis', 'sV', ...
                'shrF0', 'strF0', ...
                'pF0', 'pF1', 'pF2', 'pF3', 'pF4', 'pB1', 'pB2', 'pB3', 'pB4', ...
                'sF0', 'sF1', 'sF2', 'sF3', 'sF4', 'sB1', 'sB2', 'sB3', 'sB4', ...
                'H1', 'H2', 'H4', ...
                'A1', 'A2', 'A3', ...
                'H1A1c', 'H1A2c', 'H1A3c', 'H1H2c', 'H2H4c', ...
                'F2K', 'H2K', 'H5K', 'H42Kc', 'H2KH5Kc', ...
                'SHR', 'HNR05', 'HNR15', 'HNR25', 'HNR35', ...
                'H2KFMTalgorithm', 'HF0algorithm', 'AFMTalgorithm' ...
                };
            
            return
        end
    end

    f_handles = struct( ... 
        'vsauce_process_file', @vsauce_process_file, ...
        'process_vsauce_features', @process_vsauce_features ...
        );
         
    varargout{1} = f_handles;

    function praat_struct = vsauce_process_file(filepath)
        
        [mat_dir, mat_file, ~] = fileparts(filepath);
        
        praat_struct = load(fullfile(mat_dir, [mat_file, '.mat']));
    end
  
    function [out_mat, f_vec, t_vec] = process_vsauce_features(vsauce_struct, feature_name)
        
        out_mat = vsauce_struct.(feature_name);
        
        step = vsauce_struct.frameshift;
        
        f_vec = 0;
    
        t_vec = ((1:numel(out_mat)) - 1) * step/1000;
        
    end
end
