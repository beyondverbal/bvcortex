t = readtable('C:\BVC\Pepsico\intro_out_initial_compare_to_full.csv');
files_orig = t(:, 'filename');
files_new = t(:, 'new_file');
dest_dir = 'C:\BVC\Pepsico\converted_wav\intro\new_for_compare';

for i=1:length(files_orig{:,1})
    copyfile(strcat('C:\BVC\Pepsico\converted_wav\intro\' , files_orig{i,:}{1}), dest_dir)
end

for i=1:length(files_new{:,1})
    copyfile(files_new{i,:}{1}, dest_dir)
end