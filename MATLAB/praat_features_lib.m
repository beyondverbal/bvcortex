function varargout = praat_features_lib(opt)

    if exist('opt', 'var')
        if strcmp(opt, 'populate_features')
            varargout{1} = {...
                'Intensity', ...
                'F0', ...
                'F1', 'H1', 'A1', 'B1', ...
                'F2', 'H2', 'A2', 'B2', ...
                'F3', 'H3', 'A3', 'B3', ...
                'F4', 'H4', 'A4', 'B4', ...
                'H1-H2', 'H1-H3', 'H1-H4', ...
                'H2-H3', 'H2-H4', 'H3-H4', ...
                'A1-A2', 'A1-A3', 'A1-A4', ...
                'A2-A3', 'A2-A4', 'A3-A4', ...
                'H1-A1', 'H1-A2', 'H1-A3', 'H1-A4', ...
                'H2-A1', 'H2-A2', 'H2-A3', 'H2-A4', ...
                'H3-A1', 'H3-A2', 'H3-A3', 'H3-A4', ...
                'H4-A1', 'H4-A2', 'H4-A3', 'H4-A4' ...
                };
            
            return
        end
    end

    f_handles = struct( ... 
        'praat_process_file', @praat_process_file, ...
        'process_praat_features', @process_praat_features ...
        );
     
    
    varargout{1} = f_handles;


    
    function sliced = slice_sig(sig, wsize, step)
        
        [first, last, ~, ~] = slidewin(numel(sig), wsize, step, wsize);
        
        new_last = find(last == last(end), 1, 'first');
        first(new_last+1 : end) = [];
        last(new_last+1 : end) = [];
        
        sliced = zeros(wsize, numel(first));
        
        for n_frame = 1:numel(first)
            sliced(1:(last(n_frame)+1 - first(n_frame)), n_frame) = ...
                sig(first(n_frame):last(n_frame)).';
        end
        
        
        
    end
    
    function praat_struct = praat_process_file(filepath)

%         tic;

        [clock, formants, bandwidths] = praat_get_formants(filepath, 0, 1);
        [pclock, pitch, intensity] = praat_get_pitch_intensity(filepath, 0, 1);
        
        [sig, fs] = audioread(filepath);

        bfl = basic_features_lib();
        %[s, f, t] = bfl.param_specgram(sig, fs, 404, 50);
        [s, f, t] = bfl.param_specgram(sig, fs, 200, 80);
        
        voiced = ~isnan(pitch);
        pitch(~voiced) = [];
        
        fml = flow_management_lib();
        feature_s = fml.calc_basic_features(...
                            struct('signal', sig, 'fs', fs), 0, {'vad'});
                        
        lpc_order = 12;
        %lpc_coeff = lpc_on_ready_powspec(abs(s).^2, lpc_order).';
        sliced = slice_sig(sig, 200, 80);
        lpc_coeff = lpc(sliced, lpc_order).';
        filt = fast_allpole_freqz(1, lpc_coeff, f, fs);
        
        praat_struct = struct(  ...
            'formant_clock', clock, ...
            'formants', formants, ...
            'bandwidths', bandwidths, ...
            'pitch_clock', pclock(voiced), ...
            'pitch', pitch, ...
            'intensity', intensity, ...
            'intensity_clock', pclock, ...
            'voiced', voiced, ...
            'specgram', abs(s), ...
            'specfreq', f, ...
            'spectime', t, ...
            'fs', fs, ...
            'nwind', 404,  ...
            'lpc_filt', abs(filt) ...
            );
        
        % custom version of LPC, utilizing the input spectrum calculation
        function [a,e] = lpc_on_ready_powspec(powspec, N)

            % Compute autocorrelation vector or matrix
            powspec = [powspec(1:(end-1),:); powspec(end:-1:2,:)];
%                 powspec = [powspec; powspec(end:-1:2,:)];
%                 X = fft(x,2^nextpow2(2*size(x,1)-1));
            m = 2^nextpow2((size(powspec,1)-1));
            R = ifft(powspec,m);
            R = R./m; % Biased autocorrelation estimate

            [a,e] = levinson(R,N);

            % Return only real coefficients for the predictor if the input is real
            for k = 1:size(powspec,2)
%                     if isreal(x(:,k))
                    a(k,:) = real(a(k,:));
%                     end
            end
        end        
        
	    function samples = fast_allpole_freqz(B, A, f, fs)
            
	            % allpole filter order (number of poles)
	            m = size(A, 1);
            
	            % Create z^-m matrix (f - colvec, 0:m-1 - rowvec)
	            z_mat = exp(-1j*2*pi*f/fs * (0:(m-1)));
            
	            % sample the filter at the requested frequencies
	            samples = B ./ (z_mat * A);
	    end
        
        
        function [clock, pitch, intensity] = praat_get_pitch_intensity(file_path, start_time, end_time)

            [d, f, e] = fileparts(file_path);

            filename_modified = false;
            if any(isspace(f))
                f = strrep(f, ' ', '@@SPACE@@');
                movefile(file_path, fullfile(d, [f e]));
                filename_modified = true;
            end
            
            [status, output] = system(['c:\programs\praatcon -a "', ...
                convert_to_local_path('scripts\getpitch.psc'), '" "', d, '" "', [f e], '"']);

            if status ~= 0
                error('FileError:PraatGetPitchIntensity:Failed', 'Praat script failed!');
%                 [clock, pitch, intensity] = deal(NaN);
%                 return
            end
            
            if filename_modified
                movefile(fullfile(d, [f e]), file_path);
            end

            tokens = strsplit(output);

            % Process the tokens
            out_mat = zeros(size(tokens));
            for n = 1:numel(out_mat)
                out_mat(n) = str2double(tokens{n});
            end

            % Separate to different values
            out_mat = reshape(out_mat, 3, []);
            clock = out_mat(1, :);
            pitch = out_mat(2, :);
            intensity = out_mat(3, :);

        end        
        
        function [clock, formants, bandwidths] = praat_get_formants(file_path, start_time, end_time)

            [directory, filename, ext] = fileparts(file_path);

            [status, output] = system(['c:\programs\praatcon -a "', ...
                convert_to_local_path('scripts\getformants.psc'), '" "', directory, '" "', ...
                [filename ext], '" 0 1 5 4000 0.025']);

            if status ~= 0
                error('FileError:PraatGetFormants:Failed', ...
                    ['Praat script failed! Output:  '  output]);
%                 [clock, formants, bandwidths] = deal(NaN);
%                 return
            end

            tokens = strsplit(output);

            % Process the tokens
            out_mat = zeros(size(tokens));
            for n = 1:numel(out_mat)
                out_mat(n) = str2double(tokens{n});
            end    

            line_len = unique(diff(find(out_mat == -10)));
            assert(line_len == 10);

            % Separate to different values
            out_mat = reshape(out_mat, line_len, []);
            clock = out_mat(2, :);
            formants = out_mat(3:2:9, :);
            bandwidths = out_mat(4:2:10, :);

        end        
        
    %     toc;
    %     
    %     y = audioread(filepath);
    % 
    %     colors = [0 0 1 ; 0 0 0 ; 1 0 0 ; 0.5*[0 1 1]];
    %     
    %     figure
    %     p1 = subplot(2, 1, 1);
    %     spectrogram(y, hamming(8000 * 0.025), round(8000*0.025/2), 8000*0.025, 8000, 'yaxis')
    %     hold on
    %     for n = 1:3
    %         clr = colors(n, :);
    %         plot(clock, formants(n, :), '.', 'Color', clr);
    %         plot(clock, formants(n, :) + 0.5*bandwidths(n, :), '-', 'Color', clr * 0.7);
    %         plot(clock, formants(n, :) - 0.5*bandwidths(n, :), '-', 'Color', clr * 0.7);
    %     end
    %     
    %     plot(pclock, pitch, 'm.-')
    %     plot(pclock, pitch, 'w.-')
    %     hold off
    %     p2 = subplot(2, 1, 2);
    %     roof = 110 / 90 * quick_prctile(intensity, 90);
    %     plot(pclock, intensity, 'k');
    %     ylim([0 roof]);
    %     linkaxes([p1, p2], 'x');
    %             
    end

    function [out_mat, f_vec, t_vec] = process_praat_features(praat_struct, f_vec, t_vec, feature_name, method)
        
        match = regexp(feature_name, ...
            {'^Intensity$', ...
            '^[fF]0$', ...
            '^[fF][1-4]$', ...
            '^[hH][1-4]$', ...
            '^[aA][1-4]$', ...
            '^[bB][1-4]$', ...
            '^[aAaH][1-4]-[aAaH][1-4]$', ...
            '^voiced$'} ...
            );
        
        praat_struct.voiced_clock = praat_struct.intensity_clock;
        
        match_idx = find(~cellfun('isempty', match), 1, 'First');
        
        slice_formants();
        
        method_set = exist('method', 'var');
        
        switch match_idx
            case 1  % Intensity
                out_mat = praat_struct.intensity;
                f_vec = 0;
                t_vec = praat_struct.intensity_clock;
                
            case 2  % F0
                out_mat = praat_struct.pitch;
                f_vec = 0;
                t_vec = praat_struct.pitch_clock;
                
            case 3  % F1 .. F4
                if ~method_set
                    method = 'lpcmax';
                end
                
                n_formant = str2double(feature_name(2));    
                [out_mat, t_vec] = get_formants(n_formant, method);
                                                
                f_vec = 0;
                
            case 4  % H1 .. H4                
                if ~method_set
                    method = 'fftmax';
                end
                
                harmonic_no = str2double(feature_name(2));
                [out_mat, ~, t_vec] = read_spgram_value(praat_struct.pitch * harmonic_no, praat_struct.pitch_clock, 0.3, method);
                f_vec = 0;
                
            case 5  % A1 .. A4
                if ~method_set
                    method = 'lpcmax';
                end
                
                formant_no = str2double(feature_name(2));
                [formants, t_vec] = get_formants(formant_no, method);
                [out_mat, ~, t_vec] = read_spgram_value(formants, t_vec, -50, method);
                f_vec = 0;
                
            case 6  % B1 .. B4
                if ~method_set
                    method = 'lpcmax';
                end

                n_formant = str2double(feature_name(2));                                
                %formants = praat_struct.formants(n_formant, :);
                [formants, t_vec] = get_formants(n_formant, method);
                
                bw = praat_struct.bandwidths(n_formant, :);
                bw_clock = praat_struct.formant_clock;
                
                out_mat = repmat(formants, 1, 2) + [bw,  -bw] / 2;
                f_vec = 0;
                t_vec = [bw_clock, bw_clock];
                
                
                
            case 7  % A/H xxx - A/H xxx

                tokens = strsplit(feature_name, '-');
                
                for ntok = 1:numel(tokens)
                    tok = tokens{ntok};
                    
                    ordinal = str2double(tok(2));
                    
                    switch tok(1)
                        case 'A'
                            if ~method_set
                                method = 'lpcmax';
                            end
                            
                            [formants, t_vec] = get_formants(ordinal, method);
                            [output(ntok).data, ~, output(ntok).time] = read_spgram_value(formants, t_vec, -50, method);                            
                            [output(ntok).time, select] = unique(output(ntok).time);
                            output(ntok).data = output(ntok).data(select);
                            
                        case 'H'
                            if ~method_set || strcmp(method, 'lpcmax')
                                method = 'fftmax';
                            end
                            [output(ntok).data, ~, output(ntok).time] = read_spgram_value(praat_struct.pitch * ordinal, praat_struct.pitch_clock, 0.3, method);
                            [output(ntok).time, select] = unique(output(ntok).time);
                            output(ntok).data = output(ntok).data(select);
                    end
                end
                
                if numel(output(1).time) ~= numel(output(2).time)
                    [~, max_idx] = max([numel(output(1).time), numel(output(2).time)]);
                    if max_idx == 1
                        min_idx = 2;
                    else
                        min_idx = 1;
                    end
                   
                    % Find indices of shorter clock in longer clock
                    clock_mapping = interp1(...
                        output(max_idx).time, ...
                        1:numel(output(max_idx).time), ...
                        output(min_idx).time, ...
                        'nearest');
                    
                    outsiders = isnan(clock_mapping);
                    clock_mapping(outsiders) = [];
                    output(min_idx).time(outsiders) = [];
                    output(min_idx).data(outsiders) = [];
                                   
                    % Trim longer result to fit shorter result
                    output(max_idx).time = output(max_idx).time(clock_mapping);
                    output(max_idx).data = output(max_idx).data(clock_mapping);
                end
                
                assert(all(output(1).time == output(2).time));
                out_mat = output(1).data - output(2).data;
                f_vec = 0;
                t_vec = output(1).time;
                
            otherwise
                out_mat = praat_struct.voiced * 1000;
                f_vec = 0;
                t_vec = praat_struct.voiced_clock;
        end

        function slice_formants()
            clock_xlat = interp1(  ...
                praat_struct.voiced_clock, ...
                1:numel(praat_struct.voiced_clock), ...
                praat_struct.formant_clock, ...
                'nearest' ...
                );

            valid_inds_map = ~isnan(clock_xlat);
            formants_voiced = false(1, numel(valid_inds_map));
            formants_voiced(valid_inds_map) = ...
                praat_struct.voiced(clock_xlat(valid_inds_map));

            praat_struct.formants(:, ~formants_voiced) = [];
            praat_struct.bandwidths(:, ~formants_voiced) = [];
            praat_struct.formant_clock(~formants_voiced) = [];
        end            
        
        function [formants, t_vec] = get_formants(n_formant, method)

            formants = praat_struct.formants(n_formant, :);
            t_vec = praat_struct.formant_clock;

            [formants_power, formants, t_vec] = ...
                read_spgram_value(formants, t_vec, -50, method);
            
            unq_times = unique(t_vec);
            dup_times = unq_times(histc(t_vec, unq_times) > 1);

            for ndup = 1:numel(dup_times)
                dup_idxs = find(t_vec == dup_times(ndup));

%                 [formants_power, ~, ~] = read_spgram_value( ...
%                     formants(1, dup_idxs), ...
%                     repmat(dup_times(ndup), 1, numel(dup_idxs)), ...
%                     10, ...
%                     method ...
%                     );

                [~, max_power_idx] = max(formants_power(dup_idxs));
                remove_idxs = dup_idxs(1:end ~= max_power_idx);

                t_vec(remove_idxs) = [];
                if any(remove_idxs > numel(formants) | remove_idxs <= 0 | isnan(remove_idxs))
                    breakpoint()
                end
                formants(remove_idxs) = [];
                formants_power(remove_idxs) = [];
            end
        end
        
        function [value, fixed_freq, fixed_time] = read_spgram_value(freq, time, width, method)
            
            idx = isnan(freq);
            freq(idx) = [];
            time(idx) = [];
            
            % Sync time and freq values with that of the spectrogram, get
            % indices
            time_idx = interp1(...
                praat_struct.spectime, ...
                1:numel(praat_struct.spectime), ...
                time, ...
                'nearest', ...
                numel(praat_struct.spectime));
            freq_center = interp1(...
                praat_struct.specfreq, ...
                1:numel(praat_struct.specfreq), ...
                freq, ...
                'nearest');
            %f_width_samples = f_width_hz/100 * praat_struct.fs/praat_struct.nwind;
            
            
            % DBG code
            if any(isnan(freq_center))
                error('FileError:read_spgram_value:FreqOutOfBounds', ...
                    'formants over 4000Hz!');
            end
            
            if strcmp(method, 'lpcmax') || strcmp(method, 'fftmax')
                
                switch method
                    case 'lpcmax'
                        surface = abs(praat_struct.lpc_filt);
                    case 'fftmax'
                        surface = abs(praat_struct.specgram);
                end
                
                [HarmIdx, FreqHarmonics, TimeIdx, fixed_time] = InlineAlgoOps('GetHarmonicNeighb', ...
                            freq, time, 1, width,...
                            praat_struct.specfreq, praat_struct.spectime, 0, 4000);

                %Intensity = NanIndexable(abs(praat_struct.specgram));
                Intensity = NanIndexable(surface);            
                [value, rel_index] = nanmax(double(Intensity(squeeze(HarmIdx))), [], 1);

                delta_freq = (rel_index - (size(HarmIdx, 1)+1)/2) * median(diff(praat_struct.specfreq));
                fixed_freq = FreqHarmonics + delta_freq;
                
                value = 20*log10(value);
                
            else     % fft_exact
                specgram_idx = sub2ind(size(praat_struct.specgram), freq_center, time_idx);

                value = 20*log10(abs(praat_struct.specgram(specgram_idx)));
                fixed_freq = praat_struct.specfreq(freq_center);
                fixed_time = praat_struct.spectime(time_idx);
            end

        end
        
        function [H, f, t] = get_harmonic(ordinal, corrected, F0, F0time, abs_spectrum, spec_freq, spec_time, DeviationPercent)

            HarmonicFreq = ordinal * F0;

            % Find coefficients for frequency vector (f = m*(sub-1) + n)
            freq_m = median(diff(spec_freq));
            freq_n = spec_freq(1);
            sub = floor((HarmonicFreq - freq_n) / freq_m) + 1;

            time_m = median(diff(spec_time));
            time_n = spec_time(1);
            fine_time_idx = floor((F0time - time_n) / time_m) + 1;

            time_idx = unique(fine_time_idx);
            base_vec = zeros(1, numel(time_idx));
            for idx = 1:numel(time_idx)
                base_vec(idx) = round(median(sub(fine_time_idx == time_idx(idx))));
            end

            max_f_deviation = ceil(mean(F0) * (DeviationPercent/100) / freq_m);

            [base, offset] = meshgrid(base_vec, -max_f_deviation:max_f_deviation);
            freq_neighbors = base + offset;

            search_idxs = sub2ind(size(abs_spectrum), freq_neighbors, repmat(time_idx, [size(freq_neighbors, 1) 1]));

            [H, f_rel_idx] = max(abs_spectrum(search_idxs));

            f_idx = base_vec + f_rel_idx-1 - max_f_deviation;

            H = log(H);
            f = spec_freq(f_idx);
            t = time_m * (time_idx-1) + time_n;

        end
        
    end
end

    function [out_mat, f_vec, t_vec] = processHAs(f_name, in_mat)
        
        % Get the function type
        [match, tokens] = regexp(f_name, '^([AH])([1-4])(\*?)-([AH])([1-4])(\*?)$', 'match', 'tokens');
        
        assert(~isempty(match));
        tokens = tokens{1};
        Features = struct(  ...
            'Type',     {tokens{1}, tokens{4}}, ...     % A=Formant, H=Harmonic
            'Ordinal',  {tokens{2}, tokens{5}}, ...     % 1-4
            'Corrected', {tokens{3}, tokens{6}} ...     % '*' = corrected, ''=not corrected
            );
        
        [Features.Ordinal] = deal_array(cellfun(@str2num, {Features.Ordinal}));
        [Features.Corrected] = deal_array(cellfun(@(x) strcmp(x, '*'), {Features.Corrected}));
        
        abs_spectrum = abs(in_mat.spectrum_256);
        
        Results = [];
        
        for Feature = Features
            
            switch Feature.Type
                case 'A'
                    
                    
                    
                case 'H'
                    
                    run_libs({'basic_feat_fh'});
                    [F0, ~, t_vec] = libs.basic_feat_fh.pitch_contour_on_pitch_mat(in_mat.pitch_mat_256, in_mat.ct_vec_256, in_mat.t_vec_80);
                    
                    [H, f, t] = get_harmonic(...
                        Feature.Ordinal, ...
                        Feature.Corrected, ...
                        F0, ...
                        t_vec, ...
                        abs_spectrum, ...
                        in_mat.f_vec_256, ...
                        in_mat.t_vec_80, ...
                        20);
                    
%                     % Find the index of the frequency closest to F0 on each
%                     % frame
%                     HarmonicFreq = Feature.Ordinal * F0;
%                     
%                     % Find coefficients for frequency vector (f = m*(sub-1) + n)
%                     freq_m = median(diff(in_mat.f_vec_256));
%                     freq_n = in_mat.f_vec_256(1);
%                     sub = round((HarmonicFreq - freq_n) / freq_m) + 1;
%                     
%                     time_m = median(diff(in_mat.t_vec_80));
%                     time_n = in_mat.t_vec_80(1);
%                     time_idx = round((t_vec - time_n) / time_m) + 1;
%                     
%                     ind = sub2ind(size(in_mat.spectrum_256), sub(:), time_idx(:));
                    Results(end+1, :) = H;
            end
            
        end
        
        out_mat = Results(1, :) - Results(2, :);
        t_vec = t;
        f_vec = 0;
    end

