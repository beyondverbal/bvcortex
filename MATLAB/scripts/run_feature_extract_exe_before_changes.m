function run_feature_extract_exe(audio_files_in, varargin)
% This function is the basis for smiling_arthur's execute (via Matlab Compiler)
% Inputs
tic

fprintf('INSIDE run_.._exe\n');

% verify correct inputs
if ~check_inputs(audio_files_in, varargin);  error('check input failed');  end

% parse optional params
optionals_struct = parse_config_file(varargin);

% deal with audio
audio_struct = parse_audio(audio_files_in, optionals_struct);
% optionals_struct = parse_optional_inputs(varargin);

log_filename = strcat(optionals_struct.out_file(1:end-3),'log');
log_file     = fopen(log_filename,'wb');

% run 

fprintf('\n starting run_feature_extract loop, num of files = %d\n\n', length(audio_struct));

ROOT_DIR = optionals_struct.root;

% RES_RES = nan(length(audio_struct),length(optionals_struct.flows));
RES_RES = [];%nan(length(audio_struct),length(optionals_struct.flows));
running_index = 1; 
new_audio_struct = struct('filename', nan, 'segment', nan, 'start', nan, 'end1', nan);
for i=1:length(audio_struct)
    
    try
        fullname = strcat(ROOT_DIR,'\',num2str(audio_struct(i).patient_id),'\',audio_struct(i).filename,'.wav');
        %PATCH for .wav
        if strcmp(fullname(end-7:end),'.wav.wav'); fullname = fullname(1:end-4);
        elseif ~strcmp(fullname(end-3:end),'.wav'); fullname = strcat(fullname, '.wav');
        end            
            
        [wav_from_file, fs] = audioread(fullname);
    catch
        fprintf('PROBLEM READING AUDIO, i= %d\n',i);
        fprintf(log_file, 'pid = %d; i = %d; PROBLEM READING AUDIO file = %s   \n',audio_struct(i).patient_id, i, fullname);
        continue
    end
    try
        aux_vad_struct = prep_signal(wav_from_file, fs, audio_struct(i), optionals_struct);
    catch
        fprintf('PROBLEM PREPING AUDIO, i= %d\n',i);
        continue
    end
    
    fprintf('i = %d; starting run_feature_extract: signal len = %d; fs = %d; param_len = %d, flows_len = %d\n', ...
        i, length(aux_vad_struct.signal), fs, numel(fields(optionals_struct.params)), length(optionals_struct.flows));
    try
        
        if strcmp(optionals_struct.analysis_mode, 'maccabi')
            ana_size = optionals_struct.analysis_window*fs;
            num_of_analyses = floor(length(aux_vad_struct.signal')/ana_size);
            
            running_j_index = 1;
            for j = 1:num_of_analyses
                new_sig = aux_vad_struct.signal( 1 + (j-1)*ana_size : j*ana_size );
                new_aux = struct('signal', new_sig, 'fs', fs, 'frame_overlap', []);
                
                fprintf('\ti = %d; j = %d; st = %d; nd = %d (%d -> %d)\n', i, j, (j-1)*ana_size/fs, j*ana_size/fs, 1+(j-1)*ana_size, j*ana_size)

                try
                    [RES_RES(running_index,:), ~, ~] = run_feature_extract(new_aux, optionals_struct.params, optionals_struct.flows);
                    new_audio_struct(running_index) = struct('filename', audio_struct(i).filename, 'segment', running_j_index, 'start', (j-1)*ana_size/fs, 'end1', j*ana_size/fs);
                catch
                    fprintf('\ti = %d; j = %d; ***** FAIL *****\n', i, j);
                    continue;
                end
                running_index = running_index + 1;
                running_j_index = running_j_index + 1;
            end
        else
            [RES_RES(i,:), ~, ~] = run_feature_extract(aux_vad_struct, optionals_struct.params, optionals_struct.flows);
        end
    catch
        fprintf('i = %d; ANALYSIS FAILED; catched in run_feature_extract_exe\n', i);
        fprintf(log_file,'pid = %d; i = %d; ANALYSIS FAILED; catched in run_feature_extract_exe\n', audio_struct(i).patient_id, i);
    end
end

[~, flows_index] = flows_to_cell(optionals_struct.flows); % flow names to table VAR names

T_res = array2table(RES_RES,'VariableNames', flows_index);
T_audio = struct2table(new_audio_struct);

% T_audio_rev = T_audio(:,{'filename'});
% T = [T_audio_rev, T_res];

T = [T_audio, T_res];

fprintf('writing output to: %s\n', optionals_struct.out_file);
fclose(log_file);
writetable(T, optionals_struct.out_file, 'Delimiter',',');
fprintf('Total Running Time %.2f sec (%.2f min) \n', toc, toc/60)

function aux_vad_struct = prep_signal(wav, fs, audio_struct_i, params)
% consider offset and duration
fprintf('Preparing file %s, analysis type: %s - slicing :)\n\n', audio_struct_i.filename, params.analysis_mode);

if ~isempty(strfind(lower(params.analysis_mode), 'maccabi')) 
   
    % manage slices
    
    slices = audio_struct_i.slices;
    assert(size(slices,2)==2);
    signal = [];
    for i=1:size(slices,1)
        signal = [signal; wav(round(slices(i,1)*fs) : round(slices(i,2)*fs))]; %#ok<AGROW>
    end
    if isfield(params, 'analysis_window')
%         fprintf('\nanalysis window %d secs, full signal length %d (%.2f sec)\n\n', params.analysis_window, length(signal), length(signal)/fs)
%         fprintf('analysis window %d secs: TRUNCATING FOR MACABBI\n', params.analysis_window);
%         if length(signal) > params.analysis_window*fs
%             signal = signal(1:params.analysis_window*fs);
%         end
        
%         req_length = params.analysis_window*fs;
%         while

        aux_vad_struct = struct('signal', signal', 'fs', fs, 'frame_overlap', []);
        
    else
        fprintf('\nanalysis window not defined, signal length left %d (%.2f sec)\nn\', length(signal), length(signal)/fs);
    end
else
    offset_in   = audio_struct_i.offset;
    duration_in = audio_struct_i.duration;
    start_in    = audio_struct_i.start; 
    ms2samples = fs/1000;

    %get real offset - USE START, VAD BASED
    if isnan(start_in) || ~isnumeric(start_in)
        Offset = 1;
    else
        Offset = 1 + offset_in*ms2samples;
    end

    % fix signal wrt offset
    signal_with_offset = wav(Offset:end);

    if isnan(duration_in) || ~isnumeric(duration_in)
        % do nothing, offset is already considered
    else
        duration = duration_in*ms2samples;
        signal = signal_with_offset(1:duration);
    end

    %get real offset - USE START, VAD BASED
%     if isnan(start_in) || ~isnumeric(start_in) || start_in==0
%         % do nothing, no shift needed
%     else
%         real_start = (start_in - offset_in)*ms2samples;
%         signal = signal(real_start:end);
%     end
    
    
    if isfield(params, 'analysis_window')
        analysis_window = params.analysis_window*fs;
        if length(signal) > analysis_window
            fprintf('** Truncate signal: from %d -> %d [samples] ***\n', length(signal), analysis_window)
            signal = signal(1:analysis_window);
        end
    end
    aux_vad_struct = struct('signal', signal', 'fs', fs, 'frame_overlap', []);
end



function optionals_struct = parse_config_file(varargin)

optionals_struct = ([]);

cfg_file = varargin{1}{1};
if ~exist(cfg_file,'file')
    fprintf('cfg file %s doesn''t exist\n', cfg_file);
    error(1);
end
fid = fopen(cfg_file);
if fid < 0 
    fprint('Can''t open cfg file %s \n', cfg_file);
    error(1);
end

tline = fgetl(fid);
fprintf('\n\n *** CFG LINES *** \n');
while ischar(tline)
    disp(tline)
%     fprintf('THE LINE IS %s\n',tline);
    eval([tline,';']);
    tline = fgetl(fid);
end
fclose(fid);
fprintf('\n *** EOF CFG *** \n\n');
% get params into struct

% PRINT_DEBUG
% fprintf('CFG: print_debug = %d\n', print_debug);
if exist('print_debug','var')
    optionals_struct.print_debug = print_debug;
    fprintf('print_debug = %d\n', print_debug);
else
    optionals_struct.print_debug = 1;
    fprintf('Problem with print_debug, using default - print\n');
end
clear print_debug


% out file
% fprintf('CFG: out_file = %s\n', out_file);
if exist('out_file','var')
    optionals_struct.out_file = out_file;
    fprintf('out_file = %s\n', out_file);
else
    optionals_struct.out_file = 'default_out_file.csv';
    fprintf('Problem with out_file cfg = %s; using default %s\n', out_file, optionals_struct.out_file);
end
clear out_file

% flows
% fprintf('CFG: flows_file = %s\n', flows_file);
if exist(flows_file,'file')
    optionals_struct.flows = parse_flow_excel(flows_file);
    fprintf('SUCCESS! flows_file = %s\n', flows_file);
else
    optionals_struct.flows = parse_flow_excel;
    fprintf('Problem with flows_file, using default 205 flows\n');
end
clear flows


% audio_files_root
fprintf('CFG: audio_files_root = %s\n', audio_files_root);
if exist('audio_files_root','var')
    optionals_struct.root = audio_files_root;
    fprintf('audio_files_root = %s\n', audio_files_root);
else
    optionals_struct.audio_files_root = '';
    fprintf('No Root Dir for audio files\n');
end
clear audio_files_root


%params
fprintf('CFG: params_file = %s\n', params_file);
if exist(params_file,'file')
    optionals_struct.params;
    fprintf('flows_file = %s\n', params_file);
else
    optionals_struct.params = get_default_params;
    fprintf('Problem with params_file, using default params\n');
end
clear params_file

%analysis_window
if exist('analysis_window','var')
    optionals_struct.analysis_window = analysis_window;
    fprintf('CFG: Analysis Window = %d\n', optionals_struct.analysis_window);
    clear analysis_window
end

%params
fprintf('CFG: analysis mode = %s\n', analysis_mode);
if exist('analysis_mode','var')
    optionals_struct.analysis_mode = analysis_mode;
    fprintf('analysis_mode = %s\n', analysis_mode);
else
    optionals_struct.analysis_mode = '';
    fprintf('Analysis mode is not set\n');
end
clear analysis_mode

function res = check_inputs(audio_files_in, varargin)
if ~isempty(audio_files_in)
    narginchk(1,5)
    fprintf('Received 1 required, %d optional inputs\n\n', size(varargin, 2));
    res = size(varargin, 2);
else
    fprintf('No Audio_Files input - required!\n\n');
    res = -1;
end

function out_struct = parse_audio(audio_files_in, optional_params)

T = readtable(audio_files_in);

if ~isempty(strfind(lower(optional_params.analysis_mode), 'maccabi')) 
    filename = T{:,'filename'};
%     nurse_id = T{:,'nurse_id'};
    OSA = T{:,'OSA'};
    patient_id = T{:,'patient_id'};
    seg_start = T{:,'seg_start'};
    seg_end = T{:,'seg_end'};
%     legend = T{:,'legend'};
%     confidence = T{:,'confidence'};
    
    T2a = table(filename, patient_id, seg_start, seg_end, OSA);
    
    out_struct = arrange_slices(T2a, optional_params.print_debug);
else
    filename = T{:,'FileName'};
    offset   = T{:,'Offset'};
    wavemd5  = T{:,'wavemd5'};
    start    = T{:,'Start'};
    duration = T{:,'Duration'};
    patient_id = cell(size(start));
    T2 = table(filename, offset, wavemd5, start, duration, patient_id);
    
    out_struct = table2struct(T2);
end

fprintf('Parsing audio input: found %d file\n', length(out_struct));

function st_out = arrange_slices(Tin, print_debug)

% files   = Tin{:,'filename'};
files   = Tin.filename;
st      = Tin{:,'seg_start'};
nd      = Tin{:,'seg_end'};
% nurse   = Tin{:,'nurse_id'};
patient = Tin{:,'patient_id'};
OSA     = Tin{:,'OSA'};

st_out_index = 1;
if print_debug; fprintf('DEBUG: files(1) = %s\n', files{1,:}); end
st_out(st_out_index).filename   = files{1};
st_out(st_out_index).slices     = [st(1) nd(1)];
% st_out(st_out_index).nurse_id   = nurse(1);
st_out(st_out_index).patient_id = patient(1);
st_out(st_out_index).OSA = OSA(1);

for i=2:length(files)
    if strcmp(st_out(st_out_index).filename, files{i})
        st_out(st_out_index).slices = [st_out(st_out_index).slices; [st(i) nd(i)]];
    else
        st_out_index = st_out_index + 1;
        st_out(st_out_index).filename   = files{i};
        st_out(st_out_index).slices     = [st(i) nd(i)];
%         st_out(st_out_index).nurse_id   = nurse(i);
        st_out(st_out_index).patient_id = patient(i);
        st_out(st_out_index).OSA = OSA(i);
    end
end
% 1) remove '.wav' from filenames - coded separate
% 2) RE-ARRANGE FILES AS 10 or 20 SECONDS 
for i=1:length(st_out)
    st_out(i).filename = st_out(i).filename(1:end-4);
end

function [flows_cell_real, flows_index] = flows_to_cell(flows_struct)

p = struct2cell(flows_struct);
[~, ~, cc] = size(p);
flows_cell_real = cell(1, cc);
flows_index = cell(1, cc);
for i = 1:cc
    flows_cell_real{i} = p{6, :, i};
    flows_index{i} = strcat('feat_',num2str(i));
end

function params_s = get_default_params()

params_s.test_type = 'regular';
params_s.stop_on_first_result = 1;
params_s.send_buffer_time = 20;
params_s.fs = 8000;
params_s.max_relevance_time = 45;
params_s.relevance_tolerance = 15;
params_s.min_speech = 9;
params_s.max_speech = 10;
params_s.crit_snr = 20;
params_s.shrink_audio = 1;
params_s.slice_history = 2;
params_s.max_forced_speech = 20;
params_s.min_forced_speech = 5;
params_s.min_rasta_time = 4.9700;
params_s.min_rasta_overlap = 2.5000;
params_s.temper_low_cutoff = 40;
params_s.temper_high_cutoff = 65;
params_s.vad_fs = 100;
params_s.meta_data = '';
params_s.basic_features = {'~all'  'fft_1024'  'fft_256'  'rasta'  'vad'  'voc_src_filt'  'pitch_mat'  'tonal_dissonance'  '12th_octave_fft'  '~psysound'};
params_s.analyses_types = {'vad_sum'  'valence_arousal_temper_gender'  'audio_quality'  'tonetest'  'phrases_info'  '~basic_stats'  '~temper_2d'  '~composure'  '~cooperation'  '~servicetone'  '~salestone'};
params_s.summary_analyses_types = {'call_trend'  'total_voice_content'  'basic_stats'};
params_s.summary_audio_quality_policy = {'good'  'marginal_better'  'marginal_worse'  'bad'};
params_s.slicing_progress_flag = 0;









