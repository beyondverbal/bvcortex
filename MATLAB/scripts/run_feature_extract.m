function [val_vec, val_ind_vec, feat_struct, stitched_voice] = run_feature_extract(aux_vad_struct, params_s, flows_s)
    
    fprintf('\n in feature_extract: signal len = %d; \n', length(aux_vad_struct.signal));
    
    time_offset = 0;  
    feature_s = calc_basic_features(aux_vad_struct, time_offset);

    feature_s.meta_data = '';

    slices_s = [];
%     fprintf('***************************** SNR = 5 *************************\n');
%     params_s.crit_snr = 3;
    params_s.min_speech = 5; %seconds
    params_s.max_speech = 30;
    params_s.vad_fs = 100;
    [slices_s, ~, features_out] = process_basic_features_into_slices(slices_s, feature_s, params_s);
    
    MIN_SPEECH = 5;
%     vad_stitch = stitch_by_vad(features_out, MIN_SPEECH);
    vad_stitch = stitch_by_t_vector(aux_vad_struct.signal, slices_s, MIN_SPEECH);
    
%     feature_s = garbage_collection_on_feature_s(feature_s, params_s);
    stitched_voice.signal = check_slices(slices_s, MIN_SPEECH); % slice, min_speech
    stitched_voice.fs = slices_s(1).fs;
%     cls_fh = classifier_lib();
%     [val_vec, val_ind_vec, feat_struct] = cls_fh.calculate_flow_values_on_feature_struct (slices_s, flows_s);
    val_vec=0;
    val_ind_vec=0;
    feat_struct = [1,2,3];
end

function t_stitch = stitch_by_t_vector(signal, slices, MIN_SPEECH)
    t_stitch = [];
    for i=1:length(slices)    
        t = slices(i).t_vec_sig;
        for j=1:length(slices(i).t_vec_sig) > 0
            t_cur = t{j};
            if (t_cur(2)-t_cur(1))/slices(i).fs >= MIN_SPEECH
                t_stitch = [t_stitch signal(t_cur(1):t_cur(2))];
            end
        end
    end
end

function vad_stitch = stitch_by_vad(f, MIN_SPEECH)
    vad_vec = f.vad_vec;

    vad_vec = medfilt1(vad_vec, 31); vad_vec = medfilt1(vad_vec, 51); vad_vec = medfilt1(vad_vec, 71); vad_vec = medfilt1(vad_vec, 71);
    vad_vec = medfilt1(vad_vec, 131); vad_vec = medfilt1(vad_vec, 131); vad_vec = medfilt1(vad_vec, 171);
    
    st = []; nd = []; cs = cumsum(vad_vec);
    index = find(cs == 1, 1, 'first');
    
    while index < length(cs)-1
        st = [st index];
        while cs(index+1) > cs(index) && index < (length(cs)-1)
            index = index + 1;
        end
        nd = [nd index];
        while cs(index+1) == cs(index)
            index = index + 1;
        end
    end
    
    vad_stitch = [];
    for i=1:length(st)
        t_st = find(f.t_vec_sig == f.t_vec_80(st(i)));
        t_nd = find(f.t_vec_sig == f.t_vec_80(nd(i)));
        if (t_nd-t_st)/f.fs > MIN_SPEECH
            vad_stitch = [vad_stitch f.signal(t_st: t_nd)];        
        end
    end
    
end

function voice_stitched = check_slices(s, min_speech)
    voice_stitched = [];
%     index = 2;
    lens = 0;
    for i=1:length(s)
        if length(s(i).signal_only_voiced)/s(i).fs >= min_speech
            voice_stitched = [voice_stitched s(i).signal_only_voiced]; %#ok<*AGROW>
            lens = [lens length(s(i).signal_only_voiced)/s(i).fs];
            %             if isempty(new_s)
%                 voice_stitched = s(i);
%             else
%                 voice_stitched(index) = s(i);
%                 index = index + 1;
%             end
        end
    end
%     fprintf('stitched!! %d segments, total %.1f seconds, lens = %s\n', length(lens)-1, sum(lens), num2str(lens))
end

function feature_s = garbage_collection_on_feature_s(feature_s, params_s)
    vad_vec = feature_s.vad_vec;
    t_vec_80 = feature_s.t_vec_80;
    vad_fs = params_s.vad_fs;
    if length(vad_vec)/vad_fs > ...
                (params_s.max_relevance_time + params_s.relevance_tolerance)

        cand_ind = (t_vec_80>=(t_vec_80(end)-params_s.max_relevance_time));

        slice_start = find(cand_ind,1,'first');
        slice_end = length(t_vec_80);

        ind_s = calc_clock_inds(feature_s, slice_start, slice_end);

        feature_s = crop_feature_s(feature_s, ind_s, 'middle');                    
    end
end

% calculates progress of filling the next slice
function slicing_progress = update_slicing_progress(feature_s, params_s)      
    if params_s.slicing_progress_flag
        if isfield(feature_s,'vad_vec')
            trimmed_vad_vec = feature_s.vad_vec(max(1,end-params_s.max_relevance_time.*params_s.vad_fs):end);
            slicing_progress = params_s.min_speech - (sum(trimmed_vad_vec)/params_s.vad_fs);
        else
            slicing_progress = params_s.min_speech;
        end
    else
        slicing_progress = [];
    end
end

function feature_s = calc_basic_features(feature_s, time_offset)
%         run_libs();
 
    basic_feat_fh = basic_features_lib();  
    cls_fh = classifier_lib();
    % stats_utils_fh = stats_utils_lib();

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%% CALC_BASIC_FEATURES %%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    feature_s.input_buffer = [];

    % calc_fft_256_on_feature_struct();
    frame_overlap = feature_s.frame_overlap;
    [spectrum_256, f_vec_256, t_vec_80] = basic_feat_fh.param_specgram([frame_overlap feature_s.signal], feature_s.fs, 256, 80);
    powspec_256 = abs(spectrum_256).^2;
    feature_s.frame_overlap = feature_s.signal((end-256+80+1):end);
    feature_s.t_vec_sig = time_offset + (1:length(feature_s.signal))./feature_s.fs;
    feature_s.f_vec_256 = f_vec_256;
    feature_s.t_vec_80 = time_offset + t_vec_80;
    feature_s.spectrum_256 = spectrum_256;
    feature_s.powspec_256 = powspec_256;

    % calc_rasta_on_feature_struct();
    [rasta_mat, pure_rasta_mat] = basic_feat_fh.mat_rasta(feature_s.powspec_256, feature_s.f_vec_256, feature_s.t_vec_80);
    feature_s.rasta_mat = rasta_mat;
    feature_s.pure_rasta_mat = pure_rasta_mat;

    % calc_vad_vec_on_feature_struct();
    
    [vad_vec, raw_vad_vec] = cls_fh.vad_rasta_mat2vec(feature_s.rasta_mat, feature_s.f_vec_256, feature_s.t_vec_80);
    bp_ste_vec = cls_fh.bp_ste_on_powspeccell({feature_s.powspec_256, feature_s.f_vec_256, feature_s.t_vec_80});
    feature_s.vad_vec = vad_vec;
    feature_s.raw_vad_vec = raw_vad_vec;
    feature_s.bp_ste_vec = bp_ste_vec;

    % calc_fft_1024_on_feature_struct();
    [spectrum_1024, f_vec_1024, t_vec_512] = param_specgram(feature_s.signal, feature_s.fs, 1024, 512);
    powspec_1024 = abs(spectrum_1024).^2;

    feature_s.t_vec_512 = time_offset + t_vec_512;
    feature_s.powspec_1024 = powspec_1024;
    feature_s.f_vec_1024 = f_vec_1024;
    
end

% calculate new slices according ro VAD values
function [slices_start, slices_end] = calc_slices_by_vad(vad_vec, raw_vad_vec, params_s)

    % params
    if ~exist('params_s','var') || isempty(params_s)
        params_s = default_params();
    end

    % calculate new slices
    max_time = params_s.max_relevance_time; %seconds
    min_speech = params_s.min_speech; %seconds
    max_speech = params_s.max_speech; %seconds

%         vad_fs = 100;
    max_time_len = ceil(max_time*params_s.vad_fs); %3000
    min_vad = ceil(min_speech*params_s.vad_fs);
    max_vad = ceil(max_speech*params_s.vad_fs);

    slices_start = [];
    slices_end = [];
    i_cur = min_vad;

    cs_vad = cumsum(vad_vec);
    if cs_vad(end)>min_vad % if enough speech
        smoothing_n = ceil(params_s.vad_fs/10);
        smooth_raw_vad = conv(raw_vad_vec,ones(1,smoothing_n)/smoothing_n,'same');
        while i_cur <= length(cs_vad)
            if isempty(slices_end)            
                search_start = 1;
                %YS
%                 search_start = find(cs_vad>0, 1)
            else
%                 while(smooth_raw_vad(i_cur) < 1)
%                    i_cur = i_cur + 1; 
%                 end
%                 search_start = i_cur;
                search_start = slices_end(end)+1;
            end
            search_start = max(search_start, i_cur-max_time_len);
            if (cs_vad(i_cur) - cs_vad(search_start) > min_vad) && ...
                    ((cs_vad(i_cur) - cs_vad(search_start) > max_vad) || smooth_raw_vad(i_cur) < 1)
                    slices_start(end+1) = search_start;
                    slices_end(end+1) = i_cur;
                    i_cur = slices_end(end) + min_vad;
            else
                i_cur = i_cur + 1;
            end
        end
    end

end


function [slices_s, feature_s, features_out] = process_basic_features_into_slices(slices_s, feature_s, params_s)

     cls_fh = classifier_lib();

    %recalc vad... # TODO YS - no vad needed
    vad_vec = cls_fh.vad_vec_on_raw_vad_vec(feature_s.raw_vad_vec, feature_s.t_vec_80);
%     vad_vec = ones(size(feature_s.t_vec_80));
    hist_bp_ste = [];
    feature_s.vad_vec = cls_fh.post_vad_volume_separation_on_ste(vad_vec,feature_s.t_vec_80,feature_s.bp_ste_vec,hist_bp_ste, params_s.crit_snr);  
    
    features_out = feature_s;
    
%     feature_s.vad_vec = ones(size(feature_s.t_vec_80));

    %YS
% % % % % %     params_s.min_speech = 2;
% % % % % %     figure;plot(feature_s.vad_vec)
% % % % % %     
%     % calc slices
%     [slices_start, slices_end] = calc_slices_by_vad(feature_s.vad_vec, feature_s.raw_vad_vec, params_s);
    slices_start = 1;
    slices_end = length(feature_s.t_vec_80)-1;
    
    % slice data if possible
    if ~isempty(slices_start) && sum(feature_s.vad_vec)>0
        [new_slices_s, feature_s] = slice_and_shrink_by_vad(feature_s, slices_start, slices_end, params_s);
        
        new_slices_s = post_process_slices_after_vad(new_slices_s);

        if (isstruct(slices_s) && numel(fieldnames(slices_s)))
            slices_s = add_new_slices(slices_s, new_slices_s);
        else
            slices_s = new_slices_s;
        end
    end     
end

% postprocesses slices after vad
function slices_s = post_process_slices_after_vad(slices_s)
%     run_libs();
    psycho_feat_fh = psycho_features_lib();
    % rasta
    for i_slice = 1:length(slices_s)
        
        slices_s(i_slice).rasta_mat = strech_snr_on_rasta(slices_s(i_slice).pure_rasta_mat);
        
%         new_slices_s(i_slice) = add_post_vad_basic_features(slices_s(i_slice), [], params_s.basic_features);            
%         calc_voc_src_filt_on_feature_struct();
        [voc_src, voc_filt, ~, ~] = vocal_source_filter_separation(slices_s(i_slice).signal, slices_s(i_slice).fs,...
            slices_s(i_slice).spectrum_256, slices_s(i_slice).powspec_256,slices_s(i_slice).f_vec_256,...
            slices_s(i_slice).t_vec_80);
        slices_s(i_slice).voc_src_256 = voc_src;
        slices_s(i_slice).voc_filt_256 = voc_filt;
                
%         calc_pitch_mat_on_slices_struct();
        [pitch_mat, ct_vec, pitch_t_vec] = pitch_by_cepstrum_combined(slices_s(i_slice), fieldnames(slices_s(i_slice)), []);
        [pitch_f, pitch_a, pitch_t_vec] = pitch_contour_on_pitch_mat(pitch_mat, ct_vec, pitch_t_vec);
        slices_s(i_slice).pitch_mat_256 = pitch_mat;
        slices_s(i_slice).ct_vec_256 = ct_vec;
        slices_s(i_slice).pitch_f = pitch_f;
        slices_s(i_slice).pitch_a = pitch_a;
        slices_s(i_slice).pitch_t_vec = pitch_t_vec;

        % calc_psysound_on_feature_struct();
        psysound = struct;
        slices_s(i_slice).psysound = psysound;       

        % calc_dissonance_on_feature_struct();
        [dis_hk, diss_s, dtmap, f_vec_dtm] = psycho_feat_fh.TonalDissonance(slices_s(i_slice).powspec_256, ...
            slices_s(i_slice).f_vec_256, slices_s(i_slice).fs);
        slices_s(i_slice).dissonance_hk = dis_hk;
        slices_s(i_slice).dissonance_s = diss_s;
        slices_s(i_slice).diss_tonal_map = dtmap;
        slices_s(i_slice).f_vec_diss_tonal_map = f_vec_dtm;

        % calc_12th_octave_fft_on_feature_struct();
        [powspec_12thoct, f_vec_12thoct] = psycho_feat_fh.twelfth_oct_fft(slices_s(i_slice).powspec_1024, ...
            slices_s(i_slice).f_vec_1024);
        slices_s(i_slice).powspec_12thoct = powspec_12thoct;
        slices_s(i_slice).f_vec_12thoct = f_vec_12thoct;
                
    end
end
 
 % postprocesses slices after vad
function feature_s = post_process_slices_after_vad_OLD(slices_s, params_s, feature_s)
%     run_libs();
    psycho_feat_fh = psycho_features_lib();
    % rasta
    for i_slice = 1:length(slices_s)
        slices_s(i_slice).rasta_mat = strech_snr_on_rasta(slices_s(i_slice).pure_rasta_mat);

%         new_slices_s(i_slice) = add_post_vad_basic_features(slices_s(i_slice), [], params_s.basic_features);            
%         calc_voc_src_filt_on_feature_struct();
        [voc_src, voc_filt, ~, ~] = vocal_source_filter_separation(feature_s.signal, feature_s.fs,feature_s.spectrum_256, feature_s.powspec_256,feature_s.f_vec_256,feature_s.t_vec_80);
        feature_s.voc_src_256 = voc_src;
        feature_s.voc_filt_256 = voc_filt;
                
%         calc_pitch_mat_on_feature_struct();
        [pitch_mat, ct_vec, pitch_t_vec] = pitch_by_cepstrum_combined(feature_s, fieldnames(feature_s), []);
        [pitch_f, pitch_a, pitch_t_vec] = pitch_contour_on_pitch_mat(pitch_mat, ct_vec, pitch_t_vec);
        feature_s.pitch_mat_256 = pitch_mat;
        feature_s.ct_vec_256 = ct_vec;
        feature_s.pitch_f = pitch_f;
        feature_s.pitch_a = pitch_a;
        feature_s.pitch_t_vec = pitch_t_vec;

        % calc_psysound_on_feature_struct();
        psysound = struct;
        feature_s.psysound = psysound;       

        % calc_dissonance_on_feature_struct();
        [dis_hk, diss_s, dtmap, f_vec_dtm] = psycho_feat_fh.TonalDissonance(feature_s.powspec_256, feature_s.f_vec_256, feature_s.fs);
        feature_s.dissonance_hk = dis_hk;
        feature_s.dissonance_s = diss_s;
        feature_s.diss_tonal_map = dtmap;
        feature_s.f_vec_diss_tonal_map = f_vec_dtm;

        % calc_12th_octave_fft_on_feature_struct();
        [powspec_12thoct, f_vec_12thoct] = psycho_feat_fh.twelfth_oct_fft(feature_s.powspec_1024, feature_s.f_vec_1024);
        feature_s.powspec_12thoct = powspec_12thoct;
        feature_s.f_vec_12thoct = f_vec_12thoct;
                
    end
end
   
function [src, filt, f_vec, t_vec] = vocal_source_filter_separation(signal, fs, spectrum, powspec, f_vec, t_vec)
    lpc_order = 11;

%         wsize = 256;
%         step = 80;
% 
% 
%         % Window the signal
%         [first, last, prefix, postfix] = slidewin(numel(signal), wsize, step, wsize);
%         assert(all(prefix == 0));
%         last_full_frame = find(postfix == 0, 1, 'last');
% 
%         % Calculate full frames
%         [firsts, deltas] = meshgrid(first(1:last_full_frame), (0:(wsize-1)));
%         idx_mat = firsts + deltas;
%         wsignal = signal(idx_mat) .* repmat(hamming(wsize), [1, size(idx_mat, 2)]);
% %         [spectrum, f_vec, t_vec] = quick_spectrogram(signal,256,256-80,256,fs);
%         lpc_coeff = lpc(wsignal, lpc_order).';

    lpc_coeff = lpc_on_ready_powspec(powspec, lpc_order).';

%         % Add postfix partial frames
%         start_idx = last_full_frame + 1;
%         end_idx = start_idx-1 + find(last(start_idx:end)+1 - first(start_idx:end) > lpc_order, 1, 'last');
%         for idx = start_idx : end_idx
%             lpc_coeff(:, idx) = lpc(in_mat.signal(first(idx) : last(idx)).' .* hamming(last(idx)+1 - first(idx)), lpc_order).';
%         end

    % Create frequency response for each frame's filter
    % fast_allpole_freqz = @(B, A, f, fs) B ./ (exp(-1j*2*pi*f/fs * (0:(size(A, 1)-1))) * A);
    filt = fast_allpole_freqz(1, lpc_coeff, f_vec, fs);

    % output filter
    time_len = min(size(spectrum, 2), size(filt, 2));
    src = spectrum(:, 1:time_len) ./ (filt(:, 1:time_len)+eps);

    % feature extraction friendly real values
%         matmoodies(ispecgram(src, 256, 800, 256, 256-80), 8000);
    src = abs(src);
    filt = abs(filt);

    % remove nans
    if isnan(sum(sum(filt,1))) % faster 
        nan_mat = isnan(filt);
        src(nan_mat) = 0;
        filt(nan_mat) = 0;
    end

    % t_vec = (first(1:time_len)-1) / fs;

    function samples = fast_allpole_freqz(B, A, f, fs)

            % allpole filter order (number of poles)
            m = size(A, 1);

            % Create z^-m matrix (f - colvec, 0:m-1 - rowvec)
            z_mat = exp(-1j*2*pi*f/fs * (0:(m-1)));

            % sample the filter at the requested frequencies
            samples = B ./ (z_mat * A);
    end

    % custom version of LPC, utilizing the input spectrum calculation
    function [a,e] = lpc_on_ready_powspec(powspec, N)

            % Compute autocorrelation vector or matrix
            powspec = [powspec(1:(end-1),:); powspec(end:-1:2,:)];
%                 powspec = [powspec; powspec(end:-1:2,:)];
%                 X = fft(x,2^nextpow2(2*size(x,1)-1));
            m = 2^nextpow2((size(powspec,1)-1));
            R = ifft(powspec,m);
            R = R./m; % Biased autocorrelation estimate

            [a,e] = levinson(R,N);

            % Return only real coefficients for the predictor if the input is real
            for k = 1:size(powspec,2)
%                     if isreal(x(:,k))
                    a(k,:) = real(a(k,:));
%                     end
            end
        end

end

function rasta_spectrum = strech_snr_on_rasta(rasta_spectrum)        
    % improve SNR (contrast)
    max_spec = max(rasta_spectrum(:));
    if ~isempty(max_spec)
        atten_ind = (rasta_spectrum<=(max_spec./exp(5))) & (rasta_spectrum>=(max_spec./exp(15)));
        rasta_spectrum(atten_ind) = rasta_spectrum(atten_ind).^2./abs(max_spec./exp(5));   
    end
end

% reslice data
function [slices_s, ft_s] = slice_and_shrink_by_vad(ft_s, slices_start, slices_end, params)

    slices_s = struct;
    ind_s = [];
    for i_slice = 1:length(slices_start)
        if i_slice>1
            % append a new slice
            [slices_s(i_slice), ind_s] = cut_a_slice_by_vad(ft_s, slices_start(i_slice), slices_end(i_slice), ind_s);
        else 
            [slices_s, ind_s] = cut_a_slice_by_vad(ft_s, slices_start(i_slice), slices_end(i_slice), ind_s);
        end
        % shrink data in the new slice
        slices_s(i_slice) = shrink_slice_by_vad(slices_s(i_slice),params);
    end
    % trim start of feature_s
    ft_s = crop_feature_s(ft_s, ind_s, 'after_end');
end

function [ft_s, discarded_signal] = crop_feature_s(ft_s, ind_s, mode)
    % check for empty t_vecs
    if ~isfield(ft_s,'t_vec_80') || isempty(ft_s.t_vec_80) ||...
            isempty(ft_s.t_vec_512) || isempty(ft_s.t_vec_sig)
        discarded_signal = [];
        return;
    end
    % process input params
    ind_s = parse_ind_s(ind_s);

    switch mode
        case 'after_end'
            ind_80 = ft_s.t_vec_80>ft_s.t_vec_80(ind_s.t_ind_80(end));
            ind_512 = ft_s.t_vec_512>ft_s.t_vec_512(ind_s.t_ind_512(end));
            ind_sig = ft_s.t_vec_sig>ft_s.t_vec_sig(ind_s.t_ind_sig(end));
            dis_ind_sig = ft_s.t_vec_sig<=ft_s.t_vec_sig(ind_s.t_ind_sig(end));

        case 'before_start'                
            ind_80 = ft_s.t_vec_80<ft_s.t_vec_80(ind_s.t_ind_80(1));
            ind_512 = ft_s.t_vec_512<ft_s.t_vec_512(ind_s.t_ind_512(1));
            ind_sig = ft_s.t_vec_sig<ft_s.t_vec_sig(ind_s.t_ind_sig(1));
            dis_ind_sig = ft_s.t_vec_sig>=ft_s.t_vec_sig(ind_s.t_ind_sig(1));

        case 'middle'                
            ind_80 = ft_s.t_vec_80>=ft_s.t_vec_80(ind_s.t_ind_80(1)) &...
                            ft_s.t_vec_80<=ft_s.t_vec_80(ind_s.t_ind_80(end));
            ind_512 = ft_s.t_vec_512>=ft_s.t_vec_512(ind_s.t_ind_512(1)) &...
                            ft_s.t_vec_512<=ft_s.t_vec_512(ind_s.t_ind_512(end));
            ind_sig = ft_s.t_vec_sig>=ft_s.t_vec_sig(ind_s.t_ind_sig(1)) &...
                            ft_s.t_vec_sig<=ft_s.t_vec_sig(ind_s.t_ind_sig(end));
            dis_ind_sig = ft_s.t_vec_sig<ft_s.t_vec_sig(ind_s.t_ind_sig(1)) &...
                            ft_s.t_vec_sig>ft_s.t_vec_sig(ind_s.t_ind_sig(end));
    end

    % discarded signal
    discarded_signal = ft_s.signal(dis_ind_sig);        

    % trimmed data
    ft_s.signal = ft_s.signal(ind_sig);
    ft_s.t_vec_sig = ft_s.t_vec_sig(ind_sig);
    ft_s.t_vec_512 = ft_s.t_vec_512(ind_512);
    ft_s.t_vec_80 = ft_s.t_vec_80(ind_80);
    ft_s.powspec_1024 = ft_s.powspec_1024(:,ind_512);
    ft_s.powspec_256 = ft_s.powspec_256(:,ind_80);
    ft_s.spectrum_256 = ft_s.spectrum_256(:,ind_80);
    ft_s.rasta_mat = ft_s.rasta_mat(:,ind_80);
    ft_s.pure_rasta_mat = ft_s.pure_rasta_mat(:,ind_80);
    ft_s.vad_vec = ft_s.vad_vec(ind_80);
    ft_s.raw_vad_vec = ft_s.raw_vad_vec(ind_80);
    ft_s.bp_ste_vec = ft_s.bp_ste_vec(ind_80);

        % different input types
        function ind_s = parse_ind_s(ind_s)
            if ~isstruct(ind_s) 
                trim_len = ind_s;
                % calc clocks for trimmming
                ind_s = calc_trim_ind_s(trim_len);

            end

                % calculate the different clocks for trimming
                function ind_s = calc_trim_ind_s(trim_len)
                    ind_s.t_ind_sig = max(1,ceil(length(ft_s.t_vec_sig)-trim_len.*ft_s.fs)):...
                                  length(ft_s.t_vec_sig);    
                    ind_s.t_ind_80 = max(1,ceil(length(ft_s.t_vec_80)-trim_len.*ft_s.fs/80)):...
                                  length(ft_s.t_vec_80);
                    ind_s.t_ind_512 = convert_clock_ind(ft_s.t_vec_80,...
                                        ft_s.t_vec_512,ind_s.t_ind_80);  
                end
        end
end

% shrink vad to slices
function slice_s = shrink_slice_by_vad(slice_s, params)

        % reslicing of all rasta time resolution vectors and matrices
        vad_vec = slice_s.vad_vec;
        vad_vec_512 = interp1(slice_s.t_vec_80, vad_vec,...
                      slice_s.t_vec_512, 'nearest', 'extrap');

        if params.shrink_audio
            % reslice signal audio for natural speech
            [new_sig, out_takes, new_t_vec_sig] = shrink_audio_by_vad(slice_s.signal, slice_s.t_vec_sig, vad_vec, slice_s.t_vec_80, 0.2);

            % reslice signal for only voiced
%             [only_voiced, ~] = shrink_audio_by_vad(slice_s.signal, slice_s.t_vec_sig, double(slice_s.raw_vad_vec>1), slice_s.t_vec_80, 0.01);
            [only_voiced, ~, new_t_vec_sig] = shrink_audio_by_vad_yonatan(slice_s.signal, slice_s.t_vec_sig, double(slice_s.raw_vad_vec>1), slice_s.t_vec_80, 0.01);

            slice_s.signal = new_sig;
            slice_s.signal_only_voiced = only_voiced;
            slice_s.out_takes = [slice_s.out_takes out_takes];
            slice_s.t_vec_sig = new_t_vec_sig;

        end

        % delete columns
        del_i = find(vad_vec==0);
        del_i_512 = find(vad_vec_512==0);
        slice_s.vad_vec(del_i) = [];
        slice_s.raw_vad_vec(del_i) = [];
        slice_s.bp_ste_vec(del_i) = [];
        slice_s.t_vec_80(del_i) = [];
        slice_s.spectrum_256(:,del_i) = [];
        slice_s.powspec_256(:,del_i) = [];
        slice_s.rasta_mat(:,del_i) = [];
        slice_s.pure_rasta_mat(:,del_i) = [];
        slice_s.t_vec_512(del_i_512) = [];
        slice_s.powspec_1024(:,del_i_512) = [];

end

function [new_sig, out_takes, new_t_vec_sig] = shrink_audio_by_vad_yonatan(signal,t_vec_sig, vad_vec, t_vec_vad, fade_time)
%     figure;plot(vad_vec,'r');hold on
    
    vad_vec = medfilt1(vad_vec, 31);
    vad_vec = medfilt1(vad_vec, 51);
    vad_vec = medfilt1(vad_vec, 71);
    vad_vec = medfilt1(vad_vec, 71);
    vad_vec = medfilt1(vad_vec, 131);
    vad_vec = medfilt1(vad_vec, 131);
    vad_vec = medfilt1(vad_vec, 171);
%     plot(vad_vec, 'b*');legend('old','new');
    
    start_t_80 = t_vec_vad(diff([0 vad_vec])==1);
    end_t_80 = t_vec_vad(diff([vad_vec 0])==-1);

    start_p = interp1(t_vec_sig, 1:length(t_vec_sig), start_t_80, 'nearest', 'extrap');
    end_p = interp1(t_vec_sig, 1:length(t_vec_sig), end_t_80, 'nearest', 'extrap');
    
    old_sig = signal;
    
    new_sig = [];
    new_t_vec_sig = {}
    for jj=1:length(start_p)
        new_sig = [new_sig old_sig(start_p(jj):end_p(jj))];
        new_t_vec_sig{end+1} = [start_p(jj), end_p(jj)];
    end
    out_takes = [];
%     new_t_vec_sig = [];
       
  
end

function [new_sig, out_takes, new_t_vec_sig] = shrink_audio_by_vad(signal,t_vec_sig, vad_vec, t_vec_vad, fade_time)
    start_t_80 = t_vec_vad(diff([0 vad_vec])==1);
    end_t_80 = t_vec_vad(diff([vad_vec 0])==-1);

    start_p = interp1(t_vec_sig, 1:length(t_vec_sig), start_t_80, 'nearest', 'extrap');
    end_p = interp1(t_vec_sig, 1:length(t_vec_sig), end_t_80, 'nearest', 'extrap');

    old_sig = signal;
    new_sig = old_sig(start_p(1):end_p(1));
    new_t_vec_sig = t_vec_sig(start_p(1):end_p(1));
    out_takes = old_sig(1:start_p(1));
    len_fade = round(fade_time/(median(diff(t_vec_sig))));
    fade_out = linspace(1, 0, len_fade);
    fade_in = linspace(0, 1, len_fade);
    if length(start_p)>1
        for i_seg = 2:length(start_p)
            add_seg = old_sig(start_p(i_seg):end_p(i_seg));
            add_t_vec = t_vec_sig(start_p(i_seg):end_p(i_seg));

            if length(new_sig)>len_fade && length(add_seg)>len_fade
                len_new_sig = length(new_sig);      
                new_sig((len_new_sig-len_fade+1):len_new_sig) =...
                                    fade_out.*new_sig((len_new_sig-len_fade+1):len_new_sig)+...
                                    fade_in.*add_seg(1:len_fade);  

                new_sig((len_new_sig+1):(len_new_sig+length(add_seg)-len_fade)) =...
                                    add_seg((len_fade+1):end);

                % t_vec
                new_t_vec_sig((len_new_sig+1):(len_new_sig+length(add_seg)-len_fade)) =...
                                    add_t_vec((len_fade+1):end);
            else 
                s_len_fade = min(length(new_sig), length(add_seg));
                s_fade_out = linspace(1, 0, s_len_fade);
                s_fade_in = linspace(0, 1, s_len_fade);
                len_new_sig = length(new_sig);      
                new_sig((len_new_sig-s_len_fade+1):len_new_sig) =...
                                    s_fade_out.*new_sig((len_new_sig-s_len_fade+1):len_new_sig)+...
                                    s_fade_in.*add_seg(1:s_len_fade);

                new_sig((len_new_sig+1):(len_new_sig+length(add_seg)-s_len_fade)) =...
                                    add_seg((s_len_fade+1):end);

                % t_vec
                new_t_vec_sig((len_new_sig+1):(len_new_sig+length(add_seg)-s_len_fade)) =...
                                    add_t_vec((s_len_fade+1):end);
            end

            % update outtakes
            out_take_seg = old_sig(end_p(i_seg-1):start_p(i_seg));
            out_takes((end+1):(end+length(out_take_seg))) = out_take_seg;
        end
    end
    out_takes = [out_takes, old_sig(end_p(end):end)];
    % make the clocks line up to each other
    new_t_vec_sig(end) = t_vec_sig(end);
end

% cut a slice out
function [slice_s, ind_s] = cut_a_slice_by_vad(ft_s, slice_start, slice_end, prev_ind_s)

    % frequency vectors
    slice_s.fs = ft_s.fs;
    slice_s.f_vec_1024 = ft_s.f_vec_1024;
    slice_s.f_vec_256 = ft_s.f_vec_256;

    ind_s = calc_clock_inds(ft_s, slice_start, slice_end);
    t_ind_80 = ind_s.t_ind_80;
    t_ind_512 = ind_s.t_ind_512;
    t_ind_sig = ind_s.t_ind_sig;

    % reslicing of all rasta time resolution vectors and matrices
    slice_s.vad_vec = ft_s.vad_vec(t_ind_80);
    slice_s.raw_vad_vec = ft_s.raw_vad_vec(t_ind_80);
    slice_s.bp_ste_vec = ft_s.bp_ste_vec(t_ind_80);
    slice_s.t_vec_80 = ft_s.t_vec_80(t_ind_80);
    slice_s.powspec_256 = ft_s.powspec_256(:,t_ind_80);
    slice_s.spectrum_256 = ft_s.spectrum_256(:,t_ind_80);
    slice_s.rasta_mat = ft_s.rasta_mat(:,t_ind_80);
    slice_s.pure_rasta_mat = ft_s.pure_rasta_mat(:,t_ind_80);

    % reslicing of all 512 time resolution vectors and matrices
    slice_s.t_vec_512 = ft_s.t_vec_512(t_ind_512);
    slice_s.powspec_1024 = ft_s.powspec_1024(:,t_ind_512);

    % reslicing signal
    slice_s.signal = ft_s.signal(t_ind_sig);
    slice_s.t_vec_sig = ft_s.t_vec_sig(t_ind_sig);

    % out-takes
    if exist('prev_ind_s','var') &&...
                ~isempty(prev_ind_s) && isfield(prev_ind_s, 't_ind_sig')
        out_take_first = prev_ind_s.t_ind_sig(end);
    else
        out_take_first = 1;
    end
    slice_s.out_takes = ft_s.signal(out_take_first:t_ind_sig(1));

    % only voiced
    slice_s.signal_only_voiced = [];

    % flow type flag
    slice_s.flow_type = 'regular';
    % meta_data
    slice_s.meta_data = ft_s.meta_data;
end

function inds_clocks = calc_clock_inds(ft_s, slice_start, slice_end)
    % calculating the indeces of the clocks
    inds_clocks.t_ind_80 = (slice_start:slice_end);        
    inds_clocks.t_ind_512 = convert_clock_ind(ft_s.t_vec_80,ft_s.t_vec_512, inds_clocks.t_ind_80);
    inds_clocks.t_ind_sig = convert_clock_ind(ft_s.t_vec_80,ft_s.t_vec_sig, inds_clocks.t_ind_80);
    inds_clocks.t_ind_sig = max(1,inds_clocks.t_ind_sig(1)-40):min(length(ft_s.signal), inds_clocks.t_ind_sig(end)+40);
end

% converts between different clocks
function ind_vec_out = convert_clock_ind(t_vec_in, t_vec_out, ind_vec_in)
    [~, start_ind] = min(abs(t_vec_out-t_vec_in(ind_vec_in(1))));
    [~, end_ind] = min(abs(t_vec_out-t_vec_in(ind_vec_in(end))));
    ind_vec_out = start_ind:end_ind;
end
    
function [pitch_mat_comb, ct_vec, t_vec] = pitch_by_cepstrum_combined(s_mat, f_vec, t_vec)
        if isempty(s_mat) || ~iscell(f_vec) || ~isstruct(s_mat) || ~any(strcmpi('powspec_256',f_vec)) || ~any(strcmpi('powspec_1024',f_vec))
            [pitch_mat_comb, ct_vec, t_vec] = deal(0);
            return;
        end
        
        pow_256 = s_mat.powspec_256;
        f_vec_256 = s_mat.f_vec_256;
        t_vec_80 =  s_mat.t_vec_80;
        
        rasta_mat = s_mat.rasta_mat;
        
        pow_1024 = s_mat.powspec_1024;
        f_vec_1024 = s_mat.f_vec_1024;
        t_vec_512 =  s_mat.t_vec_512;
        
        %%%%        
        [pitch_mat_256, ct_vec_256, ~] = pitch_by_cepstrum(pow_256, f_vec_256, t_vec_80);
%         pitch_mat_256_interp = interp1(1./ct_vec_256(end:-1:1), pitch_mat_256(end:-1:1,:),...
%                                     new_f_vec_1024);
        
        [pitch_mat_rasta, ~, ~] = pitch_by_cepstrum(rasta_mat, f_vec_256, t_vec_80);
%         pitch_mat_rasta_interp = interp2(t_vec_80, 1./ct_vec_256(end:-1:1), pitch_mat_rasta(end:-1:1,:),...
%                                     t_vec_80, new_f_vec_1024);

        [pitch_mat_hps, new_f_vec_1024, t_vec_512] = pitch_by_hps(pow_1024, f_vec_1024, t_vec_512);
        pitch_mat_hps_interp = interp1(t_vec_512', pitch_mat_hps', t_vec_80','nearest',0)';
        pitch_mat_hps_interp = interp1q(1./new_f_vec_1024(end:-1:1),...
                                pitch_mat_hps_interp(end:-1:1,:), ct_vec_256');
        pitch_mat_hps_interp(isnan(pitch_mat_hps_interp)) = 0;
                
%         [pitch_mat_1024 ct_vec_1024 t_vec_512] = pitch_by_cepstrum(pow_1024, f_vec_1024, t_vec_512);
%         pitch_mat_1024_interp = interp2(t_vec_512, 1./ct_vec_1024(end:-1:1), pitch_mat_1024(end:-1:1,:),...
%                                     t_vec_80, new_f_vec_1024);
       
                                
        pitch_mat_comb = (pitch_mat_hps_interp+0.01).*(pitch_mat_256+0.001).*(pitch_mat_rasta+0.001).*(0+0.001)./(0.01*0.001*0.001*0.001);
        ct_vec = ct_vec_256;
        t_vec = t_vec_80;
        
% % %         n_plots = 4;
% % %         figure; 
% % %         subplot(n_plots,1,1);
% % %         imagesc(t_vec,ct_vec_256,(pitch_mat_256+eps)); 
% % %         subplot(n_plots,1,2);
% % %         imagesc(t_vec,ct_vec_256,(pitch_mat_rasta+eps)); 
% % % %         subplot(n_plots,1,3);
% % % %         imagesc(t_vec_80,new_f_vec_1024,(pitch_mat_1024_interp+eps)); axis xy; colorbar;
% % %         subplot(n_plots,1,3);
% % %         imagesc(t_vec,ct_vec_256,(pitch_mat_hps_interp)); 
% % %         subplot(n_plots,1,4);
% % %         imagesc(t_vec,ct_vec_256,log(pitch_mat_comb)); 
% % %         linkaxes(findobj(gcf,'type','axes'));
        
end

% pitch matrix by harmonic product spectrum
function [pitch_mat, new_f_vec, t_vec] = pitch_by_hps(s_mat, f_vec, t_vec)

    % consts
    f0_min = 65; %Hz
    f0_max = 650; %Hz
    max_harm = min(6, floor(f_vec(end)./f0_max));
    i_f_min = find(f_vec>=f0_min*max_harm,1,'first');
    i_f_max = find(f_vec<=f0_max*max_harm,1,'last');

    hps_mat = 1+zeros(size(s_mat));
    ind_final = (i_f_min):(i_f_max);
    pseudocount = 0.5/numel(ind_final);

    % product calculaton
    for i_harm = 1:max_harm
        ind_to_calc = floor(i_f_min*i_harm/max_harm):ceil(i_f_max*i_harm/max_harm);

        harm_mat = multipy_spec_mat(s_mat,f_vec, ind_to_calc,...
                                          max_harm./i_harm);

        % normalize
        harm_mat(ind_final,:) = harm_mat(ind_final,:)./...
                                (repmat(sum(harm_mat(ind_final,:),1),[length(ind_final) 1]) + eps);

        hps_mat(ind_final,:) = hps_mat(ind_final,:).*(harm_mat(ind_final,:) + pseudocount);

    end  

    pitch_mat = hps_mat(ind_final,:);

    max_spec = max(pitch_mat(:));
    pitch_mat = pitch_mat./max_spec;
    pitch_mat(pitch_mat<(1/10000)) = 1/10000;
    pitch_mat = log10(pitch_mat)-log10(1/10000);

    new_f_vec = f_vec(ind_final)./max_harm;


    % multiplys and upsamples a spectrum mat
    function out_mat = multipy_spec_mat(in_mat, f_vec, ind_to_calc, n_harm)
        if n_harm~=1
            out_mat = interp1(n_harm.*f_vec(ind_to_calc),...
                                    in_mat(ind_to_calc,:),...
                                    f_vec,'nearest',0);
        else
            out_mat = in_mat;
        end
    end

end
    

function [pitch_f_out, pitch_a_out, t_vec_out] = pitch_contour_on_pitch_mat(pitch_mat, f_vec, t_vec)
    
    f_vec = f_vec(:)';

    % first contour
    c_snr = 6;
    c_t_diff = 0.0005;
    f_range = [60 600];
    [pitch_f_out, ~, ~] = ...
        pitch_contour_on_pitch_mat_internal(pitch_mat, f_vec, t_vec, f_range, c_snr, c_t_diff);

    % second contour 
    % calc frequency range
    med_f = median(pitch_f_out);
    f_range = [0.5*med_f 2*med_f];
    % more demanding noise
    c_snr = 8;
    c_t_diff = 0.0005;
    [pitch_f_out, pitch_a_out, t_vec_out] = ...
        pitch_contour_on_pitch_mat_internal(pitch_mat, f_vec, t_vec, f_range, c_snr, c_t_diff);


        function [pitch_f_out, pitch_a_out, t_vec_out] = ...
                pitch_contour_on_pitch_mat_internal(pitch_mat, f_vec, t_vec, f_range, crit_snr, crit_t_diff) 

            % frequency
            if f_vec(end)>100 % frequencies
                f_ind = find(((f_vec >= f_range(1)) & (f_vec <= f_range(2))));
                [pitch_a, pitch_ind] = max(pitch_mat(f_ind,:),[],1);
                pitch_a = log(pitch_a+eps);

                pitch_f = f_vec(f_ind(pitch_ind));
            else              % quefrencies
                f_ind = find(((1./f_vec >= f_range(1)) & (1./f_vec <= f_range(2))));

                [pitch_a, pitch_ind] = max(pitch_mat(f_ind,:),[],1);
                pitch_a = log(pitch_a+eps);

                pitch_f = 1./f_vec(f_ind(pitch_ind));
            end

            % smth wrong with the input
            if isempty(pitch_f)
                [pitch_f_out, pitch_a_out, t_vec_out] = deal(0);
                return;
            end

            % remove noise
            [pitch_f_out, pitch_a_out, t_vec_out] = ...
                        filter_by_diff_and_amp(pitch_f, pitch_a, t_vec, crit_snr, crit_t_diff);

            [pitch_f_out, pitch_a_out, t_vec_out] = ...
                        filter_by_diff_and_amp(pitch_f_out, pitch_a_out, t_vec_out, crit_snr, crit_t_diff);

            % remove outliers
            [pitch_f_out, pitch_a_out, t_vec_out] = ...
                        remove_outliers(pitch_f_out, pitch_a_out, t_vec_out);

%                 % debug plots
%                 figure; hold on;
%                 imagesc(t_vec, f_vec, log(pitch_mat+eps), 'alphadata', 0.3); axis tight; axis ij; colorbar;
%                 plot(t_vec_out, 1./pitch_f_out, '.-');

        end            

        % remove noise by using frequency diff and amplitude
        function [pitch_f_out, pitch_a_out, t_vec_out] =...
                filter_by_diff_and_amp(pitch_f_in, pitch_a_in, t_vec_in, crit_snr, crit_diff)

            pitch_ct_in = 1./pitch_f_in;
            pitch_diff = max(abs(diff([pitch_ct_in(1) pitch_ct_in])),abs(diff([pitch_ct_in pitch_ct_in(end)])));

            % stability + amplitude criteria   
            crit_raw = (1 + (pitch_a_in - crit_snr)./abs(crit_snr)).*...
                    (2*crit_diff./(pitch_diff + crit_diff));
            
            %TODO
            stats_utils_fh = stats_utils_lib();
            % smoothing the ciriteria
            crit_smooth = stats_utils_fh.gap_vector_smoothing(crit_raw>1, 2, 0.6);

            % build back the edges
            diff_crit = diff(crit_smooth);
            crit_smooth((abs([diff_crit 0])==1) | (abs([0 diff_crit])==-1)) = 1;

            % nulling the pitch contour
            pitch_f_out = pitch_f_in(crit_smooth>=1);
            pitch_a_out = pitch_a_in(crit_smooth>=1);
            t_vec_out = t_vec_in(crit_smooth>=1);

        end

        function [pitch_f, pitch_a, t_vec] =...
                remove_outliers(pitch_f, pitch_a, t_vec)

            ct_vec = 1./(pitch_f+eps);
            pctl_vec = deal(quick_prctile(ct_vec, [25 50 75]));
%                 out_range = pctl_vec(3) - pctl_vec(1);
%                 
%                 outlier_ind = abs(ct_vec - pctl_vec(2)) > out_range; 
            outlier_ind = abs(ct_vec - pctl_vec(2)) > pctl_vec(2);

            % nulling the pitch contour
            pitch_f(outlier_ind) = [];
            pitch_a(outlier_ind) = [];
            t_vec(outlier_ind) = [];
        end
end

 % pitch matrix calculation based on cepstrum
function [pitch_mat, ct_vec, t_vec] = pitch_by_cepstrum(s_mat, f_vec, t_vec)
        
    % consts
    f0_min = 65; %Hz
    f0_max = 650; %Hz
    bpf_min = 65; %Hz
    bpf_max = 2000; %Hz

    % x_low
    % band pass 
    [s_low, f_vec] = band_pass_spectrum(s_mat, f_vec, bpf_min, bpf_max);
    % get cepstrum
    [cep_mat_low, ct_vec, ~] = fft_cepstrum(s_low, f_vec, t_vec, 'eac');
    cep_mat_low = real(cep_mat_low);        
    % eac enhancement
    ecep_mat_low = enhance_acf(cep_mat_low, ct_vec); 

    % band pass "lifter"        
    [ecep_mat_low, ct_vec] = crop_spectrum(ecep_mat_low, ct_vec, 1/f0_max, 1/f0_min);       
    pitch_mat = ecep_mat_low;


%         % x_high
%         s_high = calc_x_high_envelope(s_mat, f_vec, bpf_max);
%         % get cepstrum
%         [cep_mat_high ct_vec ~] = fft_cepstrum(s_high, f_vec, t_vec, 'eac');
%         cep_mat_high = real(cep_mat_high);
%         % eac enhancement
%         ecep_mat_high = enhance_acf(cep_mat_high, ct_vec);     
%         % band pass "lifter"        
%         [ecep_mat_high, ~] = crop_spectrum(ecep_mat_high, ct_vec, 1/f0_max, 1/f0_min);
%         
%         % combined
%         cep_comb = cep_mat_low + cep_mat_high;
%         % eac enhancement
%         ecep_comb = enhance_acf(cep_comb, ct_vec);     
%         % band pass "lifter"        
%         [ecep_comb, ct_vec] = crop_spectrum(ecep_comb, ct_vec, 1/f0_max, 1/f0_min);
%         pitch_mat = ecep_comb;
%         

%         % old calc
%         [cep_mat_old, ct_vec_old, ~] = fft_cepstrum(s_mat, f_vec, t_vec);
%         cep_mat_old = abs(cep_mat_old);
%         [cep_mat_old, ~] = crop_spectrum(cep_mat_old, ct_vec_old, 1/f0_max, 1/f0_min);
%         % augment contrasts using diff
%         abs_diff_mat = max(abs(diff(cep_mat_old([1 1:end],:),1,1)),...
%                            abs(diff(cep_mat_old([1:end end],:),1,1)));
%         aug_mat = (abs_diff_mat + 1).*(cep_mat_old + 1);      
%         pitch_mat = aug_mat;


        % band pass
        function [s_low, f_vec] = band_pass_spectrum(s_mat, f_vec, bpf_min, bpf_max)
            i_f_min = find(f_vec>=bpf_min,1,'first');
            i_f_max = find(f_vec<=bpf_max,1,'last');
            s_low = zeros(size(s_mat));
            s_low(i_f_min:i_f_max,:) = s_mat(i_f_min:i_f_max,:);
        end

        % highpass envelope
        function s_high = calc_x_high_envelope(s_mat, f_vec, bpf_max)
            i_f_max = find(f_vec<=bpf_max,1,'last');
            s_high = s_mat;
            s_high(1:i_f_max,:) = s_mat(1:i_f_max,:)./10^3;

            x_high = ispecgram(s_high, 256, 8000, 256, (256-80));
            x_high = x_high.*(x_high>0);
            [s_high, f_vec, ~] = simple_specgram(x_high, 8000);
            [s_high, ~] = band_pass_spectrum(s_high, f_vec, bpf_min, bpf_max);
        end

        % band pass
        function [s_out, f_vec] = crop_spectrum(s_mat, f_vec, bpf_min, bpf_max)
            % s_low
            i_f_min = find(f_vec>=bpf_min,1,'first');
            i_f_max = find(f_vec<=bpf_max,1,'last');
            s_out = s_mat(i_f_min:i_f_max,:);
            f_vec = f_vec(i_f_min:i_f_max);
        end

        % enhancement
        function cep_mat = enhance_acf(cep_mat, ct_vec)
            % eac
            cep_mat(cep_mat<0) = 0;
            cep2_mat = interp1(2.*ct_vec, cep_mat, ct_vec,'nearest',0);
            cep_mat = cep_mat - cep2_mat;
            cep_mat(cep_mat<0) = 0;
%                 cep3_mat = interp1(3.*ct_vec, cep_mat, ct_vec,'nearest',0);
%                 cep_mat = cep_mat - cep3_mat;
%                 cep_mat(cep_mat<0) = 0;
        end

        % scale matrix
        function out_mat = scale_mat(in_mat)
            in_mat = in_mat-min(in_mat(:));
            out_mat = in_mat./max(in_mat(:));
        end

end
    
 % cepstrum calculation
function [fft_cep, new_f_vec, t_vec] = fft_cepstrum(s_mat, f_vec, t_vec, mode)
    if ~exist('mode','var') || isempty(mode)
        mode = 'log';
    end

    % check input
    if ~isreal(s_mat) % complex spectrum
        mag_spectrum = abs(s_mat).^2;
    elseif any(s_mat<0) % log magnitude spectrum
        mag_spectrum = exp(s_mat);
    else % magnitude spectrum
        mag_spectrum = s_mat;
    end

    switch mode
        case 'eac'
%                 mag_spectrum = mag_spectrum.^0.33;
            mag_spectrum = exp(0.33.*log(mag_spectrum + eps)); % faster
            % add missing mirror part
            mag_spectrum = [mag_spectrum; mag_spectrum(end:-1:2,:)];
        case 'oneside'
            mag_spectrum = log(mag_spectrum+eps);
        otherwise 
            % add missing mirror part
            mag_spectrum = [mag_spectrum; mag_spectrum(end:-1:2,:)];
            mag_spectrum = log(mag_spectrum+eps);
    end            

    new_nfft = 2*(length(f_vec)-1);
%         new_nfft = length(f_vec);
    fs = (length(f_vec)-1)./(f_vec(end)-f_vec(1));
    new_f_vec = (fs/2*linspace(0,1,new_nfft/2+1));

    % perform ifft on each column
    fft_cep = ifft(mag_spectrum, new_nfft);
%         fft_cep = ifft(mag_spectrum);
    % discard the symmetric half
    fft_cep = fft_cep(1:length(new_f_vec),:);  
    % compute magnitude spectrum
    fft_cep = (fft_cep);
end
    

 % a quicker spectrogram with fewer argument checks and internal calls
function [y, f, t] = quick_spectrogram(x,win,noverlap,nfft,Fs)        
        try
            % Window length
            nwind = length(win);

%             f = psdfreqvec('npts',nfft,'Fs',Fs,'Range','half');
            f = get_f_vec(nfft,Fs);
            num_f = length(f);
            
            % Make x and win into columns
            x = x(:); 
            win = win(:); 
            
            ncol = fix((length(x)-noverlap)/(nwind-noverlap));
            colindex = (0:(ncol-1))*(nwind-noverlap)+1;

            % Calculate full frames
            [firsts, deltas] = meshgrid(colindex(1:ncol), (0:(nwind-1)));
            idx_mat = firsts + deltas;
            xin = x(idx_mat) .* repmat(win, [1, size(idx_mat, 2)]); 
            
            % Compute the raw STFT with the appropriate algorithm
%             [y,f] = computeDFT(xin,nfft,Fs);
            y_full = fft(xin,nfft);   
            y = y_full(1:num_f,:); % one sided            

            % colindex already takes into account the noverlap factor; Return a T
            % vector whose elements are centered in the segment.
            t = ((colindex)+((nwind)/2)')/Fs; 
            
        catch ex
            disp(['basic_features:quick_spectrogram: ' ex.message]);
            [y, f, t] = spectrogram(x,win,noverlap,nfft,Fs);
        end      
        
            function w = get_f_vec(nfft, fs)
                freq_res = fs/nfft;
                w = freq_res*(0:nfft-1);

                halfNPTS = (nfft/2)+1;
                w(halfNPTS) = fs/2;

                w = w(1:halfNPTS);
                w = w(:);                
            end
    end        
        
function [spectrum, f_vec, t_vec] = param_specgram(signal, fs, winpts, steppts)

    NFFT = winpts;
    WINDOW = [hann(winpts,'periodic')'];
    NOVERLAP = winpts - steppts;
    SAMPRATE = fs;

%         [spectrum, f_vec, t_vec] = spectrogram(signal,WINDOW,NOVERLAP,NFFT,SAMPRATE);
    [spectrum, f_vec, t_vec] = quick_spectrogram(signal,WINDOW,NOVERLAP,NFFT,SAMPRATE);

end