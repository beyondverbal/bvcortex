function run_feature_extract_feature_mat

clc, tic

% load params
tmp = load('C:\Users\yonatan.sasson\Documents\MATLAB\params');
params_s = tmp.params_s;
% params_s.shrink_audio = 0;
load ('C:\Users\yonatan.sasson\Documents\MATLAB\research\dbscripts\Mongo\Flows\TemperOriginal34Flows.mat', 'flows_s');
ref_feat_mat_filename = 'C:\BVC\BVC_CORE_OUTPUT\5.1.0.4\feature_matrix_MIX5.1.0.4_vad.csv';
ref_feat_mat = readtable(ref_feat_mat_filename, 'delimiter', 'comma'); 

flows_s = parse_flow_excel;

% input_data = readtable(in_file, 'ReadVariableNames', true);
FULL_SEGMENT = 80000;
WAVS = ref_feat_mat{:,'FileName'};
OFFSET = ref_feat_mat{:,'Start'};
fail_counter = 0;

RUN_TO = 10; %size(ref_feat_mat,1)
RES = nan(RUN_TO, size(flows_s,2));

for i = 1:RUN_TO % [2152, 4614,1976, 458] %29481      [2 1 3 5 6 9 10] %
    if mod(i,200)==0
        fprintf('i=%d\n',i);
    end
    if i==10; fprintf('i=10\n'); end
    wav = WAVS{i};
    seg_offset = OFFSET(i);
        
    [wav_from_file, fs] = audioread(strcat('C:\BVC\DownloadedVoices\',wav));
    if isnan(seg_offset)
%         start = 0;
        signal = wav_from_file(1:FULL_SEGMENT);
    else
        start = seg_offset*(8000/1000); % ms->samples
        signal = wav_from_file(start + 1: start + FULL_SEGMENT );
    end
        
    aux_vad_struct = struct('signal', signal', 'fs', fs, 'frame_overlap', []);
    
%     try
        [val_vec, ~, ~] = run_feature_extract(aux_vad_struct, params_s, flows_s);
        RES(i,:) = val_vec;
%     catch
%         fprintf('**** FAIL **** i = %d;  *** WAV = %s ***\n',i, wav);
%         fprintf(f_write,'**** FAIL **** i = %d;  *** WAV = %s ***\n',i, wav);
%         
%         fail_counter = fail_counter + 1;
%     end
%     
end
toc
compare_to_10(RES)

% save('C:\BVC\pythonanalyzer\src\MATLAB\TEMP\RES10_orig','RES')
fprintf('Time = %.2f secs, %.1f minutes;    FAILS = %d/%d\n', toc, toc/60, fail_counter, RUN_TO)

function compare_to_10(RES)
orig = load('TEMP\RES10_orig.mat');
orig = orig.RES;
fprintf('\nCOMPARISON, norm = %.2f\n\n',norm(orig(:)-RES(:)))

function junk
 %JUNK
% INSERT data into original feature matrix
[r_ref, ~] = size(ref_feat_mat);
[~, c_ref] = size(RES);
REF_OFFSET_COL = 6;
assert(size(ref_feat_mat,1) == size(RES,1));
ref_feat_mat(:, 1+(REF_OFFSET_COL + size(RES,2)):end) = []; %delete unneeded cols
for r = 1:r_ref
    for c = 1:c_ref
        
        ref_feat_mat(r, REF_OFFSET_COL + c) = RES_table(r,c);
    end
end

% writetable(T,'T3.csv','Delimiter',',','WriteVariableNames',true)
writetable(ref_feat_mat,'feature_matrix_new.csv','Delimiter',',','WriteVariableNames',true)

% figure;plot(RES(1,:),'r');hold on;plot(RES(2,:),'g');hold on;plot(RES(3,:),'b');hold on;title('runto=3, 205 features')
t=1; 

