function run_feature_extract_exe_wrapper

clc, tic

% ref_feat_mat_filename = 'C:\BVC\BVC_CORE_OUTPUT\5.2.0.1\feature_matrix_MIX5.2.0.1.csv';
% ref_feat_mat_filename = 'C:\BVC\BVC_CORE_OUTPUT\5.2.0.1\zMATLAB_TEST_features.csv';
% ref_feat_mat_filename = 'C:\BVC\YonaData1\slices2.csv';
ref_feat_mat_filename = 'C:\BVC\BVCortex\MATLAB\audio\pepsico1.csv';
% ref_feat_mat_filename = 'W:\Tatiana\Pepsico Texture MR\Analysis\from_liat_intro.csv';

cfg_file = 'C:\BVC\BVCortex\scripts\FeatureExtractionScripts\matlab_config.py';

run_feature_extract_exe(ref_feat_mat_filename, cfg_file);
