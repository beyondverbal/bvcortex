
function out_struct = parse_flow_excel(flows_file)

if nargin==0 || ~exist(flows_file,'file')
    
    fprintf('can''t read file / doesn''t exist - getting default flows\n');
    flows = get_default_flows;
else
    fprintf('reading flows from file: %s\n', flows_file);
    % flow_table = readtable('C:\Users\yonatan.sasson\Documents\MATLAB\MultiFlowsOfV3.xlsx');
    flow_table = readtable(flows_file);
    rows = size(flow_table,1);
    flows = cell(rows,1);

    for i=1:rows
        flows{i} = flow_table{i,'Flow'}{1};
    end
end

struct_index = 1;
for i=1:length(flows)
    if ~isempty(flows{i}) %&& strcmpi(flow_table{i,'IsDuplicate'}{1}, 'FALSE')
        tmp = flow_to_struct(flows{i}); %translate flow string to known struct
        out_struct(struct_index) = tmp; %#ok<AGROW>
        struct_index = struct_index +1;
    end    
end

fprintf('***************************\nPARSE_FLOW_FROM_EXCEL: Total lines: %d; total flows: %d\n***************************\n',...
    size(flows,1), struct_index-1);

function tmp = flow_to_struct(in)

indices = strfind(in,'::');
% assert(length(indices)==5)

tmp.sig2mat = {sprintf('%s', in(1            : indices(1)-1))};
tmp.sig2mat = {sprintf('%s', in(1            : indices(1)-1))};
% tmp.mat2mat = {sprintf('%s', in(indices(1)+2 : indices(2)-1))};
tmp.mat2mat = parse_dotdot(in(indices(1)+2 : indices(2)-1),':');
tmp.mat2vec = parse_dotdot(in(indices(2)+2 : indices(3)-2),'(');

switch length(indices)
    case 5
        tmp.vec2vec = parse_dotdot(in(indices(3)+2 : indices(4)-1),':');
        tmp.vec2val = parse_dotdot(in(indices(4)+2 : indices(5)-1),':');
        
    case 4
        tmp.vec2vec = parse_dotdot(in(indices(3)+2 : indices(4)-1),':');
        tmp.vec2val = parse_dotdot(in(indices(4)+2 : end),':');
    otherwise
        error('problem in ::');
        
end
tmp.flow_str = in;

function str = parse_dotdot(in, delimiter)

idx = strfind(in, delimiter);
if ~isempty(idx)
    obj1 = in(1:idx-1);
    obj2 = in(idx+1:end);
    
    if strcmpi(delimiter, '('); obj2 = str2double(obj2); end
    
    str = {obj1, obj2};
%     str = {sprintf('%s''  ''%s', in(1:idx-1), in(idx+1:end))};
%     
%     t{1}{1} = {sprintf('%s', in(1:idx-1))};
%     t{1}{2} = {sprintf('%s', in(1:idx-1))};
    
else
    str = {sprintf('%s', in)};
end


% tmp.sig2mat: {'feat_struct'}
% tmp.mat2mat: {'none'}
% tmp.mat2vec: {'pitch_a'  [1]}
% tmp.vec2vec: {'diff'  'absdiff'}
% tmp.vec2val: {'median'  ';'}
% tmp.flow_str: 'feat_struct::none::pitch_a(1)::diff:absdiff::median:;'


function default_flows = get_default_flows
default_flows = {
'feat_struct::@Harmonics.Distortion_1-10_4%_1000_4000::median(1)::absdiff::sum::;'
'feat_struct::@Harmonics.Distortion_5-10_12%_1000_2000::median(1)::absdiff::median::;'
'feat_struct::@Harmonics.Distortion_5-10_12%_1000_4000::median(1)::square::80-20pctl::;'
'feat_struct::@Harmonics.Distortion_5-10_20%_1000_2000::median(1)::square::80-20pctl::;'
'feat_struct::@Harmonics.Distortion_5-10_4%_0_4000::median(1)::rasta::sum::;'
'feat_struct::@Harmonics.Distortion_7-12_4%_1000_2000::80/20pctl(1)::rasta::tot_entropy::;'
'feat_struct::@Harmonics.Distortion_7-12_4%_1000_2000::median(1)::absdiff::80-20pctl::;'
'feat_struct::@Harmonics.Timbre_1-4_0%_1000_4000::std(2)::square::80pctl relfreq::;'
'feat_struct::@Harmonics.Timbre_5-15_20%_1000_2000::std/mean(1)::exp::80-20+mean::;'
'feat_struct::@Harmonics.Timbre_7-10_0%_1000_4000::mean(2)::none::tot_entropy::;'
'feat_struct::@Harmonics.Timbre_7-10_0%_1000_4000::skewness(1)::absdiff::std::;'
'feat_struct::@Harmonics.Timbre_7-10_0%_1000_4000::skewness(2)::clear_bottom_80::tot_entropy::;'
'feat_struct::@Harmonics.Timbre_7-10_0%_1000_4000::std/mean(1)::exp::skewness::;'
'feat_struct::@Harmonics.Timbre_7-10_0%_1000_4000::std/mean(1)::exp::tot_entropy::;'
'feat_struct::@Harmonics.Timbre_7-10_0%_1000_4000::sum(2)::log_abs::80-20+mean::;'
'feat_struct::@Harmonics.Timbre_7-10_0%_1000_4000::tot_entropy(1)::absdiff::std::;'
'feat_struct::@Harmonics.Timbre_7-12_20%_1000_4000::mean(2)::log_abs::80/20pctl::;'
'feat_struct::@Harmonics.Timbre_7-12_20%_1000_4000::skewness(1)::diff::std::;'
'feat_struct::@Harmonics.Timbre_7-12_20%_1000_4000::tot_entropy(1)::absdiff::skewness::;'
'feat_struct::12thOctFFT::80/20pctl(1)::digitize80:lpf_ma_10::std/mean:;'
'feat_struct::12thOctFFT::80pctl(1)::hpf_ma_1000:clear_bottom_20::skewness:;'
'feat_struct::12thOctFFT::80pctl(2)::clear_bottom_80:absdiff::band_sum_diff:;'
'feat_struct::12thOctFFT::80pctl(2)::lpf_ma_100:hpf_ma_10::band_sum_diff:;'
'feat_struct::12thOctFFT::80pctl(2)::none:none::50pctl relfreq:;'
'feat_struct::12thOctFFT::centroid(1)::absdiff:clear_bottom_20::tot_entropy:;'
'feat_struct::12thOctFFT::mean(1)::clear_bottom_20:log_abs::tilt_diff:;'
'feat_struct::12thOctFFT::mean(1)::lpf_ma_10:hist_vec_20::tilt_diff:;'
'feat_struct::12thOctFFT::median(1)::hpf_ma_100:lpf_ma_10::centroid:;'
'feat_struct::12thOctFFT::median(1)::hpf_ma_100:lpf_ma_10::log_rel_tilt:;'
'feat_struct::12thOctFFT::median(1)::lpf_ma_10:hist_vec_20::50pctl relfreq:;'
'feat_struct::12thOctFFT::median(1)::lpf_ma_10:hpf_ma_100::log_rel_tilt:;'
'feat_struct::12thOctFFT::median(1)::lpf_ma_10:rasta::log_rel_tilt:;'
'feat_struct::12thOctFFT::median(2)::digitize80:absdiff::log_rel_tilt:;'
'feat_struct::12thOctFFT::std(2)::clear_bottom_20:lpf_ma_100::80pctl relfreq:;'
'feat_struct::12thOctFFT::std(2)::diff:square::80-20+mean:;'
'feat_struct::12thOctFFT::std(2)::hpf_ma_10:clear_bottom_80::mean:;'
'feat_struct::12thOctFFT::std(2)::hpf_ma_1000:clear_bottom_20::band_sum_diff:;'
'feat_struct::12thOctFFT::std(2)::lpf_ma_10:clear_bottom_20::band_sum_diff:;'
'feat_struct::12thOctFFT::std(2)::lpf_ma_10:diff::80pctl relfreq:;'
'feat_struct::12thOctFFT::std(2)::lpf_ma_10:hpf_ma_100::80-20 pctl freq ratio:;'
'feat_struct::diss_tonal_map::80-20 pctl freq ratio(1)::absdiff:hist_vec_20::80pctl relfreq:;'
'feat_struct::diss_tonal_map::80-20 pctl freq ratio(1)::rem_outliers:lpf_ma_1000::sum:;'
'feat_struct::diss_tonal_map::80pctl freq(1)::log_abs:hist_vec_20::band_sum_diff:;'
'feat_struct::diss_tonal_map::centroid(1)::absdiff:lpf_ma_10::80/20pctl:;'
'feat_struct::diss_tonal_map::centroid(1)::hist_vec_20:absdiff::std/mean:;'
'feat_struct::diss_tonal_map::centroid(1)::lpf_ma_10:log_abs::sum:;'
'feat_struct::diss_tonal_map::centroid(1)::none:absdiff::std/mean:;'
'feat_struct::diss_tonal_map::centroid(1)::square:clear_bottom_20::tot_entropy:;'
'feat_struct::diss_tonal_map::kurtosis(1)::hist_vec_20:lpf_ma_10::80-20 pctl freq ratio:;'
'feat_struct::diss_tonal_map::kurtosis(1)::hist_vec_20:lpf_ma_10::centroid:;'
'feat_struct::diss_tonal_map::kurtosis(1)::hist_vec_20:rem_outliers::mean:;'
'feat_struct::diss_tonal_map::kurtosis(1)::hpf_ma_100:hist_vec_20::centroid:;'
'feat_struct::diss_tonal_map::kurtosis(1)::lpf_ma_10:lpf_ma_10::80-20+mean:;'
'feat_struct::diss_tonal_map::kurtosis(1)::none:hpf_ma_1000::tot_entropy:;'
'feat_struct::diss_tonal_map::mean(1)::clear_bottom_80:absdiff::80pctl:;'
'feat_struct::diss_tonal_map::mean(1)::clear_bottom_80:absdiff::std/mean:;'
'feat_struct::diss_tonal_map::mean(1)::clear_bottom_80:absdiff::tot_entropy:;'
'feat_struct::diss_tonal_map::mean(1)::hist_vec_20:square::std/mean:;'
'feat_struct::diss_tonal_map::mean(1)::hpf_ma_100:hpf_ma_100::80pctl:;'
'feat_struct::diss_tonal_map::mean(1)::hpf_ma_1000:clear_bottom_80::80-20+mean:;'
'feat_struct::diss_tonal_map::mean(1)::hpf_ma_1000:hpf_ma_100::80-20+mean:;'
'feat_struct::diss_tonal_map::mean(1)::hpf_ma_1000:hpf_ma_1000::80-20+mean:;'
'feat_struct::diss_tonal_map::mean(1)::lpf_ma_10:absdiff::80-20pctl:;'
'feat_struct::diss_tonal_map::mean(1)::lpf_ma_10:absdiff::std:;'
'feat_struct::diss_tonal_map::mean(1)::lpf_ma_10:hpf_ma_10::80-20pctl:;'
'feat_struct::diss_tonal_map::mean(1)::lpf_ma_10:hpf_ma_100::20pctl:;'
'feat_struct::diss_tonal_map::mean(1)::lpf_ma_10:hpf_ma_100::80pctl:;'
'feat_struct::diss_tonal_map::mean(1)::square:hpf_ma_1000::80pctl:;'
'feat_struct::diss_tonal_map::mean(2)::diff:lpf_ma_10::80-20 pctl freq ratio:;'
'feat_struct::diss_tonal_map::mean(2)::hpf_ma_10:lpf_ma_100::median:;'
'feat_struct::diss_tonal_map::mean(2)::hpf_ma_10:lpf_ma_100::rel_tilt:;'
'feat_struct::diss_tonal_map::mean(2)::lpf_ma_10:hpf_ma_10::tilt_start:;'
'feat_struct::diss_tonal_map::mean(2)::lpf_ma_10:hpf_ma_100::80-20 pctl freq ratio:;'
'feat_struct::diss_tonal_map::mean(2)::lpf_ma_10:lpf_ma_10::std:;'
'feat_struct::diss_tonal_map::mean(2)::lpf_ma_10:lpf_ma_100::skewness:;'
'feat_struct::diss_tonal_map::mean(2)::lpf_ma_100:clear_bottom_20::rel_tilt:;'
'feat_struct::diss_tonal_map::mean(2)::lpf_ma_100:hpf_ma_10::80pctl relfreq:;'
'feat_struct::diss_tonal_map::mean(2)::lpf_ma_100:hpf_ma_100::mean:;'
'feat_struct::diss_tonal_map::mean(2)::none:hpf_ma_100::tot_entropy:;'
'feat_struct::diss_tonal_map::mean(2)::none:lpf_ma_100::std/mean:;'
'feat_struct::diss_tonal_map::mean(2)::rem_outliers:clear_bottom_20::skewness:;'
'feat_struct::diss_tonal_map::mean(2)::square:lpf_ma_100::band_sum_diff:;'
'feat_struct::diss_tonal_map::mean(2)::square:square::std/mean:;'
'feat_struct::diss_tonal_map::skewness(1)::hpf_ma_10:square::sum:;'
'feat_struct::diss_tonal_map::skewness(1)::none:rasta::80-20+mean:;'
'feat_struct::diss_tonal_map::skewness(2)::absdiff:hpf_ma_10::80-20pctl:;'
'feat_struct::diss_tonal_map::skewness(2)::absdiff:lpf_ma_10::80pctl:;'
'feat_struct::diss_tonal_map::skewness(2)::rem_outliers:absdiff::20pctl:;'
'feat_struct::diss_tonal_map::skewness(2)::rem_outliers:absdiff::mean:;'
'feat_struct::diss_tonal_map::std(1)::clear_bottom_80:absdiff::kurtosis:;'
'feat_struct::diss_tonal_map::std(1)::clear_bottom_80:absdiff::skewness:;'
'feat_struct::diss_tonal_map::std(1)::clear_bottom_80:absdiff::std/mean:;'
'feat_struct::diss_tonal_map::std(1)::clear_bottom_80:absdiff::tot_entropy:;'
'feat_struct::diss_tonal_map::std(1)::digitize80:hpf_ma_10::std:;'
'feat_struct::diss_tonal_map::std(1)::hist_vec_20:hpf_ma_10::50pctl relfreq:;'
'feat_struct::diss_tonal_map::std(1)::square:lpf_ma_100::std/mean:;'
'feat_struct::diss_tonal_map::std(2)::clear_bottom_20:hpf_ma_100::80-20 pctl freq ratio:;'
'feat_struct::diss_tonal_map::std(2)::clear_bottom_20:hpf_ma_100::tot_entropy:;'
'feat_struct::diss_tonal_map::std(2)::diff:hpf_ma_100::centroid:;'
'feat_struct::diss_tonal_map::std(2)::hpf_ma_10:hpf_ma_100::std:;'
'feat_struct::diss_tonal_map::std(2)::hpf_ma_100:clear_bottom_20::mean:;'
'feat_struct::diss_tonal_map::std(2)::hpf_ma_100:clear_bottom_20::std:;'
'feat_struct::diss_tonal_map::std(2)::lpf_ma_10:clear_bottom_20::kurtosis:;'
'feat_struct::diss_tonal_map::std(2)::lpf_ma_10:diff::80pctl relfreq:;'
'feat_struct::diss_tonal_map::std(2)::lpf_ma_100:diff::skewness:;'
'feat_struct::diss_tonal_map::std(2)::none:none::skewness:;'
'feat_struct::diss_tonal_map::std(2)::none:rem_outliers::skewness:;'
'feat_struct::diss_tonal_map::std(2)::none:square::std/mean:;'
'feat_struct::diss_tonal_map::std(2)::rem_outliers:clear_bottom_20::skewness:;'
'feat_struct::diss_tonal_map::std(2)::rem_outliers:hpf_ma_10::skewness:;'
'feat_struct::diss_tonal_map::std(2)::rem_outliers:lpf_ma_10::centroid:;'
'feat_struct::diss_tonal_map::std(2)::rem_outliers:square::std/mean:;'
'feat_struct::diss_tonal_map::std(2)::square:hpf_ma_1000::20pctl relfreq:;'
'feat_struct::diss_tonal_map::std(2)::square:lpf_ma_100::centroid:;'
'feat_struct::diss_tonal_map::std/mean(1)::absdiff:lpf_ma_1000::80pctl:;'
'feat_struct::diss_tonal_map::std/mean(1)::hist_vec_20:absdiff::centroid:;'
'feat_struct::diss_tonal_map::std/mean(1)::hist_vec_20:lpf_ma_10::centroid:;'
'feat_struct::diss_tonal_map::std/mean(1)::log_abs:absdiff::avg_entropy:;'
'feat_struct::diss_tonal_map::std/mean(2)::diff:clear_bottom_80::mean:;'
'feat_struct::diss_tonal_map::std/mean(2)::hpf_ma_100:absdiff::80-20pctl:;'
'feat_struct::diss_tonal_map::std/mean(2)::rem_outliers:absdiff::80pctl:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::absdiff:clear_bottom_80::sum:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::diff:lpf_ma_10::std:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::hist_vec_20:log_abs::kurtosis:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::hpf_ma_10:square::std:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::hpf_ma_100:rasta::80-20+mean:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::hpf_ma_100:rasta::80pctl:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::hpf_ma_100:rem_outliers::std:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::hpf_ma_100:square::sum:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::hpf_ma_1000:rasta::80-20+mean:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::hpf_ma_1000:rasta::80-20pctl:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::lpf_ma_10:absdiff::std:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::lpf_ma_10:hpf_ma_100::20pctl:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::lpf_ma_10:square::80-20pctl:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::rasta:clear_bottom_20::80-20+mean:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::rasta:hist_vec_20::log_rel_tilt:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::rasta:lpf_ma_10::80/20pctl:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::rasta:lpf_ma_10::80-20+mean:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::rasta:rasta::80pctl:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::rasta:rem_outliers::80-20+mean:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::rasta:rem_outliers::std/mean:;'
'feat_struct::diss_tonal_map::tot_entropy(1)::square:hpf_ma_1000::80pctl:;'
'feat_struct::diss_tonal_map::tot_entropy(2)::hpf_ma_100:hpf_ma_100::20pctl relfreq:;'
'feat_struct::diss_tonal_map::tot_entropy(2)::hpf_ma_1000:lpf_ma_10::mean:;'
'feat_struct::diss_tonal_map::tot_entropy(2)::lpf_ma_10:clear_bottom_20::20pctl relfreq:;'
'feat_struct::diss_tonal_map::tot_entropy(2)::lpf_ma_10:hpf_ma_100::80-20 pctl freq ratio:;'
'feat_struct::diss_tonal_map::tot_entropy(2)::rem_outliers:square::std:;'
'feat_struct::diss_tonal_map::tot_entropy(2)::square:square::std:;'
'feat_struct::dissonance_hk::80/20pctl(1)::absdiff:hist_vec_20::80-20+mean:;'
'feat_struct::dissonance_hk::80/20pctl(1)::hpf_ma_10:hpf_ma_100::tot_entropy:;'
'feat_struct::dissonance_hk::80/20pctl(1)::lpf_ma_100:log_abs::80-20pctl:;'
'feat_struct::dissonance_hk::median(1)::hist_vec_20:absdiff::80-20+mean:;'
'feat_struct::dissonance_hk::median(1)::hist_vec_20:hpf_ma_10::80-20pctl:;'
'feat_struct::dissonance_hk::median(1)::hist_vec_20:hpf_ma_100::80pctl:;'
'feat_struct::dissonance_hk::median(1)::hist_vec_20:square::centroid:;'
'feat_struct::dissonance_hk::median(1)::log_abs:hpf_ma_1000::tot_entropy:;'
'feat_struct::dissonance_hk::median(1)::lpf_ma_10:rem_outliers::tot_entropy:;'
'feat_struct::dissonance_hk::median(1)::lpf_ma_100:lpf_ma_100::tot_entropy:;'
'feat_struct::dissonance_hk::median(1)::rasta:hist_vec_20::80-20+mean:;'
'feat_struct::none::pitch_a(1)::diff:absdiff::median:;'
'feat_struct::none::pitch_f(1)::absdiff::sum::;'
'feat_struct::none::pitch_f(1)::rem_outliers::20pctl::;'
'feat_struct::pitch_mat::80pctl freq(1)::none::skewness::;'
'feat_struct::pitch_mat::80pctl freq(1)::rem_outliers:rasta::20pctl:;'
'feat_struct::pitch_mat::80pctl freq(1)::square:rasta::std/mean:;'
'feat_struct::pitch_mat::80pctl(2)::log_abs::tot_entropy::;'
'feat_struct::pitch_mat::centroid(1)::square::20pctl::;'
'feat_struct::pitch_mat::high-med(1)::edge_detection::kurtosis::;'
'feat_struct::pitch_mat::high-med(1)::log_abs::std::;'
'feat_struct::pitch_mat::lm_norm(1)::log_abs::80pctl::;'
'feat_struct::pitch_mat::mean(2)::square:absdiff::80pctl relfreq:;'
'feat_struct::pitch_mat::std(2)::log_abs:clear_bottom_20::20pctl relfreq:;'
'feat_struct::pitch_mat::std(2)::norm:log_abs::band_sum_ratio:;'
'feat_struct::pitch_mat::std/mean(2)::log_abs::20pctl relfreq::;'
'feat_struct::pitch_mat::sum(2)::log_abs::80pctl relfreq::;'
'feat_struct::pitch_mat::sum(2)::log_abs:exp::avg_entropy:;'
'feat_struct::pitch_mat::tot_entropy(1)::log_abs:square::skewness:;'
'feat_struct::pitch_mat::tot_entropy(1)::square:square::skewness:;'
'feat_struct::pitch_mat::tot_entropy(2)::norm::50pctl relfreq::;'
'feat_struct::pitch_mat::tot_entropy(2)::square::tot_entropy::;'
'feat_struct::powspec::80pctl(1)::clear_bottom_20::tot_entropy::;'
'feat_struct::powspec::lm_norm(1)::rem_outliers::skewness::;'
'feat_struct::rasta::80/20pctl(2)::log_abs::20pctl relfreq::;'
'feat_struct::rasta::80pctl freq(1)::clear_bottom_20::mean::;'
'feat_struct::rasta::80pctl(2)::log_abs:norm::80-20 pctl freq ratio:;'
'feat_struct::rasta::mean(1)::none::median::;'
'feat_struct::rasta:rasta2cls_nooverlap::high-med(1)::none::mean:;'
'feat_struct::vocal_filter::80/20pctl(2)::log_abs::80-20 pctl freq ratio::;'
'feat_struct::vocal_filter::skewness(1)::clear_bottom_20::skewness::;'
'feat_struct::vocal_filter::std/mean(1)::log_abs:absdiff::median:;'
'feat_struct::vocal_filter::std/mean(1)::rem_outliers:clear_bottom_20::skewness:;'
'feat_struct::vocal_filter::tot_entropy(1)::square::skewness::;'
'feat_struct::vocal_source::80pctl(2)::rem_outliers::skewness::;'
'feat_struct::vocal_source::80pctl(2)::square::20pctl relfreq::;'
'feat_struct::vocal_source::mean(2)::absdiff::band_sum_ratio::;'
'feat_struct::vocal_source::mean(2)::square::skewness::;'
'feat_struct::vocal_source::skewness(1)::exp::mean::;'
'feat_struct::vocal_source::std(1)::norm::median::;'
'feat_struct::vocal_source::std/mean(1)::clear_bottom_80::kurtosis::;'
'feat_struct::vocal_source::std/mean(1)::digitize80:absdiff::std:;'
'feat_struct::vocal_source::std/mean(1)::log_abs:sign_square::80/20pctl:;'
'feat_struct::vocal_source::std/mean(2)::none::80/20pctl::;'
'feat_struct::vocal_source::tot_entropy(2)::clear_bottom_20::20pctl relfreq::;'
'feat_struct::vocal_source::tot_entropy(2)::diff:log_abs::80pctl:;'
'feat_struct::vocal_source::tot_entropy(2)::rem_outliers::20pctl relfreq::;'
};
