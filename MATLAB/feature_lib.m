function varargout = feature_lib(varargin)
% library file for different feature manipulation functions

libs = struct;
libs.lib_fh_names = {'basic_feat_fh','class_lib_fh','flow_lib_fh',...
                      'flow_io_lib_fh','edb_fh','stats_fh', ...
                      'psycho_feat_fh', 'praat_feat_fh', ...
                      'psysound_feat_fh', 'harmonics_feat_fh', 'vsauce_feat_fh'};
libs.lib_functions = {'basic_features_lib','classifier_lib','flow_management_lib',...
                      'matlabFlowIOLib','edb_methods','stats_utils_lib', ...
                      'psycho_features_lib', 'praat_features_lib', ...
                      'psysound_features_lib', 'harmonics_features_lib', 'vsauce_features_lib'};
run_libs('init');

f_handles = struct(... % recommended way to pass nested func handles
        'run_libs', @run_libs,...
        'mat2mat_serial', @mat2mat_serial,...
        'vec2vec_serial', @vec2vec_serial,...
        'sig2mat_transform', @sig2mat_transform,...
        'mat2mat_transform', @mat2mat_transform,...
        'mat2vec_reduce', @mat2vec_reduce,...
        'vec2vec_transform', @vec2vec_transform,...
        'vec2val_reduce', @vec2val_reduce,...
        'process_feature_flow', @process_feature_flow,...
        'input_cell_type', @input_cell_type,...
        'processHAs', @processHAs, ...
        'get_harmonic', @get_harmonic ...
         );

func_names.sig2mat = {'specgram_256','specgram_1024',...
                      'anlzr_feat','feat_struct', 'res_struct',...
                      'feat_struct_cache', 'res_struct_cache',...
                      'anlzr_feat_cache','filename','raw_signal'};
     
func_names.mat2mat = [...
                       
                       % Built-in features
                        {'none',...
                          '<html><b><i>* SPECTRAL:',...
                                'rasta','rasta_vad','rasta2cls','rasta2cls_nooverlap','fft_cepstrum',...
                                'pitch_mat','vocal_source','vocal_filter','powspec',...
                                'band_pass','g711_bandpass','anti_g711',...
                          '<html><b><i>* MATH:','norm','log_abs','exp','square','sqrt',...
                                'diff',...
                          '<html><b><i>* REDUCT:',...     
                                'add_noise',...
                                'digitize80','clear_bottom_80','digitize95','digitize_95_80',...
                                'edge_detection', ...
                                'dissonance_s', 'dissonance_hk',...
                                '12thOctFFT', 'diss_tonal_map'...
                            } ...
...
...                    % Praat features     
                       build_feature_list('Praat', praat_features_lib('populate_features')) ...
...                       
...                    % Psysound features     
                       build_feature_list('Psysound', psysound_features_lib('populate_features')) ...
...                       
...                    % Harmonics features     
                       build_feature_list('Harmonics', harmonics_features_lib('populate_features')) ...
...
...                    % Praat features     
                       build_feature_list('VoiceSauce', vsauce_features_lib('populate_features')) ...                       
...                       
                       ];
                   
                   
func_names.mat2vec = {'<html><b><i>* STAT:',...
                          'median','mean','80pctl','80/20pctl','std','sum',...
                          'mode','skewness','kurtosis','std/mean','tot_entropy','randvec'...
                      '<html><b><i>* SPECTRAL:',...
                          '80pctl freq', 'centroid',...
                          'band_sum_ratio','subband_correlation',...
                          '80-20 pctl freq ratio',...
                      '<html><b><i>* FEATURES:',...
                          'energy_scale','hm_norm','lm_norm',...
                          'high-med','low-med','composure','temper',...
                          'vad_vec','pitch_f','pitch_a',...
                          'bp_ste_vec','anlzr_outG','anlzr_outS',...
                      '<html><b><i>* MISC:',...
                            'first_el','rs_valence_val','rs_arousal_val',...
                            'rs_temper_val','rs_gender_val','rs_audio_quality_val',...
                            'rs_trend_overall_val','rs_trend_improvement_val',...
                            'labeled_valence','labeled_moodgroup21','labeled_moodgroup11', ...
                            'anlzr_outH'...
                            };

func_names.vec2vec = {'none','rem_outliers','norm','log_abs','exp',...
                        'square','sign_square','rasta','diff','absdiff',...
                        'digitize80','clear_bottom_20','clear_bottom_80',...
                        'edge_detection','hist_vec_20',...
                        'lpf_ma_10', 'lpf_ma_100', 'lpf_ma_1000',...
                        'hpf_ma_10', 'hpf_ma_100', 'hpf_ma_1000',...
                        'temp_analysis'...
                        };

func_names.vec2val = {'<html><b><i>* STAT:',...
                        'median','mean','80pctl','20pctl','80/20pctl','80-20pctl','80-20+mean'...
                        'std','sum','mode','skewness','kurtosis',...
                        'std/mean','tot_entropy','avg_entropy','randval',...
                      '<html><b><i>* SPECTRAL:',...
                        'log_rel_tilt','rel_tilt','tilt_diff','tilt_start',...
                        'centroid','80pctl relfreq','20pctl relfreq','50pctl relfreq',...
                        '80-20 pctl freq ratio',...
                        'band_sum_ratio','band_sum_diff','last 0.2 freq',...
                      '<html><b><i>* OTHER:',...
                        'numel_over_threshold','temper_enum',...
                        'rs_gender_val','rs_valence_val','rs_arousal_val',...
                        'rs_temper_val','rs_audio_quality_val',...
                        'rs_valence1', 'rs_valence2',...
                        'rs_moodgroup11_1', 'rs_moodgroup11_2', 'rs_moodgroup21_1', 'rs_moodgroup21_2',...
                        'rs_temper_2d', 'rs_energy',...
                        'rs_energyVal', 'rs_energyEnum','rs_highMed', 'rs_lowMed','rs_flow_1', 'rs_flow_2',...
                       '<html><b><i>* MISC:',...
                            'temp_analysis',...
                            'el_1','el_2','el_3','el_4','el_5',...
                            'el_6','el_7','el_8','el_9','el_10',...
                            'el_11','el_12','el_13','el_14','el_15',...
                            'el_16','el_17','el_18','el_19','el_20',...
                            'el_21',...
                            'max'...
                            };

f_handles.func_names = func_names;
varargout{1} = f_handles;

%%%nested

    %%% load libs when necessary
    function run_libs(lib_fh_toload)
        if nargin < 1 % load all
            lib_fh_toload = libs.lib_fh_names;
        end
        % assign each function hande a lib (or init all if init param is
        % passed)
        if any(strcmpi('init', lib_fh_toload))
            for i_lib = 1:numel(libs.lib_fh_names)
                libs.(libs.lib_fh_names{i_lib}) = [];
            end
        else
            for i_toload = 1:numel(lib_fh_toload)
                tf = strcmpi(lib_fh_toload{i_toload}, libs.lib_fh_names);
                i_lib = find(tf,1,'first');
                if any(tf) && isempty(libs.(libs.lib_fh_names{i_lib}))                    
                    try
                        libs.(libs.lib_fh_names{i_lib}) = eval([libs.lib_functions{i_lib} '()']);
                    catch ex
                        disp(['error loading ' libs.lib_functions{i_lib} ' : ' ex.message]);
                    end
                end
            end     
        end
    end

    % Add feature families to the features list
    function feature_list = build_feature_list(family_name, feature_names)
        
        feature_list = cell(1, 1+numel(feature_names));
        
        feature_list{1} = ['<html><b><i>* ' family_name ':'];
        
        for n = 1:numel(feature_names)
            feature_list{1+n} = ['@' family_name '.' feature_names{n}];
        end
        
    end

    % fix path 
    function str_out = fix_path(str_in)
          k1 = strfind(str_in, 'matmoodies');
          mfile_path = mfilename('fullpath');
          k2 = strfind(mfile_path, 'matmoodies');
          str_out = [mfile_path(1:(k2-1))  str_in(k1:length(str_in))];
    end

    % infer type of input cell
    function type_str = input_cell_type(input_cell)
        switch numel(input_cell)
            case 2
                if ischar(input_cell{2})
                    type_str = 'feature_val';
                elseif numel(input_cell{2})==1
                    type_str = 'sig';
                elseif numel(input_cell{2})>300 && mode(diff(input_cell{2}))<1
                    type_str = 't_vec';
                elseif numel(input_cell{2})>30 && mode(diff(input_cell{2}))>1
                    type_str = 'f_vec';
                else
                    type_str = 'feature_vec';
                end
            case 3
                if size(input_cell{1},1)>30 && size(input_cell{1},2)>300 
                    type_str = 'spec_mat';
                else
                    type_str = 'feature_mat';
                end
            otherwise
                type_str = 'uknown';
        end
    end

%     % handle input arguments
%     function varargout = parse_inputs(input_cell)
%         
%     end

    % signal to matrix transform
%     function varargout = sig2mat_transform(varargin) 
    function [out_mat, f_vec, t_vec] = sig2mat_transform(signal, fs, f_name, context)
        
%         [signal, fs, f_name] = parse_inputs(varargin);
        
        if isempty(signal) || any(isnan(signal))
            signal = zeros(1,100);
            fs = 1;
        end
        if ~exist('context','var') || isempty(context)
           context = ''; 
        end
        switch f_name
            case 'specgram_256'
                run_libs({'basic_feat_fh'});
                [out_mat, f_vec, t_vec] = libs.basic_feat_fh.simple_specgram(signal, fs);
%                 out_mat = abs(out_mat).^2;
            case 'specgram_1024'
                run_libs({'basic_feat_fh'});
                [out_mat, f_vec, t_vec] = libs.basic_feat_fh.param_specgram(signal, fs, 1024, 512);
                out_mat = abs(out_mat).^2;
            case 'anlzr_feat'
                run_libs({'flow_lib_fh','class_lib_fh'});
                
                buff_length = 20;
                overlap_ratio = 0.8;
                seg_s = libs.flow_lib_fh.calc_simple_segments(...
                                    signal, fs, buff_length, overlap_ratio);
                                
                % add signal field
                for seg_i=1:length(seg_s)
                    seg_s(seg_i).signal = signal(seg_s(seg_i).signal_ind_vec);
                end
                
                % t_vec
                t_vec = arrayfun(@(x)(x.signal_ind_vec(1) + x.signal_ind_vec(end))/(2*fs),...
                                     seg_s);

                % calc analyzer values
                seg_s = calc_analyzer_dotnet(seg_s);
                
                % feature mat
                out_mat = [[seg_s.energyVal]; ...
                           zeros(1,numel(seg_s)); ...
                           zeros(1,numel(seg_s)); ...
                           zeros(1,numel(seg_s)); ...
                           [seg_s.temp110]; ...
                           [seg_s.ciCallService]; ...
                           [seg_s.coopLevel1]; ...
                           [seg_s.outG]; ...
                           [seg_s.outS]; ...
                           [seg_s.outH];...
                           zeros(1,numel(seg_s));...
                           zeros(1,numel(seg_s));...
                           ];
                       
                f_vec = [1 2 3 4 5 6 7 8 9 10 11 12];
                
            case 'feat_struct'
                run_libs({'flow_io_lib_fh'});
                
                min_speech = 13;
                max_forced_speech = 20;
                
                [~, slices_s] = libs.flow_io_lib_fh.testOnSignal(signal, fs,...
                                struct( 'min_speech', min_speech,...
                                        'max_forced_speech', max_forced_speech,...
                                        'analyses_types', {{'na', ''}},...
                                        'test_type', {{'regular','last','~summary'}},...
                                        'stop_on_first_result', 1,...
                                        'meta_data', context,...
                                        'basic_features',{{'all','na'}}));
                
                out_mat = 0;
                f_vec = 1;
                t_vec = 0;
                if ~isempty(slices_s) && isstruct(slices_s) 
                    out_mat = slices_s(1);
                    f_vec = fieldnames(out_mat);
                    t_vec = 0;
                end 
                 
            case 'res_struct'
                run_libs({'flow_io_lib_fh'});
                
                min_speech = 13;
                max_forced_speech = 20;
                outputs = libs.flow_io_lib_fh.testOnSignal(signal, fs,...
                                struct( 'min_speech', min_speech,...
                                        'max_forced_speech', max_forced_speech,...
                                        'meta_data', context,...
                                        'test_type', {{'regular','last','~summary'}},...
                                        'analyses_types', {{...
                                                'temper_2d',...
                                                'valence_arousal_temper_gender',...
                                                'audio_quality'}},...
                                        'stop_on_first_result', 0));
                
                if isempty(outputs)
                    throw(MException('ANALYZER:SilentFile', 'Input file contains no data')); 
                end
                
                results = loadjson([outputs{:}]);
                if iscell(results)
                    results = merge_structs_to_array(results{:});
                end
                
                out_mat = [];
                f_vec = [];
                t_vec = [];
                if ~isempty(results) && isstruct(results) 
                    out_mat = results;
                    f_vec = fieldnames(out_mat);
                    t_vec = 1:numel(results);
                end
            
            case 'feat_struct_cache'
                f_name_orig = 'feat_struct';
                [out_mat, f_vec, t_vec] = ...
                        load_save_cached_data(signal, fs, f_name_orig, context);
                
            case 'res_struct_cache'
                f_name_orig = 'res_struct';
                [out_mat, f_vec, t_vec] = ...
                        load_save_cached_data(signal, fs, f_name_orig, context);
                    
            case 'anlzr_feat_cache'
                f_name_orig = 'anlzr_feat';
                [out_mat, f_vec, t_vec] = ...
                        load_save_cached_data(signal, fs, f_name_orig, context);
                
            case 'praat_struct'
                file_path = context;
                
                run_libs({'praat_feat_fh'});
                
                out_mat = feature_cache('Praat', libs.praat_feat_fh.praat_process_file, [], file_path);
                %out_mat = libs.praat_feat_fh.praat_process_file(file_path);
                f_vec = fieldnames(out_mat);
                t_vec = 0;
                
            case 'psysound_struct'
                run_libs({'psysound_feat_fh'});
                
                out_mat = libs.psysound_feat_fh.psysound_process_signal(signal, fs);
                f_vec = fieldnames(out_mat);
                t_vec = 0;
                
            case 'filename'
                out_mat = [];
                f_vec = {context};    
                t_vec = [];
                
            case 'raw_signal'
                out_mat = signal;
                f_vec = 0;
                t_vec = [];
        end
        
%         % outputs
%         if nargout == 1
%             varargout{1} = {out_mat, f_vec, t_vec};
%         else
%             varargout{1} = out_mat;
%             varargout{2} = f_vec;
%             varargout{3} = t_vec;
%         end

                function [out_mat, f_vec, t_vec] = ...
                        load_save_cached_data(signal, fs, func_name, context)
                    
                    cache_dir = regexprep(mfilename('fullpath'),...
                                'util functions\\feature_lib','mf_cache');
                    if ~exist(cache_dir, 'dir')
                        mkdir(cache_dir);
                    end
                    [~, name, ~] = fileparts(context);
                    cache_file_name = fullfile(cache_dir,...
                                        [name '_' func_name '_cache.mat']);
                    
                    try
                        load(cache_file_name,'out_mat','f_vec','t_vec');
                    catch ex
                        [out_mat, f_vec, t_vec] = ...
                                sig2mat_transform(signal, fs, func_name, context);
                        save(cache_file_name,'out_mat','f_vec','t_vec','-v6');
                    end
                end
    end

    % matrix to matrix tranformations
    function [out_mat, f_vec, t_vec] = mat2mat_transform(in_mat, f_vec, t_vec, f_name)
        
        % if the feature is part of a feature family
        if f_name(1) == '@'
            % Find separator
            separator_idx = find(f_name == '.');
            family_name = f_name(2:separator_idx-1);
            feature_name = f_name(separator_idx+1 : end);
            
            switch family_name
                case 'Praat'
                    run_libs({'praat_feat_fh'});
                    [out_mat, f_vec, t_vec] = libs.praat_feat_fh.process_praat_features(in_mat, f_vec, t_vec, feature_name);

                case 'Psysound'
                    run_libs({'psysound_feat_fh'});                    
                    [out_mat, f_vec, t_vec] = libs.psysound_feat_fh.process_psysound_features(in_mat, f_vec, t_vec, feature_name);

                case 'Harmonics'
                    run_libs({'harmonics_feat_fh'});                    
                    [out_mat, f_vec, t_vec] = libs.harmonics_feat_fh.process_harmonics_features(in_mat, f_vec, t_vec, feature_name);
                    
                case 'VoiceSauce'
                    run_libs({'vsauce_feat_fh'});
                    [out_mat, f_vec, t_vec] = libs.vsauce_feat_fh.process_vsauce_features(in_mat, feature_name);
                    
                otherwise
                    error('Unknown feature family');
            end
                
        else        
            % No family name. Process feature by its name
            
            run_libs({'harmonics_feat_fh'}); % Todo: Remove along with harmonics features
            libs.harmonics_feat_fh.run_libs();
            
            switch f_name

                %%%%%%%%%%%%%%%%%%%%%%%%
                %%%   production   %%%%%
                case 'none'
                    out_mat = in_mat;

                case 'pitch_mat'
                    if ~isstruct(in_mat) || ~iscell(f_vec) 
                        run_libs({'basic_feat_fh'});
                        out_mat = libs.basic_feat_fh.pitch_by_cepstrum_combined(in_mat, f_vec, t_vec);
                    else % feat_struct
                        [out_mat, f_vec, t_vec] = deal(in_mat.pitch_mat_256,...
                                            in_mat.ct_vec_256, in_mat.t_vec_80);
                    end

                case 'band_pass'
                    f1 = 80;
                    f2 = 1500;
                    ind_to_leave = find(f_vec>=f1 & f_vec<=f2);
                        
                    out_mat = in_mat(ind_to_leave,:);
                    f_vec = f_vec(ind_to_leave);

                case 'vocal_source'
                    if ~isstruct(in_mat) || ~iscell(f_vec) 
                        out_mat = 0;
                    else % feat_struct
                        [out_mat, f_vec, t_vec] = deal(in_mat.voc_src_256,...
                                            in_mat.f_vec_256, in_mat.t_vec_80);
                    end                

                case 'powspec'
                    if ~isstruct(in_mat) || ~iscell(f_vec) 
                        out_mat = in_mat;
                    else % feat_struct
                        [out_mat, f_vec, t_vec] = deal(in_mat.powspec_256, in_mat.f_vec_256, in_mat.t_vec_80);
                    end

                case 'rasta'
                    if ~isstruct(in_mat) || ~iscell(f_vec) 
                        run_libs({'basic_feat_fh'});
                        out_mat = libs.basic_feat_fh.mat_rasta(in_mat, f_vec, t_vec);
                    else % feat_struct
                        [out_mat, f_vec, t_vec] = deal(in_mat.rasta_mat, in_mat.f_vec_256, in_mat.t_vec_80);
                    end

                case 'vocal_filter'
                    if ~isstruct(in_mat) || ~iscell(f_vec) 
                        out_mat = 0;
                    else % feat_struct
                        [out_mat, f_vec, t_vec] = deal(in_mat.voc_filt_256,...
                                            in_mat.f_vec_256, in_mat.t_vec_80);
                    end 

                case 'digitize80'
                    out_mat = double(in_mat>quick_prctile(in_mat(:),80));  

                case 'add_noise'
                    noise_factor = sqrt(mean(in_mat(:).^2));
                    rng(1,'twister');
                    out_mat = in_mat + noise_factor.*rand(size(in_mat));
%                     rng('shuffle','twister');
                    rng(mod(floor(now*8640000).*labindex ,2^31-1));
    %                 figure; imagesc(log(in_mat + noise_factor.*rand(size(in_mat)))); axis xy;

                case 'anti_g711'
                    f1 = 300;
                    f2 = 3400;
                    ind_to_leave = find(f_vec>=f1 & f_vec<=f2);
                    out_mat = in_mat(ind_to_leave,:);
                    f_vec = f_vec(ind_to_leave);

                case 'rasta2cls'
                    run_libs({'class_lib_fh'});                
                    buff_length = 10;
                    overlap_ratio = 0.8;
                    c_stream = libs.class_lib_fh.classify_rasta_stream({in_mat, f_vec, t_vec},...
                                            buff_length, overlap_ratio,...
                                            {'high-med','low-med','energy-scale','vad'});
                    out_mat = [[c_stream.energy_scale]; ...
                               [c_stream.high_med_val]; ...
                               [c_stream.low_med_val]; ...
                               [c_stream.vad_ssnr_val]; ...
                               zeros(size([c_stream.energy_scale])); ...
                               zeros(size([c_stream.energy_scale])); ...
                               zeros(size([c_stream.energy_scale])); ...
                               zeros(size([c_stream.energy_scale])); ...
                               zeros(size([c_stream.energy_scale])); ...
                               zeros(size([c_stream.energy_scale])); ...
                               [c_stream.hm_norm]; ...
                               [c_stream.lm_norm]; ...
                               ];
                    f_vec = [1:12];
                    t_vec = ([c_stream.t_start]+[c_stream.t_end])./2;

                case 'rasta2cls_nooverlap'
                    run_libs({'class_lib_fh'});

                    if iscell(f_vec) && isstruct(in_mat)
                        [in_mat, f_vec, t_vec] = deal(in_mat.rasta_mat, in_mat.f_vec_256, in_mat.t_vec_80);
                    end

                    c_stream = libs.class_lib_fh.rasta_energy_classifiers({in_mat, f_vec, t_vec},...
                                            {'high-med','low-med','energy-scale','vad'});
                    out_mat = [[c_stream.energy_scale]; ...
                               [c_stream.high_med_val]; ...
                               [c_stream.low_med_val]; ...
                               [c_stream.vad_ssnr_val]; ...
                               zeros(size([c_stream.energy_scale])); ...
                               zeros(size([c_stream.energy_scale])); ...
                               zeros(size([c_stream.energy_scale])); ...
                               zeros(size([c_stream.energy_scale])); ...
                               zeros(size([c_stream.energy_scale])); ...
                               zeros(size([c_stream.energy_scale])); ...
                               [c_stream.hm_norm]; ...
                               [c_stream.lm_norm]; ...
                               ];
                    f_vec = [1:12];
                    t_vec = 0;

                case 'Timbre_3-6_20%_1000_2000'
                    [out_mat, f_vec, t_vec]    = libs.harmonics_feat_fh.GetHarmonicsMat(in_mat, 3:6,  0.20, 1000, 2000);
                case 'Timbre_3-13_0%_1000_4000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetHarmonicsMat(in_mat, 3:13, 0, 1000, 4000);    
                case 'Timbre_7-12_20%_1000_4000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetHarmonicsMat(in_mat, 7:12, 0.20, 1000, 4000);    
                case 'Distortion_1-6_0%_1000_4000'
                    [out_mat, f_vec, t_vec]    = libs.harmonics_feat_fh.GetDistortion(in_mat, 1:6,  0, 1000, 4000);
                case 'Distortion_5-10_12%_1000_4000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetDistortion(in_mat, 5:10, 0.12, 1000, 4000);
                case 'Distortion_5-10_20%_1000_2000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetDistortion(in_mat, 5:10, 0.20, 1000, 2000);
                case 'Distortion_1-10_12%_1000_2000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetDistortion(in_mat, 1:10, 0.12, 1000, 2000);
                case 'Distortion_1-6_20%_1000_2000'
                    [out_mat, f_vec, t_vec]    = libs.harmonics_feat_fh.GetDistortion(in_mat, 1:6,  0.20, 1000, 2000);    
                case 'Distortion_5-10_8%_1000_4000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetDistortion(in_mat, 5:10, 0.08, 1000, 4000);
                case 'Distortion_5-15_8%_1000_2000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetDistortion(in_mat, 5:15, 0.08, 1000, 2000);    
                case 'Distortion_7-12_8%_0_4000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetDistortion(in_mat, 7:12, 0.08, 0, 4000);
                case 'Distortion_1-10_4%_1000_4000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetDistortion(in_mat, 1:10, 0.04, 1000, 4000);    
                case 'Distortion_7-10_20%_1000_2000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetDistortion(in_mat, 7:10, 0.20, 1000, 2000);    
                case 'Distortion_5-8_8%_0_4000'
                    [out_mat, f_vec, t_vec]    = libs.harmonics_feat_fh.GetDistortion(in_mat, 5:8,  0.08, 0, 4000);    
                case 'Distortion_5-10_4%_0_4000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetDistortion(in_mat, 5:10, 0.04, 0, 4000);
                case 'Distortion_1-6_0%_0_4000'
                    [out_mat, f_vec, t_vec]    = libs.harmonics_feat_fh.GetDistortion(in_mat, 1:6,  0, 0, 4000);
                case 'Timbre_1-10_0%_1000_4000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetHarmonicsMat(in_mat, 1:10, 0, 1000, 4000);
                case 'Timbre_1-4_0%_1000_4000'
                    [out_mat, f_vec, t_vec]    = libs.harmonics_feat_fh.GetHarmonicsMat(in_mat, 1:4,  0, 1000, 4000);
                case 'Distortion_1-6_12%_1000_4000'
                    [out_mat, f_vec, t_vec]    = libs.harmonics_feat_fh.GetDistortion(in_mat, 1:6,  0.12, 1000, 4000);
                case 'Distortion_5-10_4%_1000_4000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetDistortion(in_mat, 5:10, 0.04, 1000, 4000);
                case 'Distortion_5-10_12%_1000_2000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetDistortion(in_mat, 5:10, 0.12, 1000, 2000);
                case 'Distortion_7-12_4%_1000_2000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetDistortion(in_mat, 7:12, 0.04, 1000, 2000);    
                case 'Timbre_7-10_0%_1000_4000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetHarmonicsMat(in_mat, 7:10, 0, 1000, 4000);
                case 'Timbre_5-15_20%_1000_2000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetHarmonicsMat(in_mat, 5:15, 0.20, 1000, 2000);
                case 'Distortion_5-10_20%_0_1000'
                    [out_mat, f_vec, t_vec]   = libs.harmonics_feat_fh.GetDistortion(in_mat, 5:10, 0.20, 0, 1000);
                %%%%%%%%%%%%%%%%%%%%%%%%
                %%% non-production %%%%%

                % dimensionless
                case 'log_abs'
                    out_mat = log(abs(in_mat) + eps);
                case 'norm'
                    out_mat = in_mat./max(in_mat(:)+eps);
                case 'exp'
                    out_mat = exp(in_mat);
                case 'square'
                    out_mat = in_mat.^2;
                case 'sqrt'
                    out_mat = in_mat.^0.5;

                case 'rasta_vad'
                    run_libs({'basic_feat_fh','class_lib_fh'});                
                    out_mat = libs.basic_feat_fh.mat_rasta(in_mat, f_vec, t_vec);
                    vad_vec = libs.class_lib_fh.vad_rasta_mat2vec(out_mat,f_vec, t_vec);

                    if any(vad_vec==0)
                        out_mat(:,vad_vec==0) = [];
                        t_vec((nnz(vad_vec==1)+1):end) = [];
                    end

                case 'fft_cepstrum'
                    run_libs({'basic_feat_fh'});
                    [out_mat,  f_vec, t_vec] =...
                            libs.basic_feat_fh.fft_cepstrum(in_mat, f_vec, t_vec);

                case 'diff'
                    out_mat = diff(in_mat,1, 2);
                    out_mat = out_mat(:, [1 1:end]);

                case 'clear_bottom_80'
                    out_mat = in_mat;
                    t_val = quick_prctile(in_mat(:),80);
                    out_mat(in_mat<t_val) = 0;

                case 'digitize95'
                    out_mat = double(in_mat>quick_prctile(in_mat(:),95));

                case 'digitize_95_80'
                    out_mat = double(in_mat>quick_prctile(in_mat(:),80))...
                            + 5*double(in_mat>quick_prctile(in_mat(:),95));

                case 'edge_detection'
    %                 gauss
                    diff2_mat = diff(in_mat,2,1);
                    diff2_mat = diff2_mat([1 1 1:end], :);
                    out_mat = abs(diff(sign(diff2_mat),1,1));  
                    out_mat = out_mat([1 1:end], :);

                case 'g711_bandpass'
                    f1 = 300;
                    f2 = 3400;
                    out_mat = in_mat;
                    out_mat((f_vec<=f1 | f_vec>=f2),:) = 0;

                case 'dissonance_s'
                    if isstruct(in_mat) && iscell(f_vec) 
                        out_mat = in_mat.dissonance_s;
                        f_vec = 1;
                        t_vec = in_mat.t_vec_80;
                    else
                        out_mat = [];
                    end

                case 'dissonance_hk'
                    if isstruct(in_mat) && iscell(f_vec)
                        out_mat = in_mat.dissonance_hk;
                        f_vec = 1;
                        t_vec = in_mat.t_vec_80;
                    else
                        out_mat = [];
                    end
                    
                case '12thOctFFT'
                    if isstruct(in_mat) && iscell(f_vec)
                        out_mat = in_mat.powspec_12thoct;
                        f_vec = in_mat.f_vec_12thoct;
                        t_vec = in_mat.t_vec_512;
                    else
                        out_mat = [];
                    end

                case 'diss_tonal_map'
                    if isstruct(in_mat) && iscell(f_vec)
                        out_mat = in_mat.diss_tonal_map;
                        f_vec = in_mat.f_vec_diss_tonal_map;
                        t_vec = in_mat.t_vec_80;
                    else
                        out_mat = [];
                    end                    
                    
                otherwise
                    out_mat = in_mat;
                    disp(['@mat2mat: function not recognized or not supported: ' f_name]);
            end % switch
            
        end % if fname(1) == '@'
        
        if ~isstruct(out_mat) 
            
            if ~isfinite(sum(sum(out_mat,1))) % faster 
                out_mat(~isfinite(out_mat)) = 0;
            end
            
            if ~isreal(out_mat)
                out_mat = abs(out_mat);
            end
        end
        
        if isempty(out_mat)
            out_mat = 0;
        end
    end

    % serial mat2mat transformation
    function [out_mat, f_vec, t_vec] = mat2mat_serial(in_mat, f_vec, t_vec, f_names_cell)
       for i=1:length(f_names_cell) 
          [in_mat, f_vec, t_vec] = ...
                mat2mat_transform(in_mat, f_vec, t_vec, f_names_cell{i});
       end
       out_mat = in_mat;
    end

    % matrix to vector transformations
    function [out_vec, coord_vec]= mat2vec_reduce(in_mat, f_vec, t_vec, f_name, dim)
        
        if dim == 1
            coord_vec = t_vec;
        elseif dim==2
            coord_vec = f_vec;
        else
            coord_vec = 1; out_vec = 0;
            disp('@mat2vec_tranform: dimension error'); return;
        end
        switch f_name
            %%%%%%%%%%%%%%%%%%%%%%%%
            %%%   production   %%%%%
            case '80/20pctl'
                pctl_mat = quick_prctile(in_mat, [80 20], dim);
                if dim==1
                    out_vec = pctl_mat(1,:)./(pctl_mat(2,:)+eps);
                else
                    out_vec = pctl_mat(:,1)./(pctl_mat(:,2)+eps);                    
                end               
                
            case 'std'                
                out_vec = std(in_mat, 0, dim);
                
            case 'median'
                out_vec = quick_prctile(in_mat, 50, dim);
            case 'mean'
                out_vec = mean(in_mat, dim);
            case '80pctl'
                out_vec = quick_prctile(in_mat, 80, dim);            
            case 'std/mean'                
                try
                    out_vec = std(in_mat, 0, dim)./(mean(in_mat, dim)+eps);
                catch ex
                    breakpoint();
                end
            case 'sum'
                out_vec = sum(in_mat, dim);
            case 'mode' 
                out_vec = quicker_mode(in_mat, dim);
            case 'skewness'
                out_vec = quicker_skewness(in_mat,1,dim);
            case 'kurtosis'
                out_vec = quicker_kurtosis(in_mat,1,dim);
                % YS
%                 out_vec = kurtosis(in_mat,1,dim);
                
            case 'tot_entropy'
                in_mat = in_mat - repmat(min(in_mat,[],1), size(in_mat,1), 1); %no negative values
                norm_vec = sum(in_mat,dim)+eps;
                if dim == 1 % normalize
                    in_mat = in_mat./repmat(norm_vec,[size(in_mat,1) 1]); 
                else 
                    in_mat = in_mat./repmat(norm_vec,[1 size(in_mat,2)]);
                    in_mat = in_mat.';                    
                end 
                out_vec = -sum(-in_mat.*log(in_mat+eps),1);                
            
            case 'centroid'
                in_mat = in_mat - repmat(min(in_mat,[],1), size(in_mat,1), 1); %no negative values
                if dim == 1
                    weight_mat = repmat(f_vec(:),[1 size(in_mat,2)]); %transposed
                    weighted_in_mat = in_mat.*weight_mat;
                    out_vec = sum(weighted_in_mat,dim) ./ (sum(in_mat,dim) + eps);
                else 
                    coord_vec = 1; out_vec = 0;
                end
                
            case 'lm_norm'
                if size(in_mat,1) >= 12
                    out_vec = in_mat(12,:);
                    coord_vec = t_vec; 
                else 
                    out_vec = 0;
                    coord_vec = 0;
                end
                    
            case 'hm_norm'
                if size(in_mat,1) >= 11
                    out_vec = in_mat(11,:);
                    coord_vec = t_vec; 
                else 
                    out_vec = 0;
                    coord_vec = 0;
                end
                
            case 'high-med'
                if size(in_mat,1) >= 2
                    out_vec = in_mat(2,:);
                    coord_vec = t_vec; 
                else 
                    out_vec = 0;
                    coord_vec = 0;
                end
                
            case '80pctl freq'
                if dim == 1
                    run_libs({'stats_fh'});
                    out_vec = libs.stats_fh.partial_sum_percentile_location_on_mat(...
                                                    in_mat, 80, 1);                  
                else
                    coord_vec = 1; out_vec = 0;
                end
                
            case '80-20 pctl freq ratio'
                if dim == 1
                    run_libs({'stats_fh'});
                    out_vec = libs.stats_fh.partial_sum_percentile_location_ratio_on_mat(...
                                                    in_mat, 80, 20, 1); 
                else
                    coord_vec = 1; out_vec = 0;
                end
            
            case 'pitch_f'                
                if ~isstruct(in_mat) || ~iscell(f_vec) 
                    run_libs({'basic_feat_fh'});
                    [out_vec, ~, coord_vec] = libs.basic_feat_fh.pitch_contour_on_pitch_mat(...
                                                        in_mat, f_vec, t_vec);
                else % feat_struct
                    [out_vec, ~, coord_vec] = deal(in_mat.pitch_f, in_mat.pitch_a, in_mat.pitch_t_vec);
                end
            
            case 'pitch_a'
                if ~isstruct(in_mat) || ~iscell(f_vec) 
                    run_libs({'basic_feat_fh'});
                    [~, out_vec, coord_vec] = libs.basic_feat_fh.pitch_contour_on_pitch_mat(...
                                                        in_mat, f_vec, t_vec);
                else % feat_struct
                    [~, out_vec, coord_vec] = deal(in_mat.pitch_f, in_mat.pitch_a, in_mat.pitch_t_vec);
                end 
                
            case 'vad_vec'
                if ~isstruct(in_mat) || ~iscell(f_vec)
                    run_libs({'class_lib_fh'});
                    out_vec = libs.class_lib_fh.raw_vad_vec_on_rastacell(...
                                                        {in_mat, f_vec, t_vec});
                else % feat_struct
                    [out_vec, coord_vec] = deal(in_mat.raw_vad_vec, in_mat.t_vec_80);
                end 
            
            case 'bp_ste_vec'
                if ~isstruct(in_mat) || ~iscell(f_vec) 
                    run_libs({'class_lib_fh'});
                    out_vec = libs.class_lib_fh.bp_ste_on_powspeccell(...
                                                        {in_mat, f_vec, t_vec});
                else % feat_struct
                    [out_vec, coord_vec] = deal(in_mat.bp_ste_vec, in_mat.t_vec_80);
                end
                              
            case 'band_sum_ratio' %only dim=1
                band_1_cut_off = 1000;
                band_2_cut_off = 2000;
%                 dim=1;
                [~, b1_ind] = min(abs(f_vec - band_1_cut_off));
                [~, b2_ind] = min(abs(f_vec - band_2_cut_off));

                in_mat = in_mat - min([min(in_mat(:)) 0]); %no negative values
                cs_mat = cumsum(in_mat, 1);
                
                out_vec = (cs_mat(b2_ind,:) - cs_mat(b1_ind,:)) ./(cs_mat(b1_ind,:) + eps);
                coord_vec = t_vec;
                
            case 'subband_correlation' % both dimensions
                
                num_bands = 15;
                f_start = 100;
                f_stop = 3000;
                f_start_ind = max([find(f_vec>=f_start, 1, 'first') 1]);
                f_stop_ind = min([find(f_vec>f_stop, 1, 'first') length(f_vec)]);
                band_ind_vec = round(logspace(log10(f_start_ind), log10(f_stop_ind), num_bands + 1));
                cs_mat = cumsum(in_mat, 1);
                % split to sub bands
                band_mat = cs_mat(band_ind_vec(2:end), :) - ...
                           cs_mat(band_ind_vec(1:(end-1)), :);
                % correlate
                corr_vec = zeros(1,size(in_mat,2));
                if dim == 1
                    for i_1 = 1:num_bands
                        for i_2 = 1:num_bands
                            if i_2 > i_1
                                corr_vec = corr_vec + band_mat(i_1, :).*band_mat(i_2, :);
                            end
                        end
                    end
                    % normalize
                    out_vec = corr_vec./(num_bands*(num_bands-1)/2);
                    coord_vec = t_vec;
                else
                    if numel(f_vec ~= size(band_mat, 2))
                        out_vec = 1;
                        coord_vec = 0;
                    else
                        max_dt = 16;
                        t_len = size(band_mat, 2);
                        sum_prod_vec = zeros(1,t_len);
                        for i_band = 1:num_bands
                            prod_vec = ones(1,(max_dt + t_len - 1));
                            band_vec = band_mat(i_band, :);
                            for dt = 1:max_dt
                                prod_vec(dt:(t_len+dt-1)) = prod_vec(dt:(t_len+dt-1)).*...
                                                          band_vec;

                            end
                            prod_win = conv(prod_vec, hann(max_dt), 'valid');
                            sum_prod_vec = sum_prod_vec + prod_win;
                        end
                        % normalize
                        out_vec = sum_prod_vec./(max_dt^2);
                        coord_vec = f_vec;
                    end
                end
                
            %%%%%%%%%%%%%%%%%%%%%%%%
            %%% non-production %%%%%
            case 'tilt'
                
                in_mat = in_mat - repmat(min(in_mat,[],1), size(in_mat,1), 1); %no negative values
                log_mat = log(abs(in_mat) + eps);
                if dim == 1    
                    if size(in_mat,1)>=2 && ~any(diff(f_vec)<=eps)
                        for row_i=1:size(log_mat,2)
                            p = polyfit(f_vec(:), log_mat(:,row_i), 1);
                            out_vec(row_i) = p(1);
                        end
                    else out_vec = 0; end
                else
                    if size(in_mat,2)>=2 && ~any(diff(f_vec)<=eps)
                        for col_i=1:size(log_mat,1)
                            p = polyfit(f_vec(:), log_mat(col_i,:), 1);
                            out_vec(col_i) = p(1);
                        end
                    else out_vec = 0; end
                end
                     
            case 'specific_loudness'
                    
            case 'composure'
                run_libs({'class_lib_fh'});
                
                [out_vec, ~, ~] = libs.class_lib_fh.classify_composure({in_mat, f_vec, t_vec});
                coord_vec = 1;
                
            case 'temper'
                run_libs({'class_lib_fh'});
                
                [out_vec, ~, ~] = libs.class_lib_fh.classify_temper({in_mat, f_vec, t_vec});
                coord_vec = 1;
                
            case 'energy_scale'
                out_vec = in_mat(1,:);
                coord_vec = t_vec;
            
            case 'low-med'
                out_vec = in_mat(3,:);
                coord_vec = t_vec;
                
            case 'anlzr_outG'
                out_vec = in_mat(8,:);
                coord_vec = t_vec;
            case 'anlzr_outS'
                out_vec = in_mat(9,:); 
                coord_vec = t_vec;  
            case 'anlzr_outH'
                out_vec = in_mat(10,:); 
                coord_vec = t_vec;   
                
            case 'randvec'
                out_vec = rand(size(coord_vec));
                
            case 'first_el'
                out_vec = in_mat(1);
                coord_vec = f_vec;
                
            case 'rs_valence_val'
                out_vec = [in_mat.valence_val];
                coord_vec = 1:numel(out_vec);  
%                 coord_vec = [in_mat.start];
            case 'rs_arousal_val'
                out_vec = [in_mat.arousal_val];
                coord_vec = 1:numel(out_vec); 
%                 coord_vec = [in_mat.start];
            case 'rs_temper_val'
                out_vec = [in_mat.temper_val];
                coord_vec = 1:numel(out_vec);    
%                 coord_vec = [in_mat.start];
            case 'rs_gender_val'
                out_vec = [in_mat.gender_val];
                coord_vec = 1:numel(out_vec);
%                 coord_vec = [in_mat.start];
            case 'rs_audio_quality_val'
                out_vec = [in_mat.audio_quality_val];
                coord_vec = 1:numel(out_vec);    
%                 coord_vec = [in_mat.start];

            case 'rs_trend_overall_val'
                run_libs({'flow_lib_fh'});
                res_s = libs.flow_lib_fh.calc_call_trend_on_results_struct(in_mat,...
                        struct('summary_audio_quality_policy', {'good','marginal_better'}));
                out_vec = res_s(end).trend_overall_val;
                coord_vec = 1:numel(out_vec);    
            case 'rs_trend_improvement_val' 
                run_libs({'flow_lib_fh'});
                res_s = libs.flow_lib_fh.calc_call_trend_on_results_struct(in_mat,...
                        struct('summary_audio_quality_policy', {'good','marginal_better'}));
                out_vec = res_s(end).trend_improvement_val;
                coord_vec = 1:numel(out_vec);   
                
            case 'labeled_valence'
                run_libs({'edb_fh'});
                if iscell(coord_vec) && ~isempty(coord_vec)
                    file_name = coord_vec{1};     
                    words_str = file_name((strfind(file_name,'[')+1):(strfind(file_name,']')-1));
                    [vocab_indices, ~, ~, ~] = libs.edb_fh.words_to_vocab_indices(words_str);
                    out_vec = libs.edb_fh.vocab_ind_to_valence(vocab_indices);
                end
            case 'labeled_moodgroup21'
                run_libs({'edb_fh'});
                if iscell(coord_vec) && ~isempty(coord_vec)
                    file_name = coord_vec{1};     
                    words_str = file_name((strfind(file_name,'[')+1):(strfind(file_name,']')-1));
                    [vocab_indices, ~, ~, ~] = libs.edb_fh.words_to_vocab_indices(words_str);
                    out_vec = libs.edb_fh.vocab_ind_to_moodgroup21(vocab_indices);
                end
            
            case 'labeled_moodgroup11'
                run_libs({'edb_fh'});
                if iscell(coord_vec) && ~isempty(coord_vec)
                    file_name = coord_vec{1};     
                    words_str = file_name((strfind(file_name,'[')+1):(strfind(file_name,']')-1));
                    [vocab_indices, ~, ~, ~] = libs.edb_fh.words_to_vocab_indices(words_str);
                    out_vec = libs.edb_fh.vocab_ind_to_moodgroup11(vocab_indices);
                end
                
            otherwise
                out_vec = 0;
                coord_vec = 1;
                disp(['@mat2vec: function not recognized or not supported: ' f_name]);
        end
        
        if isempty(out_vec)
            out_vec = 0;
        end
        
        if isnumeric(out_vec)
            out_vec(isnan(out_vec) | isinf(out_vec)) = 0;
        end
    end
    
    % vector to vector transformations
    function [out_vec, coord_vec] = vec2vec_transform(in_vec, coord_vec, f_name)
        coord_vec = coord_vec(:)';
        in_vec = in_vec(:)';
        switch f_name 
            case 'none'
                out_vec = in_vec;
            case 'log_abs'
                out_vec = log(abs(in_vec) + eps);
            case 'norm'
                out_vec = in_vec./(max(in_vec) + eps);
            case 'exp'
                out_vec = exp(in_vec);
            case 'square'
                out_vec = in_vec.^2;
            case 'sign_square'
                out_vec = in_vec.*abs(in_vec);            
                
            case 'diff'
                out_vec = [0; diff(in_vec(:))];
                
            case 'absdiff'
                out_vec = abs([0; diff(in_vec(:))]);                
            
            case 'clear_bottom_20'
                out_vec = in_vec;
                out_vec(in_vec<quick_prctile(in_vec,20)) = 0;
                
            case 'clear_bottom_80'
                out_vec = in_vec;
                out_vec(in_vec<quick_prctile(in_vec,80)) = 0;
                
            case 'rem_outliers'
                run_libs({'stats_fh'});
                [out_vec, ~] = libs.stats_fh.replace_outliers(in_vec, '' ,'trim');    
                
            case 'rasta'
                run_libs({'basic_feat_fh'});
                if numel(coord_vec)>200 && ((coord_vec(end)-coord_vec(1))<1000)
                    in_vec = in_vec - min([min(in_vec) 0]);
                    out_vec = ...
                            libs.basic_feat_fh.mat_rasta(in_vec(:)', 1, coord_vec);
                else
                    out_vec = in_vec;
                end
                
            case 'digitize80'
                out_vec = double(in_vec>quick_prctile(in_vec,80));
                    
            case 'edge_detection'
                if numel(in_vec)>3
                    diff2_vec = diff(in_vec,2);
                    diff2_vec = diff2_vec([1 1 1:end]);
                    out_vec = abs(diff(sign(diff2_vec),1));  
                    out_vec = out_vec([1 1:end]);
                else
                    out_vec = 0;
                end
                
            case 'hist_vec_20'
                n_hist = 20;
                if numel(in_vec)>n_hist
                    pctl_vec = quick_prctile(in_vec, [10 90]);
                    edges = linspace(pctl_vec(1), pctl_vec(2)+eps, n_hist);
                    out_vec = histc(in_vec, edges);
                    coord_vec = edges;
                else
                    out_vec = 0;
                    coord_vec = 0;
                end
                
            case 'lpf_ma_10'
                run_libs({'stats_fh'});
                out_vec = libs.stats_fh.vector_smoothing(in_vec, 10/2, 'mean');

            case 'lpf_ma_100'
                run_libs({'stats_fh'});
                out_vec = libs.stats_fh.vector_smoothing(in_vec, 100/2, 'mean');

            case 'lpf_ma_1000'
                run_libs({'stats_fh'});
                out_vec = libs.stats_fh.vector_smoothing(in_vec, 1000/2, 'mean');
                
            case 'hpf_ma_10'
                run_libs({'stats_fh'});
                out_vec = in_vec - libs.stats_fh.vector_smoothing(in_vec, 10/2, 'mean');

            case 'hpf_ma_100'
                run_libs({'stats_fh'});
                out_vec = in_vec - libs.stats_fh.vector_smoothing(in_vec, 100/2, 'mean');

            case 'hpf_ma_1000'
                run_libs({'stats_fh'});
                out_vec = in_vec - libs.stats_fh.vector_smoothing(in_vec, 1000/2, 'mean');
                
            case 'temp_analysis'
                run_libs({'stats_fh'});
                
                % diff
                in_vec = [0 diff(in_vec)]./[in_vec];
                % hist
                n_hist = 30;
                if numel(in_vec)>n_hist
                    pctl_vec = quick_prctile(in_vec, [5 95]);
                    pctl_vec(1) = max(pctl_vec(1), -0.03);
                    pctl_vec(2) = min(pctl_vec(2), 0.03);
                    edges = linspace(pctl_vec(1), pctl_vec(2)+eps, n_hist);
                    out_vec = histc(in_vec, edges);
                    coord_vec = edges;
                else
                    out_vec = 0;
                    coord_vec = 0;
                end
                % log
%                 out_vec = log10(out_vec + 1);
                
            otherwise
                out_vec = in_vec;
                disp(['@vec2vec: function not recognized or not supported:' f_name]);
        end
        
        
        if isempty(out_vec)
            out_vec = 0;
        end
        
        if isnumeric(out_vec)
            out_vec(isnan(out_vec) | isinf(out_vec)) = 0;
        end
    end

    % serial vec2vec transformation
    function [out_vec, coord_vec] = vec2vec_serial(in_vec, coord_vec, f_names_cell)
       for i=1:length(f_names_cell) 
          [in_vec, coord_vec] = vec2vec_transform(in_vec, coord_vec, f_names_cell{i});
       end
       out_vec = in_vec;
    end

    function toneset = get_toneset(in_vec, set)
        assert(isstruct(in_vec));
        switch set
            case 1
                toneset = {in_vec.t1, in_vec.t2, in_vec.temper_group};
            case 2 
                toneset = {in_vec.t3, in_vec.t4, in_vec.temper_group};
        end
    end

    % vector to value transformations
    function out_val = vec2val_reduce(in_vec, coord_vec, f_name)
                
        in_vec = in_vec(:)';
        coord_vec = coord_vec(:)';
        switch f_name 
            case 'median'
                out_val = median(in_vec);
            case 'mean'
                out_val = mean(in_vec);
            case '80pctl'
                out_val = quick_prctile(in_vec, [80]);
            case '20pctl'
                out_val = quick_prctile(in_vec, [20]);
            case '80/20pctl'
                out_val = quick_prctile(in_vec, 80)./(quick_prctile(in_vec, 20)+eps);
            case '80-20pctl'
                out_val = quick_prctile(in_vec, 80) - quick_prctile(in_vec, 20);
            case '80-20+mean'
                out_val = ((quick_prctile(in_vec, 80) - quick_prctile(in_vec, 20))).^2 + (mean(in_vec)).^2;
            case 'std'                
                out_val = std(in_vec, 0);
            case 'std/mean'                
                out_val = std(in_vec, 0)./(mean(in_vec)+eps);
            case 'sum'
                out_val = sum(in_vec);
            case 'mode' 
                out_val = quicker_mode(in_vec);
            case 'skewness'
                out_val = quicker_skewness(in_vec,1);
            case 'kurtosis'
                %YS
%                 out_val = kurtosis(in_vec,1);   
                out_val = quicker_kurtosis(in_vec,1);   
            case 'rs_gender_val'
                out_val = in_vec.gender_val;
            case 'rs_arousal_val'
                out_val = in_vec.arousal_val;
            case 'rs_valence_val'
                out_val = in_vec.valence_val;
            case 'rs_temper_val'
                out_val = in_vec.temper_val;
            case 'rs_audio_quality_val'
                out_val = in_vec.audio_quality_val;
            case 'rs_valence1'
                run_libs({'edb_fh'});
                toneset = get_toneset(in_vec, 1);
                out_val = libs.edb_fh.get_toneset_valence(toneset);                
            case 'rs_valence2'
                run_libs({'edb_fh'});
                toneset = get_toneset(in_vec, 2);
                out_val = libs.edb_fh.get_toneset_valence(toneset);
            case 'rs_moodgroup11_1'
                run_libs({'edb_fh'});
                toneset = get_toneset(in_vec, 1);
                out_val = find(libs.edb_fh.get_toneset_moodgroup11(toneset));
            case 'rs_moodgroup11_2'
                run_libs({'edb_fh'});
                toneset = get_toneset(in_vec, 2);
                out_val = find(libs.edb_fh.get_toneset_moodgroup11(toneset));
            case 'rs_moodgroup21_1'
                run_libs({'edb_fh'});
                toneset = get_toneset(in_vec, 1);
                out_val = find(libs.edb_fh.get_toneset_moodgroup21(toneset));
            case 'rs_moodgroup21_2'
                run_libs({'edb_fh'});
                toneset = get_toneset(in_vec, 2);
                out_val = find(libs.edb_fh.get_toneset_moodgroup21(toneset));
            case 'rs_temper_2d'
                out_val = in_vec.temper_2d;
            case 'rs_energy'
                out_val = in_vec.energy_scale;
            case 'rs_energyVal'
                out_val = in_vec.energyVal;
            case 'rs_energyEnum'
                out_val = in_vec.energyEnum;
            case 'rs_highMed'
                out_val = in_vec.highMed;
            case 'rs_lowMed'
                out_val = in_vec.lowMed;
            case 'rs_flow_1'
                out_val = in_vec.flow_1;
            case 'rs_flow_2'
                out_val = in_vec.flow_2;
                
            case '80pctl relfreq'
                run_libs({'stats_fh'});
                out_val = libs.stats_fh.partial_sum_percentile_location_on_vec(in_vec, 80);
                                
            case '20pctl relfreq'
                run_libs({'stats_fh'});
                out_val = libs.stats_fh.partial_sum_percentile_location_on_vec(in_vec, 20);
                                
            case '50pctl relfreq'
                run_libs({'stats_fh'});
                out_val = libs.stats_fh.partial_sum_percentile_location_on_vec(in_vec, 50);
                
            case '80-20 pctl freq ratio'
                run_libs({'stats_fh'});
                out_val = libs.stats_fh.partial_sum_percentile_location_ratio_on_vec(in_vec, 80, 20);
                
            case 'rel_tilt'
                in_vec = in_vec - min([min(in_vec(:)) 0]); %no negative values
                if length(in_vec)>=2 && ~any(diff(coord_vec)<=eps)
                    p = polyfit(coord_vec(:), in_vec(:), 1);
                    out_val = p(1)./coord_vec(end);
                else
                    out_val = 0;
                end
                
            case 'log_rel_tilt'
                in_vec = in_vec - min([min(in_vec(:)) 0]); %no negative values
                log_vec = log(abs(in_vec) + eps);
                if length(in_vec)>=2 && ~any(diff(coord_vec)<=eps)
                    p = polyfit(coord_vec(:), log_vec(:), 1);
                    out_val = p(1)./coord_vec(end); 
                else
                    out_val = 0;
                end
                
            case 'tilt_start'
                if numel(in_vec)>=3
                    out_val = mean(in_vec(3:end)) - mean(in_vec(1:2));
                else
                    out_val = 0;
                end
                
            case 'tilt_diff'
                in_vec = in_vec - min([min(in_vec(:))]); %no negative values
                if length(in_vec)>=2 && ~any(diff(coord_vec)<=eps)
                    p = polyfit(coord_vec(:), in_vec(:), 1);
                    out_val = diff(polyval(p, coord_vec([1 end])));
                else
                    out_val = 0;
                end
                
            case 'centroid'
                in_vec = in_vec - min([min(in_vec(:)) 0]); %no negative values
                weighted_in_vec = in_vec(:).*coord_vec(:);
                out_val = sum(weighted_in_vec) ./ (sum(in_vec) + eps);
                
            case 'tot_entropy'
                in_vec = in_vec - min([min(in_vec(:)) 0]); %no negative values
                norm_val = sum(in_vec);
                s_vec = in_vec./(norm_val + eps);
                out_val = -sum(-1*s_vec.*log(s_vec+eps));
%                 out_val = entropy_vec - min(entropy_vec);
            
            case 'avg_entropy'
                in_vec = in_vec - min([min(in_vec(:)) 0]); %no negative values
                norm_val = sum(in_vec);
                s_vec = in_vec./(norm_val + eps);
                out_val = -sum(-1*s_vec.*log(s_vec+eps))./numel(s_vec);

            case 'band_sum_ratio'
%                 if (coord_vec(end)-coord_vec(1))>1000
%                         
%                     band_1_cut_off = 1000;
%                     band_2_cut_off = 2000;
%     %                 dim=1;
%                     [~, b1_ind] = min(abs(coord_vec - band_1_cut_off));
%                     [~, b2_ind] = min(abs(coord_vec - band_2_cut_off));
% 
%                     in_vec = in_vec - min([min(in_vec) 0]); %no negative values
%                     cs_vec = cumsum(in_vec);
% 
%                     out_val = (cs_vec(b2_ind) - cs_vec(b1_ind))./(cs_vec(b1_ind)+eps); 
%                 else
                if numel(coord_vec)>=2
                    mid_ind_floor = max([1, floor(length(in_vec)/2)]);
                    mid_ind_ceil = ceil(length(in_vec)/2);
                    cs_vec = cumsum(in_vec);
                    out_val = (cs_vec(end)-cs_vec(mid_ind_ceil))./cs_vec(mid_ind_floor); 
                else
                    out_val = 0;
                end
                
            case 'band_sum_diff'
                if numel(coord_vec)>=2
                    mid_ind_floor = max([1, floor(length(in_vec)/2)]);
                    mid_ind_ceil = ceil(length(in_vec)/2);
                    cs_vec = cumsum(in_vec);
                    out_val = (cs_vec(end) - (cs_vec(mid_ind_floor) + cs_vec(mid_ind_ceil)))./...
                                                                        numel(coord_vec); 
                else
                    out_val = 0;
                end
                
            case 'numel_over_threshold'
                threshold = 3.5;
                out_val = nnz(in_vec>threshold);
            
            case 'temper_enum'
                temper_t = [30 75];
                out_val = nnz(mean(in_vec)<temper_t);
                
            case 'last 0.2 freq'
                in_vec = in_vec - min([min(in_vec(:)) 0]);
                in_vec = in_vec./max(in_vec+eps);
                threshold = 0.2;
                out_val = max(coord_vec(in_vec >= threshold));
            
            case 'temp_analysis'
                
                mid_ind = find((coord_vec<=0.005) & (coord_vec>=-0.005));
                pos_ind = find(coord_vec>0.005);
                neg_ind = find(coord_vec<-0.005);
                weighted_in_vec = in_vec(:).*coord_vec(:);
                
                if ~isempty(mid_ind)
                    mid_weight = sum(in_vec(mid_ind));
                    mid_center = sum(weighted_in_vec(mid_ind)) ./...
                                            (sum(in_vec(mid_ind)) + eps);
                else
                    mid_weight = 0;
                    mid_center = 0;
                end
                
                if ~isempty(pos_ind)
                    pos_weight = sum(in_vec(pos_ind));
                    pos_center = sum(weighted_in_vec(pos_ind)) ./...
                                            (sum(in_vec(pos_ind)) + eps);
                else
                    pos_weight = 0;
                    pos_center = 0;
                end
                
                if ~isempty(neg_ind)
                    neg_weight = sum(in_vec(neg_ind));
                    neg_center = sum(weighted_in_vec(neg_ind)) ./...
                                            (sum(in_vec(neg_ind)) + eps);
                else
                    neg_weight = 0;
                    neg_center = 0;
                end
                    
%                 out_val = (pos_weight*pos_center + neg_weight*neg_center)./...
%                             ((pos_center - neg_center) .* (mid_weight));
                out_val = pos_weight - neg_weight;
%                 out_val = pos_center/abs(neg_center);
%                 out_val = pos_center - neg_center;
%                 out_val = (pos_weight + neg_weight) ./ (mid_weight);
%                 out_val = (pos_weight ./ neg_weight) ;
                
                
%                 [pks, locs] = findpeaks([0 in_vec 0],'NPEAKS',3,'SORTSTR','descend');
%                 locs = locs - 1;
%                 if numel(locs)==3
%                     center_peak = coord_vec(locs(1));
%                     neg_peak = coord_vec(min(locs(2:3)));
%                     pos_peak = coord_vec(max(locs(2:3)));
%                 else
%                     neg_peak = coord_vec(min(locs(1:2)));
%                     pos_peak = coord_vec(max(locs(1:2)));
%                 end
%                 out_val = abs(pos_peak./neg_peak);
                
            case 'randval'
                out_val = rand;
            
            case {'el_1','el_2','el_3','el_4','el_5',...
                            'el_6','el_7','el_8','el_9','el_10',...
                            'el_11','el_12','el_13','el_14','el_15',...
                            'el_16','el_17','el_18','el_19','el_20',...
                            'el_21'}
                out_val = in_vec(str2num(f_name(4:end)));                
             
            case 'max'
                out_val = max(in_vec);
                
            otherwise
                out_val = 0;
                disp(['@vec2val: function not recognized or not supported: ' f_name]);
        end
        if isempty(out_val) || isnan(out_val) || isinf(out_val)
            out_val = 0;
        end
    end

    % flow process (full or partial)
    function [output, stage_data] = process_feature_flow(flow, input_type, input_cell, output_type, context)
                
        if ~exist('context','var') || isempty(context)
            context = '';
        end
        
        % wheather to save the intermidiate data or not
        stage_data_flag = nargout>=2;
%         stage_data_flag = 1;
        
        % input and flow type init
        switch input_type
            case 'sig'
                start_stage = 0;
                signal = input_cell{1};
                fs = input_cell{2};
                if stage_data_flag
                    stage_data{1} = input_cell;
                end
            case 'mat'
                if isfield(flow, 'sig2mat') && any(strcmp(flow.sig2mat, {'specgram_256', 'specgram_1024'})) && isstruct(input_cell{1})
                    feat_struct = input_cell{1};
                    if strcmp(flow.sig2mat, 'specgram_256')
                        out_mat = feat_struct.spectrum_256;
                        f_vec = feat_struct.f_vec_256;
                        t_vec = feat_struct.t_vec_80;
                    else
                        out_mat = feat_struct.powspec_1024;
                        f_vec = feat_struct.f_vec_1024;
                        t_vec = feat_struct.t_vec_512;                        
                    end
                else
                    out_mat = input_cell{1};
                    f_vec = input_cell{2};
                    t_vec = input_cell{3};                    
                end
                
                start_stage = 1;
                if stage_data_flag
                    stage_data = {{[],[]}, input_cell};
                end
            case 'vec'
                start_stage = 3;
                out_vec = input_cell{1};
                coord_vec = input_cell{2};
                if stage_data_flag
                    stage_data = {{[],[]}, {[],[],[]}, input_cell};
                end
        end
        
        
        % outputs
        if nargin<4
            output_type = 'val';
        else
            switch output_type
                case 'mat'
                    end_stage = 1;
                case 'vec'
                    end_stage = 3;
                case 'val'
                    end_stage = 4;
            end
        end
        
        % 0
        if (start_stage == 0 && end_stage >= 0)
            [out_mat, f_vec, t_vec] = sig2mat_transform(signal, fs, flow.sig2mat{1}, context);
            if stage_data_flag
                stage_data{end+1} = {out_mat, f_vec, t_vec};
            end
            clear signal;
        end
        
        % 1
        if (start_stage <= 1 && end_stage >= 1) && isfield(flow,'mat2mat') && ~isempty(flow.mat2mat)
            % replacing the serial in order to provide intermidiate outputs           
            for i=1:numel(flow.mat2mat) 
                [out_mat, f_vec, t_vec] = mat2mat_transform(out_mat, f_vec, t_vec, flow.mat2mat{i});                       
                if stage_data_flag
                    stage_data{end+1} = {out_mat, f_vec, t_vec};
                end
            end
        end
          
        % 2
        if (start_stage <= 1 && end_stage >= 2) && isfield(flow,'mat2vec') && ~isempty(flow.mat2vec)
                       
            [out_vec, coord_vec] = mat2vec_reduce(out_mat, f_vec, t_vec, flow.mat2vec{1},flow.mat2vec{2});
            if stage_data_flag
                stage_data{end+1} = {out_vec, coord_vec};
            end
%             clear out_mat;
        end
           
        % 3
        if (start_stage <= 3 && end_stage >= 3) && isfield(flow,'vec2vec') && ~isempty(flow.vec2vec)
            
            % replacing the serial in order to provide intermidiate outputs           
            for i=1:numel(flow.vec2vec) 
                [out_vec, coord_vec] = vec2vec_transform(out_vec, coord_vec, flow.vec2vec{i});                       
                if stage_data_flag
                    stage_data{end+1} = {out_vec, coord_vec};
                end
            end
        end
           
        % 4
        if end_stage >=4 
            val = vec2val_reduce(out_vec, coord_vec, flow.vec2val{1});                            

            if isfield(flow,'threshold') && ~isempty(flow.threshold)
                cat_str = flow.categories{1 + (val >= flow.threshold)};
            else
                cat_str = '';
            end
            
            if stage_data_flag
                stage_data{end+1} = {val, cat_str};
            end
        else
            if stage_data_flag
                stage_data{end+1} = {[], []};
            end
        end
        
        switch output_type
            case 'mat'
                output = {out_mat, f_vec, t_vec};
            case 'vec'
                output = {out_vec, coord_vec};
            case 'val'
                output = {val, cat_str};
        end
    end


end %main
