import os
import time
from shutil import copyfile
import numpy as np
import pandas as pd
import sys
sys.path.append('../.')
import MaccabiExperiments.ConfigGrid as cfg_train
from MaccabiExperiments.MetadataBase import MetadataBase
from CommonML.BasicGrid import BasicGrid
import logging.config
from TAVCommon import log_conf

logging.config.dictConfig(log_conf.create_log_dict_no_file(logging.DEBUG))
cur_logger = logging.getLogger('MetaGrid')
cur_logger.info('Starting Run :)')


start_time = time.time()
cwd = os.getcwd()

EBobj = MetadataBase()

if not os.path.exists(cfg_train.OUT_PATH):
    os.makedirs(cfg_train.OUT_PATH)

src = cwd + '/ConfigGrid.py'
dst = cfg_train.OUT_PATH + 'ConfigGrid.py'
copyfile(src, dst)

results = pd.DataFrame([])
# feature_coef = pd.DataFrame([])
curr_point = 1
total_grid_points = len(cfg_train.FEATURE_THRESHOLD_LIST) * len(cfg_train.AGE_SPLIT_LIST) * len(cfg_train.DURATION_LIST)

# preform meta grid search wrapping BasicGrid
for curr_age in cfg_train.AGE_SPLIT_LIST:
    for curr_days_death in cfg_train.DURATION_LIST:
        for curr_features_th in cfg_train.FEATURE_THRESHOLD_LIST:
            EBobj.SplitData(curr_features_th, curr_age, curr_days_death)
            mg_results = [curr_age, curr_days_death, curr_features_th]
            mg_titles = ['age_split', 'days_death', 'feature_threshold']
            mg_signature = '{}/{}'.format(curr_point, total_grid_points)

            BGobj = BasicGrid(EBobj, mg_results, mg_titles, mg_signature, cfg_train.OUT_PATH)
            curr_result = BGobj.GridSearch(cfg_train.MODEL_LIST, cfg_train.REGULAR_C_LIST, cfg_train.CLASS_RATIO_LIST,
                                           cfg_train.FEATURE_SELECTION_LIST, cfg_train.PCA_LIST)
            results = results.append(curr_result)
            curr_point += 1

results.to_csv(cfg_train.OUT_PATH + 'grid_search.csv', index=False)
# feature_coef.to_csv(cfg_train.OUT_PATH + 'feature_weights.csv', index=False)

print('Done in {} minutes.'.format(np.round((time.time( ) -start_time ) /float(60) , decimals=2)))