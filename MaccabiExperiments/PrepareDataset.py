import pandas as pd
import numpy as np
import datetime
import math
import logging
import MaccabiExperiments.ConfigGrid as cfg_train

#TODO: add sample weight


class PrepareDataset:
    def __init__(self, dataset_list=cfg_train.DATASETS):
        self.datasets = {}
        self.logger = logging.getLogger('PrepareDataset')

        for dataset in dataset_list:
            if dataset == 'train':
                self.produce_input()
            else:
                self.produce_input(drop_duplicate=False, set_type=dataset)

        self.exclude_validation_from_training()   # TODO: add comment about subset building order
        self.calculate_sample_weight()  # TODO: not relevant?

        for k in self.datasets.keys():
            self.logger.info('{} set dimensions: {}'.format(k,self.datasets[k][0].shape))

    def produce_input(self, drop_duplicate=cfg_train.DROP_DUPLICATE, set_type='train'):

        HLD_file = cfg_train.HLD_FILE
        MD0_file = cfg_train.MD0_FILE

        md0 = pd.read_csv(MD0_file, index_col=None, encoding='utf-8')
        md1 = pd.read_csv(cfg_train.MD1_FILE, index_col=None)
        md_df = pd.merge(left=md0, right=md1, on='patient_id')

        # Calculate age at time of recording
        file_date = md_df['rec_date'].values
        file_year = [int(x[:4]) for x in file_date]
        patient_age = file_year - md_df.CUSTOMER_BIRTH_Year.values
        md_df = md_df.rename(columns={'CUSTOMER_BIRTH_Year': 'patient_age'})
        md_df['patient_age'] = patient_age

        # add mortality (for vitality)
        is_dead = (md_df['DATE_DEATH'].values != '1900-12-31').astype(int)
        md_df['mortality'] = is_dead

        # Calculate duration (for cox)
        rec_date = pd.to_datetime(md_df.rec_date)
        md_df['last_alive'] = pd.to_datetime(md_df.DATE_DEATH)
        md_df.loc[md_df['mortality'] == 0, 'last_alive'] = pd.datetime.strptime(cfg_train.EXPERIMENT_DATE, '%Y-%m-%d')
        duration = (md_df.last_alive - rec_date).dt.days
        md_df['duration'] = duration

        hld_df = pd.read_csv(HLD_file, index_col=None)

        xny_df = pd.merge(left=md_df, right=hld_df, on='filename')

        if cfg_train.FILTER_NON_HEBREW:
            xny_df = xny_df.loc[(xny_df.language != xny_df.language) | (xny_df.language == 'hebrew')]  # keeps NaN and hebrew

        # BMI filter
        if cfg_train.BMI_FILTER:
            xny_df = xny_df[xny_df['BMI'] < cfg_train.BMI_LIMIT]
            print('BMI filter is on!')

        if 'BMI' in cfg_train.MULTIVAR_EXTRA:
            xny_df.dropna(subset=['BMI'], inplace=True)

        # Add record time features
        if cfg_train.INCLUDE_TIME:
            try:
                xny_df = self.add_time_features(xny_df)
            except:
                print('Cannot add time features')

        xny_df.rename(columns={'CUSTOMER_SEX': 'patient_gender'}, inplace=True)

        # M/F models
        if cfg_train.GENDER_FILTER == 'M':
            xny_df = xny_df.loc[xny_df.patient_gender == 'M']
        elif cfg_train.GENDER_FILTER == 'F':
            xny_df = xny_df.loc[xny_df.patient_gender == 'F']
        else:
            pass

        if set_type == 'train' and drop_duplicate:
            xny_df.sort_values(by=['rec_date'], inplace=True)
            xny_df.drop_duplicates(subset='patient_id', keep='last', inplace=True)
            # xny_df = self.keep_last_rec(xny_df)

        elif set_type == 'all_chf':
            xny_df = xny_df.loc[xny_df.CARDIO_CHF_CD == 1]
            if cfg_train.DO_GRID_SEARCH:
                # todo: YL, patch
                xny_df.sort_values(by=['rec_date'], inplace=True)
                xny_df.drop_duplicates(subset='patient_id', keep='last', inplace=True)
                # xny_df = self.keep_last_rec(xny_df)
        elif set_type == 'non_chf':
            xny_df = xny_df.loc[xny_df.CARDIO_CHF_CD == 0]
            if cfg_train.DO_GRID_SEARCH:
                # todo: YL, patch
                xny_df.sort_values(by=['rec_date'], inplace=True)
                xny_df.drop_duplicates(subset='patient_id', keep='last', inplace=True)
                # xny_df = self.keep_last_rec(xny_df)
        elif set_type == 'hebrew_chf':
            xny_df = xny_df.loc[(xny_df.language != xny_df.language) | (xny_df.language == 'hebrew')]  # keeps NaN and hebrew
            xny_df = xny_df.loc[xny_df.CARDIO_CHF_CD == 1]

        elif set_type == 'russian_all':
            xny_df = xny_df.loc[xny_df.language == 'russian']
        else:
            print('filter not defined')
            return

        if cfg_train.TARGET == 'COPD_CD': # todo: patch to be implemented properly
            xny_df = xny_df[(((xny_df['COPD_CD'] == 0) & ((xny_df['Asthma'] == 1) | (xny_df['Bronchitis'] == 1))) == 0)]

        if cfg_train.TARGET in xny_df.columns:
            xny_df = xny_df[xny_df[cfg_train.TARGET] != -7]

        cols = [c for c in xny_df.columns if not c.startswith('voice_detected')]
        xny_df = xny_df[cols]

        if cfg_train.MULTIFLOW_HLD:
            first_x_col = np.where(xny_df.columns.values == 'feat_1')[0][0]
        else:
            first_x_col = np.where(xny_df.columns.values == cfg_train.FIRST_FEATURE)[0][0]
        last_x_col = xny_df.columns.values[-1]
        self.logger.debug('first feature column {}; last feature column {}'.format(xny_df.columns[first_x_col], last_x_col))

        X = xny_df.iloc[:, first_x_col:].values
        feature_names = xny_df.columns[first_x_col:]

        # result = (X, Y, feature_names, xny_df)
        result = (X, feature_names, xny_df)
        self.datasets.update({set_type: result})
        return result


    def keep_last_rec(self, df):
        df_1 = df[df.segment == 1].copy()
        df_1.sort_values(by=['rec_date'], inplace=True)
        df_1.drop_duplicates(subset='patient_id', keep='last', inplace=True)
        df_1 = df_1[['filename']]
        df = pd.merge(left=df, right=df_1, on='filename')
        return df

    def add_time_features(self, xny_df):
        rec_time = xny_df.rec_date.values.astype(str)
        sin_time = []
        cos_time = []
        sin_2time = []
        cos_2time = []
        for h in rec_time:
            curr_rec_time = datetime.datetime.strptime(h, '%Y-%m-%d %H:%M:%S')
            time_in_hours = curr_rec_time.hour + curr_rec_time.minute / 60. + curr_rec_time.second / 3600.
            time_in_radians = time_in_hours * math.pi / 12.

            sin_time.append(math.sin(time_in_radians))
            cos_time.append(math.cos(time_in_radians))
            sin_2time.append(math.sin(2 * time_in_radians))
            cos_2time.append(math.cos(2 * time_in_radians))

        xny_df = xny_df.assign(sin_time=sin_time)
        xny_df = xny_df.assign(cos_time=cos_time)
        xny_df = xny_df.assign(sin_2time=sin_2time)
        xny_df = xny_df.assign(cos_2time=cos_2time)

        return xny_df

    def exclude_validation_from_training(self):
        keys = self.datasets.keys()
        remove_samples = []
        for k in keys:
            if k == 'train':
                continue
            remove_samples += list(self.datasets[k][2].filename.values)

        X, feature_names, df = self.datasets['train']
        df.reset_index(inplace=True)

        keep_idx = [idx for idx in range(df.shape[0]) if df.loc[idx,'filename'] not in remove_samples]

        new_X = X[keep_idx, :]
        new_df = df.iloc[keep_idx, :]

        self.datasets.update({'train': (new_X, feature_names, new_df)})
        return

    def calculate_sample_weight(self):
        pass

    # Need this method since target definition can be a grid search variable
    @staticmethod
    def get_target_vector(data, age_split=-1, days_death=-1):
        if cfg_train.TARGET == 'patient_gender':
            Y = data['patient_gender'].values == 'M'
        elif 'vtlt' in cfg_train.TARGET:
            Y = (data['patient_age'].values > age_split) | (data['mortality'].values == 1)
        elif cfg_train.TARGET == 'days2live':
            data = data[(data.duration >= days_death) | (
            data.mortality == 1)]  # remove patients with small duration because of recording, that are still alive
            Y = data['duration'].values < days_death
        else:
            xny_df = data[data[cfg_train.TARGET] != -7]
            Y = xny_df[cfg_train.TARGET].values
        return Y

    @staticmethod
    def duration_cleanup(data, days_death, feature_names):
        data = data[(data.duration >= days_death) | (data.mortality == 1)]  # remove patients with small duration because of recording, that are still alive
        return data

if __name__ == "__main__":
    PD = PrepareDataset(dataset_list=['train', 'all_chf', 'hebrew_chf', 'russian_all'])
