import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from lifelines import CoxPHFitter
from lifelines import KaplanMeierFitter
import statsmodels.api as sm

import MaccabiExperiments.ConfigGrid as cfg_train
import CommonML.LinearSigmoidModel as LinearSigmoidModel


class ML_Eval():
    def __init__(self):
        pass

    @staticmethod
    def EvaluateSubset(ds_info, prefix, ranking_key=cfg_train.RANKING_KEY):
        resultX = []
        titlesX = []

        gender_int = (ds_info.patient_gender == 'M').astype(int)
        ds_info = ds_info.assign(Gender=gender_int)

        # calculate mean blood pressure
        # mean value
        if cfg_train.BP_VALUE == 'mean':
            DP = ds_info['BP_LOW'].values
            SP = ds_info['BP_HI'].values
            MAP = 2.*DP/3. + SP/3.
            ds_info = ds_info.assign(BP=MAP)
        # only systolic
        elif cfg_train.BP_VALUE == 'systolic':
            SP = ds_info['BP_HI'].values
            ds_info = ds_info.assign(BP=SP)
        # binary value
        else:
            IS_HIGH = ds_info['BLOODPRESURE_CD'].values
            ds_info = ds_info.assign(BP=IS_HIGH)

        if 'vtlt' in cfg_train.TARGET or cfg_train.TARGET == 'days2live':
            cox_reg = ML_Eval.cox_regression(ds_info, prefix, ranking_key=ranking_key)
            for key in cox_reg.keys():
                resultX += list(cox_reg[key])
                titlesX += [prefix+key+'_exp(coef)',prefix+key+'_p-value']
        else:
            IncludeGenderInMV = ((cfg_train.TARGET != 'patient_gender') and (cfg_train.GENDER_FILTER == 'all'))
            if IncludeGenderInMV:
                columns = [ranking_key] + cfg_train.MULTIVAR_EXTRA
            else:
                columns = [ranking_key] + cfg_train.MULTIVAR_EXTRA
                columns = [col for col in columns if col!='Gender']
            X = ds_info[columns].as_matrix()
            y = ds_info.TrueLabel.values

            OR = ML_Eval.calculate_odds_ratio(X, y, columns)

            for col in columns:
                if col=='score' or col=='DistNormed' or col=='ScoreNormed' or col=='highQ' or col=='rescaled2skew':
                    resultX += [OR[ranking_key]['OR'], OR[ranking_key]['p-value']]
                    titlesX += [prefix+'OR_biomarker', prefix+'p_biomarker']
                else:
                    resultX += [OR[col]['OR'], OR[col]['p-value']]
                    titlesX += [prefix + 'OR_'+col, prefix + 'p_'+col]
        return resultX, titlesX

    @staticmethod
    def cox_regression(data, prefix, ranking_key=cfg_train.RANKING_KEY):
        data_cox = data[['mortality', 'duration', ranking_key] + cfg_train.MULTIVAR_EXTRA]
        if len(np.unique(data['patient_gender'].values))==2:
            gender_int = (data['patient_gender'].values == 'M').astype(int)
            data_cox = data_cox.assign(Gender=gender_int)
        data_cox = data_cox[data_cox['duration']>=0]  # removes zombie patients

        # q4 = data_cox[ranking_key].quantile(.75)
        # dich_bio = (np.where(data_cox[ranking_key] < q4, 0, 1))  # for q1-q3 assign 0, otherwise assign 1
        # data_cox[ranking_key]=dich_bio
        data_cox.to_csv(cfg_train.OUT_PATH + prefix + 'data_cox.csv', index=False)

        # data_cox[ranking_key] = LinearSigmoidModel.vector_scaler(data_cox[ranking_key])

        cf = CoxPHFitter()
        cf.fit(data_cox, 'duration', event_col='mortality')
        cf.print_summary()

        # ML_Eval.km_curve(data_cox, all_quartile=True)

        columns = [ranking_key] + cfg_train.MULTIVAR_EXTRA
        if len(np.unique(data['patient_gender'].values)) < 2:
            columns = [col for col in columns if col != 'gender_int']

        results = {}
        for col in columns:
            if col == 'score' or col == 'DistNormed' or col=='ScoreNormed' or col == 'highQ' or col=='rescaled2skew':
                results.update({'biomarker': cf.summary.loc[ranking_key,['exp(coef)','p']].values})
            else:
                results.update({col: cf.summary.loc[col, ['exp(coef)', 'p']].values})

        return results

    @staticmethod
    def km_curve(df, all_quartile=False, ranking_key=cfg_train.RANKING_KEY):
        data = df.copy()

        quartiles = pd.qcut(data[ranking_key], 4, labels=False).values
        data = data.assign(quartiles=quartiles)

        q4vs_rest = np.where(data.quartiles < 3, 0, 1)
        data = data.assign(q4vs_rest=q4vs_rest)

        T = data['duration'].values
        C = data['mortality'].values

        ax = plt.subplot(111)
        t = np.linspace(0, np.median(data.duration), 25)
        # t = np.linspace(0, 600, 25)

        if all_quartile:
            for i in np.unique(data.quartiles.values):
                kmf = KaplanMeierFitter()
                iq = np.where(data.quartiles == i)[0]
                ax = kmf.fit(T[iq], C[iq], label='Q' + str(i + 1), timeline=t).plot(ax=ax)
        else:
            kmf1 = KaplanMeierFitter()
            kmf2 = KaplanMeierFitter()
            # iq_0 = np.where(data.q4vs_rest==0)[0]
            # iq_1 = np.where(data.q4vs_rest==1)[0]
            # ax = kmf1.fit(T[iq_0],C[iq_0], label='Q1-Q3', timeline=t).plot(ax=ax)
            # ax = kmf2.fit(T[iq_1],C[iq_1], label='Q4', timeline=t).plot(ax=ax)
            iq_0 = np.where(data.quartiles == 0)[0]
            iq_1 = np.where(data.quartiles == 3)[0]
            ax = kmf1.fit(T[iq_0], C[iq_0], label='Q1', timeline=t).plot(ax=ax)
            ax = kmf2.fit(T[iq_1], C[iq_1], label='Q4', timeline=t).plot(ax=ax)
            add_at_risk_counts(kmf1, kmf2)

        plt.xlabel('Time (Days)')
        plt.ylabel('Survival Probability (%)')
        plt.title('Survival Probability as a function of the Vocal Biomarker')
        plt.savefig(cfg_train.OUT_PATH_CV + 'KM_Survival.png')
        plt.show()


    @staticmethod
    def calculate_odds_ratio(X, y, columns):
        # scaler = StandardScaler()
        # X_transformed = scaler.fit_transform(X)
        # res = sm.Logit(y, X_transformed).fit()
        try:
            res = sm.Logit(y, X).fit()
            OR = np.exp(res.params)
            p = res.pvalues
            conf = res.conf_int()
        except:
            OR = [0] * len(columns)
            p = [0] * len(columns)
            conf = [0] * len(columns)

        results = {}
        for idx,col in enumerate(columns):
            curr_res = {'OR': OR[idx], 'p-value': p[idx], 'conf_int': conf[idx]}
            results.update({col: curr_res})

        return results