from datetime import datetime
TIMESTAMP = datetime.now().strftime('%y-%m-%d')

############### dataset settings ###############

TARGET = 'vtlt25'  # 'vtlt24' # 'vtlt23' # 'OSA' # 'COPD_CD' # 'BLOODPRESURE_CD' # 'patient_gender' # 'CAD' #'days2live'

if TARGET == 'vtlt23':
    DATASETS = ['train', 'all_chf']  # ['train', 'all_chf', 'hebrew_chf', 'russian_all']
elif TARGET == 'vtlt24':
    DATASETS = ['train', 'non_chf']  # ['train', 'all_chf'] # ['train'] #
elif TARGET == 'vtlt25':
    DATASETS = ['train', 'all_chf']
else:
    DATASETS = ['train']

MULTIVAR_EXTRA = ['patient_age', 'Gender'] # ['patient_age', 'Gender', 'BMI', 'BP'] #, 'BMI'

FILTER_NON_HEBREW = False # True #
DROP_DUPLICATE = True # False #
GENDER_FILTER = 'all' # 'F' # 'M' #
BMI_FILTER = False # True #
if BMI_FILTER:
    BMI_LIMIT = 30
    print('BMI_LIMIT is ', BMI_LIMIT)
BP_VALUE = 'binary' # 'mean' # 'systolic' #

# FILTER_CARDIO = False # True #
# FILTER_NON_CHF_CARDIO = False # True #


# 'score': [0,1] sigmoid fit to the distance from hyper plane
# 'DistNormed': original rankink mechanism - normalized distance from hyper plane
# 'highQ': upper quartile
# 'rescaled2skew': map 'score' to erf and adjust sigma to one
RANKING_KEY = 'DistNormed' # 'rescaled2skew' # 'score' # 'highQ' # 'ScoreNormed' #

############### features settings ###############
MULTIFLOW_HLD = False # True #
INCLUDE_TIME = False # True #
COMBINED_HLD = False # True #

############### Whitening settings ###############
SAMPLE_WEIGHT_METHOD = 'old' # 'speaker' #  'clustering' #
AGE_WEIGHT = False # True #
if AGE_WEIGHT:
    CLASS_WEIGHTS = {'M_age1_False': 1., 'M_age1_True': 1.,
                     'M_age2_False': 1., 'M_age2_True': 1.,
                     'M_age3_False': 1., 'M_age3_True': 1.,
                     'F_age1_False': 1., 'F_age1_True': 1.,
                     'F_age2_False': 1., 'F_age2_True': 1.,
                     'F_age3_False': 1., 'F_age3_True': 1.}
else:
    # if TARGET == 'patient_gender':
    #     CLASS_WEIGHTS = {'F_True': 1., 'M_True': 1.}
    # else:
    CLASS_WEIGHTS = {'M_False': 1., 'M_True': 1., 'F_False': 1., 'F_True': 1.}

############### Folds settings ###############
N_KFOLDS = 10
N_REPEATS = 1
PERMUTATION_NUM = 50

############### grid settings ###############
RENORM_PCA = False  # todo: support in code
DO_GRID_SEARCH = True  # False #
if DO_GRID_SEARCH:
    if TARGET == 'vtlt23' or TARGET == 'vtlt25':
        MODEL_LIST = ['SVC']
        REGULAR_C_LIST = [0.1, 0.05, 0.01, 0.005, 0.001]
        CLASS_RATIO_LIST = [2, 1.8, 1.7, 1.6]
        PCA_LIST = [-1]
        FEATURE_SELECTION_LIST = [-1]

        FEATURE_THRESHOLD_LIST = [0.7, 0.65, 0.6]
        AGE_SPLIT_LIST = [88]
        DURATION_LIST = [-1]

else:  # saves model file and validation results
    # 88	-1	0.65	SVC	0.01	1.8	-1 (paper 2.3 - for Mayo combinations EX)
    # 90	-1	0.5	Logistic	0.01	1.5	-1 (for Mayo combinations)

    MODEL_LIST = ['SVC']  # ['Logistic'] #
    REGULAR_C_LIST = [0.01]
    CLASS_RATIO_LIST = [1.8]  # [1.5] #
    PCA_LIST = [-1]
    FEATURE_SELECTION_LIST = [-1]

    FEATURE_THRESHOLD_LIST = [0.65]  # [0.65] # [0.5] #
    AGE_SPLIT_LIST = [88]  # [90] #
    DURATION_LIST = [-1]


############### paths settings ###############
# paper2.2-3 input
MD1_FILE = 'E:/Algo/dat/MetaData/RecordingsData181003.csv'
MD0_FILE = 'E:/Algo/dat/MetaData/PatientsMDnFolds181003.csv'

# FEATURE_RANK_PATH = 'E:/Algo/dat/HLD/feature_noise_181226.csv'  # todo: reproduce with datemark as MD1
# FEATURE_RANK_PATH = 'E:/Algo/dat/HLD/feature_noise_combined.csv'
FEATURE_RANK_PATH = 'E:/Algo/dat/HLD/feature_noise_190128.csv'

if MULTIFLOW_HLD:
    print('Using MultiFlow HLD')
    HLD_FILE = 'E:/Algo/dat/MultiFlow/HLD_MultiFlow_SegmentsData180827_20sec.csv'
else:
    if COMBINED_HLD:
        HLD_FILE = 'E:/Algo/dat/HLD/HLD_Combined_180902.csv'
        EXPERIMENT_DATE = '2018-04-06'
    else:
        # HLD_FILE = 'E:/Algo/dat/HLD/HLD181230_NEW_PAT_M8.csv'
        # EXPERIMENT_DATE = '2018-09-02' # the last date of death in our DB is 1/9/18
        HLD_FILE = 'E:/Algo/dat/HLD/HLD190127_PAT_M8_20sec.csv'
        EXPERIMENT_DATE = '2018-09-02'

# FIRST_FEATURE = 'mfcc0_decileM1'
FIRST_FEATURE = 'mfcc_0_moments8_0'


PATH_PREFIX = 'E:/Algo/Yonatan/paper/'
# PATH_PREFIX = 'E:/Algo/DataExperiments/' + TARGET + '/' + GENDER_FILTER

if BMI_FILTER:
    PATH_PREFIX += '_' + 'BMIlt' + str(BMI_LIMIT)

if DO_GRID_SEARCH:
    if RANKING_KEY == 'DistNormed':
        OUT_PATH = PATH_PREFIX + '/M8_181004gCHFonlyNewData/'
    else:
        OUT_PATH = PATH_PREFIX + '/M8_181004g2skew/'
else:
    OUT_PATH = PATH_PREFIX + '/M8_181004pCHFonlyEX_new_data/'

print('*********** OUT PATH = {}'.format(OUT_PATH))



