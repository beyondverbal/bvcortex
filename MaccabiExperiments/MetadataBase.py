import pandas as pd
import numpy as np

import CommonML.Stratification as Stratification
import MaccabiExperiments.ConfigGrid as cfg_train
from MaccabiExperiments.PrepareDataset import PrepareDataset
from MaccabiExperiments.Evaluation import ML_Eval
from sklearn.metrics import roc_auc_score


class FeatureBase():
    def __init__(self, feature_names):
        feature_ranks = pd.read_csv(cfg_train.FEATURE_RANK_PATH)
        # exclude feature_ranks which are not in feature_names...
        self.feature_ranks = feature_ranks[feature_ranks['feature'].isin(feature_names)]
        self.feature_names = feature_names

    def GetFeatureMatrix(self, DF, feature_threshold):
        if feature_threshold > 0:
            self.current_features = self.feature_ranks[self.feature_ranks['corr'] >= feature_threshold].feature.values
        else:
            self.current_features = self.feature_names
        X = DF.loc[:, self.current_features].values
        return X

    def GetCurrentFeatures(self):
        return self.current_features

    def select_features(self, x, roc_auc, feat_sel_th=-1):
        if 0 < feat_sel_th <= 1:
            x_new = x[:, np.array(roc_auc) > feat_sel_th]

        elif feat_sel_th > 1:
            s = np.argsort(roc_auc)[-feat_sel_th:]
            x_new = x[:, s]
        else:
            x_new = x

        return x_new

class MetadataBase():
    def __init__(self):
        self.DB = PrepareDataset(dataset_list=cfg_train.DATASETS)
        X, feature_names, self.MD = self.DB.datasets['train']

        # TODO: find a better treatment
        if cfg_train.TARGET == 'days2live':
            self.MD = PrepareDataset.duration_cleanup(self.MD, days_death)
        self.MD.reset_index(inplace=True)

        self.FBobj = FeatureBase(feature_names)

        self.FoldsConf = {
            'n_folds': cfg_train.N_KFOLDS,
            'n_repeats': cfg_train.N_REPEATS,
            'permutation_num': cfg_train.PERMUTATION_NUM
        }
        self.output_columns = ['filename', 'patient_id', 'patient_gender', 'patient_age', 'mortality',
            'rec_date', 'duration', 'source', 'language', 'TrueLabel', 'PredictedLabel',
            'score', 'DistNormed', 'ScoreNormed', 'highQ', 'rescaled2skew', 'sample_weight', 'BMI', 'BP_HI', 'BP_LOW', 'BLOODPRESURE_CD']
        self.subset_by = ['patient_gender', 'source']
        self.md_ref = ['patient_age']

        # TODO CHANGE YS
        self.Folds0 = []
        self.feature_ranks = []

    def SplitData(self, feature_threshold, age_split, days_death):
        self.age_split = age_split
        self.days_death = days_death
        self.feature_threshold = feature_threshold

        Y = PrepareDataset.get_target_vector(self.MD, age_split=age_split, days_death=days_death)
        self.MD = self.MD.assign(TrueLabel=Y)

        Y_sources, Y_clustered, y_gender_pm = cluster_y(self.MD.TrueLabel.values, self.MD.patient_gender.values,
                                                             self.MD.patient_age.values, age_group1=64, age_group2=75)
        self.MD = self.MD.assign(source=Y_sources)
        self.MD = self.MD.assign(clustered=Y_clustered)
        self.MD = self.MD.assign(gender_pm=y_gender_pm)

        self.X = self.FBobj.GetFeatureMatrix(self.MD, self.feature_threshold)
        self.Y = self.MD.TrueLabel.values
        # self.MD.reset_index(inplace=True)

        if cfg_train.SAMPLE_WEIGHT_METHOD == 'clustering':
            self.W, self.MD = sample_weights_by_cluster(self.MD, low_age=40, high_age=100, age_bin_num=2, BMI_theshold=25, target='OSA')
        elif cfg_train.SAMPLE_WEIGHT_METHOD == 'speaker':
            self.W = self.calc_speaker_weights(self.MD.patient_id.values, cluster_weights=self.MD.cluster_weights.values)
        elif cfg_train.SAMPLE_WEIGHT_METHOD == 'old':
            if cfg_train.AGE_WEIGHT:
                self.W = calc_sample_weights(Y_clustered)
            else:
                self.W = calc_sample_weights(y_gender_pm)
        if cfg_train.AGE_WEIGHT:
            self.clustered = self.MD.clustered.values   # Stratify by 12 types (T/F, M/F ang age)
        else:
            self.clustered = self.MD.gender_pm.values   # Stratify by 4 types (T/F and M/F)

        # Groups by patient id self.MD.patient_id.values
        folds = Stratification.CreateFolds(self.X, self.MD, self.MD.patient_id.values, self.clustered, self.Y,
                                           self.FoldsConf, seed=0)
        roc_per_fold, self.feature_ranks = self.feature_ranking(self.X, self.Y, folds)
        self.Folds0 = folds, roc_per_fold

    @staticmethod
    def feature_ranking(xx, yy, folds):
        roc_auc_folds = [None] * len(folds)
        for ii, (train_index, test_index) in enumerate(folds):
            auc_folds = [roc_auc_score(yy[train_index], xx[train_index, i], average='macro') for i in range(xx.shape[1])]
            roc_auc_folds[ii] = [max(1 - x, x) for x in auc_folds]
            # aucX = [roc_auc_score(y_train_CV, x_train_CV[:, i], average='macro') for i in range(x_train_CV.shape[1])]
        auc_x = [roc_auc_score(yy, xx[:, j], average='macro') for j in range(xx.shape[1])]
        roc_auc_x = [max(1 - x, x) for x in auc_x]
        return roc_auc_folds, roc_auc_x

    def Folds(self):
        return self.Folds0

    def SubsetData(self, subset):
        X0, feature_names, info_df = self.DB.datasets[subset]
        # TODO: find a better treatment
        if cfg_train.TARGET == 'days2live':
            info_df = PrepareDataset.duration_cleanup(info_df, self.days_death)

        Y = PrepareDataset.get_target_vector(info_df, age_split=self.age_split, days_death=self.days_death)
        info_df = info_df.assign(TrueLabel=Y)

        X = self.FBobj.GetFeatureMatrix(info_df, self.feature_threshold)

        return X, Y, info_df

    def GetCurrentFeatures(self):
        return self.FBobj.GetCurrentFeatures()

    def get_validation_sets(self):
        validation_sets = list(self.DB.datasets.keys())
        # if 'train' in validation_sets:
        #     validation_sets.remove('train')
        return validation_sets

    # YL: this method is a wrapper for evaluation since this object is the only one passed to CommonML
    def EvaluateSubset(self, output, subset):
        return ML_Eval.EvaluateSubset(output, subset)

def cluster_y(y, gender_list, age_list, age_group1=69, age_group2=80):
    y_clustered = np.zeros(len(y)).astype(str)
    y_sources = np.zeros(len(y)).astype(str)
    y_gender_pm = np.zeros(len(y)).astype(str)

    for idx in range(len(y_clustered)):
        age = age_list[idx]
        if age < age_group1:
            age_group = 'age1'
        elif age >= age_group1 and age < age_group2:
            age_group = 'age2'
        else:
            age_group = 'age3'

        y_sources[idx] = str(gender_list[idx]) + '_' + age_group
        y_clustered[idx] = y_sources[idx] + '_' + str(bool(y[idx]))
        y_gender_pm[idx] = str(gender_list[idx]) + '_' + str(bool(y[idx]))

    return y_sources, y_clustered, y_gender_pm

def sample_weights_by_cluster(df_info, low_age=40, high_age=100, age_bin_num=2, BMI_theshold=30, target='OSA'):

    # age_bins = np.arange(low_age, high_age, age_width)
    age_bins = np.linspace(low_age, high_age, num=age_bin_num+1)

    # override limits to include outliers
    age_bins[0] = 0
    age_bins[-1] = 200

    gender_types = np.unique(df_info.patient_gender.values)
    class_types = np.unique(df_info[target].values)

    df = df_info.assign(bin=-99)
    bin_counter = 0

    for i in range(len(age_bins) - 1):
        for gender in gender_types:
            for clas in class_types:
                highBMI_idx = df[ (df.patient_age >= age_bins[i]) & (df.patient_age < age_bins[i + 1]) &
                                  (df.patient_gender == gender) & (df[target] == clas) &
                                  (df.BMI >= BMI_theshold) ].index
                lowBMI_idx = df[ (df.patient_age >= age_bins[i]) & (df.patient_age < age_bins[i + 1]) &
                                  (df.patient_gender == gender) & (df[target] == clas) &
                                  (df.BMI < BMI_theshold) ].index
                df.set_value(highBMI_idx, 'bin', bin_counter)
                bin_counter += 1
                df.set_value(lowBMI_idx, 'bin', bin_counter)
                bin_counter += 1

    clusters = np.unique(df.bin.values, return_counts=True)
    cluster_num = clusters[0]
    cluster_size = clusters[1]
    df = df.assign(sample_weight=0)
    for i in range(len(cluster_num)):
        weight_idx = df[df.bin == cluster_num[i]].index
        if cluster_size[i] == 0:
            weight = 0
        else:
            weight = 1. / cluster_size[i]
        df.set_value(weight_idx, 'sample_weight', weight)

    sample_weights = df.sample_weight.values

    return sample_weights, df

def calc_sample_weights(y_clustered):

    n_samples = len(y_clustered)
    sum_weights = sum(cfg_train.CLASS_WEIGHTS.values())
    class_count = np.unique(y_clustered, return_counts=True)
    class_count_dict = dict(zip(class_count[0], class_count[1]))
    class_weight = {}
    for key, value in class_count_dict.items():
        # weight = n_samples / (len(class_count[0]) * value)    # 'balanced'
        # weight = cfg_train.CLASS_WEIGHTS[key] / (sum_weights * value)  # with pre-defined weights
        weight = cfg_train.CLASS_WEIGHTS[key] * n_samples / (sum_weights * value)  # with pre-defined weights
        class_weight.update({key: weight})

    weight_list = np.zeros(shape=n_samples)
    for key, value in class_weight.items():
        idx = np.where(y_clustered==key)
        weight_list[idx] = value

    return weight_list

def calc_speaker_weights(speaker_vec, cluster_weights=[]):

    if len(cluster_weights)>0:
        pass

    speaker_list = np.unique(speaker_vec.astype(int).astype(str), return_counts=True)
    m = len(speaker_list[0])
    N = len(speaker_vec)
    weights = N / (speaker_list[1] * m)
    weight_dict = dict(zip(speaker_list[0],weights))

    weight_list = []
    for speaker in speaker_vec:
        weight_list.append(weight_dict[str(int(speaker))])

    if len(cluster_weights)>0:
        # cluster_w, counts = np.unique(cluster_weights, return_counts=True)
        weight_list = weight_list * cluster_weights

    return weight_list
