import math
from PATrends.SerializeDeserialize import SerializeDeserialize as SerDeSer
import numpy as np

from PATrends import PATConfig as PATCfg


class PATManager:

    def __init__(self):
        self.frame_step = PATCfg.FRAME_STEP_FEATURES
        self.frame_size = PATCfg.FRAME_SIZE_FEATURES
        self.SerDeSer = SerDeSer()

    @staticmethod
    def calc_num_of_frames(vector_len, frame_size, frame_step):
        # minus two frames to sync pitch_estimation and other features
        n = PATCfg.PITCH_NEEDED_FUTURE_FRAMES
        p = math.trunc(1 + (vector_len - frame_size) / float(frame_step)) - n

        if p < 1:  # todo
            return 0, 0
        else:
            return p, (p+n)

    def clean_up(self):
        self.SerDeSer.clean_up_lld()

    def adjust_chunk(self, chunk):
        # add signal 'tail' from last chunk
        loaded_chunk = self.SerDeSer.load_locally_lld()
        chunk_extended = np.append(loaded_chunk, chunk)

        p, p_ext = self.calc_num_of_frames(len(chunk_extended), self.frame_size, self.frame_step)

        # find max num of samples for analysis given frame size & overlap
        # process_to = (p-1) * self.frame_step + self.frame_size

        # extract signal_to_analyze (divides to exact N*frames), and also tail to remain in self.memory
        # sig_to_analyze = chunk_extended[:(p-1) * self.frame_step + self.frame_size]

        if p:
            sig_to_analyze, residual = np.split(chunk_extended, [(p - 1) * self.frame_step + self.frame_size])
            sig_to_analyze_ext = chunk_extended[:(p_ext-1) * self.frame_step + self.frame_size]

            # leave enough in buffer to insure continuous analysis
            self.SerDeSer.save_locally_lld(chunk_extended[p*self.frame_step:])
        else:
            self.SerDeSer.save_locally_lld(chunk_extended)
            sig_to_analyze = []
            residual = []
            sig_to_analyze_ext = []

        return sig_to_analyze, sig_to_analyze_ext, p, len(residual)
