import pickle
import warnings
import os


class SerializeDeserialize:
    def __init__(self):
        self.local_root_dir = 'C:/BVC/YonaData1/'
        self.local_filename_lld = self.local_root_dir + 'local_save_pickle_lld.pkl'
        self.local_filename_hld = self.local_root_dir + 'local_save_pickle_hld.pkl'
        self.lld_buffer = []
        self.hld_buffer = []
        self.save_to_file = 0  # 1: to file     0: local buffer

    @staticmethod
    def serialize(input1, filename=None):
        if filename is not None:
            # PICKLE -> TO FILE
            warnings.warn('CHECK FILENAME - WHERE DOES PICKLE GO??')
            filename = 'C:/BVC/YonaData1/serialized_pickle.pkl'
            output = open(filename, 'wb')
            pickle.dump(input1, output, protocol=pickle.HIGHEST_PROTOCOL)
            output.close()

        # PICKLE -> BYTES STREAM with two inputs
        bytes1 = pickle.dumps(input1, protocol=pickle.HIGHEST_PROTOCOL)
        return bytes1

    @staticmethod
    def de_serialize(stream_in1=None, filename=None):
        if filename is None:
            # deserialize from stream
            stream_out1 = pickle.loads(stream_in1)

            return stream_out1
        else:
            output = open(filename, 'rb')
            value1 = pickle.load(output)
            output.close()
        return value1

    def save_locally_lld(self, input1):
        if self.save_to_file:  # save to external pickle
            with open(self.local_filename_lld, 'wb') as file:
                pickle.dump(input1, file)
        else:  # save to local buffer, no external files
            self.lld_buffer = input1

    def save_locally_hld(self, input1):
        if self.save_to_file:  # save to external pickle
            with open(self.local_filename_hld, 'wb') as file:
                pickle.dump(input1, file)
        else:  # save to local buffer, no external files
            self.hld_buffer = input1

    def load_locally_lld(self):
        if self.save_to_file:  # load from external pickle
            if os.path.exists(self.local_filename_lld):
                with open(self.local_filename_lld, 'rb') as file:
                    out = pickle.load(file)
                    # print('LOAD SUCCESS IN deserialize, shape = {}'.format(out.shape))
                return out
            else:
                return []
        else:
            return self.lld_buffer

    def load_locally_hld(self):
        if self.save_to_file:  # load from external pickle
            if os.path.exists(self.local_filename_hld):

                with open(self.local_filename_hld, 'rb') as file:
                    out = pickle.load(file)
                return out
            else:
                return {'lld_frames': 0, 'cur_analysis_start': 0, 'loaded_chunk': []}
        else:
            if len(self.hld_buffer) == 0:
                return {'lld_frames': 0, 'cur_analysis_start': 0, 'loaded_chunk': []}
            else:
                return self.hld_buffer

    def clean_up_lld(self):
        if os.path.exists(self.local_filename_lld):
            os.remove(self.local_filename_lld)
        self.lld_buffer = []

    def clean_up_hld(self):
        if os.path.exists(self.local_filename_hld):
            os.remove(self.local_filename_hld)
        self.hld_buffer = []
