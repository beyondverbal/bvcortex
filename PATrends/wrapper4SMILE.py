import os
import csv
import numpy as np
from subprocess import call
from PATrends import PATConfig as PATCfg

SOX_FOLDER = os.path.join(PATCfg.dev, 'sox-14-4-2')
SMILE_FOLDER = os.path.join(PATCfg.dev, 'opensmile')
SMILE_CONFIG = 'SMILE_BVC_lld.conf'
# SMILE_CONFIG = 'os_config_lld.conf'

def extract_lld(wav_name):

    temp_out_name = wav_name.split(os.sep)[-1]
    if temp_out_name[-4:] == '.wav':
        temp_out_name = temp_out_name[:-4]
    temp_out = os.path.join(SMILE_FOLDER, temp_out_name+'os_out.csv')

    cmd = os.path.join(SMILE_FOLDER, 'SMILExtract_Release.exe') + \
          ' -configfile ' + os.sep.join(os.path.realpath(__file__).split(os.sep)[:-1] + [SMILE_CONFIG]) + \
          ' -inputfile ' + wav_name + \
          ' -O ' + temp_out + \
          ' -loglevel 0'
    failed = call(cmd)

    if failed == 3:
        corrected_wav = correct_wav(wav_name)
        failed = call(cmd.replace(wav_name, corrected_wav))

    if failed:
        return True, [], []
    feature_names, vals, t = read_output(temp_out)

    t = np.expand_dims(np.array(t)*PATCfg.SAMPLING_RATE, axis=1)
    t[0, 0] = 0
    t[1, 0] = PATCfg.FRAME_STEP_FEATURES
    t = np.hstack((t, t + PATCfg.FRAME_SIZE_FEATURES))

    vals = [np.expand_dims(i, axis=0) for i in vals]
    X = np.concatenate(vals, axis=0)

    return False, X, t, feature_names


def correct_wav(wav_in):

    wav_out = wav_in.split(os.sep)
    wav_out[-1] = '_' + wav_out[-1]
    wav_out = os.sep.join(wav_out)

    if os.path.isfile(wav_out):
        return wav_out

    call(os.path.join(SOX_FOLDER, 'sox.exe') + ' ' + wav_in + ' -e signed-integer ' + wav_out)

    return wav_out


def read_output(temp_out_filename):

    names = []
    vals = []
    time_stamp = []
    if os.path.isfile(temp_out_filename):
        with open(temp_out_filename) as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                if not len(row):
                    continue
                if row[0][0] is not '@':
                    time_stamp.append(np.array(row[1]).astype(float))
                    vals.append(np.array(row[2:-1]).astype(float))
                else:
                    curr_name = row[0].replace('@attribute ', '').replace(' numeric', '')
                    names.append(curr_name)

        os.remove(temp_out_filename)
        names = names[3:-2]
    else:
        pass

    return names, vals, time_stamp
