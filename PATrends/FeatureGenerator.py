import librosa
import numpy as np
from scipy import signal
import skimage
from skimage import util
import PATrends.PATConfig as LLDCfg


class FeatureGenerator:

    # Calculate loudness per frame of a signal
    @staticmethod
    def calc_loudness(sig, frame_size, frame_step, intensity0=0.000001, power=0.3):
        # Is -1 <= sig <= 1 always?

        if len(sig) == 0:
            return 0

        window = signal.hamming(frame_size, sym=True)
        win_sum = np.sum(window)

        frames = skimage.util.view_as_windows(sig, frame_size, step=frame_step)

        # Calc Intensity
        frames2 = np.multiply(frames, frames)
        intensity = np.dot(window, frames2.T) / win_sum

        # Calc Loudness
        loudness = (intensity / intensity0) ** power

        return loudness

    def process_chunk(self, chunk, force_params=None):

        sr = LLDCfg.SAMPLING_RATE
        if not force_params:
            fr_size = LLDCfg.FRAME_SIZE_FEATURES
            fr_step = LLDCfg.FRAME_STEP_FEATURES
        else:
            fr_size = force_params['frame_size']
            fr_step = force_params['frame_step']

        fft = librosa.stft(chunk, n_fft=fr_size,
                           hop_length=fr_step,
                           win_length=fr_size,
                           window=signal.hamming(fr_size, sym=True), center=False)
        fft_abs = np.abs(fft)
        fft_spec = fft_abs**int(LLDCfg.USE_PSD_FEATURES + 1)

        mels = librosa.feature.melspectrogram(sr=sr, S=fft_spec,
                                              n_fft=fr_size,
                                              hop_length=fr_step,
                                              n_mels=LLDCfg.MEL_BANDS_NUM,
                                              fmin=0.0, fmax=sr/2, htk=True)

        mels[mels == 0] = np.inf
        mfcc = librosa.feature.mfcc(S=np.log10(mels), n_mfcc=LLDCfg.MFCC_NUM)
        mfcc[np.isinf(mfcc)] = 0
        mfcc[np.isnan(mfcc)] = 0

        loudness = self.calc_loudness(chunk, fr_size, fr_step)

        return {'mels': mels, 'mfcc': mfcc, 'fft': fft, 'fft_spec': fft_spec, 'loudness': loudness.reshape(1, -1)}

