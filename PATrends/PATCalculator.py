import numpy as np
from PATrends.PATSignalProcessor import PATSignalProcessor
from PATrends.PATManager import PATManager
import PATrends.PATConfig as PATCfg
from librosa import util


class PATCalculator:
    def __init__(self):
        self.start_index = 0
        self.new_wav = True
        self.PATProcessor = PATSignalProcessor()
        self.PATManager = PATManager()

    def clean_up(self):
        self.PATManager.clean_up()
        self.new_wav = True

    def process_chunk(self, chunk):

        # send to manager and receive adjusted chunk; manager buffers all 'extras'
        new_chunk, new_chunk_ext, num_of_frames, residual = self.PATManager.adjust_chunk(chunk)
        if len(new_chunk) > 0:
            # send to processor to calc LLDs
            lld_dict_complete = self.PATProcessor.process_chunk(new_chunk, new_chunk_ext)

            lld_dict = self.PATProcessor.remove_extra_features(lld_dict_complete)

            lld_dict, t_sec = self.sync_lld(new_chunk, lld_dict)

            return lld_dict, self.package_lld_cfg_params(residual)
        else:
            return [], self.package_lld_cfg_params(0)

    @staticmethod
    def sync_lld(signal, lld_dict):
        lld_synced = {}

        t = util.frame(np.arange(len(signal)), frame_length=PATCfg.FRAME_SIZE_FEATURES,
                       hop_length=PATCfg.FRAME_STEP_FEATURES)[[0, -1], :].T
        t_sec = np.around(t / PATCfg.SAMPLING_RATE, decimals=2)

        max_len = min([lld_dict[i].shape[1] for i in lld_dict.keys()])

        for key in lld_dict.keys():
            lld_synced.update({key: lld_dict[key][:, :max_len]})

        t_sec = t_sec[:max_len, 0]

        return lld_synced, t_sec

    @staticmethod
    def package_lld_to_df(lld_dict):
        lld_mat = np.hstack([lld_dict[lld].T for lld in PATCfg.LLD_OUTPUT])

        return lld_mat

    @staticmethod
    def get_lld_title_list():
        titles = []
        for lld in PATCfg.LLD_OUTPUT:
            if PATCfg.LLD_BASE_DIMENSION[lld] == 1:
                titles.append(str(lld))
            else:
                for lld_dim in range(PATCfg.LLD_BASE_DIMENSION[lld]):
                    titles.append(str(lld) + str(lld_dim))
        return titles

    def package_lld_cfg_params(self, residual=0):

        # new run and cleanup() changes new_wav to True
        previous_new_wav = self.new_wav
        self.new_wav = False

        return {"residual": residual,
                "new_wav": previous_new_wav}  # how many not-processed SAMPLES are in the system

    def offline_calculate_lld_matrix(self, signal):
        lld_dict, package = self.process_chunk(signal)
        return lld_dict, self.get_lld_title_list()
