import numpy as np
import math
import pickle


def save_to_pickle(filename, obj_to_save):
    output = open(filename, 'wb')
    pickle.dump(obj_to_save, output)
    output.close()


def load_from_pickle(filename):
    pkl_file = open(filename, 'rb')
    obj = pickle.load(pkl_file)
    pkl_file.close()
    return obj


def np_load_from_csv(filename, my_delimiter=','):
    return np.genfromtxt(filename, delimiter=my_delimiter)


def contiguous_regions(condition):
    # Find the indices of changes in "condition"
    d = np.diff(condition.astype(int))
    idx, = d.nonzero()

    idx += 1
    if condition[0]:
        idx = np.r_[0, idx]

    if condition[-1]:
        idx = np.r_[idx, condition.size]
    idx.shape = (-1, 2)
    return idx


def frames_in_segment(time_frames_secs, step_samples, fs):
    time_in_samples = time_frames_secs*fs
    return int(time_in_samples/step_samples)


def get_frame_number(length, frame_size, frame_step, force_analysis=False, force_one_analysis=False):
    if force_one_analysis:  # forcing analysis on complete signal, no windowing etc.
        return 1
    if force_analysis:  # force analysis also on short frames
        #  round up, to include not-complete frames - only in offline, force_analysis mode
        return int(np.ceil(1 + (length - frame_size) / float(frame_step)))
    else:
        # include only complete frames
        return math.trunc(1 + (length-frame_size)/float(frame_step))


def frames2seconds(num_of_frames, frame_size, frame_step, fs):
    if num_of_frames < 1:
        return []
    else:
        return (frame_size + (num_of_frames-1)*frame_step)/fs


def seconds2frames_ceil(seconds, frame_size, frame_step, fs):
    return math.ceil(1 + (seconds*fs-frame_size)/float(frame_step))


def seconds2frames_floor(seconds, frame_size, frame_step, fs):
    return math.floor(1 + (seconds*fs-frame_size)/float(frame_step))
