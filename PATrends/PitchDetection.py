# from feature_extraction.FeatureOperators import FeatureOperators
import logging.config

import numpy as np
from skimage.util import view_as_windows

import PATrends.MiscMethods as Misc
from PATrends import PATConfig as PATCfg
from TAVCommon import log_conf

logging.config.dictConfig(log_conf.create_log_dict_no_file(PATCfg.STD_OUT_LEVEL))


class PitchDetection:
    def __init__(self):

        # Used for AC shift matrix (after pitch is calculated)
        self.min_shift = PATCfg.PITCH_MIN_SHIFT
        self.max_shift = PATCfg.PITCH_MAX_SHIFT
        self.logger = logging.getLogger('PitchDetection')

    @staticmethod
    def pitch_parabolic_estimation(pitch_loc, d_tau_norm):
        coeff_matrix = np.array([[(pitch_loc-1)**2, pitch_loc-1, 1],
                                 [pitch_loc**2, pitch_loc, 1],
                                 [(pitch_loc+1)**2, pitch_loc+1, 1]])
        ordinate_values = np.array([d_tau_norm[pitch_loc-1], d_tau_norm[pitch_loc], d_tau_norm[pitch_loc+1]])
        x = np.linalg.solve(coeff_matrix, ordinate_values)
        if x[0] <= 0:
            # Something went wrong; return original value
            new_pitch_loc = pitch_loc
        else:
            new_pitch_loc = (-1)*x[1]/(2*x[0])
        return PATCfg.SAMPLING_RATE / new_pitch_loc

    def calc_pitch_per_frame(self, chunk_to_process):

        pitch_vec, frame_num, frames, frames_long, r_tau, pitch_corr, th_cond = self.static_calc_pitch(chunk_to_process)

        # import matplotlib.pyplot as plt
        # plt.plot(pitch_vec)
        # plt.show()

        pitch_new = np.zeros(shape=(frame_num,))
        pitch_filt = np.copy(pitch_vec)
        pitch_vec_trunk = pitch_vec  # [:-2]

        np.seterr(divide='ignore', invalid='ignore')
        diff = abs(pitch_vec_trunk - pitch_filt) / pitch_filt

        for i in range(len(diff)):
            if diff[i] > 0.2:
                pitch_val = pitch_filt[i]
            else:
                pitch_val = pitch_vec_trunk[i]
            pitch_new[i] = pitch_val

            if False:
                if pitch_val != 0:
                    tau = int(round(PATCfg.SAMPLING_RATE/pitch_val))
                    idx = 1
                    pitch_corr[0, i] = tau
                
                    frame_dot = np.dot(frames[i, :], frames[i, :])
                    for shift in range(tau+self.min_shift, tau+self.max_shift):
                        if shift > 160:
                            continue
                        else:

                            norm = np.sqrt(frame_dot * np.dot(frames_long[i,
                                                              shift:shift + PATCfg.FRAME_SIZE_FEATURES],
                                                              frames_long[i,
                                                              shift:shift+PATCfg.FRAME_SIZE_FEATURES]))
                            pitch_corr[idx, i] = r_tau[i, shift] / norm
                            idx += 1

        log_pitch = np.zeros(pitch_new.shape)
        valid_args = np.where(pitch_new != 0)
        log_values = pitch_new[valid_args]
        log_values = np.log10(log_values)
        log_pitch[valid_args] = log_values

        # self.pitch_raw[self.frame_index:self.frame_index+frame_num] = pitch_vec    # No filter
        # self.pitch_corr[:,self.frame_index:self.frame_index+frame_num] = pitch_corr

        # pitch_new = np.append(pitch_new, [-1, -1])
        # log_pitch = np.append(log_pitch, [-1, -1])

        pitch_corr_new = pitch_corr[1 + abs(self.min_shift), :].reshape(1, -1)
        # pitch_corr_new = np.append(pitch_corr_new, (-1, -1))

        pitch_dict = {'pitch': pitch_new.reshape(1, -1),
                      'LogPitch': log_pitch.reshape(1, -1),
                      'ACpitch': pitch_corr_new.reshape(1, -1),  # pitch_corr}
                      'pitch_th_cond': th_cond.reshape(1, -1)}

        # import pickle
        # output = open('first.pkl', 'wb')
        # pickle.dump(pitch_dict, output)
        # output.close()

        return pitch_dict

    @staticmethod
    def static_calc_pitch(signal,
                          sr=PATCfg.SAMPLING_RATE,
                          win_size=PATCfg.FRAME_SIZE_FEATURES,
                          hop_size=PATCfg.FRAME_STEP_FEATURES,
                          tau_max=PATCfg.TAU_MAX,
                          threshold=PATCfg.D_NORM_THRESHOLD,
                          min_shift=PATCfg.PITCH_MIN_SHIFT,
                          max_shift=PATCfg.PITCH_MAX_SHIFT):

        frame_num = Misc.get_frame_number(len(signal), win_size, hop_size)
        frame_num = frame_num - 2

        pitch_vec = np.zeros(shape=(frame_num,))

        frames = view_as_windows(signal, win_size, step=hop_size)
        frames = frames[:frame_num, :]
        frames_long = view_as_windows(signal, win_size + tau_max, step=hop_size)

        tau = np.arange(0, tau_max + 1)  # Tau=0 what's the point... should start from 1?
        r_tau = np.zeros(shape=(frame_num, len(tau)))
        for i in range(frame_num):
            r_tau[i, :] = np.correlate(frames_long[i, :], frames[i, :], mode='valid')

        frames_long_squared = np.multiply(frames_long, frames_long)
        frames_long_cumsum = np.cumsum(frames_long_squared, axis=1)
        frames_long_cumsum = np.insert(frames_long_cumsum, 0, 0, axis=1)

        # m_tau = np.subtract(frames_long_cumsum[:,config.FRAME_STEP_FEATURES:],
        # frames_long_cumsum[:,0:config.FRAME_STEP_FEATURES+1])
        m_tau = np.subtract(frames_long_cumsum[:, win_size:], frames_long_cumsum[:, 0:tau_max + 1])
        m_tau0 = m_tau[:, 0].reshape(-1, 1)
        d_tau = np.add(np.add(m_tau0, m_tau), (-2) * r_tau)
        # d_partial_sums = np.cumsum(d_tau[:,1:], axis=1)
        d_partial_sums = np.cumsum(d_tau, axis=1)

        # What about zero elements ??
        # d_tau_norm = np.divide(np.multiply(d_tau[:,1:],tau),d_partial_sums)
        np.seterr(divide='ignore', invalid='ignore')
        d_tau_norm = np.divide(np.multiply(d_tau, tau), d_partial_sums)
        d_tau_norm = np.insert(d_tau_norm, 0, 1, axis=1)
        pitch_corr = np.zeros(shape=(32, frame_num))
        pitch_loc = int(sr / 400) + np.argmin(d_tau_norm[:, int(sr / 400): int(sr / 60)], axis=1)  # 60 - 400 Hz

        th_condition = np.zeros(shape=pitch_loc.shape)

        for i in range(frame_num):

            th_condition[i] = d_tau_norm[i, pitch_loc[i]]
            # if d_tau_norm[i, pitch_loc[i]] < threshold:
            if True:  # d_tau_norm[i, pitch_loc[i]] < threshold:
                pitch_vec[i] = PitchDetection.pitch_parabolic_estimation(pitch_loc[i], d_tau_norm[i, :])
                    
                idx = 1
                pitch_corr[0, i] = pitch_loc[i]
                frame_dot = np.dot(frames[i, :], frames[i, :])
                for shift in range(pitch_loc[i]+min_shift, pitch_loc[i]+max_shift):
                    if shift > 160:
                        continue
                    else:                     
                        norm = np.sqrt(frame_dot * np.dot(frames_long[i, shift:shift+PATCfg.FRAME_SIZE_FEATURES],
                                                          frames_long[i, shift:shift+PATCfg.FRAME_SIZE_FEATURES]))
                        pitch_corr[idx, i] = r_tau[i, shift] / norm
                        
                        idx += 1

            else:
                pitch_vec[i] = 0

        return pitch_vec, frame_num, frames, frames_long, r_tau, pitch_corr, th_condition


if __name__ == '__main__':

    import librosa as lb
    import matplotlib.pyplot as plt
    y = lb.load('C:/BVC/BVCortex/MATLAB/audio/1/A1.wav', sr=None)[0]
    cur_logger = logging.getLogger('PitchStandAlone')
    w = PATCfg.FRAME_SIZE_FEATURES
    h = PATCfg.FRAME_STEP_FEATURES
    th = PATCfg.D_NORM_THRESHOLD
    cur_logger.info('Pitch detection win_size = {}; hop_size = {}; th = {}'.format(w, h, th))
    p, a, b, c = PitchDetection.static_calc_pitch(y)[0]

    plt.plot(p)
    plt.show()
