import numpy as np
import logging
# Version number
VERSION = '5.5.0.5'

OUTPUT_FOLDER = 'C:/BVC/BVC_CORE_OUTPUT/' + VERSION + '/'
# General parameters
SAMPLING_RATE = 8000
PRE_EMPHASIS = 0  # 0-1; 0 for no PE
VAD_THRESHOLD = 10.0  # In percents
STD_OUT_LEVEL = logging.INFO
PITCH_NEEDED_FUTURE_FRAMES = 2
MINIMUM_NUMBER_OF_FRAMES_PAT_ANALISYS = 1

SKIP_HLD_SHORTCUTS = False  # in batch predict, don't use pre-saved HLD and batch matrix

REMOVE_NAN_FRAMES = False  # PATSignalProcessor, remove_extra_features - deals with nan in pitch etc.

# Machine learning feature parameters
USE_PSD_FEATURES = 1  # 0 amp; 1 power

ENGINE_CFGs = {'1': {'name': 'D4_40_orig', 'PAT': 40, 'OP': 'D4', 'DO_DD': False, 'DERIV': True, 'SMILE': False},
               '1old': {'name': 'D4_40_orig', 'PAT': 40, 'OP': 'D4', 'DO_DD': False, 'DERIV': True, 'HLD': 'orig'},
               '12': {'name': 'D4_100_der', 'PAT': 100, 'OP': 'D4', 'DO_DD': False, 'DERIV': True, 'HLD': 'orig',
                      'SMILE': False},

               # v5.5.0.1 - vitality only, used for Amazon demo 3/2019
               '17': {'PAT': 100, 'OP': 'M8', 'DO_DD': False, 'DERIV': True, 'SMILE': True, 'HLD': 'orig',
                      'MAT_SIZE': 534, 'ANALYZE_TAV': False, 'ANALYZE_VITALITY': True},

               '61': {'PAT': 100, 'OP': 'M5', 'DO_DD': False, 'DERIV': True, 'HLD': 'new', 'SMILE': False},
               # v5.5.0.2 - detectors only, starkey 3/2019
               '61b': {'PAT': 100, 'OP': 'M5', 'DO_DD': False, 'DERIV': False, 'HLD': 'new', 'SMILE': False,
                       'MAT_SIZE': 166, 'ANALYZE_TAV': True, 'ANALYZE_VITALITY': False},
               '61c': {'PAT': 100, 'OP': 'M5', 'DO_DD': False, 'DERIV': False, 'HLD': 'orig', 'SMILE': False},
               '61d': {'PAT': 100, 'OP': 'M5', 'DO_DD': False, 'DERIV': True, 'HLD': 'orig', 'SMILE': False},

               '62': {'PAT': 100, 'OP': 'D5', 'DO_DD': False, 'DERIV': True, 'HLD': 'new', 'SMILE': False},
               '62b': {'PAT': 100, 'OP': 'D5', 'DO_DD': False, 'DERIV': False, 'HLD': 'new', 'SMILE': False},
               '62c': {'PAT': 100, 'OP': 'D5', 'DO_DD': False, 'DERIV': False, 'HLD': 'orig', 'SMILE': False},
               '62d': {'PAT': 100, 'OP': 'D5', 'DO_DD': False, 'DERIV': True, 'HLD': 'orig', 'SMILE': False},

               '63': {'PAT': 100, 'OP': 'N5', 'DO_DD': False, 'DERIV': True, 'HLD': 'new', 'SMILE': False,
                      'MAT_SIZE': 166, 'ANALYZE_TAV': True, 'ANALYZE_VITALITY': False},
               '63b': {'PAT': 100, 'OP': 'N5', 'DO_DD': False, 'DERIV': False, 'HLD': 'new', 'SMILE': False,
                       'MAT_SIZE': 166, 'ANALYZE_TAV': True, 'ANALYZE_VITALITY': False},
               '63c': {'PAT': 100, 'OP': 'N5', 'DO_DD': False, 'DERIV': False, 'HLD': 'orig', 'SMILE': False},
               '63d': {'PAT': 100, 'OP': 'N5', 'DO_DD': False, 'DERIV': True, 'HLD': 'orig', 'SMILE': False},

               '64': {'PAT': 100, 'OP': 'N4', 'DO_DD': False, 'DERIV': True, 'HLD': 'new', 'SMILE': False,
                      'MAT_SIZE': 134, 'ANALYZE_TAV': True, 'ANALYZE_VITALITY': False}}

# ENGINE_CONF = ENGINE_CFGs['61b']  # 12 is old, 61b for new v5.5, 17 for vitality & smile
ENGINE_CONF = ENGINE_CFGs['64']

DEMO_PREFIX = 'C:/'

ENGINE_CONF['name'] = ENGINE_CONF['OP'] + '_' + str(ENGINE_CONF['PAT'])
if ENGINE_CONF['DERIV']:
    ENGINE_CONF['name'] = ENGINE_CONF['name'] + '_der'
if ENGINE_CONF['HLD'] == 'new':
    ENGINE_CONF['name'] = ENGINE_CONF['name'] + '_new'
if ENGINE_CONF['SMILE']:
    ENGINE_CONF['name'] = ENGINE_CONF['name'] + '_smile'

if ENGINE_CONF['PAT'] == 40:
    FRAME_SIZE_FEATURES_SEC = 2*0.032  # 0.032
    FRAME_STEP_FEATURES_SEC = 0.025  # for step 200
else:
    FRAME_SIZE_FEATURES_SEC = 0.032  # 0.032
    FRAME_STEP_FEATURES_SEC = 0.01  # 0.025  # for step 200

FRAME_SIZE_FEATURES = int(SAMPLING_RATE * FRAME_SIZE_FEATURES_SEC)
FRAME_STEP_FEATURES = int(SAMPLING_RATE * FRAME_STEP_FEATURES_SEC)
MEL_BANDS_NUM = 26
MFCC_NUM = 13

LLD_OUTPUT = ['mfcc', 'loudness', 'pitch', 'ACpitch', 'pitch_th_cond']
if ENGINE_CONF['SMILE']:
    LLD_OUTPUT = np.append(LLD_OUTPUT, 'smile')
LLD_BASE_DIMENSION = {'mfcc': MFCC_NUM, 'loudness': 1, 'pitch': 1, 'ACpitch': 1, 'smile': 1}

TAU_MAX = FRAME_STEP_FEATURES * 2
D_NORM_THRESHOLD = 0.4
PITCH_MIN_SHIFT = -5
PITCH_MAX_SHIFT = 25

VAD_FILT_SEC = 0.5

dev = './tools/'
