from PATrends import PATConfig as PATCfg
from PATrends.FeatureGenerator import FeatureGenerator
from PATrends.PATManager import PATManager
from PATrends.PitchDetection import PitchDetection
import numpy as np

import scipy
import os
import time
if PATCfg.ENGINE_CONF['SMILE']:
    from PATrends import wrapper4SMILE as Smile


class PATSignalProcessor:
    def __init__(self):
        self.FG = FeatureGenerator()
        self.Pitch = PitchDetection()

    def process_chunk(self, signal_chunk, signal_chunk_ext):

        # mel, mfcc, fft, fft_spec, loudness
        lld_dict = self.FG.process_chunk(signal_chunk, force_params=None)

        # pitch, ACpitch, LogPitch - unused, calculated @hld
        pitch_dict = self.Pitch.calc_pitch_per_frame(signal_chunk_ext)
        lld_dict.update(pitch_dict)

        if PATCfg.ENGINE_CONF['SMILE']:
            smile_dict, failed = self.calculate_os_lld(signal_chunk)
            if failed:
                print('Problem analyzing signal (os)')  # TODO: check that this is written to log with the filename
                return None
            lld_dict.update(smile_dict)

        return lld_dict

    @staticmethod
    def calculate_os_lld(signal_chunk):
        temp_file = 'temp_' + str(time.time()) + '.wav'
        signal_chunk_16 = (32768 * signal_chunk).astype(np.int16)  # TODO: add check that chunk format is float32
        scipy.io.wavfile.write(temp_file, PATCfg.SAMPLING_RATE, signal_chunk_16)  # TODO: this shouldn't happen in 'online' mode!
        failed, lld_os, lld_t_os, lld_name_os = Smile.extract_lld(temp_file)
        os.remove(temp_file)

        return {'smile': lld_os.T}, failed

    @staticmethod
    def get_column_dict(dict_matrix):
        column_dict = {}
        for key in dict_matrix.keys():
            columns = []
            if dict_matrix[key].shape[0] > 1:
                for i in range(dict_matrix[key].shape[0]):
                    columns.append(key + str(i))
            else:
                columns.append(key)

            column_dict.update({key: columns})
        return column_dict

    @staticmethod
    def remove_extra_features(lld_complete):
        lld_dict = {}
        for feature_name in PATCfg.LLD_OUTPUT:
            if PATCfg.REMOVE_NAN_FRAMES:
                nan_index = np.logical_not(np.isnan(lld_complete['pitch']))[0]
                lld_dict[feature_name] = lld_complete[feature_name][:, nan_index]
            else:
                lld_dict[feature_name] = lld_complete[feature_name]

        return lld_dict

    @staticmethod
    def get_title_list():
        titles = []
        for lld in PATCfg.LLD_OUTPUT:
            if PATCfg.LLD_BASE_DIMENSION[lld] == 1:
                titles.append(str(lld))
            else:
                for lld_dim in range(PATCfg.LLD_BASE_DIMENSION[lld]):
                    titles.append(str(lld) + str(lld_dim))
        return titles

    @staticmethod
    def static_main(signal, frame_size=None, frame_step=None):
        if not frame_size:
            frame_size = PATCfg.FRAME_SIZE_FEATURES

        if not frame_step:
            frame_step = PATCfg.FRAME_STEP_FEATURES

        if len(signal):

            processor = PATSignalProcessor()
            manager = PATManager

            p, p_ext = manager.calc_num_of_frames(len(signal), frame_size, frame_step)
            sig_to_analyze, residual = np.split(signal, [(p - 1) * frame_step + frame_size])
            sig_to_analyze_ext = signal[:(p_ext - 1) * frame_step + frame_size]

            lld_dict = processor.process_chunk(sig_to_analyze, sig_to_analyze_ext)

        else:
            lld_dict = []

        return PATSignalProcessor.remove_extra_features(lld_dict), PATSignalProcessor.get_title_list()
