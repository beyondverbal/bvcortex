import json
import time
import random
import multiprocessing
import psutil
import logging

import zmq
import asyncio
import zmq.asyncio

zmq.asyncio.install()

import engine_worker
import engine_settings as settings

# Lior

CPU_COUNT = multiprocessing.cpu_count()
WORKERS_COUNT = CPU_COUNT * settings.WORKERS_CPU_MULTIPLICATOR

workers = []


def setup_worker(i):
    # creating non-blocking full duplex pipe between manager and worker
    parent_conn, child_conn = multiprocessing.Pipe(duplex=True)

    # creating worker
    p_port = settings.CLIENTS_PORT + i
    affinity = i % CPU_COUNT
    p = multiprocessing.Process(target=engine_worker.worker, args=(child_conn, i, p_port, affinity))
    p.conn = parent_conn
    p.port = p_port
    p.loadavg = 0
    return p


async def main_loop():
    # starting zeromq server
    # context = zmq.Context()
    context = zmq.asyncio.Context()

    # control channel, keep-alive responder
    control_sock = context.socket(zmq.REP)
    control_sock.bind("tcp://*:%d" % settings.CONTROL_PORT)

    poller = zmq.asyncio.Poller()
    poller.register(control_sock, zmq.POLLIN)

    stat_proc = psutil.Process()

    # main loop
    engine_running = True
    while engine_running:
        try:
            socks = dict(await poller.poll(timeout=1))

            # control and keep-alive
            if socks.get(control_sock) == zmq.POLLIN:
                # ignore the message for now
                # later - this channel can be used to control the engine
                msg = await control_sock.recv_json()
                if msg['cmd'] == 'stop':
                    print("Stopping")
                    control_sock.send_json({'stop': True})
                    for p in workers:
                        if p.is_alive():
                            p.conn.send({'cmd': 'exit'})
                    engine_running = False
                elif msg['cmd'] == 'keepalive':
                    # reply heart-beat
                    control_sock.send_json({'live': True})
                elif msg['cmd'] == 'adress':
                    adress = msg['connection']
                    workers.append(adress)
                elif msg['cmd'] == 'newsession':
                    # map to worker with lowest load
                    p = min(workers, key=lambda x: x.loadavg)
                    print("New session: %s, port %d" % (msg['id'], p.port))
                    control_sock.send_json({'server': 'localhost', 'port': p.port})
                    p.loadavg += 1
                elif msg['cmd'] == 'cpustats':
                    cpu_percent = stat_proc.cpu_percent()
                    print('CPU Main: %.2f%%' % cpu_percent)
                    # threads_list = stat_proc.threads()
                    # for th in threads_list:
                    #     #cpu_perc = psutil.Process(th.id).cpu_percent(interval=.1)
                    #     print('PID %s: User = %f System = %f' % (th.id, th.user_time, th.system_time))
                    await control_sock.send_json({'cpustats': '%.2f' % cpu_percent})
                    # for p in workers:
                    #    if p.is_alive():
                    #        p.conn.send({'cmd': 'cpustats'})
                # elif msg['cmd'] == 'endsession':
                # print('endsession')
                # print(msg)
            # check for workers messages, non-blocking
            for i in range(WORKERS_COUNT):
                p = workers[i]
                if p.conn:
                    # message arrived? send back to client
                    if p.conn.poll(1):
                        msg = p.conn.recv()
                        print("from worker", i, msg)

                        # update worker load
                        if 'loadavg' in msg:
                            p.loadavg = msg['loadavg']
                        # if msg['reply']:
                        #    msg_out = bytes(json.dumps(msg['msg']), "utf-8")
                        #    client_sock.send_multipart([msg['client_id'], msg_out])

                    if not p.is_alive():
                        logging.debug("Worker %d is dead, restarting" % i)
                        p.conn.close()
                        p.terminate()

                        # setup new worker
                        p = setup_worker(i)
                        workers[i] = p
                        p.start()

            # idle
            # time.sleep(.001)

        except Exception as e:
            logging.exception("Manager loop")

            # clean up
    control_sock.close()


if __name__ == '__main__':
    print("Creating %d workers" % WORKERS_COUNT)

    for i in range(WORKERS_COUNT):
        p = setup_worker(i)
        workers.append(p)
        # starting worker
        p.start()

    loop = zmq.asyncio.ZMQEventLoop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(main_loop())

