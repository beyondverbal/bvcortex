import numpy as np

from sklearn.model_selection import StratifiedKFold
try:
    from sklearn.model_selection import RepeatedStratifiedKFold
    use_repeated = True
except:
    print('ImportError: cannot import name RepeatedStratifiedKFold')
    use_repeated = False

from CommonML.GroupStratifiedShuffleSplit import GroupStratifiedShuffleSplit


def CreateFolds(X_train, MD, Y_group, Y_clustered, Y_train, FoldsConf, seed=0):
    n_folds = FoldsConf['n_folds']
    n_repeats = FoldsConf['n_repeats']
    permutation_num = FoldsConf['permutation_num']

    if len(Y_group)==0:
        Y_group = np.arange(len(Y_train))
    if len(Y_clustered)==0:
        Y_clustered = Y_train

    if 'fold_id' in MD.columns:
        folds = []
        for f in np.unique(MD.fold_id.values):
            train_idx = MD[MD.fold_id != f].index.values
            test_idx = MD[MD.fold_id == f].index.values
            folds.append((train_idx, test_idx))
        fold_num = len(np.unique(MD.fold_id.values))
        if fold_num != n_folds:
            n_folds = fold_num
            print('Overriding self.n_folds; setting to',fold_num)

    elif len(Y_group)==0 or len(np.unique(Y_group))==len(Y_train):
        if use_repeated:
            skf = RepeatedStratifiedKFold(n_folds, n_repeats=n_repeats, random_state=0)
        else:
            n_repeats = 1  # override config
            skf = StratifiedKFold(n_folds)
        folds = skf.split(X_train, Y_clustered)
    else:
        GroupStrat = GroupStratifiedShuffleSplit(seed=seed)
        if n_repeats != 1:
            print('Overriding self.n_repeats; setting to 1')
            n_repeats = 1
        # folds = GroupStrat.StartifiedGroupKfold(Y_clustered, Y_group, n_folds, permutation_num=permutation_num)
        folds = GroupStrat.StartifiedGroupKfold(Y_clustered, Y_group, n_folds)
    return folds
