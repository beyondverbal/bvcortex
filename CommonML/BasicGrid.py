import pandas as pd
import numpy as np
import logging
from sklearn.metrics import roc_auc_score
from sklearn.metrics import classification_report

import CommonML.LinearSigmoidModel as LinearSigmoidModel
from CommonML.BasicCV import BasicCV
import logging.config
from TAVCommon import log_conf

logging.config.dictConfig(log_conf.create_log_dict_no_file(logging.DEBUG))
cur_logger = logging.getLogger('BasicGrid')
cur_logger.info('Starting Run :)')


class BasicGrid():
    def __init__(self, EBobj, mg_results, mg_titles, mg_signature, path):
        self.EBobj = EBobj
        self.mg_results = mg_results
        self.mg_titles = mg_titles
        self.mg_signature = mg_signature
        self.path = path
        self.logger = logging.getLogger('BasicGrid')

    def GridSearch(self, ModelList, RegularizationList, ClassRatioList, FeatSelectionList, PCAthresholdList, AlphaList):
        results = pd.DataFrame([])
        point_count = 1
        total_grid_points = len(ModelList)*len(RegularizationList)*len(ClassRatioList)*len(FeatSelectionList) * \
                            len(PCAthresholdList) * len(AlphaList)
        for curr_model in ModelList:
            for curr_C in RegularizationList:
                for curr_ratio in ClassRatioList:
                    for curr_f_sel in FeatSelectionList:
                        for curr_pca in PCAthresholdList:
                            for alpha in AlphaList:
                                self.logger.info('**** Meta Grid point ' + self.mg_signature +
                                                 ' - Basic Grid point {}/{} m:{} C:{} R:{} f_sel:{} pca:{} ****'.
                                                 format(point_count, total_grid_points, curr_model, curr_C, curr_ratio,
                                                        curr_f_sel, curr_pca))
                                PipelineConf = {
                                    'model_key': curr_model,
                                    'regularization': curr_C,
                                    'class_ratio': curr_ratio,
                                    'feat_sel': curr_f_sel,
                                    'pca': curr_pca,
                                    'alpha': alpha
                                }
                                # try:
                                curr_result = self.GridPoint(PipelineConf)
                                results = results.append(curr_result)
                                # except:
                                #     self.logger.warning('Exception From Grid Point!!!')

                                point_count += 1
        return results

    def GridPoint(self, PipelineConf):
        EBobj = self.EBobj
        gs_result = self.mg_results + [PipelineConf['model_key'], PipelineConf['regularization'],
                                       PipelineConf['class_ratio'], PipelineConf['feat_sel'],
                                       PipelineConf['pca'], PipelineConf['alpha']]
        gs_titles = self.mg_titles + ['model_type', 'regular_C', 'class_ratio', 'feat_selection', 'pca', 'alpha']

        # Cross Validation
        CVobj = BasicCV(PipelineConf, EBobj.FoldsConf)
        Folds = EBobj.Folds()
        CV_recall, CV_info = CVobj.perform_CV(EBobj.X, EBobj.Y, EBobj.W, EBobj.MD, Folds, path=self.path,
            output_columns=EBobj.output_columns, subset_by=EBobj.subset_by)

        auc_target = roc_auc_score(CV_info['TrueLabel'].values, CV_info['score'].values, average='macro')

        resultX, titlesX = EBobj.EvaluateSubset(CV_info, 'cv_')
        gs_result += CV_recall + [auc_target] + resultX
        gs_titles += ['CV_specificity', 'CV_sensitivity', 'auc_target'] + titlesX

        # Evaluate on Training set
        x_feat_sel = EBobj.FBobj.select_features(EBobj.X, EBobj.feature_ranks, feat_sel_th=PipelineConf['feat_sel'])

        alpha = PipelineConf['alpha']
        LinSigObj = LinearSigmoidModel.LinSig(PipelineConf, x_feat_sel, EBobj.Y, EBobj.W, path=self.path, alpha=alpha)
        LinSigObj.save_model(EBobj.GetCurrentFeatures(), path=self.path)

        file_name = self.path+'train_report.csv'
        open(file_name, 'w', newline='')

        # TODO: redundant score calculations?
        predictions, score, DistNormed, ScoreNormed, highQ, rescaled2skew = LinSigObj.predict(x_feat_sel, EBobj.Y, file_name=file_name)
        result = LinearSigmoidModel.calc_metrics(LinSigObj.Lin.classes_, EBobj.Y, predictions)

        gs_result += [result['recall'][0], result['recall'][1], result['count'][0], result['count'][1]]
        gs_titles += ['train_specificity', 'train_sensitivity', 'train_false_count', 'train_true_count']
        self.logger.debug('X_train.shape = {}'.format(x_feat_sel.shape))

        # print('Classification report at EER for sample'+subset)
        eer_predict = LinearSigmoidModel.predict_by_threshold(DistNormed, 0.5)
        print(classification_report(EBobj.Y, eer_predict))

        # Evaluate on Holdout sets
        for subset in EBobj.get_validation_sets():
            X, y, info_df = EBobj.SubsetData(subset)

            x_subset = EBobj.FBobj.select_features(X, EBobj.feature_ranks, feat_sel_th=PipelineConf['feat_sel'])

            result, predictions = LinSigObj.calc_stats_on_fold(x_subset, y)
            result = LinearSigmoidModel.calc_metrics(LinSigObj.Lin.classes_, y, predictions)
            output = LinSigObj.subset_info(x_subset, info_df, columns=EBobj.output_columns)

            output.to_csv(self.path + subset + '_info.csv', index=False)
            resultX, titlesX = EBobj.EvaluateSubset(output, subset + '_')

            gs_result += [result['recall'][0], result['recall'][1], result['count'][0], result['count'][1]] + resultX
            gs_titles += [subset+'_specificity', subset+'_sensitivity', subset+'_False_Count', subset+'_True_Count'] + titlesX

        gs_result = pd.DataFrame(np.array(gs_result).reshape(1, len(gs_result)), columns=gs_titles)

        return gs_result
        # YL: in this code config is not supported... yet, this feature ranking should be revived one day...
        # feature_weights = pd.DataFrame()
        # if not cfg_train.USE_PCA:
        #
        #     if cfg_train.USE_FEATURE_LIST:
        #         feature_df = pd.read_csv(cfg_train.FEATURE_RANK_PATH)
        #         feature_names = feature_df[feature_df['corr'] >= feature_threshold].feature.values
        #
        #     a, b = LinSigObj.get_linear_ab()
        #     feature_weights = [PipelineConf['regularization']] + list(a.flatten())
        #     feature_weights = pd.DataFrame(np.array(feature_weights).reshape(1, len(feature_weights)), columns=['regular_C']+list(feature_names))
        #
        # return {'grid_search': gs_result, 'feature_weights': feature_weights}
        #

