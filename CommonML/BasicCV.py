import numpy as np
import pandas as pd
from sklearn.metrics import classification_report
import CommonML.LinearSigmoidModel as LinearSigmoidModel
import CommonML.ResultsLog as ResultsLog
import logging


class BasicCV():
    def __init__(self, PipelineConf, FoldsConf):

        self.PipelineConf = PipelineConf

        self.n_folds = FoldsConf['n_folds']
        self.n_repeats = FoldsConf['n_repeats']
        self.permutation_num = FoldsConf['permutation_num']
        self.logger = logging.getLogger('BasicCV')

    def perform_CV(self, X, Y, W, MD, Folds, filename='CV_report.csv', path='', output_columns=[], subset_by=[]):

        results_CV, results_test, CV_info = self.train_on_kfold(X, Y, W, MD, Folds, output_columns=output_columns,
                                                                path=path, alpha=self.PipelineConf['alpha'])
        mean_CV, std_CV = self.calc_folds_mean_std(results_CV, 'train', Y)

        # Tables with +/- sign
        file_name = path + filename
        open(file_name, 'w', newline='')

        ResultsLog.save_to_csv_pm(mean_CV, std_CV, 'CV', file_name)

        model_classes = results_CV[0]['labels']

        CV_info.loc[:, 'TrueLabel'] = CV_info.TrueLabel.values.astype(int)
        CV_info.loc[:, 'PredictedLabel'] = CV_info.PredictedLabel.values.astype(int)
        self.calc_train_subset_stats(CV_info, model_classes, subset_by, path=path)

        # todo: revive
        # eer_predict, eer_thresh = LinearSigmoidModel.predict_at_EER(CV_info['TrueLabel'].values, CV_info['biomarker'].values)
        #
        # self.logger.debug('CV EER:')
        # self.logger.debug(classification_report(CV_info['TrueLabel'].values, eer_predict))

        return [mean_CV['recall'][0], mean_CV['recall'][1]], CV_info

    def train_on_kfold(self, X, Y, W, MD, Folds, output_columns=[], path='', alpha=1.):

        CV_info = pd.DataFrame(np.zeros([0, len(output_columns)]), columns=output_columns)

        counter = 0
        report_data_CV = []
        report_data_test = []
        features_left = 0

        # Folds is now a tuple, [0] is folds, [1] is feature ranking via auc
        roc_auc_x = Folds[1]

        th = self.PipelineConf['feat_sel']

        for i, (train_index, test_index) in enumerate(Folds[0]):
            self.logger.debug('Training on fold #{}.'.format(counter))
            # print('Training on fold #{}.'.format(counter))
            x_train_CV, x_test_CV = X[train_index], X[test_index]
            y_train_CV, y_test_CV = Y[train_index], Y[test_index]

            if 0 < th <= 1:
                x_train_CV = x_train_CV[:, np.array(roc_auc_x[i]) > th]
                x_test_CV = x_test_CV[:, np.array(roc_auc_x[i]) > th]
                features_left += (np.array(roc_auc_x[i]) > th).sum()
            elif th > 1:
                s = np.argsort(roc_auc_x[i])[-th:]
                x_train_CV = x_train_CV[:, s]
                x_test_CV = x_test_CV[:, s]
                features_left += th

            # TODO: redoing the weights needed?
            if len(W)==0:
                k_weights = [1] * len(train_index)
            else:
                k_weights = W[train_index]

            LinSigObj = LinearSigmoidModel.LinSig(self.PipelineConf, x_train_CV, y_train_CV, k_weights, alpha=alpha)
            # TODO: add protection for when fold don't have all classes (rare cases in tav for example)
            result, predictions = LinSigObj.calc_stats_on_fold(x_test_CV, y_test_CV)

            # YS TODO - predict on EER
            predictions, score, DistNormed, ScoreNormed, highQ, rescaled2skew = LinSigObj.predict(x_test_CV, y_test_CV)
            predictions_eer, th_eer = LinearSigmoidModel.predict_at_EER(y_test_CV, DistNormed)
            result = LinearSigmoidModel.calc_metrics(np.unique(y_test_CV), y_test_CV, predictions_eer)

            report_data_CV.append(result)

            if len(MD) != 0:
                Y_info_fold = MD.iloc[test_index, :].reset_index(drop=True)
                output = LinSigObj.subset_info(x_test_CV, Y_info_fold, columns=output_columns)
                CV_info = pd.concat([CV_info, output], ignore_index=True)
            counter += 1

        CV_info.to_csv(path+'CV_info.csv', index=False)


        return report_data_CV, report_data_test, CV_info

    def calc_folds_mean_std(self, results, set_type, Y):
        accuracy = np.array([])
        precision = np.array([])
        recall = np.array([])
        f1 = np.array([])
        labels = results[0]['labels']
        for result in results:
            accuracy = np.append(accuracy,result['accuracy'])
            precision = np.append(precision,result['precision'])
            recall = np.append(recall,result['recall'])
            f1 = np.append(f1,result['f1'])

        precision = precision.reshape(self.n_folds*self.n_repeats,len(labels)+1)
        recall = recall.reshape(self.n_folds*self.n_repeats,len(labels)+1)
        f1 = f1.reshape(self.n_folds*self.n_repeats,len(labels)+1)

        accuracy_mean = np.mean(accuracy)
        precision_mean = np.mean(precision, axis=0)
        recall_mean = np.mean(recall, axis=0)
        f1_mean = np.mean(f1, axis=0)

        accuracy_std = np.std(accuracy)
        precision_std = np.std(precision, axis=0)
        recall_std = np.std(recall, axis=0)
        f1_std = np.std(f1, axis=0)

        label_count = np.unique(Y,return_counts=True)
        count_dict = dict(zip(label_count[0], label_count[1]))
        count = []
        for idx in range(len(labels)):
            if labels[idx] in count_dict.keys():
                count.append(count_dict[labels[idx]])
            else:
                count.append(0)

        result_mean = {'accuracy':accuracy_mean , 'labels':labels, 'precision':precision_mean, 'recall':recall_mean, 'f1':f1_mean, 'count':count}
        results_std = {'accuracy':accuracy_std , 'labels':labels, 'precision':precision_std, 'recall':recall_std, 'f1':f1_std, 'count':count}
        return result_mean, results_std


    def calc_train_subset_stats(self, dataset, classes, subset_by, path=''):

        file_name = path+'subset_report.csv'  # TO DO: Add path (to all csv outputs in general)
        open(file_name, 'w', newline='')

        for subset in subset_by:
            sources = np.unique(dataset[subset].values)
            for source in sources:
                source_data = dataset.loc[dataset[subset]==source]
                labeled = source_data['TrueLabel'].values
                predicted = source_data['PredictedLabel'].values

                result = LinearSigmoidModel.calc_metrics(classes, labeled, predicted)
                ResultsLog.save_to_csv(result, file_name, source)

