import numpy as np
import pandas as pd
from sklearn.model_selection import GroupShuffleSplit
from sklearn.preprocessing import LabelEncoder
from sklearn.utils import shuffle
from numpy.random import RandomState
from sklearn.model_selection import GroupKFold

class GroupStratifiedShuffleSplit:

    def __init__(self, n_splits=3, test_size=0.2, seed=0):
        self.n_splits = n_splits
        self.test_size = test_size
        self.cv = GroupShuffleSplit(n_splits=100, test_size=self.test_size, random_state=seed)

    def split(self, X, y, groups=None):

        train_index = []
        test_index = []
        unbalance_meas = []
        y = LabelEncoder().fit_transform(y)
        for curr_train_index, curr_test_index in self.cv.split(X, y, groups=groups):
            prop_train = np.bincount(y[curr_train_index])/len(curr_train_index)
            prop_test = np.bincount(y[curr_test_index])/len(curr_test_index)
            unbalance_meas.append(np.sum(np.abs(prop_train - prop_test)))
            train_index.append(curr_train_index)
            test_index.append(curr_test_index)
        sorted_meas_ind = np.argsort(unbalance_meas)
        index_tuples = []
        for i in range(self.n_splits):
            index_tuples.append((train_index[sorted_meas_ind[i]], test_index[sorted_meas_ind[i]]))

        return iter(index_tuples)

    # obsolete
    @staticmethod
    def StartifiedGroupKfold(y, groups, n_splits, permutation_num=20, seed=0):

        ix = np.array(range(len(groups)))
        unique_groups = np.unique(groups)

        all_splits = []
        unbalance_meas = []
        y = LabelEncoder().fit_transform(y)
        prop_y = np.bincount(y)/len(y)

        for random_state in range(seed*permutation_num,seed*permutation_num+permutation_num):
            prng = RandomState(random_state)
            prng.shuffle(unique_groups)
            splits = np.array_split(unique_groups, n_splits)
            all_splits.append(splits)

            splits_deviation = 0
            for split in splits:
                mask = [el in split for el in groups]
                # fold = ix[mask]
                fold = ix[np.array(mask)]  # TODO WTF!!!

                prop_split = np.bincount(y[fold])/len(fold)
                if len(prop_split) == len(prop_y):
                    splits_deviation += np.sum(np.abs(prop_y - prop_split))
                else:
                    print('bad split')
                    splits_deviation += len(prop_y)
            unbalance_meas.append(splits_deviation)
        sorted_meas_ind = np.argsort(unbalance_meas)

        # Get the winning-split fold-indices
        kfolds = []
        for split in all_splits[sorted_meas_ind[0]]:
            mask = [el in split for el in groups]
            # test = ix[mask]
            test = ix[np.array(mask)]
            # test = ix[ix[mask]==True]
            train = [x for x in ix if x not in test]
            kfolds.append([train,test])

        return kfolds

    @staticmethod
    def StartifiedGroupKfold_new(y_cluster, y_group, n_splits):
        # stratify by target type (cluster), group by y_group (samples per id for example)

        df = pd.DataFrame()
        df = df.assign(y_cluster=y_cluster)
        df = df.assign(y_group=y_group)
        df = df.sample(frac=1)  # shuffle but keep the index

        gkf = GroupKFold(n_splits=n_splits)

        kfolds = []
        for i in range(n_splits):
            kfolds.append([[],[]])

        for cluster in np.unique(y_cluster):
            cluster_idx = df[df.y_cluster==cluster].index
            y = df.loc[cluster_idx,'y_group'].values
            fold_i = 0
            for curr_train_index, curr_test_index in gkf.split(cluster_idx, groups=y):
                kfolds[fold_i][0] += list(cluster_idx[curr_train_index])
                kfolds[fold_i][1] += list(cluster_idx[curr_test_index])
                fold_i += 1

        # test_list = []
        # for fold_i in range(n_splits):
        #     df.at[kfolds[fold_i][1], 'fold'] = fold_i
        #
        #     df_fold = np.array(y_cluster)[kfolds[fold_i][1]]
        #     counts = np.unique(df_fold, return_counts=True)[1]
        #     sum = np.sum(counts)
        #     counts = counts/sum
        #     print('fold {}: size={}, ratios={}'.format(fold_i, len(kfolds[fold_i][1]), counts))
        #
        #     test_list += list(np.unique(np.array(y_group)[kfolds[fold_i][1]]))
        #
        # print('len(test_list) = {}'.format(len(test_list)))
        # df.to_csv('debug.csv')

        del df
        return kfolds


if __name__ == "__main__":

    from random import randint

    # Mock data for testing
    n_splits = 10
    patient_id = np.arange(0,1000)
    y_cluster = []
    y_group = []
    for pat in patient_id:
        y_id = np.random.choice([1,2,3,4], p=[0.2,0.4,0.25,0.15])
        repeated = randint(1,8)
        for i in range(repeated+1):
            y_cluster.append(y_id)
            y_group.append(pat)

    GroupStratifiedShuffleSplit.StartifiedGroupKfold(y_cluster, y_group, n_splits)