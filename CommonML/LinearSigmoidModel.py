import numpy as np
import pandas as pd

from scipy.optimize import brentq
from scipy.optimize.minpack import curve_fit
from scipy.interpolate import interp1d
from scipy.special import erfinv

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline
from sklearn.externals import joblib

from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score

import logging
import pickle
import matplotlib.pyplot as plt

from CommonML.WeightedLogisticRegression import WeightedLogisticRegression
from CommonML.WeightedLinearSVC import WeightedLinearSVC
from CommonML.WeightedLinearSVC import WeightedSVC

import CommonML.ResultsLog as ResultsLog


def func_sigmoid(x, x0, k):
    y = 1 / (1 + np.exp(-k * (x - x0)))
    return y


class LinSig():
    def __init__(self, PipelineConf, X, Y, W, path=None, alpha=1.):
        self.PipelineConf = PipelineConf
        class_weight = self.apply_class_ratio(np.unique(Y), PipelineConf['class_ratio'])
        W, class_weight = self.normalize_weights(Y, W, class_weight)

        model_dict = {'Logistic': WeightedLogisticRegression(class_weight=class_weight, sample_weight=W, C=PipelineConf['regularization']),
                      'SVC': WeightedSVC(class_weight=class_weight, sample_weight=W, C=PipelineConf['regularization']),  # with linear kernel
                      'LinearSVC': WeightedLinearSVC(class_weight=class_weight, sample_weight=W, C=PipelineConf['regularization'])  # bugged - sample weights are ignored by sklearn
                      }

        # create sklearn pipeline
        estimators = []
        estimators.append(('standardize', StandardScaler()))
        if PipelineConf['pca'] > 0:
            estimators.append(('pca', PCA(n_components=PipelineConf['pca'], svd_solver='full')))
            if False:
                estimators.append(('pca_norm', StandardScaler()))
        estimators.append(('lin', model_dict[PipelineConf['model_key']]))

        self.Lin = Pipeline(estimators)
        self.Lin.fit(X, Y)

        lin_dist = self.linear_dist(X)
        DistNormed, self.DistScaling = vector_scaler(lin_dist)

        self.Sig = self.fit2sigmoid(DistNormed, Y, path=path, alpha=alpha)

        self.logger = logging.getLogger('LinSig')

    def get_linear_ab(self):
        a = self.Lin.named_steps['lin'].coef_
        b = self.Lin.named_steps['lin'].intercept_
        return a, b

    def linear_dist(self, X):
        x_tranformed = self.Lin.named_steps['standardize'].transform(X)
        if 'pca' in self.Lin.named_steps.keys():
            x_tranformed = self.Lin.named_steps['pca'].transform(x_tranformed)
            if False:
                x_tranformed = self.Lin.named_steps['pca_norm'].transform(x_tranformed)

        a, b = self.get_linear_ab()
        x_value = (np.dot(a, x_tranformed.T) + b)[0]
        return x_value

    def predict(self, X, Y, file_name=None):
        predictions = self.Lin.predict(X)

        if file_name is not None:
            result = calc_metrics(self.Lin.classes_, Y, predictions)
            ResultsLog.save_to_csv(result, file_name, 'Prediction Stats')
            self.logger.debug(classification_report(Y, predictions))
            # print(classification_report(Y, predictions))

        lin_dist = self.linear_dist(X).reshape(-1, 1)

        DistNormed = self.DistScaling.transform(lin_dist)
        score = func_sigmoid(DistNormed, self.Sig[0], self.Sig[1])

        q4Id = np.percentile(score, 75)
        highQ = (np.where(score < q4Id, 0, 1))  # for q1-q3 assign 0, otherwise assign 1

        # DistNormed, sc1 = vector_scaler(lin_dist)
        ScoreNormed, sc2 = vector_scaler(score)  # todo: YL - to delete - redundant

        # todo: if proves positive, turn into a function and use after feature selection...
        rescaled2skew = erfinv(2 * score - 1)
        rescaled2skew = rescaled2skew / np.std(rescaled2skew)
        # plt.hist(rescaled2skew, 50)

        # todo: another option which did not pass - yet an option for scaling
        # argS = np.argsort(lin_dist)
        # rescaled2uniform = (np.argsort(argS) / len(lin_dist))

        return predictions, score, DistNormed, ScoreNormed, highQ, rescaled2skew

    def calc_stats_on_fold(self, x_test, y_test, file_name=''):
        predictions = self.Lin.predict(x_test)
        result = calc_metrics(self.Lin.classes_, y_test, predictions)

        if file_name != '':
            ResultsLog.save_to_csv(result, file_name, 'Prediction Stats')
            print(classification_report(y_test, predictions))

        return result, predictions


    def subset_info(self, X, MD, columns=['empty']):
        predictions, score, DistNormed, ScoreNormed, highQ, rescaled2skew = self.predict(X, MD.TrueLabel.values)

        output = pd.DataFrame(np.zeros([len(X), len(columns)]), columns=columns)

        for col in columns:
            if col == 'PredictedLabel':
                output.loc[:, 'PredictedLabel'] = predictions
            elif col == 'score':
                output.loc[:, 'score'] = score
            elif col == 'DistNormed':
                output.loc[:, 'DistNormed'] = DistNormed
            elif col == 'ScoreNormed':
                output.loc[:, 'ScoreNormed'] = ScoreNormed
            elif col=='highQ':
                output.loc[:, 'highQ'] = highQ
            elif col == 'rescaled2skew':
                output.loc[:, 'rescaled2skew'] = rescaled2skew
            else:
                try:
                    output.loc[:, col] = MD.loc[:, col].values
                except:
                    output.loc[:, col] = ['no_value'] * len(X)

        return output

    def save_model(self, feature_names, path=''):
        # joblib.dump((self.Lin, feature_names, self.Sig), path + 'model.pkl')
        #
        # # build new
        # newLin = {'standardize': self.Lin.named_steps['standardize'].transform,
        #           'lin': self.Lin.named_steps['lin'].coef_}
        # if 'pca' in self.Lin.named_steps.keys():
        #     newLin.update({'pca': self.Lin.named_steps['pca'].transform})
        # joblib.dump((newLin, feature_names, self.Sig), path + 'model_new.pkl')

        mu = np.zeros([1, len(feature_names[0])])
        mu[0, feature_names[1]] = self.Lin.named_steps['standardize'].mean_

        a = np.zeros([1, len(feature_names[0])])
        a[0, feature_names[1]] = self.Lin.named_steps['lin'].coef_ / self.Lin.named_steps['standardize'].scale_

        b = np.dot(mu, a.T)

        # mu = self.Lin.named_steps['standardize'].mean_
        #
        # mu_tot = np.zeros([1, len(feature_names[0])])
        # mu_tot[0, feature_names[1]] = mu
        #
        # # b_tot = np.dot(mu_tot, self.Lin.named_steps['lin'].coef_.T)/self.Lin.named_steps['standardize'].scale_
        #
        # b_tot = np.dot(mu_tot, a_tot.T)
        #
        # # s = self.Lin.named_steps['standardize'].scale_
        #
        # # a_tilda = self.Lin.named_steps['lin'].coef_
        #
        # a_tot = np.zeros([1, len(feature_names[0])])
        # a_tot[0, feature_names[1]] = self.Lin.named_steps['lin'].coef_/self.Lin.named_steps['standardize'].scale_

        joblib.dump((a, b, (self.Sig, self.DistScaling)), path + 'model_new_comb.pkl')

    @staticmethod
    def convert_old_model_to_new_thin_model(old_path, new_path):
        vitality_lin, feat_names, vitality_sig = joblib.load(old_path)

        new_lin = {'standardize': vitality_lin.named_steps['standardize'].transform,
                   'lin': vitality_lin.named_steps['lin'].coef_}
        if 'pca' in vitality_lin.named_steps.keys():
            new_lin.update({'pca': vitality_lin.named_steps['pca'].transform})
        with open(new_path, 'wb') as f:
            pickle.dump((new_lin, feat_names, vitality_sig), f)

    def load_model(self, path):
        self.Lin, feature_names, self.Sig = joblib.load(path + 'model.pkl')
        return self.Lin, feature_names, self.Sig

    # private methods:
    def fit2sigmoid(self, dist, Y, sample_weight=None, path=None, alpha=1.):
        false_pos_rate, true_pos_rate, thresholds = roc_curve(Y, dist, sample_weight=sample_weight, drop_intermediate=True)
        false_neg_rate = 1 - true_pos_rate
        sig_val = (false_neg_rate / (false_neg_rate + false_pos_rate)) ** alpha

        initial_guess = [0.5, 2]
        popt, pcov = curve_fit(func_sigmoid, thresholds, sig_val, p0=initial_guess)

        if path is not None:
            plt.plot(thresholds, sig_val, 'b')
            plt.plot(thresholds, func_sigmoid(thresholds, popt[0], popt[1]), 'r')
            plt.savefig(path + 'biomarker_score.png')
            plt.close()

        return popt

    def apply_class_ratio(self, class_names, class_ratio):
        if class_ratio == 1. or class_ratio <= 0:
            return None  # TODO: should maybe return weights 1 for ratio 1

        if len(class_names) != 2:
            print('class weight ratio setting only works on binary class!')
            return None

        # TODO Typo here?
        # if len(class_names) < 1.:
        #     class_weights = [class_ratio, 1.]
        # else:
        class_weights = [1., 1. / class_ratio]

        class_weight = dict(zip(class_names, class_weights))
        return class_weight

    def normalize_weights(self, y_target, sample_weights, class_weights):
        labels, counts = np.unique(y_target, return_counts=True)
        if len(labels) != 2 or class_weights is None:
            return sample_weights, class_weights

        # TODO: is the sample weight normalization redundant? for 'old' it's already N/2 for each class
        N = len(y_target)
        label0_idx = np.where(y_target == labels[0])[0]
        label1_idx = np.where(y_target == labels[1])[0]
        multi = np.zeros(N)

        sum0 = np.sum(sample_weights[label0_idx])
        factor0 = N / (2 * sum0)
        sum1 = np.sum(sample_weights[label1_idx])
        factor1 = N / (2 * sum1)

        multi[label0_idx] = factor0
        multi[label1_idx] = factor1

        new_sample_weights = sample_weights * multi

        class_factor = 2 / (class_weights[labels[0]] + class_weights[labels[1]])
        new_class_weights = {labels[0]: class_factor * class_weights[labels[0]],
                             labels[1]: class_factor * class_weights[labels[1]]}

        return new_sample_weights, new_class_weights


def calc_metrics(model_classes, y_test, predictions):
    accuracy = accuracy_score(y_test, predictions)
    precision = precision_score(y_test, predictions, average=None)
    recall = recall_score(y_test, predictions, average=None)
    f1 = f1_score(y_test, predictions, average=None)

    label_count = np.unique(y_test, return_counts=True)
    count_dict = dict(zip(label_count[0], label_count[1]))
    count = []
    for idx in range(len(model_classes)):
        try:
            count.append(count_dict[model_classes[idx]])
        except:
            # Happens if a class is missing from y_test due to low stats
            count.append(0)
            if len(precision) < 2:  # To DO: make this '2' more general
                precision = np.insert(precision, idx, 0)
                recall = np.insert(recall, idx, 0)
                f1 = np.insert(f1, idx, 0)

    precision = np.append(precision, np.mean(precision))
    recall = np.append(recall, np.mean(recall))
    f1 = np.append(f1, np.mean(f1))

    result = {'accuracy': accuracy, 'labels': model_classes, 'precision': precision, 'recall': recall, 'f1': f1, 'count': count}
    return result


def vector_scaler(x):
    scaler = StandardScaler().fit(x.reshape(-1,1))
    x_scaled = scaler.transform(x.reshape(-1,1))
    return x_scaled, scaler


def predict_at_EER(true_y, pre_thresh_value):
    false_pos_rate, true_pos_rate, thresholds = roc_curve(true_y, pre_thresh_value)
    eer = brentq(lambda x: 1. - x - interp1d(false_pos_rate, true_pos_rate)(x), 0., 1.)
    thresh = interp1d(false_pos_rate, thresholds)(eer)
    predictions = predict_by_threshold(pre_thresh_value, thresh)
    return predictions, thresh


def predict_by_threshold(pre_thresh_value, threshold):
    predictions = pre_thresh_value >= threshold
    return predictions


def plot_roc_curve(y_true, y_score, weights=None, extra_scores={}, titles={'figure': 'Receiver operating characteristic',
                                                                           'xaxis': 'False Positive Rate',
                                                                           'yaxis': 'True Positive Rate',
                                                                           'curve': 'ROC curve'}, path=''):
    # Note: y_score need to be pre-threhold value

    false_pos_rate, true_pos_rate, thresholds = roc_curve(y_true, y_score, sample_weight=weights, drop_intermediate=True)
    roc_auc = roc_auc_score(y_true, y_score, average='macro', sample_weight=weights)

    plt.figure()
    lw = 2
    plt.plot(false_pos_rate, true_pos_rate, color='darkgreen', lw=lw, label=titles['curve'] + ' (AUC = %0.2f)' % roc_auc)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])
    plt.xlabel(titles['xaxis'])
    plt.ylabel(titles['yaxis'])
    plt.title(titles['figure'])

    colors = ['red', 'orange', 'purple', 'blue', 'cyan']
    ncolor = 0
    for key, value in extra_scores.items():
        false_pos_rate, true_pos_rate, thresholds = roc_curve(y_true, value, sample_weight=weights, drop_intermediate=True)
        roc_auc = roc_auc_score(y_true, value, average='macro', sample_weight=weights)
        plt.plot(false_pos_rate, true_pos_rate, color=colors[ncolor % 5], lw=lw, label=key + ' (AUC = %0.2f)' % roc_auc)
        ncolor += 1

    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--', label='random')
    plt.legend(loc="lower right")
    plt.grid()
    plt.savefig(path + 'ROC_figure.png')
    plt.show()

if __name__ == "__main__":
    # Use this to convert old model to new

    logging.getLogger('LinSig - StandAlone')

    old_model_path = 'E:/Algo/DataExperiments/vtlt25/all/M8_181004pCHFonlyEX/model.pkl'
    new_model_path = 'E:/Algo/DataExperiments/vtlt25/all/M8_181004pCHFonlyEX/vitality_model.pkl'
    LinSig.convert_old_model_to_new_thin_model(old_model_path, new_model_path)
    logging.warning('Wrote new model for vitality')
