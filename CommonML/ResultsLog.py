import csv


def save_to_csv(result, file_name, source):
    with open(file_name, 'a', newline='') as file:
        writer = csv.writer(file, delimiter=',')
        writer.writerow([source])
        writer.writerow(['accuracy' ,result['accuracy']])
        writer.writerow(['Class' ,result['labels'][0] ,result['labels'][1]])
        writer.writerow(['F1score' ,result['f1'][0] ,result['f1'][1] ,result['f1'][2]])
        writer.writerow(['Recall' ,result['recall'][0] ,result['recall'][1] ,result['recall'][2]])
        writer.writerow(['Precision' ,result['precision'][0] ,result['precision'][1] ,result['precision'][2]])
        writer.writerow(['count' ,result['count'][0] ,result['count'][1]])

# TODO: use class keys instead of indices (to work for tav as well)
def save_to_csv_pm(mean, std, set_type, file_name):
    def round_with_err(val, err, sig=2):
        val = round(val, sig)
        err = round(err, sig)
        return val, err

    def value_error(mean, std, key, idx):
        val, err = round_with_err(mean[key][idx], std[key][idx])
        valerr = str(val) + ' ± ' + str(err)
        return valerr

    with open(file_name, 'a', newline='') as file:
        writer = csv.writer(file, delimiter=',')
        writer.writerow([set_type])
        acc_val, acc_err = round_with_err(mean['accuracy'], std['accuracy'])
        writer.writerow(['accuracy',str(acc_val) + ' ± ' + str(acc_err)])
        writer.writerow(['Class', mean['labels'][0], mean['labels'][1], 'macro'])
        writer.writerow(['F1score', value_error(mean,std,'f1',0), value_error(mean,std,'f1',1), value_error(mean,std,'f1',2)])
        writer.writerow(['Recall', value_error(mean,std,'recall',0), value_error(mean,std,'recall',1),value_error(mean,std,'recall',2)])
        writer.writerow(['Precision', value_error(mean,std,'precision',0), value_error(mean,std,'precision',1), value_error(mean,std,'precision',2)])
        writer.writerow(['count',mean['count'][0],mean['count'][1]])

