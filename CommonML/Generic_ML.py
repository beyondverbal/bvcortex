import sys
sys.path.append('../.')

import pandas
import numpy as np
import csv
import time
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE

import CommonML.LinearSigmoidModel as LinearSigmoidModel
import CommonML.ResultsLog as ResultsLog

from sklearn.feature_selection import SelectFromModel
from sklearn.linear_model import LassoCV
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA

from sklearn.metrics import explained_variance_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import median_absolute_error
from sklearn.metrics import r2_score


class ML_Base():
    def __init__(self, regularization=1., pca=0.99, use_pca=False, model_key='Logistic', split_data=False, class_ratio=1.,
                 n_folds=10, n_repeats=1, permutation_num=50):

        print('!!!!! ML_Base __init__() used !!!!!')
        self.regularization = regularization
        self.pca = pca
        self.split_data = split_data
        self.model_key = model_key
        self.class_ratio = class_ratio
        self.use_pca = use_pca
        self.self.n_repeats = n_repeats
        self.self.n_folds = n_folds
        self.permutation_num = permutation_num



    @staticmethod
    def calc_regression_metrics(y_true, y_predict, sample_weight=None):
        if not sample_weight:
            print('sample_weight not defined!')
        print('explained_variance_score =',explained_variance_score(y_true, y_predict, sample_weight=sample_weight))    # best is 1
        print('mean_absolute_error =',mean_absolute_error(y_true, y_predict, sample_weight=sample_weight))              # best is 0
        print('mean_squared_error =',mean_squared_error(y_true, y_predict, sample_weight=sample_weight))                # best is 0
        # print('mean_squared_log_error =',mean_squared_log_error(y_true, y_predict, sample_weight=sample_weight))      # best is 0
        print('median_absolute_error =',median_absolute_error(y_true, y_predict))                                       # best is 0
        print('r2_score =',r2_score(y_true, y_predict, sample_weight=sample_weight))                                    # best is 1


    @staticmethod
    def lasso_feature_selection(X_orig, y, feature_names=[], threshold=0.25, min_features=0.25, threshold_step=0.1):
        lasso_clf = LassoCV(n_jobs=-1)
        # print('Using elastic net for feature selection!!')
        # lasso_clf = ElasticNetCV(l1_ratio=[0.1,0.5,0.7,0.9,0.95,0.99,1], fit_intercept=True, normalize=False, cv=3, n_alphas=100)
        X = np.copy(X_orig)

        # int, ceil or trunk?
        feature_threshold = int(X.shape[1] * min_features)

        scaler = StandardScaler().fit(X)
        X = scaler.transform(X)

        sfm = SelectFromModel(lasso_clf, threshold=threshold)
        sfm.fit(X, y)
        n_features = sfm.transform(X).shape[1]

        while n_features > feature_threshold:
            sfm.threshold += threshold_step
            X_transform = sfm.transform(X)
            n_features = X_transform.shape[1]

        idx = sfm.get_support(indices=True)

        keep_feature_names = None
        if len(feature_names)==X.shape[1]:
            keep_feature_names = feature_names[idx]

        return idx, keep_feature_names


    @staticmethod
    def TSNE_visualize(X, Y, n_components=4, perplexity=50, init='pca', learning_rate=50, n_iter=1000):

        t0 = time.time()
        X_TSNE = TSNE(n_components=n_components, perplexity=perplexity, init=init, learning_rate=learning_rate, n_iter=n_iter, method='exact').fit_transform(X)
        #X_TSNE = TSNE_model.fit_transform(X)
        t1 = time.time()
        print('TSNE: %.2g sec' % (t1-t0))

        blue = Y==0
        red = Y==1

        # plt.scatter(X_TSNE[red, 0], X_TSNE[red, 1], c='yellow', s=8)
        # plt.scatter(X_TSNE[blue, 0], X_TSNE[blue, 1], c='darkblue', s=8)

        from sklearn.cluster import KMeans
        kmeans = KMeans(n_clusters=7, random_state=0).fit(X_TSNE)
        labels = kmeans.labels_

        colors = ['black', 'red', 'orange', 'yellow', 'blue', 'green', 'purple', 'cyan']
        for i in np.arange(7):
            idx = labels == i
            plt.scatter(X_TSNE[idx, 0], X_TSNE[idx, 1], c=colors[i], s=8, marker='o', edgecolors='face')

        plt.show()

        return labels

    @staticmethod
    def LDA_visualize(X, Y, n_components=2):

        t0 = time.time()
        lda = LDA(n_components=2)
        X_LDA = lda.fit(X,Y).transform(X)
        t1 = time.time()
        print('LDA: %.2g sec' % (t1-t0))

        blue = Y==0
        red = Y==1

        plt.hist(X_LDA[blue, 0], bins=15, color='b', histtype='step', label='Negative', normed=True)
        plt.hist(X_LDA[red, 0], bins=15, color='r', histtype='step', label='Positive', normed=True)
        plt.legend(loc='best')

        plt.show()



    @staticmethod
    def precision_curve(data, true_label_key='TrueLabel', value_key='biomarker', percenitle_num=50, perc_step=0.01, keep_csv=True, path=''):
        data = data.sort_values(value_key, ascending=False)
        data.reset_index(drop=True, inplace=True)

        true_label = data[true_label_key].values

        total_positive = len(np.where(true_label==1)[0])
        total_negative = len(np.where(true_label == 0)[0])

        # find first N percentiles
        percentiles_idx = []
        perc = perc_step
        for i in range(len(true_label)):
            curr_pos = len(np.where(true_label[:i]==1)[0])   # current positive; out of the total 1, how many 1's do we have now. DF was sorted by score previously
            curr_perc = curr_pos / total_positive
            if curr_perc >= perc:
                percentiles_idx.append(i)
                perc += perc_step

            if len(percentiles_idx)==percenitle_num:
                break

        x = np.arange(perc_step,perc_step+percenitle_num/100,perc_step)

        precision = []
        fallout = []
        for i in percentiles_idx:
            curr_true_pos = len(np.where(true_label[:i+1] == 1)[0])
            curr_precision = curr_true_pos / (i+1)
            precision.append(curr_precision)

            curr_false_pos =  len(np.where(true_label[:i+1] == 0)[0])
            curr_fallout = curr_false_pos / total_negative
            fallout.append(curr_fallout)

        if keep_csv:
            output = pandas.DataFrame(np.zeros([len(x), 3]), columns=['recall', 'precision', 'false_positive_rate'])
            output.loc[:,'recall'] = x
            output.loc[:,'precision'] = precision
            output.loc[:,'false_positive_rate'] = fallout
            output.to_csv(path+'precision_plot.csv', index=False)


        plt.figure()
        lw = 2
        plt.plot(x, precision, color='darkorange', lw=lw)
        plt.xlim([0.01, percenitle_num/100+0.02])
        # plt.ylim([0.5, 1.05])
        plt.ylim([0., 1.05])
        # plt.xlabel('% identified patients')
        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.title('')
        plt.show()




