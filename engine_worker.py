import time
import json
import psutil
import logging
import os,gc
from datetime import datetime

import numpy as np
#from zhelpers import socket_set_hwm, zpipe

import zmq
import asyncio
import zmq.asyncio
zmq.asyncio.install()

import librosa

from TAVEngine.BVCEngineAPI import BVCEngineAPI
import TAVEngine.ModelLoader as ModelLoader

import engine_settings as settings


#if not os.path.exists('logs'):
#    os.makedirs('logs')
logging.basicConfig(format='%(asctime)s %(message)s',filename='worker.log', level=logging.DEBUG)


clients_map = {}


async def recv_and_process(conn, myid, client_port, affinity):

    # starting zeromq server
    context = zmq.asyncio.Context()

    client_sock = context.socket(zmq.ROUTER)

    client_sock.bind("tcp://*:%d" % client_port)


    #poller = zmq.Poller()
    #poller.register(client_sock, zmq.POLLIN)

    stat_proc = psutil.Process()

    ModelLoader.load_model()

    # Affinity works on Linux, Windows - doesn't work on Mac
    if hasattr(stat_proc, 'cpu_affinity'):
        stat_proc.cpu_affinity([affinity])

    while True:
        try:
           
            if True:
                #client_id, cmd, data = client_sock.recv_multipart()
                client_id, cmd, data = await client_sock.recv_multipart()
                cmd = cmd.decode("utf-8")
                #print("from client", client_id.decode("utf-8"), cmd)

                # find client, create if new
                if client_id not in clients_map:
                    engine = BVCEngineAPI()
                    clients_map[client_id] = {
                        'engine': engine,
                        'buffer': None
                    }
                    #engine.NewSession()

                    # report new sesion to manager
                    conn.send({'id': myid, 'newsession': True, 'loadavg': len(clients_map.keys())})
                else:
                    engine = clients_map[client_id]['engine']

                # message processing
                if cmd == 'data':
                    client = clients_map[client_id]

                    # prepending buffer, if not empty
                    if client['buffer']:
                        data = client['buffer'] + data
                        #print("Buffer %d" % len(data))
                    if len(data) < settings.BUFFER_LEN:
                        # still not enough for min buffer
                        client['buffer'] = data
                    else:
                        # strip non-frame chunk from data and save to buffer
                        frame_leftover = len(data) % settings.BUFFER_FRAME
                        if frame_leftover:
                            client['buffer'] = data[-frame_leftover:] or None
                            data = data[:-frame_leftover]
                        else:
                            client['buffer'] = None

                        # convert to float
                        chunk = librosa.util.buf_to_float(data, dtype=np.float32)
                        print('chanks:',len(chunk))
                        # process
                        results = engine.process_chunk(chunk)  #.ProcessChunk(chunk)
                        if results is not None:
                            print('results',type(results),str(results))
                            await client_sock.send_multipart([client_id, bytes(json.dumps(results), "utf-8")])

                elif cmd == 'endsession':
                    # end session, return final result and clean up
                    # TODO: process code as in 'data', put into function
                    # engine doesn't have SessionEnd call yet
                    # results = engine.SessionEnd()
                    client = clients_map[client_id]
                    if client['buffer']:
                        data = client['buffer']
                        if len(data)>3:
                            frame_leftover = len(data) % settings.BUFFER_FRAME
                            if frame_leftover:
                                data = data[:-frame_leftover]
                            chunk = librosa.util.buf_to_float(data, dtype=np.float32)
                            results = engine.process_chunk(chunk)
                            if results is not None:
                                print("Last buffer results", results)
                                await client_sock.send_multipart([client_id, bytes(json.dumps(results), "utf-8")])
                        del client['buffer']

                    # send summary
                    summary = engine.end_session()
                    print("Summary", summary)
                    await client_sock.send_multipart([client_id, bytes(json.dumps(summary), "utf-8")])

                    del engine
                    del clients_map[client_id]

                    # report end sesion to manager
                    conn.send({'id': myid, 'endsession': True, 'loadavg': len(clients_map.keys())})
                    
            # idle handling
            #time.sleep(.01)
            
        except Exception as e:
            logging.exception("Worker loop")
            print(e)

    client_sock.close()


def worker(conn, myid, client_port, affinity):
    """worker function"""
    loop = zmq.asyncio.ZMQEventLoop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(recv_and_process(conn, myid, client_port, affinity))

