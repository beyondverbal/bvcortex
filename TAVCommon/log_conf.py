import logging
import logging.config


def create_log_dict(std_out_level, file_level, out_file):
    return {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s',
                'datefmt': '%Y-%m-%d %H:%M'  # 'datefmt': '%Y-%m-%d %H:%M:%S'
            },
        },
        'handlers': {
            'default': {
                'level': std_out_level,  # 'DEBUG',  # 'INFO',
                'formatter': 'standard',
                'class': 'logging.StreamHandler'},
            'file': {
                'level': file_level,  # 'DEBUG',  # ''WARNING',
                'class': 'logging.handlers.RotatingFileHandler',
                'formatter': 'standard',
                'filename': out_file,  # 'C:/BVC/BVC_CORE_OUTPUT/5.4.0.0/TAV_EER_GRID/log_def.log',
                'maxBytes': 1024,
                'backupCount': 3}
        },
        'loggers': {
            '': {
                'handlers': ['default', 'file'],
                'level': std_out_level,  # 'INFO',
                'propagate': True
            },
            'django.request': {
                'handlers': ['default', 'file'],
                'level': 'WARN',
                'propagate': False
            },
        }
    }


def create_log_dict_no_file(std_out_level):
    return {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s',
                'datefmt': '%Y-%m-%d %H:%M'  # 'datefmt': '%Y-%m-%d %H:%M:%S'
            },
        },
        'handlers': {
            'default': {
                'level': std_out_level,  # 'DEBUG',  # 'INFO',
                'formatter': 'standard',
                'class': 'logging.StreamHandler'},
        },
        'loggers': {
            '': {
                'handlers': ['default'],
                'level': std_out_level,  # 'INFO',
                'propagate': True
            },
            'django.request': {
                'handlers': ['default'],
                'level': 'WARN',
                'propagate': False
            },
        }
    }

# TODO - remove
DEFAULT_LOGGING = {
     'version': 1,
     'disable_existing_loggers': False,
     'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
     },
     'handlers': {
        'default': {
            'level': 'DEBUG',  # 'INFO',
            'formatter': 'standard',
            'class': 'logging.StreamHandler'},
        'file': {
                'level': 'DEBUG',  # ''WARNING',
                'class': 'logging.handlers.RotatingFileHandler',
                'formatter': 'standard',
                'filename': 'C:/BVC/BVC_CORE_OUTPUT/5.4.0.0/TAV_EER_GRID/log_def.log',
                'maxBytes': 1024,
                'backupCount': 3}
     },
     'loggers': {
        '': {
            'handlers': ['default', 'file'],
            'level': 'INFO',
            'propagate': True
        },
        'django.request': {
            'handlers': ['default', 'file'],
            'level': 'WARN',
            'propagate': False
        },
     }
}


def configure_logger(name, log_path):
    logging.config.dictConfig({
        'version': 1,
        'formatters': {
            'default': {'format': '%(asctime)s - %(levelname)s - %(message)s', 'datefmt': '%Y-%m-%d %H:%M'}
        },
        'handlers': {
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'default',
                'stream': 'ext://sys.stdout'
            },
            'file': {
                'level': 'DEBUG',
                'class': 'logging.handlers.RotatingFileHandler',
                'formatter': 'default',
                'filename': log_path,
                'maxBytes': 1024,
                'backupCount': 3
            }
        },
        'loggers': {
            'default': {
                'level': 'DEBUG',
                'handlers': ['console', 'file']
            }
        },
        'disable_existing_loggers': False
    })
    return logging.getLogger(name)


