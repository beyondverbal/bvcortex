import numpy as np
import librosa
import math
import os
import pandas as pd
import pickle
from TAVEngine import HLDConfig as HLDCfg
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
import logging.config
from TAVCommon import log_conf

logging.config.dictConfig(log_conf.create_log_dict_no_file(logging.INFO))
cur_logger = logging.getLogger('AuxFunctions')


def save_load_hld(hld_folder, wav, payload=None):
    pkl_wav = wav + '.pkl'
    if not payload:
        if os.path.isfile(hld_folder + pkl_wav):
            with open(hld_folder + pkl_wav, 'rb') as file:
                out = pickle.load(file)
            return out
        else:
            return None
    else:
        with open(hld_folder + pkl_wav, 'wb') as file:
            pickle.dump(payload, file)


def apply_stop_conditions(stop_cond_dict):
    cur_logger.error('implement me!')


def get_wav_list_from_dir(folder_path):
    wav_list_from_folder = []
    for cur_file in os.listdir(folder_path):
        if cur_file.endswith(".wav"):
            wav_list_from_folder.append(folder_path + cur_file)
    wavs_no_path = [os.path.basename(x) for x in wav_list_from_folder]
    md5 = ['NA' for x in wav_list_from_folder]
    return {'wav_list': wav_list_from_folder, 'wavemd5': md5, 'wav_no_path': wavs_no_path}


def get_wav_list_from_file(filename):
    wav_list_from_folder = []
    for cur_file in os.listdir(folder_path):
        if cur_file.endswith(".wav"):
            wav_list_from_folder.append(folder_path + cur_file)
    wavs_no_path = [os.path.basename(x) for x in wav_list_from_folder]
    md5 = ['NA' for x in wav_list_from_folder]
    return {'wav_list': wav_list_from_folder, 'wavemd5': md5, 'wav_no_path': wavs_no_path}

def create_db_from_source_name(source_name, source_file):
    tmp = pd.read_csv(source_file)
    tmp = tmp[tmp['sourceName'] == source_name]

    md5 = tmp.wavemd5.values
    wav_no_path = tmp.FileName.values + '.wav'
    wav_list = 'C:/BVC/DownloadedVoices/' + wav_no_path

    offset = tmp.segment_offset.values
    duration = tmp.segment_duration.values
    labels = {'temper': tmp.temper.values, 'arousal': tmp.arousal.values, 'valence': tmp.valence.values}
    return {'wav_list': wav_list, 'wavemd5': md5, 'wav_no_path': wav_no_path,
            'offset': offset, 'duration': duration, 'labels': labels}


def calc_metrics(y_test, predictions, model_classes):
    accuracy = accuracy_score(y_test, predictions)
    precision = precision_score(y_test, predictions, average=None)
    recall = recall_score(y_test, predictions, average=None)
    f1 = f1_score(y_test, predictions, average=None)

    label_count = np.unique(y_test, return_counts=True)
    count_dict = dict(zip(label_count[0], label_count[1]))
    count = []
    for idx in range(len(model_classes)):
        try:
            count.append(count_dict[model_classes[idx]])
        except:
            # Happens if a class is missing from y_test due to low stats
            count.append(0)
            if len(precision) < 2:   # To DO: make this '2' more general
                precision = np.insert(precision, idx, 0)
                recall = np.insert(recall, idx, 0)
                f1 = np.insert(f1, idx, 0)

    precision = np.append(precision, np.mean(precision))
    recall = np.append(recall, np.mean(recall))
    f1 = np.append(f1, np.mean(f1))

    result = {'accuracy': accuracy, 'labels': model_classes, 'precision': precision, 'recall': recall,
              'f1': f1, 'count': count}
    return result


def get_wav_list_from_file(input_file, file_type='database'):
    if file_type is 'database':
        dataset = pd.read_csv(input_file)
        wavs = np.array(['C:/BVC/DownloadedVoices/' + w + '.wav' for w in dataset.FileName.values])
        return {'wav_list': wavs, 'offset': dataset.segment_offset.values, 'wavemd5': dataset.wavemd5.values,
                'wav_no_path': dataset.FileName.values}
    elif file_type is 'excel_liat':
        dataset = pd.read_csv(input_file)
        dataset.assign(wavemd5='nan')
        for i, fname in enumerate(dataset['filename']):
            dataset.loc[i, 'filename'] = fname + '.wav'
            dataset.loc[i, 'seg_start'] = dataset.loc[i, 'seg_start']*1000
            dataset.loc[i, 'seg_end'] = dataset.loc[i, 'seg_end']*1000

        return {'wav_list': dataset.loc[:, 'filename'], 'offset': dataset.loc[:, 'seg_start'],
                'duration': dataset.loc[:, 'seg_end'], 'wavemd5': dataset.loc[:, 'legend']}
    else:
        cur_logger.error('PLEASE IMPLEMENT!')


def chunker(signal, fs, chunk_length=1):
    chunk_size = int(fs * chunk_length)
    if chunk_size > len(signal):
        chunk_size = len(signal)
    indices = []
    chunk_num = 1
    index = chunk_size * chunk_num
    while index < len(signal):
        indices.append(index)
        chunk_num += 1
        index = chunk_size * chunk_num

    return np.split(signal, indices)


def calc_new_signal(signal, offset, duration, ms_to_samples_rat):
    if math.isnan(offset):
        offset_smpls = 0
    else:
        offset_smpls = int(offset * ms_to_samples_rat)

    if math.isnan(duration):
        duration_smpls = len(signal)
        new_signal = signal[offset_smpls: duration_smpls]
    else:
        duration_smpls = int(duration * ms_to_samples_rat)
        new_signal = signal[offset_smpls: offset_smpls + duration_smpls]

    return new_signal, offset_smpls, duration_smpls


def get_other_titles():
    cur_logger.error('implement me')


def read_wav(cur_file, frame_size=None, sampling_rate=None, resample=False):
    try:
        signal, fs = librosa.load(cur_file, sr=None)
    except OSError as err:
        cur_logger.error('      OS error: {0} skipping file'.format(err))
        return None, None
    except:
        cur_logger.error('      *** UNKNOWN ERROR ***: Cannot read {}, skipping file.'.format(cur_file))
        return [], None

    if sampling_rate:
        if fs != sampling_rate:
            if resample:
                new_sig = librosa.resample(signal, fs, sampling_rate)
                cur_logger.warning('        *** CHANGED SAMPLING RATE FROM {} -> {} ***'.format(fs, sampling_rate))
                return new_sig, sampling_rate
            else:
                cur_logger.error('ERROR: Signal sampling rate different than the predefined value, skipping file ({}).'.format(cur_file))
                return None, None

    if frame_size:
        if len(signal) < frame_size:
            return None, None

    return signal, fs


def read_wav2(cur_file):
    try:
        signal, fs = librosa.load(cur_file, sr=None)
        return signal, fs, None
    except Exception as ex:
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        return None, None, message

    # except OSError as err:
    #     # cur_logger.error('OS error: {0} skipping file'.format(err))
    #     return None, None, 'OS error: {0} skipping file'
    # except IOError:
    #     # cur_logger.error('An error occurred trying to read the file')
    #     return None, None, 'An error occurred trying to read the file'
    # except ValueError:
    #     # cur_logger.error('Non-numeric data found in the file')
    #     return None, None, 'Non-numeric data found in the file'
    # except ImportError:
    #     # cur_logger.error('NO module found')
    #     return None, None, 'NO module found'
    # except EOFError:
    #     # cur_logger.error('Why did you do an EOF on me?')
    #     return None, None, 'Why did you do an EOF on me?'
    # except KeyboardInterrupt:
    #     # cur_logger.error('You cancelled the operation')
    #     return None, None, 'You cancelled the operation'
    # except:
    #     # cur_logger.error('*** UNKNOWN ERROR ***: Cannot read {}, skipping file.'.format(cur_file))
    #     return [], None, '*** UNKNOWN ERROR ***: Cannot read {}, skipping file.'.format(cur_file)


def get_predict_titles():
    titles = ['wavemd5', 'GlobalStart', 'GlobalEnd', 'GlobalDuration', 'GlobalVADSumVoiced', 'DefaultStart',
              'DefaultEnd', 'DefaultDuration', 'Anger', 'AngerValue', 'Sadness', 'SadnessValue',
              'TemperStart', 'TemperEnd', 'TemperDuration',
              'TemperVADSumVoiced', 'TemperLabel', 'TemperVal', 'TemperConf', 'ArousalStart', 'ArousalEnd',
              'ArousalDuration', 'ArousalVADSumVoiced', 'ArousalLabel', 'ArousalVal', 'ArousalConf', 'ValenceStart',
              'ValenceEnd', 'ValenceDuration', 'ValenceVADSumVoiced', 'ValenceLabel', 'ValenceVal', 'ValenceConf',
              'EmotionGroup', 'EmotionGroupConf', 'TemperOrig', 'ArousalOrig', 'ValenceOrig']
    return titles


def prepare_line_for_thor(filename, st, nd, conf=-1, op_code='predict'):
    if op_code is 'titles':
        titles = ['FileName', 'start', 'end', 'label', 'conf']
        return titles

    elif op_code is 'predict':
        return np.array([filename, st, nd, 'NA', conf])


def write_to_thor(op_code, tav_predict=None, detector_predict=None, wav=None):
    if op_code is 'titles':
        titles = ['FileName', 'start', 'end', 'label', 'conf']
        return titles

    elif op_code is 'predict':
        global_start = detector_predict['start']/1000.0
        # global_end = tav_predict['end']/1000.0
        global_end = global_start + 5

        pos_val = detector_predict['high_valence_detector']
        neg_val = detector_predict['low_valence_detector']

        det = np.nan
        if pos_val is 'unanalyzable' or neg_val is 'unanalyzable':
            det = ''
        elif pos_val and neg_val:
            det = 'P1N1'
        elif pos_val and not neg_val:
            det = 'P1N0'
        elif not pos_val and neg_val:
            det = 'P0N1'
        elif not pos_val and not neg_val:
            det = 'P0N0'

        pos_val_conf = detector_predict['high_valence_detector_conf']
        if pos_val_conf == 100:
            cur_logger.warning('CHECK THIS!!!')
            pos_val_conf = 99
        elif pos_val_conf == -1:
            pos_val_conf = 0
        neg_val_conf = detector_predict['low_valence_detector_conf']
        if neg_val_conf == 100:
            cur_logger.warning('CHECK THIS!!!')
            neg_val_conf = 99
        elif neg_val_conf == -1:
            neg_val_conf = 0

        conf = ('{0:2.2f}'.format(pos_val_conf + neg_val_conf / 100))
        return np.array([wav, global_start, global_end, det, conf])


def predict_tav_detector_to_df(op_code, tav_predict=None, detector_predict=None, wav=None, wavemd5=np.nan):
    if op_code is 'titles':
        titles = ['FileName', 'wavemd5', 'GlobalStart', 'GlobalEnd', 'GlobalDuration', 'GlobalVADSumVoiced',
                  'PosValence', 'PosValenceConf', 'NegValence', 'NegValenceConf',
                  'TemperLabel', 'TemperVal', 'TemperConf',
                  'ArousalLabel', 'ArousalVal', 'ArousalConf',
                  'ValenceLabel', 'ValenceVal', 'ValenceConf',
                  'EmotionGroup', 'EmotionGroupConf']
        return titles

    elif op_code is 'predict':
        global_start = tav_predict['start']
        global_end = tav_predict['end']
        global_duration = tav_predict['duration']
        global_vad = tav_predict['vad_sum_voiced']

        temper = tav_predict['temper_group']
        temper_val = tav_predict['temper_val']
        temper_conf = tav_predict['temper_reject_score']
        arousal = tav_predict['arousal_group']
        arousal_val = tav_predict['arousal_val']
        arousal_conf = tav_predict['arousal_reject_score']
        valence = tav_predict['valence_group']
        valence_val = tav_predict['valence_val']
        valence_conf = tav_predict['valence_reject_score']

        emotion_group = tav_predict['emotion_group']
        emotion_group_conf = tav_predict['emotion_group_reject_score']

        anger_detector = detector_predict['anger_detector']
        anger_conf = detector_predict['anger_detector_conf']
        sadness_detector = detector_predict['sadness_detector']
        sadness_conf = detector_predict['sadness_detector_conf']

        return np.array(
            [wav, wavemd5, global_start, global_end, global_duration, global_vad,
             anger_detector, anger_conf, sadness_detector, sadness_conf,
             temper, temper_val, temper_conf, arousal, arousal_val, arousal_conf, valence, valence_val, valence_conf,
             emotion_group, emotion_group_conf])


def predict_detector_to_df(op_code, tav_predict=None, detector_predict=None, wav=None, wavemd5=np.nan):
    if op_code is 'titles':
        titles = ['FileName', 'wavemd5', 'Start', 'End', 'Duration', 'VAD',
                  'PosValence', 'PosValenceScore', '%', 'NegValence', 'NegValenceScore', '%',
                  'HighArousal', 'HighArousalScore', '%', 'LowArousal', 'LowArousalScore', '%',
                  'valence_combined', 'valence_combined_val', 'arousal_combined', 'arousal_combined_val']

        return titles

    elif op_code is 'predict':
        global_start = detector_predict['start']
        global_end = detector_predict['end']
        global_duration = detector_predict['duration']
        global_vad = detector_predict['vad_sum_voiced']

        pos_val_detector = detector_predict['high_valence_detector']
        pos_val_conf = detector_predict['high_valence_detector_conf']
        neg_val_detector = detector_predict['low_valence_detector']
        neg_val_conf = detector_predict['low_valence_detector_conf']

        high_arousal_detector = detector_predict['high_arousal_detector']
        high_arousal_conf = detector_predict['high_arousal_detector_conf']
        low_arousal_detector = detector_predict['low_arousal_detector']
        low_arousal_conf = detector_predict['low_arousal_detector_conf']

        return np.array(
            [wav, wavemd5, global_start,
             # global_end, global_duration,
             global_vad,
             tav_predict['temper_group'], tav_predict['temper_val'], tav_predict['temper_reject_score'],
             tav_predict['arousal_group'], tav_predict['arousal_val'], tav_predict['arousal_reject_score'],
             tav_predict['valence_group'], tav_predict['valence_val'], tav_predict['valence_reject_score'],
             pos_val_detector, pos_val_conf, #'',
             neg_val_detector, neg_val_conf, #'',
             high_arousal_detector, high_arousal_conf, #'',
             low_arousal_detector, low_arousal_conf, #'',
             detector_predict['valence_detector'], detector_predict['valence_detector_val'],
             detector_predict['arousal_detector'], detector_predict['arousal_detector_val'],
             detector_predict['valence_detector_70'], detector_predict['arousal_detector_70'],
             detector_predict['valence_high_lin_dist'], detector_predict['valence_low_lin_dist'],
             detector_predict['arousal_high_lin_dist'], detector_predict['arousal_low_lin_dist']])


def summary_detector_to_df(op_code, summary=None, summary_tav=None, det_post_proc=None, wav=None, md5='SUMMARY'):
    if op_code is 'titles':
        titles = ['FileName', 'md5', 'start',
                  # 'end', 'duration',
                  'VAD',
                  'TemperMode', 'TemperMean', 'TemperPercent',
                  'ArousalMode', 'ArousalMean', 'ArousalPercent',
                  'ValenceMode', 'ValenceMean', 'ValencePercent',
                  'PosValenceMode', 'PosValenceMean',# 'PosValencePercent',
                  'NegValenceMode', 'NegValenceMean', #'NegValencePercent',
                  'HighArousalMode', 'HighArousalMean',# 'HighArousalPercent',
                  'LowArousalMode', 'LowArousalMean', #'LowArousalPercent',
                  'valence_comb', 'valence_comb_val', 'arousal_comb', 'arousal_comb_val',
                  'valence_comb_70', 'arousal_comb_70',
                  'pos_val_LD', 'neg_val_LD','hi_ar_LD', 'lo_ar_LD']

        return titles

    elif op_code is 'predict':
        if summary['valid']:
            return np.array(
                [wav, md5, np.nan,
                 # np.nan, np.nan,
                 np.nan,
                 summary_tav['temper_mode'], summary_tav['temper_mean'], summary_tav['temper_mode_percent'],
                 summary_tav['arousal_mode'], summary_tav['arousal_mean'], summary_tav['arousal_mode_percent'],
                 summary_tav['valence_mode'], summary_tav['valence_mean'], summary_tav['valence_mode_percent'],
                 summary['high_valence_mode'], summary['high_valence_median'], #summary['high_valence_mode_percent'],
                 summary['low_valence_mode'], summary['low_valence_median'], #summary['low_valence_mode_percent'],
                 summary['high_arousal_mode'], summary['high_arousal_median'], #summary['high_arousal_mode_percent'],
                 summary['low_arousal_mode'], summary['low_arousal_median'], #summary['low_arousal_mode_percent'],
                 summary['valence_mode'], summary['valence_median'],
                 summary['arousal_mode'], summary['arousal_median'],
                 det_post_proc['valence'],
                 det_post_proc['arousal']])
        else:
            return np.array([wav])


def predict_to_df(predict_result, wav, wavemd5=np.nan):

    global_start = predict_result['start']
    global_end = predict_result['end']
    global_duration = predict_result['duration']
    global_vad = predict_result['vad_sum_voiced']

    temper_start = predict_result['temper_start']
    temper_end = predict_result['temper_end']
    temper_duration = predict_result['temper_duration']
    temper_vad_sum = predict_result['temper_vad_sum_voiced']
    temper = predict_result['temper_group']
    temper_val = predict_result['temper_val']
    temper_conf = predict_result['temper_reject_score']

    arousal_start = predict_result['arousal_start']
    arousal_end = predict_result['arousal_end']
    arousal_duration = predict_result['arousal_duration']
    arousal_vad_sum = predict_result['arousal_vad_sum_voiced']
    arousal = predict_result['arousal_group']
    arousal_val = predict_result['arousal_val']
    arousal_conf = predict_result['arousal_reject_score']

    valence_start = predict_result['valence_start']
    valence_end = predict_result['valence_end']
    valence_duration = predict_result['valence_duration']
    valence_vad_sum = predict_result['valence_vad_sum_voiced']
    valence = predict_result['valence_group']
    valence_val = predict_result['valence_val']
    valence_conf = predict_result['valence_reject_score']

    emotion_group = predict_result['emotion_group']
    emotion_group_conf = predict_result['emotion_group_reject_score']

    anger_detector = predict_result['anger_detector']
    anger_conf = predict_result['anger_detector_conf']
    sadness_detector = predict_result['sadness_detector']
    sadness_conf = predict_result['sadness_detector_conf']

    return np.array([wav, wavemd5, global_start, global_end, global_duration, global_vad, default_start, default_end,
                     default_duration,
                     anger_detector, anger_conf, sadness_detector, sadness_conf,
                     temper_start, temper_end, temper_duration, temper_vad_sum, temper, temper_val, temper_conf,
                     arousal_start, arousal_end, arousal_duration, arousal_vad_sum, arousal, arousal_val, arousal_conf,
                     valence_start, valence_end, valence_duration, valence_vad_sum, valence, valence_val, valence_conf,
                     emotion_group, emotion_group_conf])


def predict_to_df_with_summary(summary, wav):

    global_start = 'arousal_mean'
    global_end = summary['arousal_mean']
    global_duration = 'arousal_mode'
    global_vad = summary['arousal_mode']

    # default_vad = predict_result['default_vad_sum_voiced']

    default_start = 'arousal_mode_percent'
    default_end = summary['arousal_mode_percent']
    default_duration = 'duration'

    temper_start = summary['duration']
    temper_end = 'emotion_group_mode'
    temper_duration = summary['emotion_group_mode']
    temper_vad_sum = 'emotion_group_mode_percent'
    temper = summary['emotion_group_mode_percent']
    temper_val = 'temper_mean'
    temper_conf = summary['temper_mean']

    arousal_start = 'temper_mode'
    arousal_end = summary['temper_mode']
    arousal_duration = 'temper_mode_percent'
    arousal_vad_sum = summary['temper_mode_percent']
    arousal = 'valence_mean'
    arousal_val = summary['valence_mean']
    arousal_conf = 'valence_mode'

    valence_start = summary['valence_mode']
    valence_end = 'valence_mode_percent'
    valence_duration = summary['valence_mode_percent']
    valence_vad_sum = 'NA'
    valence = 'NA'
    valence_val = 'NA'
    valence_conf = 'NA'

    emotion_group = 'NA'
    emotion_group_conf = 'NA'

    # no tone test at the moment
    tonetest_start = None
    tonetest_end = None
    tonetest_duration = None
    tonetest_vad_sum = None
    t1 = None
    t2 = None
    t3 = None
    t4 = None
    phrase1 = None
    phrase2 = None
    default_vad = None

    # orig_temper = None
    # orig_arousal = None
    # orig_valence = None

    anger_detector = summary['anger_detector']
    sadness_detector = summary['sadness_detector']

    return np.array([wav, global_start, global_end, global_duration, global_vad, default_start, default_end,
                     default_duration, default_vad, anger_detector, sadness_detector,
                     temper_start, temper_end, temper_duration, temper_vad_sum, temper, temper_val, temper_conf,
                     arousal_start, arousal_end, arousal_duration, arousal_vad_sum, arousal, arousal_val, arousal_conf,
                     valence_start, valence_end, valence_duration, valence_vad_sum, valence, valence_val, valence_conf,
                     tonetest_start, tonetest_end, tonetest_duration, tonetest_vad_sum, t1, t2, t3, t4, phrase1,
                     phrase2, emotion_group, emotion_group_conf])  # , orig_temper, orig_arousal, orig_valence])
