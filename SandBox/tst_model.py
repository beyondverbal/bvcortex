
from sklearn.externals import joblib
import pandas as pd
import numpy as np


def func_sigmoid(x, x0, k):
    y = 1 / (1 + np.exp(-k * (x - x0)))
    return y


f = {}
f.update({'a': 'C:/BVC/BVC_CORE_OUTPUT/5.5.0.1/CV/high_temper/model_new.pkl'})
f.update({'b': 'C:/BVC/BVC_CORE_OUTPUT/5.5.0.1/CV/high_temper/model_new_comb.pkl'})
f.update({'c': 'C:/BVC/BVC_CORE_OUTPUT/5.5.0.1/CV/high_temper/model_new_comb.pkl'})

tst = np.arange(0, 80)/80
tst = tst.reshape(1, -1)

model_lin, feature_names, model_sig = joblib.load(f['a'])

x_transformed = model_lin['standardize'](tst)
lin_dist = np.dot(model_lin['lin'], x_transformed.T)[0]

newLin, f, Sig, mu_tot, a_tot = joblib.load(f['b'])
tst_full = np.zeros([1, 166])
tst_full[0, f[1]] = tst
# fact = l['lin']/scl.reshape(1, -1)
tmp2 = (tst_full-mu_tot)
lin_dist_comb = np.dot(a_tot, tmp2.T)[0]

print('lin1 = {}; lin2 = {}; rat = {}'.format(lin_dist, lin_dist_comb, lin_dist/lin_dist_comb))
t=1