import pandas as pd
import numpy as np
import librosa

root = 'C:/BVC/From Sagi/vitality/'
root_out = root + 'wavs_segmented_old/'
meta_data = pd.read_excel(root + 'MetaData.xlsx', sheet_name='MetaData')

old_seg = pd.read_excel(root + 'MetaData.xlsx', sheet_name='OldSeg')
New_seg = pd.read_excel(root + 'MetaData.xlsx', sheet_name='NewSeg')

files = meta_data.filename.values

for cur_file in files:
    signal, fs = librosa.load(root + 'wavs/' + str(cur_file) + '.wav', sr=None)
    if fs != 8000:
        print('fs is {} for wav {}'.format(fs, cur_file))
        continue
    segs = old_seg.loc[old_seg['filename'] == cur_file, ['seg_start', 'seg_end']]
    new_sig = np.array([])

    for i, val in enumerate(segs.seg_start):
        st_sample = int(val*fs)
        nd_sample = int(segs.seg_end.values[i]*fs)

        new_sig = np.append(new_sig, signal[st_sample: nd_sample])

    librosa.output.write_wav(root_out + str(cur_file) + '.wav', new_sig[:21*fs], fs)
