import pandas as pd

from TAVTrainer import ConfigTraining as Cfg

root = Cfg.OUT_PATH_GRID  # + 'D4_40_orig/'

res = pd.DataFrame([])
for i, tav in enumerate(['temper', 'arousal', 'valence']):
    for j, tav_class in enumerate(['high', 'low']):
        cur = pd.read_csv(root + tav + '_' + tav_class + '_grid_search.csv')
        if i == 0 and j == 0:
            res = cur.copy()
        else:
            res = res.append(cur)
res.to_csv(root + 'all_results.csv', index=None)

