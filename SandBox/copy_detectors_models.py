from shutil import copyfile

import numpy as np
import pandas as pd

from PATrends import PATConfig as PATCfg
from TAVTrainer import ConfigTraining as Cfg

ADD_POSTFIX = True
POSTFIX = ''

task_dict = {'1': 'send to excel',  # for grid search_ create excel with all 6 detectors + total sheet
             '2': 'copy_to_models',  # copy model from working version's cv dir to project's model dir
             '3': 'kvetch_grid_search'}  # create results_all, one sheet for all!!, esp. if grid=1
task = task_dict['2']

totals_df = pd.DataFrame(columns=['tav', 'auc'])
if task == 'send to excel':

    src_dir = Cfg.OUT_PATH_GRID
    xls_name = src_dir + 'all_results'
    if ADD_POSTFIX:
        xls_name = xls_name + '_' + POSTFIX
    xls_name = xls_name + '.xlsx'
    writer = pd.ExcelWriter(xls_name, engine='xlsxwriter')

    # src_dir = Cfg.OUT_PATH_GRID + PATCfg.ENGINE_CONF['name'] + '/'
    # writer = pd.ExcelWriter(src_dir + PATCfg.ENGINE_CONF['name'] + '.xlsx', engine='xlsxwriter')

    for i, tav in enumerate(['temper', 'arousal', 'valence']):
        for j, tav_class in enumerate(['high', 'low']):
            tav_tav = tav_class + '_' + tav

            res_file = src_dir + tav + '_' + tav_class + '_grid_search.csv'
            if ADD_POSTFIX:
                res_file = src_dir + '/' + POSTFIX + '/' + tav + '_' + tav_class + '_grid_search' + POSTFIX + '.csv'

            res_df = pd.read_csv(res_file)
            res_df.sort_values('auc_target', ascending=False, inplace=True)
            auc = res_df.auc_target.values
            totals_df = totals_df.append({'tav': tav_tav, 'auc': np.max(auc)}, ignore_index=True)

            mmax = np.argmax(auc)

            res_df.to_excel(writer, sheet_name=tav_tav)
    totals_df = totals_df.append({'tav': 'average', 'auc': np.mean(totals_df.auc.values)}, ignore_index=True)
    totals_df.to_excel(writer, sheet_name='total')
    print('wrote to file {}'.format(res_file))
    writer.save()

elif task == 'copy_to_models':
    src_dir = Cfg.OUT_PATH_CV
    dst_dir = './TAVEngine/ModelLoader/'

    for i, tav in enumerate(['temper', 'arousal', 'valence']):
        for j, tav_class in enumerate(['high', 'low']):
            tav_tav = tav_class + '_' + tav
            # res.to_csv(root + 'all_results.csv', index=None)
            src = src_dir + tav_tav + '/model_new_comb.pkl'
            dst = dst_dir + tav_tav + '_model.pkl'
            copyfile(src, dst)

elif task == 'kvetch_grid_search':
    root = Cfg.OUT_PATH_GRID  # + 'D4_40_orig/'

    res = pd.DataFrame([])
    for i, tav in enumerate(['temper', 'arousal', 'valence']):
        for j, tav_class in enumerate(['high', 'low']):
            cur = pd.read_csv(root + tav + '_' + tav_class + '_grid_search.csv')
            if i == 0 and j == 0:
                res = cur.copy()
            else:
                res = res.append(cur)
    res.to_csv(root + 'all_results.csv', index=None)