import csv
import logging.config

import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score

from TAVCommon import log_conf

logging.config.dictConfig(log_conf.create_log_dict_no_file(logging.DEBUG))
cur_logger = logging.getLogger('Compare Engine Versions')
cur_logger.info('Starting Run :)')


def calc_metrics(y_test, predictions, model_classes):
    accuracy = accuracy_score(y_test, predictions)
    precision = precision_score(y_test, predictions, average=None)
    recall = recall_score(y_test, predictions, average=None)
    f1 = f1_score(y_test, predictions, average=None)

    label_count = np.unique(y_test, return_counts=True)
    count_dict = dict(zip(label_count[0], label_count[1]))
    count = []
    for idx in range(len(model_classes)):
        try:
            count.append(count_dict[model_classes[idx]])
        except:
            # Happens if a class is missing from y_test due to low stats
            count.append(0)
            if len(precision) < 2:   # To DO: make this '2' more general
                precision = np.insert(precision, idx, 0)
                recall = np.insert(recall, idx, 0)
                f1 = np.insert(f1, idx, 0)

    precision = np.append(precision, np.mean(precision))
    recall = np.append(recall, np.mean(recall))
    f1 = np.append(f1, np.mean(f1))

    result = {'accuracy': accuracy, 'labels': model_classes, 'precision': precision, 'recall': recall,
              'f1': f1, 'count': count}
    return result


def prep_gt(proj, db):
    db.drop(db[db['sourceName'] != proj].index, inplace=True)
    gt_df = db[['wavemd5', 'temper', 'arousal', 'valence']]
    gt_df = gt_df.rename(columns={'temper': 'temper_gt', 'arousal': 'arousal_gt', 'valence': 'valence_gt'})
    return gt_df


def prep_tav(db):
    db.drop(db[db['md5'] == 'SUMMARY'].index, inplace=True)
    db2 = db[['md5', 'TemperMode', 'ArousalMode', 'ValenceMode']]
    db2 = db2.rename(columns={'md5': 'wavemd5', 'TemperMode': 'temper_tav54', 'ArousalMode': 'arousal_tav54',
                              'ValenceMode': 'valence_tav54'})
    return db2


def write_result_row(csv_writer, cur_tav, cur_text, res, order):
    csv_writer.writerow([cur_tav, cur_text,
                         np.round(res['recall'][order[0]], 3),
                         np.round(res['recall'][order[1]], 3),
                         np.round(res['recall'][order[2]], 3),
                         np.round(res['recall'][order[3]], 3),
                         res['count'][order[0]],
                         res['count'][order[1]],
                         res['count'][order[2]],
                         np.array(res['count']).sum()])


def compare_versions(project, w_flag, detector_res=None):
    if project == 'youtube':
        INCLUDE_v500 = False
    else:
        INCLUDE_v500 = True

    tav50 = pd.read_csv('C:/BVC/Projects/' + project + '/tav_v5_4/' + project + ' 5.0.0.csv')
    if detector_res is None:
        detector_res = pd.read_csv('C:/BVC/Projects/' + project + '/mid_res_073_073.csv')
    ground_t = pd.read_csv('C:/BVC/Projects/val_set_MIX5.3.0.0.csv')

    # prep gt
    gt_pd = prep_gt(project, ground_t.copy())

    # prep v5.0.0
    v50_pd = tav50[['md5', 'temper', 'arousal', 'valence']]
    v50_pd = v50_pd.rename(columns={'md5': 'wavemd5', 'temper': 'temper_label_50', 'arousal': 'arousal_label_50',
                                    'valence': 'valence_label_50'})

    detector_pred_df = detector_res[['wavemd5', 'temper_det_pred', 'arousal_det_pred', 'valence_det_pred']].copy()

    comb = pd.merge(left=gt_pd, right=detector_pred_df, on='wavemd5')
    if INCLUDE_v500:
        comb = pd.merge(left=comb, right=v50_pd, on='wavemd5')

    if not w_flag:
        res_df = pd.DataFrame(columns=['proj', 'db', 'tav', 'id', 'recall_low', 'recall_med', 'recall_high',
                                       'recall_mean', 'count_low', 'count_med', 'count_high', 'total'])

    for tav in ['temper', 'arousal', 'valence']:
        cur_logger.debug('now in tav = {}'.format(tav))
        for competitor in ['_det_pred']:  # , '_tav54']:
            # writer.writerow([tav, 'stand alone', 'with', competitor])
            cur_logger.debug('now comparing with = {}'.format(competitor))
            # print('detectors before = {}'.format(comb.shape))

            if tav == 'arousal':
                order = [1, 2, 0, 3]
            elif tav == 'valence':
                order = [0, 1, 2, 3]
            else:
                order = [1, 2, 0, 3]

            # prep detector from mid_res
            comb_temp = comb.copy()
            # remove ambiguous from ground_truth
            comb_temp.drop(comb_temp[comb_temp[tav + '_gt'] == 'ambiguous'].index, inplace=True)

            classes = np.unique(comb_temp[tav + '_gt'].values)

            if INCLUDE_v500:
                comb_temp.drop(comb_temp[comb_temp[tav + '_label_50'] == 'ambiguous'].index, inplace=True)
                comb_temp.drop(comb_temp[comb_temp[tav + '_label_50'] == 'unanalyzable'].index, inplace=True)
            comb_temp.drop(comb_temp[comb_temp[tav + competitor] == 'ambiguous'].index, inplace=True)
            comb_temp.drop(comb_temp[comb_temp[tav + competitor] == 'unanalyzable'].index, inplace=True)

            # AMBIGUOUS REMOVED FROM BOTH!!

            # calc metrics for v5.0.0 vs. gt

            if INCLUDE_v500:
                r_v5 = calc_metrics(comb_temp[tav + '_gt'].values, comb_temp[tav + '_label_50'].values, classes)
                if w_flag:
                    write_result_row(writer, tav, 'v5', r_v5, order)

            # calc metrics for v54 (tav or detector_based) vs. gt
            r_det = calc_metrics(comb_temp[tav + '_gt'].values, comb_temp[tav + competitor].values, classes)
            if w_flag:
                write_result_row(writer, tav, competitor, r_det, order)
            else:
                res_df = res_df.append({'proj': project, 'db': 'db', 'tav': tav, 'id': competitor,
                                        'recall_low': np.round(r_det['recall'][order[0]], 3),
                                        'recall_med': np.round(r_det['recall'][order[1]], 3),
                                        'recall_high': np.round(r_det['recall'][order[2]], 3),
                                        'recall_mean': np.round(r_det['recall'][order[3]], 3),
                                        'count_low': r_det['count'][order[0]],
                                        'count_med': r_det['count'][order[1]],
                                        'count_high': r_det['count'][order[2]],
                                        'total': np.array(r_det['count']).sum()},  ignore_index=True)
    return res_df


if __name__ == "__main__":
    write_flag = 1

    for project in ['scripps', 'youtube', 'mayo']:

        if write_flag == 1:
            file_name = 'C:/BVC/Projects/' + project + '/' + project + '_result_summary.csv'
            with open(file_name, 'w', newline='') as file:
                writer = csv.writer(file, delimiter=',')
                writer.writerow(['tav', 'id', 'recall_low', 'recall_med', 'recall_high', 'recall_mean', 'count_low',
                                 'count_med', 'count_high', 'total'])
                compare_versions(project, writer)
        else:
            writer = None
            compare_versions(project, writer)
