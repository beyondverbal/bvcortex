import logging
import os
import pickle

import numpy as np
import pandas as pd
from sklearn.metrics import roc_auc_score

import CommonML.Stratification as Stratification
from TAVTrainer import ConfigTraining as Cfg
from TAVTrainer.ProduceDatasetsDetector import ProduceDatasets


class FeatureBase:
    def __init__(self, feature_names):
        self.feature_names = feature_names
        self.selected_features = []
        self.feature_sort_vector = []

    def select_features(self, x, roc_auc, feat_sel_th=-1):
        if 0 < feat_sel_th <= 1:
            x_new = x[:, np.array(roc_auc) > feat_sel_th]
            self.selected_features = self.feature_names[np.array(roc_auc) > feat_sel_th]

        elif feat_sel_th > 1:
            self.feature_sort_vector = np.argsort(roc_auc)[-feat_sel_th:]
            x_new = x[:, self.feature_sort_vector]
            self.selected_features = self.feature_names[self.feature_sort_vector]
        else:
            x_new = x
            self.selected_features = self.feature_names
            self.feature_sort_vector = np.arange(0, len(self.feature_names))  # include all features
        return x_new

    def get_selected_features_full_vector(self):
        members_to_remove = np.setdiff1d(np.arange(0, len(self.feature_names)), self.feature_sort_vector)
        features_copy = self.feature_names.values
        features_copy[members_to_remove] = 0
        return features_copy, self.feature_sort_vector

    def get_selected_features(self):
        # return self.selected_features
        return self.get_selected_features_full_vector()


class ExperimentBase:
    def __init__(self):
        self.DB = {}
        for tav in Cfg.TAV:
            for tav_class in Cfg.TAV_CLASS:
                self.DB.update({tav + '_' + tav_class: ProduceDatasets(tav, tav_class, filter_type=Cfg.DATASETS)})

        self.train_data = {}  # init later
        self.validation_sets = []  # list, per tav+tav_class
        self.cur_iteration_val_sets = {}  # dict, per tav/tav_class

        _, self.features_names, _ = self.DB[Cfg.TAV[0] + '_' + Cfg.TAV_CLASS[0]].datasets['train']
        self.FBobj = FeatureBase(self.features_names)

        self.FoldsConf = {'n_folds': Cfg.N_KFOLDS, 'n_repeats': Cfg.N_REPEATS, 'permutation_num': Cfg.PERMUTATION_NUM}
        # self.output_columns = ['wavemd5', 'FileName_x', 'temper_mean', 'arousal_mean', 'valence_mean', 'source',
        #                        'TrueLabel', 'PredictedLabel', 'biomarker', 'score', 'linear_biomarker', 'sample_weight']
        self.output_columns = ['wavemd5', 'FileName_x', 'temper_mean', 'arousal_mean', 'valence_mean', 'source', 'TrueLabel',
                               'PredictedLabel', 'DistNormed', 'score', 'ScoreNormed', 'sample_weight', 'lin_dist']
        # predictions, score, DistNormed, ScoreNormed, highQ, rescaled2skew
        self.subset_by = ['source']

        self.X = []
        self.Y = []
        self.MD = []

        self.feature_ranks = []
        # self.feature_names = []
        self.Folds0 = []
        self.W = []
        self.md_ref = ['biomarker']  # remove this...
        self.logger = logging.getLogger('ExpBase')

    def GetCurrentFeatures(self):
        return self.FBobj.get_selected_features()
        # return self.feature_names

    def split_data(self, tav, tav_class, feature_threshold):
        x, self.feature_names, xny_df = self.DB[tav + '_' + tav_class].datasets['train']
        y = ProduceDatasets.get_target_vector(xny_df.copy(), tav, tav_class, feature_threshold)

        xny_df = xny_df.assign(TrueLabel=y)
        xny_df = xny_df.assign(source=xny_df.sourceName)
        xny_df = xny_df.assign(clustered=xny_df.speakerID)

        self.X = x
        self.Y = np.array(xny_df.TrueLabel.values)
        self.MD = xny_df
        self.MD.reset_index(inplace=True)

        # calculate weights
        self.W, self.MD = sample_weights_by_cluster(self.MD)

        # if Cfg.DBG_OVERRIDE_FOLDS:
        #     with open(Cfg.OUT_FOLDER + 'folds/temper_high_folds0_1.33 - 719.pkl', 'rb', ) as f:
        #         folds = pickle.load(f)
        # else:
        #     folds_file = Cfg.OUT_FOLDER + 'folds/' + tav + '_' + tav_class + '_folds0_' + str(feature_threshold) + '.pkl'
        #     if os.path.isfile(folds_file):
        #         with open(folds_file, 'rb', ) as f:
        #             folds = pickle.load(f)
        #     else:
        #         self.logger.info('Creating folds and calculating feature ranking... patience please')
        #         folds = Stratification.CreateFolds(self.X, self.MD, xny_df.clustered, [], self.Y, self.FoldsConf, seed=0)
        #         with open(folds_file, 'wb', ) as f:
        #             pickle.dump(folds, f)

        self.logger.info('Creating folds and calculating feature ranking... patience please')
        folds = Stratification.CreateFolds(self.X, self.MD, xny_df.clustered, [], self.Y, self.FoldsConf, seed=0)

        roc_per_fold, self.feature_ranks = self.feature_ranking(self.X, self.Y, folds)
        self.Folds0 = folds, roc_per_fold

    @staticmethod
    def feature_ranking(xx, yy, folds):
        roc_auc_folds = [None] * len(folds)
        for ii, (train_index, test_index) in enumerate(folds):
            auc_fold = [roc_auc_score(yy[train_index], xx[train_index, i], average='macro') for i in range(xx.shape[1])]
            roc_auc_folds[ii] = [max(1 - x, x) for x in auc_fold]
        auc_x = [roc_auc_score(yy, xx[:, j], average='macro') for j in range(xx.shape[1])]
        roc_auc_x = [max(1 - x, x) for x in auc_x]
        return roc_auc_folds, roc_auc_x

    def Folds(self):  # TODO
        return self.Folds0

    def produce_validation_sets(self, tav, tav_class, curr_features_th):
        for subset in self.validation_sets:
            pr_set = self.DB[tav + '_' + tav_class].datasets[subset]
            y = ProduceDatasets.get_target_vector(pr_set[2].copy(), tav, tav_class, curr_features_th)
            db = (pr_set, y)
            self.cur_iteration_val_sets.update({subset: db})

    def SubsetData(self, subset):
        db, y = self.cur_iteration_val_sets[subset]
        x, feature_names, info_df = db
        # y = ProduceDatasets.get_target_vector(info_df.copy(), tav, tav_class, th)

        info_df = info_df.assign(TrueLabel=y)
        if Cfg.USE_FEATURE_LIST:
            feature_df = pd.read_csv(Cfg.FEATURE_RANK_PATH)
            feature_names = feature_df[feature_df['corr'] >= self.feature_threshold].feature.values
            X = info_df.loc[:, feature_names].as_matrix()

        return x, y, info_df

    def determine_validation_sets(self, tav, tav_class):
        validation_sets = list(self.DB[tav + '_' + tav_class].datasets.keys())
        if 'train' in validation_sets:
            validation_sets.remove('train')
        self.validation_sets = validation_sets

    def get_validation_sets(self):
        return self.validation_sets

    # YL: this method is a wrapper for evaluation since this object is the only one passed to CommonML
    def EvaluateSubset(self, output, subset):
        return [-1], ['N/A']
        # return ML_Eval.EvaluateSubset(output, subset)


def sample_weights_by_cluster(df_info):

    y = df_info.TrueLabel.values

    clusters = np.unique(y, return_counts=True)
    cluster_num = clusters[0]
    if len(cluster_num) != 2:
        print('*** WARNING *** cluster_num != 2 in ExperimentBase->sample_weights_by_cluster')
    cluster_size = clusters[1]
    df_info = df_info.assign(sample_weight=1)

    n = len(y)/2
    # df_info['sample_weight'] = (df_info.TrueLabel == cluster_num[0]).astype(int) * (n / cluster_size[0]) + \
    #                            (df_info.TrueLabel == cluster_num[1]).astype(int) * (n / cluster_size[1])

    df_info['sample_weight'] = np.where(df_info.TrueLabel == cluster_num[0], [n/cluster_size[0]], [n/cluster_size[1]])

    return df_info.sample_weight.values, df_info
