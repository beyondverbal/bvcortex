from datetime import datetime
import logging
from PATrends import PATConfig as PATCfg
from TAVEngine import HLDConfig as HLDCfg
import pandas as pd
TIMESTAMP = datetime.now().strftime('%y-%m-%d')

pd.set_option("display.max_rows", 10)
pd.set_option("display.max_columns", 50)

StdOutLogLevel = logging.INFO
FileLogLevel = logging.WARNING

RELIABILITY_THRESHOLD = 0.6

DBG_FILT10 = False
DBG_OVERRIDE_FOLDS = False

# Needed
OUT_FOLDER = PATCfg.OUTPUT_FOLDER
OUT_PATH_CV = OUT_FOLDER + 'CV/'
OUT_PATH_GRID = OUT_FOLDER + 'GRID/'
OUT_LOGGER = OUT_FOLDER
DATASETS = ['train']  # , 'scripps']  # , 'youtube', 'scripps']

TAV = ['temper', 'arousal', 'valence']
# TAV = ['temper', 'arousal']
TAV_CLASS = ['high', 'low']
# TAV_CLASS = ['high']

AROUSAL_CONDITIONAL = None  # 1  # [None]

cfg_dict_62b = {'temper': {'high': {'M': ['Logistic'], 'C': [2], 'R': [0.85], 'F': [80], 'PCA': [-1], 'Alpha': [2]},
                       'low': {'M': ['Logistic'], 'C': [2], 'R': [1.9], 'F': [-1], 'PCA': [-1], 'Alpha': [2]},
                       'Alpha': [1], 'feature_th': [1.33]},
            'arousal': {'high': {'M': ['Logistic'], 'C': [10], 'R': [1.5], 'F': [-1], 'PCA': [-1], 'Alpha': [2]},
                        'low': {'M': ['Logistic'], 'C': [2], 'R': [1.9], 'F': [140], 'PCA': [-1], 'Alpha': [2]},
                        'Alpha': [1], 'feature_th': [1.33]},
            'valence': {'high': {'M': ['Logistic'], 'C': [10], 'R': [1.3], 'F': [120], 'PCA': [-1], 'Alpha': [2]},
                        'low': {'M': ['Logistic'], 'C': [0.02], 'R': [1.1], 'F': [-1], 'PCA': [-1], 'Alpha': [2]},
                        'feature_th': [1.51]}}
# 63
cfg_dict_63 = {'temper': {'high': {'M': ['Logistic'], 'C': [0.5], 'R': [1], 'F': [80], 'PCA': [-1], 'Alpha': [2]},
                       'low': {'M': ['Logistic'], 'C': [1], 'R': [1.8], 'F': [-1], 'PCA': [-1], 'Alpha': [2]},
                       'Alpha': [1], 'feature_th': [1.33]},
            'arousal': {'high': {'M': ['Logistic'], 'C': [0.5], 'R': [1.5], 'F': [110], 'PCA': [-1], 'Alpha': [2]},
                        'low': {'M': ['Logistic'], 'C': [0.5], 'R': [1.8], 'F': [-1], 'PCA': [-1], 'Alpha': [2]},
                        'Alpha': [1], 'feature_th': [1.33]},
            'valence': {'high': {'M': ['Logistic'], 'C': [0.5], 'R': [1.8], 'F': [-1], 'PCA': [-1], 'Alpha': [2]},
                        'low': {'M': ['Logistic'], 'C': [0.5], 'R': [1.8], 'F': [-1], 'PCA': [-1], 'Alpha': [2]},
                        'feature_th': [1.51]}}

# 63
cfg_dict_63 = {'temper': {'high': {'M': ['Logistic'], 'C': [1], 'R': [0.8], 'F': [80], 'PCA': [-1], 'Alpha': [2]},
                          'low': {'M': ['Logistic'], 'C': [1], 'R': [1.8], 'F': [-1], 'PCA': [-1], 'Alpha': [2]},
                          'Alpha': [1], 'feature_th': [1.33]},
               'arousal': {'high': {'M': ['Logistic'], 'C': [0.1], 'R': [1.4], 'F': [110], 'PCA': [-1], 'Alpha': [2]},
                           'low': {'M': ['Logistic'], 'C': [0.01], 'R': [1], 'F': [110], 'PCA': [-1], 'Alpha': [2]},
                           'Alpha': [1], 'feature_th': [1.33]},
               'valence': {'high': {'M': ['Logistic'], 'C': [1], 'R': [1.8], 'F': [140], 'PCA': [-1], 'Alpha': [2]},
                           'low': {'M': ['Logistic'], 'C': [0.01], 'R': [1], 'F': [-1], 'PCA': [-1], 'Alpha': [2]},
                           'feature_th': [1.51]}}
#64
cfg_dict = {'temper': {'high': {'M': ['Logistic'], 'C': [0.1], 'R': [0.8], 'F': [80], 'PCA': [-1], 'Alpha': [2]},
                          'low': {'M': ['Logistic'], 'C': [1], 'R': [1.8], 'F': [140], 'PCA': [-1], 'Alpha': [2]},
                          'Alpha': [1], 'feature_th': [1.33]},
               'arousal': {'high': {'M': ['Logistic'], 'C': [0.1], 'R': [1.4], 'F': [140], 'PCA': [-1], 'Alpha': [2]},
                           'low': {'M': ['Logistic'], 'C': [0.01], 'R': [1.6], 'F': [110], 'PCA': [-1], 'Alpha': [2]},
                           'Alpha': [1], 'feature_th': [1.33]},
               'valence': {'high': {'M': ['Logistic'], 'C': [5], 'R': [1.8], 'F': [140], 'PCA': [-1], 'Alpha': [2]},
                           'low': {'M': ['Logistic'], 'C': [0.01], 'R': [1.8], 'F': [140], 'PCA': [-1], 'Alpha': [2]},
                           'feature_th': [1.51]}}

FIRST_FEATURE_DICT = {'deciles4': 'mfcc_0_deciles4_0', 'moments8': 'mfcc_0_moments8_0', 'moments4': 'mfcc_0_moments4_0',
                      'deciles5': 'mfcc_0_deciles5_0', 'moments9': 'mfcc_0_moments9_0', 'moments5': 'mfcc_0_moments5_0',
                      'nonagon5': 'mfcc_0_nonagon5_0', 'nonagon4': 'mfcc_0_nonagon4_0'}
if HLDCfg.OPERATOR_LIST[0] in FIRST_FEATURE_DICT.keys():
    FIRST_FEATURE = FIRST_FEATURE_DICT[HLDCfg.OPERATOR_LIST[0]]
else:
    logging.error('No first_feature definition')

RUN_QC_CONFIG = True  # True: 1 selected config, False: grid search

FEATURE_THRESHOLD_LIST = [1.33]  # 1.33 for temper, arsoual, 1.51 for valence ensures similar number as v5.0
REGULAR_C_LIST = [5, 2, 1, 0.5, 0.1, 0.05, 0.01, 0.001]#
MODEL_LIST = ['Logistic']  # ['SVC']  #,
CLASS_RATIO_LIST = [1.8, 1.6, 1.4, 1.2, 1, 0.8]  # []#
PCA_LIST = [-1]
FEAT_SEL_LIST = [-1, 140, 110, 80]  # [-1, 140, 110]
ALPHA_LIST = [2]

HLD_FILE = OUT_FOLDER + 'feature_matrix_MIX' + PATCfg.VERSION + '_' + PATCfg.ENGINE_CONF['name'] + '.csv'
USE_FEATURE_LIST = False
FEATURE_RANK_PATH = 1

# Produce Sets
DETECTOR_STD_MAX = 1.4  # across all 6 detectors, take only std lower than MAX

# ####################### PREDICT SETTING
PREDICT_CONFLICT_BECOMES_MEDIUM = False
VALENCE_DETECTOR_STAND_ALONE = True  # True  # True: do NOT combine temper+arousal to infer valence
IGNORE_VALUES_FROM_0_5_TO_TH = False  # proba values between 0.5 -> TH becomes ambiguous

# ############## Misc settings ###############
DO_GRID_SEARCH = True  # False #
RUN_VALIDATION = True  # True #


emotion_groups = {
    'temper_low_arousal_low_valence_low': 'Sadness/Uncertainty/Boredom',
    'temper_low_arousal_med_valence_low': 'Sadness/Uncertainty/Boredom',
    'temper_med_arousal_med_valence_med': 'Neutral',
    'temper_med_arousal_med_valence_high': 'Warmth/Calmness',
    'temper_med_arousal_high_valence_high': 'Happiness/Enthusiasm/Friendliness',
    'temper_high_arousal_med_valence_low': 'Anger/Dislike/Stress',
    'temper_high_arousal_high_valence_low': 'Anger/Dislike/Stress'}


# ############## dataset settings ###############
TARGET = 'OSA'  # 'CAD' #
FILTER_NON_HEBREW = True  # False #
DROP_DUPLICATE = True  # False #
USE_TEST = False  # True #
GENDER_FILTER = 'all'  # 'F' # 'M' #

# ############## features settings ###############
MULTIFLOW_HLD = False  # True #

# For CV
N_KFOLDS = 10
N_REPEATS = 1
# For stratification
PERMUTATION_NUM = 50



################################ DELETE ALL THOSE SOON ################################

# v5.4.0.1
# HIGH TEMPER 1.33, 0.005, pca=80, R:1.2, M:Logistic
# LOW TEMPER 1.33, c=0.01, pca=-1, M:Logistic, R:1.6
# LOW_AROUSAL CFG 1.51, C:0.005, M:Logistic, R:1.6, PCA -1
# HIGH_AROUSAL CFG 1.51, C:0.005, M:Logistic, R:1.4, PCA -1
# LOW_VALENCE: 1.51, C:0.1, R:1, Logistic, PCA: -1
# HIGH_VALENCE: 1.51, C:0.1, R:1.4, Logistic, PCA: -1

cfg_dict_v5401 = {'temper': {'high': {'M': ['Logistic'], 'C': [0.005], 'R': [1.2], 'F': [-1], 'PCA': [-1]},
                             'low': {'M': ['Logistic'], 'C': [0.01], 'R': [1.6], 'F': [-1], 'PCA': [-1]},
                             'feature_th': [1.33]},
                  'arousal': {'high': {'M': ['Logistic'], 'C': [0.005], 'R': [1.6], 'F': [-1], 'PCA': [-1]},
                              'low': {'M': ['Logistic'], 'C': [0.005], 'R': [1.4], 'F': [-1], 'PCA': [-1]},
                              'feature_th': [1.33]},
                  'valence': {'high': {'M': ['Logistic'], 'C': [0.1], 'R': [1], 'F': [-1], 'PCA': [-1]},
                              'low': {'M': ['Logistic'], 'C': [0.1], 'R': [1.4], 'F': [-1], 'PCA': [-1]},
                              'feature_th': [1.51]}}

cfg_dict_m4_100_der = {'temper': {'high': {'M': ['Logistic'], 'C': [1.5], 'R': [0.8], 'F': [80], 'PCA': [-1]},
                       'low': {'M': ['Logistic'], 'C': [4], 'R': [1.75], 'F': [-1], 'PCA': [-1]},
                       'feature_th': [1.33]},
                       'arousal': {'high': {'M': ['Logistic'], 'C': [0.5], 'R': [1.75], 'F': [-1], 'PCA': [-1]},
                                   'low': {'M': ['Logistic'], 'C': [0.2], 'R': [1], 'F': [-1], 'PCA': [-1]},
                                   'feature_th': [1.33]},
                       'valence': {'high': {'M': ['Logistic'], 'C': [10], 'R': [1.75], 'F': [-1], 'PCA': [-1]},
                                   'low': {'M': ['Logistic'], 'C': [0.1], 'R': [1.3], 'F': [-1], 'PCA': [-1]},
                                   'feature_th': [1.51]}}

INCLUDE_TIME = False  # True #
MD_FEATURES_ONLY = False  # True #
