import logging.config
import os
import time
from shutil import copyfile
import numpy as np
import pandas as pd
from CommonML.BasicGrid import BasicGrid
from TAVTrainer import ConfigTraining as Cfg
from TAVTrainer.ExperimentBase import ExperimentBase
from TAVCommon import log_conf

logging.config.dictConfig(log_conf.create_log_dict_no_file(Cfg.StdOutLogLevel))
cur_logger = logging.getLogger('RunAnalysis')
cur_logger.info('Starting Run :)')

start_time = time.time()
EBObj = ExperimentBase()

if Cfg.DO_GRID_SEARCH:

    if not os.path.exists(Cfg.OUT_PATH_GRID):
        os.makedirs(Cfg.OUT_PATH_GRID)

    src = './TAVTrainer/ConfigTraining.py'
    dst = Cfg.OUT_PATH_GRID + 'config_training.py'
    copyfile(src, dst)

    total_grid_points = len(Cfg.FEATURE_THRESHOLD_LIST) * len(Cfg.TAV) * len(Cfg.TAV_CLASS)
    curr_point = 1

    # run meta grid search wrapping BasicGrid
    for tav in Cfg.TAV:
        for tav_class in Cfg.TAV_CLASS:
            EBObj.determine_validation_sets(tav, tav_class)
            results = pd.DataFrame([])

            if Cfg.RUN_QC_CONFIG:
                data_th = Cfg.cfg_dict[tav]['feature_th']
            else:
                data_th = Cfg.FEATURE_THRESHOLD_LIST
            # for target_th in Cfg.FEATURE_THRESHOLD_LIST:
            # for target_th in Cfg.cfg_dict[tav]['feature_th']:
            for target_th in data_th:
                if tav_class is 'low':
                    target_th = -target_th
                EBObj.split_data(tav, tav_class, target_th)
                if Cfg.RUN_VALIDATION:
                    EBObj.produce_validation_sets(tav, tav_class, target_th)
                mg_results = [tav, tav_class, target_th]
                mg_titles = ['tav', 'tav_class', 'target_th']
                mg_signature = '{}/{} {}_{}, th: {}'.format(curr_point, total_grid_points, tav, tav_class, target_th)

                # TODO CV_OUT DEPENDS ON TAV (e.g. separate for high_temp etc.)
                if not os.path.exists(Cfg.OUT_PATH_CV + tav_class + '_' + tav + '/'):
                    os.makedirs(Cfg.OUT_PATH_CV + tav_class + '_' + tav + '/')

                BGObj = BasicGrid(EBObj, mg_results, mg_titles, mg_signature, Cfg.OUT_PATH_CV+tav_class+'_'+tav+'/')

                if Cfg.RUN_QC_CONFIG:
                    curr_result = BGObj.GridSearch(Cfg.cfg_dict[tav][tav_class]['M'],
                                                   Cfg.cfg_dict[tav][tav_class]['C'],
                                                   Cfg.cfg_dict[tav][tav_class]['R'],
                                                   Cfg.cfg_dict[tav][tav_class]['F'],
                                                   Cfg.cfg_dict[tav][tav_class]['PCA'],
                                                   Cfg.cfg_dict[tav][tav_class]['Alpha'])
                else:
                    curr_result = BGObj.GridSearch(Cfg.MODEL_LIST, Cfg.REGULAR_C_LIST, Cfg.CLASS_RATIO_LIST,
                                                   Cfg.FEAT_SEL_LIST, Cfg.PCA_LIST, Cfg.ALPHA_LIST)

                results = results.append(curr_result)
                curr_point += 1
            results.to_csv(Cfg.OUT_PATH_GRID + tav + '_' + tav_class + '_grid_search.csv', index=False)

print('Done in {} minutes.'.format(np.round((time.time() - start_time) / float(60), decimals=2)))
