import numpy as np
import pandas as pd

import TAVTrainer.ConfigTraining as Cfg
from PATrends import PATConfig as PATCfg
from TAVCommon import log_conf
import logging.config
logging.config.dictConfig(log_conf.create_log_dict_no_file(Cfg.StdOutLogLevel))
cur_logger = logging.getLogger('ProduceDataSets')


class ProduceDatasets:
    def __init__(self, tav, det_class, upper_th=0.99, lower_th=0.99, filter_type='train'):
        self.datasets = {}
        for cur_filter in filter_type:
            self.produce_input(tav, det_class, upper_th, lower_th, cur_filter)
        self.logger = logging.getLogger('ProduceSets')

    def produce_input(self, tav, det_class, upper_th, lower_th, filter_type):

        hld_file = Cfg.HLD_FILE
        logging.debug('HLD file = {}'.format(hld_file))

        if filter_type == 'train':
            md0_file = Cfg.OUT_FOLDER + 'detectors_datasets/' + tav + '_train_set_MIX' + PATCfg.VERSION + '.csv'
            md0 = pd.read_csv(md0_file, index_col=None, encoding='utf-8')
        else:  # validation with specific dataset
            md0_file = Cfg.OUT_FOLDER + 'detectors_datasets/' + tav + '_val_set_MIX' + PATCfg.VERSION + '.csv'
            md0_all = pd.read_csv(md0_file, index_col=None, encoding='utf-8')
            md0 = md0_all.loc[md0_all.sourceName == filter_type]
        hld_df = pd.read_csv(hld_file, index_col=None)

        # use TH from cfg to remove ambiguous samples - BOTH IN TRAIN AND VALIDATION - calculated in main, from Cfg
        shape0 = md0.shape
        md0 = md0.drop(md0[np.logical_and(md0[tav + '_mean'] < upper_th, md0[tav + '_mean'] > lower_th)].index)

        logging.debug('{}_{} subset: {}, dropped {} files with TH'.format(tav, det_class,
                                                                             filter_type, shape0[0]-md0.shape[0]))
        # md0.dropna(subset=['sourceName_y'], inplace=True)  # Remove NaN from source

        if Cfg.AROUSAL_CONDITIONAL:
            md0 = md0.drop(md0[md0.arousal_mean < Cfg.AROUSAL_CONDITIONAL].index)

        # create 'label' e.g. high_temper column, with 1/0 wrt th_upper
        if det_class is 'high':
            md0[det_class + '_' + tav] = np.where(md0[tav + '_mean'] >= upper_th, [1], [0])
        else:
            md0[det_class + '_' + tav] = np.where(md0[tav + '_mean'] <= lower_th, [1], [0])
        hld_df.dropna(inplace=True)

        xny_df = pd.merge(left=md0, right=hld_df, on='wavemd5')

        # Set target vector
        Y = np.array(xny_df[det_class + '_' + tav].values, dtype=bool)

        cols = [c for c in xny_df.columns if not c.startswith('voice_detected')]
        xny_df = xny_df[cols]

        first_x_col = np.where(xny_df.columns.values == Cfg.FIRST_FEATURE)[0][0]
        last_x_col = xny_df.columns.values[-1]
        # print('first feature column {}; last feature column {}'.format(xny_df.columns[first_x_col], last_x_col))

        X = xny_df.iloc[:, first_x_col:].values
        feature_names = xny_df.columns[first_x_col:]

        result = (X, feature_names, xny_df)
        self.datasets.update({filter_type: result})
        # return X, Y, feature_names, xny_df.iloc[:, :first_x_col]

    @staticmethod
    def get_target_vector(xny_df_copy, tav, tav_class, th):

        if tav_class is 'high':
            xny_df_copy = xny_df_copy.assign(target_y=xny_df_copy[tav + '_mean'] >= th)
            cur_logger.info('tav = {}; th={}; {}/{} high'.format(tav, th, xny_df_copy.target_y.values.sum(), len(xny_df_copy[tav + '_mean'].values)))
        elif tav_class is 'low':
            xny_df_copy = xny_df_copy.assign(target_y=xny_df_copy[tav + '_mean'] <= th)
            cur_logger.info('tav = {}; th={}; {}/{} low'.format(tav, th, xny_df_copy.target_y.values.sum(), len(xny_df_copy[tav + '_mean'].values)))
        else:
            cur_logger.error('ERROR!!! in get_target_vector **********************')

        return xny_df_copy.target_y.values
