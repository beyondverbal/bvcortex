import time

import numpy as np
import pandas

from PATrends import PATConfig as PATCfg
from TAVTrainer import ConfigTraining as Cfg

ROOT_DIR = PATCfg.OUTPUT_FOLDER
version = 'MIX'+PATCfg.VERSION


def check_unique(dataset):
    test = pandas.merge(left=dataset, right=dataset, on='FileName')
    diff = test.shape[0] - dataset.shape[0]
    if diff == 0:
        return True
    else:
        return False


def select_set_by_tav(dataset, tav_in):
    dataset_copy = dataset.copy()
    dataset_copy = dataset_copy.loc[dataset_copy[tav_in + '_std'] <= Cfg.DETECTOR_STD_MAX]

    return dataset_copy


def select_test_by_group(dataset, train_val, tav_in):
    dataset_copy = select_set_by_tav(dataset, tav_in)

    filename = ROOT_DIR + 'detectors_datasets/' + tav_in + '_' + train_val + '_set_' + version + '.csv'
    dataset_copy.to_csv(filename, sep=',', index=False)

    print(tav_in + ' ' + train_val + '_set:', dataset_copy.shape)


def main():
    start_time = time.time()

    labels_data = pandas.read_csv(PATCfg.OUTPUT_FOLDER + '/detectors_datasets/TAVLabelsSpeakerID.csv', encoding='ISO-8859-1')
    labels_data = labels_data.rename(columns={'wav_name': 'FileName'})

    # Build unique datasets to include in training or validation
    ############################################################
    moodies = labels_data.loc[labels_data['sourceName_y'] == 'moodies2']
    moodies = moodies.loc[moodies['speakerID'] != 'moodies2_webMoodies']
    moodiesEN = moodies.loc[moodies['country'].str.contains('United States|United Kingdom|Australia|Canada|South Africa|New Zealand')].copy()
    moodiesES = moodies.loc[moodies['country'].str.contains('Mexico|Spain|Brazil|Ecuador|Argentina|Colombia|Chile')].copy()
    moodiesAR = moodies.loc[moodies['country'].str.contains('Saudi Arabia|Egypt|Iraq|Jordan|Syrian Arab Republic|United Arab Emirates|Kuwait|Morocco|Yemen|Oman|Palestinian, State Of|Libya|Algeria|South Sudan|Bahrain|Lebanon')].copy()
    moodiesEU = moodies.loc[moodies['country'].str.contains('Russian Federation|Germany|Netherlands|France|Greece|Italy|Switzerland|Finland|Bulgaria|Sweden|Croatia|Estonia|Belgium|Poland|Romania|Norway|Austria|Ireland|Ukraine|Czech Republic|Denmark|Serbia|Lithuania|Bosnia And Herzegovina')].copy()
    moodiesSEA = moodies.loc[moodies['country'].str.contains('China|Japan|Korea, Republic Of|Hong Kong|Malaysia|Singapore|Indonesia|Taiwan, Province Of China|Viet Nam')].copy()

    moodiesEN.loc[np.array(moodiesEN.index), 'sourceName'] = 'moodies_EN'
    moodiesES.loc[np.array(moodiesES.index), 'sourceName'] = 'moodies_ES'
    moodiesAR.loc[np.array(moodiesAR.index), 'sourceName'] = 'moodies_AR'
    moodiesEU.loc[np.array(moodiesEU.index), 'sourceName'] = 'moodies_EU'
    moodiesSEA.loc[np.array(moodiesSEA.index), 'sourceName'] = 'moodies_SEA'

    atnt = labels_data.loc[labels_data['sourceName_y'] == 'atnt']
    atntC = atnt.loc[atnt['speakerID'].str.contains('customer')].copy()
    atntA = atnt.loc[atnt['speakerID'].str.contains('agent')].copy()
    atntO = atnt.loc[~atnt['speakerID'].str.contains('customer') & ~atnt['speakerID'].str.contains('agent')].copy()

    atntC.loc[atntC.index, 'sourceName'] = 'atnt_C'
    atntA.loc[atntA.index, 'sourceName'] = 'atnt_A'
    atntO.loc[atntO.index, 'sourceName'] = 'atnt_O'

    nielsen = labels_data.loc[labels_data['sourceName_y'] == 'nielsen']
    nielsenEN = nielsen.loc[nielsen['speakerID'].str.contains('US')].copy()
    nielsenES = nielsen.loc[nielsen['speakerID'].str.contains('MX')].copy()
    nielsenEN.loc[nielsenEN.index, 'sourceName'] = 'nielsen_EN'
    nielsenES.loc[nielsenES.index, 'sourceName'] = 'nielsen_ES'

    metavue = labels_data.loc[labels_data['sourceName'] == 'metavue']
    metavueID = metavue.loc[metavue['speakerID'].str.contains('indonesia')].copy()
    metavueRU = metavue.loc[metavue['speakerID'].str.contains('russia')].copy()
    metavueID.loc[metavueID.index, 'sourceName'] = 'metavue_ID'
    metavueRU.loc[metavueRU.index, 'sourceName'] = 'metavue_RU'

    gfk = labels_data.loc[labels_data['sourceName'] == 'gfk']
    nestle = labels_data.loc[labels_data['sourceName'] == 'nestle']
    lrw = labels_data.loc[labels_data['sourceName'] == 'lrw']
    millward_brown = labels_data.loc[labels_data['sourceName'] == 'millward_brown']
    tns = labels_data.loc[labels_data['sourceName'] == 'tns']

    scripps = labels_data.loc[labels_data['sourceName'] == 'scripps']
    mayo = labels_data.loc[labels_data['sourceName'] == 'mayo']
    baidu = labels_data.loc[labels_data['sourceName'] == 'baidu']
    kron = labels_data.loc[labels_data['sourceName'] == 'haifa_assafkron']

    youtube = labels_data.loc[labels_data['sourceCategory'] == 'youtube'].copy()
    youtube.loc[youtube.index, 'sourceName'] = 'youtube'

    # Merge datasets into training and validation sets
    ##################################################

    train_set = pandas.concat([moodiesEN, moodiesES, moodiesEU, nielsenEN, nielsenES, gfk, nestle, lrw, millward_brown,
                               tns, metavueRU])
    val_set = pandas.concat([moodiesSEA, atntA, atntO, scripps, mayo, youtube, baidu, metavueID, atntC, moodiesAR])

    all_samples = pandas.concat([train_set, val_set])
    all_samples.to_csv(ROOT_DIR + 'detectors_datasets/labels_data_MIX' + PATCfg.VERSION + '.csv', sep=',', index=False)

    datasets = {'train': train_set, 'val': val_set}

    print('labels_data: {}'.format(labels_data.shape))
    print('train_set: {}'.format(train_set.shape))
    print('val_set: {}'.format(val_set.shape))

    for train_or_val in ['train', 'val']:
        for tav in ['temper', 'arousal', 'valence']:
            select_test_by_group(datasets[train_or_val], train_or_val, tav)
    t = time.time()
    print('Done. {0:.2f} second runtime ({1:.2f} minutes).'.format(t - start_time, (t - start_time)/float(60)))


if __name__ == "__main__":
    main()
