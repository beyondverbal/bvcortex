from pymongo import MongoClient
from PATrends import PATConfig as PATCfg
import time
import pandas as pd
import numpy as np
import pickle


class Exporter:

    def __init__(self, exclude_empty_tav=True):

        self.tav_strings = ['temper', 'arousal', 'valence']
        self.wav_quality_strings = ['audioQuality', 'authenticity', 'wavSourceDesc']
        self.df_key = 'wavemd5'
        self.label_reduction_dict = \
            {'temper': {'low': -3, 'semi-low': -2, 'semi_low': -2, 'medium': 0, 'semi-high': 2,
                        'high': 3, 'medium_high': 2, 'semi_high': 2, 'ambiguous': np.nan, '0': ''},
             'arousal': {'very_low_activation': -3, 'low_activation': -2,
                         'neutral': 0, 'activated': 2, 'very_activated': 3,
                         'ambiguous': np.nan, '0': ''},
             'valence': {'negative': -3, 'semi-negative': -2, 'semi_negative': -2,
                         'neutral': 0, 'semi-positive': 2, 'positive': 3,
                         'semi_positive': 2, 'ambiguous': np.nan, '0': ''}}

        # self.label_reduction_dict = \
        #     {'temper': {'low': 'low', 'semi-low': 'low', 'semi_low': 'low', 'medium': 'medium', 'semi-high': 'high',
        #                 'high': 'high', 'medium_high': 'high', 'semi_high': 'high', 'ambiguous': 'ambiguous', '0': ''},
        #      'arousal': {'very_low_activation': 'low', 'low_activation': 'low',
        #                  'neutral': 'neutral', 'activated': 'high', 'very_activated': 'high',
        #                  'ambiguous': 'ambiguous', '0': ''},
        #      'valence': {'negative': 'negative', 'semi-negative': 'negative', 'semi_negative': 'negative',
        #                  'neutral': 'neutral', 'semi-positive': 'positive', 'positive': 'positive',
        #                  'semi_positive': 'positive', 'ambiguous': 'ambiguous', '0': ''}}

        self.df = None
        self.exclude_empty_tav = exclude_empty_tav

        self.filename = PATCfg.OUTPUT_FOLDER + 'TAVlabels'
        if self.exclude_empty_tav:
            self.filename += 'Netto'
        self.filename += '.csv'

        self.default_date = '1980-01-01 00:00:00'
        self.require_multi_labeling = True
        self.pull_from_db = False
        self.labelers_black_list = pd.Series.from_csv('./scripts/MongoDB/labelers_black_list.csv')
        self.projects_black_list = pd.Series.from_csv('./scripts/MongoDB/projects_black_list.csv')
        self.all_labelers = []
        self.all_projects = []
        self.docs2find = {}

    def main(self, limit=0):

        start_time = time.time()

        if self.pull_from_db:
            self.client = MongoClient('mongodb://Nazar:BvC2016p@ds044998-a0.mongolab.com:' +
                                      '44998,ds044998-a1.mongolab.com:44996/bvc-api-v3-analisys?replicaSet=rs-ds044998')
            print('using standard DB...')
            self.full_export(limit)
            self.df.to_pickle('C:/BVC/MongoDB/mongo_db_full.pkl')
            with open('C:/BVC/MongoDB/all_labelers.pkl', 'wb') as file:
                pickle.dump(self.all_labelers, file)
            with open('C:/BVC/MongoDB/all_projects.pkl', 'wb') as file:
                pickle.dump(self.all_projects, file)

        else:
            print('using local DB...')
            self.df = pd.read_pickle('C:/BVC/MongoDB/mongo_db_full.pkl')
            with open('C:/BVC/MongoDB/all_labelers.pkl', 'rb') as file:
                self.all_labelers = pickle.load(file)

            with open('C:/BVC/MongoDB/all_projects.pkl', 'rb') as file:
                self.all_projects = pickle.load(file)

        self.get_agreements()
        self.process_file_quality()

        if self.exclude_empty_tav:
            self.empty_tav_exclusion()

        self.calc_seg_order()
        self.reorder_columns()

        self.df.to_csv(path_or_buf=self.filename, index_label=self.df_key)

        delta_t = time.time() - start_time
        print('Done. {} second runtime ({} minutes).'.format(delta_t, delta_t / float(60)))

    def full_export(self, limit):

        col = self.client['bvc-api-v3-analisys'].labelsV3
        counter = 0

        cursor = col.find(self.docs2find).limit(int(limit))
        tot_docs = cursor.count()

        tav_df_merged = []

        for doc in cursor:

            url = ''
            if 'urls' in doc and doc['urls'] is not None:
                url = doc['urls'][0]

            path = ''
            if 'path' in doc and doc['path'] is not None:
                path = doc['path']

            wavemd5 = ''
            if 'wavemd5' in doc and doc['wavemd5'] is not None:
                wavemd5 = doc['wavemd5']

            offset = ''
            duration = ''
            if 'location' in doc and doc['location'] is not None:
                if 'offset' in doc['location']:
                    offset = str(doc['location']['offset'])
                    if offset == 'None':
                        offset = ''
                if 'duration' in doc['location'] is not None:
                    duration = str(doc['location']['duration'])
                    if duration == 'None':
                        duration = ''

            origin_name = ''
            origin_category = ''
            if 'origin' in doc and doc['origin'] is not None:
                if 'source' in doc['origin'][0]:
                    origin_name = str(doc['origin'][0]['source'])
                if 'category' in doc['origin'][0]:
                    origin_category = str(doc['origin'][0]['category'])

            all_relevant_keys = self.tav_strings + self.wav_quality_strings + ['lablers', 'created', 'project']
            record = []
            if 'labels' in doc:
                curr_doc_dict = {i: '' for i in all_relevant_keys}
                curr_doc_dict['wavemd5'] = wavemd5
                curr_doc_dict['origin_category'] = origin_category
                curr_doc_dict['origin_name'] = origin_name
                curr_doc_dict['path'] = path
                curr_doc_dict['segment_duration'] = duration
                curr_doc_dict['segment_offset'] = offset
                curr_doc_dict['wav_url'] = url
                curr_doc_dict['segment_id'] = doc['_id']
                curr_doc_dict['wav_name'] = curr_doc_dict['wav_url'].split(sep='/')[-1]
                for label in doc['labels']:
                    curr_record = curr_doc_dict.copy()
                    at_least_one_label = False
                    for curr_key in list(label.keys()):
                        if curr_key not in all_relevant_keys:
                            continue

                        if curr_key == 'lablers':
                            curr_record[curr_key] = label[curr_key][0]
                        else:
                            curr_record[curr_key] = str(label[curr_key])
                            if not at_least_one_label:
                                at_least_one_label = curr_record[curr_key] != ''
                    curr_record['created'] = curr_record.get('created', self.default_date)

                    excluded = curr_record['lablers'] in self.labelers_black_list or \
                               curr_record['project'] in self.projects_black_list
                    if not excluded and at_least_one_label:
                        record.append(curr_record)

            if not len(record):
                continue

            curr_dict = dict()

            project = [i['project'] for i in record]
            all_labelers = np.array([i['lablers'] for i in record])
            label_date = np.array([i['created'] for i in record])
            all_projects = np.array(project)

            all_cat = self.tav_strings + self.wav_quality_strings

            for curr_cat in all_cat:
                all_curr_cat_vals = np.array([i[curr_cat] for i in record])
                curr_dict[curr_cat + '_raw'] = ';'.join(all_curr_cat_vals) + ';'
                valid = np.logical_and(all_curr_cat_vals != '', all_curr_cat_vals != '0')
                if np.any(valid):
                    curr_dict[curr_cat] = ';'.join(all_curr_cat_vals[valid]) + ';'
                    curr_dict[curr_cat + '_labeler'] = ';'.join(all_labelers[valid]) + ';'
                    curr_dict[curr_cat + '_date'] = ';'.join(label_date[valid]) + ';'
                    self.all_projects += list(all_projects[valid])
                else:
                    curr_dict[curr_cat] = np.nan
                    curr_dict[curr_cat + '_labeler'] = np.nan
                    curr_dict[curr_cat + '_date'] = np.nan

            curr_dict = self.take_most_updated(curr_dict, all_cat)
            for curr_cat in all_cat:
                curr_dict.pop(curr_cat + '_date', None)

            curr_dict['labeler_raw'] = ';'.join(all_labelers)
            curr_dict['label_date_raw'] = ';'.join(label_date)
            curr_dict['comment'] = ''
            curr_dict['path'] = ';'.join(set([i['path'] for i in record]))
            curr_dict['wav_url'] = ';'.join(set([i['wav_url'] for i in record]))
            curr_dict['wav_name'] = ';'.join(set([i['wav_name'] for i in record]))
            curr_dict['wavemd5'] = ';'.join(set([i['wavemd5'] for i in record]))
            curr_dict['project'] = ';'.join(sorted(set(project)))
            curr_dict['origin_name'] = ';'.join(set([i['origin_name'] for i in record]))
            curr_dict['origin_category'] = ';'.join(set([i['origin_category'] for i in record]))

            offset = np.unique([i['segment_offset'] for i in record])
            if len(offset) > 1:
                curr_dict['comment'] = curr_dict['comment'] + '; offset: ' + ';'.join(offset)
                offset = offset[0]
            curr_dict['segment_offset'] = offset

            duration = np.unique([i['segment_duration'] for i in record])
            if len(duration) > 1:
                curr_dict['comment'] = curr_dict['comment'] + '; duration: ' + ';'.join(duration)
                duration = duration[0]
            curr_dict['segment_duration'] = duration

            curr_dict['segment_order'] = ''

            tav_df_merged.append(pd.DataFrame(curr_dict, index=[wavemd5]))

            counter += 1
            if counter == 5000:
                break
            if np.remainder(counter, 1000) == 0:
                print(str(np.around(counter/tot_docs*100, decimals=1)) + '% of docs processed')

        self.df = pd.concat(tav_df_merged, axis=0)

        self.all_labelers = list(set(self.all_labelers))
        self.all_projects = list(set(self.all_projects))

    @staticmethod
    def take_most_updated(dict_to_filt, relevant_categories):

        for curr_cat in relevant_categories:
            if not isinstance(dict_to_filt[curr_cat], str):
                continue
            valid_res = np.array(dict_to_filt[curr_cat].split(';')[:-1])
            valid_res_labeler = np.array(dict_to_filt[curr_cat + '_labeler'].split(';')[:-1])
            valid_res_date = np.array(dict_to_filt[curr_cat + '_date'].split(';')[:-1])
            name_date_cat = []
            for i in range(len(valid_res)):
                name_date_cat += [valid_res_labeler[i] + valid_res_date[i]]
            name_date_cat = np.array(name_date_cat)
            _, idx = np.unique(name_date_cat, return_index=True)
            unq_name_date_cat = name_date_cat[np.sort(idx)]
            new_valid_res = []
            new_valid_res_labeler = []
            for i in range(len(unq_name_date_cat)):
                is_duplicate = np.where(name_date_cat == unq_name_date_cat[i])[0]
                most_updated = is_duplicate[np.argsort(valid_res_date[is_duplicate])[-1]]
                new_valid_res.append(valid_res[most_updated])
                new_valid_res_labeler.append(valid_res_labeler[most_updated])
            dict_to_filt[curr_cat] = ';'.join(new_valid_res) + ';'
            dict_to_filt[curr_cat + '_labeler'] = ';'.join(new_valid_res_labeler) + ';'

        return dict_to_filt

    @staticmethod
    def get_list(df, col):
        val = df[col]
        if len(df.shape) == 1:
            val = (val, )
        else:
            val = val.tolist()
        val = np.array(val)
        return val

    def reorder_columns(self):

        orig = self.df.columns
        cols = ['wav_url', 'wav_name', 'origin_name', 'origin_category']

        for curr_orig in orig:
            if 'segment' in curr_orig:
                cols.append(curr_orig)

        for curr_tav in self.tav_strings:
            for curr_orig in orig:
                if curr_tav in curr_orig:
                    cols.append(curr_orig)

        cols.append('labeler_raw')
        cols.append('label_date_raw')

        for quality in self.wav_quality_strings:
            for curr_orig in orig:
                if quality in curr_orig:
                    cols.append(curr_orig)

        cols = cols + list(set(orig)-set(cols))

        self.df = self.df[cols]
        self.df = self.df.sort_values(['wav_name', 'segment_order'])

    def get_agreements(self):
        counter = 0
        index = self.df.index.values

        reliability_dict = {}
        all_vals_dict = {}
        for curr_index in index:
            counter += 1
            reliability_dict[curr_index] = {}
            all_vals_dict[curr_index] = {}

            for curr_tav in self.tav_strings:
                curr_vals = self.df.get_value(curr_index, curr_tav)
                # curr_vals = self.df.loc(curr_index, curr_tav)
                print('counter = {}; curr_vals = {}'.format(counter, curr_vals))

                curr_reliability = np.nan
                curr_mean = -100  # just to stand out, maybe use np.nan
                curr_std = 10  # high enough to ignore
                num_legit_labels = 0
                all_vals_dict[curr_index][curr_tav + '_all'] = np.nan
                if isinstance(curr_vals, str):
                    curr_vals = np.array(curr_vals.split(';')[:-1])
                    orig_curr_vals_len = len(curr_vals)
                    curr_vals = curr_vals[curr_vals != '0']  # TODO, 0 is legal now
                    all_vals_dict[curr_index][curr_tav + '_all'] = ';'.join(curr_vals)
                    for j in range(len(curr_vals)):
                        curr_vals[j] = self.label_reduction_dict[curr_tav][curr_vals[j].lower()]

                    curr_vals = np.array(curr_vals)
                    curr_vals = curr_vals[curr_vals != '']    # TODO
                    curr_vals = curr_vals[curr_vals != 'nan']

                    curr_vals = np.array(curr_vals, dtype=int)

                    print('**** CUR_VALS = {}; counter = {}; orig_len = {}'.format(curr_vals, counter,orig_curr_vals_len))
                    num_labelers = len(curr_vals)
                    if len(curr_vals) == 0:
                        curr_vals = ''
                    elif len(curr_vals) == 1:
                        curr_vals = ''

                    else:  # more than 1 legit labels
                        curr_std = np.nanstd(curr_vals)
                        curr_mean = np.nanmean(curr_vals)
                        num_legit_labels = len(curr_vals)
                        curr_reliability = num_legit_labels/orig_curr_vals_len
                        unq_vals, unq_vals_counts = np.unique(curr_vals, return_counts=True)
                        curr_vals = unq_vals[unq_vals_counts == np.max(unq_vals_counts)][0]

                reliability_dict[curr_index][curr_tav + '_reliability'] = curr_reliability
                reliability_dict[curr_index][curr_tav + '_mean'] = curr_mean
                reliability_dict[curr_index][curr_tav + '_std'] = curr_std
                reliability_dict[curr_index][curr_tav + '_legit_labels'] = num_legit_labels
                self.df.set_value(curr_index, curr_tav, curr_vals)

        reliability_dict = pd.DataFrame.from_dict(reliability_dict, orient='index')
        all_vals_dict = pd.DataFrame.from_dict(all_vals_dict, orient='index')

        self.df = pd.concat([self.df, reliability_dict, all_vals_dict], axis=1)

    def process_file_quality(self):

        index = self.df.index.values

        for curr_index in index:

            label_name = 'authenticity'
            curr_vals = self.df.get_value(curr_index, label_name)  # TODO
            # curr_vals = self.df.loc(curr_index, label_name)  # TODO
            if curr_vals == curr_vals:  # is not nan
                curr_vals = curr_vals.split(';')[:-1]
                if np.all(np.array(curr_vals) == 'natural'):
                    resulted = 'pass'
                else:
                    resulted = 'fail'
                self.df.set_value(curr_index, label_name, resulted)

            label_name = 'audioQuality'
            curr_vals = self.df.get_value(curr_index, label_name)
            # curr_vals = self.df.loc(curr_index, label_name)
            if curr_vals == curr_vals:  # is not nan
                curr_vals = curr_vals.split(';')[:-1]
                if np.any(np.array(curr_vals) == 'bad'):
                    resulted = 'fail'
                else:
                    resulted = 'pass'
                self.df.set_value(curr_index, label_name, resulted)

            label_name = 'wavSourceDesc'
            curr_vals = self.df.get_value(curr_index, label_name)
            # curr_vals = self.df.loc(curr_index, label_name)
            if curr_vals == curr_vals:  # is not nan
                curr_vals = curr_vals.split(';')[:-1]
                is_new = ['old' not in i for i in curr_vals]
                if np.any(is_new):
                    resulted = curr_vals[np.where(is_new)[0][0]]
                else:
                    resulted = 'old'
                self.df.set_value(curr_index, label_name, resulted)

    def empty_tav_exclusion(self):

        index = self.df.index.values
        keys_to_keep = []

        for curr_index in index:

            curr_wav_url = self.df.get_value(curr_index, 'wav_url')
            # curr_wav_url = self.df.loc(curr_index, 'wav_url')
            if curr_wav_url != curr_wav_url or curr_wav_url == '':  # curr_wav_url is nan
                continue

            any_tav_val = []
            for curr_tav in self.tav_strings:
                curr_tav_val = self.df.get_value(curr_index, curr_tav + '_all')
                # curr_tav_val = self.df.loc(curr_index, curr_tav + '_all')
                if curr_tav_val == curr_tav_val:
                    curr_tav_val_wo_semicolon = ''.join(curr_tav_val.split(';'))
                    curr_tav_val_wo_semicolon0 = ''.join(curr_tav_val_wo_semicolon.split('0'))
                    if curr_tav_val_wo_semicolon0 is not '':
                        any_tav_val.append(curr_tav_val)
            if any_tav_val:
                keys_to_keep.append(curr_index)

        self.df = self.df.loc[keys_to_keep, :]

    def calc_seg_order(self):

        index = self.df.index.values
        wav_name = self.get_list(self.df, 'wav_name')
        seg_offset = self.get_list(self.df, 'segment_offset')
        seg_offset_float = []
        for i in seg_offset:
            if i == '':
                seg_offset_float.append(np.nan)
            else:
                seg_offset_float.append(float(i))
        seg_offset_float = np.array(seg_offset_float)

        for curr_wav in list(set(wav_name)):

            ind_wav = np.where(wav_name == curr_wav)[0]
            sorted_offset = np.argsort(seg_offset_float[ind_wav])
            for i in range(len(sorted_offset)):
                self.df.set_value(index[ind_wav[sorted_offset[i]]], 'segment_order', i)


if __name__ == "__main__":

    obj = Exporter()
    obj.main(limit=0)
