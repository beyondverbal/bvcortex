import pandas as pd
import numpy as np
from PATrends import PATConfig as PATCfg
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
import csv


def calc_metrics(y_test, predictions, model_classes):
    accuracy = accuracy_score(y_test, predictions)
    precision = precision_score(y_test, predictions, average=None)
    recall = recall_score(y_test, predictions, average=None)
    f1 = f1_score(y_test, predictions, average=None)

    label_count = np.unique(y_test, return_counts=True)
    count_dict = dict(zip(label_count[0], label_count[1]))
    count = []
    for idx in range(len(model_classes)):
        try:
            count.append(count_dict[model_classes[idx]])
        except:
            # Happens if a class is missing from y_test due to low stats
            count.append(0)
            if len(precision) < 2:   # To DO: make this '2' more general
                precision = np.insert(precision, idx, 0)
                recall = np.insert(recall, idx, 0)
                f1 = np.insert(f1, idx, 0)

    precision = np.append(precision, np.mean(precision))
    recall = np.append(recall, np.mean(recall))
    f1 = np.append(f1, np.mean(f1))

    result = {'accuracy': accuracy, 'labels': model_classes, 'precision': precision, 'recall': recall,
              'f1': f1, 'count': count}
    return result


file_in = PATCfg.OUTPUT_FOLDER + 'CV/CV_info_pos_valence_th_2.csv'
file_out = PATCfg.OUTPUT_FOLDER + 'CV/CV_info_pos_valence_th_2_reject.csv'
df = pd.read_csv(file_in)
det_class = 'positive'  # TODO
tav = 'valence'  # TODO
upper_th = 2
lower_th = 2
Truth = df.TrueLabel.values
prob = df.score.values

# result_cv = pd.DataFrame({'TH': ['Sensitivity', 'Specificity', 'Precision','PrecisionAdjusted' ,'PassRate', 'pred_true']})
result_cv = pd.DataFrame({'Threshold': ['DetectCount', 'RejectRate', 'Sensitivity', 'Specificity', 'Precision',
                                        'PrecisionAdjusted']})


for i, conf_th in enumerate(np.round(np.linspace(0.5, 0.9, num=9), 2)):
    result_new_th = []
    predictions = prob >= conf_th
    result_eer = calc_metrics(Truth, predictions, [False, True])
    if i == 0:
        rr_baseline = predictions.sum()

    tp = np.array(np.logical_and(predictions == 1, Truth == 1), dtype=int).sum()
    fn = np.array(np.logical_and(predictions == 0, Truth == 1), dtype=int).sum()
    tn = np.array(np.logical_and(predictions == 0, Truth == 0), dtype=int).sum()
    fp = np.array(np.logical_and(predictions == 1, Truth == 0), dtype=int).sum()

    pred_between_05_and_th = np.logical_and(prob >= 0.5, prob < conf_th)
    add_to_tp = np.logical_and(Truth, pred_between_05_and_th)
    tp2 = tp + add_to_tp.sum()

    result_new_th += [int(predictions.sum())]  # num
    result_new_th += [1 - np.round(predictions.sum() / rr_baseline, 2)]
    result_new_th += [np.round(result_eer['recall'][1], 2)]  # sens
    result_new_th += [np.round(result_eer['recall'][0], 2)]  # spec
    result_new_th += [np.round(result_eer['precision'][1], 2)]  # PRECISION
    result_new_th += [np.round(tp2/(tp2+fp), 2)]  # PRECISION

    result_cv[str(conf_th)] = result_new_th
result_cv.to_csv(file_out, index=None)  # , mode='a', header=True

with open(file_out, 'a', newline='') as file:
    csv_writer = csv.writer(file, delimiter=',')
    csv_writer.writerow(['Detector', 'upper TH', 'lower TH', 'Total_Count', 'True_Count'])
    csv_writer.writerow([det_class + '_' + tav, upper_th, lower_th, len(Truth), int(Truth.sum())])
    csv_writer.writerow([' '])