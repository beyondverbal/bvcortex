import pandas
from PATrends import PATConfig as PATCfg

speakerID_df = pandas.read_csv('./metadata/metadata.csv', encoding='ISO-8859-1',
                               low_memory=False)[['wavemd5']+['sourceCategory']+['sourceName']+['sourceName_y'] +
                                                 ['sourceBatch']+['country']+['speakerID']]

# TAVlabels = pandas.read_csv('./scripts/MongoDB/TAVlabelsNetto.csv', encoding='ISO-8859-1')[
TAVlabels = pandas.read_csv(PATCfg.OUTPUT_FOLDER + 'TAVlabelsNetto.csv', encoding='ISO-8859-1')[
    ['wavemd5'] + ['wav_name'] + ['segment_duration']+['segment_offset'] + ['segment_order'] + ['audioQuality'] +
    ['temper'] + ['temper_reliability'] + ['temper_mean'] + ['temper_std'] + ['temper_legit_labels'] +
    ['arousal'] + ['arousal_reliability'] + ['arousal_mean'] + ['arousal_std'] + ['arousal_legit_labels'] +
    ['valence'] + ['valence_reliability'] + ['valence_mean'] + ['valence_std'] + ['valence_legit_labels'] +
    ['authenticity']]

combined_df = pandas.merge(left=TAVlabels, right=speakerID_df, on='wavemd5', how='left')

# Fill empty speakerID with a running integer
empty_indices = combined_df[combined_df['speakerID'].isnull()].index
for i, index in enumerate(empty_indices):
    combined_df.set_value(index, 'speakerID', 'None_'+str(i))

combined_df.to_csv(PATCfg.OUTPUT_FOLDER + 'TAVlabelsSpeakerID.csv', index=False)

print('TAVlabels.shape = ', TAVlabels.shape)
print('speakerID_df.shape = ', speakerID_df.shape)
print('combined_df.shape = ', combined_df.shape)
