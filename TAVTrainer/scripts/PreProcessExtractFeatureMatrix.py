import csv
import logging.config
import operator
import time
from functools import partial
from multiprocessing import Pool

import numpy as np
import pandas as pd

import TAVCommon.AuxFunctions as AuxFunc
from PATrends import PATConfig as PATCfg
from PATrends.PATCalculator import PATCalculator
from TAVEngine import HLDConfig as HLDCfg
from TAVCommon import log_conf
from TAVEngine.HLDCalculator import HLDCalculator

logging.config.dictConfig(log_conf.create_log_dict_no_file(PATCfg.STD_OUT_LEVEL))
cur_logger = logging.getLogger('RunAnalysis')
cur_logger.info('Starting Run :)')

root = PATCfg.OUTPUT_FOLDER
wavs_folder = 'C:/BVC/DownloadedVoices/'
in_file = root + '/detectors_datasets/labels_data_MIX' + PATCfg.VERSION + '.csv'
in_file_pd = pd.read_csv(in_file)
# output_file = root + 'feature_matrix_MIX' + PATCfg.VERSION + '_D4_100_orig_TEST.csv'
output_file = root + 'feature_matrix_MIX' + PATCfg.VERSION + '_' + PATCfg.ENGINE_CONF['name'] + '.csv'
total = 0


def result_vec2df(wav, md5, offset_ms, i, duration_ms, vad_sum, feature_vector):

    prefix = np.array([wav, offset_ms, md5, i, duration_ms, vad_sum])
    return np.concatenate((prefix, feature_vector))


def do_iteration(ii):
    globals()['total'] = globals()['total'] + 1

    if not (globals()['total'] % 100):
        print('total = {}'.format(globals()['total']))

    row = in_file_pd.iloc[ii, :]
    wav = row['FileName'] + '.wav'
    md5 = row['wavemd5']
    seg_offset = row['segment_offset']  # milliseconds
    seg_duration = row['segment_duration']
    ms2samples = PATCfg.SAMPLING_RATE/1000
    full_smpls = HLDCfg.SEGMENT_LENGTH * PATCfg.SAMPLING_RATE

    dict = {'wav': wav, 'md5': md5, 'seg_offset': seg_offset, 'seg_duration': seg_duration,
            'ms2samples': ms2samples, 'full_smpls': full_smpls, 'folder_path': 'C:/BVC/DownloadedVoices/'}

    signal, fs = AuxFunc.read_wav(dict['folder_path'] + dict['wav'], PATCfg.FRAME_SIZE_FEATURES, PATCfg.SAMPLING_RATE)

    if signal is [] or fs != PATCfg.SAMPLING_RATE:
        logging.debug('{}'.format(dict['folder_path'] + dict['wav']))

    else:
        new_signal, offset_smpls, dur_smpls = AuxFunc.calc_new_signal(signal, seg_offset, seg_duration, ms2samples)

        if len(new_signal) < dict['full_smpls']:
            print('******* Signal too short. len = {} secs, wav = {}'.format(len(signal), wav))

        lld_calc = PATCalculator()
        hld_calc = HLDCalculator()

        lld_dictionary, lld_params = lld_calc.process_chunk(new_signal)

        target, _ = lld_calc.PATManager.calc_num_of_frames(10, PATCfg.FRAME_SIZE_FEATURES_SEC,
                                                           PATCfg.FRAME_STEP_FEATURES_SEC)
        lld_params['residual'] = target * 2
        vad = np.array(lld_dictionary['pitch'], dtype=bool)[0]

        for ii, val in enumerate(lld_dictionary['pitch_th_cond'][0]):
            if val >= PATCfg.D_NORM_THRESHOLD:
                vad[ii] = False

        steps = len(vad) - target
        if steps < 0:
            if steps < -1:
                cur_logger.warning('steps NEGATIVE; i = {}; wav = {}; md5 = {};'.format(ii, wav, md5))
            steps = 0
        vad_result = np.zeros(steps)

        if steps > 0:
            for vad_step in range(0, steps):
                vad_result[vad_step] = vad[vad_step: vad_step + target].sum()

            max_index, max_value = max(enumerate(vad_result), key=operator.itemgetter(1))

            st = max_index
            nd = max_index + target
            dict1 = lld_dictionary

            dic_trunc = {}  # apply st:nd to features
            for key in dict1.keys():
                dic_trunc.update({key: dict1[key][:, st: nd]})
            new_start = offset_smpls / dict['ms2samples'] + max_index * PATCfg.FRAME_STEP_FEATURES / dict['ms2samples']
        else:
            dic_trunc = lld_dictionary
            new_start = offset_smpls / dict['ms2samples']

        lld_dict_aug, hld_features, ts, my_vad, processed_secs = hld_calc.process_matrix(dic_trunc, lld_params)

        if len(hld_features) > 0:
            row = (result_vec2df(dict['wav'], dict['md5'], offset_smpls / dict['ms2samples'], new_start,
                   dur_smpls / dict['ms2samples'], my_vad[0].mean(), hld_features[0]))

            with open(output_file, 'a', newline='') as file:
                writer = csv.writer(file, delimiter=',')
                writer.writerow(row)

        lld_calc.clean_up()


def main():
    info_titles = np.array(['FileName', 'Offset', 'wavemd5', 'Start', 'Duration', 'VADSumVoiced'])
    feature_titles = HLDCalculator.get_hld_features_titles()
    feature_titles = np.concatenate((info_titles, feature_titles))

    with open(output_file, 'w', newline='') as file:
        writer = csv.writer(file, delimiter=',')
        writer.writerow(feature_titles)

    l = len(in_file_pd)
    print('total {} files *** NO PARALLEL'.format(l))
    for i in np.arange(0, l):
        print('analyzing file no. {}'.format(i))
        do_iteration(i)


def main_parallel():

    info_titles = np.array(['FileName', 'Offset', 'wavemd5', 'Start', 'Duration', 'VADSumVoiced'])
    feature_titles = HLDCalculator.get_hld_features_titles()
    feature_titles = np.concatenate((info_titles, feature_titles))

    with open(output_file, 'w', newline='') as file:
        writer = csv.writer(file, delimiter=',')
        writer.writerow(feature_titles)

    pool = Pool(processes=3)
    func = partial(do_iteration)
    length = len(in_file_pd)
    print('total {} files *** RUNNING PARALLEL'.format(length))
    rng = np.arange(0, length)
    # rng = np.arange(0, 4)
    pool.map(func, rng)
    pool.close()
    pool.join()


if __name__ == "__main__":

    st_time = time.time()

    # main_parallel()
    main()

    time_dif = time.time() - st_time
    print('Done. {0:.2f} second runtime ({1:.2f} minutes).'.format(time_dif, time_dif / float(60)))
