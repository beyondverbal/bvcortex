import config_training as cfg_train
import pandas as pd
import numpy as np
import datetime
import time
import random

def select_ten_points(times, idx_list, point_num=10):
    if point_num >= len(times):
        return list(idx_list)
    keep_idx = []
    deciles = np.percentile(times, np.arange(10, 100, 10))
    deciles = np.insert(deciles, 0, 0)
    deciles = np.append(deciles, np.max(times))
    for i in range(point_num):
        time_decile = np.where((times>deciles[i]) & (times<=deciles[i+1]))[0]
        pick_idx = random.choice(time_decile)
        keep_idx.append(idx_list[pick_idx])
    return keep_idx

FULL_HLD_FILE = 'E:/Algo/dat/HLD/HLD180321_segments.csv'
NEW_HLD_FILE = 'E:/Algo/dat/HLD/HLD180321.csv'

md0 = pd.DataFrame.from_csv(cfg_train.MD1_FILE, index_col=None)[['filename']+['patient_id']+['rec_date']]
hld = pd.DataFrame.from_csv(FULL_HLD_FILE, index_col=None)

merged_df = pd.merge(left=md0, right=hld, on='filename')

times = []
rec_time = merged_df.rec_date.values.astype(str)
start_times = merged_df.start.values
for idx in range(merged_df.shape[0]):
    curr_rec_time = datetime.datetime.strptime(rec_time[idx], '%Y-%m-%d %H:%M:%S')
    add_sec = start_times[idx]
    abs_seconds = time.mktime(curr_rec_time.timetuple()) + add_sec
    times.append(abs_seconds)
merged_df = merged_df.assign(segment_second=times)

# keep only 10 segments per file
file_list = np.unique(merged_df.filename.values)
keep_idx = []
for file in file_list:
    file_df = merged_df[merged_df['filename']==file]
    keep_idx += select_ten_points(file_df.segment_second.values, file_df.index)
merged_df = merged_df.iloc[keep_idx,:]
merged_df.reset_index(inplace=True, drop=True)

# keep only 10 segments per patient
patient_list = np.unique(merged_df.patient_id.values)
keep_idx = []
for patient in patient_list:
    patient_df = merged_df[merged_df['patient_id'] == patient]
    idx = select_ten_points(patient_df.segment_second.values, patient_df.index)
    if len(idx)<10:
        print('Patient {} has only {} segments.'.format(patient,len(idx)))
    keep_idx += idx
merged_df = merged_df.iloc[keep_idx,:]

cols = hld.columns
merged_df = merged_df[cols]

merged_df.to_csv(NEW_HLD_FILE, index=False)
