import numpy as np
import pandas as pd
from scipy import stats


def load_df(bmkr_file, hosps_file):
    print (bmkr_file + ' ' + hosps_file)
    Scores = pd.read_csv(bmkr_file)
    Scores.drop_duplicates(subset='filename', keep='first', inplace=True)

    MD = pd.read_csv(hosps_file)
    ScoreVsMD = pd.merge(left=Scores, right=MD, on='filename')

    # fix gender
    # if ScoreVsMD['patient_gender'].dtype is not np.dtype('bool'):
    #     gender_int = ScoreVsMD['patient_gender'].values == 'M'
    #     ScoreVsMD = ScoreVsMD.assign(patient_gender=gender_int)

    return ScoreVsMD


if __name__ == '__main__':
    bmkr_file = 'E:/Algo/DataExperiments/vtlt23/all/M8_181004pPaper2.3/all_chf_info.csv'
    hosps_file = 'E:/Algo/dat/MetaData/PackedHosp1811125.csv'
    ScoreVsMD = load_df(bmkr_file, hosps_file)
    ScoreVsMD.rename(index=str, inplace=True, columns={'patient_id_y': 'patient_id'})
    ScoreVsMD.to_csv('E:/Algo/DataExperiments/vtlt_cc/hospsNbiomarkerOfCHFsamples.csv')
    bb=0

    all_patients = ScoreVsMD[['patient_id', 'language', 'patient_gender', 'patient_age', 'mortality']].copy()
    all_patients.drop_duplicates(subset='patient_id', keep='first', inplace=True)
    all_patients = all_patients.assign(sample_count=0)
    all_patients = all_patients.assign(cc=0.)
    all_patients = all_patients.assign(std_hosps=0.)
    all_patients = all_patients.assign(std_score=0.)

    for p_i, p_r in all_patients.iterrows():
        if (p_i == 56):
            bb=0
        print(p_i)
        patient_records = ScoreVsMD[ScoreVsMD['patient_id'] == p_r['patient_id']]
        all_patients.sample_count[p_i] = patient_records.shape[0]
        all_patients.language[p_i] = ','.join(map(str,list(set(patient_records.language.values))))
        if all_patients.sample_count[p_i] < 3:
            bb=0
        else:
            r = stats.pearsonr(patient_records.hosp_in_180d, patient_records.score)
            all_patients.cc[p_i] = r[0]
            all_patients.std_hosps[p_i] = np.std(patient_records.hosp_in_180d)
            all_patients.std_score[p_i] = np.std(patient_records.score)

    all_patients.to_csv('E:/Algo/DataExperiments/vtlt_cc/cc2hosps.csv')
    bb=0

