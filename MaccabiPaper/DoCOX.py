import numpy as np
import pandas as pd
from lifelines import CoxPHFitter
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve

PAPER_FILTER_ONLINE = False
FILTER_FILE = 'E:/Algo/DataExperiments/vtlt23/all/M8_181004pPaper2.3/benchmark_sample.csv'

def load_df(csv_file):
    ScoreVsMD = pd.read_csv(csv_file)

    # remove other languages
    ScoreVsMD = ScoreVsMD[(ScoreVsMD.language != ScoreVsMD.language) | (ScoreVsMD.language == 'russian') | (ScoreVsMD.language == 'hebrew')]
    # fix gender
    if ScoreVsMD['patient_gender'].dtype is not np.dtype('bool'):
        gender_int = ScoreVsMD['patient_gender'].values == 'M'
        ScoreVsMD = ScoreVsMD.assign(patient_gender=gender_int)

    return ScoreVsMD


def FilterAsPaper(ScoreVsMD):
    # remove duplicate calls (keep only rows for latest call per patient) - not relevant for dana's excels
    ScoreVsMD.sort_values(['patient_id', 'duration'], ascending=[True, True], inplace=True)
    ScoreVsMD.drop_duplicates(subset='patient_id', keep='first', inplace=True)

    ScoreVsMD = ScoreVsMD[ScoreVsMD.duration >= 0]  # TODO: elsewhere - here, just for stats / removes zombie patients' recordings

    if PAPER_FILTER_ONLINE:
        ScoreVsMD.to_csv(FILTER_FILE, index=False, columns=['filename'])

    return ScoreVsMD

def cox_regression(ScoreVsMD, event_col='mortality', duration_col='duration', predictors=['score']):
    data_cox = ScoreVsMD[[event_col, duration_col] + predictors]
    data_cox = data_cox[data_cox[duration_col] >= 0]  # removes zombie patients

    cf = CoxPHFitter()
    cf.fit(data_cox, duration_col, event_col=event_col)
    # cf.print_summary()

    results = {}
    for col in predictors:
       results.update({col: cf.summary.loc[col, ['exp(coef)', 'p']].values})
    return results

def VariousCOX(ScoreVsMD1):
    print(ScoreVsMD1.shape[0], sum(ScoreVsMD1.mortality))

    q4 = ScoreVsMD1.score.quantile(.75)
    HighQ = (np.where(ScoreVsMD1.score < q4, 0, 1))  # for q1-q3 assign 0, otherwise assign 1
    ScoreVsMD1 = ScoreVsMD1.assign(HighQ=HighQ)

    results = cox_regression(ScoreVsMD1, predictors=['score', 'patient_age', 'patient_gender'])
    print(results)

    results = cox_regression(ScoreVsMD1, predictors=['HighQ', 'patient_age', 'patient_gender'])
    print(results)

    ScoreN = ScoreVsMD1.score / np.std(ScoreVsMD1.score)
    ScoreVsMD1 = ScoreVsMD1.assign(ScoreN=ScoreN)
    results = cox_regression(ScoreVsMD1, predictors=['ScoreN', 'patient_age', 'patient_gender'])
    print(results)

    DistN = ScoreVsMD1.DistNormed / np.std(ScoreVsMD1.DistNormed)
    ScoreVsMD1 = ScoreVsMD1.assign(DistN=DistN)
    results = cox_regression(ScoreVsMD1, predictors=['DistN', 'patient_age', 'patient_gender'])
    print(results)

    if False:
        cnnScoreN = ScoreVsMD1.cnn_score / np.std(ScoreVsMD1.cnn_score)
        ScoreVsMD1 = ScoreVsMD1.assign(cnnScoreN=cnnScoreN)
        results = cox_regression(ScoreVsMD1, predictors=['cnnScoreN', 'patient_age', 'patient_gender'])
        print(results)

        results = cox_regression(ScoreVsMD1, predictors=['ScoreN', 'cnnScoreN', 'patient_age', 'patient_gender'])
        print(results)


if __name__ == '__main__':
    csv_file = 'E:\\Algo\\DataExperiments\\vtlt23\\all\\M8_181004pPaper2.3\\all_chf_info.csv'
    ScoreVsMD = load_df(csv_file)

    if PAPER_FILTER_ONLINE:
        ScoreVsMD = FilterAsPaper(ScoreVsMD)
    else:
        benchmark_sample = pd.read_csv(FILTER_FILE)
        ScoreVsMD = pd.merge(left=ScoreVsMD, right=benchmark_sample, on='filename')
        ScoreVsMD = FilterAsPaper(ScoreVsMD)

    if False:
        # patch to analyze Moti's CNN
        cnn_scores = pd.read_csv('E:/Algo/Yotam/Docs/CNNvitalityScores.csv')
        ScoreVsMD = pd.merge(left=ScoreVsMD, right=cnn_scores, on='patient_id')
    if True:
        Y = (ScoreVsMD['patient_age'].values > 88) | (ScoreVsMD['mortality'].values == 1)
        print('auc(score) = ', roc_auc_score(Y, ScoreVsMD['score'].values, average='macro'))
        false_pos_rate, true_pos_rate, thresholds = roc_curve(Y, ScoreVsMD['score'].values, drop_intermediate=True)
        ROC_df = pd.DataFrame(np.hstack((
                                np.expand_dims(thresholds, axis=1),
                                np.expand_dims(false_pos_rate, axis=1),
                                np.expand_dims(true_pos_rate, axis=1)
                                )),
                     columns = ['thresholds', 'false_pos_rate', 'true_pos_rate'])

    # FilterBenchmark:
    # BenchmarkSamples = pd.read_csv('E:\\Algo\\dat\\MetaData\\BenchmarkRecordings.csv')
    # ScoreVsMDF = pd.merge(left=ScoreVsMD, right=BenchmarkSamples, on='filename')
    # VariousCOX(ScoreVsMDF)

    # ScoreVsMD1.to_csv('E:\\Algo\\Yotam\\Paper\\ScoreVsMD2.3.csv')
    print (csv_file)
    VariousCOX(ScoreVsMD)
