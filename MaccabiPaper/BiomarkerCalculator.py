from sklearn.externals import joblib
import pandas as pd
import numpy as np
import datetime
import math
from lifelines import CoxPHFitter
from lifelines import KaplanMeierFitter
from lifelines.plotting import add_at_risk_counts
from matplotlib import pyplot as plt
import config_training as cfg_train
from sklearn.metrics import classification_report

def add_time_features(df):
    rec_time = df.rec_date.values.astype(str)
    sin_time = []
    cos_time = []
    sin_2time = []
    cos_2time = []
    for h in rec_time:
        curr_rec_time = datetime.datetime.strptime(h, '%Y-%m-%d %H:%M:%S')
        # curr_rec_time = datetime.datetime.strptime(h, '%d/%m/%y %H:%M')
        time_in_hours = curr_rec_time.hour + curr_rec_time.minute / 60. + curr_rec_time.second / 3600.
        time_in_radians = time_in_hours * math.pi / 12.

        sin_time.append(math.sin(time_in_radians))
        cos_time.append(math.cos(time_in_radians))
        sin_2time.append(math.sin(2 * time_in_radians))
        cos_2time.append(math.cos(2 * time_in_radians))

    df = df.assign(sin_time=sin_time)
    df = df.assign(cos_time=cos_time)
    df = df.assign(sin_2time=sin_2time)
    df = df.assign(cos_2time=cos_2time)

    return df

def clean_columns(hld_df):

    cols = [c for c in hld_df.columns if not c.startswith('voice_detected')]
    hld_df = hld_df[cols]

    return hld_df


def calc_biomarker(X, model):
    x_tranformed = model.named_steps['standardize'].transform(X)
    steps = list(model.named_steps.keys())

    if 'pca' in steps:
        x_tranformed = model.named_steps['pca'].transform(x_tranformed)

    if 'SVM' in steps:
        a = model.named_steps['SVM'].coef_
        b = model.named_steps['SVM'].intercept_

    if 'LR' in steps:
        a = model.named_steps['LR'].coef_
        b = model.named_steps['LR'].intercept_

    try:
        x_value = (np.dot(a, x_tranformed.T) + b)[0]
    except:
        print('********* Invalid model! *********')
        x_value = [0] * x_tranformed.shape[0]

    return x_value

def predict(X, Y, model):
    predictions = model.predict(X)
    print(classification_report(Y, predictions))

def cox_regression(data):
    data_cox = data[['patient_age', 'mortality', 'duration', 'biomarker']]
    gender_int = (data['CUSTOMER_SEX'].values == 'M').astype(int)
    data_cox = data_cox.assign(Gender=gender_int)
    data_cox = data_cox[data_cox['duration']>=0]  # removes zombie patients
    cf = CoxPHFitter()
    cf.fit(data_cox, 'duration', event_col='mortality')
    cf.print_summary()

    return {'biomarker': cf.summary.loc['biomarker',['exp(coef)','p']].values,
            'patient_age': cf.summary.loc['patient_age',['exp(coef)','p']].values,
            'Gender': cf.summary.loc['Gender',['exp(coef)','p']].values}

def km_curve(df, all_quartile=False):
    data = df.copy()

    quartiles = pd.qcut(data['biomarker'], 4, labels=False).values
    data = data.assign(quartiles=quartiles)

    q4vs_rest = np.where(data.quartiles < 3, 0, 1)
    data = data.assign(q4vs_rest=q4vs_rest)

    T = data['duration'].values
    C = data['mortality'].values

    ax = plt.subplot(111)
    t = np.linspace(0, np.median(data.duration), 25)
    # t = np.linspace(0, 600, 25)

    if all_quartile:
        for i in np.unique(data.quartiles.values):
            kmf = KaplanMeierFitter()
            iq = np.where(data.quartiles==i)[0]
            ax = kmf.fit(T[iq],C[iq], label='Q'+str(i+1), timeline=t).plot(ax=ax)
    else:
        kmf1 = KaplanMeierFitter()
        kmf2 = KaplanMeierFitter()
        # iq_0 = np.where(data.q4vs_rest==0)[0]
        # iq_1 = np.where(data.q4vs_rest==1)[0]
        # ax = kmf1.fit(T[iq_0],C[iq_0], label='Q1-Q3', timeline=t).plot(ax=ax)
        # ax = kmf2.fit(T[iq_1],C[iq_1], label='Q4', timeline=t).plot(ax=ax)
        iq_0 = np.where(data.quartiles==0)[0]
        iq_1 = np.where(data.quartiles==3)[0]
        ax = kmf1.fit(T[iq_0],C[iq_0], label='Q1', timeline=t).plot(ax=ax)
        ax = kmf2.fit(T[iq_1],C[iq_1], label='Q4', timeline=t).plot(ax=ax)
        add_at_risk_counts(kmf1, kmf2)

    plt.xlabel('Time (Days)')
    plt.ylabel('Survival Probability (%)')
    plt.title('Survival Probability as a function of the Vocal Biomarker')
    plt.show()

def load_md(input_file):
    patients_df = pd.DataFrame.from_csv(input_file, index_col=None)
    return patients_df

def process_md(md_file, recording_file):
    md_df = pd.DataFrame.from_csv(md_file, index_col=None)
    recording_file = pd.DataFrame.from_csv(recording_file, index_col=None)

    md_df = pd.merge(left=md_df, right=recording_file, on='patient_id')

    # filter 'used' samples (training + chf test)
    # md_df = md_df.loc[md_df.used_patient == 0]

    # filter training samples only
    md_df = md_df.loc[(md_df.used_patient == 0) | ((md_df.used_patient == 1) & (md_df.CARDIO_CHF_CD == 1))]

    if cfg_train.FILTER_NON_HEBREW:
        md_df = md_df.loc[(md_df.language != md_df.language) | (md_df.language == 'hebrew')]  # keeps NaN and hebrew

    if cfg_train.DROP_DUPLICATE:
        md_df.sort_values(by=['rec_date'], inplace=True)
        md_df.drop_duplicates(subset='patient_id', keep='last', inplace=True)

    # Calculate age at time of recording
    file_date = md_df['rec_date'].values
    file_year = [int(x[:4]) for x in file_date]
    patient_age = file_year - md_df.CUSTOMER_BIRTH_Year.values
    md_df = md_df.rename(columns={'CUSTOMER_BIRTH_Year': 'patient_age'})
    md_df['patient_age'] = patient_age

    # Is dead?
    # is_dead = (md_df['DATE_DEATH'].values != '12/31/99 12:00 AM').astype(int)
    is_dead = (md_df['DATE_DEATH'].values != '1900-12-31 00:00:00.000').astype(int)
    md_df['mortality'] = is_dead

    # Calculate duration (for cox)
    rec_date = pd.to_datetime(md_df.rec_date)
    # md_df['DATE_DEATH'].replace(to_replace='9999-12-31 00:00:00.000', value='1900-12-31 00:00:00.000', inplace=True)  # year 9999 causes out-of-bounds in nanosecond units
    md_df['last_alive'] = pd.to_datetime(md_df.DATE_DEATH)
    md_df.loc[md_df['mortality'] == 0, 'last_alive'] = pd.datetime.strptime(cfg_train.EXPERIMENT_DATE, '%Y-%m-%d')
    # md_df.loc[md_df['mortality'] == 1, 'last_alive'] = pd.datetime.strptime(cfg_train.EXPERIMENT_DATE, '%Y-%m-%d')
    duration = (md_df.last_alive - rec_date).dt.days
    md_df['duration'] = duration

    return md_df


def main(patients_df, output_file, model_path, HLD_FILE):

    try:
        # model = joblib.load('E:/Algo/Models/model.pkl') # For the old models
        model, feature_names = joblib.load(model_path)  # For the new models
    except:
        print('********* Model file not found! *********')
        return

    try:
        hld_df = pd.DataFrame.from_csv(HLD_FILE, index_col=None)
        hld_df.dropna(inplace=True)
    except:
        print('********* HLD file not found! *********')
        return

    # patients_df = pd.DataFrame.from_csv(input_file, index_col=None)[['filename']+['patient_id']+['rec_date']]

    merged_df = pd.merge(left=patients_df, right=hld_df, on='filename')
    merged_df = add_time_features(merged_df)

    missing_files = patients_df.shape[0] - merged_df.shape[0]
    if missing_files>0:
        print('********* Missing '+str(missing_files)+' files *********')

    # first_x_col = np.where(merged_df.columns.values == 'mfcc0_decileM1')[0][0]

    keep_cols = feature_names # For the new models
    X_features = merged_df[keep_cols] # For the new models
    # X_features = clean_columns(merged_df) # For the old models

    # X = X_features.iloc[:, first_x_col:].as_matrix()
    X = X_features.as_matrix()

    if 'label' in merged_df.columns:
        Y = merged_df.label.values
        predict(X, Y, model)
    else:
        print('Cannot run predict (no label column)')

    biomarker = calc_biomarker(X, model)
    merged_df = merged_df.assign(biomarker=biomarker)

    cox_regression(merged_df)
    km_curve(merged_df)

    output_df = merged_df[['filename','patient_id', 'rec_date', 'biomarker']]
    output_df.to_csv(output_file, index=False)

if __name__ == "__main__":


    # Process input; if using md input with no file info
    PROCESS_INFO = True # False #

    # md_file = 'E:/Algo/Nimrod/TEMP/chf_test.csv'
    # md_file = 'E:/Algo/dat/MetaData/PatientsMD180329_extra.csv'
    md_file = 'E:/Algo/Nimrod/share/acute_set.csv'
    # md_file = 'E:/Algo/CHFoverTime/CHF_over_time.csv'
    # md_file = 'E:/Algo/Nimrod/TEMP/russian_test.csv'

    recording_file = 'E:/Algo/dat/MetaData/RecordingsData180327.csv'

    output_file = 'E:/Algo/Nimrod/TEMP/biomarker_output.csv'

    model_path = 'E:/Algo/Nimrod/TEMP/TestCHF_Logit82_0.01_age_wCHF/model.pkl'
    HLD_FILE = 'E:/Algo/dat/HLD/HLD180327.csv'

    if PROCESS_INFO:
        md_df = process_md(md_file, recording_file)
    else:
        md_df = load_md(md_file)


    main(md_df, output_file, model_path, HLD_FILE)