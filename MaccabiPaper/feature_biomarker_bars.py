import pandas as pd
from scipy.stats.stats import pearsonr
import numpy as np
import matplotlib.pyplot as plt

from CommonML.LinearSigmoidModel import LinSig

DROP_DUPLICATES = True
REAL_NAMES = False

class LinSig_Calc(LinSig):
    def __init__(self):
        pass

data = pd.read_csv('E:/Algo/DataExperiments/vtlt23/all/M8_181004pPaper2.3/all_chf_info.csv')
data = data[data.duration >= 0]

MODEL_PATH = 'E:/Algo/DataExperiments/vtlt23/all/M8_181004pPaper2.3/'

LSM = LinSig_Calc()

Lin, feature_names, Sig = LSM.load_model(MODEL_PATH)

hld_df = pd.read_csv('E:/Algo/dat/HLD/HLD181004.csv')
hld_df.dropna(inplace=True)

merged = pd.merge(left=data, right=hld_df, on='filename')
merged.dropna(inplace=True)

if DROP_DUPLICATES:
    merged.sort_values(by=['rec_date'], inplace=True)
    merged.drop_duplicates(subset='patient_id', keep='last', inplace=True)

X = merged[feature_names].as_matrix()

predictions, score, DistNormed, ScoreNormed, highQ, rescaled2skew = LSM.predict(X, None)
biomarker = DistNormed.flatten()

quartiles = pd.qcut(biomarker, 4, labels=False)
q4 = quartiles == 3
q123 = quartiles != 3

X_norm = Lin.named_steps['standardize'].transform(X)

r = []
for i in range(X_norm.shape[1]):
    curr_r = pearsonr(X_norm[:,i],biomarker)[0]
    if curr_r != curr_r:
        curr_r = 0
    r.append(curr_r)

sorted = np.argsort(np.abs(r))
sorted = list(reversed(sorted)) # largest to smallest

sorted_features = X_norm[:,sorted]
try:
    sorted_names = feature_names[sorted]
except:
    sorted_names = []

features_mean_q4 = np.mean(sorted_features[q4,:], axis=0)
features_std_q4 = np.std(sorted_features[q4,:], axis=0)
features_mean_q123 = np.mean(sorted_features[q123,:], axis=0)
features_std_q123 = np.std(sorted_features[q123,:], axis=0)

N = 20
ind = np.arange(N)
width = 0.35

q4_mean_err = features_std_q4[0:N] / np.sqrt(sorted_features[q4,:].shape[0])
q123_mean_err = features_std_q123[0:N] / np.sqrt(sorted_features[q123,:].shape[0])

###########  mean with errors ###################

# fig, ax = plt.subplots()
# rects1 = ax.bar(ind, features_mean_q4[0:N], width, color='r', yerr=q4_mean_err)
# rects2 = ax.bar(ind+width, features_mean_q123[0:N], width, color='b', yerr=q123_mean_err)
#
# ax.set_ylabel('mean value')
# ax.set_title('')
# ax.set_xticks(ind + width/2)
# ax.legend((rects1, rects2), ('Q4', 'Q1-3'), loc='best')
#
# if REAL_NAMES:
#     ax.set_xticklabels(sorted_names[0:N])
# else:
#     names = ['feature_'+str(i) for i in range(1,21)]
#     ax.set_xticklabels(names)
#
#
# plt.xticks(rotation='vertical')
# # plt.margins(0.2)
# plt.subplots_adjust(bottom=0.25)
# plt.show()


###########  distribution comparison ###################

height_q4 = 2*features_std_q4[0:N]
height_q123 = 2*features_std_q123[0:N]

bottom_q4 = [ features_mean_q4[i]-features_std_q4[i] for i in range(N) ]
bottom_q123 = [ features_mean_q123[i]-features_std_q123[i] for i in range(N) ]

fig, ax = plt.subplots()
rects1 = ax.bar(ind, height_q4, width, bottom_q4, color='r')
rects2 = ax.bar(ind+width, height_q123, width, bottom_q123, color='green')
rects1_mean = ax.bar(ind, [0.02]*20, width, features_mean_q4[0:N], color='black')
rects2_mean = ax.bar(ind+width, [0.02]*20, width, features_mean_q123[0:N], color='black')

ax.set_ylabel('mean value')
ax.set_title('')
ax.set_xticks(ind + width/2)
ax.legend((rects1, rects2), ('Q4', 'Q1-3'), loc='best')

if REAL_NAMES:
    ax.set_xticklabels(sorted_names[0:N])
else:
    names = ['feature_'+str(i) for i in range(1,21)]
    ax.set_xticklabels(names)


plt.xticks(rotation='vertical')
plt.subplots_adjust(bottom=0.25)

plt.savefig('biomarker_score.png')
# plt.show()
plt.close()


#######################################################################################################

# days_to_survive = 360
# data_fix = merged[(merged.duration_x >= days_to_survive) | (merged.mortality_x == 1)]  # remove patients with small duration because of recording, that are still alive
#
# biomarker = data_fix.biomarker.values
# duration = data_fix.duration_x.values
# quartiles = pd.qcut(data_fix['biomarker'], 4, labels=False).values
# data_fix = data_fix.assign(quartiles=quartiles)
#
# alive = duration >= days_to_survive
# dead = duration < days_to_survive
#
# ind = np.arange(4)
# width = 0.35
#
# alive_quartiles = []
# dead_quartiles = []
# alive_df = data_fix.loc[alive, 'quartiles']
# dead_df = data_fix.loc[dead, 'quartiles']
# for q in range(4):
#     alive_quartiles.append(alive_df[alive_df == q].shape[0])
#     dead_quartiles.append(dead_df[dead_df == q].shape[0])
#
# alive_err = np.sqrt(alive_quartiles) / np.sum(alive_quartiles)
# dead_err = np.sqrt(dead_quartiles) / np.sum(dead_quartiles)
# alive_quartiles = alive_quartiles / np.sum(alive_quartiles)
# dead_quartiles = dead_quartiles / np.sum(dead_quartiles)
#
# p1 = plt.bar(ind, alive_quartiles, width, color='b', yerr=alive_err)
# # p2 = plt.bar(ind, dead_quartiles, width, bottom=alive_quartiles, color='r', yerr=dead_err)
# p2 = plt.bar(ind+width, dead_quartiles, width, color='r', yerr=dead_err)
#
# plt.ylabel('percent')
# plt.title('Survival in ' + str(days_to_survive) + ' days')
# plt.xticks(ind, ('Q1', 'Q2', 'Q3', 'Q4'))
# plt.legend((p1, p2), ('alive', 'dead'), loc='best')
#
# plt.show()

#################################################################

# N = 20
# ind = np.arange(N)
# width = 0.35
#
# skimmed_x = data_fix[feature_names].as_matrix()
# x_transformed = model[0].named_steps['standardize'].transform(skimmed_x)
# if 'pca' in model[0].named_steps:
#     x_transformed = model[0].named_steps['pca'].transform(x_transformed)
#
# r = []
# for i in range(x_transformed.shape[1]):
#     curr_r = pearsonr(skimmed_x[:,i],alive)[0]
#     if curr_r != curr_r:
#         curr_r = 0
#     r.append(curr_r)
#
# sorted = np.argsort(np.abs(r))
# sorted = list(reversed(sorted)) # largest to smallest
#
# # r_sorted = [r[i] for i in sorted]
# # print(r_sorted)
#
# sorted_features =  x_transformed[:,sorted]
# sorted_names = feature_names[sorted]
#
# features_mean_q4 = np.mean(sorted_features[dead,:], axis=0)
# features_std_q4 = np.std(sorted_features[dead,:], axis=0)
# features_mean_q123 = np.mean(sorted_features[alive,:], axis=0)
# features_std_q123 = np.std(sorted_features[alive,:], axis=0)
#
# height_q4 = 2*features_std_q4[0:N]
# height_q123 = 2*features_std_q123[0:N]
#
# bottom_q4 = [ features_mean_q4[i]-features_std_q4[i] for i in range(N) ]
# bottom_q123 = [ features_mean_q123[i]-features_std_q123[i] for i in range(N) ]
#
# fig, ax = plt.subplots()
# rects1 = ax.bar(ind, height_q4, width, bottom_q4, color='r')
# rects2 = ax.bar(ind+width, height_q123, width, bottom_q123, color='green')
# rects1_mean = ax.bar(ind, [0.02]*20, width, features_mean_q4[0:N], color='black')
# rects2_mean = ax.bar(ind+width, [0.02]*20, width, features_mean_q123[0:N], color='black')
#
# ax.set_ylabel('mean value')
# ax.set_title('')
# ax.set_xticks(ind + width/2)
# ax.legend((rects1, rects2), ('dead', 'alive'), loc='best')
#
# if REAL_NAMES:
#     ax.set_xticklabels(sorted_names[0:N])
# else:
#     names = ['feature_'+str(i) for i in range(1,21)]
#     ax.set_xticklabels(names)
#
#
# plt.xticks(rotation='vertical')
# plt.subplots_adjust(bottom=0.25)
#
# plt.show()

hld_data = pd.read_csv('E:/Algo/dat/HLD/HLD181004.csv')
cols = ['filename', 'segment'] + list(feature_names)
hld_data = hld_data[cols]

file_names, file_counts = np.unique(hld_data.filename.values, return_counts=True)
keep_files = file_names[np.where(file_counts>1)]

keep_index = [idx for idx in hld_data.index if hld_data.loc[idx,'filename'] in keep_files]

hld_data = hld_data.iloc[keep_index,:]
X = hld_data[list(feature_names)].as_matrix()

predictions, score, DistNormed, ScoreNormed, highQ, rescaled2skew = LSM.predict(X, None)
biomarker = DistNormed.flatten()

output = hld_data[['filename', 'segment']]
output = output.assign(biomarker=biomarker)

chf_files = data[['filename']].copy()
chf_files.drop_duplicates(subset='filename', keep='last', inplace=True)
output = pd.merge(left=output, right=chf_files, on='filename')

output_1 = output[output.segment == 1].copy()
output_1.rename(columns={'segment': 'segment_1', 'biomarker': 'biomarker_1'}, inplace=True)
output_2 = output[output.segment == 2].copy()
output_2.rename(columns={'segment': 'segment_2', 'biomarker': 'biomarker_2'}, inplace=True)

output.to_csv('biomarker_consistency.csv', index=False)

output_r = pd.merge(left=output_1, right=output_2, on='filename')
biomarker_1 = output_r.biomarker_1.values
biomarker_2 = output_r.biomarker_2.values
pearson_r = pearsonr(biomarker_1,biomarker_2)
print(pearson_r)







