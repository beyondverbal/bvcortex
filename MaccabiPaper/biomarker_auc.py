import pandas as pd
from sklearn.metrics import classification_report
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler

import sys
sys.path.append('../.')
from MaccabiML.Generic_ML import ML_Base
import produce_dataset
import CommonML.LinearSigmoidModel as LinearSigmoidModel

SUB_SAMPLE = False
CLUSTER_SAMPLE = False
# UP_SAMPLE = False
N_REPEATS = 200
CNN_BIOMARKER = False

TARGET = 'OSA' # 'CAD' #

def load_npy():
    # mat = np.load('E:/Algo/Nimrod/output/OSA/res_nimrod.npy')
    mat = np.load('E:/Algo/DataExperiments/OSA/CNN/res_nimrod.npy')
    col = ['patient_id', 'CNN_score', 'GroundTruth']
    df = pd.DataFrame(data=mat, columns=col, index=None)
    df.loc[:,'patient_id'] = df.loc[:,'patient_id'].astype(int)
    df.loc[:, 'GroundTruth'] = df.loc[:, 'GroundTruth'].astype(int)
    return df

def load_csv(path, is_primary=True, target_name=TARGET):
    if is_primary:
        columns = ['patient_id', 'patient_gender', 'patient_age', 'BMI', 'biomarker', 'TrueLabel']
    else:
        columns = ['patient_id', 'biomarker']
    data = pd.read_csv(path)[columns]
    if is_primary:
        gender = (data.patient_gender.values == 'M').astype(int)
        data = data.assign(gender=gender)
        data.rename(columns={'TrueLabel': target_name}, inplace=True)
    else:
        data.rename(columns={'biomarker': 'biomarker2'}, inplace=True)
    return data


ml_base = ML_Base(regularization=1)

# data1 = load_csv('E:/Algo/DataExperiments/OSA/all/CV/21-06-18_clusteringWeight/CV_info.csv', is_primary=True)
# data2 = load_csv('E:/Algo/DataExperiments/OSA/all/CV/21-06-18_MultiFlow_clusteringWeight/CV_info.csv', is_primary=False)
data1 = load_csv('E:/Algo/DataExperiments/OSA/Ex0625/all/CV/18-06-28_SVM_oldWeight4/CV_info.csv', is_primary=True)
data2 = load_csv('E:/Algo/DataExperiments/OSA/Ex0625/all/CV/18-06-28_MultiFlow_SVM_oldWeight4/CV_info.csv', is_primary=False)
data = pd.merge(left=data1, right=data2, on='patient_id')

if CNN_BIOMARKER:
    extra_biomarker = load_npy()
    data = pd.merge(left=data, right=extra_biomarker, on='patient_id')

sampled_df = pd.DataFrame(np.zeros([0, len(data.columns)]), columns=data.columns)
if SUB_SAMPLE:
    for i in range(N_REPEATS):
        print('sample',i)
        if TARGET=='OSA':
            sample = produce_dataset.uniform_sampling(data, target=TARGET, age_bin_num=2, BMI_theshold=25)
            # sample = produce_dataset.uniform_sampling(data, target=TARGET, age_bin_num=3, BMI_theshold=31)
        else:
            sample = produce_dataset.uniform_sampling(data)
        sampled_df = pd.concat([sampled_df, sample], ignore_index=True)
    data = sampled_df
elif CLUSTER_SAMPLE:
    data = produce_dataset.uniform_sampling(data, target=TARGET, age_bin_num=2, BMI_theshold=25)

# data = produce_dataset.new_uniform_sampling(data, age_bin_num=2, BMI_bin_num=4, target=TARGET)
# data = produce_dataset.up_sampling_by_weights(data)
# data = data[data.gender.values==0]

if CNN_BIOMARKER:
    X = data[['biomarker', 'biomarker2', 'CNN_score', 'patient_age', 'gender', 'BMI']].as_matrix()
else:
    X = data[['biomarker', 'biomarker2', 'patient_age', 'gender', 'BMI']].as_matrix()
# X = data[['biomarker', 'patient_age', 'gender', 'BMI']].as_matrix()
    # X = data[['biomarker', 'patient_age', 'BMI']].as_matrix()

X_mdOnly = data[['patient_age', 'gender', 'BMI']].as_matrix()
# X_mdOnly = data[['patient_age', 'BMI']].as_matrix()

Y = data[TARGET].values

print('X.shape = ',X.shape)

def fit_multi(X, Y):
    logit = LogisticRegression(class_weight='balanced', C=1, fit_intercept=True)
    # scaler = StandardScaler()
    # X_transformed = scaler.fit_transform(X)
    # logit.fit(X_transformed, Y)
    logit.fit(X, Y)

    a = logit.coef_
    b = logit.intercept_
    # x_value = (np.dot(a, X_transformed.T) + b)[0]
    x_value = (np.dot(a, X.T) + b)[0]
    x_value = LinearSigmoidModel.vector_scaler(x_value)

    return x_value, logit

x_value, model = fit_multi(X, Y)
auc_cad = roc_auc_score(Y, x_value, average='macro')
print('auc = ',auc_cad)

print('*** MD Only ***')
x_value_MD, model_MD = fit_multi(X_mdOnly, Y)
auc_cad_MD = roc_auc_score(Y, x_value_MD, average='macro')
print('auc = ',auc_cad_MD)

false_pos_rate, true_pos_rate, thresholds = roc_curve(Y, x_value, drop_intermediate=True)
false_pos_rate_MD, true_pos_rate_MD, thresholds_MD = roc_curve(Y, x_value_MD, drop_intermediate=True)

plt.figure()
lw = 2
plt.plot(false_pos_rate, true_pos_rate, color='darkgreen', lw=lw, label='biomarker + MD' + ' (AUC = %0.2f)' % auc_cad)
plt.plot(false_pos_rate_MD, true_pos_rate_MD, color='red', lw=lw, label='MD only' + ' (AUC = %0.2f)' % auc_cad_MD)
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.0])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC curve')

plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--', label='random')
plt.legend(loc="lower right")
plt.grid()
plt.show()


# odds ratio
# coef = model.coef_
# print('biomarker', 'patient_age', 'gender')
# print(np.exp(coef))
#
# coef = model_MD.coef_
# print('patient_age', 'gender')
# print(np.exp(coef))

# odds ratio
import statsmodels.api as sm
# scaler = StandardScaler()
# X_transformed = scaler.fit_transform(X)
# res = sm.Logit(Y, X_transformed).fit()
res = sm.Logit(Y, X).fit()
print('biomarker', 'biomarker2', 'patient_age', 'gender', 'BMI')
print('OR:',np.exp(res.params))
print('p-value:',res.pvalues)
conf = res.conf_int()
print('conf int:',np.exp(conf))