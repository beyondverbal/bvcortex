import config_training as cfg_train

import pandas
import numpy as np
import matplotlib.pyplot as plt


def precision_curve(CV_info, percentile_num=50, perc_step=0.01):
    data = CV_info[['TrueLabel'] + ['sig_val']]
    data = data.sort_values('sig_val', ascending=False)
    data.reset_index(drop=True, inplace=True)

    true_label = data['TrueLabel'].values

    total_positive = len(np.where(true_label == 1)[0])
    total_negative = len(np.where(true_label == 0)[0])

    # find first N percentiles
    percentiles_idx = []
    perc = perc_step
    for i in range(len(true_label)):
        curr_pos = len(np.where(true_label[:i] == 1)[
                           0])  # current positive; out of the total 1, how many 1's do we have now. DF was sorted by score previously
        curr_perc = curr_pos / total_positive
        if curr_perc >= perc:
            percentiles_idx.append(i)
            perc += perc_step

        if len(percentiles_idx) == percentile_num:
            break

    x = np.arange(perc_step, perc_step + percentile_num / 100, perc_step)

    precision = []
    fallout = []
    for i in percentiles_idx:
        curr_true_pos = len(np.where(true_label[:i + 1] == 1)[0])
        curr_precision = curr_true_pos / (i + 1)
        precision.append(curr_precision)

        curr_false_pos = len(np.where(true_label[:i + 1] == 0)[0])
        curr_fallout = curr_false_pos / total_negative
        fallout.append(curr_fallout)

    output = pandas.DataFrame(np.zeros([len(x), 3]), columns=['recall', 'precision', 'false_positive_rate'])
    output.loc[:, 'recall'] = x
    output.loc[:, 'precision'] = precision
    output.loc[:, 'false_positive_rate'] = fallout
    output.to_csv(cfg_train.OUT_PATH + 'precision_plot.csv', index=False)

    plt.figure()
    lw = 2
    plt.plot(x, precision, color='darkorange', lw=lw)
    plt.xlim([0.01, percentile_num / 100 + 0.02])
    plt.ylim([0.5, 1.05])
    plt.xlabel('% identified CHF patients')
    plt.ylabel('Precision')
    plt.title('')
    plt.show()
