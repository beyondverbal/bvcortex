import numpy as np
import pandas as pd
from lifelines import CoxPHFitter
from sklearn.metrics import roc_auc_score
from sklearn.linear_model import LogisticRegression
import statsmodels.api as sm


FILTER_FILE = 'E:/Algo/DataExperiments/vtlt23/all/M8_181004pPaper2.3/benchmark_sample.csv'

def load_df(csv_file):
    ScoreVsMD = pd.read_csv(csv_file)

    # remove other languages
    ScoreVsMD = ScoreVsMD[(ScoreVsMD.language != ScoreVsMD.language) | (ScoreVsMD.language == 'russian') | (ScoreVsMD.language == 'hebrew')]
    # fix gender
    if ScoreVsMD['patient_gender'].dtype is not np.dtype('bool'):
        gender_int = ScoreVsMD['patient_gender'].values == 'M'
        ScoreVsMD = ScoreVsMD.assign(patient_gender=gender_int)

    return ScoreVsMD


def FilterAsPaper(ScoreVsMD):
    # remove duplicate calls (keep only rows for latest call per patient) - not relevant for dana's excels
    ScoreVsMD.sort_values(['patient_id', 'duration'], ascending=[True, True], inplace=True)
    ScoreVsMD.drop_duplicates(subset='patient_id', keep='first', inplace=True)

    ScoreVsMD = ScoreVsMD[ScoreVsMD.duration >= 0]  # TODO: elsewhere - here, just for stats / removes zombie patients' recordings

    return ScoreVsMD

def cox_regression(ScoreVsMD, event_col='mortality', duration_col='duration', predictors=['score']):
    data_cox = ScoreVsMD[[event_col, duration_col] + predictors]
    data_cox = data_cox[data_cox[duration_col] >= 0]  # removes zombie patients

    cf = CoxPHFitter()
    cf.fit(data_cox, duration_col, event_col=event_col)
    # cf.print_summary()

    results = {}
    for col in predictors:
        results.update({col: cf.summary.loc[col, ['exp(coef)', 'p']].values})
        
    return results

def VariousCOX(ScoreVsMD1):
    print(ScoreVsMD1.shape[0], sum(ScoreVsMD1.mortality))

    results = cox_regression(ScoreVsMD1, predictors=['score', 'patient_age', 'patient_gender'])
    print(results)

    DistN = ScoreVsMD1.DistNormed / np.std(ScoreVsMD1.DistNormed)
    ScoreVsMD1 = ScoreVsMD1.assign(DistN=DistN)
    results = cox_regression(ScoreVsMD1, predictors=['DistN', 'patient_age', 'patient_gender'])
    print(results)


def CalcMDcontribution(ScoreVsMD2, Y):
    X = ScoreVsMD2[['DistNormed', 'patient_age', 'patient_gender', 'BMI']]
    # X = ScoreVsMD2[['patient_age', 'patient_gender', 'BMI']]
    # X = ScoreVsMD2[['DistNormed', 'patient_age', 'BMI']]
    if 'BMI' in X.columns:
        mask = (X.BMI == X.BMI)
        X1 = X[mask].values
        Y1 = Y[mask.values]
    else:
        X1 = X.values
        Y1 = Y

    logit = LogisticRegression(class_weight='balanced', C=1, fit_intercept=True)
    logit.fit(X1, Y1)
    a = logit.coef_
    x_value = (np.dot(a, X.T))[0]
    auc = roc_auc_score(Y, x_value, average='macro')
    print('auc(score+MD) =', auc, '\npredictors=', X.columns, '\nexp=', np.exp(a[0]), '\n')
    #
    # # mod = sm.Logit(Y1, X1)
    # mod = sm.Logit(Y, X)
    # res = mod.fit()
    # print(res.summary())


def CalcROC(ScoreVsMD2, HospsKey, Duration):
    Hosps = ScoreVsMD2[HospsKey].values
    HospsTH = np.percentile(Hosps, 90)
    # HospsTH = 0.75 * Duration
    # HospsTH = 0.1
    Y = (Hosps >= HospsTH)
    print('\n\n', Duration, 'days, HospsTH =', HospsTH, ': \nauc(score) =', roc_auc_score(Y, ScoreVsMD2['score'].values, average='macro'))

    CalcMDcontribution(ScoreVsMD2, Y)

    n_bins = 10
    th = np.percentile(ScoreVsMD2['score'].values, np.linspace(0,1,0.2))
    mn = np.zeros(n_bins)
    sd = np.zeros(n_bins)
    for i in range(n_bins):
        low_th = i/n_bins
        hi_th = (i+1)/n_bins
        DecileSet = ScoreVsMD2[(ScoreVsMD2['score'] >= low_th) & (ScoreVsMD2['score'] < hi_th)]
        mn[i] = np.mean(DecileSet[HospsKey].values)
        sd[i] = np.std(DecileSet[HospsKey].values)
    print ('Means', mn, sd)

if __name__ == '__main__':
    csv_file = 'E:\\Algo\\DataExperiments\\vtlt24\\all\\M8_181004pAllEX\\CV_info.csv'
    ScoreVsMD = load_df(csv_file)

    benchmark_sample = pd.read_csv(FILTER_FILE)
    ScoreVsMD = pd.merge(left=ScoreVsMD, right=benchmark_sample, on='filename')
    ScoreVsMD = FilterAsPaper(ScoreVsMD)

    # md_file = 'E:\\Algo\\dat\\MetaData\\motis_ispozim_revised.csv'
    md_file = 'E:\\Algo\\dat\\MetaData\\PackedHosp1811125.csv'
    MD2df = pd.read_csv(md_file)
    ScoreVsMD2 = pd.merge(left=ScoreVsMD, right=MD2df, on='filename')

    CalcROC(ScoreVsMD2, 'hosp_in_30d', 30)
    CalcROC(ScoreVsMD2, 'hosp_in_90d', 90)
    CalcROC(ScoreVsMD2, 'hosp_in_180d', 180)
    CalcROC(ScoreVsMD2, 'hosp_in_360d', 360)

    # print (csv_file)
    # VariousCOX(ScoreVsMD)
