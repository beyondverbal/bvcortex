import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from sklearn.preprocessing import StandardScaler


days2death = 90
do_hospitalization = True #  #True #
if do_hospitalization:
    predictors = ['biomarker', 'PVD_CD', 'HT_call', 'eGFR_1y', 'BMI_1y'] #, 'low_eGFR_1y', 'obesity_1y', 'patient_age'
    titles =     ['biomarker', 'PVD', 'hypertension', 'eGFR', 'BMI'] #, 'low_eGFR_1y', 'obesity_1y', 'patient_age'
else:
    predictors = ['biomarker', 'PVD_CD', 'HT_call', 'eGFR_1y', 'BMI_1y', 'hosp_1y'] #, 'low_eGFR_1y', 'obesity_1y', 'patient_age'
    titles =     ['biomarker', 'PVD', 'hypertension', 'eGFR', 'BMI', 'prior hosp'] #, 'low_eGFR_1y', 'obesity_1y', 'patient_age'

out_filename = 'predictorsAUC.csv'

def load_xy():
    if do_hospitalization:
        filename = 'E:\\Team\\Dana_T\\article\\test_set_1018\\.21-10-18\\data\\cox_final_hosp.xlsx'  # MERGED BIOMARKER DATA WITH DEMOGRAPHIC DATA
    else:
        filename = 'E:\\Team\\Dana_T\\article\\test_set_1018\\.16-10-18\\data\\cox_final.xlsx' # MERGED BIOMARKER DATA WITH DEMOGRAPHIC DATA
    xls = pd.ExcelFile(filename)
    xny_df = xls.parse('Sheet1')

    # remove duplicate calls (keep only rows for latest call per patient) - not relevant for dana's excels
    xny_df.sort_values(['customer_seq', 'date_call'], ascending=[True, False], inplace=True)
    xny_df.drop_duplicates(subset='customer_seq', keep='first', inplace=True)
    # remove other languages
    xny_df = xny_df[(xny_df.language == 'russian') | (xny_df.language == 'hebrew')]

    xny_df = xny_df[(xny_df.duration >= days2death) | (xny_df.mortality == 1)]  # remove patients with small duration because of recording, that are still alive

    X = xny_df[predictors].as_matrix()
    Y = (xny_df.duration < days2death) & (xny_df.mortality == 1)

    return X, Y

if __name__ == '__main__':
    X, Y = load_xy()

    plt.figure(figsize=(10,5))
    lw = 2
    aucX = np.zeros(X.shape[1])
    for i in range(X.shape[1]):
        valid_records = (X[:, i] == X[:, i])
        if predictors[i] in ('obesity_1y', 'eGFR_1y', 'BMI_1y'):
            X[valid_records, i] = -X[valid_records, i]
            predictors[i] = predictors[i] + '^'
        aucX[i] = roc_auc_score(Y[valid_records], X[valid_records, i], average='macro')
        false_pos_rate, true_pos_rate, thresholds = roc_curve(Y[valid_records], X[valid_records, i], drop_intermediate=True)
        plt.plot(false_pos_rate, true_pos_rate, lw=lw, label=titles[i] + ' (AUC = %0.3f, N = %d)' % (aucX[i], np.sum(valid_records)))

    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])
    plt.xlabel('1 - Specificity')
    plt.ylabel('Sensitivity')
    if do_hospitalization:
        plt.title('ROC - %d day hospitalization' % days2death)
    else:
        plt.title('ROC - %d day survival' % days2death)

    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--', label='random')
    plt.legend(loc="lower right")
    plt.grid()
    plt.show()

    pd.DataFrame(np.hstack((np.expand_dims(predictors, axis=1), np.expand_dims(aucX, axis=1))),
        columns = ['predictor', 'AUC']).to_csv(out_filename)
