import numpy as np
import pandas as pd
from scipy.stats import pearsonr

def combine_20seg(engine_HLD_path, multiflow_HLD_path, output_path):
    engine_HLD = pd.DataFrame.from_csv(engine_HLD_path, index_col=None)
    multiflow_HLD = pd.DataFrame.from_csv(multiflow_HLD_path, index_col=None)

    # drop duplicate files (one segment per file)
    multiflow_HLD = multiflow_HLD[multiflow_HLD.segment == 1]
    engine_HLD = engine_HLD[engine_HLD.segment == 1]

    # This is a bug work-arond (multiple segmnts, but only 1 segment number)
    # TODO: find source of bug (duplicated file names?)
    # multiflow_HLD = filter_by_segment_num(multiflow_HLD, 1)
    multiflow_HLD.drop_duplicates(subset=['filename', 'segment'], keep=False, inplace=True)

    # check engine HLD segments per file
    file_list = np.unique(engine_HLD.filename.values)
    if len(file_list) != engine_HLD.shape[0]:
        print('WARNING: Engine HLD contains files with multiple segments')

    # remove some columns
    keep_cols = [col for col in multiflow_HLD.columns if (col!='segment') and (col!='start') and (col!='end1') and (col!='end')]
    multiflow_HLD = multiflow_HLD[keep_cols]

    keep_cols = [c for c in engine_HLD.columns if not c.startswith('voice_detected')]
    engine_HLD = engine_HLD[keep_cols]

    comb_HLD = pd.merge(left=engine_HLD, right=multiflow_HLD, on='filename')
    comb_HLD.to_csv(output_path, index = False)
    print('engine_HLD.shape =', engine_HLD.shape)
    print('multiflow_HLD.shape =', multiflow_HLD.shape)
    print('comb_HLD.shape =',comb_HLD.shape)

def combine_10seg(engine_HLD_path, multiflow_HLD_path, output_path):
    engine_HLD = pd.DataFrame.from_csv(engine_HLD_path, index_col=None)
    multiflow_HLD = pd.DataFrame.from_csv(multiflow_HLD_path, index_col=None)

    # keep first two segments
    multiflow_HLD = multiflow_HLD[(multiflow_HLD.start == 0) | (multiflow_HLD.start == 10)]
    engine_HLD = engine_HLD[(engine_HLD.start == 0) | (engine_HLD.start == 10)]

    # This is a bug work-arond (multiple segmnts, but only 1 and 2 segment number)
    # TODO: find source of bug (duplicated file names?)
    multiflow_HLD.drop_duplicates(subset=['filename', 'segment'], keep=False, inplace=True)

    # only keep files with 2 10-sec segments
    multiflow_HLD = filter_by_segment_num(multiflow_HLD, 2)
    engine_HLD = filter_by_segment_num(engine_HLD, 2)

    # remove some columns
    keep_cols = [col for col in multiflow_HLD.columns if (col!='start') and (col!='end1') and (col!='end')]
    multiflow_HLD = multiflow_HLD[keep_cols]

    keep_cols = [col for col in engine_HLD.columns if (col!='start') and (col!='end')]
    engine_HLD = engine_HLD[keep_cols]

    keep_cols = [c for c in engine_HLD.columns if not c.startswith('voice_detected')]
    engine_HLD = engine_HLD[keep_cols]

    comb_HLD = pd.merge(left=engine_HLD, right=multiflow_HLD, on=['filename', 'segment'])

    comb_HLD.to_csv(output_path, index = False)
    print('engine_HLD.shape =', engine_HLD.shape)
    print('multiflow_HLD.shape =', multiflow_HLD.shape)
    print('comb_HLD.shape =',comb_HLD.shape)

def filter_by_segment_num(df, segment_num):

    df.reset_index(inplace=True)
    file_list, counts = np.unique(df.filename.values, return_counts=True)
    file_idx = np.where(counts == segment_num)
    file_list = file_list[file_idx[0]]
    keep_idx = [idx for idx in df.index if df.loc[idx, 'filename'] in file_list]
    return df.iloc[keep_idx,:]

def feature_noise(input_path, output_path):
    hld_df = pd.DataFrame.from_csv(input_path, index_col=None)
    first_df = hld_df.drop_duplicates(subset='filename', keep='first')
    second_df = hld_df.drop_duplicates(subset='filename', keep='last')

    # Consistency check
    if len(np.where(first_df.segment.values != 1)[0]) != 0:
        print('WARNING: Problem in first_df, exiting')
        return
    if len(np.where(second_df.segment.values != 2)[0]) != 0:
        print('WARNING: Problem in second_df, exiting')
        return
    if first_df.shape[0] != second_df.shape[0]:
        print('WARNING: df size mismatch, exiting')
        return

    first_x_col = np.where(hld_df.columns.values == 'mfcc0_decileM1')[0][0]
    last_x_col = hld_df.columns.values[-1]
    print('first feature column {}; last feature column {}'.format(hld_df.columns[first_x_col], last_x_col))

    first_X = first_df.iloc[:, first_x_col:].as_matrix()
    last_X = second_df.iloc[:, first_x_col:].as_matrix()
    feature_names = hld_df.columns[first_x_col:]

    corr = []
    for i in range(len(feature_names)):
        pears_r = pearsonr(first_X[:, i], last_X[:, i])[0]
        corr.append(pears_r)

    corr = np.array(corr)

    one_arg = np.where(corr == 1)[0]
    nan_arg = np.where(corr != corr)[0]
    remove_idx = np.append(one_arg, nan_arg)

    corr = np.array([corr[i] for i in range(len(corr)) if i not in remove_idx])
    feature_names = [feature_names[i] for i in range(len(feature_names)) if i not in remove_idx]

    output = pd.DataFrame(corr, columns=['corr'], index=feature_names)
    output.index.name = 'feature'

    output.to_csv(output_path)

if __name__ == "__main__":

    engine_HLD_path = 'E:/Algo/dat/HLD/HLD180901.csv'
    multiflow_HLD_path = 'E:/Algo/dat/HLD/HLD_MultiFlow_SegmentsData180827_20sec.csv'
    output_path = 'E:/Algo/dat/HLD/HLD_Combined_180902.csv'

    combine_20seg(engine_HLD_path, multiflow_HLD_path, output_path)

    engine_HLD_path = 'E:/Algo/dat/HLD/HLD180901_10segments.csv'
    multiflow_HLD_path = 'E:/Algo/dat/HLD/HLD_MultiFlow_SegmentsData180815_10sec.csv'
    output_path = 'E:/Algo/dat/HLD/HLD_Combined_10sec_180902.csv'

    combine_10seg(engine_HLD_path, multiflow_HLD_path, output_path)

    input = 'E:/Algo/dat/HLD/HLD_Combined_10sec_180902.csv'
    output = 'E:/Algo/dat/HLD/feature_noise_combined.csv'

    feature_noise(input, output)