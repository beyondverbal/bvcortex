import pandas as pd
import numpy as np
import time
import datetime
from datetime import timedelta
import os

from MaccabiPaper.PaperResults import PaperConfig as cfg

def sub_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)
        os.makedirs(path + 'data')
        os.makedirs(path + 'data\overtime')
        os.makedirs(path + 'output')

    return
def date_format(data):
    data.date_call = pd.to_datetime(data.date_call, format='%m/%d/%Y %H:%M')
    data.DATEFROMCARDIHD = pd.to_datetime(data.DATEFROMCARDIHD, format='%Y-%m-%d')
    data.DATEFROMPVD = pd.to_datetime(data.DATEFROMPVD, format='%Y-%m-%d %H:%M:%S.%f')
    data.DATEFROMDAIBETIC = pd.to_datetime(data.DATEFROMDAIBETIC, format='%Y-%m-%d')
    data.DATEFROMBLOODPRESURE = pd.to_datetime(data.DATEFROMBLOODPRESURE, format='%Y-%m-%d')
    data.DATEFROMCANCER = pd.to_datetime(data.DATEFROMCANCER, format='%Y-%m-%d %H:%M:%S.%f')
    data.DATE_FROM_CKD = pd.to_datetime(data.DATE_FROM_CKD, format='%Y-%m-%d')
    data.DATE_FROM_COPD = pd.to_datetime(data.DATE_FROM_COPD, format='%Y-%m-%d')
    data.DATE_FROM_CHF = pd.to_datetime(data.DATE_FROM_CHF, format='%Y-%m-%d')
    return


def prep_overtime(data, parameter, analysis_path, year_range=1):
    """chooses the closest test to call, for the defined parameter, with a maximal 1y delta """

    print('Preparing', parameter, 'data...')

    # import over-time data
    if parameter == 'BP':
        test = pd.read_csv(cfg.INPUT_PATH + cfg.BP_INPUT, index_col=None, encoding='utf-8-sig')
        test.rename(columns={'Date_Medida_Refuit': 'Date_Medida', 'Value1': 'SBP', 'Value2': 'DBP'}, inplace=True)

    if parameter == 'eGFR':
        test = pd.read_csv(cfg.INPUT_PATH + cfg.EGFR_INPUT, index_col=None, encoding='utf-8-sig')
        test.rename(columns={'SAMPLE_DATE_ISO': 'Date_Medida', 'LAB_RESULT': 'eGFR'}, inplace=True)
        test['low_eGFR'] = np.where(test.eGFR < 60, 1, 0) # create low_eGFR field (eGFR below 60 = '1', '0' otherwise)

    if parameter == 'BMI':
        test = pd.read_csv(cfg.INPUT_PATH + cfg.BMI_INPUT, index_col=None, encoding='utf-8-sig')
        test.rename(columns={'Date_Medida_Refuit': 'Date_Medida'}, inplace=True)
        test['obesity'] = np.where(test.BMI > 30, 1, 0) # create obesity field (BMI above 30 = '1', '0' otherwise)

    if parameter == 'creatinine':
        test = pd.read_csv(cfg.INPUT_PATH + cfg.CREAT_INPUT, index_col=None, encoding='utf-8-sig')
        test.rename(columns={'SAMPLE_DATE_ISO': 'Date_Medida', 'LAB_RESULT': 'creatinine'}, inplace=True)

    if parameter == 'glucose':
        test = pd.read_csv(cfg.INPUT_PATH + cfg.GLUC_INPUT, index_col=None, encoding='utf-8-sig')
        test.rename(columns={'SAMPLE_DATE_ISO': 'Date_Medida', 'LAB_RESULT': 'glucose'}, inplace=True)

    if parameter == 'hemog':
        test = pd.read_csv(cfg.INPUT_PATH + cfg.HEMO_INPUT, index_col=None, encoding='utf-8-sig')
        test.rename(columns={'SAMPLE_DATE_ISO': 'Date_Medida', 'LAB_RESULT': 'hemog'}, inplace=True)

    if parameter == 'trig':
        test = pd.read_csv(cfg.INPUT_PATH + cfg.TRIG_INPUT, index_col=None, encoding='utf-8-sig')
        test.rename(columns={'SAMPLE_DATE_ISO': 'Date_Medida', 'LAB_RESULT': 'trig'}, inplace=True)

    if parameter == 'cholesterol':
        test = pd.read_csv(cfg.INPUT_PATH + cfg.CHOL_INPUT, index_col=None, encoding='utf-8-sig')
        test.rename(columns={'SAMPLE_DATE_ISO': 'Date_Medida', 'LAB_RESULT': 'cholesterol'}, inplace=True)

    if parameter == 'LDL':
        test = pd.read_csv(cfg.INPUT_PATH + cfg.LDL_INPUT, index_col=None, encoding='utf-8-sig')
        test.rename(columns={'SAMPLE_DATE_ISO': 'Date_Medida', 'LAB_RESULT': 'LDL'}, inplace=True)

    # add date_call to overtime test data
    test.Date_Medida = pd.to_datetime(test.Date_Medida, format='%Y-%m-%d')
    test = test.merge(data[['customer_seq', 'date_call']], on='customer_seq', how='left')

    # compute days difference between the call and the BP test
    days_diff = (test.date_call - test.Date_Medida).dt.days
    test['days_diff'] = days_diff

    # keep tests only up to 365 days before call
    predictor = test[(test['days_diff'] >= 0) & (test['days_diff'] <= year_range*365)].copy()

    # sort rows by customer_seq (smallest to largest ID) and by days_diff (smallest to largest)
    predictor.sort_values(['customer_seq', 'days_diff'], ascending=[True, True], inplace=True)

    # keep only the closest test per patient
    predictor.drop_duplicates(subset='customer_seq', keep='first', inplace=True)

    # save overtime data
    predictor.to_csv(analysis_path + 'data/overtime/' + parameter + '.csv', index=False, encoding='utf-8-sig')

    print('Number of patients who have', parameter, 'test', year_range, 'y before call is:', len(predictor))

    return

def prep_hosp(data):

    """adds a 'hosp' field to the data, in which the patient receives '1' if been hospitalized (in any department) up to 1y before call, '0' otehrwise"""

    print('Preparing hosp data...')

    hosp_data = pd.read_csv(cfg.INPUT_PATH + cfg.HOSP_09_INPUT, index_col=None, encoding='utf-8-sig')

    # change dates from string to date format
    hosp_data.ISHPUZ_ENTRANCE_DATE = pd.to_datetime(hosp_data.ISHPUZ_ENTRANCE_DATE, format='%Y%m%d')

    # add date_call to overtime data
    hosp_data = hosp_data.merge(data[['customer_seq', 'date_call']], on='customer_seq')

    # change field name
    hosp_data.rename(columns={' date_call_x': ' date_call'}, inplace=True)

    # remove unnecessary columns
    hosp_data.drop(['CARE_GIVER_TYPE_DESC', 'CARE_GIVER_CD', 'CARE_GIVER_DESC', 'CARE_GIVER_DPRT_TYPE_CD',
                    'CARE_GIVER_DPRT_TYPE_DESC', 'CARE_GIVER_DPRT_CD','CARE_GIVER_DPRT_DESC','ISHPUZ_TYPE_CD'],
                   axis=1, inplace=True)

    # compute days difference between call date and the ishpuz entrance date
    days_diff = (hosp_data.date_call - hosp_data.ISHPUZ_ENTRANCE_DATE).dt.days
    hosp_data['days_diff'] = days_diff

    # keep hospitalizations that started up to 365 days before call
    hosp = hosp_data[(hosp_data['days_diff'] >= 0) & (hosp_data['days_diff'] <= 360)].copy()

    # sort rows by customer_seq (smallest to largest ID) and by days_diff (smallest to largest)
    hosp.sort_values(['customer_seq', 'days_diff'], ascending=[True, True], inplace=True)

    # keep only the closest hospitalization per patient
    hosp.drop_duplicates(subset='customer_seq', keep='first', inplace=True)

    print('Number of patients who were hospitalized up to 1 y before call is:', len(hosp))

    return hosp

def prep_hosp_mortality(data):

    """adds a 'mortality' field to the data, in which the patient receives '1' if been hospitalized (in any department) after the call, '0' otehrwise"""

    print('Preparing hosp data for mortality...')

    hosp_data = pd.read_csv(cfg.INPUT_PATH + cfg.HOSP_04_INPUT, index_col=None, encoding='utf-8-sig')
    # hosp_data = pd.read_csv(cfg.INPUT_PATH + cfg.HOSP_09_INPUT, index_col=None, encoding='utf-8-sig')

    # change dates from string to date format
    hosp_data.ISHPUZ_ENTRANCE_DATE = pd.to_datetime(hosp_data.ISHPUZ_ENTRANCE_DATE, format='%Y%m%d')

    # add recording info and bio
    hosp_data = hosp_data.merge(data, on='customer_seq')

    # change field name
    hosp_data.rename(columns={' date_call_x': ' date_call'}, inplace=True)

    # remove unnecessary columns
    hosp_data.drop(['CARE_GIVER_TYPE_DESC', 'CARE_GIVER_CD', 'CARE_GIVER_DESC', 'CARE_GIVER_DPRT_TYPE_CD',
                    'CARE_GIVER_DPRT_TYPE_DESC', 'CARE_GIVER_DPRT_CD','CARE_GIVER_DPRT_DESC','ISHPUZ_TYPE_CD'],
                   axis=1, inplace=True)

    # compute days difference between call date and the ishpuz entrance date
    days_diff = (hosp_data.ISHPUZ_ENTRANCE_DATE - hosp_data.date_call).dt.days
    hosp_data['days_diff'] = days_diff

    # "mortality" if hospitalizations > 1 day
    ishpuz_days = hosp_data.ISHPUZ_DAYS_CALCULATED.values
    mortality = ((ishpuz_days > 0) & (days_diff >=0)).astype(int)
    hosp_data = hosp_data.assign(mortality=mortality)

    # sort rows by customer_seq (smallest to largest ID) and by days_diff (smallest to largest)
    hosp_data.sort_values(['customer_seq', 'mortality', 'days_diff'], ascending=[True, False, True], inplace=True)

    # keep only the closest hospitalization per patient
    hosp_data.drop_duplicates(subset='customer_seq', keep='first', inplace=True)

    hosp_data.date_call = pd.to_datetime(hosp_data.date_call, format='%Y-%m-%d')
    hosp_data.DATE_DEATH.replace({'9999': '2200'}, regex=True, inplace=True)  # year 9999 has too many nanoseconds
    hosp_data.DATE_DEATH = pd.to_datetime(hosp_data.DATE_DEATH, format='%Y-%m-%d')
    hosp_data = hosp_data.assign(duration=[-1]*hosp_data.shape[0])

    # people who are alive and not hospitalized between call to EXPERIMENT_DATE
    idx1 = hosp_data[(hosp_data.mortality == 0) & (hosp_data.DATE_DEATH > cfg.EXPERIMENT_DATE)].index
    hosp_data.at[idx1,'duration'] = (cfg.EXPERIMENT_DATE - hosp_data.date_call).dt.days

        # people who are hospitalized between call to EXPERIMENT_DATE
    idx3 = hosp_data[(hosp_data.mortality == 1)].index
    hosp_data.at[idx3, 'duration'] = (hosp_data.ISHPUZ_ENTRANCE_DATE - hosp_data.date_call).dt.days

    # people who are not hospitalized between call to EXPERIMENT_DATE but have died
    idx2 = hosp_data[(hosp_data.mortality == 0) & (hosp_data.DATE_DEATH <= cfg.EXPERIMENT_DATE)].index
    hosp_data.at[idx2, 'duration'] = (hosp_data.DATE_DEATH - hosp_data.date_call).dt.days
    if not cfg.HOSP_PAPER_LOGIC:
        hosp_data.at[idx2, 'mortality'] = 1


    # TODO: temporary "fix" to remove patients with a latest call after the experiment date, since using april's table
    if cfg.EXPERIMENT_DATE == '2018-04-06':
        print('Apllying hosp_data = hosp_data[hosp_data.duration >=0] ;  Need to be removed if using newest tables!!')
        hosp_data = hosp_data[hosp_data.duration >=0]

    return hosp_data


def merge_overtime(parameter, data_input, analysis_path):

    print()
    print ('Merging cox data with', parameter, 'data...')

    data = data_input.copy()
    if 'BP_HI' in data.columns and 'BP_LOW' in data.columns:  # Needed because of bug in MOMA exporting; appear in CV_info (training info)
        data.drop(['BP_HI', 'BP_LOW'], axis=1, inplace=True)

    parameter_df = pd.read_csv(analysis_path + 'data/overtime/' + parameter + '.csv', index_col=None, encoding='utf-8-sig')

    if parameter == 'BP':
        merged = data.merge(parameter_df[['customer_seq', 'SBP', 'DBP']], on='customer_seq', how='left')
        merged['MAP'] = (merged.SBP + 2 * merged.DBP) / 3

    elif parameter == 'eGFR':
        merged = data.merge(parameter_df[['customer_seq', parameter, 'low_eGFR']], on='customer_seq', how='left')

    elif parameter == 'BMI':
        merged = data.merge(parameter_df[['customer_seq', parameter, 'obesity']], on='customer_seq', how='left')

    elif parameter == 'hosp':
        merged = data.merge(parameter_df[['customer_seq', 'ISHPUZ_ENTRANCE_DATE']], on='customer_seq', how='left')
        merged.ISHPUZ_ENTRANCE_DATE = pd.to_datetime(merged.ISHPUZ_ENTRANCE_DATE, format='%Y-%m-%d')  # TODO: check why there are nans here (NT)
        merged['ISHPUZ_ENTRANCE_DATE'] = (merged['ISHPUZ_ENTRANCE_DATE'] > datetime.datetime(1890, 1, 1)) * 1 # assign '1' to patients who have hospitalzation entrance date up to 1y before call, 0 otherwise
        merged.rename(columns={'ISHPUZ_ENTRANCE_DATE': 'hosp'}, inplace=True)

    elif parameter == 'hosp_mortality':
        data.drop(['mortality', 'date_call', 'duration', 'DATE_DEATH',
                   'filename', 'patient_gender', 'patient_age',
                   'source', 'language', 'PredictedLabel', 'score', 'biomarker',
                   'ScoreNormed', 'highQ', 'rescaled2skew', 'sample_weight', 'BLOODPRESURE_CD_x', 'CUSTOMER_BIRTH_Year',
                   'CUSTOMER_DISTRICT_DESC', 'CARDIO_YN_CD', 'DATEFROMCARDIO',
                   'CARDIO_CHF_CD', 'DATE_FROM_CHF', 'CARDIO_PIRPUR_CD',
                   'DATEFROMCARDIPIRPUR', 'CARDIO_IHD', 'DATEFROMCARDIHD', 'CARDIO_MI',
                   'DATEFROMCARDMI', 'CVD_CD', 'DATEFROMCVD', 'CVA_CD', 'DATEFROMCVA',
                   'PVD_CD', 'DATEFROMPVD', 'DIAB_YN_CD', 'DATEFROMDAIBETIC', 'DIAB_TYPE',
                   'BLOODPRESURE_CD_y', 'DATEFROMBLOODPRESURE', 'CANCER_REG_CD',
                   'DATEFROMCANCER', 'CKD_CD', 'DATE_FROM_CKD', 'CKD_STAGE', 'COPD_CD',
                   'DATE_FROM_COPD', 'bio_quartiles', 'dich_bio', 'CAD_call', 'PVD_call',
                   'Diab_call', 'HT_call', 'Cancer_call', 'CKD_call', 'COPD_call',
                   'CHF_duration'], axis=1, inplace=True)
        parameter_df = pd.read_csv(analysis_path + 'data/overtime/' + parameter + '.csv', index_col=None, encoding='utf-8-sig')
        merged = data.merge(parameter_df, on='customer_seq', how='left')

    else:
        merged = data.merge(parameter_df[['customer_seq', parameter]], on='customer_seq', how='left')


    print('Finished merging cox data with', parameter, 'data.')
    print()
    print('********************************************************************')
    print("\n")

    return merged


def add_extra_info(input_data):
    data = input_data.copy()
    # add cardio drugs
    print('Adding cardio drugs to data...')
    print()
    drugs = pd.read_csv(cfg.INPUT_PATH + cfg.DRUGS_INPUT, index_col=None,
                                  encoding='utf-8-sig')  # import drugs data
    data = data.merge(drugs, on='customer_seq')  # merge drugs data with data

    # add lung cancer
    print('Adding lung cancer registry to data...')
    print()
    lung_cancer = pd.read_csv(cfg.INPUT_PATH + cfg.CANCER_INPUT, index_col=None,
                                        encoding='utf-8-sig')  # import Lung cancer data
    lung_cancer.drop_duplicates(subset='customer_seq', keep='first', inplace=True)
    data = data.merge(lung_cancer[['customer_seq', 'DISEASE_CODE']], on='customer_seq',
                      how='left')  # merge lung cancer data with data
    data.rename(columns={'DISEASE_CODE': 'lung_cancer'}, inplace=True)
    data['lung_cancer'] = data['lung_cancer'].replace([2], [1])
    data['lung_cancer'] = (data['lung_cancer'] == 1) * 1  # assign '1' to patients who have lung cancer, '0' otherwise

    # add asthma data
    print('Adding asthma diagnoses to data...')
    print("     '1': Patients who recieved asthma diagnosis until the call")
    print("     '0': Patients who recieved asthma diagnosis after call")
    print()
    asthma = pd.read_csv(cfg.INPUT_PATH + cfg.ASTHMA_INPUT, index_col=None,
                                   encoding='utf-8-sig')  # import asthma data
    asthma['DATE_DIAGNOSIS'] = asthma[['DATE_DIAGNOSIS']].applymap(str).applymap(
        lambda s: "{}-{}-{}".format(s[0:4], s[4:6], s[6:]))
    asthma.DATE_DIAGNOSIS = asthma.DATE_DIAGNOSIS.map(lambda x: x.rstrip('.0'))
    asthma.DATE_DIAGNOSIS = pd.to_datetime(asthma.DATE_DIAGNOSIS, format='%Y-%m-%d')
    asthma = asthma.merge(data[['customer_seq', 'date_call']], on='customer_seq',
                          how='left')  # add date_call to asthma data
    asthma.date_call = pd.to_datetime(asthma.date_call, format='%Y-%m-%d')
    asthma['days_diff'] = (
    asthma.date_call - asthma.DATE_DIAGNOSIS).dt.days  # compute delta between date_diagnosis and date_call
    asthma.sort_values(['customer_seq', 'days_diff'], ascending=[True, True],
                       inplace=True)  # sort rows by customer_seq (smallest to largest ID) and by days_diff (smallest to largest)
    asthma.drop_duplicates(subset='customer_seq', keep='first',
                           inplace=True)  # keep only earliest diagnosis per patient
    asthma['asthma'] = np.where(asthma.days_diff <= 0, 0, 1)
    data = data.merge(asthma[['customer_seq', 'asthma', 'DATE_DIAGNOSIS']], on='customer_seq',
                      how='left')  # merge lung cancer data with data
    data.rename(columns={'DATE_DIAGNOSIS': 'Date_from_asthma'}, inplace=True)
    data['asthma'] = (data['lung_cancer'] == 1) * 1  # assign '1' to patients who have lung cancer, '0' otherwise

    # create a 'lung_disease' field (COPD or asthma or lung cancer)
    print('Creating Lung Disease field:')
    print("     '1': Patients who have COPD, lung cancer or asthma")
    print("     '0': Patient who don't have any serious lung disease")

    # lung_data = data[['customer_seq', 'lung_cancer', 'asthma', 'COPD_call']]
    lung_data = data[(data.COPD_call == 1) | (data.asthma == 1) | (data.lung_cancer == 1)]
    lung_data = lung_data.assign(lung_disease=1)
    data = data.merge(lung_data[['customer_seq', 'lung_disease']], on='customer_seq', how='left')
    data['lung_disease'] = (data['lung_disease'] == 1) * 1  # assign '1' to patients who have hospitalzation entrance date up to 1y before call, 0 otherwise

    # Language filter
    if cfg.LANGUAGE == 'heb_russ':
        data = data[(data.language == 'russian') | (data.language == 'hebrew')].copy()
        print('**** Keeping Hebrew and Russian ****')
    elif cfg.LANGUAGE == 'heb':
        data = data[data.language == 'hebrew'].copy()
        print('**** Keeping Hebrew only ****')
    elif cfg.LANGUAGE == 'russ':
        data = data[data.language == 'russian'].copy()
        print('**** Keeping Russian only ****')
    else:
        print('**** Keeping all languages ****')

    return data


######################################################################################

def process_data(analysis_path, biomarker_file):

    # 1) MERGE BIOMARKER DATA WITH DEMOGRAPHIC DATA

    # create sub-dirs
    sub_dir(analysis_path)

    print()
    print('Starting data pre-processing...')
    print()
    print('Keeping only the last call for each patient...')
    print()
    print('Merging biomarker data (algo) with demographic data (Maccabi)...')
    print()

    # Load original biomarker data produced by Algo (biomarker values, date call, duration, nortality, age and gender)
    bio_data = pd.read_csv(biomarker_file)

    bio_data.rename(columns={'patient_id': 'customer_seq', 'rec_date': 'date_call', 'DistNormed': 'biomarker'}, inplace=True)  # change columns names for convenience
    bio_data.date_call = pd.to_datetime(bio_data.date_call, format='%Y-%m-%d %H:%M:%S') # change date_call format from string, to date format
    bio_data.fillna('hebrew', inplace=True) # replace empty cells in 'language' column with 'hebrew'

    DD = pd.read_csv(cfg.INPUT_PATH + cfg.DEMO_INPUT, index_col=None, encoding='utf-8-sig')

    # bio_data: keep latest call only
    bio_data.sort_values(['customer_seq', 'date_call'], ascending=[True, False], inplace=True)  # sort rows by customer_seq (smallest to largest number) and by call (latest to earliest)
    bio_data.drop_duplicates(subset='customer_seq', keep='first', inplace=True)  # remove duplicate calls (keep only rows for latest call per patient)
    
    # merge bio_data with demographic data (DD)
    data = bio_data.merge(DD, on='customer_seq')

    # change gender to 0,1
    val2replace = {'F': int(0), 'M': int(1)}  # replace gender strings F,M to 0,1
    data['patient_gender'] = data['patient_gender'].map(val2replace)

    # change all dates from string to date format
    date_format(data)

    # remove negative duration
    # data = data[data.duration >= 0]

    # compute biomarker quartiles and add to data as a new field
    bio_quartiles = pd.qcut(data['biomarker'], 4, labels=False).values
    data['bio_quartiles'] = bio_quartiles

    # compute categorical biomarker groups: "0" (Q1-Q3) and "1" (Q4) and add to data as a new field
    q4 = data.biomarker.quantile(.75)
    dich_bio = (np.where(data.biomarker < q4, 0, 1))  # for q1-q3 assign 0, otherwise assign 1
    data['dich_bio'] = dich_bio


    ## compute biomarker tertiaries and add to data as a new field
    #bio_tertiaries = pd.qcut(data['biomarker'], 3, labels=False).values
    #data['bio_tertiaries'] = bio_tertiaries

    ## compute categorical biomarker groups: "0" (T1-T2) and "1" (T3) and add to data as a new field
    #t3 = data.biomarker.quantile(.67)
    #dich_bio = (np.where(data.biomarker < t3, 0, 1))  # for q1-q3 assign 0, otherwise assign 1
    #data['dich_bio'] = dich_bio


    # drop unnecessary fields
    data.drop(
        ['TrueLabel', 'BMI', 'CUSTOMER_SEX_CODE', 'STATUSCUSTOMER', 'CUSTOMER_STATUS_DATE', 'CUSTOMER_DISTRICT_CODE',
         'TIA_CD',
         'DATEFROMTIA', 'DATETOTIA', 'NONCVA_CD', 'DATEFROMNONCVA', 'BREAST_CANCER_CD', 'BREAST_CANCER_FROM_DATE',
         'COLON_CANCER_CD', 'DATE_FROM_COLON_CANCER', 'COUMADIN_CD', 'DATEFROMCOUMADIN', 'DIALIZACD',
         'DATEFROMDIALIZA'], axis=1, inplace=True)

    # save file
    data.to_csv(analysis_path + 'data/cox_Demographic.csv', index=False, encoding='utf-8-sig')

    print("Finished merging biomarker data with demographic data.")
    print("\n")
    print('******************************************************************************')
    print("\n")


    # 2) CREATE NEW REGISTRIES

    print('Creating new registries based on date call...')
    print("     '1': Patients who entered the registry up to 30d after call")
    print("     '0': Patients who entered the registry 30d or more after call")
    print()

    # replace registry entry date for patient who are "0" in registry, with date_call + 3 months
    data.loc[(data.DATEFROMCARDIHD == datetime.datetime(1800, 1, 1)), 'DATEFROMCARDIHD'] = data.date_call + \
                                                                                           timedelta(days=90)
    data.loc[(data.DATEFROMPVD == datetime.datetime(1800, 1, 1)), 'DATEFROMPVD'] = data.date_call + \
                                                                                   timedelta(days=90)
    data.loc[(data.DATEFROMDAIBETIC == datetime.datetime(1800, 1, 1)), 'DATEFROMDAIBETIC'] = data.date_call + \
                                                                                             timedelta(days=90)
    data.loc[(data.DATEFROMBLOODPRESURE == datetime.datetime(1800, 1, 1)), 'DATEFROMBLOODPRESURE'] = data.date_call + \
                                                                                                     timedelta(days=90)
    data.loc[(data.DATEFROMCANCER == datetime.datetime(1800, 1, 1)), 'DATEFROMCANCER'] = data.date_call + \
                                                                                         timedelta(days=90)
    data.loc[(data.DATE_FROM_CKD == datetime.datetime(1800, 1, 1)), 'DATE_FROM_CKD'] = data.date_call + \
                                                                                       timedelta(days=90)
    data.loc[(data.DATE_FROM_COPD == datetime.datetime(1800, 1, 1)), 'DATE_FROM_COPD'] = data.date_call + \
                                                                                         timedelta(days=90)

    # compute delta (days) between registry entrance date, and date_call
    data['CAD_30'] = (data.DATEFROMCARDIHD - data.date_call).dt.days
    data['PVD_30'] = (data.DATEFROMPVD - data.date_call).dt.days
    data['Diab_30'] = (data.DATEFROMDAIBETIC - data.date_call).dt.days
    data['HT_30'] = (data.DATEFROMBLOODPRESURE - data.date_call).dt.days
    data['Cancer_30'] = (data.DATEFROMCANCER - data.date_call).dt.days
    data['CKD_30'] = (data.DATE_FROM_CKD - data.date_call).dt.days
    data['COPD_30'] = (data.DATE_FROM_COPD - data.date_call).dt.days


    # create new registries, based on the delta between the registry entry date, and date of the call:
    # 1: if patient entered the registry up to 30d after the call
    # 0: if patient entered the registry more than 30d after call
    data['CAD_call'] = np.where(data.CAD_30 > 30, 0, 1)
    data['PVD_call'] = np.where(data.PVD_30 > 30, 0, 1)
    data['Diab_call'] = np.where(data.Diab_30 > 30, 0, 1)
    data['HT_call'] = np.where(data.HT_30 > 30, 0, 1)
    data['Cancer_call'] = np.where(data.Cancer_30 > 30, 0, 1)
    data['CKD_call'] = np.where(data.CKD_30 > 30, 0, 1)
    data['COPD_call'] = np.where(data.COPD_30 > 30, 0, 1)

    # delete unessecary fields
    data.drop(['CAD_30', 'PVD_30', 'Diab_30', 'HT_30', 'Cancer_30', 'CKD_30', 'COPD_30'], axis=1, inplace=True)

    CHF_duration= (data.date_call - data.DATE_FROM_CHF).dt.days
    data['CHF_duration'] = CHF_duration

    # save file
    data.to_csv(analysis_path + 'data/cox_registries.csv', index=False, encoding='utf-8-sig')  # this cox now contains: the biomarker values, the original registries (e.g., CAD_CD) and the new registries (e.g., 'CAD_call)

    print("Finished creating new registries and merging them to data.")
    print("\n")
    print('******************************************************************************')
    print("\n")


    # 3) ADD OVERTIME TESTS

    print('Preparing overtime tests: BMI, blood tests.')
    print('Choosing the closest test to call, up to 1y before call...')
    print("\n")
    print('******************************************************************************')
    print("\n")

    # prepare overtime tests

    cox_reg = pd.read_csv(analysis_path + 'data\\cox_registries.csv', index_col=None, encoding='utf-8-sig')

    # SBP, DBP
    parameter='BP'
    prep_overtime(data, parameter, analysis_path)
    cox_reg = merge_overtime(parameter, cox_reg, analysis_path)

    # eGFR
    parameter='eGFR'
    prep_overtime(data, parameter, analysis_path)
    cox_reg = merge_overtime(parameter, cox_reg, analysis_path)

    # BMI, obesity
    parameter='BMI'
    prep_overtime(data, parameter, analysis_path)
    cox_reg = merge_overtime(parameter, cox_reg, analysis_path)

    # creatinine
    parameter='creatinine'
    prep_overtime(data, parameter, analysis_path)
    cox_reg = merge_overtime(parameter, cox_reg, analysis_path)

    # glucose
    parameter='glucose'
    prep_overtime(data, parameter, analysis_path)
    cox_reg = merge_overtime(parameter, cox_reg, analysis_path)

    # hemog
    parameter='hemog'
    prep_overtime(data, parameter, analysis_path)
    cox_reg = merge_overtime(parameter, cox_reg, analysis_path)

    # LDL
    parameter='LDL'
    prep_overtime(data, parameter, analysis_path, year_range=5)
    cox_reg = merge_overtime(parameter, cox_reg, analysis_path)

    # cholesterol
    parameter = 'cholesterol'
    prep_overtime(data, parameter, analysis_path, year_range=5)
    cox_reg = merge_overtime(parameter, cox_reg, analysis_path)

    # trig
    parameter='trig'
    prep_overtime(data, parameter, analysis_path, year_range=5)
    cox_reg = merge_overtime(parameter, cox_reg, analysis_path)

    # hosp
    parameter = 'hosp'
    hosp = prep_hosp(data)
    hosp.to_csv(analysis_path + 'data/overtime/hosp.csv', index=False, encoding='utf-8-sig')
    cox_reg = merge_overtime(parameter, cox_reg, analysis_path)

    cox_reg.to_csv(analysis_path + 'data/cox_overtime.csv', index=False, encoding='utf-8-sig')

    parameter = 'hosp_mortality'
    hosp_data = prep_hosp_mortality(data)
    hosp_data.to_csv(analysis_path + 'data/overtime/hosp_mortality.csv', index=False, encoding='utf-8-sig')
    cox_reg_hosp = merge_overtime(parameter, cox_reg, analysis_path)

    cox_reg_hosp = add_extra_info(cox_reg_hosp)
    cox_reg_hosp.to_csv(analysis_path + 'data/cox_final_hosp.csv', index=False, encoding='utf-8-sig')

    print("Finished preparing the overtime tests and merging it to data.")
    print("\n")
    print('******************************************************************************')
    print("\n")


    # 4) ADD ADITIONAL DATA: DRUGS, LUNG DISEASES

    data_cox_final = pd.read_csv(analysis_path + 'data/cox_overtime.csv', index_col=None, encoding='utf-8-sig')
    data_cox_final = add_extra_info(data_cox_final)
    data_cox_final.to_csv(analysis_path + 'data/cox_final.csv', index=False, encoding='utf-8-sig')

    print("Finished adding all parameters to data.")
    print("\n")



if __name__ == '__main__':
    print('*********** Processing test set **************')
    process_data(cfg.ANALYSIS_PATH, cfg.BIOMARKER_FILE)

    print('*********** Processing training set **************')
    process_data(cfg.TRAINING_ANALYSIS_PATH, cfg.TRAINING_INFO)
