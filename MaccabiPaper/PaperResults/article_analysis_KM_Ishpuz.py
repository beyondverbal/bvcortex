import pandas as pd
import numpy as np
import os
from lifelines import CoxPHFitter
from lifelines import KaplanMeierFitter
from lifelines.statistics import logrank_test
from lifelines.plotting import add_at_risk_counts
from matplotlib import pyplot as plt

from article_analysis import multi_cox, uni_cox
import PaperConfig as cfg





def km_curve():
    T = data['duration'].values
    C = data['mortality'].values
    time_points = 25
    med_duration = np.median(data.duration)

    ax = plt.subplot(111)
    t = np.linspace(0, med_duration, time_points)

    kmf1 = KaplanMeierFitter()
    kmf2 = KaplanMeierFitter()

    iq_0 = np.where(data['dich_bio'] == 0)[0]
    iq_1 = np.where(data['dich_bio'] == 1)[0]

    ax = kmf1.fit(T[iq_0], C[iq_0], label='Low Biomarker', timeline=t).plot(ax=ax)
    ax = kmf2.fit(T[iq_1], C[iq_1], label='High Biomarker', timeline=t).plot(ax=ax)

    plt.xlabel('Time (Days)')
    plt.ylabel('Hospitalization Probability (%)')
    ymin, ymax = plt.ylim()
    plt.ylim(ymax,ymin)

    ax.set_yticklabels(ax.get_yticks())
    locs, labels = plt.yticks()
    labels_text = [item.get_text() for item in labels]
    new_labels = ['']
    for l in labels_text[1:-1]:
        new_l = 1 - float(l)
        new_labels.append(str(round(new_l,1)))
    new_labels.append('')
    plt.yticks(locs, new_labels)

    plt.title('Hospitalization Probability as a function of the Biomarker')
    add_at_risk_counts(kmf1, kmf2)
    savepath = os.path.join(results_path, 'km_figure_hosp.png')
    plt.savefig(savepath)  # save fig

    high_bio = np.where(data['dich_bio'] == [1])[0]
    results = logrank_test(T[high_bio], T[~high_bio], C[high_bio], C[~high_bio], alpha=.99)
    results.print_summary()

    return results


if __name__ == '__main__':

    cox_name = 'cox_final_hosp.csv'
    results_path = 'E:/Algo/Nimrod/Debug/MOMAdb/analysis/output_hosp'
    data = pd.read_csv(cfg.COX_PATH + cox_name, index_col=False, encoding='utf-8-sig')

    # TODO: why there are nan's here??
    data.dropna(subset=['duration'], inplace=True)

    multi_cox(data, cfg.BIOMARKER_TYPE, cfg.HOSP_RESULTS_PATH)
    km_curve()
