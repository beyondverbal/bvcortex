import pandas as pd

EXPERIMENT_DATE = pd.datetime.strptime('2018-09-02', '%Y-%m-%d')  # pd.datetime.strptime('2018-04-06', '%Y-%m-%d') #
BIOMARKER_TYPE = 'dich_bio'  # choose: 'dich_bio' or 'biomarker' for continuous biomarker
LANGUAGE = 'all'  # 'heb_russ' # 'heb' # 'russ' #

# Use paper's hospitalization target logic
HOSP_PAPER_LOGIC = True # False #

ANALYSIS_PATH = 'E:/Algo/Nimrod/Debug/MOMAdb/analysis/'
INPUT_PATH = 'E:/Algo/Nimrod/Debug/MOMAdb/'
BIOMARKER_FILE = 'E:/Algo/DataExperiments/vtlt23/all/M8_181004pPaper2.3/all_chf_info.csv'

TRAINING_ANALYSIS_PATH = 'E:/Algo/Nimrod/Debug/MOMAdb/analysis/training_info/'
TRAINING_INFO = 'E:/Algo/DataExperiments/vtlt23/all/M8_181004pPaper2.3/CV_info.csv'

COX_PATH = 'E:/Algo/Nimrod/Debug/MOMAdb/analysis/data/'
RESULTS_PATH = 'E:/Algo/Nimrod/Debug/MOMAdb/analysis/output'
HOSP_RESULTS_PATH = 'E:/Algo/Nimrod/Debug/MOMAdb/analysis/output_hosp'

DEMO_INPUT = 'Demographic_181114.csv'
BP_INPUT = 'BP_181114.csv'
EGFR_INPUT = 'eGFR_181114.csv'
BMI_INPUT = 'BMI_181114.csv'
CREAT_INPUT = 'creatinine_181114.csv'
GLUC_INPUT = 'glucose_181114.csv'
HEMO_INPUT = 'hemog_181114.csv'
TRIG_INPUT = 'trig_181114.csv'
CHOL_INPUT = 'cholesterol_181114.csv'
LDL_INPUT = 'LDL_181114.csv'
DRUGS_INPUT = 'cardio_drugs_181114.csv'
CANCER_INPUT = 'Lung_cancer_181114.csv'
ASTHMA_INPUT = 'Asthma_181114.csv'
HOSP_09_INPUT = 'HOSPITAL_2018_09_181114.csv'
HOSP_04_INPUT = 'HOSPITAL_150000_181114.csv' # 'HOSPITAL_2018_09_181114.csv' #