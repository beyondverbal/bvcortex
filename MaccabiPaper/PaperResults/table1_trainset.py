import pandas as pd
import numpy as np

from MaccabiPaper.PaperResults import PaperConfig as cfg
from MaccabiPaper.PaperResults.article_table1 import normality_test


def table1(data, field):
    """This function is only for continuous variables! it computes all the measures for table 1:
    - for normally distributed variables: mean, std and t-test
    - for non-normally distributed variables: median, IQR and Mann-Whitney test"""

    normality = normality_test(data, field)

    print('****************************************************************************')

    if normality == 0:
        print('     - ', field, 'does not distributes normally.')
        print()

        if field == 'biomarker' or field == 'duration' or field == 'creatinine':

            med = (data[field].quantile([0.50])).round(3)
            q1 = (data[field].quantile([0.25])).round(3)
            q3 = (data[field].quantile([0.75])).round(3)

            print('        Median = %.1f' % med, '[ IRQ: %.5f' % q1, '- %.5f' % q3, ']')
            print()

            return

        else:
            med = (data[field].quantile([0.50]).round(0))
            q1 = (data[field].quantile([0.25]).round(0))
            q3 = (data[field].quantile([0.75]).round(0))

            print('        Median = %.1f' % med, '[ IRQ: %.1f' % q1, '- %.1f' % q3, ']')
            print()

            return

    if normality == 1:

        print('     - ' , field, 'distributes normally. Computes Mean, std and t-test...')

        if field == 'biomarker' or field == 'duration' or field == 'creatinine':

            mean = data[field].mean()
            std = data[field].std()

            print('        Mean = %.5f' % mean, '( ± %.2f' % std, ')')
            print()

            return

        else:
            mean = data[field].mean().__round__(0)
            std = data[field].std().__round__(0)

            print('        Mean = %.1f' % mean, '( ± %.1f' % std, ')')
            print()

            print()
            return

    return


def table1_cat(field):
    """This function is only for categorical variables! it computes all the measures for table 1: 
    frequency, percentages and t-test"""

    print('****************************************************************************')
    print()
    print('     ', field, 'characteristics:')

    # print number and percentage of patients in category 1 (=disease)
    print()
    len1 = len(data.loc[data[field] == 1])
    pcnt1 = ((len1 / len(data)) * 100).__round__(0)
    print('         -', field, 'all patients: n=', len1, '(%', pcnt1, ')')

    print()
    return


if __name__ == '__main__':

    data = pd.read_csv(cfg.TRAINING_ANALYSIS_PATH + 'data/cox_final.csv', index_col=None, encoding='utf-8-sig')

    print("Numbers of patients in train_set is: " + str(len(data)), "\n") # count number of patients

    data['lang_heb'] = np.where((data.language == 'hebrew'), 1, 0)

    table1_list = ['biomarker', 'patient_age', 'SBP', 'DBP', 'MAP', 'eGFR', 'creatinine', 'BMI', 'trig', 'cholesterol',
                   'glucose', 'duration']
    for tab in table1_list:
        table1(data, tab)

    table1_cat_list = ['patient_gender', 'lang_heb', 'CKD_call', 'Diab_call', 'COPD_call', 'lung_disease', 'HT_call',
                       'Cancer_call', 'CAD_call', 'PVD_call', 'low_eGFR', 'obesity', 'hosp', 'BetaBlockers', 'ACI',
                       'Diuretic', 'Spironolactone', 'Statins']
    for tab in table1_cat_list:
        table1_cat(tab)
