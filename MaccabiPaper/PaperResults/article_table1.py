import pandas as pd
import numpy as np
from scipy.stats import shapiro
from scipy.stats import ttest_ind
from scipy.stats import mannwhitneyu

import PaperConfig as cfg



def normality_test(data, field):
    """computes Shapiro normality test, and return '1' for variables are distributed normally, '0' otherwise"""
    stats, p = shapiro(data[field])
    alpha = 0.05
    # print('Statistics=%.3f, p=%.3f' % (stats, p))
    global normality

    if p < alpha:
        normality=0

    else:
        normality=1

    return normality

def ttest(field):
    # For normally distributed variables; performs t-test for differences in the defined 'field', between the low and high biomarker groups

    # remove nan values

    t_data = data.dropna(subset=[field]) # for the defined field (e.g., BMI), remove all patients that have NAN valus = don't have a test in the desired time range

    # split data to low and high biomarker groups (low=Q1-Q3, high=Q4)
    low_bio = t_data.loc[t_data['dich_bio'] == 0]
    high_bio = t_data.loc[t_data['dich_bio'] == 1]

    # perform t_test
    t_score, p_val = ttest_ind(low_bio[field], high_bio[field])
    print("             t_score = %g, p_val = %g" %(t_score, p_val))
    # print("t-test results for", field, ": t_score = %g, p_val = %g" %(t_score, p_val))

    return

def MannWit(field):
    """For variables that aren't normally distributed; performs Mann-Whitney test for differences in the defined 'field',
    between the low and high biomarker groups"""

    # remove nan values
    man_data = data.dropna(subset=[field])

    # split data to low and high biomarker groups (low=Q1-Q3, high=Q4)
    low_bio = man_data.loc[man_data['dich_bio'] == 0]
    high_bio = man_data.loc[man_data['dich_bio'] == 1]

    # perform Mann-Whitney U test
    u_score, p_val = mannwhitneyu(low_bio[field], high_bio[field])
    print("      u_score = %g, p_val = %g" %(u_score, p_val))
    print()

def table1(data, field):
    """This function is only for continuous variables! it computes all the measures for table 1:
    - for normally distributed variables: mean, std and t-test
    - for non-normally distributed variables: median, IQR and Mann-Whitney test"""

    normality = normality_test(data, field)

    print('****************************************************************************')

    low_bio = data.loc[data['dich_bio'] == 0]
    high_bio = data.loc[data['dich_bio'] == 1]

    if normality == 0:
        print('     - ', field, 'does not distributes normally.')
        print()

        med = (data[field].quantile([0.5]))
        q1 = (data[field].quantile([0.25]))
        q3 = (data[field].quantile([0.75]))

        med_low = np.percentile(low_bio[field], 50)
        q1_low = np.percentile(low_bio[field], 25)
        q3_low = np.percentile(low_bio[field], 75)
        med_high = np.percentile(high_bio[field], 50)
        q1_high = np.percentile(high_bio[field], 25)
        q3_high = np.percentile(high_bio[field], 75)

        print('        Median = %.1f' % med, '[ IRQ: %.5f' % q1, '- %.5f' % q3, ']')
        print()

        print('     - ', field, "in low biomarker group:")
        print('        Median = %.1f' % med_low, '[ IRQ: %.5f' % q1_low, '- %.5f' % q3_low,']')
        print()

        print('     - ', field, "in high biomarker group:")
        print('        Median = %.1f' % med_high, '[ IRQ: %.5f' % q1_high, '- %.5f' % q3_high,']')
        print()

        print('     - Mann-Whitney test:')
        MannWit(field)

    if normality == 1:

        print('     - ' , field, 'distributes normally. Computes Mean, std and t-test...')

        mean = data[field].mean()
        std = data[field].std()

        mean_low = low_bio[field].mean()
        std_low = low_bio[field].std()
        mean_high = high_bio[field].mean()
        std_high = high_bio[field].std()

        print('        Mean = %.5f' % mean, '( ± %.2f' % std, ')')
        print()

        print('     - ', field, "in low biomarker group:")
        print('        Mean = %.5f' % mean_low, '( ± %.2f' % std_low, ')')
        print()

        print('     - ', field, "in high biomarker group:")
        print('        Mean = %.5f' % mean_high, '( ± %.2f' % std_high, ')')

        print()
        print('     - t-test:')
        ttest(field)
        print("\n")

def table1_cat(field):
    """This function is only for categorical variables! it computes all the measures for table 1: 
    frequency, percentages and t-test"""

    print('****************************************************************************')
    print()
    print('     ', field, 'characteristics:')

    # split data by dich_bio:
    low_bio = data.loc[data['dich_bio'] == 0]
    high_bio = data.loc[data['dich_bio'] == 1]

    # print number and percentage of patients in category 1 (=disease)
    print()
    len1 = len(data.loc[data[field] == 1])
    pcnt1 = ((len1 / len(data)) * 100).__round__(0)
    print('         -', field, 'all patients: n=', len1, '(%', pcnt1, ')')

    # print number and percentage of patients in category 1 (=disease) by biomarker
    len1_low_bio = len(low_bio.loc[low_bio[field] == 1])
    len1_high_bio = len(high_bio.loc[high_bio[field] == 1])
    pcnt1_low_bio = ((len1_low_bio/len(low_bio)) * 100).__round__(0)
    pcnt1_high_bio = ((len1_high_bio/len(high_bio)) * 100).__round__(0)

    print('         -', field, 'in low bio:', len1_low_bio, '(', pcnt1_low_bio, '%)')
    print('         -', field, 'in high bio:', len1_high_bio, '(', pcnt1_high_bio, '%)')

    print('         - t-test:')
    ttest(field)
    print()
    return

    ######################################################################################

if __name__ == '__main__':

    data = pd.read_csv(cfg.COX_PATH + 'cox_final.csv', index_col=None, encoding='utf-8-sig')

    print("Numbers of patients in", cfg.LANGUAGE, "is: " + str(len(data)), "\n") # count number of patients

    data['lang_heb'] = np.where((data.language == 'hebrew'), 1, 0)

    table1_list = ['biomarker', 'patient_age', 'SBP', 'DBP', 'MAP', 'eGFR', 'creatinine', 'BMI', 'trig', 'cholesterol',
                   'glucose', 'duration', 'CHF_duration']
    for tab in table1_list:
        table1(data, tab)

    table1_cat_list = ['patient_gender', 'lang_heb', 'CKD_call', 'Diab_call', 'COPD_call', 'lung_disease', 'HT_call',
                       'Cancer_call', 'CAD_call', 'PVD_call', 'low_eGFR', 'obesity', 'hosp', 'BetaBlockers', 'ACI',
                       'Diuretic', 'Spironolactone', 'Statins']
    for tab in table1_cat_list:
        table1_cat(tab)
