import pandas as pd
import numpy as np
import os
from lifelines import CoxPHFitter
from lifelines import KaplanMeierFitter
from lifelines.statistics import logrank_test
from lifelines.plotting import add_at_risk_counts
from matplotlib import pyplot as plt

import PaperConfig as cfg

######################################################################################

def calc_uni_cox(predictor, data, digits=2):
    print('Performing cox regression for ', predictor)
    print()
    data_clean = data.dropna(subset=[predictor])
    data_cox = data_clean[[predictor, 'mortality', 'duration']]  # import predictors + T and C
    cf = CoxPHFitter()
    cf.fit(data_cox, 'duration', event_col='mortality')
    print(cf.summary)
    # transform Confidence Interval to exponent:
    low_CI = np.exp(cf.summary['lower 0.95']).round(digits)
    high_CI = np.exp(cf.summary['upper 0.95']).round(digits)
    print()
    print(predictor, 'Corrected Confidence Interval Values: [ %.2f' % low_CI, '- %.2f' % high_CI, ']')
    print("\n")
    print('----------------------------------------------------------------------')
    print("\n")

def cox_summary_CI(summary):
    # transform Confidence Interval to exponent:
    print()
    print('Corrected Confidence Interval Values:')
    for idx in summary.index:
        low_CI = summary.loc[idx, 'lower 0.95']
        high_CI = summary.loc[idx, 'upper 0.95']
        low_CI = np.exp(low_CI).round(2)
        high_CI = np.exp(high_CI).round(2)
        print(idx,': [ %.2f' % low_CI, '- %.2f' % high_CI, ']')
    print()


def uni_cox(data_input):
    """Performs Cox Regression for each predictor, separately"""

    data = data_input.copy()
    lang_heb = np.where((data.language == 'hebrew'), 1, 0)
    data = data.assign(lang_heb=lang_heb)

    print("Performing univariate cox regression... ")
    print("\n")
    print('******************************************************************************')
    print("\n")

    predictors = {'dich_bio': 3, 'biomarker': 3, 'patient_age': 2, 'patient_gender':2, 'lang_heb': 2, 'CAD_call': 2,
                  'PVD_call': 2, 'HT_call': 2, 'Diab_call': 2, 'Cancer_call': 2, 'CKD_call': 2, 'COPD_call': 2,
                  'lung_disease': 2, 'BetaBlockers': 2, 'ACI': 2, 'Diuretic': 2, 'Spironolactone': 2, 'Statins': 2,
                  'MAP': 2, 'eGFR': 5, 'low_eGFR': 2, 'BMI': 2, 'obesity': 2, 'creatinine': 6,
                  'trig': 6, 'cholesterol': 6, 'hemog': 6, 'LDL': 6, 'glucose': 6, 'hosp': 2,
                  'CHF_duration': 2}

    for predictor in predictors.keys():
        calc_uni_cox(predictor, data, digits=predictors[predictor])

    return

def multi_cox(data_input, bio, results_path):

    data = data_input.copy()
    print("Performing multivariate cox regression... ")
    print("\n")
    print('******************************************************************************')
    print("\n")

    lang_heb = np.where((data.language == 'hebrew'), 1, 0)
    data = data.assign(lang_heb=lang_heb)

    ### Basic Cox

    print('Computing Cox Regression for dich_bio, age and gender')
    print("\n")

    cf = CoxPHFitter()

    data_cox = data[[bio, 'patient_age', 'patient_gender', 'mortality', 'duration']]
    cf.fit(data_cox, 'duration', event_col='mortality')
    cf = CoxPHFitter()
    cf.fit(data_cox, 'duration', event_col='mortality')
    cf.print_summary()
    print()

    cox_summary_CI(cf.summary)

    if not os.path.exists(results_path):
        os.makedirs(results_path)
    cf.summary.to_csv(results_path + '/' + 'cox_basic.csv', index=False, encoding='utf-8-sig')  # save cox output as xlsx

    print()
    print('******************************************************************************')
    print("\n")




    ### Basic + Clinical Cox

    print('Computing Cox Regression for age, gender and clinical parameters')
    print("\n")

    data_cox = data[[bio, 'patient_age', 'patient_gender', 'PVD_call', 'HT_call',
                     'lung_disease', 'lang_heb','Cancer_call', 'mortality', 'duration']]
  # ,'CAD_call', 'CKD_call', 'hosp','chf_duration',

    # data_cox.dropna(inplace=True)

    cf = CoxPHFitter()
    cf.fit(data_cox, 'duration', event_col='mortality')
    cf.print_summary()

    cox_summary_CI(cf.summary)

    cf.summary.to_csv(results_path + '/' + 'cox_registries.csv', index=False, encoding='utf-8-sig')  # save cox output as xlsx

    print()
    print('******************************************************************************')
    print("\n")


    ### All Predictors Cox
    data_cox = data[[bio, 'patient_age', 'PVD_call', 'HT_call','hemog',
                     'lung_disease','Cancer_call','MAP','trig','hosp','obesity',
                     'creatinine', 'BMI', 'CKD_call', 'eGFR','mortality', 'duration']].copy()
    #, ','chf_duration','lang_heb''patient_gender','CAD_call'
    data_cox.dropna(subset=['obesity'], inplace=True)  # remove NaNs
    data_cox.dropna(subset=['MAP'], inplace=True)  # remove NaNs
    data_cox.dropna(subset=['hemog'], inplace=True)  # remove NaNs
    data_cox.dropna(subset=['trig'], inplace=True)  # remove NaNs
    data_cox.dropna(subset=['creatinine'], inplace=True)  # remove NaNs
    data_cox.dropna(subset=['BMI'], inplace=True)  # remove NaNs
    data_cox.dropna(subset=['hosp'], inplace=True)  # remove NaNs
    data_cox.dropna(subset=['eGFR'], inplace=True)  # remove NaNs

    print("\n")
    print('******************************************************************************')
    print("\n")
    print('Computing Cox Regression for all predictors')
    print("\n")

    cf.fit(data_cox, 'duration', event_col='mortality')
    cf = CoxPHFitter()
    cf.fit(data_cox, 'duration', event_col='mortality')
    cf.print_summary()

    cox_summary_CI(cf.summary)

    cf.summary.to_csv(results_path + '/' + 'cox_all.csv', index=False, encoding='utf-8-sig')  # save cox output as xlsx

    print('Finished computing Multivariate Cox Regression, and saving results. Results are saved in: ', results_path, '\\cox_all.csv')


    return
def km_curve():
    T = data['duration'].values
    C = data['mortality'].values
    time_points = 25
    med_duration = np.median(data.duration)

    ax = plt.subplot(111)
    t = np.linspace(0, med_duration, time_points)

    kmf1 = KaplanMeierFitter()
    kmf2 = KaplanMeierFitter()

    iq_0 = np.where(data['dich_bio'] == 0)[0]
    iq_1 = np.where(data['dich_bio'] == 1)[0]

    ax = kmf1.fit(T[iq_0], C[iq_0], label='Low Biomarker', timeline=t).plot(ax=ax)
    ax = kmf2.fit(T[iq_1], C[iq_1], label='High Biomarker', timeline=t).plot(ax=ax)

    plt.xlabel('Time (Days)')
    plt.ylabel('Survival Probability (%)')
    plt.title('Survival Probability as a function of the Vocal Biomarker')
    add_at_risk_counts(kmf1, kmf2)
    savepath = os.path.join(cfg.RESULTS_PATH, 'km_figure.png')
    plt.savefig(savepath)  # save fig

    high_bio = np.where(data['dich_bio'] == [1])[0]
    results = logrank_test(T[high_bio], T[~high_bio], C[high_bio], C[~high_bio], alpha=.99)
    results.print_summary()


    return results
def int_analysis(data_input,bio):
    data = data_input.copy()

    def prep_int(data,bio):
        """creates interaction fields (biomarker * parameter)"""

        age_med = np.where(data.patient_age <= (np.median(data.patient_age)), 0, 1)
        data = data.assign(age_med=age_med)

        if bio == 'dich_bio':

            bio_age = np.where((data.age_med * data.dich_bio == 1) , 1, 0)
            data = data.assign(bio_age=bio_age)

            bio_gender = np.where((data.patient_gender * data.dich_bio == 1) , 1, 0)
            data = data.assign(bio_gender=bio_gender)

            lang_heb = np.where((data.language ==  'hebrew') , 1, 0)
            data = data.assign(lang_heb=lang_heb)

            bio_language = np.where((data.lang_heb * data.dich_bio == 1) , 1, 0)
            data = data.assign(bio_language=bio_language)

            bio_hosp = np.where((data.hosp * data.dich_bio == 1) , 1, 0)
            data = data.assign(bio_hosp=bio_hosp)

            bio_CAD = np.where((data.CAD_call * data.dich_bio == 1) , 1, 0)
            data = data.assign(bio_CAD=bio_CAD)

            bio_PVD = np.where((data.PVD_call * data.dich_bio == 1) , 1, 0)
            data = data.assign(bio_PVD=bio_PVD)

            bio_HT = np.where((data.HT_call * data.dich_bio == 1) , 1, 0)
            data = data.assign(bio_HT=bio_HT)

            bio_CKD = np.where((data.CKD_call * data.dich_bio == 1) , 1, 0)
            data = data.assign(bio_CKD=bio_CKD)

            bio_lung = np.where((data.lung_disease * data.dich_bio == 1) , 1, 0)
            data = data.assign(bio_lung=bio_lung)

            bio_cancer = np.where((data.Cancer_call * data.dich_bio == 1) , 1, 0)
            data = data.assign(bio_cancer=bio_cancer)

            data.to_csv(cfg.COX_PATH + int_dich, index=False, encoding='utf-8-sig')  # save cox output as xlsx

            data_eGFR = data.copy()
            data_eGFR.dropna(subset=['low_eGFR'], inplace=True)
            bio_eGFR = np.where((data_eGFR.low_eGFR * data_eGFR.dich_bio == 1) , 1, 0)
            data_eGFR = data_eGFR.assign(bio_eGFR=bio_eGFR)
            data_eGFR.to_csv(cfg.COX_PATH + 'cox_int_dich_eGFR.csv', index=False, encoding='utf-8-sig')  # save cox output as xlsx

            data_obes = data.copy()
            data_obes.dropna(subset=['obesity'], inplace=True)
            bio_obesity = np.where((data_obes.obesity * data_obes.dich_bio == 1), 1, 0)
            data_obes = data_obes.assign(bio_obesity=bio_obesity)
            data_obes.to_csv(cfg.COX_PATH + 'cox_int_dich_obes.csv', index=False, encoding='utf-8-sig')  # save cox output as xlsx

            print('\n' * 50)

            return

        if bio == 'biomarker':

            data['bio_age'] = data.patient_age * data.biomarker
            data['bio_gender'] = data.patient_gender * data.biomarker
            data['lang_heb'] = np.where((data.language == 'hebrew'), 1, 0)
            data['bio_language'] = data.lang_heb * data.biomarker
            data['bio_hosp'] = data.hosp * data.biomarker
            data['bio_CAD'] = data.CAD_call * data.biomarker
            data['bio_PVD'] = data.PVD_call * data.biomarker
            data['bio_HT'] = data.HT_call * data.biomarker
            data['bio_CKD'] = data.CKD_call * data.biomarker
            data['bio_lung'] = data.lung_disease * data.biomarker
            data['bio_cancer'] = data.Cancer_call * data.biomarker
            data.to_csv(cfg.COX_PATH + int_con, index=False, encoding='utf-8-sig')  # save cox output as xlsx
            data_eGFR = data
            data_eGFR.dropna(subset=['low_eGFR'], inplace=True)
            data_eGFR['bio_eGFR'] = data_eGFR.low_eGFR * data_eGFR.biomarker
            data_eGFR.to_csv(cfg.COX_PATH + 'cox_int_con_eGFR.csv', index=False, encoding='utf-8-sig')  # save cox output as xlsx
            data_obes = data
            data_obes.dropna(subset=['obesity'], inplace=True)
            data_obes['bio_obesity'] = data_obes.obesity * data_obes.biomarker
            data_obes.to_csv(cfg.COX_PATH + 'cox_int_con_obes.csv', index=False, encoding='utf-8-sig')  # save cox output as xlsx

            print('\n' * 50)

            return

    def cox_int(predictor):
        """performs multi-variate cox regression (table 3B) with a different interaction variable each time"""

        cf = CoxPHFitter()

        if bio == 'dich_bio':
            data_df = pd.read_csv(cfg.COX_PATH + int_dich, index_col=None, encoding='utf-8-sig')
            data_eGFR = pd.read_csv(cfg.COX_PATH + 'cox_int_dich_eGFR.csv', index_col=None, encoding='utf-8-sig')
            data_obes = pd.read_csv(cfg.COX_PATH + 'cox_int_dich_obes.csv', index_col=None, encoding='utf-8-sig')

        if bio == 'biomarker':
            data_df = pd.read_csv(cfg.COX_PATH + int_con, index_col=None, encoding='utf-8-sig')
            data_eGFR = pd.read_csv(cfg.COX_PATH + 'cox_int_con_eGFR.csv', index_col=None, encoding='utf-8-sig')
            data_obes = pd.read_csv(cfg.COX_PATH + 'cox_int_con_obes.csv', index_col=None, encoding='utf-8-sig')

        print("\n")
        print('******************************************************************************')
        print("\n")
        print('Computing Cox Regression with', bio, 'and', predictor, '...')
        print("\n")

        if predictor == 'bio_eGFR':
            data_cox = data_eGFR[[bio, 'patient_age', 'patient_gender', 'CAD_call', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                 'Cancer_call', 'hosp', predictor, 'mortality', 'duration']]
        elif predictor == 'bio_obesity':
            data_cox = data_obes[[bio, 'patient_age', 'patient_gender', 'CAD_call', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                 'Cancer_call', 'hosp', predictor, 'mortality', 'duration']]
        else:
            data_cox = data_df[[bio, 'patient_age', 'patient_gender', 'CAD_call', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                 'Cancer_call', 'hosp', predictor, 'mortality', 'duration']]


        cf.fit(data_cox, 'duration', event_col='mortality')
        cf = CoxPHFitter()
        cf.fit(data_cox, 'duration', event_col='mortality')
        cf.print_summary()
        # print('lower 0.95:', np.exp(cf.summary['lower 0.95']).round(2))
        # print('upper 0.95:', np.exp(cf.summary['upper 0.95']).round(2))
        print()

        if bio == 'dich_bio':
            cf.summary.to_csv(cfg.RESULTS_PATH + '/' + int_dich + '_' + predictor + '.csv', index=False, encoding='utf-8-sig')  # save cox output as xlsx

        if bio == 'con_bio':
            cf.summary.to_csv(cfg.RESULTS_PATH + '/' + int_con + '_' + predictor + '.csv', index=False, encoding='utf-8-sig')  # save cox output as xlsx

        alpha = 0.05

        p = cf.summary.loc[cf.summary.index[10], 'p']

        global sig
        if p < alpha:
            sig = 1
            print('----------------------------------------------------------------')
            print("\n")
            print(predictor, 'is significant.')
            print("\n")
            print('----------------------------------------------------------------')
            print()
        else:
            sig = 0
            print('----------------------------------------------------------------')
            print("\n")
            print(predictor, 'is not significant.')
            print("\n")

        return

    def cox_seperated(predictor):
        """performs cox regression seperately for each of group in the specified interaction predictor"""

        #if sig == 1:

        print('Computing Cox Resression seperately for', predictor, 'groups, with', bio, ':')
        print()
        print('----------------------------------------------------------------')
        print("\n")

        cf = CoxPHFitter()

        if bio == 'dich_bio':
            data_df = pd.read_csv(cfg.COX_PATH + int_dich, index_col=None, encoding='utf-8-sig')
            data_eGFR = pd.read_csv(cfg.COX_PATH + 'cox_int_dich_eGFR.csv', index_col=None, encoding='utf-8-sig')
            data_obes = pd.read_csv(cfg.COX_PATH + 'cox_int_dich_obes.csv', index_col=None, encoding='utf-8-sig')

        if bio == 'biomarker':
            data_df = pd.read_csv(cfg.COX_PATH + int_con, index_col=None, encoding='utf-8-sig')
            data_eGFR = pd.read_csv(cfg.COX_PATH + 'cox_int_con_eGFR.csv', index_col=None, encoding='utf-8-sig')
            data_obes = pd.read_csv(cfg.COX_PATH + 'cox_int_con_obes.csv', index_col=None, encoding='utf-8-sig')

        group0 = data_df.loc[data_df[predictor] == 0]
        group1 = data_df.loc[data_df[predictor] == 1]

        if predictor == 'patient_gender':
            cox0 = group0[[bio, 'patient_age', 'CAD_call', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                           'Cancer_call', 'hosp', 'mortality', 'duration']]
            cox1 = group1[[bio, 'patient_age', 'CAD_call', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                           'Cancer_call', 'hosp', 'mortality', 'duration']]

        if predictor == 'age_med':
            cox0 = group0[[bio, 'patient_gender', 'CAD_call', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                           'Cancer_call', 'hosp', 'mortality', 'duration']]
            cox1 = group1[[bio, 'patient_gender', 'CAD_call', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                           'Cancer_call', 'hosp', 'mortality', 'duration']]

        if predictor == 'lang_heb':
            cox0 = group0[[bio, 'patient_age', 'patient_gender', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                           'hosp', 'mortality', 'duration']]
            cox1 = group1[[bio, 'patient_age', 'patient_gender', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                           'hosp', 'mortality', 'duration']]

        if predictor == 'hosp':
            cox0 = group0[[bio, 'patient_age', 'patient_gender', 'CAD_call', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                           'Cancer_call', 'mortality', 'duration']]
            cox1 = group1[[bio, 'patient_age', 'patient_gender', 'CAD_call', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                           'Cancer_call', 'mortality', 'duration']]

        if predictor == 'CAD_call':
            cox0 = group0[[bio, 'patient_age', 'patient_gender', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                           'Cancer_call', 'hosp', 'mortality', 'duration']]
            cox1 = group1[[bio, 'patient_age', 'patient_gender', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                           'Cancer_call', 'hosp', 'mortality', 'duration']]

        if predictor == 'PVD_call':
            cox0 = group0[[bio, 'patient_age', 'patient_gender', 'HT_call', 'CKD_call', 'lung_disease',
                           'Cancer_call', 'hosp', 'mortality', 'duration']]
            cox1 = group1[[bio, 'patient_age', 'patient_gender', 'HT_call', 'CKD_call', 'lung_disease',
                           'Cancer_call', 'hosp', 'mortality', 'duration']]

        if predictor == 'HT_call':
            cox0 = group0[[bio, 'patient_age', 'patient_gender', 'CAD_call', 'PVD_call', 'CKD_call', 'lung_disease',
                           'Cancer_call', 'hosp', 'mortality', 'duration']]
            cox1 = group1[[bio, 'patient_age', 'patient_gender', 'CAD_call', 'PVD_call', 'CKD_call', 'lung_disease',
                           'Cancer_call', 'hosp', 'mortality', 'duration']]

        if predictor == 'Cancer_call':
            cox0 = group0[
                [bio, 'patient_age', 'patient_gender', 'CAD_call', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                 'hosp', 'mortality', 'duration']]
            cox1 = group1[
                [bio, 'patient_age', 'patient_gender', 'CAD_call', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                 'hosp', 'mortality', 'duration']]

        if predictor == 'lung_disease':
            cox0 = group0[[bio, 'patient_age', 'patient_gender', 'CAD_call', 'PVD_call', 'HT_call', 'CKD_call',
                           'hosp', 'mortality', 'duration']]
            cox1 = group1[[bio, 'patient_age', 'patient_gender', 'CAD_call', 'PVD_call', 'HT_call', 'CKD_call',
                           'hosp', 'mortality', 'duration']]

        if predictor == 'obesity':
            non_obese = data_obes.loc[data_obes[predictor] == 0]
            obese = data_obes.loc[data_obes[predictor] == 1]

            cox0 = non_obese[[bio, 'patient_age', 'patient_gender', 'CAD_call', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                        'hosp', 'mortality', 'duration']]
            cox1 = obese[[bio, 'patient_age', 'patient_gender', 'CAD_call', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                        'hosp', 'mortality', 'duration']]

        if predictor == 'low_eGFR':
            normal_eGFR = data_eGFR.loc[data_eGFR[predictor] == 0]
            low_eGFR = data_eGFR.loc[data_eGFR[predictor] == 1]

            cox0 = normal_eGFR[[bio, 'patient_age', 'patient_gender', 'CAD_call', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                        'hosp', 'mortality', 'duration']]
            cox1 = low_eGFR[[bio, 'patient_age', 'patient_gender', 'CAD_call', 'PVD_call', 'HT_call', 'CKD_call', 'lung_disease',
                        'hosp', 'mortality', 'duration']]

        print('Cox Regression for group0 in', predictor, ':')
        cf.fit(cox0, 'duration', event_col='mortality')
        cf.print_summary()
        print('lower 0.95:', np.exp(cf.summary['lower 0.95']).round(2))
        print('upper 0.95:', np.exp(cf.summary['upper 0.95']).round(2))
        print
        cf.summary.to_csv(cfg.RESULTS_PATH + '/' + 'cox_int_' + bio + predictor + '0.csv', index=False, encoding='utf-8-sig')  # save cox output as xlsx

        print("\n")
        print('----------------------------------------------------------------')
        print("\n")

        print('Cox Regression for group1 in', predictor, ':')
        cf.fit(cox1, 'duration', event_col='mortality')
        cf.print_summary()
        print('lower 0.95:', np.exp(cf.summary['lower 0.95']).round(2))
        print('upper 0.95:', np.exp(cf.summary['upper 0.95']).round(2))
        print
        cf.summary.to_csv(cfg.RESULTS_PATH + '/' + 'cox_int_' + bio + predictor + '1.csv', index=False, encoding='utf-8-sig')  # save cox output as xlsx

        return


     #   return


    ################################

    prep_int(data,bio)

    cox_int('bio_age')
    cox_seperated('age_med')

    cox_int('bio_gender')
    cox_seperated('patient_gender')

    cox_int('bio_language')
    cox_seperated('lang_heb')

    cox_int('bio_hosp')
    cox_seperated('hosp')

    cox_int('bio_PVD')
    cox_seperated('PVD_call')

    cox_int('bio_HT')
    cox_seperated('HT_call')

    cox_int('bio_lung')
    cox_seperated('lung_disease')

    cox_int('bio_cancer')
    cox_seperated('Cancer_call')

    cox_int('bio_eGFR')
    cox_seperated('low_eGFR')

    cox_int('bio_obesity')
    cox_seperated('obesity')

    return

if __name__ == '__main__':
    int_dich = 'cox_int_dich.csv'
    int_con = 'cox_int_con.csv'
    data = pd.read_csv(cfg.COX_PATH + 'cox_final.csv', index_col=False, encoding='utf-8-sig')

    uni_cox(data)
    multi_cox(data, cfg.BIOMARKER_TYPE, cfg.RESULTS_PATH)
    km_curve()
    int_analysis(data,cfg.BIOMARKER_TYPE)