from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline
from sklearn import mixture
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler, RobustScaler
from multiprocessing import Pool
from matplotlib import pyplot
from scipy import stats
import itertools
import sys
from cycler import cycler
try:
    from pandas.plotting import scatter_matrix
except ImportError:
    from pandas.tools.plotting import scatter_matrix
from sklearn.manifold import TSNE
from scipy import linalg
from matplotlib import patches


def plot_confusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=pyplot.cm.jet):

    indy = np.sum(cm, axis=1) > 0
    cm = cm[indy, :]
    indx = np.sum(cm, axis=0) > 0
    cm = cm[:, indx]
    pyplot.imshow(cm, interpolation='nearest', cmap=cmap)
    pyplot.title(title)
    pyplot.colorbar()
    pyplot.xticks(np.arange(np.sum(indx)), classes[indx], rotation=90)
    pyplot.yticks(np.arange(np.sum(indy)), classes[indy])

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        pyplot.text(j, i, cm[i, j], horizontalalignment="center", color="white" if cm[i, j] > thresh else "black")

        pyplot.tight_layout()
    pyplot.ylabel('True label')
    pyplot.xlabel('Predicted label')


def plot_scatter_matrix(X, y, X_train, transformer=None, gmm=None, listy=None, robust=False):

    if len(X_train):
        if transformer is None:
            steps = []
            if robust:
                steps.append(('scaler', RobustScaler()))
            else:
                steps.append(('scaler', StandardScaler()))
            steps.append(('pca', PCA(n_components=np.minimum(5, X_train.shape[1]))))
            transformer = Pipeline(steps)
        transformer.fit(X_train)
    if transformer is None:
        Xt = X
    else:
        Xt = transformer.transform(X)

    if len(y):
        color_wheel = create_color_wheel(y)
        colors = [color_wheel[i] for i in y]
    else:
        color_wheel = ''
        colors = 'black'
    splot = scatter_matrix(pd.DataFrame(data=Xt[:, :3]), c=colors, alpha=0.6, s=100)

    if gmm is not None:
        if listy is None:
            model_colors = ['gold']*gmm.means_.shape[0]
        else:
            model_colors = [color_wheel[i] for i in listy]
        plot_gmm(gmm.means_, gmm.covariances_, splot, model_colors)

    pyplot.suptitle(color_wheel)


def plot_TSNE(X, y):

    X_embedded = TSNE(n_components=2).fit_transform(X)
    color_wheel = create_color_wheel(y)
    pyplot.scatter(X_embedded[:, 0], X_embedded[:, 1], s=100, c=[color_wheel[i] for i in y], alpha=0.6)
    pyplot.suptitle(color_wheel)


def create_color_wheel(y):

    color_cycle = cycler(color=['red', 'blue', 'green', 'black', 'cyan', 'magenta', 'yellow', 'pink'])
    cc = color_cycle()

    color_wheel = {}
    unq_y = list(set(y))
    for i, j in zip(unq_y, cc):
        color_wheel[i] = list(j.values())[0]
    return color_wheel


def split_ind(n, n_jobs):

    ind_start = 0
    ind_step = int(np.ceil(n/n_jobs))
    ind = []
    for i in range(n_jobs):
        ind.append([ind_start, ind_start + ind_step])
        ind_start += ind_step
        if ind_start > n:
            break
    ind[-1][-1] = n

    return ind


def run_parallel(part_fun, num_el, n_jobs):

    args = split_ind(num_el, n_jobs)

    if n_jobs > 1:
        pool = Pool(processes=n_jobs)
        results = pool.map(part_fun, args)
        pool.close()
        pool.join()
    else:
        results = part_fun(args[0])

    return results


def bhattacharyya_distance(mu1, mu2, covar1, covar2):

    covar = 0.5*(covar1 + covar2)
    mud = (mu1 - mu2).reshape(-1, 1)

    return (1/8*np.dot(np.dot(mud.T, linalg.inv(covar)), mud) +
            0.5*np.log(linalg.det(covar)/(linalg.det(covar1)*linalg.det(covar2))**0.5))[0][0]


def print_mean_std(x, title, robust=False):

    if robust:
        m = np.median(x)
        s = stats.iqr(x)
    else:
        m = np.mean(x)
        s = np.std(x)

    print(title + ': ' + str(np.around(m, decimals=3)) + '(' + str(np.around(s, decimals=3)) + ')')


def print_elapsed(title, counter, tot_count, step=10):
    step = np.minimum(step, tot_count)
    counter += 1
    if not np.remainder(counter, step):
        print(str((100 * counter) // tot_count) + '% ' + title)
    return counter


def validate_flow_operators(operators):

    for i in range(len(operators)):
        if i == 0:
            continue
        operators[i] = operators[i] or operators[i-1]
    return operators


def plot_gmm(means, covariances, splot, colors):

    for i in range(splot.shape[0]):
        for j in range(splot.shape[1]):

            if i == j:
                continue

            for k in range(means.shape[0]):
                v, w = linalg.eigh(np.array([[covariances[k, j, j], covariances[k, j, i]],
                                             [covariances[k, i, j], covariances[k, i, i]]]))
                v = 2. * np.sqrt(2.) * np.sqrt(v)
                u = w[0] / linalg.norm(w[0])

                # Plot an ellipse to show the Gaussian component
                angle = np.arctan(u[1] / u[0])
                angle = 180. * angle / np.pi  # convert to degrees
                ell = patches.Ellipse(means[k, [j, i]], v[0], v[1], 180. + angle, color=colors[k])
                ell.set_clip_box(splot[i, j].bbox)
                ell.set_alpha(0.5)
                splot[i, j].add_artist(ell)


def read_label_file(filename, prefix='', selected_label=None, excluded_label=[]):

    try:
        curr_labels = np.genfromtxt(filename, dtype=None)
        failed = False
    except ValueError:
        print('failed to read ' + filename)
        failed = True
        return failed, [], [], []
    seg_start = []
    seg_end = []
    label = []
    for i in curr_labels:
        try:
            curr_label = i[2].decode("utf-8")
        except:
            print('failed to read ' + filename)
            failed = True
            return failed, [], [], []

        if selected_label:
            if curr_label != selected_label:
                continue
        if curr_label in excluded_label:
            continue
        seg_start.append(np.around(i[0], decimals=1))
        seg_end.append(np.around(i[1], decimals=1))
        label.append(prefix + curr_label)

    return failed, label, seg_start, seg_end


def simpleaudio_preprocess(audio):

    # normalize to 16-bit range
    audio *= 32767
    # convert to 16-bit data
    audio = audio.astype(np.int16)
    return audio

def progress_bar(count,total,status=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))
    percent = round(100.0 * count / float(total),1)

    bar = '#' * filled_len + '-' * (bar_len - filled_len)
    sys.stdout.write('\r[%s] %s%s %s' % (bar,percent,'%',status))
    sys.stdout.flush()


if __name__ == '__main__':

    n = 3

    steps = []
    steps.append(('scaler', StandardScaler()))
    steps.append(('pca', PCA(n_components=np.minimum(n, 3))))
    pipeline = Pipeline(steps)

    n_samples = 500

    # Generate random sample, two components
    np.random.seed(0)
    C = np.random.rand(1, n)
    X_pca = np.dot(np.random.randn(500, n), np.random.rand(n, n))
    X_train = np.dot(np.random.randn(500, n), np.random.rand(n, n))
    X_test = np.dot(np.random.randn(500, n), np.random.rand(n, n)) + 2
    y = np.zeros((X_test.shape[0],))
    # Fit a Gaussian mixture with EM using five components
    pipeline.fit(X_pca)
    tX_train = pipeline.transform(X_train)
    gmm = mixture.GaussianMixture(n_components=2, covariance_type='full').fit(tX_train)

    X1 = np.concatenate((X_test, X_train))
    y1 = np.concatenate((y, 2 * np.ones((X_train.shape[0],))))
    plot_scatter_matrix(X1, np.ravel(y1), [], transformer=pipeline, gmm=gmm)











