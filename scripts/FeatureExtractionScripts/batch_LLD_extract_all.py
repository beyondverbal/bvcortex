import sys
import pandas as pd
import os
sys.path.append("../.")
import wrapper4LLD_offline as LLD
import utils
import numpy as np


RECALL = True # False #
print('RECALL is set to', RECALL)

DATA_SET = 'MACCABI' # 'CELEBS' # 'CHINA_CAD' # 'MAYO' #
DEBUG_LIMIT = False # True #

def convert_start_end(x):
    # print(x)
    if type(x) is float:
        return x
    xx = x.split(':')
    return int(xx[-1])

if __name__ == '__main__':
    if DATA_SET == 'MAYO':
        samples_list = 'C:/BVC/HealthAnalysis/Mayo/MetaData/mayo_full_171227.csv'
        features_folder = 'C:/BVC/HealthAnalysis/Mayo/LLDcsv'
        samples_folder = 'C:\\BVC\\HealthAnalysis\\Mayo\\VoiceSamples'
        full_lld_folder = None

        samples_df = pd.read_csv(samples_list, index_col=None)
        samples_df = samples_df.dropna(how='all')
        if DEBUG_LIMIT:
            samples_df = samples_df[75:85]

        # print(samples_df['name'])
        # print(type(samples_df['name'].iloc[0]))
        #
        wav_list = samples_df['name'].apply(lambda x: x + '.wav')
        # print(wav_list)
        seg_start = samples_df['start'].apply(lambda x: convert_start_end(x))
        seg_end = samples_df['end'].apply(lambda x: convert_start_end(x))
        segments_list = []
        for i in range(len(seg_start)):
            segments_list.append(zip([seg_start[i]], [seg_end[i]]))

        # segments_list = zip(seg_start,seg_end)
    elif DATA_SET == 'MACCABI':
        samples_folder = None
        full_lld_folder = 'E:/Algo/dat/PAT'
        # results = pd.read_csv('E:/Algo/dat/MetaData/SegmentsData181003.csv', index_col=None)
        results = pd.read_csv('E:/Algo/dat/MetaData/DB4Segments190127.csv', index_col=None)
        features_folder = 'E:/Algo/dat/PATsliced'
        segments_list = []
        wav_list = []
        all_files = np.unique(results.filename.values)

        # fix negative seg_start
        fix_idx = results[results['seg_start'] < 0].index.values
        results.loc[fix_idx, 'seg_start'] = 0

        counter = 0
        if DEBUG_LIMIT:
            all_files = all_files[:5]
        for curr_file in all_files:  # todo: something works very slow in this loop
            counter = utils.print_elapsed('prepare data', counter, len(all_files), step=100)
            ind_file = results.filename.values == curr_file
            curr_results = results.loc[ind_file].copy()
            curr_results.sort_values('seg_start', inplace=True)
            segments_list.append(zip(curr_results.seg_start.values, curr_results.seg_end.values))
            if np.any(curr_results.seg_start.values < 0):
                print('negative seg_start found!', curr_file)
            if np.any( np.diff(curr_results.seg_start.values) <= 0):
                print('mis-ordered seg_start found!', curr_file)
            patient_id = curr_results.patient_id.values[0]  # works only if no two patients share a file!!
            wav_list.append(os.path.join('E:/recordings/by_p/', str(patient_id), str(curr_file) + '.wav'))

    elif DATA_SET == 'CHINA_CAD':
        features_folder = 'C:/BVC/HealthAnalysis/China_CAD/LLD'
        samples_folder = 'C:\\BVC\\HealthAnalysis\\China_CAD\\Recordings'
        full_lld_folder = None

        wav_list = []
        for file in os.listdir(samples_folder):
            if file.endswith('.wav'):
                wav_list.append(file)

        if DEBUG_LIMIT:
            wav_list = wav_list[:5]
        segments_list = None
    elif DATA_SET == 'CELEBS':
        features_folder = 'C:/BVC/HealthAnalysis/CelebsHeartIssues/LLD'
        samples_folder = 'C:\\BVC\\HealthAnalysis\\CelebsHeartIssues\\RecordingsOverTime'
        full_lld_folder = None

        wav_list = []
        for root, directories, filenames in os.walk(samples_folder):
            for filename in filenames:
                if filename.endswith('.wav'):
                    wav_list.append(os.path.join(root,filename))
        samples_folder = None
        segments_list = None

    LLD.extract_list(wav_list, segments_list, feature_path=features_folder, samples_path=samples_folder, recall=RECALL, full_lld_path=full_lld_folder)
