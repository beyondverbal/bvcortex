import numpy as np
import pandas as pd
from scipy.stats import pearsonr

def process_10seg(input, output_path):
    engine_HLD = pd.read_csv(input, index_col=None)

    # keep first two segments
    engine_HLD = engine_HLD[(engine_HLD.start == 0) | (engine_HLD.start == 10)]

    # only keep files with 2 10-sec segments
    engine_HLD = filter_by_segment_num(engine_HLD, 2)

    # remove some columns
    keep_cols = [col for col in engine_HLD.columns if (col!='start') and (col!='end')]
    engine_HLD = engine_HLD[keep_cols]

    keep_cols = [c for c in engine_HLD.columns if not c.startswith('voice_detected')]
    engine_HLD = engine_HLD[keep_cols]

    feature_noise(engine_HLD, output_path)


def filter_by_segment_num(df, segment_num):

    df.reset_index(inplace=True)
    file_list, counts = np.unique(df.filename.values, return_counts=True)
    file_idx = np.where(counts == segment_num)
    file_list = file_list[file_idx[0]]
    keep_idx = [idx for idx in df.index if df.loc[idx, 'filename'] in file_list]
    return df.iloc[keep_idx,:]

def feature_noise(hld_df, output_path):

    first_df = hld_df.drop_duplicates(subset='filename', keep='first')
    second_df = hld_df.drop_duplicates(subset='filename', keep='last')

    # Consistency check
    if len(np.where(first_df.segment.values != 1)[0]) != 0:
        print('WARNING: Problem in first_df, exiting')
        return
    if len(np.where(second_df.segment.values != 2)[0]) != 0:
        print('WARNING: Problem in second_df, exiting')
        return
    if first_df.shape[0] != second_df.shape[0]:
        print('WARNING: df size mismatch, exiting')
        return

    # first_x_col = np.where(hld_df.columns.values == 'mfcc_0_moments8_0')[0][0]
    first_x_col = np.where(hld_df.columns.values == 'mfcc_0_deciles5_0')[0][0]
    last_x_col = hld_df.columns.values[-1]
    print('first feature column {}; last feature column {}'.format(hld_df.columns[first_x_col], last_x_col))

    first_X = first_df.iloc[:, first_x_col:].values
    last_X = second_df.iloc[:, first_x_col:].values
    feature_names = hld_df.columns[first_x_col:]

    corr = []
    for i in range(len(feature_names)):
        pears_r = pearsonr(first_X[:, i], last_X[:, i])[0]
        corr.append(pears_r)

    corr = np.array(corr)

    one_arg = np.where(corr == 1)[0]
    nan_arg = np.where(corr != corr)[0]
    remove_idx = np.append(one_arg, nan_arg)

    corr = np.array([corr[i] for i in range(len(corr)) if i not in remove_idx])
    feature_names = [feature_names[i] for i in range(len(feature_names)) if i not in remove_idx]

    output = pd.DataFrame(corr, columns=['corr'], index=feature_names)
    output.index.name = 'feature'

    output.to_csv(output_path)


if __name__ == "__main__":

    date = '190128'

    input = 'E:/Algo/dat/HLD/HLD190128_PAT_D5_10sec.csv'
    output = 'E:/Algo/dat/HLD/feature_noise_' + date + '_D5.csv'

    process_10seg(input, output)


