import os
import csv
import numpy as np
from subprocess import call
from subprocess import check_output
from PATrends import PATConfig as Cfg
from scripts.FeatureExtractionScripts import matlab_config as MatlabCfg
MATLAB_FOLDER = os.path.join(Cfg.dev, 'smiling_arthur/application/')
# os.path.isfile(os.path.join(os.getcwd(), MATLAB_CONFIG))
MATLAB_CONFIG = os.path.join(os.getcwd(),'scripts\FeatureExtractionScripts\matlab_config.py')

def main(wav_list):

    # construct inputs for EXE

    # audio list
    audio_list = 'C:/BVC/pythonanalyzer/src/MATLAB/TEMP/feature_matrix_test_short_err.csv'
    # cfg file

    # run
    # cmd = os.path.join(MATLAB_FOLDER, 'smiling_arthur.exe ' + audio_list + ' ' + MATLAB_CONFIG)
    cmd_output = check_output([os.path.join(MATLAB_FOLDER, 'smiling_arthur.exe'), audio_list, MATLAB_CONFIG])
    # failed = call(cmd)

    print('a = {}'.format("".join(map(chr, cmd_output))))

    read_output(MatlabCfg.out_file)

def extract_lld(wav_name):

    temp_out_name = wav_name.split(os.sep)[-1]
    if temp_out_name[-4:] == '.wav':
        temp_out_name = temp_out_name[:-4]
    temp_out = os.path.join(SMILE_FOLDER, temp_out_name+'os_out.csv')

    cmd = os.path.join(SMILE_FOLDER, 'SMILExtract_Release.exe') + \
          ' -configfile ' + os.sep.join(os.path.realpath(__file__).split(os.sep)[:-1] + [SMILE_CONFIG]) + \
          ' -inputfile ' + wav_name + \
          ' -O ' + temp_out + \
          ' -loglevel 0'
    failed = call(cmd)

    if failed == 3:
        corrected_wav = correct_wav(wav_name)
        failed = call(cmd.replace(wav_name, corrected_wav))

    if failed:
        return True, [], []
    feature_names, vals, t = read_output(temp_out)

    t = np.expand_dims(np.array(t)*Cfg.SAMPLING_RATE, axis=1)
    t[0, 0] = 0
    t[1, 0] = Cfg.FRAME_STEP_FEATURES
    t = np.hstack((t, t + Cfg.FRAME_SIZE_FEATURES))

    vals = [np.expand_dims(i, axis=0) for i in vals]
    X = np.concatenate(vals, axis=0)

    return False, X, t, feature_names


def correct_wav(wav_in):

    wav_out = wav_in.split(os.sep)
    wav_out[-1] = '_' + wav_out[-1]
    wav_out = os.sep.join(wav_out)

    if os.path.isfile(wav_out):
        return wav_out

    call(os.path.join(SOX_FOLDER, 'sox.exe') + ' ' + wav_in + ' -e signed-integer ' + wav_out)

    return wav_out


def read_output(temp_out_filename):

    names = []
    vals = []
    time_stamp = []
    if os.path.isfile(temp_out_filename):
        with open(temp_out_filename) as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                if not len(row):
                    continue
                if row[0][0] is not '@':
                    time_stamp.append(np.array(row[1]).astype(float))
                    vals.append(np.array(row[2:-1]).astype(float))
                else:
                    curr_name = row[0].replace('@attribute ', '').replace(' numeric', '')
                    names.append(curr_name)

        os.remove(temp_out_filename)
        names = names[3:-2]
    else:
        pass

    return names, vals, time_stamp


if __name__ == '__main__':

    output_folder = 'C:\\BVC\\HealthAnalysis\\Mayo\\LLDpkl'
    input_folder = 'C:\\BVC\\HealthAnalysis\\Mayo\\VoiceSamples'

    output_folder = 'C:/BVC/YonaData1/Results/Ref'
    input_folder = 'C:/BVC/YonaData1'

    if not os.path.isfile(MATLAB_CONFIG):
        exit('MATLAB CONFIG DOESNT EXIST')
    if not os.path.isdir(MATLAB_FOLDER):
        exit('MATLAB FOLDER DOESNT EXIST')

    wav_list = []

    for file in os.listdir(os.path.join(input_folder)):
        if file.endswith(".wav") and file[0] != '_':
            wav_list.append(os.path.join(input_folder, file))

    main(wav_list)
