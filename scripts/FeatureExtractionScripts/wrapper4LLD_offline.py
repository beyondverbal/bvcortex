import os
import numpy as np
import pandas as pd
import librosa as lb
import sys
import pickle
import time
import random
from functools import partial
import multiprocessing
from multiprocessing import Pool
sys.path.append('../.')
sys.path.append('../..')
import utils
import PATrends.PATConfig as PATCfg
from PATrends.PATCalculator import PATCalculator
from TAVEngine.HLDCalculator import HLDCalculator
from TAVEngine import HLDConfig as HLDCfg


def extract_list(wav_list, segments_list=None, feature_path=None, samples_path=None, recall=False, full_lld_path=None):
    log_file = open(os.path.join(feature_path, 'lld_extract.log'), 'w')

    counter = 0
    all_lld_files = []

    for index, curr_wav in enumerate(wav_list):

        counter = utils.print_elapsed('lld extracted', counter, len(wav_list), step=2)

        if full_lld_path:
            lld_full = os.path.join(full_lld_path, str(os.path.splitext(curr_wav.split(os.sep)[-1])[0]) + '.pkl')
        else:
            lld_full = ''

        lld_filename = os.path.join(feature_path, str(os.path.splitext(curr_wav.split(os.sep)[-1])[0]) + '.pkl')

        if samples_path:
            curr_wav = samples_path + os.sep + curr_wav
        if recall and os.path.isfile(lld_filename):
            continue

        if os.path.isfile(lld_full):
            with open(lld_full, 'rb') as handle:
                lld_dict = pickle.load(handle)
                frame_num = len(next(iter(lld_dict.values()))[0, :])
                t_sec = np.array([i * PATCfg.FRAME_STEP_FEATURES_SEC for i in range(frame_num)])

        else:
            lld_dict, t_sec = compute_full_lld(curr_wav, convert2float32=True)
            if lld_dict is not None:
                with open(lld_full, 'wb') as handle:
                    pickle.dump(lld_dict, handle)
        if lld_dict is None:
            log_file.write(lld_filename + '\tfailed\n')
            continue

        # derivatives
        hld_calc = HLDCalculator()
        lld_dict = hld_calc.post_process_lld(lld_dict)

        # keep only lld_base
        lld_dict_slice = {}
        for feature_name in HLDCfg.LLD_BASE:
            lld_dict_slice.update({feature_name: lld_dict[feature_name]})

        # TODO: maybe fix this when 'VAD' is produced?
        # lld_dict_slice.update({'VAD': lld_dict_slice['VAD'].reshape(1, -1)})

        # TODO: why no need for sync again after derivatives

        # slice lld
        if segments_list is not None:
            start_and_end = list(zip(*segments_list[index]))
            lld_dict_slice = slice2segments(lld_dict_slice, list(start_and_end[0]), list(start_and_end[1]))

        # save sliced
        with open(lld_filename, 'wb') as handle:
            pickle.dump(lld_dict_slice, handle)
        
        all_lld_files.append(lld_filename)

    log_file.close()
    return all_lld_files


def compute_full_lld(wav_name, convert2float32=True):
    if not os.path.isfile(wav_name):
        print('ERROR: Cannot read {}, skipping file.'.format(wav_name))
        return None, None
    try:
        signal, sr = lb.load(wav_name, sr=None)
        if sr is None:
            print('Problem analyzing {} (sr=None, signal length={})'.format(wav_name, len(signal)))
            return None, None

        processor = PATCalculator()
        lld_dict, residual = processor.process_chunk(signal)

        # TODO: no need to do here, also no need for t_sec
        lld_dict, t_sec = PATCalculator.sync_lld(signal, lld_dict)

    except:
        print('Problem analyzing {}'.format(wav_name))
        return None, None

    if convert2float32:
        lld_dict32 = {}
        for key in lld_dict.keys():
            lld_dict32.update({key: lld_dict[key].astype(np.float32)})
        return lld_dict32, t_sec
    else:
        return lld_dict, t_sec


def slice2segments(lld_dict, segments_start, segments_end):
    feature_rate = 1 / PATCfg.FRAME_STEP_FEATURES_SEC
    seg_start = (np.array(segments_start) * feature_rate)
    seg_end = (np.array(segments_end) * feature_rate)

    # frame_count = len(next(iter(lld_dict.values()))[0, :])  # has bug: vad has only 1 dim, so will fail if picked it (random)
    frame_count = len(lld_dict['VAD'])

    # Correct nan entries, shouldn't happen for Maccabi
    if np.all(np.isnan(seg_start)):
        seg_start = np.float64(0)
    if np.all(np.isnan(seg_end)):
        seg_end = np.float64(frame_count)

    if type(seg_start) is np.float64:
        lld_dict_sliced = {}
        for key in lld_dict.keys():
            if key == 'VAD':
                lld_dict_sliced.update({key: lld_dict[key][int(seg_start):int(seg_end)]})
            else:
                lld_dict_sliced.update({key: lld_dict[key][:,int(seg_start):int(seg_end)]})
        return lld_dict_sliced

    # do the slicing
    filt = np.zeros(frame_count, dtype=np.bool)
    for i in range(seg_start.shape[0]):
        filt[int(seg_start[i]):int(seg_end[i])] = 1

    lld_dict_sliced = {}
    for key in lld_dict.keys():
        if key == 'VAD':
            lld_dict_sliced.update({key: lld_dict[key][filt]})
        else:
            lld_dict_sliced.update({key: lld_dict[key][:,filt]})
    return lld_dict_sliced


def extract_lld(wav_list, feature_path=None, samples_path='', recall=False):
    counter = 0

    for curr_wav in wav_list:
        lld_filename = os.path.join(feature_path, str(os.path.splitext(curr_wav.split(os.sep)[-1])[0]) + '.pkl')

        input_file = os.path.join(samples_path, curr_wav)

        if recall and os.path.isfile(lld_filename):
            counter = utils.print_elapsed('files analyzed', counter, len(wav_list), step=2)
            continue

        lld_dict, t_sec = compute_full_lld(input_file, convert2float32=True)
        if lld_dict is None:
            counter = utils.print_elapsed('files analyzed', counter, len(wav_list), step=2)
            continue

        with open(lld_filename, 'wb') as handle:
            pickle.dump(lld_dict, handle)

        counter = utils.print_elapsed('files analyzed', counter, len(wav_list), step=2)
        # if counter==3:
        #    break


def main(output_folder=[], wav_list=[], n_jobs=1, recall=False):

    start_time = time.time()

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    if n_jobs != 1:
        if n_jobs < 0:
            n_processes = multiprocessing.cpu_count() + n_jobs
        else:
            n_processes = n_jobs

        random.shuffle(wav_list)
        division = len(wav_list) / float(n_processes)
        wav_list_partials = [wav_list[int(round(division * i)): int(round(division * (i + 1)))] for i in
                             range(n_processes)]

        pool = Pool(processes=n_processes)
        func = partial(extract_lld, feature_path=output_folder, recall=recall)
        pool.map(func, wav_list_partials)
        pool.close()
        pool.join()
    else:
        extract_lld(wav_list, feature_path=output_folder, recall=recall)

    print('Done in {} minutes.'.format(np.round((time.time() - start_time) / float(60), decimals=2)))


if __name__ == '__main__':
    output_folder = 'E:/Algo/dat/PAT/'

    # files_df = pd.read_csv('E:/Algo/dat/MetaData/RecordingsData181003.csv', index_col=None)
    files_df = pd.read_csv('E:/Algo/dat/MetaData/DB4Recordings181230.csv', index_col=None)
    wav_list_orig = (files_df.filename.values).astype(str)
    wav_list = []
    patient_id = files_df.patient_id.values  # works only if no two patients share a file!!

    for i, w in enumerate(wav_list_orig):
        wav_list.append(os.path.join('E:/recordings/by_p/', str(patient_id[i]), str(w) + '.wav'))

    n_jobs = 3

    if False:
        wav_list = ['E:/Algo/Nimrod/Debug/test.wav']
        n_jobs = 1

    print('n_jobs = {}'.format(n_jobs))
    main(output_folder=output_folder, wav_list=wav_list, recall=True, n_jobs=n_jobs)



