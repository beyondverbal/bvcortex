import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
from sklearn.feature_selection import mutual_info_classif
from scipy import stats
from sklearn.metrics import roc_auc_score
from sklearn.preprocessing import StandardScaler


DATE_SIGN = '180510'
# DATA_SET = 'CHINA_CAD'; FEATURE_SET = '500PDcM4' #  '500PV4' #
DATA_SET = 'TEMPER_P' # 'TEMPER_M' # 'MACCABI' #
# DATA_SET = 'MAYO' # FEATURE_SET = 'article' # 'mix_all' #


def AddComboFeatures(xny_df):
    Top10CC4M = xny_df[['dmfcc1_decileM1', 'dmfcc0_decileM2', 'dmfcc0_decileM4', 'dmfcc6_decileM4', 'dmfcc2_decileM3', 'lspFreq[0]_decileM2', 'mfcc10_decileM2', 'lspFreq[0]_decileM4', 'mfcc6_decileM4', 'mfcc9_decileM2']].as_matrix()
    W10CC4M = [-1, 1, -1, -1, 1, 1, 1, -1, -1, 1]
    Top10AUC4M = xny_df[['dmfcc0_decileM2', 'dmfcc0_decileM4', 'pcm_zcr_decileM4', 'dmfcc6_decileM4', 'dmfcc9_decileM4', 'dmfcc1_decileM1', 'mfcc11_decileM1', 'lspFreq[0]_decileM2', 'mfcc10_decileM2', 'dmfcc2_decileM3']].as_matrix()
    W10AUC4M = [1, -1, -1, -1, -1, -1, 1, 1, 1, 1]
    Top10AUC4F = xny_df[['dmfcc2_decileM2', 'mfcc12_decileM1', 'lspFreq[0]_decileM1', 'dmfcc2_decileM4', 'pcm_zcr_decileM1', 'dmfcc4_decileM4', 'mfcc2_decileM1', 'formantFreqLpc[1]_decileM1', 'PlpCC[1]_decileM1', 'dmfcc12_decileM3']].as_matrix()
    W10AUC4F = [1, -1, -1, -1, -1, -1, 1, 1, 1, 1]
    Top10CC4F = xny_df[['dmfcc2_decileM2', 'dmfcc2_decileM4', 'dloudness_decileM3', 'mfcc12_decileM1', 'PlpCC[3]_decileM2', 'lspFreq[0]_decileM1', 'dmfcc4_decileM2', 'mfcc5_decileM3', 'dmfcc4_decileM4', 'formantFreqLpc[5]_decileM3']].as_matrix()
    W10CC4F = [1, -1, 1, -1, 1, -1, 1, -1, -1, -1]
    Top20AUC4M = xny_df[['dmfcc0_decileM2', 'dmfcc0_decileM4', 'pcm_zcr_decileM4', 'dmfcc6_decileM4', 'dmfcc9_decileM4', 'dmfcc1_decileM1', 'mfcc11_decileM1', 'lspFreq[0]_decileM2', 'mfcc10_decileM2', 'dmfcc2_decileM3', 'dmfcc10_decileM4', 'mfcc9_decileM2', 'dmfcc2_decileM1', 'mfcc4_decileM4', 'dLogPitch_decileM4', 'lspFreq[0]_decileM4', 'dLogPitch_decileM2', 'PlpCC[0]_decileM4', 'dmfcc1_decileM4', 'dmfcc9_decileM2']].as_matrix()
    W20AUC4M = [0.116497189, -0.11479189, -0.114191878, -0.104117981, -0.099128403, -0.099065243, 0.099002084, 0.098875766, 0.096602034, 0.095844123, -0.09199141, 0.09199141, -0.091612455, -0.091486137, -0.089086086, -0.088959768, 0.087696583, -0.086496558, -0.085233373, 0.0847281]
    Top20AUC4F = xny_df[['dmfcc2_decileM2', 'mfcc12_decileM1', 'lspFreq[0]_decileM1', 'dmfcc2_decileM4', 'pcm_zcr_decileM1', 'dmfcc4_decileM4', 'formantFreqLpc[1]_decileM1', 'mfcc2_decileM1', 'PlpCC[1]_decileM1', 'dmfcc12_decileM3', 'dmfcc6_decileM2', 'dACpitch_decileM3', 'dmfcc4_decileM2', 'PlpCC[3]_decileM2', 'dmfcc5_decileM2', 'dmfcc8_decileM4', 'dloudness_decileM1', 'lpcCoeff[7]_decileM1', 'lpcCoeff[1]_decileM2', 'lspFreq[1]_decileM1']].as_matrix()
    W20AUC4F = [0.044495674, 0.015758226, -0.036474452, -0.037863955, -0.05292743, -0.08466494, -0.008494916, 0.007610687, -0.018537232, -0.034958631, 0.075317375, 0.04563254, 0.073991031, 0.041906145, 0.038685025, -0.069001453, 0.04316933, 0.041906145, -0.040769279, -0.035527064]

    scaler = StandardScaler()
    cbCADByCC4M=np.dot(scaler.fit_transform(Top10CC4M), W10CC4M)
    cbCADByCC4F=np.dot(scaler.fit_transform(Top10CC4F), W10CC4F)
    cbCADByAUC4M=np.dot(scaler.fit_transform(Top20AUC4M), W20AUC4M)
    cbCADByAUC4F=np.dot(scaler.fit_transform(Top20AUC4F), W20AUC4F)

    xny_df = xny_df.assign(ComboCADByCC4M=cbCADByCC4M)
    xny_df = xny_df.assign(ComboCADByCC4F=cbCADByCC4F)
    xny_df = xny_df.assign(ComboCADByCC4X=cbCADByCC4M+cbCADByCC4F)
    xny_df = xny_df.assign(ComboCADByAUC4M=cbCADByAUC4M)
    xny_df = xny_df.assign(ComboCADByAUC4F=cbCADByAUC4F)
    xny_df = xny_df.assign(ComboCADByAUC4X=cbCADByAUC4M+cbCADByAUC4F)
    xny_df = xny_df.assign(ComboCAD=cbCADByCC4M+cbCADByCC4F+cbCADByAUC4M+cbCADByAUC4F)

    return xny_df


def load_xy():
    if DATA_SET == 'MAYO':
        if FEATURE_SET == 'article':
            out_filename = 'C:/BVC/HealthAnalysis/Mayo/Features/article_features_ranking_on_p1r3TTT.csv'
            xny_filename = 'C:/BVC/HealthAnalysis/Mayo/Features/mayoclinic_20170301FXn+metadata.csv'
            xny_df = pd.DataFrame.from_csv(xny_filename)
            first_x_col = 7
        elif FEATURE_SET == 'mix_all':
            out_filename = 'C:/BVC/HealthAnalysis/Mayo/Features/ex_vad_features_rank_by_p1r3.csv'
            samples_list = 'C:/BVC/HealthAnalysis/Mayo/MetaData/mayoclinic_files+metadata.csv'
            HLD_file = 'C:/BVC/HealthAnalysis/Mayo/Features/HLDex_vad.csv'
            hld_df = pd.DataFrame.from_csv(HLD_file, index_col=None)
            md_df = pd.DataFrame.from_csv(samples_list, index_col=None)
            hld_df = hld_df.rename(columns={'filename': 'FileName'})
            hld_df.FileName = hld_df.FileName + '.wav'
            xny_df = pd.merge(left=md_df, right=hld_df, on='FileName')
            first_x_col = 8
        # filter to interesting records
        xny_df = xny_df.loc[xny_df['RecTag'] == 'p1r3']
        # xny_df = xny_df.loc[(xny_df['RecTag'] == 'p1r3') | (xny_df['RecTag'] == 'p1r2') | (xny_df['RecTag'] == 'p1r1')]
        xny_df = xny_df.loc[xny_df['Indication'] != 'Transplant']
        xny_df = xny_df.dropna(axis = 0, subset = ['Disease'])

        Y = (xny_df['Disease'].values != 'Normal') & (xny_df['Disease'].values != '0')
        # Y = (xny_df['Disease'].values == 'Severe') | (xny_df['Disease'].values == 'Moderate')
    elif DATA_SET == 'MACCABI':
        HLD_file = 'E:/Algo/dat/HLD/HLD180205.csv'
        MD0_file = 'E:/Algo/Yotam/SQL/CompoundDiagnoses.csv'
        md0 = pd.DataFrame.from_csv(MD0_file, index_col=None)
        md_table = MetadataTable()
        md1 = md_table.read()
        md1 = pd.DataFrame(data=md1, columns=md_table.column_names)
        #md1[['patient_id']] = md1[['patient_id']].apply(pd.to_numeric)
        md0.patient_id = md0.patient_id.astype(str)
        md_df = pd.merge(left=md1, right=md0, on='patient_id')

        hld_df = pd.DataFrame.from_csv(HLD_file, index_col=None)
        hld_df.filename = hld_df.filename.astype(str)
        xny_df = pd.merge(left=md_df, right=hld_df, on='filename')

        xny_df = xny_df.loc[((xny_df.CHF != 0) & ((xny_df.COPD != 0) | (xny_df.Diabetes != 0))) == 0]
        xny_df = xny_df.loc[((xny_df.CHF == 0) & (xny_df.NotNoCHF != 0)) == 0]
        Y = xny_df.CHF.values != 0
        out_filename = 'E:/Algo/Analysis/features_ranking_on_CHF180205.csv'

        # xny_df = xny_df.loc[((xny_df.COPD != 0) & ((xny_df.CHF != 0) | (xny_df.Diabetes != 0))) == 0]
        # xny_df = xny_df.loc[((xny_df.COPD == 0) & (xny_df.NotNoCOPD != 0)) == 0]
        # Y = xny_df.COPD.values != 0
        # out_filename = 'E:/Team/Yotam/Analysis/all_features_ranking_on_COPD.csv'

        # xny_df = xny_df.loc[((xny_df.Diabetes != 0) & ((xny_df.CHF != 0) | (xny_df.COPD != 0))) == 0]
        # xny_df = xny_df.loc[((xny_df.Diabetes == 0) & (xny_df.NotNoDiabetes != 0)) == 0]
        # Y = xny_df.Diabetes.values != 0
        # out_filename = 'E:/Team/Yotam/Analysis/all_features_ranking_on_Diabetes.csv'

        first_x_col = 10
    elif DATA_SET == 'TEMPER_M':
        out_filename = 'C:/BVC/FeatureMatrices/MatlabTemper_features_ranking180508.csv'
        HLD_file = 'C:/BVC/FeatureMatrices/MatlabTemperMatrix.csv'

        xny_df = pd.DataFrame.from_csv(HLD_file, index_col=None)
        first_x_col = 20
        Y = xny_df['Label'].values
    elif DATA_SET == 'TEMPER_P':
        out_filename = 'C:/BVC/FeatureMatrices/PythonTemper_features_ranking180508B.csv'
        HLD_file = 'C:/BVC/FeatureMatrices/feature_matrix_MIX4.3.1.8.csv'
        MD0_file = 'C:/BVC/PythonAnalyzer/ProduceDatasets/temper_train_set_MIX4.3.1.8.csv'

        hld_df = pd.DataFrame.from_csv(HLD_file, index_col=None)
        md_df = pd.DataFrame.from_csv(MD0_file, index_col=None)
        xny_df = pd.merge(left=md_df, right=hld_df, on='wavemd5')

        first_x_col = 25
        Ytxt = xny_df['AgreedLabel'].values
        Y = 1 * (Ytxt != 'low')
        # Y[np.where(Ytxt == 'high')] = 2
    elif DATA_SET == 'CHINA_CAD':
        # MD0_file = 'C:/BVC/HealthAnalysis/China_CAD/MetaData/Metadata_1-100_301-400.xlsx'
        MD0_file = 'C:/BVC/HealthAnalysis/China_CAD/MetaData/Metadata_1-208_301-605.xlsx'
        if FEATURE_SET == '500PV4':
            HLD_file = 'C:/BVC/HealthAnalysis/China_CAD/Features/ChinaHLD500V4.csv'
            # HLD_file = 'C:/BVC/HealthAnalysis/China_CAD/Features/HLD_1-100_301-400ex.csv'
        else:
            HLD_file = 'C:/BVC/HealthAnalysis/China_CAD/Features/ChinaHLD500.csv'

        # out_filename = 'C:/BVC/HealthAnalysis/China_CAD/Features/ChinaCAD_' + FEATURE_SET + 'scores' + DATE_SIGN + 'R123gt20.csv'
        out_filename = 'C:/BVC/HealthAnalysis/China_CAD/Features/ChinaCAD_' + FEATURE_SET + 'nCOMBOscores' + DATE_SIGN + 'R2gt20.csv'

        hld_df = pd.DataFrame.from_csv(HLD_file, index_col=None)

        xls = pd.ExcelFile(MD0_file)
        md_df = xls.parse('MD0')

        hld_df.user = ''
        hld_df.context = ''
        for index in hld_df.index:
            hld_df.set_value(index, 'user', int(hld_df.loc[index, 'filename'][4:7]))
            hld_df.set_value(index, 'context', hld_df.loc[index, 'filename'][-1:])

        # hld_df = hld_df[(hld_df['context'] == '1') | (hld_df['context'] == '2') | (hld_df['context'] == '3')]
        hld_df = hld_df[(hld_df['context'] == '2')]
        hld_df = hld_df.drop('context', 1)

        xny_df = pd.merge(left=md_df, right=hld_df, on='user')
        gender = xny_df.gender == 'male'
        age = xny_df.age
        xny_df = xny_df.assign(ageFX = age)
        xny_df = xny_df.assign(genderFX = gender)

        xny_df = AddComboFeatures(xny_df)

        first_x_col = 9
        Y = (xny_df['coronary_score'].values > 20)

    # random.shuffle(Y)
    X = xny_df.iloc[:, first_x_col:].as_matrix()
    feature_names = xny_df.columns[first_x_col:]

    return X, Y, gender, age, feature_names, out_filename

if __name__ == '__main__':
    X, Y, gender, age, feature_names, out_filename = load_xy()

    Xm = X[gender]
    Ym = Y[gender]
    Xf = X[~gender]
    Yf = Y[~gender]
    ageM = age[gender]
    ageF = age[~gender]

    r = [stats.pearsonr(X[:, i], Y) for i in range(X.shape[1])]
    rx = [abs(i[0]) for i in r]
    px = [i[1] for i in r]
    r = [stats.pearsonr(Xm[:, i], Ym) for i in range(Xm.shape[1])]
    rm = [abs(i[0]) for i in r]
    pm = [i[1] for i in r]
    r = [stats.pearsonr(Xf[:, i], Yf) for i in range(Xf.shape[1])]
    rf = [abs(i[0]) for i in r]
    pf = [i[1] for i in r]
    # pf = [np.sign(i[0]) for i in r]

    r = [stats.pearsonr(X[:, i], gender) for i in range(Xm.shape[1])]
    rx2g = [abs(i[0]) for i in r]
    # px2g = [i[1] for i in r]
    r = [stats.pearsonr(Xm[:, i], ageM) for i in range(Xm.shape[1])]
    rm2a = [abs(i[0]) for i in r]
    # pm2a = [i[1] for i in r]
    r = [stats.pearsonr(Xf[:, i], ageF) for i in range(Xf.shape[1])]
    rf2a = [abs(i[0]) for i in r]
    # pf2a = [i[1] for i in r]

    # mi = mutual_info_classif(X, Y, discrete_features=False, n_neighbors=2)
    # r = [stats.spearmanr(Xm[:, i], Ym) for i in range(X.shape[1])]

    if len(set(Y)) > 2:
        aucX = rx
        aucM = rm
        aucF = rf
    else:
        aucX = [roc_auc_score(Y,  X[:, i],  average='macro') for i in range(X.shape[1])]
        aucM = [roc_auc_score(Ym, Xm[:, i], average='macro') for i in range(Xm.shape[1])]
        aucF = [roc_auc_score(Yf, Xf[:, i], average='macro') for i in range(Xf.shape[1])]
    roc_aucX = [max(1-x, x) for x in aucX]
    roc_aucM = [max(1-x, x) for x in aucM]
    roc_aucF = [max(1-x, x) for x in aucF]
    weight4M = [x-0.5 for x in aucM]
    weight4F = [x-0.5 for x in aucF]

    pd.DataFrame(np.hstack((np.expand_dims(feature_names, axis=1),
                            np.expand_dims(roc_aucX, axis=1),
                            np.expand_dims(roc_aucM, axis=1),
                            np.expand_dims(roc_aucF, axis=1),
                            np.expand_dims(weight4M, axis=1),
                            np.expand_dims(weight4F, axis=1),
                            np.expand_dims(rx, axis=1),
                            np.expand_dims(rm, axis=1),
                            np.expand_dims(rf, axis=1),
                            np.expand_dims(rx2g, axis=1),
                            np.expand_dims(rm2a, axis=1),
                            np.expand_dims(rf2a, axis=1),
                            np.expand_dims(px, axis=1),
                            np.expand_dims(pm, axis=1),
                            np.expand_dims(pf, axis=1))),
                 columns = ['FeatureID', 'AUC4X', 'AUC4M', 'AUC4F', 'weight4M', 'weight4F', 'CC2Tgt4X', 'CC2Tgt4M', 'CC2Tgt4F', 'CC2gender4X', 'CC2age4M', 'CC2age4F', 'p(CC2Tgt4X)', 'p(CC2Tgt4M)', 'p(CC2Tgt4F)']).to_csv(out_filename)

    # featureX = X[:,np.where(feature_names == 'lspFreq[6]_range')[0][0]]
    # featureX0 = featureX[Y == 0]
    # featureX1 = featureX[Y == 1]
    # plt.hist([featureX0, featureX1])
    # plt.show()

    n = Y.shape
    print(n)
