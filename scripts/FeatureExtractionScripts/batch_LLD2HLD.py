import os
import pandas as pd
import numpy as np
import csv
import sys
import pickle
sys.path.append("../.")
sys.path.append("../..")
# from scripts.FeatureExtractionScripts import utils
from FeatureExtractionScripts import utils
import PATrends.PATConfig as config
from TAVEngine.HLDCalculator import HLDCalculator
from TAVEngine import HLDConfig as HLDCfg


DATA_SET = 'MACCABI' # 'CHINA_CAD' # 'CELEBS' # 'MAYO' #


def lld2hld_by_list(lld_files, seg_size=20):

    hld_calc = HLDCalculator()

    counter = 0
    hld_dict = dict()
    for curr_lld in lld_files:
        if not curr_lld.endswith('.pkl'):
            continue
        counter = utils.print_elapsed('hld extracted', counter, len(lld_files), step=100)
        try:
            with open(curr_lld, 'rb') as handle:
                lld = pickle.load(handle)
        except:
            print(curr_lld+' error')
            continue

        new_trunc = hld_calc.adjust_trunc(seg_length=seg_size)
        hld = hld_calc.HLD_Features.compute_features(lld, HLDCfg.OPERATOR_LIST, new_trunc)

        hld_dict[os.path.splitext(curr_lld.split(os.sep)[-1])[0]] = hld

    feature_name = hld_calc.get_hld_features_titles()
    df_hld = pd.DataFrame.from_dict(hld_dict, orient='index')
    df_hld.columns = feature_name

    return df_hld


def lld2hld_by_list_multi_segments(lld_files, writer, seg_size=20):

    frames_per_sec = int(config.SAMPLING_RATE / config.FRAME_STEP_FEATURES)
    seg_unit = frames_per_sec * seg_size

    hld_calc = HLDCalculator()

    counter = 0
    produce_titles = True

    for curr_lld in lld_files:
        if not curr_lld.endswith('.pkl'):
            continue
        counter = utils.print_elapsed('hld extracted', counter, len(lld_files), step=100)
        try:
            with open(curr_lld, 'rb') as handle:
                lld = pickle.load(handle)
        except:
            print(curr_lld+' error')
            continue


        vad_s = lld['VAD']

        filename = os.path.splitext(curr_lld.split(os.sep)[-1])[0]
        start = 0
        end = seg_unit
        seg_num = 1
        while end <= len(vad_s):

            new_trunc = hld_calc.adjust_trunc(seg_length=seg_size)
            hld = hld_calc.HLD_Features.compute_features(lld, HLDCfg.OPERATOR_LIST, [x+start for x in new_trunc])

            if produce_titles:
                feature_name = hld_calc.get_hld_features_titles()

                feature_name = np.append(['filename', 'segment', 'start', 'end'], feature_name)
                writer.writerow(feature_name)
                produce_titles = False

            writer.writerow([filename, seg_num, start/frames_per_sec, end/frames_per_sec] + list(hld))

            start += seg_unit
            end += seg_unit
            seg_num += 1

def reduce_segment_num(hld_df, seg2keep):
    segments = np.arange(1,seg2keep+1)
    hld_df = hld_df[hld_df.segment.isin(segments)]
    return hld_df


if __name__ == '__main__':
    CALC_SEGMENTS = True
    REDUCE_SEGMENT_NUM = False

    print('CALC_SEGMENTS =',CALC_SEGMENTS)
    print('REDUCE_SEGMENT_NUM =', REDUCE_SEGMENT_NUM)

    if DATA_SET == 'MAYO':
        LLD_folder = 'C:/BVC/HealthAnalysis/Mayo/LLDcsv'
        HLD_file = 'C:/BVC/HealthAnalysis/Mayo/Features/MayoHLD171228.csv'
    elif DATA_SET == 'MACCABI':
        LLD_folder = 'E:/Algo/dat/PATsliced'
        HLD_file = 'E:/Algo/dat/HLD/HLD190128_PAT_M5_10sec.csv'
    elif DATA_SET == 'CHINA_CAD':
        LLD_folder = 'C:/BVC/HealthAnalysis/China_CAD/LLD'
        HLD_file = 'C:/BVC/HealthAnalysis/China_CAD/Features/ChinaHLD500V4.csv'
    elif DATA_SET == 'CELEBS':
        LLD_folder = 'C:/BVC/HealthAnalysis/CelebsHeartIssues/LLD'
        HLD_file = 'C:/BVC/HealthAnalysis/CelebsHeartIssues/Features/CelebsHLD.csv'

    lld_files = os.listdir(LLD_folder)
    if len(lld_files):
        lld_files = [os.path.join(LLD_folder, i) for i in lld_files]

        if CALC_SEGMENTS:
            with open(HLD_file, 'w', newline='') as file:
                writer = csv.writer(file, delimiter=',')
                lld2hld_by_list_multi_segments(lld_files, writer, seg_size=10)

            if REDUCE_SEGMENT_NUM:
                hld_df = pd.read_csv(HLD_file, index_col=None)
                hld_df = reduce_segment_num(hld_df, 2)
                hld_df.to_csv(HLD_file, index=False)

        else:
            df_hld = lld2hld_by_list(lld_files)
            df_hld.to_csv(HLD_file, index_label='filename')

