import pandas as pd
from shutil import copyfile

samples_list = 'C:/BVC/HealthAnalysis/Mayo/MetaData/MayoMD181205.csv'
samples_folder = 'C:/BVC/HealthAnalysis/Mayo/fullWaves'

samples_df = pd.DataFrame.from_csv(samples_list, index_col=None)
samples_df = samples_df.dropna(how='all')

samples_df = samples_df[samples_df['is_completed']==1]

suffix = ['Neu_Q', 'Pos_Q', 'Neg_Q']

count = 0
for suf in suffix:
    suf_df = samples_df[samples_df.label == suf].copy()
    suf_df.drop_duplicates(subset='path', inplace=True)

    wav_list = (suf_df['path'].apply(lambda x: x.replace('\\BVC\\WavesRepository\\mayo\\fullWaves', samples_folder))).values
    modified_wav_names = [path[:-4]+'_'+suf+'.wav' for path in wav_list]

    for idx in range(len(wav_list)):
        copyfile(wav_list[idx], modified_wav_names[idx])
        count += 1

print('Copied {} files'.format(count))