import pandas as pd

output_df = pd.DataFrame.from_csv('C:/BVC/HealthAnalysis/Mayo/output/mayo_scores.csv', index_col=None)[['filename'] + ['DistNormed']]

output_df.rename(index=str, columns={'DistNormed': 'biomarker'}, inplace=True)

labels=[]
new_filenames = []

for file in output_df.filename.values:
    if file.endswith('_Neu_Q'):
        labels.append('Neu_Q')
        new_filenames.append(file[:-6])
    elif file.endswith('_Pos_Q'):
        labels.append('Pos_Q')
        new_filenames.append(file[:-6])
    elif file.endswith('_Neg_Q'):
        labels.append('Neg_Q')
        new_filenames.append(file[:-6])
    else:
        labels.append('')
        new_filenames.append(file)

output_df = output_df.assign(label=labels)
output_df = output_df.assign(filename=new_filenames)


output_df.to_csv('C:/BVC/HealthAnalysis/Mayo/output/mayo_score_clean.csv', index=False)