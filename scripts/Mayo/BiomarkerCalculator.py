import pandas as pd

from CommonML.LinearSigmoidModel import LinSig

# Need this to bypass original __init__
class LinSig_Calc(LinSig):
    def __init__(self):
        pass

# MODEL_PATH = 'E:/Algo/Nimrod/M8_181004pPaper2.3_model4mayo/'
# HLD_FILE = 'E:/Algo/dat/MayoHLD/MayoHLD181205.csv'
MODEL_PATH = 'C:/BVC/HealthAnalysis/Mayo/model_vtl23/'
HLD_FILE = 'C:/BVC/HealthAnalysis/Mayo/Features/MayoHLD181205.csv'

OUTPUT_PATH = 'C:/BVC/HealthAnalysis/Mayo/output/'
OUTPUT_FILE = 'mayo_scores.csv'


LSM = LinSig_Calc()

Lin, feature_names, Sig = LSM.load_model(MODEL_PATH)

hld_df = pd.DataFrame.from_csv(HLD_FILE, index_col=None)
hld_df.dropna(inplace=True)

X = hld_df[feature_names].as_matrix()
file_list = hld_df.filename.values

predictions, score, DistNormed, ScoreNormed, highQ, rescaled2skew = LSM.predict(X, None)

output = pd.DataFrame(data={'filename': file_list,
                            'predictions': predictions,
                            'score': score,
                            'DistNormed': DistNormed.flatten(),
                            'ScoreNormed': ScoreNormed.flatten(),
                            'highQ': highQ,
                            'rescaled2skew': rescaled2skew})

output = output[['filename','predictions','score','DistNormed','ScoreNormed','highQ','rescaled2skew']]

output.to_csv(OUTPUT_PATH + OUTPUT_FILE, index=False)