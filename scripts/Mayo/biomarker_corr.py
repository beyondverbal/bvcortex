import pandas as pd
import numpy as np
from scipy.stats.stats import pearsonr

FILES = 'C:/BVC/HealthAnalysis/Mayo/MetaData/mayo_files_test.csv'
LABELS = 'C:/BVC/HealthAnalysis/Mayo/MetaData/mayo_cad_test.csv'
BIOMARKER = 'C:/BVC/HealthAnalysis/Mayo/output/mayo_score_clean.csv'

files_df = pd.DataFrame.from_csv(FILES, index_col=None)
lables_df = pd.DataFrame.from_csv(LABELS, index_col=None)
biomarker_df = pd.DataFrame.from_csv(BIOMARKER, index_col=None)

files_df.drop_duplicates(subset='filename', inplace=True)

data_df = pd.merge(left=files_df, right=lables_df, on='Patient')
data_df = pd.merge(left=data_df, right=biomarker_df, on='filename')

r_dict = {'Neu_Q': '1', 'Pos_Q': '2', 'Neg_Q': '3'}

for key in r_dict.keys():
    idx = data_df[data_df.label == key].index
    data_df.at[idx,'r'] = r_dict[key]

# all
cad = data_df.MaxPCNT.values
biomarker = data_df.biomarker.values
pearson_r = pearsonr(cad,biomarker)
print('all:',pearson_r)

# r1
cad = data_df[data_df.r==1].MaxPCNT.values
biomarker = data_df[data_df.r==1].biomarker.values
pearson_r = pearsonr(cad,biomarker)
print('r1:',pearson_r)

# r2
cad = data_df[data_df.r==2].MaxPCNT.values
biomarker = data_df[data_df.r==2].biomarker.values
pearson_r = pearsonr(cad,biomarker)
print('r2:',pearson_r)

# r3
cad = data_df[data_df.r==3].MaxPCNT.values
biomarker = data_df[data_df.r==3].biomarker.values
pearson_r = pearsonr(cad,biomarker)
print('r3:',pearson_r)

# p1
cad = data_df[data_df.p==1].MaxPCNT.values
biomarker = data_df[data_df.p==1].biomarker.values
pearson_r = pearsonr(cad,biomarker)
print('p1:',pearson_r)

# p2,p3,p4,p5
cad = data_df[data_df.p>1].MaxPCNT.values
biomarker = data_df[data_df.p>1].biomarker.values
pearson_r = pearsonr(cad,biomarker)
print('p>1:',pearson_r)

# p1 and r1
cad = data_df[(data_df.r==1) & (data_df.p==1)].MaxPCNT.values
biomarker = data_df[(data_df.r==1) & (data_df.p==1)].biomarker.values
pearson_r = pearsonr(cad,biomarker)
print('p1 and r1:',pearson_r)

# p1 and r2
cad = data_df[(data_df.r==2) & (data_df.p==1)].MaxPCNT.values
biomarker = data_df[(data_df.r==2) & (data_df.p==1)].biomarker.values
pearson_r = pearsonr(cad,biomarker)
print('p1 and r2:',pearson_r)

# p1 and r3
cad = data_df[(data_df.r==3) & (data_df.p==1)].MaxPCNT.values
biomarker = data_df[(data_df.r==3) & (data_df.p==1)].biomarker.values
pearson_r = pearsonr(cad,biomarker)
print('p1 and r3:',pearson_r)

# p>1 and r1
cad = data_df[(data_df.r==1) & (data_df.p>1)].MaxPCNT.values
biomarker = data_df[(data_df.r==1) & (data_df.p>1)].biomarker.values
pearson_r = pearsonr(cad,biomarker)
print('p>1 and r1:',pearson_r)

# p>1 and r2
cad = data_df[(data_df.r==2) & (data_df.p>1)].MaxPCNT.values
biomarker = data_df[(data_df.r==2) & (data_df.p>1)].biomarker.values
pearson_r = pearsonr(cad,biomarker)
print('p>1 and r2:',pearson_r)

# p>1 and r3
cad = data_df[(data_df.r==3) & (data_df.p>1)].MaxPCNT.values
biomarker = data_df[(data_df.r==3) & (data_df.p>1)].biomarker.values
pearson_r = pearsonr(cad,biomarker)
print('p>1 and r3:',pearson_r)


