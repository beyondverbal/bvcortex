# define number of workers by CPU count, multiply by 2 if CPUs are not fully used
WORKERS_CPU_MULTIPLICATOR = 1
WORKERS_SERVER = 'localhost'

# connection ports
CONTROL_PORT = 5554
CLIENTS_PORT = 5555

# buffer settings
BUFFER_LEN = 16000
BUFFER_FRAME = 2
