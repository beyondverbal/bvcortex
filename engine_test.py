import json
import time
import zmq
import logging

import audioread
import os
from datetime import datetime

import engine_settings as settings
import time


if not os.path.exists('logs'):
    os.makedirs('logs')
logging.basicConfig(filename='logs/tester.log', level=logging.DEBUG)

start = time.time()
TEST_CLIENTS = 1
TEST_TIME_SECONDS = 60000

context = zmq.Context()

keepalive = context.socket(zmq.REQ)
keepalive.connect("tcp://localhost:%d" % settings.CONTROL_PORT)
keepalive.send_json({'cmd': 'keepalive'})
print(keepalive.recv_json())

sockets = []

for i in range(TEST_CLIENTS):
    keepalive.send_json({'cmd': 'newsession', 'id': 100 + i})
    worker = keepalive.recv_json()

    s = context.socket(zmq.DEALER)
    s.setsockopt(zmq.IDENTITY, b'client%d' % i)
    s.connect("tcp://%s:%d" % (worker['server'], worker['port']))

    sockets.append(s)

poller = zmq.Poller()
for s in sockets:
    poller.register(s, zmq.POLLIN)

folder_path = 'C:/BVC/test/'
file_name = 'rosie8k_short.wav'


def read_audio_frames():
    with audioread.audio_open(os.path.realpath(folder_path + file_name)) as input_file:
        for frame in input_file:
            yield frame


audio_frames = read_audio_frames()

i = 16
running = True
while running:
    # is there anything to send?
    try:
        frame = audio_frames.__next__()
        for s in sockets:
                s.send_multipart([bytes('data', "utf-8"), frame])
    except:
        print('End of file, send end session')
        for s in sockets:
            s.send_multipart([bytes('endsession', "utf-8"), bytes([])])
        
        for s in sockets:
            result = s.recv_json()
            while isinstance(result, list): 
                print('\nsocket%d more' % n, result)
                result = s.recv_json()
            print('\nsocket%d summery' % n, result)
            # exit on end last client
            TEST_CLIENTS-=1
            if TEST_CLIENTS==0:
                end = time.time()
                print("Cliens finished in {0}".format(end-start))
                #keepalive.send_json({'cmd': 'stop'})
        #socks = dict(poller.poll(timeout=.01))
        #for n, s in enumerate(sockets):
        #    if socks.get(s) == zmq.POLLIN:
        #        result = s.recv_json()
        #        time.sleep(.01)
        #        if result['start'] is None: 
        #            print('Summery resived')
        #            print('socket%d summery' % n, result)
        #        else:
        #            print('More resived')
        #            print('socket%d more=>' % n, result)
        #            print('socket%d summery' % n, s.recv_json())
        #keepalive.send_json({'cmd': 'stop'})
        #print(keepalive.recv_json())
        running = False
        continue

    socks = dict(poller.poll(timeout=.01))
    for n, s in enumerate(sockets):
        if socks.get(s) == zmq.POLLIN:
            print('socket%d' % n, s.recv_json())

    # idle
    time.sleep(.01)

    i += 1
    if i % 100 == 0:
        keepalive.send_json({'cmd': 'cpustats'})
        print(keepalive.recv_json())
