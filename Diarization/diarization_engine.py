import librosa
from bob.bio.spear.preprocessor import Energy_2Gauss
import scipy.io.wavfile as wav
import os
import shutil
import glob
import random
import numpy as np
import copy
import scipy
from shutil import copyfile
from distutils.dir_util import copy_tree
import main_facenet_speaker_class
import facenet
from PIL import Image
from combined_vad import create_vad



def erase_folder_ifexist_and_recreate(folder):
    if os.path.exists(folder):
        shutil.rmtree(folder)
    os.makedirs(folder)

def erase_folder_ifexist(folder):
    if os.path.exists(folder):
        shutil.rmtree(folder)

def calc_score_l2_norm( x, y):
    d = x - y
    score = np.dot(d, d)
    return score


def calc_correlation_score( x, y):
    s = np.corrcoef(x, y)
    score = s[0, 1]
    return score

class cluster_obj:

    def __init__(self):
        self.rep_indexs =[]
        self.avarage_rep = None
        self.number_rep_in_cluster = 1
        self.last_merge_probability = -2
        self.speaker_no_noise_label = False
        self.orig_img_ind = None


class list_clusters_obj:

    def __init__(self, list_clusters, min_tresh_for_marge):

        self.list_clusters = list_clusters
        self.score_matrix = None
        self.correlation_matrix = None
        self.min_tresh_for_marge = min_tresh_for_marge
        self.clusters_len = len(list_clusters)
        self.cluster()


    def calc_base_score_matrix(self):

        len1 = self.clusters_len
        self.score_matrix = np.zeros([len1, len1])
        for i in range(len1 - 1):
            self.score_matrix[i, i] = 0.
            for j in range(i + 1, len1):
                s1 = self.list_clusters[i].avarage_rep
                s2 = self.list_clusters[j].avarage_rep
                self.score_matrix[i, j] = calc_score_l2_norm(s1, s2)
                self.score_matrix[j, i] = self.score_matrix[i, j]
        self.score_matrix[len1 - 1, len1 - 1] = 0.

    def calc_base_correlation_matrix(self):

        # len1 = self.clusters_len
        # self.correlation_matrix = np.zeros([len1, len1])
        # for i in range(len1 - 1):
        #     self.correlation_matrix[i, i] = -2.
        #     for j in range(i + 1, len1):
        #         self.correlation_matrix[i, j] = calc_correlation_score(self.score_matrix[i, :], self.score_matrix[j, :])
        #         self.correlation_matrix[j, i] = self.correlation_matrix[i, j]
        # self.correlation_matrix[len1 - 1, len1 - 1] = -2.

        self.correlation_matrix = np.corrcoef(self.score_matrix)
        np.fill_diagonal(self.correlation_matrix, -2.)

    def calc_score_matrixs(self):
        self.calc_base_score_matrix()
        self.calc_base_correlation_matrix()

    def merge_2_clusters_in_a_list(self, indx, indy, temp_tresh):

        nx = self.list_clusters[indx].number_rep_in_cluster
        ny = self.list_clusters[indy].number_rep_in_cluster
        dvec_x = self.list_clusters[indx].avarage_rep
        dvec_y = self.list_clusters[indy].avarage_rep
        tempx = self.list_clusters[indx].rep_indexs
        tempy = self.list_clusters[indy].rep_indexs
        mergedlist = list(set(tempx + tempy))
        if len(tempx) != nx:
            print(' BUG - nx != len(tempx)')
        if len(tempy) != ny:
            print(' BUG - nx != len(tempx)')
        if len(mergedlist) != len(tempx) + len(tempy):
            print('BUG - 2 clusters contained the same indexes')
        mergedlist.sort()
        self.list_clusters[indx].rep_indexs = mergedlist
        new_dvec = (nx * dvec_x + ny * dvec_y) / (nx + ny)
        self.list_clusters[indx].avarage_rep = new_dvec
        self.list_clusters[indx].last_merge_probability = temp_tresh
        self.list_clusters[indx].number_rep_in_cluster = nx + ny
        self.list_clusters.pop(indy)
        self.clusters_len = len(self.list_clusters)


    def cluster(self):

        self.calc_score_matrixs()
        while True:

            if len(self.list_clusters) == 1:
                break

            ind_max = np.argmax(self.correlation_matrix)
            ind2_max = np.unravel_index(ind_max, self.correlation_matrix.shape)
            indx = ind2_max[0]
            indy = ind2_max[1]
            temp_tresh = self.correlation_matrix[indx, indy]

            if temp_tresh > self.min_tresh_for_marge and self.clusters_len >0:
                if indx < indy:
                    self.merge_2_clusters_in_a_list(indx, indy, temp_tresh)
                else:
                    self.merge_2_clusters_in_a_list(indy, indx, temp_tresh)
            else:
                break
            self.calc_score_matrixs()
        return


class wav_diarization:

    def __init__(self, wav_path, deep_speaker_obj, debug_mode=False,
                 temp_folder_path='', label_audacity_file_path='', min_tresh_for_marge=0.7,
                 base_min_imgs_for_speaker=5, base_min_continue_speach=1,
                 base_duration_for_segment=40,
                 percentage_of_speech_per_second_tresholds =0.5, debug_flag_calc_img=True,
                 min_speech_seconds_in_wav = 100, dvec_dim =128, hop_length=50,
                save_rep_dir='', load_rep_path='', delete_diarization_img_flag=False):

        self.sr = 8000

        # TODO: these parameter should be outside and shared
        self.fmin = 80
        self.fmax = 4000
        self.win_len = 512
        self.hop_length = hop_length
        self.dvec_dim = dvec_dim
        self.wav_batch_size = 500
        self.spec_length_time_size = 160

        self.percentage_of_speech_per_second_tresholds = percentage_of_speech_per_second_tresholds
        self.img_length_in_sec = (self.hop_length * self.spec_length_time_size)/float(self.sr)
        self.number_of_clusters = -17 #any negative number will be fine
        self.number_of_segment_clusters = 3 #any positive integer >1 will be fine number will be fine
        self.wav_path = wav_path
        self.temp_folder_path = temp_folder_path
        self.wav_imgs_path = self.temp_folder_path + '/img'
        self.wav_dvec_path = self.temp_folder_path + '/dvec'
        self.deep_speaker_obj = deep_speaker_obj
        self.min_tresh_for_marge = min_tresh_for_marge
        self.base_min_imgs_for_speaker = base_min_imgs_for_speaker  # min numbrt of image to consider speaker (1s)
        self.base_min_continue_speach = base_min_continue_speach
        self.base_duration_for_segment = base_duration_for_segment
        self.vad_win_size = int(self.sr * 0.01)
        self.vad_bob = Energy_2Gauss()
        self.img_size = self.spec_length_time_size
        self.test_win_spec = self.spec_length_time_size
        self.debug = debug_mode
        self.lable_audacity_file_path = label_audacity_file_path
        self.list_clusters = []
        self.orig_list_clusters = None
        self.list_segment_clusters = [] #reducing time comutation/
        self.label_data = []
        self.base_time_segments = None
        self.epsilon = 0.0001
        self.vad_label=None
        self.vad_label_precent =None
        self.debug_flag_calc_img= debug_flag_calc_img
        self.id_dic = None
        self.inv_id_dic = None

        self.orig_speaker_ids =None
        self.save_rep_dir = save_rep_dir
        self.load_rep_path = load_rep_path
        self.min_speech_seconds_in_wav = min_speech_seconds_in_wav
        np.set_printoptions(suppress=True)
        self.delete_diarization_img_flag = delete_diarization_img_flag

        if self.debug:
            # self.debug_folder = '/media/moti/motiHD2/tesing/temp/test/'
            self.debug_folder = '/media/algo/nimrod/Debug/Diarization/'  ####

    def moti_sort_path_list(self, path_list):
        len1 = len(path_list)
        sort_list = np.zeros([len1,2], dtype=np.int)
        for i in range(len1):
            temp = path_list[i]
            b = temp.rfind('/')
            tmp1 = temp[b+1:]
            tmp2 =tmp1.split('_')
            tmp3 = int(tmp2[0])
            sort_list[i,0]=i
            sort_list[i,1]=tmp3
        sort1= sort_list[sort_list[:, 1].argsort()]
        ind = sort1[:,0]
        path_list1 = np.array(path_list)
        np_sort_list = path_list1[ind]
        sort2_list = np_sort_list.tolist()
        return sort2_list




    def init_wav_diarization(self):

        dvec_temp_path = self.wav_dvec_path
        command = dvec_temp_path + '/*.npy'
        dvev_path_list1 = glob.glob(command)
        dvev_path_list= self.moti_sort_path_list(dvev_path_list1)
        len1 = len(dvev_path_list)
        if not self.debug_flag_calc_img:
            self.vad_label_precent = np.loadtxt(self.debug_folder + 'vad_label_precent.txt')
            self.vad_label = np.loadtxt(self.debug_folder + 'vad_label_precent.txt')

        len2 = self.vad_label.shape[0]
        self.number_of_clusters = np.min([len1, len2])
        self.base_time_segments = np.zeros([self.number_of_clusters, 7])

        for i in range(self.number_of_clusters):
            temp =cluster_obj()
            temp.rep_indexs = [i]
            temp.avarage_rep = np.load(dvev_path_list[i])
            file_name = dvev_path_list[i].replace(dvec_temp_path + '/', '')
            file_name1 = file_name.replace('.npy', '')
            file_info = file_name1.split('_')
            self.base_time_segments[i, 0] = int(file_info[1]) * self.hop_length / float(self.sr) #start img time
            self.base_time_segments[i, 1] = ( int(file_info[2]) +1) * self.hop_length / float(self.sr) #eng img time
            self.base_time_segments[i, 2] = -5 #speaker id
            self.base_time_segments[i, 3] = int(file_info[3])# img number
            self.base_time_segments[i, 4] = self.vad_label[i] #binary vad
            self.base_time_segments[i, 5] = self.vad_label_precent[i] # probability vad
            self.base_time_segments[i, 6] = -1 # -1 for noise 1 for speaker
            temp.orig_img_ind = int(file_info[0])

            self.list_clusters.append(temp)

        self.orig_list_clusters = copy.deepcopy(self.list_clusters)


    def merge_segment_clusters(self):

        new_clusters =[]
        for i in range(self.number_of_segment_clusters):
            temp_list_of_clisters_obj = self.list_segment_clusters[i]
            len1 = temp_list_of_clisters_obj.clusters_len
            for j in range(len1):
                tmp_cluster_obj = temp_list_of_clisters_obj.list_clusters[j]
                new_clusters.append(tmp_cluster_obj)

        self.list_clusters = new_clusters
        self.number_of_clusters = len(new_clusters)

    def make_length_filter_on_clusters(self):

        if self.label_data.shape[0]==0:
            return False
        all_id_list = self.label_data[:, 2].tolist()
        temp1 = set(all_id_list)
        label_data_id_list = list(temp1)
        len1 = len(label_data_id_list)
        short_talk_speaker_id =[]
        for i in range(len1):
            id = label_data_id_list[i]
            ind_id = np.where(self.label_data[:, 2]==id)
            id_data = self.label_data[ind_id[0], :]
            length_vec = id_data[:, 1]-id_data[:, 0]
            total_length = np.sum(length_vec)
            if total_length < self.base_min_imgs_for_speaker:
                short_talk_speaker_id.append(id)
        for short_id in short_talk_speaker_id:
            delete_t = np.where(self.label_data[:, 2] == short_id)
            self.label_data = np.delete(self.label_data, delete_t, 0)
        if self.label_data.shape[0] > 0:
            return True
        else:
            return False


    def update_one_segment_clusters_iteration(self):

        num_segments = self.number_of_clusters / self.base_duration_for_segment
        last_segment_len = self.number_of_clusters - num_segments * self.base_duration_for_segment
        cure_segment = 0
        self.number_of_segment_clusters = 0
        self.list_segment_clusters =[]
        for i in range(num_segments):
            temp_list = self.list_clusters[cure_segment:cure_segment+self.base_duration_for_segment]
            temp_list_clusters_obj = list_clusters_obj(temp_list, self.min_tresh_for_marge)
            self.list_segment_clusters.append(temp_list_clusters_obj)
            cure_segment += self.base_duration_for_segment

        self.number_of_segment_clusters += num_segments
        temp_list = self.list_clusters[cure_segment:cure_segment+last_segment_len]
        if len(temp_list) > 0:
            temp_list_clusters_obj = list_clusters_obj(temp_list, self.min_tresh_for_marge)
            self.list_segment_clusters.append(temp_list_clusters_obj)
            self.number_of_segment_clusters +=1

        self.merge_segment_clusters()



    def get_dict_id(self):

        non_noise_element = np.where(self.base_time_segments[:,6]>0)
        segment_speaker_ids = non_noise_element[0].tolist()
        temp = self.base_time_segments[segment_speaker_ids,2]
        orig_speker_ids = set(temp)
        orig_speker_ids_list = list(orig_speker_ids)
        orig_speker_ids_list.sort()
        id_dic = {}
        len_dict = len(orig_speker_ids_list)
        for i in range(len_dict):
            key1 = str(int(orig_speker_ids_list[i]))
            id_dic[key1] = str(i)
        return id_dic, orig_speker_ids_list, len_dict

    def create_label_data_file(self):

        fid = open(self.lable_audacity_file_path, 'w')
        len1 = self.label_data.shape[0]
        if len1 ==0:
            print('No speakrs where found. probebly wave is too short')
            fid.close()
            return False, None, None
        orig_id=[]
        new_id =[]
        count =0
        for i in range(len1 - 1):
            t1 = self.label_data[i][2]
            if t1 >=0:
                if t1 not in orig_id:
                    orig_id.append(t1)
                    new_id.append(count)
                    count+=1
                ind =orig_id.index(t1)
                id =new_id[ind]
                msg = '{0}\t{1}\tspeaker{2}\n'.format(self.label_data[i][0], self.label_data[i][1], id)
                self.label_data[i][2] = float(id)
            else:
                msg = '{0}\t{1}\tnoise\n'.format(self.label_data[i][0], self.label_data[i][1])
            fid.write(msg)

        t1 = self.label_data[len1 - 1][2]
        if t1 >=0:
            if t1 not in orig_id:
                orig_id.append(t1)
                new_id.append(count)
                count+=1
            ind =orig_id.index(t1)
            id =new_id[ind]
            msg = '{0}\t{1}\tspeaker{2}\n'.format(self.label_data[len1 - 1][0], self.label_data[len1 - 1][1], id)
            self.label_data[len1 - 1][2] = float(id)
        else:
            msg = '{0}\t{1}\tnoise\n'.format(self.label_data[len1 - 1][0], self.label_data[len1 - 1][1])
        fid.write(msg)
        fid.close()
        return True, orig_id, new_id


    def calc_one_speaker_speech_time(self, speaker_id, data1):

        len1 = data1.shape[0]
        out_data = []

        if len1==0:
            return False, []
        if len1 == 1:
            if self.base_min_continue_speach ==1:
                temp = [data1[0][0], data1[0][1], speaker_id]
                out_data.append(temp)
                return True, out_data
            else:
                return False, []

        start = int(data1[0,3])
        ind_start = 0
        prev = start
        ind_prev = 0
        flag = False
        if len1 == 2:
            cure = data1[1][3]
            diff = cure - start
            if diff ==1:
                if self.base_min_continue_speach <=2:
                    time_seg = [data1[0][0], data1[1][1], speaker_id]
                    out_data.append(time_seg)
                    return True, out_data
                else:
                    return False, []
            else:
                if self.base_min_continue_speach <= 1:
                    time_seg = [data1[0][0], data1[0][1], speaker_id]
                    out_data.append(time_seg)
                    time_seg1 = [data1[1][0], data1[1][1], speaker_id]
                    out_data.append(time_seg1)
                    return True, out_data

                else:
                    return False, []

        else:
            for i in range(1, len1 - 1):
                cure = data1[i][3]
                ind_cure = i
                if cure - prev == 1:
                    prev += 1
                    ind_prev =i
                    continue
                else:
                    if prev - start >= self.base_min_continue_speach - 1:
                        time_seg = [data1[ind_start][0], data1[ind_prev][1], speaker_id]
                        out_data.append(time_seg)
                        flag = True
                    start = cure
                    ind_start =ind_cure
                    prev = start
                    ind_prev = ind_start


        cure = data1[len1-1][3]
        if cure - prev == 1:
            prev += 1
            ind_prev =len1-1

        if prev - start >= self.base_min_continue_speach - 1:
            time_seg = [data1[ind_start][0], data1[ind_prev][1], speaker_id]
            out_data.append(time_seg)
            flag = True

        return flag, out_data

    def save_speaker_rep(self, orig_id, new_id):
        len1 = len(orig_id)
        erase_folder_ifexist_and_recreate(self.save_rep_dir)
        for i in range(len1):
            str_id = str(int(new_id[i]))
            path = self.save_rep_dir + '/speaker{0}'.format(str_id)
            erase_folder_ifexist_and_recreate(path)
            str_orig_id = str(int(orig_id[i]))
            orig_cluster_index = self.inv_id_dic[str_orig_id]
            cluster = self.list_clusters[int(orig_cluster_index)]
            rep = cluster.avarage_rep
            np.savetxt(path + '/rep.txt', rep)


    def compare_labels2old_rep(self, orig_id, new_id):
        len1 = len(orig_id)
        old_rep = np.loadtxt(self.load_rep_path)
        score_tabel1 = np.zeros(len1)
        score_tabel2 = np.zeros(len1)

        for i in range(len1):
            str_id = str(int(new_id[i]))
            str_orig_id = str(int(orig_id[i]))
            orig_cluster_index = self.inv_id_dic[str_orig_id]
            cluster = self.list_clusters[int(orig_cluster_index)]
            rep = cluster.avarage_rep
            score_tabel1[i] = calc_correlation_score(old_rep, rep)
            score_tabel2[i] = calc_score_l2_norm(old_rep, rep)


    def calc_label_data(self):
        for i in range(self.number_of_clusters):
            cluster = self.list_clusters[i]
            ind = cluster.rep_indexs
            self.base_time_segments[ind, 2] = i
            mean_noise = np.mean(self.base_time_segments[ind, 4])
            if mean_noise > self.percentage_of_speech_per_second_tresholds:
                cluster.speaker_no_noise_label = True
                self.base_time_segments[ind, 6] = 1
        self.base_time_segments = self.base_time_segments[self.base_time_segments[:, 0].argsort()]
        ind_noise = np.where(self.base_time_segments[:,6] == -1)
        data_noise = self.base_time_segments[ind_noise[0].tolist()]
        data_noise = data_noise[data_noise[:, 0].argsort()]
        flag, out_data = self.calc_one_speaker_speech_time(-1, data_noise)
        if flag:
            self.label_data += out_data
        self.id_dic, self.orig_speker_ids_list, len_dict = self.get_dict_id()
        self.inv_id_dic = dict((self.id_dic[k], k) for k in self.id_dic)
        for i in range(len_dict):
            orig_speaker_id = self.orig_speker_ids_list[i]
            ind_speaker = np.where(self.base_time_segments[:, 2] == orig_speaker_id)
            data_speaker = self.base_time_segments[ind_speaker[0].tolist()]
            data_speaker = data_speaker[data_speaker[:, 0].argsort()]
            new_speaker_id1 = str(int(orig_speaker_id))
            new_speaker_id = int(self.id_dic[new_speaker_id1])
            flag, out_data = self.calc_one_speaker_speech_time(new_speaker_id, data_speaker)
            if flag:
                self.label_data += out_data
        self.label_data = np.array(self.label_data)
        length_flag = self.make_length_filter_on_clusters()
        if length_flag:
            self.label_data = self.label_data[self.label_data[:, 0].argsort()]
            data_file_flag, orig_id, new_id = self.create_label_data_file()
            if not data_file_flag:
                print('No speaker found')
                return
            if self.save_rep_dir !='':
                self.save_speaker_rep(orig_id, new_id)
            if self.load_rep_path != '':
                self.compare_labels2old_rep(orig_id, new_id)
            return True, ''
        else:
            erase_folder_ifexist_and_recreate(self.save_rep_dir)
            print('no speaker')
            return False, 'no speaker found'

    def create_img_and_reps(self):
        img_flag, msg = self.create_imgs_from_diarization_wav()
        if not img_flag:
            return False, self.vad_label, None
        self.create_dvecs_from_img_folder()
        return True, self.vad_label, 'o.k'

    def create_img_and_reps2(self, img_folder_path, dvev_path):
        img_flag, msg = self.create_imgs_from_diarization_wav2(img_folder_path)
        if not img_flag:
            return False, None
        self.create_dvecs_from_img_folder(based_img_folder = img_folder_path, dvec_folder_path = dvev_path)
        return True, self.vad_label, 'o.k'

    def create_vad_img_and_reps(self):
        img_flag, msg = self.create_imgs_from_diarization_wav()
        if not img_flag:
            return False, None
        self.create_dvecs_from_img_folder()
        return True, self.vad_label, 'o.k'


    def diarization(self):

        if self.debug_flag_calc_img:
            img_flag, msg = self.create_imgs_from_diarization_wav()
            if not img_flag:
                return False, None, None, msg
            self.create_dvecs_from_img_folder()

        if self.delete_diarization_img_flag:
            if os.path.exists(self.wav_imgs_path):
                shutil.rmtree(self.wav_imgs_path)

        self.init_wav_diarization()
        old_length = len(self.list_clusters)
        flag = True
        while flag:
            self.update_one_segment_clusters_iteration()
            if old_length == len(self.list_clusters):
                flag = False
            else:
                old_length = len(self.list_clusters)
                print('{0}'.format(old_length))
        label_flag, msg = self.calc_label_data()
        if label_flag:
            return True, self.label_data, self.vad_label, ''
        else:
            return False, None, None, msg


    def create_synchronize_spectrogram_librosa_no_vad(self):

        fmin = self.fmin
        fmax = self.fmax
        win_len = self.win_len
        fs, samples = wav.read(self.wav_path)
        len34 = len(samples)
        if len34 < 3*fs:
            msg = 'num samples in wav = {0} less then 3 seconds'.format(len34)
            return False, None, None, msg
        if fs != self.sr:
            print('BUG expected to sample rate equal ={0} and get sample rate = {1}'.format(self.sr, fs))

        data1 = samples.astype(np.float64)
        len1 = data1.shape[0]
        noise = np.random.uniform(low=-0.001, high=0.001, size=len1)
        data1 += noise


        S = librosa.feature.melspectrogram(data1, sr=fs, n_fft=win_len, hop_length=self.hop_length, n_mels=self.img_size, fmin=fmin,
                                           fmax=fmax)
        log_S = librosa.logamplitude(S, ref_power=np.max)
        len1 = log_S.shape[1]
        segment_time = np.zeros(len1, dtype=np.int)
        for i in range(len1):
            segment_time[i] = i

        comb_seg, pure_label = create_vad(self.wav_path)

        self.vad_label = pure_label
        self.vad_label_precent = pure_label
        if self.debug:
            np.savetxt(self.debug_folder + 'vad_label.txt',self.vad_label, fmt='%1.0f')
            np.savetxt(self.debug_folder + 'vad_label_precent.txt', self.vad_label_precent, fmt='%1.3f')
        return True, log_S, segment_time, ''

    def create_synchronize_spectrogram_librosa(self, wav_path):

        fmin = self.fmin
        fmax = self.fmax
        win_len = self.win_len
        vad_bob = self.vad_bob
        fs, samples = wav.read(wav_path)
        if fs != self.sr:
            print('BUG expected to sample rate equal ={0} and get sample rate = {1}'.format(self.sr, fs))


        vad_win_size = int(fs * 0.01)

        data1 = samples.astype(np.float64)
        len1 = data1.shape[0]
        noise = np.random.uniform(low=-0.001, high=0.001, size=len1)
        data1 += noise
        input_sig = [fs, data1]
        r, d, label = vad_bob.__call__(input_sig)
        if not np.isfinite(d).all():
            print('Nan or not finite in vad {0}'.format(audiopath))
            return False, None

        S = librosa.feature.melspectrogram(data1, sr=fs, n_fft=win_len, hop_length=self.hop_length, n_mels=self.img_size, fmin=fmin,
                                           fmax=fmax)
        log_S = librosa.logamplitude(S, ref_power=np.max)

        len1 = log_S.shape[1]
        len11 = label.shape[0]
        spec_length_time_size = self.spec_length_time_size
        spec_win_size = self.hop_length
        len2 = len1 * spec_win_size
        len3 = len11 * vad_win_size
        if len2 < len3:
            len4 = len1
        else:
            len4 = len3 / spec_win_size
        ind = []
        log_S1 = log_S[:, :len4]
        segment_time = np.zeros(len4, dtype=np.int)
        for i in range(len4):
            segment_time[i] = i
            st = i * spec_win_size
            en = st + spec_win_size
            vad_st = st / vad_win_size
            vad_en = en / vad_win_size + 1
            if vad_en < len11 + 1:
                la = label[vad_st:vad_en]
            else:
                la = label[vad_st:len11]
            len5 = la.shape[0]
            len6 = np.count_nonzero(la)
            if len5 != len6:
                ind.append(i)
        d1 = np.delete(log_S1, ind, 1)
        d2 = np.delete(segment_time, ind, 0)

        return True, d1, d2

    def create_imgs_from_diarization_wav(self):

        erase_folder_ifexist_and_recreate(self.wav_imgs_path)
        flag, ims, ims_time, msg34 = self.create_synchronize_spectrogram_librosa_no_vad()
        temp_len = 0
        if flag:
            len55 = ims.shape[1]
            num_files = len55 / self.spec_length_time_size
            if num_files > 0:
                if num_files< self.min_speech_seconds_in_wav:
                    msg = 'number of spectrogram time frames is {0} - less than {1} min speech seconds in wav'.format(len55, self.min_speech_seconds_in_wav)
                    print(msg)
                    return False, msg
                num_sample =0
                for j1 in range(num_files):

                    ims1 = ims[:, temp_len:temp_len + self.spec_length_time_size]
                    sample_frames = ims_time[temp_len:temp_len + self.spec_length_time_size]
                    start_time_sample = sample_frames[0]
                    end_time_sample = sample_frames[-1]
                    temp_len = temp_len + self.test_win_spec
                    path_x = self.wav_imgs_path + '/{0}_{1}_{2}_{3}.jpg'.format(num_sample, int(start_time_sample), int(end_time_sample),j1)
                    dd = np.zeros([self.img_size, self.img_size, 3])
                    dd[:, :, 0] = dd[:, :, 1] = dd[:, :, 2] = ims1
                    scipy.misc.imsave(path_x, dd)
                    num_sample += 1
                msg = 'create {0} spectrograms'.format(num_sample)
                print(msg)
                return True, msg
            else:
                msg = 'number of spectrogram time frames is {0} - less than {1}'.format(len55, self.spec_length_time_size)
                print(msg)
                return False, msg

        else:
            msg = msg34
            print(msg)
            return False, msg

    def get_spectograms_seg_index(self, seg_time, ims_time):
        len1 = len(seg_time)
        spec_frames_in_1s = int(self.sr/self.hop_length)
        if len1==0:
            return False, None
        seg_list =[]

        for i in range(len1):
            st =seg_time[i][0]
            en = seg_time[i][1]
            st0=st*spec_frames_in_1s
            en0=en*spec_frames_in_1s
            zz = np.where(ims_time >= st0)
            ind_st =zz[0][0]
            zz1 = np.where(ims_time <= en0)
            ind_en = zz1[0][-1]
            temp = [ind_st, ind_en]
            seg_list.append(temp)

        return True, seg_list


    def create_imgs_from_diarization_wav_for_training(self, ims, ims_time, seg_time, wav_name, wav_imgs_path, img_out_folder):

        seg_flag, seg_list = self.get_spectograms_seg_index(seg_time, ims_time)
        if not seg_flag:
            msg = '{0} rrrr  segment length problem'.format(wav_imgs_path)
            print(msg)
            return False, msg
        len1 =len(seg_list)
        s_flag = False
        num_sample = 0
        for i in range(len1):
            temp = seg_list[i]
            len55 = temp[1]-temp[0]+1
            num_files = int(float(len55) / self.spec_length_time_size)
            if num_files > 0:

                temp_len  = temp[0]
                for j1 in range(int(num_files)):
                    ims1 = ims[:, temp_len:temp_len + self.spec_length_time_size]
                    sample_frames = ims_time[temp_len:temp_len + self.spec_length_time_size]
                    start_time_sample = sample_frames[0]
                    end_time_sample = sample_frames[-1]
                    temp_len = temp_len + self.test_win_spec
                    path_x = img_out_folder + '/{0}_{1}_{2}_{3}_{4}.jpg'.format(wav_name, num_sample, int(start_time_sample),
                                                                                int(end_time_sample), num_sample)
                    dd = np.zeros([self.img_size, self.img_size, 3])
                    dd[:, :, 0] = dd[:, :, 1] = dd[:, :, 2] = ims1
                    scipy.misc.imsave(path_x, dd)
                    num_sample += 1
                    if not s_flag:
                        s_flag= True
        return s_flag, ''

    def create_continued_imgs_from_diarization_wav_for_training(self, ims, ims_time, seg_time, wav_name, wav_imgs_path, img_out_folder):

        seg_flag, seg_list = self.get_spectograms_seg_index(seg_time, ims_time)
        if not seg_flag:
            msg = '{0} rrrr  segment length problem'.format(wav_imgs_path)
            print(msg)
            return False, msg
        len1 =len(seg_list)
        s_flag = False
        all_ims=np.zeros([self.spec_length_time_size,0])
        for i in range(len1):
            temp = seg_list[i]
            temp_len  = temp[0]
            temp_len1= temp[1]
            ims1 = ims[:, temp_len:temp_len1]
            all_ims = np.concatenate((all_ims,ims1), axis=1)

        len55 = all_ims.shape[1]
        num_files = int(float(len55) / self.spec_length_time_size)
        if num_files > 0:
            temp_len = 0
            for j1 in range(int(num_files)):
                ims1 = all_ims[:, temp_len:temp_len + self.spec_length_time_size]
                temp_len = temp_len + self.spec_length_time_size
                path_x = img_out_folder + '/{0}_{1}.jpg'.format(wav_name, j1)

                dd = np.zeros([self.img_size, self.img_size, 3])
                dd[:, :, 0] = dd[:, :, 1] = dd[:, :, 2] = ims1
                scipy.misc.imsave(path_x, dd)
                if not s_flag:
                    s_flag = True
        else:
            return False, 'less frames ({0}) for one img'.format(len55)

        return s_flag, ''

    def create_vad_imgs_from_diarization(self, ims, ims_time, wav_name, wav_imgs_path, img_out_folder):

        len55 = ims.shape[1]
        num_files = len55 / self.spec_length_time_size
        temp_len = 0
        if num_files > 0:
            for i in range(num_files):

                ims1 = ims[:, temp_len:temp_len + self.spec_length_time_size]
                sample_frames = ims_time[temp_len:temp_len + self.spec_length_time_size]
                start_time_sample = sample_frames[0]
                end_time_sample = sample_frames[-1]
                temp_len = temp_len + self.test_win_spec
                path_x = img_out_folder + '/{0}_{1}_{2}_{3}_{4}.jpg'.format(wav_name, i, int(start_time_sample),
                                                                            int(end_time_sample), i)
                dd = np.zeros([self.img_size, self.img_size, 3])
                dd[:, :, 0] = dd[:, :, 1] = dd[:, :, 2] = ims1
                scipy.misc.imsave(path_x, dd)
                #num_sample += 1
        else:
            return False, 'num_files = 0 in {0}'.format(wav_imgs_path)
        return True, ''



    def create_dvecs_from_img_folder(self ):

        based_img_folder = self.wav_imgs_path
        dvec_folder_path = self.wav_dvec_path
        batch_size = self.wav_batch_size
        deep_speaker = self.deep_speaker_obj
        img_size = self.img_size
        erase_folder_ifexist_and_recreate(dvec_folder_path)
        command = based_img_folder + '/*.jpg'
        dir_list1 = glob.glob(command)
        len1 = len(dir_list1)
        num_runs = int(len1/batch_size)
        remind = len1 - batch_size*num_runs
        images = np.zeros((batch_size, img_size, img_size, 3))
        images_r = np.zeros((remind, img_size, img_size, 3))
        if num_runs == 0:
            flag2 = True
        for i in range(num_runs):
            flag1 = True
            flag2 = True
            file_name1 = []
            for j in range(batch_size):
                path11 = dir_list1[batch_size*i + j]
                n1 = path11.replace(based_img_folder, '')
                n2 = n1.replace('.jpg', '.npy')
                file_name1.append(n2)
                fff = scipy.misc.imread(path11)
                images[j, :, :, :] = facenet.prewhiten(fff)
            try:
                flag, rep, str6 = deep_speaker.get_rep_from_img(images)
                print(based_img_folder + ' {0}'.format(i))
            except Exception as error:
                print('ERROR in ' + dir)
                flag1 = False
                print(error + ' comput one by one')
                for j in range(batch_size):
                    try:
                        flag, rep, str6 = deep_speaker.get_rep_from_img(images[j])
                        rep_path = dvec_folder_path + file_name1[j]
                        np.save(rep_path, rep)
                    except Exception as error1:
                        print(dir_list1[batch_size*i + j])
                        print(error1)
                        continue
            if flag1:
                for j in range(batch_size):
                    rep_path = dvec_folder_path + file_name1[j]
                    np.save(rep_path, rep[j])

        if remind != 0:

            file_name1 = []

            for k in range(remind):
                path11 = dir_list1[batch_size * num_runs + k]
                n1 = path11.replace(based_img_folder, '')
                n2 = n1.replace('.jpg', '.npy')
                file_name1.append(n2)
                fff = scipy.misc.imread(path11)
                images_r[k, :, :, :] = facenet.prewhiten(fff)
            try:
                flag, rep, str6 = deep_speaker.get_rep_from_img(images_r)
            except Exception as error:
                print('ERROR in remind ')
                flag2 = False
                print(error + ' ERROR in comput one by one')
                for j in range(remind):
                    try:
                        flag, rep, str6 = deep_speaker.get_rep_from_img(images_r[j])
                        rep_path = dvec_folder_path + file_name1[j]
                        np.save(rep_path, rep)

                    except Exception as error1:
                        print(dir_list1[batch_size * num_runs + j])
                        print(error1)
                        continue
            if flag2:
                for j in range(remind):
                    rep_path = dvec_folder_path + file_name1[j]
                    np.save(rep_path, rep[j])


    def create_dvecs_from_img_folder2(self, dir_list1, based_img_folder, dvec_folder_path ):

        based_img_folder = based_img_folder
        dvec_folder_path = dvec_folder_path
        batch_size = self.wav_batch_size
        deep_speaker = self.deep_speaker_obj
        img_size = self.img_size
        erase_folder_ifexist_and_recreate(dvec_folder_path)
        len1 = len(dir_list1)
        num_runs = int(len1/batch_size)
        remind = len1 - batch_size*num_runs
        images = np.zeros((batch_size, img_size, img_size, 3))
        images_r = np.zeros((remind, img_size, img_size, 3))
        if num_runs == 0:
            flag2 = True
        for i in range(num_runs):
            flag1 = True
            flag2 = True
            file_name1 = []
            for j in range(batch_size):
                path11 = dir_list1[batch_size*i + j]
                n1 = path11.replace(based_img_folder, '')
                n2 = n1.replace('.jpg', '.npy')
                file_name1.append(n2)
                fff = scipy.misc.imread(path11)
                images[j, :, :, :] = facenet.prewhiten(fff)
            try:
                flag, rep, str6 = deep_speaker.get_rep_from_img(images)
                print(based_img_folder + ' {0}'.format(i))
            except Exception as error:
                print('ERROR in ' + dir)
                flag1 = False
                print(error + ' comput one by one')
                for j in range(batch_size):
                    try:
                        flag, rep, str6 = deep_speaker.get_rep_from_img(images[j])
                        rep_path = dvec_folder_path + file_name1[j]
                        np.save(rep_path, rep)
                    except Exception as error1:
                        print(dir_list1[batch_size*i + j])
                        print(error1)
                        continue
            if flag1:
                for j in range(batch_size):
                    rep_path = dvec_folder_path + file_name1[j]
                    np.save(rep_path, rep[j])

        if remind != 0:

            file_name1 = []

            for k in range(remind):
                path11 = dir_list1[batch_size * num_runs + k]
                n1 = path11.replace(based_img_folder, '')
                n2 = n1.replace('.jpg', '.npy')
                file_name1.append(n2)
                fff = scipy.misc.imread(path11)
                images_r[k, :, :, :] = facenet.prewhiten(fff)
            try:
                flag, rep, str6 = deep_speaker.get_rep_from_img(images_r)
            except Exception as error:
                print('ERROR in remind ')
                flag2 = False
                print(error + ' ERROR in comput one by one')
                for j in range(remind):
                    try:
                        flag, rep, str6 = deep_speaker.get_rep_from_img(images_r[j])
                        rep_path = dvec_folder_path + file_name1[j]
                        np.save(rep_path, rep)

                    except Exception as error1:
                        print(dir_list1[batch_size * num_runs + j])
                        print(error1)
                        continue
            if flag2:
                for j in range(remind):
                    rep_path = dvec_folder_path + file_name1[j]
                    np.save(rep_path, rep[j])

    def compare_orig_with_n_p(self, p_rep, n_rep):

        o= self.orig_list_clusters
        len1 = len(o)
        score_out = np.zeros([len1,3])
        for i in range(len1):
            temp = o[i]
            cure_rep = temp.avarage_rep
            score_out[i,0] =calc_correlation_score(p_rep, cure_rep)
            score_out[i,1] =calc_correlation_score(n_rep, cure_rep)
            score_out[i,2] = temp.orig_img_ind
        score_out1 = score_out[score_out[:, 2].argsort()]
        return score_out1





if __name__ == "__main__":

    macabi_model = True
    if macabi_model:

        speakerModelPath = '/home/ds.mkm-research.org.il/moti.halpert/nvvp_workspace/moti/src/facenet-master/model_macabi/20180409-170432/model-220180422-131918.pb'
        speakerModelName = 'model-20180422-131918.pb'
        min_tresh_for_marge1 = 0.75
        base_min_imgs_for_speaker1 = 10
        base_min_continue_speech1 = 1
        base_duration_for_segment1 = 80
        percentage_of_speech_per_second_tresholds1 = 0.5
        smoothing_window1 = 10
    else:
        speakerModelPath = '/home/ds.mkm-research.org.il/moti.halpert/nvvp_workspace/moti/src/facenet-master/models_beyond/20180129-161547/model-20180129-161547.pb'
        speakerModelName = 'model-20180129-161547.pb'
        min_tresh_for_marge1 = 0.75
        base_min_imgs_for_speaker1 = 10
        base_min_continue_speech1 = 1
        base_duration_for_segment1 = 80
        percentage_of_speech_per_second_tresholds1 = 0.5
        smoothing_window1 = 10

    wavs_folder = '/media/dev_algo/moti/testing/origg_data'
    wav_path = wavs_folder + '/812093003336319.wav'
    wav_path = wavs_folder + '/812093003660988.wav'
    wav_path = wavs_folder + '/812093002409903.wav'
    temp_path = '/media/dev_algo/moti/testingls/tmp_all'
    lable_audacity_file_path = wavs_folder + '/speaker_label.txt'

    debug_flag_calc_img1 = True

    if debug_flag_calc_img1:
        deep_speaker_obj = main_facenet_speaker_class.main_facenet_speaker_class(speakerModelName, speakerModelPath, True)
    else:
        deep_speaker_obj = None

    save_rep_dir1= '/home/ds.mkm-research.org.il/moti.halpert/nvvp_workspace/moti/testing/temp_user_dvec'
    #vad_bob = Energy_2Gauss(smoothing_window=smoothing_window1)

    a=wav_diarization( wav_path, deep_speaker_obj,
                       temp_folder_path=temp_path, label_audacity_file_path=lable_audacity_file_path,
                       min_tresh_for_marge=min_tresh_for_marge1,
                       base_min_imgs_for_speaker=base_min_imgs_for_speaker1,
                       base_min_continue_speach=base_min_continue_speech1,
                       base_duration_for_segment=base_duration_for_segment1,
                       percentage_of_speech_per_second_tresholds=percentage_of_speech_per_second_tresholds1,
                       debug_flag_calc_img=debug_flag_calc_img1, save_rep_dir=save_rep_dir1)
    data = a.diarization()




