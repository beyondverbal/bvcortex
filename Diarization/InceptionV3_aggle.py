"""
Adapted from keras example cifar10_cnn.py
Train ResNet-18 on the CIFAR10 small images dataset.

GPU run command with Theano backend (with TensorFlow, the GPU is automatically used):
    THEANO_FLAGS=mode=FAST_RUN,device=gpu,floatX=float32 python cifar10.py
"""
from __future__ import print_function
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils
from keras.callbacks import ReduceLROnPlateau, CSVLogger, EarlyStopping
import pickle
import numpy as np
from scipy import misc
import numpy as np
import glob
import os
import shutil
import random
import resnet
import keras
from keras.applications.inception_v3 import InceptionV3
from keras.preprocessing import image
from keras.models import Model
from keras.layers import Dense, GlobalAveragePooling2D
from keras import backend as K
from keras.optimizers import SGD


def create_inception_model():
    # create the base pre-trained model
    base_model = keras.applications.inception_resnet_v2.InceptionResNetV2(include_top=False, weights=None, input_tensor=None, input_shape=(160,160,3), pooling=None, classes=31)


    # add a global spatial average pooling layer
    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    # let's add a fully-connected layer
    x = Dense(1024, activation='relu')(x)
    # and a logistic layer -- let's say we have 200 classes
    predictions = Dense(31, activation='softmax')(x)

    # this is the model we will train
    model = Model(input=base_model.input, output=predictions)

    # first: train only the top layers (which were randomly initialized)
    # i.e. freeze all convolutional InceptionV3 layers
    if 0:
        for layer in base_model.layers:
            layer.trainable = False

    # compile the model (should be done *after* setting layers to non-trainable)
    model.compile(optimizer=SGD(lr=0.005, momentum=0.9), loss='categorical_crossentropy')
    return model





img_size = 160
betch_size = 32
nb_classes = 31
chank_len = 1024
epoch_len = chank_len *32
num_batchs_in_chank = 32
num_epoc = 200
num_chanks_in_epoc =32

LABLES = ['yes', 'no', 'up', 'down', 'left', 'right', 'on', 'off', 'stop', 'go', 'silence']

def erase_folder_ifexist_and_recreate(folder):
    if os.path.exists(folder):
        shutil.rmtree(folder)
    os.makedirs(folder)


def get_files_name_and_label(img_folders_path):

    dir_list = os.listdir(img_folders_path)

    index_and_str_label = []
    img_file_list =[]
    label_index = -1
    count = 0
    for dir11 in dir_list:
        if dir11 == '':
            continue
        if dir11 == '\n':
            continue
        if '.DS_Store' in dir11:
            continue
        label_index +=1
        txt_label = dir11.replace('_dum', '')
        temp1 =[label_index, txt_label]
        index_and_str_label.append(temp1)
        print(dir11)
        command = img_folders_path + '/{0}/*.jpg'.format(dir11)
        img_list = glob.glob(command)
        len_list = len(img_list)
        for i in range(len_list):
            temp = [img_list[i], label_index]
            img_file_list.append(temp)
            count +=1

    return  img_file_list, index_and_str_label, count

def get_label_table(img_folders_path):

    dir_list = os.listdir(img_folders_path)

    index_str_label = []
    index_label =[]
    label_index = -1
    for dir11 in dir_list:
        if dir11 == '':
            continue
        if dir11 == '\n':
            continue
        if '.DS_Store' in dir11:
            continue
        label_index +=1
        index_label.append(label_index)
        txt_label = dir11.replace('_dum', '')
        index_str_label.append(txt_label)

    return  index_str_label, index_label


def crate_data_pak(img_folders_path, res_path, info_path, chank_len):


    img_file_list, index_and_str_label, num_of_files = get_files_name_and_label(img_folders_path)
    if len(img_file_list) != num_of_files:
        print('BUG len(img_file_list) != num_of_files')
        return False, None, None
    #erase_folder_ifexist_and_recreate(data_path)
    all_label_path = info_path +'/label.pkl'
    all_img_path =  info_path +'/wav_file_list.pkl'
    with open(all_label_path, 'wb') as ff:
        pickle.dump(index_and_str_label, ff)
    with open(all_img_path, 'wb') as ff1:
        pickle.dump(img_file_list, ff1)

    num_iter = int(num_of_files/chank_len)
    remind = num_of_files - num_iter*chank_len
    np_ind = np.arange(num_of_files)
    len_ind = num_of_files
    for i in range(num_iter):
        chank_temp_ind = random.sample(np_ind, chank_len)
        np_ind = np.delete(np_ind, chank_temp_ind)
        len_ind -= chank_len
        data = np.zeros([chank_len, img_size, img_size, 3], dtype=np.uint8)
        data_lable = np.zeros([chank_len, 1], dtype=np.uint8)
        for j in range(chank_len):
            item = img_file_list[chank_temp_ind[j]]
            temp_path = item[0]
            fff = misc.imread(temp_path)
            data[j,:,:,:] = fff
            data_lable[j]=np.uint8(item[1])
        np_data_path = res_path +'/img_{0}.npy'.format(i)
        np_data_path_label = res_path +'/label_{0}.npy'.format(i)
        np.save(np_data_path, data)
        np.save(np_data_path_label, data_lable)
        print('wavs iteration {0}'.format((i+1)*chank_len))
    if remind > 0:
        np_ind1 = np.arange(num_of_files)
        np_ind1 = np.delete(np_ind1, np_ind)
        temp_f = random.sample(np_ind1, chank_len - remind)
        data = np.zeros([chank_len, img_size, img_size, 3], dtype=np.uint8)
        data_lable = np.zeros([chank_len, 1], dtype=np.uint8)
        len2 = temp_f.shape[0]
        for j in range(remind):
            item = img_file_list[np_ind[j]]
            temp_path = item[0]
            fff = misc.imread(temp_path)
            data[j,:,:,:] = fff
            data_lable[j]=np.uint8(item[1])

        for j in range(len2):
            item = img_file_list[temp_f[j]]
            temp_path = item[0]
            fff = misc.imread(temp_path)
            data[j+remind,:,:,:] = fff
            data_lable[j+remind]=np.uint8(item[1])
        np_data_path = res_path +'/img_{0}.npy'.format(num_iter)
        np_data_path_label = res_path +'/label_{0}.npy'.format(num_iter)
        np.save(np_data_path, data)
        np.save(np_data_path_label, data_lable)
        print('wavs iteration {0}'.format(num_iter*chank_len))


def create_epoc_list(train_data_path):
    command = train_data_path + '/*img*'
    data_list = glob.glob(command)
    label_list = []
    for item in data_list:
        temp = item.replace(train_data_path,'')
        temp1 = temp.replace('img', 'label')
        label_list.append(train_data_path + temp1)
    return data_list, label_list

def train_one_epoce(model, data_list, label_list, num_chanks_in_epoc, betch_size, num_batchs_in_chank, nb_classes, epoc_num):

    len1 = len(data_list)
    index_all = range(len1)
    chank_inds = random.sample(index_all, num_chanks_in_epoc)
    if 0:
        res = np.zeros([num_chanks_in_epoc * 1024, 2])
        pr = 0.
        count=0.
    for i in range(num_chanks_in_epoc):
        print('epoc num {0} chank num {1}'.format(epoc_num, i))
        ind = chank_inds[i]
        chank_data_path = data_list[ind]
        chank_label_path = label_list[ind]
        chank_data = np.load(chank_data_path)
        chank_label = np.load(chank_label_path)
        for j in range(num_batchs_in_chank):
            cur_pos = betch_size*j
            X_train = chank_data[cur_pos:cur_pos+betch_size]
            y_train = chank_label[cur_pos:cur_pos+betch_size]
            X_train = X_train.astype('float32')
            Y_train = np_utils.to_categorical(y_train, nb_classes)
            X_train /= 128.
            model.train_on_batch(X_train, Y_train)
            if 0:
                count += betch_size
                res_batch = model.predict(X_train)
                res_batch1 = np.argmax(res_batch, axis=1)
                temp_ind = i*1024 + j*betch_size
                res[temp_ind:temp_ind+ betch_size,0] =res_batch1
                res[temp_ind:temp_ind+ betch_size,1] =y_train[:,0]
                cc = res_batch1 == y_train[:, 0]
                pr_temp = np.sum(cc)
                pr += int(pr_temp)
                temp_acure = float(pr) / count
    return model


def test_all(model, data_list, label_list, betch_size, num_batchs_in_chank):

    len1 = len(data_list)
    res = np.zeros([len1 * 1024, 2])
    count=0.
    pr=0.
    for i in range(len1):
        chank_data_path = data_list[i]
        chank_label_path = label_list[i]
        chank_data = np.load(chank_data_path)
        chank_label = np.load(chank_label_path)
        for j in range(num_batchs_in_chank):
            cur_pos = betch_size*j
            X_test = chank_data[cur_pos:cur_pos+betch_size]
            y_test = chank_label[cur_pos:cur_pos+betch_size]
            X_test = X_test.astype('float32')
            #Y_test = np_utils.to_categorical(y_test, nb_classes)
            X_test /= 128.
            res_batch = model.predict(X_test)
            count += betch_size
            res_batch1 = np.argmax(res_batch, axis=1)
            temp_ind = i*1024 + j*betch_size
            res[temp_ind:temp_ind+ betch_size,0] =res_batch1
            res[temp_ind:temp_ind+ betch_size,1] =y_test[:,0]
            cc = res_batch1 == y_test[:, 0]
            pr_temp = np.sum(cc)
            pr += int(pr_temp)
            temp_acure = pr/count
    print(temp_acure)
    return temp_acure, res


def train_keras_model(train_data_path, test_data_path, output_mpdel_folder):

    data_list, label_list = create_epoc_list(train_data_path)
    test_data_list, test_label_list = create_epoc_list(test_data_path)

    model = resnet.ResnetBuilder.build_resnet_18((3, 160, 160), 31)
    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    for j in range(num_epoc):
        model = train_one_epoce(model, data_list, label_list, num_chanks_in_epoc, betch_size, num_batchs_in_chank, nb_classes, j)
        path = output_mpdel_folder + '/model_{0}'.format(j)
        model.save(path)
        if j>1:
            delete_path = output_mpdel_folder + '/model_{0}'.format(j-2)
            os.remove(delete_path)
        print('start validation epoc {0}'.format(j))
        temp_acure, res =test_all(model, test_data_list, test_label_list, betch_size, num_batchs_in_chank)

def train_inception_model(train_data_path, test_data_path, output_mpdel_folder):

    model = create_inception_model()

    data_list, label_list = create_epoc_list(train_data_path)
    test_data_list, test_label_list = create_epoc_list(test_data_path)


    for j in range(num_epoc):
        model = train_one_epoce(model, data_list, label_list, num_chanks_in_epoc, betch_size, num_batchs_in_chank, nb_classes, j)
        path = output_mpdel_folder + '/model_{0}'.format(j)
        model.save(path)
        if j>1:
            delete_path = output_mpdel_folder + '/model_{0}'.format(j-2)
            os.remove(delete_path)
        print('start validation epoc {0}'.format(j))
        temp_acure, res =test_all(model, test_data_list, test_label_list, betch_size, num_batchs_in_chank)

def test_train(model_path):

    iv_path = '/media/moti/Transcend/moti/data/kaggel_data/speech/img/train_aug2_test'
    res_path = '/media/moti/Transcend/moti/data/kaggel_data/keras/res1/data/res/res_v2_aug2.txt'
    model = keras.models.load_model(model_path)
    fid = open(res_path,'w')

    index_str_label, index_label = get_label_table(iv_path)
    dir_list = os.listdir(iv_path)

    count =0
    correct1 = 0.
    for dir in dir_list:
        if dir == '':
            continue
        if dir == '\n':
            continue
        if dir == '.DS_Store':
            continue
        cure_label = dir.replace('_dum','')
        ind_label = index_str_label.index(cure_label)

        path1  = iv_path + '/' + dir
        command = path1 + '/*.jpg'
        dir_list1 = glob.glob(command)
        len2 = len(dir_list1)
        num_batchs = int(len2/betch_size)
        remind = len2 - num_batchs*betch_size
        batch_data = np.zeros([betch_size, img_size, img_size, 3], dtype=np.uint8)

        for i in range(num_batchs):
            for j in range(betch_size):
                item = dir_list1[i*betch_size + j]
                fff = misc.imread(item)
                batch_data[j, :, :, :] = fff
            batch_data = batch_data.astype('float32')
            #Y_test = np_utils.to_categorical(y_test, nb_classes)
            batch_data /= 128.
            res_batch = model.predict(batch_data)
            res_batch1 = np.argmax(res_batch, axis=1)
            for k in range(betch_size):
                ind1 = res_batch1[k]
                rec1 = index_str_label[ind1]
                b11 = cure_label not in LABLES and rec1 not in LABLES
                b1 = b11 or rec1==cure_label
                if b1:
                    correct1 +=1.
                count +=1
                msg44 ='{0} {1} {2} {3} {4} {5}'.format(b1, ind_label, ind1, int(cure_label not in LABLES), int(rec1 not in LABLES), correct1 / count )
                fid.write(msg44 + '\n')

                if int(count) % 500 ==0:
                    msg5 = '{0}'.format(correct1/count)
                    print(msg5)

        batch_data_remind = np.zeros([remind, img_size, img_size, 3], dtype=np.uint8)
        if remind >0:
            for j in range(remind):
                item = dir_list1[num_batchs * betch_size + j]
                fff = misc.imread(item)
                batch_data_remind[j, :, :, :] = fff
            res_batch = model.predict(batch_data_remind)
            res_batch1 = np.argmax(res_batch, axis=1)
            for k in range(remind):
                ind1 = res_batch1[k]
                rec1 = index_str_label[ind1]
                b11 = cure_label not in LABLES and rec1 not in LABLES
                b1 = b11 or rec1 == cure_label
                if b1:
                    correct1 += 1.
                count +=1
                msg44 = '{0} {1} {2} {3} {4} {5}'.format(b1, ind_label, ind1, int(cure_label not in LABLES), int(rec1 not in LABLES), correct1 / count )
                fid.write(msg44 + '\n')

                if int(count) % 500 == 0:
                    msg5 = '{0}'.format(correct1 / count)
                    print(msg5)

    fid.close()


if 0:
    res_path = '/media/moti/Transcend/moti/data/kaggel_data/keras/res1/data/train'
    info_path = '/media/moti/Transcend/moti/data/kaggel_data/keras/res1/data/info_train'
    img_folders_path = '/media/moti/Transcend/moti/data/kaggel_data/speech/img/train_aug2_train'
    chank_len = 1024
    crate_data_pak(img_folders_path, res_path, info_path, chank_len)
if 0:
    res_path = '/media/moti/Transcend/moti/data/kaggel_data/keras/res1/data/test'
    info_path = '/media/moti/Transcend/moti/data/kaggel_data/keras/res1/data/info_test'
    img_folders_path = '/media/moti/Transcend/moti/data/kaggel_data/speech/img/train_aug2_test'
    chank_len = 1024
    crate_data_pak(img_folders_path, res_path, info_path, chank_len)



if 0:
    train_data_path = '/media/moti/Transcend/moti/data/kaggel_data/keras/res1/data/train'
    test_data_path = '/media/moti/Transcend/moti/data/kaggel_data/keras/res1/data/test'
    output_mpdel_folder = '/media/moti/Transcend/moti/data/kaggel_data/keras/res1/data/model/1'
    train_inception_model(train_data_path, test_data_path, output_mpdel_folder)
if 1:
    model_path ='/media/moti/Transcend/moti/data/kaggel_data/keras/res1/data/model/2/model_111'
    test_train(model_path)


if 0:
    train_data_path = '/media/moti/Transcend/moti/data/kaggel_data/keras/res1/data/train'
    test_data_path = '/media/moti/Transcend/moti/data/kaggel_data/keras/res1/data/test'
    output_mpdel_folder = '/media/moti/Transcend/moti/data/kaggel_data/keras/res1/data/model/2'
    train_inception_model(train_data_path, test_data_path, output_mpdel_folder)




