import diarization_engine as diar
import main_facenet_speaker_class
import pandas as pd
import pickle
import shutil
import numpy as np
np.set_printoptions(suppress=True)
import os
import glob
import random
import datetime
import time
np.set_printoptions(suppress=True)

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_curve, auc
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dense
from tensorflow.python.keras.optimizers import SGD
from tensorflow.python.keras.optimizers import RMSprop

from tensorflow.python.keras.utils import to_categorical

macabi_model = True

if macabi_model:
    speakerModelPath = '/media/algo/Nimrod/Diarization/facenet_models/20180428-121839/model-20180428-121839.pb'
    speakerModelName = 'model-20180428-121839.pb'
else:
    speakerModelPath = '/media/algo/Nimrod/Diarization/facenet_models/20180129-161547/model-20180129-161547.pb'
    speakerModelName = 'model-20180129-161547.pb'

debug_flag_calc_img1 = True

if debug_flag_calc_img1:
    deep_speaker_obj = main_facenet_speaker_class.main_facenet_speaker_class(speakerModelName, speakerModelPath, True)
else:
    deep_speaker_obj = None


base_all_wav_path = '/media/winshares01'


base_osa_folder = '/media/algo/nimrod/Debug/Diarization/'
base_meta_folder = '/media/algo/dat/MetaData/'
base_macabi_temp_train = '/media/algo/nimrod/Debug/Diarization/img'
img_path_p = base_macabi_temp_train + '/p'

base_dvecs = base_osa_folder + '/base_dvec'
base_ava_rep = base_osa_folder + '/dvec/voice_print'

csv_mata_file = base_meta_folder + '/SegmentsDataAll180827.csv'
csv_p_n_file = base_meta_folder + '/FileNursePatient180827.csv'
out_pkl_wavs_list_path = base_osa_folder + '/wav_list.pkl'

moti_log_file = base_osa_folder + '/moti_log_file.txt'




quik_flag = True
diar_algo_tresh_high = 0.5
diar_algo_tresh_low = 0.2
min_tresh_for_marge1 = 0.75
base_min_imgs_for_speaker1 = 10
base_min_continue_speech1 = 1
base_duration_for_segment1 = 80
pitch_tresholds1 = 20
percentage_of_speech_per_second_tresholds1 = 0.5
smoothing_window1 = 10
min_speech_seconds_in_wav1 = 60
min_imgs_for_voice_print = 1
rep_dim = 128

create_wav_list_flag = False


label_dic ={"0": 'patient', "1": 'nurse', "2": "both","3": 'none' }




def main_preper_n_p_full_imgs(create_wav_list_flag=create_wav_list_flag, base_macabi_temp_train=base_macabi_temp_train):

    if create_wav_list_flag:
        wavs_list =create_wav_list()
    else:
        with open(out_pkl_wavs_list_path, 'rb') as fp:
            wavs_list =pickle.load(fp)

    len1 = len(wavs_list)
    diar.erase_folder_ifexist_and_recreate(base_macabi_temp_train)
    base_csv_data = pd.read_csv(csv_mata_file, encoding='ISO-8859-1')
    p_id_lib = []
    n_id_lib = []
    a = diar.wav_diarization('', deep_speaker_obj,
                             temp_folder_path='', label_audacity_file_path='',
                             min_tresh_for_marge=min_tresh_for_marge1,
                             base_min_imgs_for_speaker=base_min_imgs_for_speaker1,
                             base_min_continue_speach=base_min_continue_speech1,
                             base_duration_for_segment=base_duration_for_segment1,
                             percentage_of_speech_per_second_tresholds=percentage_of_speech_per_second_tresholds1,
                             debug_flag_calc_img=debug_flag_calc_img1,
                             min_speech_seconds_in_wav=min_speech_seconds_in_wav1,
                             save_rep_dir='')
    for i in range(len1):
        pid_first_flag = False
        nid_first_flag = False
        try:
            print('create imgs wav num {0}'.format(i))
            base_wav_info= wavs_list[i]
            wav_path = base_wav_info[1]
            wav_name = base_wav_info[0]
            print(wav_name, ' ', wav_path)
            p_id = base_wav_info[2]
            p_id_path = base_macabi_temp_train + '/p/{0}'.format(p_id)

            n_id = base_wav_info[3]
            n_id_path = base_macabi_temp_train + '/n/{0}'.format(n_id)

            p1, n1, p1_orig, n1_orig = create_p_n_time_list(base_csv_data, wav_name)
            if p_id not in p_id_lib:
                diar.erase_folder_ifexist_and_recreate(p_id_path)
                p_id_lib.append(p_id)
                pid_first_flag = True
            if n_id not in n_id_lib:
                diar.erase_folder_ifexist_and_recreate(n_id_path)
                n_id_lib.append(n_id)
                nid_first_flag = True

            flag, ims, ims_time = a.create_synchronize_spectrogram_librosa(wav_path)
            img_flag, msg = a.create_imgs_from_diarization_wav_for_training(ims, ims_time, p1_orig, wav_name, wav_path, p_id_path)
            if not img_flag:
                if pid_first_flag:
                    if os.path.exists(p_id_path):
                        shutil.rmtree(p_id_path)
                        p_id_lib.remove(p_id)

            img_flag, msg = a.create_imgs_from_diarization_wav_for_training(ims, ims_time, n1_orig, wav_name, wav_path, n_id_path)
            if not img_flag:
                if nid_first_flag:
                    if os.path.exists(n_id_path):
                        shutil.rmtree(n_id_path)
                        n_id_lib.remove(n_id)

        except Exception as error:
            msg ='error in wav {0} name: {1} path {2} with error {3}'.format(i,wav_name,wav_path, error)
            print(msg)
            fid_moti_log.write(msg  + '\n')
            fid_moti_log.flush()
            continue



def main_preper_n_p_full_imgs_after_diarization(base_wav_folder):

    dir_list = os.listdir(base_wav_folder)
    len1 = len(dir_list)

    a = diar.wav_diarization('', deep_speaker_obj,
                             temp_folder_path='', label_audacity_file_path='',
                             min_tresh_for_marge=min_tresh_for_marge1,
                             base_min_imgs_for_speaker=base_min_imgs_for_speaker1,
                             base_min_continue_speach=base_min_continue_speech1,
                             base_duration_for_segment=base_duration_for_segment1,
                             percentage_of_speech_per_second_tresholds=percentage_of_speech_per_second_tresholds1,
                             debug_flag_calc_img=debug_flag_calc_img1,
                             min_speech_seconds_in_wav=min_speech_seconds_in_wav1,
                             save_rep_dir='')
    for i in range(len1):
        folder_id = dir_list[i]


        try:
            print('create imgs wav num {0} folder {1}'.format(i,folder_id))
            folder_id1 = folder_id.split('_')
            wav_name = folder_id1[0]
            working_folder = base_wav_folder + '/' + folder_id + '/orig_data'
            wav_path = working_folder + '/{0}.wav'.format(folder_id)
            print(wav_name, ' ', wav_path)
            p_id = folder_id1[2]
            p_id_path = working_folder + '/p'

            n_id = folder_id1[1]
            n_id_path = working_folder + '/n'

            label_file_path = working_folder + '/speaker_label.txt'
            if os.path.isfile(label_file_path):
                p1, n1 = create_p_n_time_list_from_label_file(label_file_path)

            else:
                msg = 'no label file in wav {0} name: {1} '.format(i, wav_name)
                print(msg)
                fid_moti_log.write(msg + '\n')
                fid_moti_log.flush()
                continue
            if len(n1)==0:
                msg = 'p label length 0 in {0} name: {1} '.format(i, wav_name)
                print(msg)
                fid_moti_log.write(msg + '\n')
                fid_moti_log.flush()
                continue


            diar.erase_folder_ifexist_and_recreate(p_id_path)
            diar.erase_folder_ifexist_and_recreate(n_id_path)


            flag, ims, ims_time = a.create_synchronize_spectrogram_librosa(wav_path)
            if not flag:
                msg = 'unabel to create spectrogram in wav {0} name: {1} '.format(i, wav_name)
                print(msg)
                fid_moti_log.write(msg + '\n')
                fid_moti_log.flush()

            #img_flag, msg = a.create_imgs_from_diarization_wav_for_training(ims, ims_time, p1, wav_name, wav_path, p_id_path)
            img_flag, msg1 = a.create_continued_imgs_from_diarization_wav_for_training(ims, ims_time, p1, wav_name, wav_path,
                                                                            p_id_path)

            if not img_flag:
                msg = 'failed create_continued_imgs p folder {0} with error {1}'.format(wav_name, msg1)
                print(msg)
                fid_moti_log.write(msg + '\n')
                fid_moti_log.flush()

            img_flag, msg1 = a.create_continued_imgs_from_diarization_wav_for_training(ims, ims_time, n1, wav_name, wav_path, n_id_path)
            if not img_flag:
                msg = 'failed create_continued_imgs n folder {0} with error {1}'.format(wav_name, msg1)
                print(msg)
                fid_moti_log.write(msg + '\n')
                fid_moti_log.flush()

        except Exception as error:
            msg ='error in wav {0} name: {1} path {2} with error {3}'.format(i,wav_name,wav_path, error)
            print(msg)
            fid_moti_log.write(msg  + '\n')
            fid_moti_log.flush()
            continue


def create_p_n_time_list(base_csv_data=None, wav_name=None):

    if base_csv_data is None:
        base_csv_data = pd.read_csv(csv_mata_file, encoding='ISO-8859-1')
    if wav_name is None:
        wav_name = 812093000864627
    info_data1 = base_csv_data.loc[base_csv_data['filename'] == wav_name]
    len1 = info_data1['filename'].shape[0]
    p_times =[]
    n_times = []
    p_times_orig =[]
    n_times_orig = []
    for i in range(len1):
        temp = info_data1.iloc[i]
        confidence = temp['confidence']

        legend = temp['legend']
        seg_start = int(temp['seg_start'])
        seg_end = int(temp['seg_end'])
        temp_t = [seg_start, seg_end]
        temp_t_orig = [temp['seg_start'], temp['seg_end']]
        if seg_end > seg_start:
            if legend == u'p':
                p_times.append(temp_t)
                p_times_orig.append(temp_t_orig)
            elif legend == u'n':
                n_times.append(temp_t)
                n_times_orig.append(temp_t_orig)

        else:
            if legend == u'p':
                p_times_orig.append(temp_t_orig)
            else:
                n_times_orig.append(temp_t_orig)
            continue
    return p_times, n_times, p_times_orig, n_times_orig



def create_p_n_time_list_from_label_file(label_file):

    label_data = read_file_by_line(label_file)
    p_times =[]
    n_times =[]

    for item in label_data:
        if item != '':
            item1 = item.split('\t')
        else:
            msg ='label data contained null line {0}'.format(base_info[0])
            print(msg)
            fid_moti_log(msg + '\n')
            continue
        ll = item1[2]
        st = int(item1[0])
        en = int(item1[1])
        time_1 = [st, en]
        if ll == 'patient':
            p_times.append(time_1)
        elif ll == 'nurse':
            n_times.append(time_1)

    return p_times, n_times


def create_wav_list(csv_mata_file1=csv_mata_file, out_pkl_wavs_list_path1=out_pkl_wavs_list_path):

    # TODO: this method only gets the labled files!! change to include all available files

    base_csv_data = pd.read_csv(csv_mata_file1, encoding='ISO-8859-1')[['filename']]
    base_csv_p_n_data = pd.read_csv(csv_p_n_file, encoding='ISO-8859-1')

    base_csv_data.drop_duplicates(subset='filename', inplace=True)
    base_csv_p_n_data = pd.merge(left=base_csv_p_n_data, right=base_csv_data, on='filename')
    base_csv_p_n_data.dropna(inplace=True)

    base_csv_p_n_data['wav_path'] = base_all_wav_path + '/' + base_csv_p_n_data.patient_id.astype(str) + '/' + base_csv_p_n_data.filename.astype(str) + '.wav'
    base_csv_p_n_data['nurse_id'] = base_csv_p_n_data.nurse_id.values.astype(int)

    base_csv_p_n_data = base_csv_p_n_data[['filename']+['wav_path']+['patient_id']+['nurse_id']]   # TODO: temp handling of ordered columns later on; need to move everything to df

    base_csv_p_n_data = base_csv_p_n_data[base_csv_p_n_data.patient_id==65056]
    print('Using only patient_id=65056')

    with open(out_pkl_wavs_list_path1, 'wb') as fp:
        pickle.dump(base_csv_p_n_data.as_matrix(), fp)

    return base_csv_p_n_data.as_matrix()


def read_file_by_line(file_path):
    tag1 = '\n'
    fid = open(file_path, 'r')
    data1 = fid.read()
    fid.close()
    data2 = data1.split(tag1)
    if data2[-1] == '':
        del data2[-1]
    return data2



def get_all_reps_from_folder(rep_folder, dvec_dim=rep_dim):

    command = rep_folder + '/*.npy'
    files_list = glob.glob(command)
    len1 = len(files_list)
    reps = np.zeros([len1, dvec_dim])
    for i in range(len1):
        reps[i] = np.load(files_list[i])
    return reps

def create_rep_from_all_folder_imgs(based_img_folder, dvec_folder_path, diarizatin_obj):
    command = based_img_folder + '/*.jpg'
    jpg_list = glob.glob(command)
    len1 = len(jpg_list)
    if len1< min_imgs_for_voice_print:
        msg = 'num img = {0}'.format(len1)
        return False, None, msg
    diarizatin_obj.create_dvecs_from_img_folder2(jpg_list, based_img_folder, dvec_folder_path)
    reps = get_all_reps_from_folder(dvec_folder_path)
    rep = np.mean(reps, axis=0)
    return True, rep, str(len1)



def create_voice_print1(img_path =img_path_p, base_print = base_dvecs, base_rep = base_ava_rep):

    a = diar.wav_diarization('', deep_speaker_obj,
                             temp_folder_path='', label_audacity_file_path='',
                             min_tresh_for_marge=min_tresh_for_marge1,
                             base_min_imgs_for_speaker=base_min_imgs_for_speaker1,
                             base_min_continue_speach=base_min_continue_speech1,
                             base_duration_for_segment=base_duration_for_segment1,
                             percentage_of_speech_per_second_tresholds=percentage_of_speech_per_second_tresholds1,
                             debug_flag_calc_img=debug_flag_calc_img1,
                             min_speech_seconds_in_wav=min_speech_seconds_in_wav1,dvec_dim=rep_dim,
                             save_rep_dir='')


    diar.erase_folder_ifexist_and_recreate(base_print)
    diar.erase_folder_ifexist_and_recreate(base_rep)

    list_dir = os.listdir(img_path)
    count = 0
    for item in list_dir:
        if item == '':
            continue
        if item == '\n':
            continue
        if '.DS_Store' in item:
            continue
        if count % 100 == 0:
            print(count)
        temp_img_dir_path = img_path + '/' + item
        dvec_temp_path = base_print + '/' + item
        try:
            print('process folder {0} num {1}'.format(item, count))
            rep_flag, rep, info1 = create_rep_from_all_folder_imgs(temp_img_dir_path, dvec_temp_path, a)
            if not rep_flag:
                print(info1)
                msg = 'problem with folder {0} num {1} in create_rep_from_all_folder_imgs {2}'.format(item, count,info1)
                print(msg)
                fid_moti_log.write(msg + '\n')
                fid_moti_log.flush()
                continue

            rep_path = base_rep + '/{0}.npy'.format(item)
            if os.path.isfile(rep_path):
                msg = 'Bug rep folder {0} all ready exist'.format(rep_folder)
                fid_moti_log.write(msg)
                fid_moti_log.flush()
                continue

            np.save(rep_path, rep)
            count += 1


        except Exception as error:
            msg ='error in img folder {0} with error {1}'.format(item, error)
            print(msg)
            fid_moti_log.write(msg  + '\n')
            fid_moti_log.flush()
            continue
    print('total dvec ={0}'.format(count))


if 1:
    fid_moti_log = open(moti_log_file, 'w')

    if 1:
        create_wav_list()
    if 1:
        main_preper_n_p_full_imgs()

    if 1:
        create_voice_print1(img_path=base_macabi_temp_train + '/p', base_print=base_dvecs + '/p', base_rep=base_ava_rep + '/p')
        create_voice_print1(img_path=base_macabi_temp_train + '/n', base_print=base_dvecs + '/n', base_rep=base_ava_rep + '/n')




    fid_moti_log.close()



