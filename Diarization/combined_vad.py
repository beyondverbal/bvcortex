from bob.bio.spear.preprocessor import Energy_2Gauss
import scipy.io.wavfile as wav
import os
import random
import numpy as np
import scipy
from aubio import source, pitch
import aubio



def smoothing(labels, smoothing_window):

    if np.sum(labels)< smoothing_window:
        return labels
    segments = []
    for k in range(1,len(labels)-1):
        if labels[k]==0 and labels[k-1]==1 and labels[k+1]==1 :
            labels[k]=1
    for k in range(1,len(labels)-1):
        if labels[k]==1 and labels[k-1]==0 and labels[k+1]==0 :
            labels[k]=0

    seg = np.array([0,0,labels[0]])
    for k in range(1,len(labels)):
        if labels[k] != labels[k-1]:
            seg[1]=k-1
            segments.append(seg)
            seg = np.array([k,k,labels[k]])
    seg[1]=len(labels)-1
    segments.append(seg)

    if len(segments) < 2:
        return labels

    curr = segments[0]
    next = segments[1]

  # Look at the first segment. If it's short enough, just change its labels
    if (curr[1]-curr[0]+1) < smoothing_window and (next[1]-next[0]+1) > smoothing_window:
        if curr[2]==1:
            labels[curr[0] : (curr[1]+1)] = np.zeros(curr[1] - curr[0] + 1)
            curr[2]=0
        else: #curr[2]==0
            labels[curr[0] : (curr[1]+1)] = np.ones(curr[1] - curr[0] + 1)
            curr[2]=1

    for k in range(1,len(segments)-1):
        prev = segments[k-1]
        curr = segments[k]
        next = segments[k+1]

        if (curr[1]-curr[0]+1) < smoothing_window and (prev[1]-prev[0]+1) > smoothing_window and (next[1]-next[0]+1) > smoothing_window:
            if curr[2]==1:
                labels[curr[0] : (curr[1]+1)] = np.zeros(curr[1] - curr[0] + 1)
                curr[2]=0
            else: #curr[2]==0
                labels[curr[0] : (curr[1]+1)] = np.ones(curr[1] - curr[0] + 1)
                curr[2]=1

    prev = segments[-2]
    curr = segments[-1]

    if (curr[1]-curr[0]+1) < smoothing_window and (prev[1]-prev[0]+1) > smoothing_window:
        if curr[2]==1:
            labels[curr[0] : (curr[1]+1)] = np.zeros(curr[1] - curr[0] + 1)
            curr[2]=0
        else: #if curr[2]==0
            labels[curr[0] : (curr[1]+1)] = np.ones(curr[1] - curr[0] + 1)
            curr[2]=1

    return labels

def create_vad_seg_file(np_time_seg, vad_file_path):
    fid = open(vad_file_path, 'w')
    dict = {}
    dict['0'] = 'noise'
    dict['1'] = 'speech'
    len2 = len(np_time_seg)
    for i in range(len2 - 1):
        if np_time_seg[i][2] == 1:
            msg = '{0}\t{1}\t{2}\n'.format(np_time_seg[i][0], np_time_seg[i][1], dict[str(int(np_time_seg[i][2]))])
            fid.write(msg)
    if np_time_seg[len2 - 1][2] == 1:
        msg = '{0}\t{1}\t{2}'.format(np_time_seg[len2 - 1][0], np_time_seg[len2 - 1][1], dict[str(int(np_time_seg[len2 - 1][2]))])
        fid.write(msg)
    fid.close()

def create_vad_labels(labels, time_step=0.01, vad_file_path=''):

    time_seg =[]
    len1 = len(labels)
    seg =[0,0,labels[0]]
    for i in range(1,len1-1):

        if labels[i] != labels[i-1]:
            seg[1] = i*time_step
            time_seg.append(seg)
            seg = [i*time_step, 0, labels[i]]

    seg[1] = (len1-1) * time_step
    time_seg.append(seg)
    np_time_seg = np.array(time_seg)
    if vad_file_path != '':
        create_vad_seg_file(np_time_seg, vad_file_path)
    return np_time_seg



def create_vad(wav_path, smoothing_window1=1,
               percentage_of_speech_per_second_tresholds=.35,
               time_resolution=1.0,
               vad_label_path=''):

    num_segments_in_frame = int(time_resolution *100)
    time_resolution1 = num_segments_in_frame/100.


    vad_bob = Energy_2Gauss(smoothing_window=smoothing_window1)
    fs, samples = wav.read(wav_path)

    data1 = samples.astype(np.float64)

    len1 = data1.shape[0]
    noise = np.random.uniform(low=-1.0, high=1.0, size=len1)
    data1 += noise

    input_sig = [fs, data1]
    r, d, labels = vad_bob.__call__(input_sig)
    orig_len_wav = len(labels)
    num_frames = int(orig_len_wav/num_segments_in_frame)
    new_len = num_frames*num_segments_in_frame
    np_labels = labels[:new_len]
    np_labels1 = np_labels.reshape([num_frames, num_segments_in_frame])
    np_labels2 = np_labels1.mean(axis=1)
    np_labels3 = np.where(np_labels2 > percentage_of_speech_per_second_tresholds, 1, 0)
    energy_seg = create_vad_labels(np_labels3, time_step=time_resolution1, vad_file_path=vad_label_path)


    return energy_seg, np_labels3


if __name__ == "__main__":


    wav_path = '/media/moti/motiHD2/tesing/temp/test_vad/song.wav'
    temp_path = '/media/moti/motiHD2/tesing/temp/tmp_all'
    vad_label_path1 = '/media/moti/motiHD2/tesing/temp/test_vad/vad_label.txt'
    pitch_label_path1 = '/media/moti/motiHD2/tesing/temp/test_vad/pitch_label.txt'
    combiend_label_path1 = '/media/moti/motiHD2/tesing/temp/test_vad/combiend_label.txt'
    vad_label_path1 = '/media/moti/motiHD2/tesing/temp/test_vad/vad_label.txt'
    g_label_path1 = '/media/moti/motiHD2/tesing/temp/test_vad/vad_label_g.txt'
    smoothing_window1 =1
    pitch_tresholds_low1 =20
    pitch_tresholds_high1 = 4000
    percentage_of_speech_per_second_tresholds1 = 0.5
    time_resolution1 = 1
    vad_aggressive1=3




    if 1:
        seg, base_label = create_vad(wav_path, smoothing_window1=smoothing_window1,
               percentage_of_speech_per_second_tresholds=percentage_of_speech_per_second_tresholds1,
               time_resolution=time_resolution1,
               vad_label_path=vad_label_path1)

    b=1
