from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import logging


import tensorflow as tf
import numpy as np
import time
import sys
import os
import facenet
import pickle
import glob
from tensorflow.python.platform import gfile



class main_facenet_speaker_class:

    def __init__(self, speakerModelName, speakerModelPath, load_model_flag):

        self.rootPath = os.path.dirname(os.path.realpath(__file__))
        self.model_name = speakerModelName
        self.model_full_path = speakerModelPath

        graph1 = tf.Graph()

        # self.sess = tf.Session()
        self.sess = tf.Session(graph=graph1)
        with graph1.as_default():

            # Load the model
            str1 = 'load face model {0}'.format(self.model_name)
            print(str1)
            logging.info(str1)
            if load_model_flag:

                f = gfile.FastGFile(self.model_full_path, 'rb')
                str1 = 'end load face model'
                print(str1)
                logging.info(str1)

                graph_def = tf.GraphDef()
                graph_def.ParseFromString(f.read())
                tf.import_graph_def(graph_def, name='')
                str1 = 'end arange face model'
                print(str1)
                logging.info(str1)

                self.images_placeholder = graph1.get_tensor_by_name("input:0")
                self.embeddings = graph1.get_tensor_by_name("embeddings:0")
                self.phase_train_placeholder = graph1.get_tensor_by_name("phase_train:0")
                graph1.finalize()

                self.image_size = 160
                self.embedding_size = self.embeddings.get_shape()[1]
                str1 = 'facenet image_size = {0}'.format(self.image_size)
                str2 = 'facenet embedding_size = {0}'.format(self.embedding_size)
                print(str1)
                print(str2)
                logging.info(str1)
                logging.info(str2)


    def get_rep_from_img(self, image):

        feed_dict = {self.images_placeholder: image, self.phase_train_placeholder: False}
        rep = self.sess.run(self.embeddings, feed_dict=feed_dict)
        nan_in_rep = np.isnan(rep)
        qq = True
        str1 = 'rep  sahape (getRep) = {0}'.format(rep.shape)
        print(str1)
       # logging.info(str1)
        if qq not in nan_in_rep:
            msg = 'rep not contain Nan'
            print(msg)
            #logging.info(msg)
            return True, rep, msg
        else:
            msg = 'rep contain Nan'
            print(msg)
            #logging.info(msg)

            return False, None, msg




 
