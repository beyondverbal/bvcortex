import diarization_engine as diar
import main_facenet_speaker_class
import pandas as pd
import pickle
import shutil
import numpy as np
np.set_printoptions(suppress=True)
import os
import glob
import random
import datetime
import time
import traceback


macabi_model = True

if macabi_model:
    # speakerModelPath = '/home/ds.mkm-research.org.il/moti.halpert/nvvp_workspace/moti/src/facenet-master/model_macabi/20180428-121839/model-20180428-121839.pb'
    speakerModelPath = '/media/algo/Nimrod/Diarization/facenet_models/20180428-121839/model-20180428-121839.pb'
    speakerModelName = 'model-20180428-121839.pb'
else:
    # speakerModelPath = '/home/ds.mkm-research.org.il/moti.halpert/nvvp_workspace/moti/src/facenet-master/models_beyond/20180129-161547/model-20180129-161547.pb'
    speakerModelPath = '/media/algo/Nimrod/Diarization/facenet_models/20180129-161547/model-20180129-161547.pb'
    speakerModelName = 'model-20180129-161547.pb'

delete_diarization_img_flag1 = True # False #
diar_algo_tresh_high = 0.5
diar_algo_tresh_low = 0.2
min_tresh_for_marge1 = 0.75
base_min_imgs_for_speaker1 = 10
base_min_continue_speech1 = 3
base_duration_for_segment1 = 80
# pitch_tresholds1 = 20
percentage_of_speech_per_second_tresholds1 = 0.5
# smoothing_window1 = 10
min_speech_seconds_in_wav1 = 30
min_imgs_for_voice_print = 6
label_dic ={"0": 'patient', "1": 'nurse', "2": "both","3": 'none' }

algo_path = '/media/algo/nimrod/Debug/Diarization/'
base_data_path = '/media/winshares01'

csv_mata_file = '/media/algo/dat/MetaData/SegmentsDataAll180827.csv'
csv_p_n_file = '/media/algo/dat/MetaData/FileNursePatient180827.csv'

out_pkl_base_un_segment = algo_path + 'un_segment.pkl'
out_txt_base_un_segment = algo_path + 'un_segment.txt'
patient_list_path = algo_path + 'patient_list.csv'

temp_path = algo_path + 'temp'
save_rep_speakers_dir1 = temp_path + '/tmp_speakers_reps'
wavs_folder = temp_path + '/orig_data'
lable_audacity_file_path = wavs_folder + '/speaker_label.txt'
lable_audacity_file_path_algo = wavs_folder + '/speaker_label_algo.txt'

moti_log_file = '/media/algo/nimrod/Debug/Diarization/log_file.txt'
fid_moti_log = open(moti_log_file, 'w')
# base_macabi_temp_train = temp_path + '/img/macabi_train_temp'
# base_macabi_temp_dvec = temp_path + '/dvec/macabi_train_temp'
base_macabi_temp_train = algo_path + 'img'
base_macabi_temp_dvec = algo_path + 'dvec'

base_macbi_voice_prints = algo_path + 'dvec/voice_print'   # TODO: reproduce this 'locally'
# base_macbi_voice_prints = '/home/ds.mkm-research.org.il/moti.halpert/nvvp_workspace/moti/data/dvec/voice_print'   # TODO: reproduce this 'locally'
dvec_temp_path = algo_path + 'dvec/temp'
base_p_list_path = base_macbi_voice_prints + '/p_list.txt'
base_n_list_path = base_macbi_voice_prints + '/n_list.txt'
base_macabi_un_segment_folder = algo_path + 'macabi_un_segment'

all_wav_wav_path = algo_path + 'macabi_un_segment'
# all_wav_wav_path = algo_path + 'img'
csv_all_file_path = algo_path + 'csv_debug.csv'
debug_flag_calc_img1 = True

if debug_flag_calc_img1:
    deep_speaker_obj = main_facenet_speaker_class.main_facenet_speaker_class(speakerModelName, speakerModelPath, True)
else:
    deep_speaker_obj = None
create_wav_list_flag = True






def get_speaker_reps(spekers_reps_folder_path, dvec_dim=128, spec_length_time_size=160):

    dir_list = os.listdir(spekers_reps_folder_path)
    len1 = len(dir_list)
    if len1==0:
        return False, None, None

    speaker_names = []
    speaker_reps = []
    count = 0
    for i in range(len1):
        dir11 = dir_list[i]
        if not check_dir_item(dir11):
            continue
        rep_file_path = spekers_reps_folder_path + '/' + dir11 + '/rep.txt'
        nnn = dir11.replace('speaker', 's')
        tt = np.loadtxt(rep_file_path)
        speaker_reps.append(tt)
        speaker_names.append(nnn)
        count +=1
    if count> 0:
        speaker_reps1 = np.array(speaker_reps)
        return True, speaker_names, speaker_reps1
    else:
        return False, None, None





def corespond_speakers_to_n_p( score_tabel, n_p_score, speakers_names, diar_algo_tresh_high = 0.37, diar_algo_tresh_low=0.20):
    tresh = diar_algo_tresh_high
    tresh_low = np.maximum(diar_algo_tresh_low + n_p_score, diar_algo_tresh_low)
    if n_p_score>tresh:
        msg = 'moti print error: nurse patiant score higher then treshold'
        return False, None, msg
    len1 = score_tabel.shape[0]
    if len1==0:
        msg = 'length of score tabel is zeros'
        return False, None, msg

    corespond = []
    for i in range(len1):
        s1 = speakers_names[i]

        temp_scores = score_tabel[i]
        s_p = temp_scores[0]
        s_n = temp_scores[1]
        if s1=='noise':
            temp = [s1, 'noise', s_p]
            corespond.append(temp)
            continue
        c1 = s_p > tresh and s_n< tresh_low
        #c1 = s_p > tresh and s_n< tresh
        if c1:
            temp =[s1, 'patient', s_p]
            corespond.append(temp)
            continue
        c2 = s_n > tresh and s_p< tresh_low
        #c2 = s_n > tresh and s_p< tresh
        if c2:
            temp =[s1, 'nurse', s_n]
            corespond.append(temp)
            continue
        c3 = s_n > tresh and s_p> tresh_low or s_p > tresh and s_n > tresh_low
 #       c3 = s_n > tresh and s_p> tresh

        if c3:
            temp =[s1, 'both', s_p]
            corespond.append(temp)
            continue
        temp = [s1, 'other', s_p]
        corespond.append(temp)
    return True, corespond, ''




def write_label_data2_new_labelfile(orig_label_path, new_label_path, corespond_tabel):

    label_speakers = []
    ids =[]
    scores = []
    for item in corespond_tabel:
        label_speakers.append(item[0])
        ids.append(item[1])
        scores.append(item[2])
    old_labels = read_file_by_line(orig_label_path)
    fid1 = open(new_label_path, 'w')
    label_data = []
    for item1 in old_labels:
        item = item1.split('\t')
        st = item[0]
        en = item[1]
        speaker1 = item[2]
        if speaker1=='noise':
            speaker='no'
            sco = -6.0
            r_id = 'no'
        else:
            speaker = speaker1.replace('speaker', 's')
            ind = label_speakers.index(speaker)
            sco = scores[ind]
            r_id = ids[ind]
        if r_id == 'patient' or r_id == 'nurse' or r_id=='no':
            temp = [st, en, r_id, sco]
        else:
            if r_id=='both':
                temp = [st, en, 'b', sco]
            else:
                temp = [st, en, speaker, sco]
        label_data.append(temp)
        msg = '{0}\t{1}\t{2}\n'.format(temp[0], temp[1], temp[2])
        fid1.write(msg)

    fid1.close()
    return label_data




def write_label_diarization_csv(label_data, csv_path, p_id, n_id, wav_name):
    labels = ['filename' ,'nurse_id' ,'patient_id' ,'seg_start', 'seg_end','legend', 'confidence', 'timestamp']
    len1 = len(label_data)
    base_data = []
    for i in range(len1):
        ff = label_data[i]
        tt = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        if ff[2]=='patient':
            temp = [wav_name, n_id, p_id, ff[0], ff[1], 'p', ff[3], tt]
        else:
            if ff[2] =='nurse':
                temp = [ wav_name, n_id, p_id, ff[0], ff[1], 'n', ff[3], tt]
            else:
                if ff[2] == 'b':
                    temp = [wav_name, n_id, p_id, ff[0], ff[1], 'b', ff[3], tt]
                else:
                    temp = [wav_name, n_id, p_id, ff[0], ff[1], ff[2], ff[3], tt]
        base_data.append(temp)
    df = pd.DataFrame.from_records(base_data, columns=labels)
    df.to_csv(csv_path, index=False)







def main_cat_speaker_un_segment(copy_wavs_flag=False, start_from_begining_flag=True):

    with open(out_pkl_base_un_segment, 'rb') as fp:
        wavs_list = pickle.load(fp)
    if start_from_begining_flag:
        diar.erase_folder_ifexist_and_recreate(base_macabi_un_segment_folder)
        p_id_list=[]
    else:
        p_id_list = os.listdir(base_macabi_un_segment_folder)

    len1 = len(wavs_list)
    msg = 'length wav script is {0}'.format(len1)
    print(msg)
    fid_moti_log.write(msg + '\n')
    fid_moti_log.flush()

    for i in range(len1):
        try:
            base_wav_info = wavs_list[i]
            wav_name = str(base_wav_info[0])
            wav_path = base_wav_info[1]
            msg = 'start process wav {0} {1}'.format(i, wav_path)
            print(msg)
            fid_moti_log.write(msg + '\n')
            fid_moti_log.flush()

            p_id = base_wav_info[2]
            n_id = base_wav_info[3]
            p_rep_path = base_wav_info[4]
            n_rep_path = base_wav_info[5]

            if not os.path.isfile(p_rep_path):

                msg = 'Error: no rep for p {0}'.format(p_rep_path)
                print(msg)
                fid_moti_log.write(msg + '\n')
                fid_moti_log.flush()
                continue
            if not os.path.isfile(n_rep_path):
                msg = 'Error: no rep for n {0}'.format(n_rep_path)
                print(msg)
                fid_moti_log.write(msg + '\n')
                fid_moti_log.flush()
                continue

            rep_p = np.load(p_rep_path)
            rep_n = np.load(n_rep_path)
            n_p_score = diar.calc_correlation_score(rep_p, rep_n)


            p_id_folder  = base_macabi_un_segment_folder + '/' + str(p_id)
            new_wav_folder = base_macabi_un_segment_folder + '/' + str(p_id) + '/' + wav_name
            if p_id not in p_id_list:
                os.makedirs(p_id_folder)
                p_id_list.append(p_id)
            if os.path.exists(new_wav_folder):
                continue
            os.makedirs(new_wav_folder)
            if copy_wavs_flag:
                new_wav_path = new_wav_folder  +'/' + wav_name + '.wav'
                shutil.copy2(wav_path, new_wav_path)
                wav_path = new_wav_path

            lable_audacity_file_path = new_wav_folder + '/speaker_label.txt'
            csv_path =  new_wav_folder + '/speaker_label.csv'
            save_rep_speakers_dir1 = new_wav_folder + '/dvec'
            lable_audacity_after_identification = new_wav_folder + '/speaker_label1.txt'
            score_orig_n_p_path = new_wav_folder + '/score_p_n_otig.txt'

            a = diar.wav_diarization(wav_path, deep_speaker_obj,
                                     temp_folder_path=new_wav_folder, label_audacity_file_path=lable_audacity_file_path,
                                     min_tresh_for_marge=min_tresh_for_marge1,
                                     base_min_imgs_for_speaker=base_min_imgs_for_speaker1,
                                     base_min_continue_speach=base_min_continue_speech1,
                                     base_duration_for_segment=base_duration_for_segment1,
                                     percentage_of_speech_per_second_tresholds=percentage_of_speech_per_second_tresholds1,
                                     debug_flag_calc_img=debug_flag_calc_img1,
                                     min_speech_seconds_in_wav=min_speech_seconds_in_wav1,
                                     save_rep_dir=save_rep_speakers_dir1,
                                     delete_diarization_img_flag=delete_diarization_img_flag1)



            flag_diarization, data, vad_label, diar_msg = a.diarization()
            if not flag_diarization:
                print(diar_msg)
                fid_moti_log.write(diar_msg + '\n')
                fid_moti_log.flush()
                continue

            frame_scores = a.compare_orig_with_n_p(rep_p, rep_n)
            len4 = len(frame_scores)
            fid_13 = open(score_orig_n_p_path, 'w')

            for i in range(len4):
                msg = '{0}, against_p={1}, against_n={2}'.format(frame_scores[i][2], frame_scores[i][0], frame_scores[i][1])
                fid_13.write(msg + '\n')
            fid_13.close()



            if flag_diarization:

                flag, speakers_name, reps1 = get_speaker_reps(save_rep_speakers_dir1)
                if flag:
                    len11 = reps1.shape[0]
                    score_speakers = np.zeros([len11, 2])
                    score_speakers_list =[]
                    for i in range(len11):
                        s_id = speakers_name[i]
                        tt = diar.calc_correlation_score(rep_p, reps1[i])
                        s1 = '{0}_against_p={1}'.format(s_id, tt)
                        score_speakers[i, 0] = tt
                        tt = diar.calc_correlation_score(rep_n, reps1[i])
                        score_speakers[i, 1] = tt
                        s2 = '{0}_against_n={1}'.format(s_id, tt)
                        temp=[s1, s2]
                        score_speakers_list.append(temp)
                    coresponds_flag, coresponds, msg1 = corespond_speakers_to_n_p( score_speakers, n_p_score, speakers_name, diar_algo_tresh_high = 0.37, diar_algo_tresh_low=0.20)
                    if not coresponds_flag:

                        print(msg1)
                        fid_moti_log.write(msg1 + '\n')
                        fid_moti_log.flush()
                        continue

                    score_path = new_wav_folder + '/score.txt'
                    fid = open(score_path, 'w')
                    msg = 'p_against_n={0}'.format(n_p_score)
                    fid.write(msg + '\n')
                    for vv in range(score_speakers.shape[0]):
                        msg = '{0},\t{1}\n'.format(score_speakers_list[vv][0], score_speakers_list[vv][1])
                        fid.write(msg)
                    fid.close()

                    label_data = write_label_data2_new_labelfile(lable_audacity_file_path, lable_audacity_after_identification, coresponds)
                    write_label_diarization_csv(label_data, csv_path, p_id, n_id, wav_name)
                else:
                    msg ='moti print error: no reps found in get_speaker_reps'
                    print(msg)
                    fid_moti_log.write(msg + '\n')
                    fid_moti_log.flush()
                    continue


        except Exception as error:
            tb = traceback.format_exc()
            #print tb
            msg ='error in wav {0} name: {1} path {2} with error {3} traceback {4}'.format(i,wav_name,wav_path, error, tb)
            print(msg)
            fid_moti_log.write(msg  + '\n')
            fid_moti_log.flush()
            continue

    return None



def read_file_by_line(file_path):
    tag1 = '\n'
    fid = open(file_path, 'r')
    data1 = fid.read()
    fid.close()
    data2 = data1.split(tag1)
    if data2[-1] == '':
        del data2[-1]
    return data2

def create_avaliabale_p_n_list():
    fid_p = open(base_p_list_path, 'w')
    fid_n = open(base_n_list_path, 'w')
    p_base_print =base_macbi_voice_prints + '/p'
    list_dir = os.listdir(p_base_print)
    for item in list_dir:
        if not check_dir_item(item):
            continue
        fid_p.write('{0}\n'.format(item))
    fid_p.close()
    n_base_print =base_macbi_voice_prints + '/n'
    list_dir = os.listdir(n_base_print)
    for item in list_dir:
        if not check_dir_item(item):
            continue
        fid_n.write('{0}\n'.format(item))
    fid_n.close()


def create_list_for_unsegments_wavs():

    base_csv_p_n_data = pd.read_csv(csv_p_n_file, encoding='ISO-8859-1')
    list_p = read_file_by_line(base_p_list_path)
    list_n = read_file_by_line(base_n_list_path)
    segment_data = base_csv_p_n_data.loc[:, ['filename','patient_id','nurse_id']]
    len1 = segment_data.shape[0]
    info_list = []
    count = 0
    for i in range(len1):
        if i % 1000 == 0:
            print(i)
        line_data = segment_data.iloc[i]
        wav_name = line_data['filename']
        patient_id = str(line_data['patient_id'])
        nurse_id = str(line_data['nurse_id'])
        if patient_id not in list_p:
            continue
        if nurse_id not in list_n:
            continue
        temp_path_p = base_macbi_voice_prints + '/p/{0}/{0}.npy'.format(patient_id)
        temp_path_n = base_macbi_voice_prints + '/n/{0}/{0}.npy'.format(nurse_id)

        wav_path = base_data_path + '/{0}/{1}.wav'.format(patient_id, wav_name)
        if not os.path.isfile(wav_path):
            msg = 'wav name {0} not exist'.format(wav_path)
            print(msg)
            fid_moti_log.write(msg + '\n')
            fid_moti_log.flush()
        temp = [wav_name, wav_path, patient_id, nurse_id,  temp_path_p, temp_path_n]
        info_list.append(temp)
        count +=1

    print('number of wav file is {0}'.format(count))
    with open(out_pkl_base_un_segment, 'wb') as fp:
        pickle.dump(info_list, fp)
    fid = open(out_txt_base_un_segment, 'w')
    for item in info_list:
        msg = '{0},{1},{2},{3},{4},{5}'.format(item[0], item[1], item[2], item[3], item[4], item[5])
        fid.write(msg + '/n')
    fid.close()


def create_list_for_unsegments_patient():

    base_csv_p_n_data = pd.read_csv(csv_p_n_file, encoding='ISO-8859-1')
    segment_data = base_csv_p_n_data.loc[:, ['filename','patient_id','nurse_id']]
    len1 = segment_data.shape[0]
    base_csv_patient_list = pd.read_csv(patient_list_path, encoding='ISO-8859-1')
    segment_patient_list =base_csv_patient_list.loc[:, ['patient_id']]
    len2 = segment_patient_list.shape[0]
    info_list = []
    count = 0
    for i in range(len2):
        if i % 1000 == 0:
            print(i)
        line_data = segment_patient_list.iloc[i]
        patient_id = line_data['patient_id']
        ids_wavs = segment_data.loc[segment_data['patient_id']==patient_id]
        temp_path_p = base_macbi_voice_prints + '/p/{0}.npy'.format(patient_id)
        temp_path_p_flag = not os.path.isfile(temp_path_p)
        if temp_path_p_flag:
            msg = 'patient {0}: no patient rep'.format(patient_id)
            print(msg)
            fid_moti_log.write(msg + '\n')
            fid_moti_log.flush()
            continue
        print(patient_id)
        if 2087001==patient_id:
            patient_id=patient_id

        for idx in ids_wavs.index:  # TODO: maybe change to this whenever using .iloc[k] in the code  (avoids int-float trunc bug)
            nurse_id = int(ids_wavs.loc[idx,'nurse_id'])  # TODO: sort-out the types for id's and filenames - int/float/str
            wav_name = ids_wavs.loc[idx, 'filename']


            temp_path_n = base_macbi_voice_prints + '/n/{0}.npy'.format(nurse_id)
            temp_path_n_flag = not os.path.isfile(temp_path_n)
            wav_path = base_data_path + '/{0}/{1}.wav'.format(patient_id, wav_name)
            wav_path_flag = not os.path.isfile(wav_path)

            if wav_path_flag or temp_path_n_flag:
                msg = 'wav name {0}: wav flag = {1}, nurse flag = {2}'.format(wav_path,wav_path_flag, temp_path_n_flag)
                print(msg)
                fid_moti_log.write(msg + '\n')
                fid_moti_log.flush()
            temp = [wav_name, wav_path, patient_id, nurse_id,  temp_path_p, temp_path_n]
            info_list.append(temp)
            count +=1

    print('number of wav file is {0}'.format(count))
    with open(out_pkl_base_un_segment, 'wb') as fp:
        pickle.dump(info_list, fp)
    fid = open(out_txt_base_un_segment, 'w')
    for item in info_list:
        msg = '{0},{1},{2},{3},{4},{5}'.format(item[0], item[1], item[2], item[3], item[4], item[5])
        fid.write(msg + '/n')
    fid.close()



def create_csv_all(csv_all_file, base_folder):

    list_dir = os.listdir(base_folder)
    frams_= pd.DataFrame()
    list1 = []
    folder_counter =0
    for item in list_dir:
        if not check_dir_item(item):
            continue
        file1 = base_folder + '/' + item + '/orig_data/speaker_label.csv'
        if os.path.isfile(file1):
            df = pd.read_csv(file1, index_col=None, header=0)
            list1.append(df)
        else:
            pass

    frams_ = pd.concat(list1)
    frams_.to_csv(csv_all_file, index=False)



def create_all_patient_csv(csv_all_file, base_folder):

    list_dir = os.listdir(base_folder)
    frams_= pd.DataFrame()
    list1 = []
    count_csv_files = 0
    for item in list_dir:
        if not check_dir_item(item):
            continue
        pat = base_folder + '/' + item
        temp_list = os.listdir(pat)
        for wav_name in temp_list:
            wav_folder = pat + '/' + wav_name
            command = wav_folder + '/*.csv'
            csv_list = glob.glob(command)
            len1 = len(csv_list)
            if len1==0:
                msg = 'paitent {0} wav {1} has no csv'.format(item, wav_name)
                fid_moti_log.write(msg + '\n')
                fid_moti_log.flush()
                continue
            if len1>1:
                msg = 'Error: paitent {0} wav {1} has {2} csv (should be noly one)'.format(item, wav_name, len1)
                fid_moti_log.write(msg + '\n')
                fid_moti_log.flush()
                continue

            file1 = wav_folder + '/speaker_label.csv'
            if os.path.isfile(file1):
                df = pd.read_csv(file1, index_col=None, header=0)
                list1.append(df)
                count_csv_files +=1
                if count_csv_files % 500 == 0:
                    print(count_csv_files)
            else:
                msg = 'Error: paitent {0} wav {1} is not a file'.format(item, wav_name)
                fid_moti_log.write(msg + '\n')
                fid_moti_log.flush()
                continue
    print('total files number = {0}'.format(count_csv_files))
    frams_ = pd.concat(list1)
    frams_.to_csv(csv_all_file, index=False)


def check_dir_item(item):
    is_valid = True
    if item == '':
        is_valid = False
    if item == '\n':
        is_valid = False
    if '.DS_Store' in item:
        is_valid = False
    return is_valid




# if __name__ == '__main__':

import cProfile
import pstats
pr = cProfile.Profile()
pr.enable()


start_time = time.time()

if 1:
    # TODO: check that this works!
    create_avaliabale_p_n_list()
if 0:
    create_list_for_unsegments_wavs()
if 1:
   create_list_for_unsegments_patient()
if 1:
   main_cat_speaker_un_segment()
if 1:
   create_all_patient_csv(csv_all_file_path, all_wav_wav_path)
#create_csv_all(csv_all_file_path, all_wav_wav_path)
fid_moti_log.close()
print('Done in {} minutes.'.format(np.round((time.time() - start_time) / float(60), decimals=2)))

pr.disable()
stats = pstats.Stats(pr)
stats.sort_stats('cumtime').print_stats(20)



