import logging
from collections import Counter

import numpy as np
import pandas as pd
from TAVEngine import HLDConfig as HLDCfg
from TAVEngine.MLEvaluation import MLEvaluation


class DetectorPredict:

    def __init__(self):
        self.PRED = MLEvaluation()
        self.result_list = []
        self.all_tav = ['temper', 'arousal', 'valence']
        self.all_classes = ['high', 'low']
        self.logger = logging.getLogger('DetectorPredict')
        self.list_dict = {}
        self.enforce_adaptation = False

        self.result_df_new = pd.DataFrame([])  # todo will replace all other results, now used after matrix_calc

    def clean_up(self):

        self.result_list = []
        self.result_df_new = pd.DataFrame([])

    def predict_from_hld_matrix(self, feature_matrix, vad_vec, ts, th_dict=HLDCfg.DETECTOR_THs):

        out_df = pd.DataFrame([])
        # out_df = out_df.assign(end=ts+HLDCfg.SEGMENT_LENGTH, duration=HLDCfg.SEGMENT_LENGTH)

        if feature_matrix.shape[0] > 0:
            feat_no_nan = np.logical_not(np.any(np.isnan(feature_matrix), axis=1))
            out_df = out_df.assign(do_predict=np.logical_and((vad_vec >= HLDCfg.VAD_THRESHOLD), feat_no_nan))

            predict_vector = self.PRED.predict_matrix(feature_matrix, out_df.do_predict.values, th_dict)

            for tav in self.all_tav:
                out_df = pd.concat([out_df, predict_vector[tav + '_tav']], axis=1,)
            out_df = pd.concat([out_df, predict_vector['vitality']], axis=1, )

            if HLDCfg.ENFORCE_ALLOWED_COMB:  # TODO please remove this sometime

                out_df.assign(temper_pred_new=0, arousal_pred_new=0, valence_pred_new=0,
                              temper_conf_new=0, arousal_conf_new=0, valence_conf_new=0)

                for jj in range(0, out_df.shape[0]):

                    comb_conf = {'temper': out_df.loc[jj, 'temper_score'], 'arousal': out_df.loc[jj, 'arousal_score'],
                                 'valence': out_df.loc[jj, 'valence_score']}
                    comb_dist = {'temper': 1, 'valence': 1, 'arousal': 1}
                    comb_pred = {'temper': out_df.loc[jj, 'temper_predict'], 'arousal': out_df.loc[jj, 'arousal_predict'],
                                 'valence': out_df.loc[jj, 'valence_predict']}

                    if np.any(out_df.iloc[jj, :].copy().isnull()):
                        print('jj = {} SKIPPING ******************'.format(jj))
                        continue

                    t_pred, a_pred, v_pred, t_score, a_score, v_score = self.verify_allowed_tav_comb(comb_pred, comb_dist, comb_conf)
                    out_df.loc[jj, 'temper_pred_new'] = t_pred
                    out_df.loc[jj, 'arousal_pred_new'] = a_pred
                    out_df.loc[jj, 'valence_pred_new'] = v_pred
                    out_df.loc[jj, 'temper_conf_new'] = t_score
                    out_df.loc[jj, 'arousal_conf_new'] = a_score
                    out_df.loc[jj, 'valence_conf_new'] = v_score

            self.result_df_new = out_df.copy()

        return out_df

    @staticmethod
    def verify_allowed_tav_comb(curr_comb_pred, curr_comb_dist, curr_comb_conf):

        def name2num(x):
            return HLDCfg.toNumDic[HLDCfg.classesDic[x]] / 50

        comb = HLDCfg.allowed_comb

        diff_size = list()
        total_dec_diff = list()
        total_prob_diff = list()
        for ii in comb.index:
            pred_diff = list()
            dec_diff = list()
            prob_diff = list()
            for jj in curr_comb_pred.keys():
                curr = curr_comb_pred[jj]
                # proposed = comb.get_value(ii, jj)
                proposed = comb.loc[ii, jj]
                if jj == 'temper':
                    if curr != proposed:
                        pred_diff.append(2)
                    else:
                        pred_diff.append(0)
                else:
                    pred_diff.append(np.abs(name2num(curr) - name2num(proposed)))

                dec_diff.append(8)
                prob_diff.append((curr != proposed)*curr_comb_conf[jj])

            pred_diff = np.array(pred_diff)
            diff_size.append(np.sum(pred_diff))
            total_dec_diff.append(np.sum(dec_diff))
            total_prob_diff.append(np.sum(prob_diff))

        all_replace_cost = np.vstack((np.expand_dims(diff_size, axis=0),
                                      np.expand_dims(total_dec_diff, axis=0),
                                      np.expand_dims(total_prob_diff, axis=0)))
        all_replace_cost = np.divide(all_replace_cost, np.max(all_replace_cost, axis=1)[:, None])

        replace_cost_weight = HLDCfg.replace_cost_weight
        replace_cost_weight_vec = list()
        for ii in ['diff_size','total_dec_diff','total_prob_diff']:
            replace_cost_weight_vec.append(replace_cost_weight[ii])
        total_replace_cost = np.dot(np.expand_dims(replace_cost_weight_vec, axis=0), all_replace_cost)

        selected_comb = comb.loc[str(np.argsort(total_replace_cost)[0][0])]
        temperPred = selected_comb['temper']
        arousalPred = selected_comb['arousal']
        valencePred = selected_comb['valence']

        # If replaced, change confidence to minimum
        temperConf = curr_comb_conf['temper']
        arousalConf = curr_comb_conf['arousal']
        valenceConf = curr_comb_conf['valence']
        if temperPred != curr_comb_pred['temper'] and temperConf >= HLDCfg.CONFIDENCE_THRESHOLD:
            temperConf = HLDCfg.CONFIDENCE_THRESHOLD
        if arousalPred != curr_comb_pred['arousal'] and arousalConf >= HLDCfg.CONFIDENCE_THRESHOLD:
            arousalConf = HLDCfg.CONFIDENCE_THRESHOLD
        if valencePred != curr_comb_pred['valence'] and valenceConf >= HLDCfg.CONFIDENCE_THRESHOLD:
            valenceConf = HLDCfg.CONFIDENCE_THRESHOLD

        return temperPred, arousalPred, valencePred, temperConf, arousalConf, valenceConf

    def calculate_summary(self):
        summary_result = {}

        # Vitality
        summary_result.update({'vitality_score': self.result_df_new.loc[0, 'vitality_score']})

        for tav in self.all_tav:
            # TAV three class results
            summary_result.update({tav + '_label': self.result_df_new.loc[0, tav + '_label'],
                                   tav + '_score': self.result_df_new.loc[0, tav + '_score']})

            # high/low detector
            for cur_class in self.all_classes:
                tav_tav = tav + '_' + cur_class

                summary_result.update({tav_tav + '_label': self.result_df_new.loc[0, tav_tav + '_label'],
                                       tav_tav + '_score': self.result_df_new.loc[0, tav_tav + '_score']})

        return summary_result

    def calculate_summary_matrix(self, df):
        summary_result = {}
        list_result = []
        for tav in self.all_tav:
            tmp_df = df[[tav + '_label', tav + '_score']]
            tmp_df = tmp_df.dropna()
            group = tmp_df[tav + '_label'].values
            score = tmp_df[tav + '_score'].values
            if len(group) == 0 or len(score) == 0:
                summary_result.update({tav + '_median': np.nan})
                summary_result.update({tav + '_mode': np.nan})
                summary_result.update({tav + '_mode_percent': np.nan})
                list_result = np.append(list_result, [np.nan, np.nan, np.nan])
                continue

            mean, mode, mode_percent, median = self.summary_per_type(score, group)
            med_rounded = np.around(100*median, decimals=0)

            # update result dictionary
            summary_result.update({tav + '_median': med_rounded})
            summary_result.update({tav + '_mode': mode})
            summary_result.update({tav + '_mode_percent': mode_percent})

            list_result = np.append(list_result, [med_rounded, mode, mode_percent])

        # detectors
        for tav in self.all_tav:
            for tav_class in self.all_classes:
                tav_tav = tav + '_' + tav_class
                if len(df[tav_tav + '_score'].values) == 0:
                    summary_result.update({tav_tav + '_median': np.nan})
                    list_result = np.append(list_result, [np.nan])
                    continue
                else:
                    med_score = np.around(100*np.median(df[tav_tav + '_score'].values), decimals=0)
                    summary_result.update({tav_tav + '_median': med_score})
                    list_result = np.append(list_result, [med_score])

        # Vitality
        vitality_array = df.loc[0, 'vitality_score']
        summary_result.update({'vitality_score': np.median(vitality_array)})

        return summary_result, list_result

    @staticmethod
    def summary_per_type(val_list, group_list, is_emotion_groups=False):

        group_list = [x for x in group_list if x != 'ambiguous']
        val_list = [y for y in val_list if y != None]

        # Can happen if the first (or first few) chunks are zero
        if len(group_list) == 0:
            # if np.all(np.isnan(group_list)):
            if is_emotion_groups:
                return np.nan, 'Inexplicit emotion', np.nan, np.nan
            else:
                return np.nan, 'ambiguous', np.nan, np.nan

        if is_emotion_groups:
            mean = None
            median = None
        else:
            mean = np.nanmean(val_list)
            median = np.nanmedian(val_list)

        group_list = [x for x in group_list if str(x) != 'nan']
        modal = Counter(group_list)
        mode = modal.most_common(1)
        if len(group_list) == 0:
            mode_percent = 0
        else:
            mode_percent = 100 * mode[0][1] / len(group_list)

        return mean, mode[0][0], mode_percent, median

    def get_summary_matrix_and_end_session_df(self, df):

        summary_df = df.loc[df['new'] == 1].copy()
        summary_df = summary_df.reset_index(drop=True)
        # summary_df = summary_df[['i', 'wav', 'vad', 'end', 'do_predict']]
        summary_df = summary_df[['wav', 'end', 'do_predict']]
        summary_df = summary_df.rename(columns={'do_predict': 'valid'})

        summary_cols = ['temper_median', 'temper_mode', 'temper_mode_percent',  # TAV
                        'arousal_median', 'arousal_mode', 'arousal_mode_percent',
                        'valence_median', 'valence_mode', 'valence_mode_percent',

                        'anger', 'sadness', 'high', 'low', 'positive', 'negative']  # detectors

        for s in summary_cols:
            summary_df[s] = np.nan

        for i in range(0, df.i.values[-1:][0] + 1):
            cur_df = df.loc[df['i'] == i].copy()
            summary_result, summary_list = self.calculate_summary_matrix(cur_df)
            summary_df.loc[i, summary_cols] = summary_list

        self.clean_up()
        return summary_df

    def get_summary_and_end_session_df(self, df):
        try:
            df_end = df[df['do_predict'] == True]
            end = df_end.start.values[-1] + df_end.end.values[-1]
        except:
            self.logger.warning('issue with end calculation, sending NaN')
            end = None
        summary_df = pd.DataFrame(columns=['start', 'end', 'valid'],
                                  data=[[None, end, np.any(df.do_predict.values)]])
        summary_cols = ['temper_median', 'temper_mode', 'temper_mode_percent',  # TAV
                        'arousal_median', 'arousal_mode', 'arousal_mode_percent',
                        'valence_median', 'valence_mode', 'valence_mode_percent',
                        'temper_high_median', 'temper_low_median', 'arousal_high_median',
                        'arousal_low_median', 'valence_high_median', 'valence_low_median']

        for s in summary_cols:
            summary_df[s] = None

        for i in range(0, df.i.values[-1:][0] + 1):
            summary_result, summary_list = self.calculate_summary_matrix(df)
            # summary_df.loc[i, summary_cols] = summary_list

            for s in summary_cols:
                summary_df[s] = summary_result[s]

        summary_df.rename(columns={'temper_high_median': 'anger', 'temper_low_median': 'sadness',
                                   'arousal_high_median': 'high', 'arousal_low_median': 'low',
                                   'valence_high_median': 'positive', 'valence_low_median': 'negative'}, inplace=True)
        self.clean_up()
        return summary_df
