import numpy as np
from scipy.signal import medfilt
import PATrends.MiscMethods as Misc
from PATrends import PATConfig as PATCfg
from TAVEngine import HLDConfig as HLDCfg
from TAVEngine.HLDFeatureVector import HLDFeatureVector
from TAVEngine.PATDerivative import PATDerivative
from PATrends.SerializeDeserialize import SerializeDeserialize as SerDeSer
import sys


class HLDCalculator:
    def __init__(self):
        self.LLD_Derivative_FG = PATDerivative()
        self.HLD_Features = HLDFeatureVector(HLDCfg.LLD_BASE)
        self.SerDeSer = SerDeSer()
        self.seg_len = HLDCfg.SEGMENT_LENGTH
        self.win_overlap = HLDCfg.SLIDING_WINDOW_OVERLAP
        self.fs = PATCfg.SAMPLING_RATE
        self.fr_size_feat = PATCfg.FRAME_SIZE_FEATURES
        self.fr_step_feat = PATCfg.FRAME_STEP_FEATURES
        self.ANALYZE_ONE_SEGMENT = HLDCfg.ANALYZE_ONE_SEGMENT

    def post_process_lld(self, lld_dict):
        if PATCfg.ENGINE_CONF['HLD'] == 'orig':
            for ii, val in enumerate(lld_dict['pitch_th_cond'][0]):
                if val >= PATCfg.D_NORM_THRESHOLD:
                    lld_dict['pitch'][0][ii] = 0
                    lld_dict['ACpitch'][0][ii] = 0

            # apply median filter to pitch
            lld_dict_med = self.apply_median_filter(lld_dict, 'pitch')

            # calculate VAD & LogPitch
            dict_to_add = {'VAD': np.array(lld_dict_med['pitch'], dtype=bool)[0], 'LogPitch': np.log10(lld_dict['pitch'])}
            lld_dict_med.update(dict_to_add)

            # add derivatives to the list HLDCfg.FEATURES_TO_DERIVE and return
            return self.LLD_Derivative_FG.add_derivatives(lld_dict_med)

        elif PATCfg.ENGINE_CONF['HLD'] == 'new':
            lld_dict_med = self.apply_median_filter(lld_dict, 'pitch')

            # prepare pith derivative before TH
            if HLDCfg.USE_NEW_DERIVATIVE:
                ac_pitch = lld_dict['ACpitch']
                ac_pitch[np.isnan(ac_pitch)] = 0
                log_pitch = np.log10(lld_dict['pitch'])
                log_pitch[np.isnan(log_pitch)] = 0
                # tmp = {'ac_p_orig': lld_dict['ACpitch'], 'log_p_orig': np.log10(lld_dict['pitch'])}
                tmp = {'ac_p_orig': ac_pitch, 'log_p_orig': log_pitch}
                pitch_derive = self.LLD_Derivative_FG.add_derivatives(tmp, features_to_derive=['log_p_orig', 'ac_p_orig'])

            # # pipe TH from PitchDetection.py
            for ii, val in enumerate(lld_dict['pitch_th_cond'][0]):
                if val >= PATCfg.D_NORM_THRESHOLD or np.isnan(val):
                    lld_dict['pitch'][0][ii] = 0
                    lld_dict['ACpitch'][0][ii] = 0
                    if HLDCfg.USE_NEW_DERIVATIVE:
                        pitch_derive['dlog_p_orig'][0][ii] = 0
                        pitch_derive['dac_p_orig'][0][ii] = 0

            dict_to_add = {'VAD': np.array(lld_dict_med['pitch'], dtype=bool)[0],
                           'LogPitch': np.log10(lld_dict['pitch'])}
            dict_to_add['LogPitch'][0, np.logical_not(dict_to_add['VAD'])] = 0
            lld_dict_med['ACpitch'][0, np.logical_not(dict_to_add['VAD'])] = 0
            lld_dict_med.update(dict_to_add)
            # add derivatives to the list HLDCfg.FEATURES_TO_DERIVE and return
            dict_with_derivatives = self.LLD_Derivative_FG.add_derivatives(lld_dict_med)
            if HLDCfg.USE_NEW_DERIVATIVE:
                dict_with_derivatives['dLogPitch'] = pitch_derive['dlog_p_orig']
                dict_with_derivatives['dACpitch'] = pitch_derive['dac_p_orig']

            return dict_with_derivatives

    def manage_lld_matrices(self, lld_dict, params):
        if len(lld_dict) == 0:
            return [], [], []
        elif lld_dict[next(iter(lld_dict))].shape[1] == 0:
            return [], [], []

        redis_data = self.SerDeSer.load_locally_hld()
        # keep track of num of frames received from PAT
        redis_data['lld_frames'] += lld_dict[PATCfg.LLD_OUTPUT[0]].shape[1]  # REPLACES params["num_of_frames"]

        # accumulate lld_matrices in local buffer
        if len(redis_data['loaded_chunk']) == 0:
            redis_data['loaded_chunk'] = lld_dict  # to be used later with load-save
        else:
            for key in lld_dict:
                redis_data['loaded_chunk'][key] = \
                    np.hstack([np.array(redis_data['loaded_chunk'][key]), np.array(lld_dict[key])])
        self.SerDeSer.save_locally_hld(redis_data)

        processed_secs, segment_len_fr, segment_len_fr_step = self.aux_calc_hld_params(params, redis_data['lld_frames'])
        if processed_secs >= self.seg_len or HLDCfg.FORCE_ANALYSIS_FOR_SHORT_FRAMES:

            hld_analyses = Misc.get_frame_number(processed_secs, self.seg_len, self.win_overlap,
                                                 force_analysis=HLDCfg.FORCE_ANALYSIS_FOR_SHORT_FRAMES,
                                                 force_one_analysis=self.ANALYZE_ONE_SEGMENT)
            cur_start_time = list()
            dictionary_list = list()
            for x in range(0, hld_analyses):

                dict_to_analyze = {}
                for key, value in redis_data['loaded_chunk'].items():
                    dict_to_analyze[key] = value[:, :segment_len_fr]

                # delete - up to SEGMENT_LENGTH_FRAMES_OVERLAP
                for key, value in redis_data['loaded_chunk'].items():
                    redis_data['loaded_chunk'][key] = value[:, segment_len_fr_step:]

                # re-calc lld frames counter
                redis_data['lld_frames'] -= segment_len_fr_step

                # update counters
                cur_start_time.append(redis_data['cur_analysis_start'])
                redis_data['cur_analysis_start'] += self.win_overlap

                # save to redis - chunks, lld_counter, next_start_time
                self.SerDeSer.save_locally_hld(redis_data)

                dictionary_list.append(dict_to_analyze)

            return dictionary_list, cur_start_time, processed_secs
        else:
            return [], [], []

    def process_matrix(self, lld_dict, lld_params):

        if lld_params['new_wav'] is True:
            self.hld_cleanup()

        # process incoming lld package, update counters, decide if calc hld
        dict_list, start_time, processed_secs = self.manage_lld_matrices(lld_dict, lld_params)

        feature_vector = list()
        lld_dict_aug = list()
        vad_list = list()
        for i in range(0, len(dict_list)):  # if len of dict list is 0, no analysis

            # augment lld_dictionary to include medfilt, derivatives, VAD, LogPitch
            lld_dict_aug.append(self.post_process_lld(dict_list[i]))

            # find truncation params w.r.t config files
            new_trunc = self.adjust_trunc()

            # calculate HLDs
            feature_vector.append(self.HLD_Features.compute_features(lld_dict_aug[i], HLDCfg.OPERATOR_LIST, new_trunc))
            vad_list.append(lld_dict_aug[i]['VAD'][new_trunc[0]:new_trunc[1]])

        return lld_dict_aug, feature_vector, start_time, vad_list, processed_secs

    @staticmethod
    def get_hld_features_titles():
        return HLDFeatureVector.feature_titles()

    def aux_calc_hld_params(self, lld_params, lld_frame_counter):

        segment_len_fr = Misc.frames_in_segment(self.seg_len, self.fr_step_feat, self.fs)
        segment_len_fr_step = Misc.frames_in_segment(self.win_overlap, self.fr_step_feat, self.fs)

        proc_sec = lld_params["residual"] / self.fs + Misc.frames2seconds(lld_frame_counter,
                                                                          self.fr_size_feat, self.fr_step_feat, self.fs)

        # if self.ANALYZE_ONE_SEGMENT:
        #     segment_len_fr = sys.maxsize
        #     segment_len_fr_step = lld_frame_counter

        return proc_sec, segment_len_fr, segment_len_fr_step

    @staticmethod
    def adjust_trunc(seg_length=HLDCfg.SEGMENT_LENGTH):  # TODO need to replace HLD.SEGMENT_LENGTH to self.seg_len so can override it from api
        # calculates frames to truncate per hld calc. Defines at HLDCfg. e.g. for 10 sec, step = 80, target is 1000 fr
        target_num_frames = int(np.ceil(PATCfg.SAMPLING_RATE * seg_length) / PATCfg.FRAME_STEP_FEATURES)
        return [HLDCfg.TRUNC_ST, target_num_frames + HLDCfg.TRUNC_ND]

    @staticmethod
    def apply_median_filter(dict_in, selected_key):
        vector = dict_in[selected_key]
        vector_med = medfilt(vector[0, :], kernel_size=HLDCfg.MED_FILT_KERNEL)
        dict_in[selected_key] = vector_med.reshape(1, -1)
        return dict_in

    def hld_cleanup(self):
        self.SerDeSer.clean_up_hld()
