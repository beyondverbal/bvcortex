import pandas as pd
import numpy as np
from PATrends import PATConfig as PATCfg
import logging.config
from TAVCommon import log_conf

pd.options.display.max_columns = 999
logging.config.dictConfig(log_conf.create_log_dict_no_file(PATCfg.STD_OUT_LEVEL))
cur_logger = logging.getLogger('HLD Config')


TRUNC_ST = 5
TRUNC_ND = -10

# CONTROL
RESAMPLE = True  # if sampling rate is different than defined, let librosa resample

FORCE_ADAPTATION = False
ADAPTATION_BIAS = 0.15

if 'ANALYZE_VITALITY' in PATCfg.ENGINE_CONF.keys():
    cur_logger.debug('ANALYZE_VITALITY = {}'.format(PATCfg.ENGINE_CONF['ANALYZE_VITALITY']))
    ANALYZE_VITALITY = PATCfg.ENGINE_CONF['ANALYZE_VITALITY']
    SEGMENT_LENGTH = 10 + 10*PATCfg.ENGINE_CONF['ANALYZE_VITALITY']  # 10 if False, 20 if True
else:
    ANALYZE_VITALITY = False
    SEGMENT_LENGTH = 10  # In sec

if 'ANALYZE_TAV' in PATCfg.ENGINE_CONF.keys():
    cur_logger.debug('ANALYZE_TAV = {}; SEGMENT_LEN = {}'.format(PATCfg.ENGINE_CONF['ANALYZE_TAV'], SEGMENT_LENGTH))
    ANALYZE_DETECTORS = PATCfg.ENGINE_CONF['ANALYZE_TAV']
else:
    ANALYZE_DETECTORS = True

SLIDING_WINDOW_OVERLAP = 5  # In sec; 0 for no overlap\sliding-window
FORCE_ANALYSIS_FOR_SHORT_FRAMES = False  # should always be FALSE! use only in offline mode with short segments
ANALYZE_ONE_SEGMENT = False

MFCC_NUM = PATCfg.MFCC_NUM
MED_FILT_KERNEL = 5
VAD_THRESHOLD = 10.0
CONFIDENCE_THRESHOLD = 0.1
EG_CONFIDENCE_THRESHOLD = 0.3
ENFORCE_ALLOWED_COMB = False

# if PATCfg.ENGINE_CONF['DO_DD']:
#     DERIVE_DELTA_DELTA = True
# else:
#     DERIVE_DELTA_DELTA = False
#     LLD_BASE = ['mfcc', 'loudness', 'dmfcc', 'dloudness', 'LogPitch', 'ACpitch', 'VAD', 'dLogPitch', 'dACpitch',
#                 'ddmfcc', 'ddloudness', 'ddLogPitch', 'ddACpitch']
# else:
LLD_BASE = ['mfcc', 'loudness', 'dmfcc', 'dloudness', 'LogPitch', 'ACpitch', 'VAD', 'dLogPitch', 'dACpitch']

DERIVE_DELTA_DELTA = False
if PATCfg.ENGINE_CONF['DO_DD']:
    DERIVE_DELTA_DELTA = True
    LLD_BASE = np.append(LLD_BASE, ['ddmfcc', 'ddloudness', 'ddLogPitch', 'ddACpitch'])
if PATCfg.ENGINE_CONF['SMILE']:
    LLD_BASE = np.append(LLD_BASE, 'smile')

FEATURES_TO_DERIVE = ['mfcc', 'loudness', 'LogPitch', 'ACpitch']
# TODO: fix 'smile' number - get from os config or something

# Used for title generation when only doing feature extract
AUG_LLD_BASE_DIMENSION = {'mfcc': MFCC_NUM, 'loudness': 1, 'dmfcc': MFCC_NUM, 'dloudness': 1, 'LogPitch': 1, 'VAD': 1,
                          'dLogPitch': 1, 'ACpitch': 1, 'dACpitch': 1, 'voice_detected': 1, 'ddmfcc': MFCC_NUM,
                          'ddloudness': 1, 'ddLogPitch': 1, 'ddACpitch': 1, 'smile': 34}

tav_dict = {
    'temper': {'low': 'low', 'med': 'medium', 'high': 'high', 'ambiguous': 'ambiguous'},
    'arousal': {'low': 'low', 'med': 'neutral', 'high': 'high', 'ambiguous': 'ambiguous'},
    'valence': {'low': 'negative', 'med': 'neutral', 'high': 'positive', 'ambiguous': 'ambiguous'}}


MODELS_FOLDER = './TAVEngine/ModelLoader/'
HIGH_TEMPER_MODEL_PATH = MODELS_FOLDER + 'high_temper_model.pkl'
LOW_TEMPER_MODEL_PATH = MODELS_FOLDER + 'low_temper_model.pkl'
HIGH_AROUSAL_MODEL_PATH = MODELS_FOLDER + 'high_arousal_model.pkl'
LOW_AROUSAL_MODEL_PATH = MODELS_FOLDER + 'low_arousal_model.pkl'
HIGH_VALENCE_MODEL_PATH = MODELS_FOLDER + 'high_valence_model.pkl'
LOW_VALENCE_MODEL_PATH = MODELS_FOLDER + 'low_valence_model.pkl'
VITALITY_MODEL_PATH = MODELS_FOLDER + 'vitality_model.pkl'

th_h = 0.51  # .95
th_l = 0.5
# THs for detector predictors - upper sets 'detector true', lower: false; in between: ambiguous
DETECTOR_THs = {'temper': {'high_upper': th_h, 'high_lower': th_l, 'low_upper': th_h, 'low_lower': th_l},
                'arousal': {'high_upper': th_h, 'high_lower': th_l, 'low_upper': th_h, 'low_lower': th_l},
                'valence': {'high_upper': th_h, 'high_lower': th_l, 'low_upper': th_h, 'low_lower': th_l}}
# # THs for detector predictors - upper sets 'detector true', lower: false; in between: ambiguous
# DETECTOR_THs = {'temper': {'high_upper': 0.9, 'high_lower': 0.55, 'low_upper': 0.8, 'low_lower': 0.65},
#                 'arousal': {'high_upper': 0.9, 'high_lower': 0.55, 'low_upper': 0.8, 'low_lower': 0.64},
#                 'valence': {'high_upper': 0.9, 'high_lower': 0.6, 'low_upper': 0.8, 'low_lower': 0.6}}

USE_VOICED_ONLY = True

VAD_FEATURES = ['VAD_R', 'VAD_ZC', 'VAD_false_mean', 'VAD_false_std', 'VAD_true_mean', 'VAD_true_std']

if PATCfg.ENGINE_CONF['OP'] == 'D4':
    OPERATOR_LIST = ['deciles4']
elif PATCfg.ENGINE_CONF['OP'] == 'M8':
    OPERATOR_LIST = ['moments8']
elif PATCfg.ENGINE_CONF['OP'] == 'M4':
    OPERATOR_LIST = ['moments4']
elif PATCfg.ENGINE_CONF['OP'] == 'D5':
    OPERATOR_LIST = ['deciles5']
elif PATCfg.ENGINE_CONF['OP'] == 'M5':
    OPERATOR_LIST = ['moments5']
elif PATCfg.ENGINE_CONF['OP'] == 'N5':
    OPERATOR_LIST = ['nonagon5']
elif PATCfg.ENGINE_CONF['OP'] == 'N4':
    OPERATOR_LIST = ['nonagon4']

if PATCfg.ENGINE_CONF['DERIV']:
    USE_NEW_DERIVATIVE = True
else:
    USE_NEW_DERIVATIVE = False

OPERATOR_LIST_DIM = {'deciles4': 4, 'moments4': 4, 'moments8': 8, 'deciles5': 5, 'moments5': 5, 'nonagon5': 5, 'nonagon4': 4}

allowed_comb = {
    '0': {'temper': 'low',      'valence': 'negative',  'arousal': 'low'},
    '1': {'temper': 'low',      'valence': 'negative',  'arousal': 'neutral'},
    '2': {'temper': 'medium',   'valence': 'neutral',   'arousal': 'neutral'},
    '3': {'temper': 'medium',   'valence': 'positive',  'arousal': 'neutral'},
    '4': {'temper': 'medium',   'valence': 'positive',  'arousal': 'high'},
    '5': {'temper': 'high',     'valence': 'negative',  'arousal': 'neutral'},
    '6': {'temper': 'high',     'valence': 'negative',  'arousal': 'high'}}
allowed_comb = pd.DataFrame(allowed_comb).T
replace_cost_weight = {'diff_size': 3, 'total_dec_diff': 0, 'total_prob_diff': 1}
classesDic = {'high': "H", 'low': "L", 'medium': "M",
              'negative': "L", 'neutral': "M", 'positive': "H"}
toNumDic = {"H": 100, "L": 0, "M": 50}
