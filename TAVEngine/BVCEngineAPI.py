import logging.config
import os
import numpy as np
import pandas as pd
from librosa import util
import PATrends.PATConfig as PATCfg
import TAVEngine.HLDConfig as HLDCfg
import TAVCommon.AuxFunctions as AuxFunc
from PATrends.PATCalculator import PATCalculator
from TAVCommon import log_conf
from TAVEngine.DetectorPredict import DetectorPredict as PredictFromHLD
from TAVEngine.HLDCalculator import HLDCalculator

logging.config.dictConfig(log_conf.create_log_dict_no_file(logging.INFO))
cur_logger = logging.getLogger('BVCEngineAPI - v5.5')


class BVCEngineAPI:
    def __init__(self):
        self.pat_calc = PATCalculator()
        self.hld_calc = HLDCalculator()
        self.predictor = PredictFromHLD()  # now detectors only ;-)
        self.mid_result = pd.DataFrame([])

    def process_chunk(self, signal):

        predict_result = self.tav_engine_core(signal, 0, 'unknown')
        if predict_result.shape[0] > 0:
            if len(self.mid_result) == 0:
                self.mid_result = predict_result
            else:
                self.mid_result = self.mid_result.append(predict_result)
        else:
            return None
        res = self.filter_output_fields(predict_result.to_dict('records')[0], op='mid_result')

        return res

    @staticmethod
    def filter_output_fields(res_dict, op='summary'):
        new_dict = {}
        if op == 'summary':
            for key in ['start', 'end']:
                new_dict.update({key: res_dict[key]})
            for key in ['anger', 'sadness', 'high', 'low', 'positive', 'negative']:
                new_dict.update({key + '_summary': res_dict[key]})

        elif op == 'mid_result':
            new_dict = {'start': res_dict['start'],
                        'end': res_dict['end'],
                        'duration': res_dict['duration'],
                        'anger': np.round(100*res_dict['temper_high_score'], 0),
                        'sadness': np.round(100*res_dict['temper_low_score'], 0),
                        'high': np.round(100*res_dict['arousal_high_score'], 0),
                        'low': np.round(100*res_dict['arousal_low_score'], 0),
                        'positive': np.round(100*res_dict['valence_high_score'], 0),
                        'negative': np.round(100*res_dict['valence_low_score'], 0)
                        }

        return new_dict

    def end_session(self, calculate_summary=True):
        self.pat_calc.clean_up()
        self.predictor.clean_up()

        if calculate_summary:
            summ = self.predictor.get_summary_and_end_session_df(self.mid_result)
            self.mid_result = pd.DataFrame([])
            return self.filter_output_fields(summ.to_dict('records')[0])
        else:
            self.mid_result = pd.DataFrame([])
            return None

    def tav_engine_core(self, signal, i, file_id):

        result_df = pd.DataFrame(columns=['new', 'i', 'wav', 'vad', 'start', 'duration', 'end'])

        hld_np_matrix = np.empty([0, PATCfg.ENGINE_CONF['MAT_SIZE']])

        # PAT
        pat_dictionary, pat_params = self.pat_calc.process_chunk(signal)  # calc basic lld features
        # HLD
        pat_dict_aug, hld, ts, vad_list, t_total = self.hld_calc.process_matrix(pat_dictionary, pat_params)

        for k in range(len(vad_list)):
            vad_s = round(100 * vad_list[k].mean(), 0)

            # result_df.loc[len(result_df)] = [int(not bool(k)), i, file_id, vad_s, ts[k], t_total]
            result_df.loc[len(result_df)] = [0, i, file_id, vad_s, ts[k], t_total, ts[k] + t_total]
            hld_np_matrix = np.vstack((hld_np_matrix, hld[k]))
        # self.pat_calc.clean_up()  # clean buffers etc.
        vad_vec = result_df['vad'].values
        ts_vec = result_df['start'].values

        predict_res_detector = self.predictor.predict_from_hld_matrix(hld_np_matrix, vad_vec, ts_vec)

        return pd.concat([result_df, predict_res_detector], axis=1)  # , sort=False)

    @staticmethod
    def filter_by_segment(pat_dict, df, fs):

        hop_size = PATCfg.FRAME_STEP_FEATURES

        def calc_frame(st, nd):
            start_fr = int(st*fs/hop_size)  # can start from 0
            end_fr = int((nd*fs/hop_size)) - 1  # minus one for edge effects
            return start_fr, end_fr

        new_pat_dict = {}
        seconds_from_df = 0
        indices = np.array([], dtype=int)
        for i in range(0, len(df.seg_start.values)):
            st_fr, nd_fr = calc_frame(df.seg_start.values[i], df.seg_end.values[i])
            seconds_from_df += df.seg_end.values[i] - df.seg_start.values[i]
            indices = np.append(indices, np.arange(st_fr, nd_fr, dtype=int))
            cur_logger.debug('          adding seconds {}->{}, frames {}->{}'.format(
                df.seg_start.values[i], df.seg_end.values[i], st_fr, nd_fr))

        # Patch todo - barbaric! cut indices vector to match number of frames (avoid calling non-existing frames)
        max_len = pat_dict[next(iter(pat_dict))].shape[1]
        indices = indices[indices < max_len]

        for key in pat_dict.keys():
            new_pat_dict.update({key: pat_dict[key][:, indices]})

        # import matplotlib.pyplot as plt
        # x = np.zeros([1, 3000])
        # x[0, indices] = 1
        # plt.spy(x)#, markersize=4, precision=1)
        return new_pat_dict, seconds_from_df

    def tav_engine_core_multi(self, signal, file_id, dict_in, fs):
        cur_logger.info('       started tav_engine_core_multi for file {}...'.format(file_id))

        # PAT
        pat_dictionary_orig, pat_params_orig = self.pat_calc.process_chunk(signal)  # calc basic lld features

        spkr_df = pd.DataFrame(columns=['wav', 'spkr', 'vad', 'total[sec]', 'vitality', 'valid'])
        for spkr in dict_in['speakers']:

            cur_logger.info('           started analysis for speaker {}...'.format(spkr))
            hld_np_matrix = np.empty([0, PATCfg.ENGINE_CONF['MAT_SIZE']])

            pat_dictionary, tot_secs = self.filter_by_segment(pat_dictionary_orig, dict_in[spkr], fs)

            # HLD
            pat_dict_aug, hld, ts, vad_list, t_total = self.hld_calc.process_matrix(pat_dictionary, pat_params_orig)

            if len(vad_list) > 0:
                k = 0  # for compatibility
                vad_s = round(100 * vad_list[k].mean(), 0)
                hld_np_matrix = np.vstack((hld_np_matrix, hld[k]))
                self.pat_calc.clean_up()  # clean buffers etc.
                predict_res_detector = self.predictor.predict_from_hld_matrix(hld_np_matrix, vad_s, ts[k])
                vitality = predict_res_detector.vitality_score.values[0]

                valid = predict_res_detector.do_predict.values
                spkr_df.loc[len(spkr_df)] = [file_id, spkr, vad_s, np.round(t_total, 1), vitality, valid[0]]
            else:
                spkr_df.loc[len(spkr_df)] = [file_id, spkr, -1, int(tot_secs), -1, False]

        return spkr_df

    def process_stream(self, stream):
        cur_logger.info('Starting process_stream')
        signal = util.buf_to_float(stream, dtype=np.float32)
        cur_logger.debug('Starting process_stream - signal length {} samples'.format(len(signal)))

        self.predictor.enforce_adaptation = False  # no adaptation in predict
        self.mid_result = self.tav_engine_core(signal, 0, 'stream')
        return self.mid_result

    @staticmethod
    def analyze_demo_df(df):
        speaker_dict = {}
        speakers = np.unique(df.legend.values)
        cur_logger.debug('      found {} speakers: {}'.format(len(speakers), speakers))
        speaker_dict.update({'speakers': speakers})
        for spkr in speaker_dict['speakers']:
            df_spkr = df[df.legend == spkr][['seg_start'] + ['seg_end']]
            speaker_dict.update({spkr: df_spkr})

        return speaker_dict

    @staticmethod
    def analyze_demo_df_old(df):

        file_dict = {}
        file_dict.update({'files': np.unique(df.filename.values)})

        cur_logger.debug('  Analyzing input csv... found {} files'.format(len(file_dict['files'])))

        for file in file_dict['files']:
            file_dict.update({file: {}})

            df_file = df[df.filename == file][['seg_start'] + ['seg_end'] + ['legend']]
            speakers = np.unique(df_file.legend.values)

            cur_logger.debug('      File {} ... found {} speakers'.format(file, len(speakers)))

            file_dict[file].update({'speakers': speakers})
            for spkr in file_dict[file]['speakers']:
                df_spkr = df_file[df_file.legend == spkr][['seg_start'] + ['seg_end']]
                file_dict[file].update({spkr: df_spkr})

        return file_dict

    def analyze_file_demo(self, file_path, df):

        cur_logger.debug('Hello! starting analyze_file_demo...')

        speaker_dict = self.analyze_demo_df(df)
        self.hld_calc.ANALYZE_ONE_SEGMENT = True  # use only SEGMENT_LENGTH from signal
        self.predictor.enforce_adaptation = False  # no adaptation in predict

        signal, fs = AuxFunc.read_wav(PATCfg.DEMO_PREFIX + file_path, PATCfg.FRAME_SIZE_FEATURES, PATCfg.SAMPLING_RATE)
        if signal is None or fs is None:
            cur_logger.debug('signal for {} is null...'.format(PATCfg.DEMO_PREFIX + file_path))
            return None
        else:
            cur_logger.debug('signal for {} was read. len = {} secs...'.format(PATCfg.DEMO_PREFIX + file_path, len(signal)/fs))
            return self.tav_engine_core_multi(signal, PATCfg.DEMO_PREFIX + file_path, speaker_dict, fs)

    def analyze_file(self, file):
        self.hld_calc.ANALYZE_ONE_SEGMENT = HLDCfg.ANALYZE_ONE_SEGMENT  # use only SEGMENT_LENGTH from signal
        self.predictor.enforce_adaptation = HLDCfg.FORCE_ADAPTATION  # no adaptation in predict

        signal, fs = AuxFunc.read_wav(file, PATCfg.FRAME_SIZE_FEATURES, PATCfg.SAMPLING_RATE, resample=HLDCfg.RESAMPLE)
        if signal is None or fs is None:
            return None
        else:
            return self.tav_engine_core(signal, 0, file)

    @staticmethod
    def pre_process_batch(in_file):
        if type(in_file) is str:  # dir or
            if os.path.isdir(in_file):
                p = os.listdir(in_file)
                wav_list = [in_file + x for x in p if x.endswith('.wav')]
                cur_logger.debug('detected directory as input: {}; extracted {} files'.format(in_file, len(wav_list)))
                return {'wav_list': np.array(wav_list)}
            elif os.path.isfile(in_file):
                if in_file.endswith('.csv'):
                    df = pd.read_csv(in_file)
                    if 'filename' in list(df):
                        cur_logger.warning('detected csv file as in_file; {} files'.format(len(df.filename.values)))
                        return {'wav_list': np.array(df.filename.values)}
                    else:
                        cur_logger.warning('detected file as input - but not filename column')
                elif in_file.endswith('.wav'):
                    cur_logger.warning('detected WAV file as input - single_file mode')
                    return {'wav_list': [in_file]}
                else:
                    cur_logger.warning('detected file as input - but not CSV!')

        elif type(in_file) is dict:
            if 'wav_list' not in in_file.keys():
                cur_logger.warning('dictionary as input - but no ''wav_list'' key')
        else:
            cur_logger.warning('input type ({}) not recognized for analyze_batch'.format(type(in_file)))

    def analyze_batch(self, batch_input):
        wav_dict = self.pre_process_batch(batch_input)

        # TODO Patch - if only one file, don't use adaptation
        if len(wav_dict['wav_list']) > 1:
            self.predictor.PRED.enforce_adaptation = HLDCfg.FORCE_ADAPTATION
            cur_logger.warning('*** ENFORCE ADAPTATION =  {} ***'.format(self.predictor.PRED.enforce_adaptation))

        cur_logger.info('Analyze Batch Mode. {} files'.format(len(wav_dict['wav_list'])))
        result_df = pd.DataFrame(columns=['new', 'i', 'wav', 'vad', 'start', 'end'])
        hld_np_matrix = np.empty([0, PATCfg.ENGINE_CONF['MAT_SIZE']])  # M8 534

        for i, wav in enumerate(wav_dict['wav_list']):
            cur_logger.debug('wav_num = {}/{}; wav = {}'.format(i, len(wav_dict['wav_list']), wav[:-4]))

            signal, fs = AuxFunc.read_wav(wav, PATCfg.FRAME_SIZE_FEATURES, PATCfg.SAMPLING_RATE, resample=HLDCfg.RESAMPLE)
            if signal is None or fs is None:
                continue
            else:
                # PAT
                pat_dictionary, pat_params = self.pat_calc.process_chunk(signal)  # calc basic lld features
                # HLD
                pat_dict_aug, hld, ts, vad_list, t_total = self.hld_calc.process_matrix(pat_dictionary, pat_params)

                for k in range(len(vad_list)):
                    vad_s = round(100 * vad_list[k].mean(), 0)

                    result_df.loc[len(result_df)] = [int(not bool(k)), i, wav, vad_s, ts[k], t_total]
                    hld_np_matrix = np.vstack((hld_np_matrix, hld[k]))
            self.pat_calc.clean_up()  # clean buffers etc.
        vad_vec = result_df['vad'].values
        ts_vec = result_df['start'].values

        predict_res_detector = self.predictor.predict_from_hld_matrix(hld_np_matrix, vad_vec, ts_vec)

        return pd.concat([result_df, predict_res_detector], axis=1)  # , sort=False)

    def end_session_matrix(self, df):
        self.pat_calc.clean_up()
        return self.predictor.get_summary_matrix_and_end_session_df(df)
