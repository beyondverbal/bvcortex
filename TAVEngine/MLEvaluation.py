import logging
import numpy as np
import pandas as pd
from TAVEngine import ModelLoader as ML
from TAVEngine import HLDConfig as HLDCfg
from sklearn.preprocessing import StandardScaler


class MLEvaluation:
    def __init__(self):
        ML.load()
        self.logger = logging.getLogger('MLEvaluation')
        self.enforce_adaptation = False
        self.tav = ['temper', 'arousal', 'valence']

    def predict_vitality(self, model_lin, model_sig, x_test, valid_vec):
        def func_sigmoid(x, x0, k):
            y = 1 / (1 + np.exp(-k * (x - x0)))
            return y

        if type(model_lin) is not dict:

            # x_transformed = model_lin.named_steps['standardize'].transform(x_test[valid_vec, :])
            x_transformed = model_lin.named_steps['standardize'].transform(x_test)
            if 'pca' in model_lin.named_steps.keys():
                x_transformed = model_lin.named_steps['pca'].transform(x_transformed)

            a = np.array(model_lin.named_steps['lin'].coef_, dtype="float64")
        else:
            # x_transformed = model_lin['standardize'](x_test[valid_vec, :])
            x_transformed = model_lin['standardize'](x_test)
            if 'pca' in model_lin.keys():
                x_transformed = model_lin['pca'](x_transformed)
            a = np.array(model_lin['lin'], dtype="float64")

        x_transformed = np.array(x_transformed, dtype="float64")

        projection = np.dot(a, x_transformed.T)[0]  # todo: 64 bit
        if self.enforce_adaptation:
            lin_dist = projection - np.mean(projection)
        else:
            lin_dist = projection

        score = func_sigmoid(lin_dist, 0, model_sig[1])

        return {'conf': score, 'lin_dist': lin_dist}

    @staticmethod
    def predict_from_dist_normed(model_a, model_b, model_sig2, dist_normed):
        def func_sigmoid(x, x0, k):
            y = 1 / (1 + np.exp(-k * (x - x0)))
            return y

        model_sig, dist_scaler = model_sig2
        score = func_sigmoid(dist_normed, model_sig[0], model_sig[1])

        return score

    def predict_from_model_new(self, model_a, model_b, model_sig2, x_test, valid_vec):

        model_sig, dist_scaler = model_sig2

        def func_sigmoid(x, x0, k):
            y = 1 / (1 + np.exp(-k * (x - x0)))
            return y

        # projection = np.dot(model_a, x_test.T)[0] - model_b[0]
        lin_dist = np.dot(model_a, x_test.T)[0] - model_b[0]
        dist_normed = dist_scaler.transform(lin_dist.reshape(-1, 1))[:, 0]

        if self.enforce_adaptation:
            dist_normed = dist_normed - np.mean(dist_normed) + HLDCfg.ADAPTATION_BIAS


            # dist_normed = dist_normed/np.std(dist_normed)
         #   # scaler = StandardScaler().fit(dist_normed.reshape(-1, 1))
         #   # dist_normed = scaler.transform(dist_normed.reshape(-1, 1))[:, 0]

        score_from_sig = func_sigmoid(dist_normed, model_sig[0], model_sig[1])

        return {'conf': score_from_sig, 'lin_dist': lin_dist}

    @staticmethod
    def create_tav_place_holders(mat_len):
        out_dict = {}
        for i, tav in enumerate(['temper', 'arousal', 'valence']):
            col_list = [tav + '_high_label', tav + '_high_score', tav + '_label',
                        tav + '_low_label', tav + '_low_score', tav + '_score',
                        tav + '_high_lin', tav + '_low_lin']
            out_dict.update({tav: pd.DataFrame(-1, index=np.arange(mat_len), columns=col_list)})

        return out_dict['temper'], out_dict['arousal'], out_dict['valence']

    def predict_matrix(self, matrix, valid, th_dict):
        matrix = matrix.astype(np.float32)

        if HLDCfg.ANALYZE_DETECTORS:
            hi_temper = self.predict_from_model_new(ML.HighTemperA, ML.HighTemperB, ML.HighTemperSig, matrix, valid)
            lo_temper = self.predict_from_model_new(ML.LowTemperA, ML.LowTemperB, ML.LowTemperSig, matrix, valid)
            hi_ar = self.predict_from_model_new(ML.HighArousalA, ML.HighArousalB, ML.HighArousalSig, matrix, valid)
            lo_ar = self.predict_from_model_new(ML.LowArousalA, ML.LowArousalB, ML.LowArousalSig, matrix, valid)
            pos_val = self.predict_from_model_new(ML.HighValenceA, ML.HighValenceB, ML.HighValenceSig, matrix, valid)
            neg_val = self.predict_from_model_new(ML.LowValenceA, ML.LowValenceB, ML.LowValenceSig, matrix, valid)

            # for tav in self.tav:
            temper_tav = self.fuse_two_detectors('temper', hi_temper, lo_temper, th_dict['temper'], id='temper')
            arousal_tav = self.fuse_two_detectors('arousal', hi_ar, lo_ar, th_dict['arousal'], id='arousal')
            valence_tav = self.fuse_two_detectors('valence', pos_val, neg_val, th_dict['arousal'], id='valence')

            # add lin dist
            temper_tav = temper_tav.assign(temper_high_lin=hi_temper['lin_dist'], temper_low_lin=lo_temper['lin_dist'])
            arousal_tav = arousal_tav.assign(arousal_high_lin=hi_ar['lin_dist'], arousal_low_lin=lo_ar['lin_dist'])
            valence_tav = valence_tav.assign(pos_val_lin=pos_val['lin_dist'], neg_val_lin=neg_val['lin_dist'])
        else:
            temper_tav, arousal_tav, valence_tav = self.create_tav_place_holders(matrix.shape[0])

        if HLDCfg.ANALYZE_VITALITY:
            # todo - now the model has the complete feature list, in the future - complete vector same as emotions
            # has to be similar to TAV, right now using old model calculation
            df = pd.DataFrame([])
            df = pd.concat([df, pd.DataFrame(matrix)], axis=1)
            df.columns = ML.VitalityFullFeatList

            mat_short = df[ML.VitalityFeat].as_matrix()
            vitality_raw = self.predict_vitality(ML.VitalityLin, ML.VitalitySig, mat_short, valid)
            vitality = pd.DataFrame(
                data={'vitality_score': [int(100 * np.round(vitality_raw['conf'], 2))]})  # 100 from .995
        else:
            vitality = pd.DataFrame(
                data={'vitality_score': [-1 * np.ones([1, matrix.shape[0]])][0][0]})
        return {'temper_tav': temper_tav, 'arousal_tav': arousal_tav, 'valence_tav': valence_tav, 'vitality': vitality}

    def fuse_two_detectors(self, tav, high, low, th_dict, id=None):
        h_upper = th_dict['high_upper']
        h_lower = th_dict['high_lower']
        l_upper = th_dict['low_upper']
        l_lower = th_dict['low_lower']

        df = pd.DataFrame(data={'score_high': high['conf'], 'score_low': low['conf']})

        # calc pred high, low - NO AMBIG! for
        df = df.assign(pred_high=df['score_high'] >= h_upper)
        df = df.assign(pred_low=df['score_low'] >= l_upper)
        df['pred_high'] = df['pred_high'].astype(int)
        df['pred_low'] = df['pred_low'].astype(int)

        # determine ambiguous upper_th > score > lower_th
        df.loc[(df['score_high'] > h_lower) & (df['score_high'] < h_upper), 'pred_high'] = 'ambiguous'
        df.loc[(df['score_low'] > l_lower) & (df['score_low'] < l_upper), 'pred_low'] = 'ambiguous'

        # Calc Med
        df = df.assign(pred_med=np.logical_and(np.logical_not(df['pred_high']), np.logical_not(df['pred_low'])))
        df['pred_med'] = df['pred_med'].astype(int)

        df = df.assign(pred_total='ambiguous')
        df.loc[np.logical_and(df.pred_high == 1, np.logical_and(df.pred_med == 0, df.pred_low == 0)), 'pred_total'] = \
            HLDCfg.tav_dict[tav]['high']
        df.loc[np.logical_and(df.pred_high == 0, np.logical_and(df.pred_med == 1, df.pred_low == 0)), 'pred_total'] = \
            HLDCfg.tav_dict[tav]['med']
        df.loc[np.logical_and(df.pred_high == 0, np.logical_and(df.pred_med == 0, df.pred_low == 1)), 'pred_total'] = \
            HLDCfg.tav_dict[tav]['low']

        # import matplotlib.pyplot as plt
        # plt.figure(1)
        # plt.subplot(3, 1, 1)
        #
        # n, bins, patches = plt.hist(df.score_high.values, np.linspace(0,1,51)[1:], facecolor='g', alpha=0.75)
        # plt.xlim([0.0, 1.0])
        # plt.title('score high; id = {}'.format(id))
        #
        # plt.subplot(3, 1, 2)
        # n, bins, patches = plt.hist(df.score_low.values, np.linspace(0,1,51)[1:], density=True, facecolor='g', alpha=0.75)
        # plt.title('score low; id = {}'.format(id))
        # plt.xlim([0.0, 1.0])
        # plt.show()

        # how many conflicts?
        self.logger.debug('conflicts found: {}'.format((np.logical_and(df['pred_high'], df['pred_low']) == True).sum()))

        df = df.assign(score_total=df.score_high-df.score_low)
        df.loc[(df['pred_total'] == 'ambiguous'), 'score_total'] = np.nan  # was 'Nan'

        return pd.DataFrame(data={tav + '_high_score': df.score_high,
                                  tav + '_high_label': df.pred_high,
                                  tav + '_low_score': df.score_low,
                                  tav + '_low_label': df.pred_low,
                                  tav + '_score': df.score_total,
                                  tav + '_label': df.pred_total})

    @staticmethod
    def classify_predict_result(score, upper_th, lower_th):
        if score >= upper_th:
            prediction = True
        elif lower_th < score < upper_th:
            prediction = 'ambiguous'
        else:
            prediction = False
        return prediction
