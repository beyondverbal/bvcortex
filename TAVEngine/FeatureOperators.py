import numpy as np


class FeatureOperators:
    def __init__(self):
        self.OperatorDictionary = {'deciles4': self.deciles4,
                                   'deciles5': self.deciles5,  # base 10, 5 moments
                                   'moments8': self.moments8,
                                   'moments4': self.moments4,
                                   'moments5': self.moments5,
                                   'nonagon5': self.nonagon5,  # base9, 5 moments
                                   'nonagon4': self.nonagon4  # base9, 5 moments
                                   }
        self.matrix = None

    def apply_operator(self, name, matrix, dim):
        self.matrix = matrix

        rows, cols = self.matrix.shape
        if cols == 0:
            return self.none_operator(dim)

        if name in self.OperatorDictionary:
            operator = self.OperatorDictionary[name]
            return operator()
        else:
            print('ERROR: \'{}\' is not a valid operator name.'.format(name))
            return self.none_operator(dim)
    
    # ######### Support Methods ##########

    def none_operator(self, dim):
        rows, cols = self.matrix.shape
        none_vec = np.zeros(shape=(rows*dim,))
        for i in range(0, rows*dim):
            none_vec[i] = np.nan
        return none_vec 

    def moments8(self):
        p13 = np.percentile(self.matrix, np.linspace(100 / 13, 100, 13)[:-1], axis=1)
        dec_matrix = np.array([[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                               [1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, -1],
                               [-1, -1, -1, 1, 1, 1, 1, 1, 1, -1, -1, -1],
                               [-1, -1, -1, 1, 1, 1, -1, -1, -1, 1, 1, 1],
                               [1, 0, -1, -1, 0, 1, 1, 0, -1, -1, 0, 1],
                               [1, 0, -1, -1, 0, 1, -1, 0, 1, 1, 0, -1],
                               [-1, 0, 1, -1, 0, 1, 1, 0, -1, 1, 0, -1],
                               [-1, 0, 1, -1, 0, 1, -1, 0, 1, -1, 0, 1]])
        return (p13.T @ dec_matrix.T).T.reshape(-1)

    def moments5(self):
        p13_5 = np.percentile(self.matrix, np.linspace(100 / 13, 100, 13)[:-1], axis=1)
        dec_matrix = np.array([[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                               [1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, -1],
                               [-1, -1, -1, 1, 1, 1, 1, 1, 1, -1, -1, -1],
                               [-1, -1, -1, 1, 1, 1, -1, -1, -1, 1, 1, 1],
                               [1, 0, -1, -1, 0, 1, 1, 0, -1, -1, 0, 1]])
        return (p13_5.T @ dec_matrix.T).T.reshape(-1)

    def moments4(self):
        p13_4 = np.percentile(self.matrix, np.linspace(100 / 13, 100, 13)[:-1], axis=1)
        dec_matrix = np.array([[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                               [1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, -1],
                               [-1, -1, -1, 1, 1, 1, 1, 1, 1, -1, -1, -1],
                               [-1, -1, -1, 1, 1, 1, -1, -1, -1, 1, 1, 1]])
        return (p13_4.T @ dec_matrix.T).T.reshape(-1)

    def deciles4(self):
        deciles = np.percentile(self.matrix, np.linspace(10, 100, 10)[:-1], axis=1)

        dec_matrix = np.array([[1, 1, 1, 1, 1, 1, 1, 1, 1],       # m1
                               [1, 1, 1, 1, 0, -1, -1, -1, -1],   # m2
                               [-1, -1, 1, 1, 0, 1, 1, -1, -1],   # m3
                               [-1, -1, 1, 1, 0, -1, -1, 1, 1]])   # m4

        return (deciles.T @ dec_matrix.T).T.reshape(-1)  # from matrix to long vector

    def deciles5(self):
        deciles_5 = np.percentile(self.matrix, np.linspace(10, 100, 10)[:-1], axis=1)

        dec_matrix = np.array([[1, 1, 1, 1, 1, 1, 1, 1, 1],       # m1
                               [1, 1, 1, 1, 0, -1, -1, -1, -1],   # m2
                               [-1, -1, 1, 1, 0, 1, 1, -1, -1],   # m3
                               [-1, -1, 1, 1, 0, -1, -1, 1, 1],   # m4
                               [1, -1, -1, 1, 0, 1, -1, -1, 1]])  # m5

        return (deciles_5.T @ dec_matrix.T).T.reshape(-1)  # from matrix to long vector

    def nonagon5(self):
        deciles = np.percentile(self.matrix, np.linspace(100 / 9, 100, 9)[:-1], axis=1)
        dec_matrix = np.array([[1, 1, 1, 1, 1, 1, 1, 1],       # m1
                               [1, 1, 1, 1, -1, -1, -1, -1],   # m2
                               [-1, -1, 1, 1, 1, 1, -1, -1],   # m3
                               [-1, -1, 1, 1, -1, -1, 1, 1],   # m4
                               [1, -1, -1, 1, 1, -1, -1, 1]])  # m5

        return (deciles.T @ dec_matrix.T).T.reshape(-1)  # from matrix to long vector

    def nonagon4(self):
        deciles = np.percentile(self.matrix, np.linspace(100 / 9, 100, 9)[:-1], axis=1)
        dec_matrix = np.array([[1, 1, 1, 1, 1, 1, 1, 1],       # m1
                               [1, 1, 1, 1, -1, -1, -1, -1],   # m2
                               [-1, -1, 1, 1, 1, 1, -1, -1],   # m3
                               [-1, -1, 1, 1, -1, -1, 1, 1]])   # m4

        return (deciles.T @ dec_matrix.T).T.reshape(-1)  # from matrix to long vector
