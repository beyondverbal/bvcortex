import logging
from sklearn.externals import joblib
import sys
from TAVEngine import HLDConfig as HLDCfg
logger = logging.getLogger('BLV.DetectorModelLoader')

HighTemperA = None
HighTemperB = None
HighTemperSig = None
LowTemperA = None
LowTemperB = None
LowTemperSig = None
HighArousalA = None
HighArousalB = None
HighArousalSig = None
LowArousalA = None
LowArousalB = None
LowArousalSig = None
HighValenceA = None
HighValenceB = None
HighValenceSig = None
LowValenceA = None
LowValenceB = None
LowValenceSig = None
VitalityLin = None
VitalityFeat = None
VitalitySig = None
VitalityFullFeatList = None


# Load from MLEvaluation (testing)
def load():
    global HighTemperA, LowTemperA, HighArousalA, LowArousalA, HighValenceA, LowValenceA, VitalityLin, \
        HighTemperB, LowTemperB, HighArousalB, LowArousalB, HighValenceB, LowValenceB, VitalityFeat, \
        HighTemperSig, LowTemperSig, HighArousalSig, LowArousalSig, HighValenceSig, LowValenceSig, VitalitySig, \
        VitalityFullFeatList

    # load data
    logger.debug("Loading data (in load())")

    # newLin, feature_names, self.Sig, A_tot, a_tot
    HighTemperA, HighTemperB, HighTemperSig = joblib.load(HLDCfg.HIGH_TEMPER_MODEL_PATH)
    LowTemperA, LowTemperB, LowTemperSig = joblib.load(HLDCfg.LOW_TEMPER_MODEL_PATH)
    HighArousalA, HighArousalB, HighArousalSig = joblib.load(HLDCfg.HIGH_AROUSAL_MODEL_PATH)
    LowArousalA, LowArousalB, LowArousalSig = joblib.load(HLDCfg.LOW_AROUSAL_MODEL_PATH)
    HighValenceA, HighValenceB, HighValenceSig = joblib.load(HLDCfg.HIGH_VALENCE_MODEL_PATH)
    LowValenceA, LowValenceB, LowValenceSig = joblib.load(HLDCfg.LOW_VALENCE_MODEL_PATH)
    VitalityLin, VitalityFeat, VitalitySig, VitalityFullFeatList = joblib.load(HLDCfg.VITALITY_MODEL_PATH)


# Load during worker creation (server)
def load_model():
    global HighTemperA, LowTemperA, HighArousalA, LowArousalA, HighValenceA, LowValenceA, VitalityLin, \
        HighTemperB, LowTemperB, HighArousalB, LowArousalB, HighValenceB, LowValenceB, VitalityFeat, \
        HighTemperSig, LowTemperSig, HighArousalSig, LowArousalSig, HighValenceSig, LowValenceSig, VitalitySig, \
        VitalityFullFeatList

    # if data is already loaded, skip
    if HighTemperA:
        # print("Model is already loaded, skipping (in load_model())")
        return

    # load data
    updated_modules = {}
    for engine_module_name in filter(lambda x: x.startswith('TAVEngine.'), sys.modules.keys()):
        updated_modules[engine_module_name[len('TAVEngine.'):]] = sys.modules[engine_module_name]
    for k, v in updated_modules.items():
        sys.modules[k] = v

    logger.info("Loading data (in load_model())")
    HighTemperA, HighTemperB, HighTemperSig = joblib.load(HLDCfg.HIGH_TEMPER_MODEL_PATH)
    LowTemperA, LowTemperB, LowTemperSig = joblib.load(HLDCfg.LOW_TEMPER_MODEL_PATH)
    HighArousalA, HighArousalB, HighArousalSig = joblib.load(HLDCfg.HIGH_AROUSAL_MODEL_PATH)
    LowArousalA, LowArousalB, LowArousalSig = joblib.load(HLDCfg.LOW_AROUSAL_MODEL_PATH)
    HighValenceA, HighValenceB, HighValenceSig = joblib.load(HLDCfg.HIGH_VALENCE_MODEL_PATH)
    LowValenceA, LowValenceB, LowValenceSig = joblib.load(HLDCfg.LOW_VALENCE_MODEL_PATH)
    VitalityLin, VitalityFeat, VitalitySig, VitalityFullFeatList = joblib.load(HLDCfg.VITALITY_MODEL_PATH)
