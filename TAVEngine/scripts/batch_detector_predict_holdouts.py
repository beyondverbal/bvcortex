import csv
import logging.config
import os, random
import pickle
import time
import numpy as np
import pandas as pd
from shutil import copyfile
import PATrends.PATConfig as PATCfg
import TAVEngine.HLDConfig as HLDCfg
import TAVCommon.AuxFunctions as AuxFunc
from PATrends.PATCalculator import PATCalculator
from TAVCommon import log_conf
from CommonML import LinearSigmoidModel as LinSig
from TAVEngine.DetectorPredict import DetectorPredict as PredictDetector
from TAVEngine.HLDCalculator import HLDCalculator
from sklearn.metrics import roc_auc_score
from scipy import stats
from TAVEngine import ModelLoader as ML
import matplotlib.pyplot as plt
import sklearn.metrics
logging.config.dictConfig(log_conf.create_log_dict_no_file(logging.INFO))
cur_logger = logging.getLogger('Main')
cur_logger.info('Starting Run :)')


def plot_anova(subset, dic):
    fig = plt.figure()

    col = ['r', 'g', 'b']
    for i, tav in enumerate(['temper', 'arousal', 'valence']):
        plt.subplot(1, 3, i + 1)

        # t_c = dic[tav]['counts']
        # plt.legend('Temper {} {} {}'.format(t_c[0] / t_c.sum(), t_c[1] / t_c.sum(), t_c[2] / t_c.sum()), loc='upper left')
        # plt.plot(-5, -5, '')

        plt.xticks([-1, 0, 1])
        plt.yticks([-1, -0.5,  0, 0.5, 1])
        plt.ylim([-1, 1])
        plt.xlim([-1.5, 1.5])
        for cl in [-1, 0, 1]:
            plt.errorbar(cl, dic[tav][cl]['mean'], dic[tav][cl]['std'], marker='s', mfc='red',
                     mec='green', ms=20, mew=4, color=col[i])
        plt.title(tav + ' anova_p = {}'.format(dic[tav]['anova_p']))
        plt.title(tav, fontsize=16)
    # plt.legend(('Temper r={}; p={}'.format(
    #     np.round(dic['temper'][0], 2), np.format_float_scientific(dic['temper']['anova_p'], exp_digits=3, precision=1)
    # ), 'Arousal r={}; p={}'.format(
    #     np.round(dic['arousal'][0], 2), np.format_float_scientific(dic['arousal']['anova_p'], exp_digits=3, precision=1)
    # ), 'Valence r={}; p={}'.format(
    #     np.round(dic['valence'][0], 2), np.format_float_scientific(dic['valence']['anova_p'], exp_digits=3, precision=1)
    # )), loc='upper left')

    mng = plt.get_current_fig_manager()
    mng.window.showMaximized()
    # mng.frame.Maximize(True)
    plt.show(block=False)
    t=1
    # plt.savefig(PATCfg.OUTPUT_FOLDER + '_anova_graph_' + subset + '.png')


def plot_correlation(subset, dic):
    fig = plt.figure()
    ax = fig.add_axes((0.1, 0.2, 0.8, 0.7))
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    ax.spines['left'].set_color('none')
    ax.spines['bottom'].set_color('none')
    ax.set_ylim([-1, 1])
    ax.set_xlim([-1, 1])
    ax.set_xticks([])
    ax.set_yticks([])
    ax.axes.get_xaxis().set_visible(False)
    ax.axes.get_yaxis().set_visible(False)
    x = np.linspace(-1, 1, 1000)
    # plt.annotate('Correlation', xy=(0.5, 0.5), arrowprops=dict(arrowstyle='->'), xytext=(15, -10))
    # ax.text(-0.5, 0.5, '{}\nCorrelation'.format(subset))
    col = ['r', 'g', 'b']
    list = [dic['temper'][0], dic['arousal'][0],dic['valence'][0]]
    list2 = np.flip(np.argsort(list))
    leg = []
    for i, tav in enumerate(['temper', 'arousal', 'valence']):
        plt.plot(x, x*dic[tav][0], linestyle='-', color=col[i])
        # plt.text(0.2 + list2[i]*0.2, list2[i]*0.2*dic[tav][0],
        leg += [np.round(dic[tav][0], 2), np.format_float_scientific(dic[tav][1], exp_digits=3, precision=1)]
        l2 = np.array((0.4, 0.4 * dic[tav][0]))
        trans_angle = plt.gca().transData.transform_angles(np.array(((45 * dic[tav][0]),)),
                                                           l2.reshape((1, 2)))[0]
        # plt.text(l2[0], l2[1], '{}'.format(tav), fontsize=11,
        #          rotation=trans_angle, rotation_mode='anchor')
    plt.legend(('Temper r={}; p={}'.format(
        np.round(dic['temper'][0], 2), np.format_float_scientific(dic['temper'][1], exp_digits=3, precision=1)
    ), 'Arousal r={}; p={}'.format(
        np.round(dic['arousal'][0], 2), np.format_float_scientific(dic['arousal'][1], exp_digits=3, precision=1)
    ), 'Valence r={}; p={}'.format(
        np.round(dic['valence'][0], 2), np.format_float_scientific(dic['valence'][1], exp_digits=3, precision=1)
    )), loc='upper left')

    plt.plot(x, np.zeros(x.shape), color='k')
    plt.plot(np.zeros(x.shape), x, color='k')
    plt.title('{}\nPearson Correlation'.format(subset))
    plt.plot(x, x, 'k-')
    plt.show(block=False)

    t=1
    # plt.savefig(PATCfg.OUTPUT_FOLDER + '_r_graph_' + subset + '.png')


def predict_three_class(hld_result_df, path_dict, predictor, hld_np_matrix, run_typ, writer=None):
    # #################################################
    # ############ PREDICT  WITH  FULL  MATRIX  #######
    # #################################################
    vad_vec = hld_result_df['vad'].values
    ts_vec = hld_result_df['ts'].values

    # prepare ground_truth
    gt = pd.read_csv(path_dict['folder_path'] + 'val_set_MIX5.3.0.0.csv')
    gt = gt[['wavemd5', 'temper', 'arousal', 'valence']]

    for th_h in [0.5, 0.6, 0.7]:  # np.linspace(0.8, 0.5, 30, endpoint=False): #
        th_l = th_h - 0.0
        th_dict = {'temper': {'high_upper': th_h, 'high_lower': th_l, 'low_upper': th_h, 'low_lower': th_l},
                   'arousal': {'high_upper': th_h, 'high_lower': th_l, 'low_upper': th_h, 'low_lower': th_l},
                   'valence': {'high_upper': th_h, 'high_lower': th_l, 'low_upper': th_h, 'low_lower': th_l}}

        pred_res = predictor.predict_from_hld_matrix(hld_np_matrix, vad_vec, ts_vec, th_dict=th_dict)

        merge_df = pd.concat([hld_result_df, pred_res], axis=1)  # , sort=False)
        # merge_df.to_csv(path_dict['folder_path'] + 'mid_res_073_073.csv', index=None)

        merge_df = merge_df[['wavemd5', 'do_predict', 'temper_label', 'arousal_label', 'valence_label']]

        mrg2 = pd.merge(left=merge_df, right=gt, on='wavemd5')

        for tav in ['temper', 'arousal', 'valence']:
            df = mrg2.copy()
            # print('*********************** {} ********'.format(tav))
            df = df.drop(df[df[tav] == 'ambiguous'].index)
            df = df.drop(df[df[tav + '_label'] == 'ambiguous'].index)
            classes = np.unique(df[tav].values)

            if tav == 'valence':
                o = [0, 1, 2, 3]
            else:
                o = [1, 2, 0, 3]
            res = AuxFunc.calc_metrics(df[tav].values, df[tav + '_label'].values, classes)

            l = np.round(res['recall'][o[0]], 2)
            lc = res['count'][o[0]]
            m = np.round(res['recall'][o[1]], 2)
            mc = res['count'][o[1]]
            h = np.round(res['recall'][o[2]], 2)
            hc = res['count'][o[2]]
            tot = np.round(res['recall'][o[3]], 2)
            acc = np.round(res['accuracy'], 2)
            totc = np.array(res['count']).sum()

            print('th_h = {}; recall low = {} ({}) ;med =  {} ({}) ; high = {} ({}) ; total = {} ({}) acc = {}'.format(
                th_h, l, lc, m, mc, h, hc, tot, totc, acc))

            # print('Adaptation is {}'.format(predictor.PRED.enforce_adaptation))
            writer.writerow([run_typ, tav, th_h, l, lc, m, mc, h, hc, tot, totc, acc])


def calc_stats_on_tav_score(df, tav, writer=None):

    df = df.dropna()

    TH_GT = [1.33]
    TH_PRED = [0.2, 0.5, 0.6]
    if tav == 'temper':
        vec = ['', 'Spearman', '','', 'Pearson', '', '', '','', '', 'gt_discrete - predict cont']
        for _ in TH_GT:
            vec += ['']
        vec += ['', '', 'gt_cont, pred discrete']
    else:
        vec = ['']
    if writer:
        writer.writerow(vec)

    res = [tav]
    res_title = ['']

    pred_cont = df[tav + '_score'].values
    gt_cont = df[tav + '_mean'].values

    # Spearman
    r_spearman, p_spearman = stats.spearmanr(pred_cont, gt_cont)
    res += [np.round(r_spearman, 2), p_spearman]
    res_title += ['r_spearman', 'p_spearman']

    # Pearson
    r_pearson, p_pearson = stats.pearsonr(pred_cont, gt_cont)

    res += ['', np.round(r_pearson, 2), p_pearson]
    res_title += ['', 'r_pearson', 'p_pearson']

    res += ['N = {}'.format(len(pred_cont))]
    res_title += ['gt_3class predictor_continuous']

    for th_gt in TH_GT:
        # gt three class

        if tav == 'valence':
            th_gt = 1.51


        df = df.assign(gt_three_class=0)
        df.loc[df[tav + '_mean'] < -th_gt, 'gt_three_class'] = -1
        df.loc[df[tav + '_mean'] > th_gt, 'gt_three_class'] = 1

        classes, counts = np.unique(df.gt_three_class.values, return_counts=True)
        tot = counts.sum()
        print('classes: {0} ({1}, {2:.2f}%), {3} ({4}, {5:.2f}%), {6} ({7}, {8:.2f}%)'.format(
            classes[0], counts[0], counts[0]/tot, classes[1], counts[1], counts[1]/tot, classes[2], counts[2], counts[2]/tot))

        _, anova_pred_ct_gt_3c_p = stats.f_oneway(pred_cont, df['gt_three_class'].values)
        if anova_pred_ct_gt_3c_p < 0.01:
            anova_pred_ct_gt_3c_p = np.format_float_scientific(anova_pred_ct_gt_3c_p, exp_digits=3, precision=2)
        else:
            anova_pred_ct_gt_3c_p = np.format_float_positional(anova_pred_ct_gt_3c_p, unique=False, precision=4)

        _, counts = np.unique(df.gt_three_class.values, return_counts=True)

        res += ['{} ({}, {}, {})'.format(anova_pred_ct_gt_3c_p, counts[0], counts[1], counts[2])]
        res_title += ['th = {}'.format(th_gt)]

        anova_dict = {}
        for cl in classes:
            tmp = df[df['gt_three_class'] == cl]
            tmp_score = tmp[tav + '_score'].values

            anova_dict.update({cl: {'mean': np.mean(tmp_score), 'std': np.std(tmp_score)}})
        anova_dict.update({'anova_p': anova_pred_ct_gt_3c_p, 'counts': counts})
    # res += [' ', 'N = {}'.format(len(pred_cont))]
    # res_title += [' ', 'gt_continuous predictor_3class']

    # for th_pred in TH_PRED:
    #     # gt three class
    #     df = df.assign(pred_three_class=0)
    #     df.loc[df[tav + '_score'] < -th_pred, 'pred_three_class'] = -1
    #     df.loc[df[tav + '_score'] > th_pred, 'pred_three_class'] = 1
    #
    #     _, anova_pred_3c_gt_ct_p = stats.f_oneway(gt_cont, df['pred_three_class'].values)
    #     if anova_pred_3c_gt_ct_p < 0.01:
    #         anova_pred_3c_gt_ct_p = np.format_float_scientific(anova_pred_3c_gt_ct_p, exp_digits=3, precision=2)
    #     else:
    #         anova_pred_3c_gt_ct_p = np.format_float_positional(anova_pred_3c_gt_ct_p, unique=False, precision=4)
    #
    #     _, counts = np.unique(df.pred_three_class.values, return_counts=True)
    #
    #     res += ['{} ({}, {}, {})'.format(anova_pred_3c_gt_ct_p, counts[0], counts[1], counts[2])]
    #     res_title += ['th = {}'.format(th_pred)]
    #
    # res += [' ', 'N = {}'.format(len(pred_cont))]
    # res_title += [' ', 'chi_square']

    # for th_gt in TH_GT:
    #     for th_pred in TH_PRED:
    #         df = df.assign(gt_three_class=0)
    #         df.loc[df[tav + '_mean'] < -th_gt, 'gt_three_class'] = -1
    #         df.loc[df[tav + '_mean'] > th_gt, 'gt_three_class'] = 1
    #
    #         df = df.assign(pred_three_class=0)
    #         df.loc[df[tav + '_score'] < -th_pred, 'pred_three_class'] = -1
    #         df.loc[df[tav + '_score'] > th_pred, 'pred_three_class'] = 1
    #
    #         _, chi_square = stats.f_oneway(df['gt_three_class'].values, df['pred_three_class'].values)
    #         res += ['{}'.format(chi_square)]
    #         res_title += ['pred = {}; gt = {}'.format(th_pred, th_gt)]

    print(res_title)
    print(res)
    if writer:
        writer.writerow(res_title)
        writer.writerow(res)

    return (r_pearson, p_pearson), anova_dict


def calc_stats_on_cv(df, tav, writer, label, raw):

    r_sc, p_sc = stats.pearsonr(df[tav + '_mean'].values, df.DistNormed.values)
    r_dist, p_dist = stats.pearsonr(df[tav + '_mean'].values, df.score.values)
    r_s2 = np.round(r_sc, 2)
    r_dist2 = np.round(r_dist, 2)
    if p_sc < 0.0001:
        p_sc2 = np.format_float_scientific(p_sc, exp_digits=3, precision=2)
    else:
        p_sc2 = np.round(p_sc, 3)
    if p_dist < 0.0001:
        p_dist2 = np.format_float_scientific(p_dist, exp_digits=3, precision=2)
    else:
        p_dist2 = np.round(p_dist, 3)
    print('************************** tav = {} r score/dist {}/{}, p score/dist {}/{}'.format(tav, r_sc, r_dist, p_sc2, p_dist2))

    writer.writerow(['tav', 'r_score', 'r_dist', 'p_score', 'p_dist'])
    writer.writerow([tav, r_s2, r_dist2, p_sc2, p_dist2])

    # labelrs
    # raw2 = pd.merge(left=df, right=raw, on='wavemd5')
    # raw2 = raw2[[tav + '_mean_x', tav + '_all', tav + '_raw']]

    raw2 = raw[[tav + '_mean_x', tav + '_all']].copy()
    # raw2 = raw2.drop(raw2[raw2[tav + '_mean_x'] == -100].index, inplace=True)

    label_reduction_dict = \
        {'temper': {'low': -3, 'semi-low': -2, 'semi_low': -2, 'medium': 0, 'semi-high': 2,
                    'high': 3, 'medium_high': 2, 'semi_high': 2, 'ambiguous': np.nan, '0': ''},
         'arousal': {'very_low_activation': -3, 'low_activation': -2,
                     'neutral': 0, 'activated': 2, 'very_activated': 3,
                     'ambiguous': np.nan, '0': ''},
         'valence': {'negative': -3, 'semi-negative': -2, 'semi_negative': -2,
                     'neutral': 0, 'semi-positive': 2, 'positive': 3,
                     'semi_positive': 2, 'ambiguous': np.nan, '0': ''}}
    N = 10
    for nn in np.arange(0, N):
        # raw2 = raw2.assign(label2num=-1)#0*np.ones([1, 10]))
        raw2['rand_' + str(nn)] = -1
    for i, val in enumerate(raw2[tav + '_all'].values):
        curr_vals = np.array(val.split(';'))
        if (i % 2000) == 0:
            print('i = {}'.format(i))
        curr_vals = curr_vals[curr_vals != '0']  # TODO, 0 is legal now
        new_list = []
        for j in range(len(curr_vals)):
            new_list += [label_reduction_dict[tav][curr_vals[j].lower()]]
            curr_vals[j] = label_reduction_dict[tav][curr_vals[j].lower()]
        for nn in np.arange(0, N):
            rand_item = new_list[random.randrange(len(new_list))]
            raw2.loc[i, 'rand_' + str(nn)] = rand_item

    r = []
    p = []
    raw2 = raw2.dropna()
    for nn in np.arange(0, N):
        r1, p1 = stats.pearsonr(raw2['rand_' + str(nn)].values, raw2[tav + '_mean_x'].values)
        r += [r1]
        p += [p1]
    print('mean_r = {}; mean_p = {} r = {}'.format(np.mean(np.array(r)), np.std(np.array(r)), r))


def main(writer, wav_dict, run_typ=None, path_dict=None):
    wav_list = wav_dict['wav_list']
    wav_no_path = wav_dict['wav_no_path']
    wav_md5 = wav_dict['wavemd5']

    # Get Predict Titles - if other titles needed, see AuxFunc
    if run_typ is 'thor':
        writer.writerow(AuxFunc.write_to_thor(op_code='titles'))
    elif run_typ is 'summary':
        writer.writerow(AuxFunc.summary_detector_to_df(op_code='titles'))
    elif run_typ is 'test':
        writer.writerow(AuxFunc.predict_detector_to_df(op_code='titles'))

    lld_calc = PATCalculator()
    hld_calc = HLDCalculator()
    predictor = PredictDetector()
    predictor.PRED.enforce_adaptation = path_dict['adaptation']
    cur_logger.warning('Adaptation is {}'.format(predictor.PRED.enforce_adaptation))

    # Loop over files
    wav_num = 0
    hld_result_dict = {}
    hld_result_df = pd.DataFrame(columns=['new', 'i', 'wav', 'wavemd5', 'ts', 'vad'])

    hld_np_matrix = np.empty([0, PATCfg.ENGINE_CONF['MAT_SIZE']])  # D4, no dd

    # #################################################
    # ############ LOAD META DF AND FULL MATRIX #######
    # #################################################
    if os.path.isfile(path_dict['matrix_out_file']) and not PATCfg.SKIP_HLD_SHORTCUTS:
        with open(path_dict['matrix_out_file'], 'rb') as f:
            hld_result_df, hld_np_matrix = pickle.load(f)
        cur_logger.info('loading DF from {}; matrix shape {}'.format(path_dict['matrix_out_file'], hld_np_matrix.shape))
        cur_logger.warning('LOADING MATRIX FILE!! path = {}'.format(path_dict['matrix_out_file']))
    else:
        for i, wav in enumerate(wav_list):
            wav_num += 1
            # if not (i % 50):
            cur_logger.info('wav_num = {}/{};'.format(i, len(wav_list)))
            if 'stop_conditions' in path_dict.keys():
                if i > path_dict['stop_conditions']['i']:
                    break
                if path_dict['stop_conditions']['wav']:
                    print('cur = {}; stop = {}'.format(wav_no_path[i], path_dict['stop_conditions']['wav']))
                    if wav_no_path[i] != path_dict['stop_conditions']['wav']:
                        continue
            cur_logger.debug('wav_num = {}/{}; wav = {}; md5 = {}'.format(i, len(wav_list), wav[:-4], wav_md5[i]))

            signal, fs = AuxFunc.read_wav(wav, PATCfg.FRAME_SIZE_FEATURES, PATCfg.SAMPLING_RATE)
            if signal is None or fs is None:
                continue
            else:

                # Check if offset...
                if 'offset' in wav_dict.keys():
                    cur_offset = wav_dict['offset'][i]
                    if np.isnan(cur_offset):
                        signal = signal[:fs * 10]
                    else:  # nan
                        cur_offset_sec = int(cur_offset * fs / 1000)
                        signal = signal[cur_offset_sec: cur_offset_sec + fs * 10]

                hld_exist = AuxFunc.save_load_hld(path_dict['hld_folder'], wav_no_path[i][:-4])
                if not hld_exist or PATCfg.SKIP_HLD_SHORTCUTS:  # calc hld and save it
                    # PAT
                    lld_dictionary, lld_params = lld_calc.process_chunk(signal)  # calc basic lld features
                    # HLD
                    lld_dict_augmented, hld_features, ts, vad_list, secs = hld_calc.process_matrix(lld_dictionary, lld_params)
                    hld2save = lld_dict_augmented, hld_features, ts, vad_list
                    AuxFunc.save_load_hld(path_dict['hld_folder'], wav_no_path[i][:-4], hld2save)
                    hld_result_dict.update({i: hld2save})
                else:  # hld loaded
                    hld_result_dict.update({i: hld_exist})
                    _, hld_features, ts, vad_list = hld_exist

            for k in range(len(hld_features)):
                if run_typ is 'test':  # single file
                    predict_res_old = predictor.predict_from_hld(hld_features[k], vad_list[k], ts[k])
                    predict_row = AuxFunc.predict_to_df(predict_res_old, wav_no_path[i][:-4], wav_md5[i])
                    writer.writerow(predict_row)
                else:
                    vad_s = round(100 * vad_list[k].mean(), 0)

                    hld_result_df.loc[len(hld_result_df)] = [int(not bool(k)), i, wav_no_path[i], wav_md5[i], ts[k], vad_s]
                    hld_np_matrix = np.vstack((hld_np_matrix, hld_features[k]))
            lld_calc.clean_up()  # clean buffers etc.

        with open(path_dict['matrix_out_file'], 'wb') as f:
            pickle.dump((hld_result_df, hld_np_matrix), f)
        cur_logger.info('saving matrix to {}; matrix size {}'.format(path_dict['matrix_out_file'], hld_np_matrix.shape))
        print('matrix shape = {}'.format(hld_np_matrix.shape))

    # #################################################
    # ############ PREDICT  WITH  FULL  MATRIX  #######
    # #################################################
    # predict_three_class(hld_result_df, path_dict, predictor, hld_np_matrix, run_typ, writer=writer)

    vad_vec = hld_result_df['vad'].values
    ts_vec = hld_result_df['ts'].values

    # prepare ground_truth
    gt = pd.read_csv(path_dict['folder_path'] + 'val_set_MIX5.3.0.0.csv')
    gt = gt[['wavemd5', 'temper', 'arousal', 'valence']]

    th_h = 0.5
    th_l = th_h
    th_dict = {'temper': {'high_upper': th_h, 'high_lower': th_l, 'low_upper': th_h, 'low_lower': th_l},
               'arousal': {'high_upper': th_h, 'high_lower': th_l, 'low_upper': th_h, 'low_lower': th_l},
               'valence': {'high_upper': th_h, 'high_lower': th_l, 'low_upper': th_h, 'low_lower': th_l}}
    pred_res = predictor.predict_from_hld_matrix(hld_np_matrix, vad_vec, ts_vec, th_dict=th_dict)
    merge_df = pd.concat([hld_result_df, pred_res], axis=1)  # , sort=False)
    mrg2 = pd.merge(left=merge_df, right=gt, on='wavemd5')

    det_auc = []
    anova_dict = {}
    pearson_dict = {}
    df_dict = {}
    raw_labels = pd.read_csv(PATCfg.OUTPUT_FOLDER + 'detectors_datasets/train_set_unfiltered_no100.csv')

    for i, tav in enumerate(['temper', 'arousal', 'valence']):
        mrg3 = mrg2.copy()
        for j, tav_class in enumerate(['high', 'low', 'cv']):
            tav_tav = tav + '_' + tav_class

            df_dict.update({tav_tav: pd.DataFrame([])})
            writer.writerow(['database', 'cv AUC', 'detector', 'th', 'sensitivity', 'specificity', 'accuracy'])

            cur_gt = mrg2[tav].values == HLDCfg.tav_dict[tav][tav_class]
            cur_score = mrg2[tav_tav + '_score'].values
            det_auc += [np.round(roc_auc_score(cur_gt, cur_score, average='macro'), 3)]

            eer_predict, eer_thresh = LinSig.predict_at_EER(cur_gt, cur_score)

            df_dict[tav_tav] = df_dict[tav_tav].assign(filename=mrg2['wav'].values)
            df_dict[tav_tav] = df_dict[tav_tav].assign(wavemd5=mrg2['wavemd5'].values)
            df_dict[tav_tav] = df_dict[tav_tav].assign(gt=mrg2[tav].values)
            df_dict[tav_tav] = df_dict[tav_tav].assign(gt_detector=cur_gt)
            df_dict[tav_tav] = df_dict[tav_tav].assign(score=cur_score)
            for cur_th in [0.5, 0.6, 0.7, 0.8]:
                cm = sklearn.metrics.confusion_matrix(cur_gt, cur_score > cur_th, [True, False])
                # print(cm)
                res = AuxFunc.calc_metrics(cur_gt, cur_score > cur_th, [True, False])
                sens = np.round(res['recall'][1], 2)
                spec = np.round(res['recall'][0], 2)

                df_dict[tav_tav] = df_dict[tav_tav].assign(th=cur_score > cur_th)
                df_dict[tav_tav] = df_dict[tav_tav].rename(columns={'th': 'th_' + str(cur_th)})

                # #################################################
                # ############        CV EVALUATION         #######
                # #################################################

                cv = pd.read_csv(PATCfg.OUTPUT_FOLDER + 'CV/' + tav_class + '_' + tav + '/CV_info.csv')
                cv_score = cv.score.values
                cv_gt = np.array(cv.TrueLabel.values, dtype=bool)
                cv_auc = roc_auc_score(cv_gt, cv_score, average='macro')

                # cv_eer_predict, cv_eer_thresh = LinSig.predict_at_EER(cv_gt, cv_score)
                # if tav_tav == 'temper_high':
                #     cv_score = predictor.PRED.predict_from_lin_dist(ML.HighTemperA, ML.HighTemperB, ML.HighTemperSig,
                #                                                     cv.DistNormed.values)
                # elif tav_tav == 'temper_low':
                #     cv_score = predictor.PRED.predict_from_lin_dist(ML.LowTemperA, ML.LowTemperB, ML.LowTemperSig,
                #                                                     cv.DistNormed.values)
                # else:
                #     cv_score = np.nan
                # print('CV AUC for {} = {}; eer_th = {}'.format(tav_tav, cv_auc, cv_eer_thresh))
                # cv_res = AuxFunc.calc_metrics(cv_gt, cv_score >= cur_th, [True, False])
                # cv_sens = np.round(cv_res['recall'][1], 2)
                # cv_spec = np.round(cv_res['recall'][0], 2)
                # print('{}: th = {} sens = {} spec = {} acc = {}; cv_sens = {}; cv_spec = {}'.format(
                #     tav_tav, cur_th, sens, spec, np.round(res['accuracy'], 2), cv_sens, cv_spec))
                writer.writerow([run_typ, cv_auc, tav_tav, cur_th, sens, spec, np.round(res['accuracy'], 2)])
            # df_dict[tav_tav].to_csv(PATCfg.OUTPUT_FOLDER + run_typ + '_' + tav_tav + '.csv', index=None)

        mrg3 = mrg3[['wavemd5', tav + '_score', tav]]
        gt2 = pd.read_csv(PATCfg.OUTPUT_FOLDER + 'detectors_datasets/' + tav + '_val_set_MIX5.5.0.4.csv', encoding='utf-8')
        gt2 = gt2[['wavemd5', tav + '_mean']]
        mrg4 = pd.merge(left=mrg3, right=gt2, on='wavemd5')
        pearson_dict[tav], anova_dict[tav] = calc_stats_on_tav_score(mrg4.copy(), tav)
        print('{}; AUC = {}; av = {}'.format(run_typ, np.round(det_auc, 2), np.round(np.mean(det_auc), 2)))
        writer.writerow([run_typ, 'AUC = ', det_auc])
        writer.writerow([''])

    # plot_anova(run_typ, anova_dict)
    # plot_correlation(run_typ, pearson_dict)
    t=1

if __name__ == "__main__":
    start_time = time.time()
    total_df = pd.DataFrame([])
    sets = ['mayo', 'scripps', 'youtube']

    with open(PATCfg.OUTPUT_FOLDER + 'stats.csv', 'w', newline='') as file:
        csv_writer = csv.writer(file, delimiter=',')
        for analysis_selector in sets:
            csv_writer.writerow(['Set:', analysis_selector])
            csv_writer.writerow([' '])
            path_dictionary = {}
            wav_dictionary = {}
            root = 'C:/BVC/Pepsico/converted_wav/'

            if analysis_selector is 'mayo' or analysis_selector is 'scripps' or analysis_selector is 'youtube':
                out_path = 'C:/BVC/Projects/' + analysis_selector + '/64/'
                if not os.path.isdir(out_path):
                    os.mkdir(out_path)
                    time.sleep(1)
                    print('CREATING DIR {}'.format(out_path))
                if not os.path.isfile(out_path + '/val_set_MIX5.3.0.0.csv'):
                    copyfile('C:/BVC/Projects/' + analysis_selector + '/val_set_MIX5.3.0.0.csv', out_path + 'val_set_MIX5.3.0.0.csv')
                    print('copy from {} -> {}'.format('C:/BVC/Projects/' + analysis_selector + '/val_set_MIX5.3.0.0.csv', out_path))

                path_dictionary = {'folder_path': out_path,
                                   'hld_folder': out_path + 'hld_' + analysis_selector + '/',
                                   'output_file': out_path + analysis_selector + '_out.csv'}
                path_dictionary.update({'matrix_out_file': path_dictionary['output_file'][:-4] + '.pkl'})
                path_dictionary.update({'adaptation': True})
                wav_dictionary = AuxFunc.create_db_from_source_name(analysis_selector, out_path + 'val_set_MIX5.3.0.0.csv')
            elif analysis_selector is 'test':
                out_path = 'C:/BVC/Projects/' + analysis_selector + '/'
                path_dictionary = {'folder_path': out_path,
                                   'hld_folder': out_path + 'hld_' + analysis_selector + '/',
                                   'output_file': out_path + analysis_selector + '_out.csv'}
                path_dictionary.update({'matrix_out_file': path_dictionary['output_file'][:-4] + '.pkl'})
                wav_dictionary = AuxFunc.get_wav_list_from_dir(out_path + 'wavs/')

            if not os.path.isdir(path_dictionary['hld_folder']):
                os.mkdir(path_dictionary['hld_folder'])

            path_dictionary.update({'stop_cond': {'d': 3000000000, 'wav': None}})

            cur_logger.info('\n\n*********************\nANALYZING HOLDOUT: {}\n********************\n '.format(analysis_selector))

            main(csv_writer, wav_dictionary, analysis_selector, path_dictionary)

        cur_logger.info('Done. {0:.1f} second runtime ({1:.1f} '
                        'minutes).'.format(time.time() - start_time, (time.time() - start_time)/float(60)))
