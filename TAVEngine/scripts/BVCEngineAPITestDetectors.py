import csv
import logging.config
import os
from PATrends import PATConfig as PATCfg
from TAVCommon import AuxFunctions as Aux, log_conf
import pandas as pd
from TAVEngine.BVCEngineAPI import BVCEngineAPI as Api

logging.config.dictConfig(log_conf.create_log_dict_no_file(logging.INFO))
cur_logger = logging.getLogger('BVCEngineAPI - v5.5 TEST')


class BVCEngineAPITest:
    def __init__(self):
        self.api = Api()

    def main_from_single_file_demo(self, file_path, df):

        cur_logger.info('Starting API test *** from SINGLE file FOR DEMO ***')
        e1 = self.api.analyze_file_demo(file_path, df)
        self.api.end_session(calculate_summary=False)
        return e1

    def main_from_single_file(self, single_file, out_file):
        cur_logger.info('Starting API test *** from SINGLE wav file ***')
        e1 = self.api.analyze_file(single_file)
        e2 = self.api.end_session()
        return e2

    def main_from_file(self, in_file, out_file):
        cur_logger.info('Starting API test *** from file ***')
        e1 = self.api.analyze_batch(in_file)
        e2 = self.api.end_session()

    def main_from_dir(self, dir_in, out_file):
        cur_logger.info('Starting API test *** from directory {} ***'.format(dir_in))
        with open(out_file, 'w') as file:
            csv_writer = csv.writer(file, delimiter=',')
            e1 = self.api.analyze_batch(dir_in)
            e2 = self.api.end_session_matrix(e1)
            e2.to_csv(out_file, index=None)
            import pandas as pd
            e2_df = pd.DataFrame.from_dict(e2)

    def main_given_wav(self, wavs, out_file):
        cur_logger.info('Starting API test *** from chunks ***')
        with open(out_file, 'w') as file:
            csv_writer = csv.writer(file, delimiter=',')
            for cur_wav in wavs:

                signal, fs = Aux.read_wav(cur_wav, PATCfg.FRAME_SIZE_FEATURES, PATCfg.SAMPLING_RATE, resample=True)
                cur_logger.info('wav = {}, length = {} sec'.format(cur_wav, len(signal)/fs))  # TODO YS PRINT

                chunk_list = Aux.chunker(signal, fs, 2)
                for index, chunk in enumerate(chunk_list):
                    e1 = self.api.process_chunk(chunk)

                    if e1 is not None:
                        print(e1)

                e2 = self.api.end_session()
                print('************************')
                print(e2)


if __name__ == "__main__":

    Engine = BVCEngineAPITest()

    # source = 'single_file_demo'
    source = 'chunk'

    if source == 'chunk':
        out_file_chunk = 'C:/BVC/test/rosie8k_short.csv'
        Engine.main_given_wav(['C:/BVC/test/rosie8k_short.wav'], out_file_chunk)

    elif source == 'test_vitality_demo':
        most = pd.read_csv('C:/BVC/test/most.csv')
        wav_list_from_folder = []
        i = 0
        for cur_file in most.FileName.values:
            if i < 165:
                i += 1
                continue
            # wav_list_from_folder.append('C:/DownloadedVoices/' + cur_file + '.wav')
            df = pd.read_csv('C:/BVC/test/demo1.csv')[['seg_start'] + ['seg_end'] + ['legend']]
            file_path_in = 'BVC/DownloadedVoices/' + cur_file + '.wav'
            # file_path_in = 'BVC/test/test1.wav'
            print('i={}'.format(i))
            res = Engine.main_from_single_file_demo(file_path_in, df)
            cur_logger.info('i = {}; Vitality result for {} is {}'.format(i, file_path_in, res.vitality.values))
            i += 1

    elif source == 'dir':

        out_file_dir = 'C:/BVC/Projects/Starkey/Starkey/tst_summ.csv'
        target_dir = 'C:/BVC/Projects/Starkey/Starkey/'
        Engine.main_from_dir(target_dir, out_file_dir)

    elif source == 'dir2':
        out_file_dir = 'C:/BVC/BVCortex/SandBox/api_test/test_from_dir2.csv'
        target_dir = 'C:/BVC/BVCortex/SandBox/api_test/test_waves/'
        Engine.main_from_dir(target_dir, out_file_dir)

    elif source == 'from_csv':
        out_file_file = 'C:/BVC/BVCortex/SandBox/api_test/test_from_file.csv'
        wavs_file = 'C:/BVC/BVCortex/SandBox/mayo_metadata/api_test.csv'
        Engine.main_from_file(wavs_file, out_file_file)

    elif source == 'single_wav':
        # out_file_file = 'C:/BVC/test/812093000658757.csv'
        # single_wav_file = 'C:/BVC/test/812093000658757.wav'
        out_file_file = 'C:/BVC/Projects/Starkey/Starkey/single.csv'
        single_wav_file = 'C:/BVC/Projects/Starkey/Starkey/recording_1541590029720.wav'
        Engine.main_from_single_file(single_wav_file, out_file_file)

    elif source == 'single_file_demo':
        # df = pd.read_csv('C:/BVC/test/demo2.csv')[['seg_start'] + ['seg_end'] + ['legend']]
        df = pd.read_csv('C:/BVC/test/rozi.csv')[['seg_start'] + ['seg_end'] + ['legend']]
        # file_path_in = 'BVC/test/812093000658757.wav'
        # file_path_in = 'BVC/DownloadedVoices/14456bfc-c302-4dd5-baf6-e8304cf1f6ac.wav'
        # file_path_in = 'BVC/DownloadedVoices/3d8dca23-9369-4e7a-ac0e-c9057da15e03.wav'
        file_path_in = 'BVC/test/RosieORIG.wav'
        # file_path_in = 'BVC/test/test1.wav'

        res = Engine.main_from_single_file_demo(file_path_in, df)
        cur_logger.info('Vitality result for {} is {}'.format(file_path_in, res.vitality.values))
    elif source == 'single_file_server':
        out_file_file = 'E:/Algo/Yonatan/test_results/Recording_mono.wavout1.csv'
        single_wav_file = 'E:/Algo/Yonatan/test_waves/test1.wav'
        res = Engine.main_from_single_file(single_wav_file, out_file_file)
