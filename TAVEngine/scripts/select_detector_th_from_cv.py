import pandas as pd
import numpy as np
from TAVTrainer import ConfigTraining as Cfg
import TAVCommon.AuxFunctions as AuxFunc
import csv
from sklearn.metrics import confusion_matrix
TAV = ['temper', 'arousal', 'valence']
CLASSES = ['high', 'low']
# TAV = ['temper']
# CLASSES = ['high']


def get_metrics_two_class(cur_res, print_me=False, th=None):
    f1 = np.round(cur_res['recall'][0], 2)
    fc1 = cur_res['count'][0]
    t1 = np.round(cur_res['recall'][1], 2)
    tc1 = cur_res['count'][1]
    tot1 = np.round(cur_res['accuracy'], 2)
    totc1 = np.array(cur_res['count']).sum()
    if print_me:
        print('tav = {}; th_h = {}; sens(F)  = {} ({}); sens(T) = {} ({}) ; acc = {} ({});'.format(
            tav_tav, np.round(th, 3), f1, fc1, t1, tc1, tot1, totc1))

    return f1, fc1, t1, tc1, tot1, totc1


def get_metrics_three_class(tav, cur_res, print_me=False, th=None):
    if tav == 'valence':
        oo = [0, 1, 2, 3]
    else:
        oo = [1, 2, 0, 3]

    l1 = np.round(cur_res['recall'][oo[0]], 2)
    lc1 = cur_res['count'][oo[0]]
    m1 = np.round(cur_res['recall'][oo[1]], 2)
    mc1 = cur_res['count'][oo[1]]
    h1 = np.round(res['recall'][oo[2]], 2)
    hc1 = cur_res['count'][oo[2]]
    recall_macro = np.round(res['recall'][oo[3]], 2)
    tot1 = np.round(cur_res['accuracy'], 2)
    totc1 = np.array(cur_res['count']).sum()
    if print_me:
        print('th = {}; low = {} ({}) ; med = {} ({}) ; high = {} ({}) ; acc = {} ({}) macro {}'.format(
            th, l1, lc1, m1, mc1, h1, hc1, tot1, totc1, recall_macro))
    return l1, lc1, m1, mc1, h1, hc1, tot1, totc1, recall_macro


GT_TH = {'temper': 1.33, 'arousal': 1.33, 'valence': 1.51}

gt = pd.read_csv(Cfg.OUT_FOLDER + 'detectors_datasets/labels_data_MIX5.5.0.1.csv')
gt = gt[['wavemd5', 'temper_mean', 'arousal_mean', 'valence_mean']]
print('** USING NEW GT ***')

gt_old = pd.read_csv(Cfg.OUT_FOLDER + 'labels_data_MIX5.3.0.0.csv')
gt_old = gt_old[['wavemd5', 'temper', 'arousal', 'valence']]
# if cur_class is 'high':
#     df[tav + '_gt'] = df[tav + '_mean'].values >= GT_TH[tav]
# else:
#     df[tav + '_gt'] = df[tav + '_mean'].values <= -GT_TH[tav]
#
# true_cnt = df[tav + '_gt'].values.sum()
# print('******* ground_truth: {} / {} are ''true'' for TH = {}  ******** '.format(
#     true_cnt, len(df[tav + '_gt'].values), GT_TH[tav]))

with open(Cfg.OUT_FOLDER + 'th_select_cv2.csv', 'w', newline='') as file:
    csv_writer = csv.writer(file, delimiter=',')
    # csv_writer.writerow(['tav', 'th', 'false %', 'f_cnt', 'True %', 't_cnt', 'accuracy', 'total_cnt', 'gt_true'])
    csv_writer.writerow(['tav', 'th_h', 'th_l', 'low', 'l_p_c', 'l_gt_c', 'med', 'm_p_c', 'm_gt_c', 'high', 'h_p_c',
                         'h_gt_c', 'acc', 'total_cnt', 'recall_macro',
                         'low_tp', 'low_fn', 'low_fp', 'med_tp', 'med_fn', 'med_fp', 'hi_tp', 'hi_fn', 'hi_fp'])
    for tav in TAV:
        for th_h in np.linspace(0.95, 0.55, 9, endpoint=True):
            for th_l in [th_h]:  # np.linspace(0.95, 0.55, 9, endpoint=True):
                # df_all = {}

                df_low1 = pd.read_csv(Cfg.OUT_PATH_CV + 'low_' + tav + '/' + 'CV_info.csv')
                df_low = df_low1[['wavemd5', 'score']]

                df_high1 = pd.read_csv(Cfg.OUT_PATH_CV + 'high_' + tav + '/' + 'CV_info.csv')
                df_high = df_high1[['wavemd5', 'score']]

                th_d = {'high_upper': th_h, 'high_lower': th_h, 'low_upper': th_l, 'low_lower': th_l}

                df_fuse = AuxFunc.fuse_two_detectors(tav, df_high, df_low, th_d)
                df_fuse = df_fuse[['wavemd5', tav + '_predict']]

                fuse_with_gt = pd.merge(left=gt_old, right=df_fuse, on='wavemd5')
                fuse_with_gt = fuse_with_gt[[tav, tav + '_predict']]
                before_drop = len(fuse_with_gt)
                fuse_with_gt.drop(fuse_with_gt[fuse_with_gt[tav] == 'ambiguous'].index, inplace=True)
                fuse_with_gt.drop(fuse_with_gt[fuse_with_gt[tav + '_predict'] == 'ambiguous'].index, inplace=True)
                print('before drop = {}; after drop = {}'.format(before_drop, len(fuse_with_gt)))
                pred = fuse_with_gt[tav + '_predict'].values
                res = AuxFunc.calc_metrics(pred, fuse_with_gt[tav].values, np.unique(pred))  # test, pred

                l, lc, m, mc, h, hc, tot, totc, recall_macro = get_metrics_three_class(tav, res, print_me=True, th=th_h)

                l_count = np.unique(fuse_with_gt[tav].values, return_counts=True)
                try:
                    low_c = l_count[1][np.argwhere(l_count[0] == 'low')][0][0]
                    med_c = l_count[1][np.argwhere(l_count[0] == 'medium')][0][0]
                    high_c = l_count[1][np.argwhere(l_count[0] == 'high')][0][0]
                except:
                    low_c = -1
                    med_c = -1
                    high_c = -1

                # low
                pred_low = pred == 'low'
                gt_low = fuse_with_gt[tav].values == 'low'
                cm = confusion_matrix(gt_low, pred_low, [True, False])
                tp = cm[0, 0]
                fn = cm[1, 0]
                fp = cm[0, 1]

                # med
                pred_med = pred == 'medium'
                gt_med = fuse_with_gt[tav].values == 'medium'
                cmm = confusion_matrix(gt_med, pred_med, [True, False])
                tpm = cmm[0, 0]
                fnm = cmm[1, 0]
                fpm = cmm[0, 1]

                # high
                pred_high = pred == 'high'
                gt_high = fuse_with_gt[tav].values == 'high'
                cmh = confusion_matrix(gt_high, pred_high, [True, False])
                tph = cmh[0, 0]
                fnh = cmh[1, 0]
                fph = cmh[0, 1]
                csv_writer.writerow([tav, th_h, th_l, l, lc, low_c, m, mc, med_c, h, hc, high_c,tot, totc,
                                     recall_macro, tp, fn, fp, tpm, fnm, fpm, tph, fnh, fph])
