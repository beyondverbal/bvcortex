from itertools import product
import numpy as np
import PATrends.MiscMethods as Misc
from TAVEngine import HLDConfig as HLDCfg
from TAVEngine.FeatureOperators import FeatureOperators


class HLDFeatureVector:
    def __init__(self, key_list):
        self.FO = FeatureOperators()

        # Key list order has to match the feature order in feature_vec !
        self.key_list = key_list

    # Return an array of feature titles.
    # Key list order has to match the feature order in feature_vec !
    # Pass the keys as a list in order to keep the order fixed
    @staticmethod
    def feature_titles(key_list=HLDCfg.LLD_BASE, lld_operators=HLDCfg.OPERATOR_LIST,
                       lld_dimension=HLDCfg.AUG_LLD_BASE_DIMENSION):
        title_vec = []

        for key in key_list:
            # VAD features require 'manual' naming (for now)
            if key == 'VAD':
                vad_titles = HLDCfg.VAD_FEATURES
                title_vec = title_vec + vad_titles  
            else:
                for feature in lld_operators:
                    for feature_dim in range(0, HLDCfg.OPERATOR_LIST_DIM[feature]):
                        for row in range(0, lld_dimension[key]):
                            title = key + '_' + str(row) + '_' + feature + '_' + str(feature_dim)
                            title_vec.append(title)
                   
        return np.array(title_vec)

    # Computes features from a matrix
    @staticmethod
    def calc_hld_features(base_mat, operator_list=None, vad=None, trunc=None, voice_only_bypass=False, lld_names=None):

        feature_mat = np.copy(base_mat)  # TODO: check if necessary
        if len(feature_mat.shape) == 1:
            feature_mat = feature_mat.reshape(1, -1)
        feature_mat = feature_mat.T

        n, p = feature_mat.shape

        if trunc is not None:
            trunc[0] = np.minimum(trunc[0], n)
            trunc[1] = np.minimum(trunc[1], n)
            feature_mat = feature_mat[trunc[0]:trunc[1], :]
            if vad is not None:
                vad = vad[trunc[0]:trunc[1]]

        n, p = feature_mat.shape

        mask = np.ones((n,), dtype=bool)

        mask[np.logical_not(np.isfinite(np.sum(feature_mat, axis=1)))] = 0

        if vad is not None and (HLDCfg.USE_VOICED_ONLY or voice_only_bypass):
            mask[np.logical_not(vad)] = 0

        feature_mat = feature_mat[mask, :]

        n, p = feature_mat.shape

        fo = FeatureOperators()

        if operator_list is None:
            operator_list = list(fo.OperatorDictionary.keys())

        # cur_features = np.zeros((p*len(operator_list)))
        lens = [HLDCfg.OPERATOR_LIST_DIM[x] for x in operator_list]
        cur_features = np.zeros((p * np.sum(lens)))
        for i in range(len(operator_list)):
            operator_result = fo.apply_operator(operator_list[i], feature_mat.T, lens[i])
            # cur_features[np.arange(i*p, (i+1)*p)] = operator_result
            cur_features[np.arange(i * p * lens[i], (i + 1) * p * lens[i])] = operator_result

        if lld_names is not None:
            names = np.array([j + '_' + i for i, j in product(operator_list, lld_names)])
        else:
            names = []

        return cur_features, names

    @staticmethod
    def calc_vad_features(vad_vec, trunc_features):
        
        cur_features = []

        vad_vec_trunc = vad_vec[trunc_features[0]:trunc_features[1]]

        # vad ratio
        vad_R = len(vad_vec_trunc[vad_vec_trunc == 1]) / len(vad_vec_trunc)
        cur_features = np.append(cur_features, vad_R)

        # vad zero-crossings
        vad_diff = np.diff(vad_vec_trunc.astype(int))
        vad_zc = len(vad_diff[vad_diff != 0]) / HLDCfg.SEGMENT_LENGTH
        cur_features = np.append(cur_features, vad_zc)

        # vad-false stats
        vad_0 = Misc.contiguous_regions(vad_vec_trunc == 0)
        diff_0 = np.diff(vad_0, axis=1)
        if len(diff_0) == 0:
            cur_features = np.append(cur_features, 0)
            cur_features = np.append(cur_features, 0)
        else:
            cur_features = np.append(cur_features, np.mean(diff_0))
            cur_features = np.append(cur_features, np.std(diff_0))

        # vad-true stats
        vad_1 = Misc.contiguous_regions(vad_vec_trunc == 1)
        diff_1 = np.diff(vad_1, axis=1)
        if len(diff_1) == 0:
            cur_features = np.append(cur_features, 0)
            cur_features = np.append(cur_features, 0)
        else:
            cur_features = np.append(cur_features, np.mean(diff_1))
            cur_features = np.append(cur_features, np.std(diff_1))

        return cur_features

    def compute_features(self, lld_dict, lld_operators, lld_trunc):
        feature_vec = []

        vad_vec = lld_dict['VAD']

        for key in self.key_list:
            if key == 'VAD':
                feature_vec = np.hstack([feature_vec, self.calc_vad_features(vad_vec, lld_trunc)])
            else:
                feature_vec = np.hstack([feature_vec, self.calc_hld_features(lld_dict[key], operator_list=lld_operators,
                                                                             vad=vad_vec, trunc=lld_trunc)[0]])

        return feature_vec


if __name__ == '__main__':

    n_lld = 100

    n_samples = 1000

    base_matrix_in = np.random.rand(n_lld, n_samples)

    features, feature_names = HLDFeatureVector.calc_hld_features(base_matrix_in, trunc=[5, 90],
                                                                 lld_names=[str(i) for i in range(n_lld)])

    print(features.shape)
