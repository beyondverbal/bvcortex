from librosa.feature import delta
from TAVEngine.FeatureOperators import FeatureOperators
from TAVEngine import HLDConfig as HLDCfg


class PATDerivative:
    def __init__(self):
        self.FO = FeatureOperators()

    @staticmethod
    def add_derivatives(lld_dict, features_to_derive=HLDCfg.FEATURES_TO_DERIVE):
        new_dict = {}
        for key, value in lld_dict.items():

            if key in features_to_derive:
                # print('deriving {}'.format(key))
                new_k = 'd' + key
                tmp_delta = delta(value, width=3, order=1, axis=1)

                if HLDCfg.DERIVE_DELTA_DELTA:
                    # add delta delta
                    new_k2 = 'dd' + key
                    tmp_d_delta = delta(tmp_delta, width=3, order=1, axis=1)

                    new_entry = {new_k: tmp_delta, new_k2: tmp_d_delta}
                else:
                    new_entry = {new_k: tmp_delta}

                new_dict.update(new_entry)

        lld_dict.update(new_dict)

        return lld_dict
